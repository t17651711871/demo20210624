import com.geping.etl.UNITLOAN.dataDictionary.ZqFieldUtils;
import com.geping.etl.UNITLOAN.util.CheckDataUtils;
import com.geping.etl.UNITLOAN.util.DateUtils;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.lang.reflect.Array;
import java.util.*;

/**
 * @PACKAGE_NAME: PACKAGE_NAME
 * @USER: tangshuai
 * @DATE: 2021/5/20
 * @TIME: 15:31
 * @描述:
 */
public class Demo01 {
    @Test
    public void test01() {
        double a = 10;
        Double b = 10d;
        Double c = a;
        System.out.println("b = " + b);
        System.out.println("a = " + a);

    }

    @Test
    public void test02() {
        int a = 1;
        int b = 1;
        if (a == b) {
            System.out.println("相等");
        } else {
            System.out.println("不相等");
        }
    }

    @Test
    public void test03() {
        int a = 5;
        switch (a) {
            case 1:
                System.out.println("输出1");
                break;
            case 2:
                System.out.println("输出2");
                break;
            case 3:
                System.out.println("输出3");
                break;
            default:
                System.out.println("最终");
                break;
        }
    }

    @Test
    public void test04() {
        boolean a = false;
        int i = 0;
        while (!a) {
            i++;
            if (i % 3 == 0) {
                continue;
            }
            if (i == 10) {
                a = true;
            }
            System.out.println("i = " + i);
        }
    }

    @Test
    public void test05() {
        boolean a = false;
        int i = 0;
        do {
            i++;
            if (i == 10) {
                a = true;
            }
            System.out.println("i = " + i);
        } while (!a);
    }

    @Test
    public void test06() {
        String a =  "ts";
        String s = a.toUpperCase();
        System.out.println("s = " + s);
    }

    @Test
    public void test07() {
        List gqlxList = ZqFieldUtils.gqlxList;
        Map<String,List> map = new HashMap<>();
        map.put("gqlxList",gqlxList);

        List gqlxList1 = map.get("gqlxList");
        System.out.println(gqlxList1);
    }

    @Test
    public void test08() {
        String s ="10200";
        String s1 ="100";
        boolean b = Double.parseDouble(s) == Double.parseDouble(s1);
        System.out.println("b = " + b);
    }

    @Test
    public void test09() {
        String s ="2100-12-31";
        String s1 ="2021-05-11";
        boolean b = DateUtils.inCurrentMM(s);
        boolean b1 = DateUtils.inCurrentMM(s1);
        System.out.println("b = " + b);
        System.out.println("b1 = " + b1);
    }

    @Test
    public void test10() {
        String tzyezrmb = "0.00";
        boolean b =Double.parseDouble(tzyezrmb) <= 0;
        System.out.println("b = " + b);
    }
    @Test
    public void test11() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        //int month = cal.get(Calendar.MONTH);
        String monthStr;
        if(month<10) {
            monthStr = year+"-0"+month;
        }else {
            monthStr = year+"-"+month+1;
        }
        System.out.println("monthStr = " + monthStr);
    }
    @Test
    public void test12() {
        List baseCode = ZqFieldUtils.fxrhyList;
        System.out.println("baseCode = " + baseCode);
    }
    @Test
    public void test13() {
        String s = "TB99-";
        String substring = s.substring(0, 5);
        boolean equals = substring.equals(s);
        System.out.println("equals = " + equals);
    }
    @Test
    public void test14() {
        //校验是否存在某个符号
        String s = "Tb99.";
        Pattern p = Pattern.compile("[,，]");
        Matcher m = p.matcher(s);
        boolean match = m.find();
        System.out.println("match = " + match);
    }
    @Test
    public void test15() {
        Set<String> set = new HashSet<>();
        List<String> list =new LinkedList<>();
        set.add("1");
        set.add("1");
        list.add("1");
        list.add("1");
        System.out.println("set = " + set);
        System.out.println("list = " + list);
    }
}
