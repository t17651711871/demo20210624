package zzz;

/**
 * @PACKAGE_NAME: zzz
 * @USER: tangshuai
 * @DATE: 2021/6/4
 * @TIME: 14:57
 * @描述:
 */
public enum Test01 {
    A("张三","23"),
    B("李四","18")
    ;

    public static String getName(String code){
        for (Test01 t:Test01.values()
             ) {
            if (code.equals(t.name())){
                return t.name;
            }
        }
        return null;
    }
    public static String getAge(String code){
        for (Test01 t:Test01.values()
             ) {
            if (code.equals(t.name())){
                return t.getAge();
            }
        }
        return null;
    }

    private String name;
    private String age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    Test01(String name, String age) {
        this.name = name;
        this.age = age;
    }
}
class Test001{
    public static void main(String[] args) {
        String a = Test01.getName("A");
        String b = Test01.getAge("B");
        System.out.println("a = " + a);
        System.out.println("b = " + b);
    }
}