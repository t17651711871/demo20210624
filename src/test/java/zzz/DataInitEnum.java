package zzz;

/***
 *  数据初始化枚举类
 * @author liang.xu
 * @date 2021.4.28
 */
public enum DataInitEnum {


    CLDKXX("存量单位贷款基础数据信息", "bi_devp.pkg_fbdr_api.sp_xcldkxx(null,%s)"),
    DKFSXX("单位贷款发生额信息","bi_devp.pkg_fbdr_api.sp_xdkfsxx(null,%s)"),
    DKDBHT("单位贷款担保合同信息", "bi_devp.pkg_fbdr_api.sp_xdkdbht(null,%s)"),
    DKDBWX("单位贷款担保物信息","bi_devp.pkg_fbdr_api.sp_xdkdbwx(null,%s)"),
    FTYKHX("非同业单位客户基础信息","bi_devp.pkg_fbdr_api.sp_xftykhx(null,%s)"),
    CLGRDKXX("存量个人贷款信息", "bi_devp.pkg_fbdr_api.sp_xclgrdkxx(null,%s)"),
    GRDKFSXX("个人贷款发生额信息", "bi_devp.pkg_fbdr_api.sp_xgrdkfsxx(null,%s)"),
    GRKHXX("个人客户基础信息", "bi_devp.pkg_fbdr_api.sp_xgrkhxx(null,%s)");


    private String reportName;
    private String procedure;


    private DataInitEnum(String reportName, String  procedure) {
        this.reportName=reportName;
        this.procedure=procedure;
    }


    public static String getProcedure(String  code){
        for(DataInitEnum dataInit: DataInitEnum.values()){
            if(dataInit.name().equals(code)){
                return dataInit.getProcedure();
            }
        }
        return null;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

}