package zzz;

/**
 * @PACKAGE_NAME: zzz
 * @USER: tangshuai
 * @DATE: 2021/6/4
 * @TIME: 10:39
 * @描述:
 */
public enum EnumUser {
    ZS("张三", "小米", "17651711871"),
    LS("李四", "华为", "23454353453");


    /**
     * 通过表名获取手机号
     * @param className
     * @return
     */
    public static String getName(String className) {
        for (EnumUser user : EnumUser.values()
        ) {
            if (user.name().equals(className)){
                return user.price;
            }
        }
        return null;
    }


    private String name;
    private String phone;
    private String price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    private EnumUser(String name, String phone, String price) {
        this.name = name;
        this.phone = phone;
        this.price = price;
    }

    private EnumUser(String name) {
        this.name = name;
    }


}
class EnumDemo01{
    public static void main(String[] args) {

    }
}