package com.geping.etl.SHGJG.util;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

/**
 * jdbc工具类
 * @author WuZengWen
 * @date 2018年7月30日 上午10:09:06
 */
public class JDBCUtil {
	
	static String driver = "";
	static String url = "";
	static String username = "";
	static String password = "";
	static Connection con = null;
	static Properties prop = new Properties();
	
	/**
	 * 有参获取数据库连接池的方法，不填则取默认值
	 * @param jdbcpro自定义资源文件路径
	 * @param sqldriver自定义驱动名称
	 * @param sqlurl自定义路径地址
	 * @param sqluser自定义用户名
	 * @param sqlpass自定义密码
	 * @return
	 */
	public static Connection getConnection(String jdbcpro,String sqldriver,String sqlurl,String sqluser,String sqlpass) {
		try {
			//读取属性文件.properties,未填写则取默认值/data/jdbc.properties
			InputStream resourceAsStream = null;
			if(StringUtils.isNotBlank(jdbcpro)) {
				resourceAsStream = JDBCUtil.class.getClassLoader().getResourceAsStream(jdbcpro);
			}else {
				resourceAsStream = JDBCUtil.class.getClassLoader().getResourceAsStream("/intg/jdbc.properties");
			}
			if(StringUtils.isNotBlank(sqldriver)) {
				
			} else {
				sqldriver = "mysql.driverClassName";
			}
			if(StringUtils.isNotBlank(sqlurl)) {
							
			} else {
				sqlurl = "mysql.url";			
			}
			if(StringUtils.isNotBlank(sqluser)) {
				
			} else {
				sqluser = "mysql.username";
			}
			if(StringUtils.isNotBlank(sqlpass)) {
				
			} else {
				sqlpass = "mysql.password";
			}
			//加载属性列表
			prop.load(resourceAsStream);
			Iterator<String> it=prop.stringPropertyNames().iterator();
			while(it.hasNext()){
				String key=it.next();
					if(key.equals(sqldriver)) {
						driver = prop.getProperty(key);
					}
					if(key.equals(sqlurl)) {
						url = prop.getProperty(key);
					}
					if(key.equals(sqluser)) {
						username = prop.getProperty(key);
					}
					if(key.equals(sqlpass)) {
						password = prop.getProperty(key);
					}
				}
			Class.forName(driver);
			con = DriverManager.getConnection(url , username , password );
			//in.close();
			resourceAsStream.close();
			}catch(ClassNotFoundException cnfexce){   
				System.out.println("找不到驱动程序类 ,加载驱动失败:"+cnfexce);   
				cnfexce.printStackTrace() ;   
			}catch(SQLException sqlexce){   
			    System.out.println("sql语句执行失败:"+sqlexce);   
			    sqlexce.printStackTrace() ;   
			}catch(Exception e){
				System.out.println("连接失败:"+e);
				e.printStackTrace();
		    }
			return con; 
	}
	
	/**
	 * 无参获取数据库连接池的方法,默认资源文件路径为"/intg/jdbc.properties"
	 * 默认驱动名称,路径地址,用户名,密码分别为"oracle.driverClassName","oracle.url","oracle.username","oracle.password"
	 * @return
	 */
	public static Connection getConnection() {
		try{
		//读取属性文件.properties
		InputStream resourceAsStream = JDBCUtil.class.getClassLoader().getResourceAsStream("/intg/jdbc.properties");
		//加载属性列表
		prop.load(resourceAsStream);
		Iterator<String> it=prop.stringPropertyNames().iterator();
		while(it.hasNext()){
		String key=it.next();
			if(key.equals("mysql.driverClassName")) {
				driver = prop.getProperty(key);
			}
			if(key.equals("mysql.url")) {
				url = prop.getProperty(key);
			}
			if(key.equals("mysql.username")) {
				username = prop.getProperty(key);
			}
			if(key.equals("mysql.password")) {
				password = prop.getProperty(key);
			}
		}
		Class.forName(driver);
		con = DriverManager.getConnection(url , username , password );
		//in.close();
		resourceAsStream.close();
		}catch(ClassNotFoundException cnfexce){   
			System.out.println("找不到驱动程序类 ,加载驱动失败:"+cnfexce);   
			cnfexce.printStackTrace() ;   
		}catch(SQLException sqlexce){   
		    System.out.println("sql语句执行失败:"+sqlexce);   
		    sqlexce.printStackTrace() ;   
		}catch(Exception e){
			System.out.println("连接失败:"+e);
			e.printStackTrace();
	    }
		return con; 
	}
	
	/**
	 * 执行sql语句返回记录数量
	 * @param query 需要执行的sql语句
	 * @return
	 */
	public static Integer findTotalElements(String query) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		query = "select count(*) from (" + query + ")";
		try {
			ps=getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while(rs.next()) {
				count = rs.getInt(1);
			}
		} catch(SQLException sqlexce){   
		    System.out.println("sql语句执行失败:"+sqlexce);   
		    sqlexce.printStackTrace() ;   
		} finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return count;
	}
	
	/**
	 * 执行复杂的sql语句查询数据
	 * @param query 需要执行的sql语句
	 * @param idName 目标表的主键名称
	 * @param firstIndex 查询起始页
	 * @param pageSize 查询数量
	 * @param strings 查询字段集合不能为空和*
	 * @return
	 */
	public static List<Object[]> selectQuery(String query,String idName,Integer firstIndex,Integer pageSize,String... strings){
		String dbname = "";
		List<Object[]> list = new ArrayList<Object[]>();
		try{
			//读取属性文件.properties
			InputStream resourceAsStream = JDBCUtil.class.getClassLoader().getResourceAsStream("/intg/jdbc.properties");
			//加载属性列表
			prop.load(resourceAsStream);
			Iterator<String> it=prop.stringPropertyNames().iterator();
			while(it.hasNext()){
			String key=it.next();
				if(key.equals("mysql.driverClassName")) {
					driver = prop.getProperty(key);
					if("oracle.jdbc.driver.OracleDriver".equals(driver)) {
						dbname = "oracle";
					}else if("net.sourceforge.jtds.jdbc.Driver".equals(driver)) {
						dbname = "sqlserver";
					}else if("com.mysql.jdbc.Driver".equals(driver)) {
						dbname = "mysql";
					}
				}
				if(key.equals("mysql.url")) {
					url = prop.getProperty(key);
				}
				if(key.equals("mysql.username")) {
					username = prop.getProperty(key);
				}
				if(key.equals("mysql.password")) {
					password = prop.getProperty(key);
				}
			}
			Class.forName(driver);
			con = DriverManager.getConnection(url , username , password );
			if(StringUtils.isNotBlank(dbname)) {
				if("oracle".equals(dbname)) {
					list = oracleQuery(con,query,idName,firstIndex,pageSize,strings);
				}else if("sqlserver".equals(dbname)) {
					list = sqlserverQuery(con,query,idName,firstIndex,pageSize,strings);
				}else if("mysql".equals(dbname)) {
					list = mysqlQuery(con,query,idName,firstIndex,pageSize,strings);
				}
			}
			//in.close();
			resourceAsStream.close();
			}catch(ClassNotFoundException cnfexce){   
				System.out.println("找不到驱动程序类 ,加载驱动失败:"+cnfexce);   
				cnfexce.printStackTrace() ;   
			}catch(SQLException sqlexce){   
			    System.out.println("sql语句执行失败:"+sqlexce);   
			    sqlexce.printStackTrace() ;   
			}catch(Exception e){
				System.out.println("连接失败:"+e);
				e.printStackTrace();
		    }
		return list;
	}
	
	public static List<Object[]> selectQuery1(String query,String state,String decideopiniondate, Integer firstIndex,Integer pageSize,String... strings){
		String dbname = "";
		List<Object[]> list = new ArrayList<Object[]>();
		try{
			//读取属性文件.properties
			InputStream resourceAsStream = JDBCUtil.class.getClassLoader().getResourceAsStream("/intg/jdbc.properties");
			//加载属性列表
			prop.load(resourceAsStream);
			Iterator<String> it=prop.stringPropertyNames().iterator();
			while(it.hasNext()){
			String key=it.next();
				if(key.equals("mysql.driverClassName")) {
					driver = prop.getProperty(key);
					if("oracle.jdbc.driver.OracleDriver".equals(driver)) {
						dbname = "oracle";
					}else if("net.sourceforge.jtds.jdbc.Driver".equals(driver)) {
						dbname = "sqlserver";
					}else if("com.mysql.jdbc.Driver".equals(driver)) {
						dbname = "mysql";
					}
				}
				if(key.equals("mysql.url")) {
					url = prop.getProperty(key);
				}
				if(key.equals("mysql.username")) {
					username = prop.getProperty(key);
				}
				if(key.equals("mysql.password")) {
					password = prop.getProperty(key);
				}
			}
			Class.forName(driver);
			con = DriverManager.getConnection(url , username , password );
			if(StringUtils.isNotBlank(dbname)) {
				if("oracle".equals(dbname)) {
					list = oracleQuery1(con,query,state,decideopiniondate,firstIndex,pageSize,strings);
				}else if("sqlserver".equals(dbname)) {
					list = sqlserverQuery(con,query,state,firstIndex,pageSize,strings);
				}else if("mysql".equals(dbname)) {
					list = mysqlQuery(con,query,state,firstIndex,pageSize,strings);
				}
			}
			//in.close();
			resourceAsStream.close();
			}catch(ClassNotFoundException cnfexce){   
				System.out.println("找不到驱动程序类 ,加载驱动失败:"+cnfexce);   
				cnfexce.printStackTrace() ;   
			}catch(SQLException sqlexce){   
			    System.out.println("sql语句执行失败:"+sqlexce);   
			    sqlexce.printStackTrace() ;   
			}catch(Exception e){
				System.out.println("连接失败:"+e);
				e.printStackTrace();
		    }
		return list;
	}
	
	/**
	 * 执行复杂的sql语句适用于oracle数据库
	 * @param con 数据库连接池
	 * @param query 需要执行的sql语句
	 * @param idName 目标表的主键名称
	 * @param firstIndex 查询起始页
	 * @param pageSize 查询数量
	 * @param strings 查询字段集合不能为空和*
	 * @return
	 */
	public static List<Object[]> oracleQuery(Connection con,String query,String idName,Integer firstIndex,Integer pageSize,String... strings){
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Object[]> list = new ArrayList<Object[]>();
		if(firstIndex > 0) {
			Integer temp = firstIndex;
			firstIndex = temp*pageSize;
			pageSize = (temp+1)*pageSize;
		}
		query = "select * from(select * from(select table1_1.*,row_number() over(order by "+ idName +" asc) as rownumber from(" +
		query + ") table1_1) table1_2 where table1_2.rownumber>" + firstIndex + ") where rownumber <=" + pageSize;
		try {
			ps=con.prepareStatement(query);
			rs = ps.executeQuery();
			while(rs.next()) {
				if(strings != null && strings.length > 0) {
					Object[] obj = new Object[strings.length];
					for(int i=0;i<strings.length;i++) {
						obj[i] = rs.getString(strings[i]);
					}
					list.add(obj);
				}
			}
		} catch(SQLException sqlexce){   
		    System.out.println("sql语句执行失败:"+sqlexce);   
		    sqlexce.printStackTrace() ;   
		} finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	public static List<Object[]> oracleQuery1(Connection con,String query,String state,String decideopiniondate,Integer firstIndex,Integer pageSize,String... strings){
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Object[]> list = new ArrayList<Object[]>();
		if(firstIndex > 0) {
			Integer temp = firstIndex;
			firstIndex = temp*pageSize;
			pageSize = (temp+1)*pageSize;
		}
		query = "select * from(select * from(select table1_1.*,row_number() over(order by "+ state +" asc,"+decideopiniondate+" desc) as rownumber from(" +
		query + ") table1_1) table1_2 where table1_2.rownumber>" + firstIndex + ") where rownumber <=" + pageSize;
		try {
			ps=con.prepareStatement(query);
			rs = ps.executeQuery();
			while(rs.next()) {
				if(strings != null && strings.length > 0) {
					Object[] obj = new Object[strings.length];
					for(int i=0;i<strings.length;i++) {
						obj[i] = rs.getString(strings[i]);
					}
					list.add(obj);
				}
			}
		} catch(SQLException sqlexce){   
		    System.out.println("sql语句执行失败:"+sqlexce);   
		    sqlexce.printStackTrace() ;   
		} finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	/**
	 * 执行复杂的sql语句适用于sqlserver数据库
	 * @param con 数据库连接池
	 * @param query 需要执行的sql语句
	 * @param idName 目标表的主键名称
	 * @param firstIndex 查询起始页
	 * @param pageSize 查询数量
	 * @param strings 查询字段集合不能为空和*
	 * @return
	 */
	public static List<Object[]> sqlserverQuery(Connection con,String query,String idName,Integer firstIndex,Integer pageSize,String... strings){
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Object[]> list = new ArrayList<Object[]>();
		if(firstIndex > 0) {
			Integer temp = firstIndex;
			firstIndex = temp*pageSize;
			pageSize = (temp+1)*pageSize;
		}
		query = "select top " + pageSize + " table1_1.* from (select row_number() over(order by " + idName + " asc) as rownumber,* from(" +
		query + ") as table1_2) as table1_1 where rownumber>" + firstIndex;
		try {
			ps=con.prepareStatement(query);
			rs = ps.executeQuery();
			while(rs.next()) {
				if(strings != null && strings.length > 0) {
					Object[] obj = new Object[strings.length];
					for(int i=0;i<strings.length;i++) {
						obj[i] = rs.getString(strings[i]);
					}
					list.add(obj);
				}
			}
		} catch(SQLException sqlexce){   
		    System.out.println("sql语句执行失败:"+sqlexce);   
		    sqlexce.printStackTrace() ;   
		} finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	/**
	 * 执行复杂的sql语句适用于mysql数据库
	 * @param con 数据库连接池
	 * @param query 需要执行的sql语句
	 * @param idName 目标表的主键名称
	 * @param firstIndex 查询起始页
	 * @param pageSize 查询数量
	 * @param strings 查询字段集合不能为空和*
	 * @return
	 */
	public static List<Object[]> mysqlQuery(Connection con,String query,String idName,Integer firstIndex,Integer pageSize,String... strings){
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Object[]> list = new ArrayList<Object[]>();
		if(firstIndex > 0) {
			Integer temp = firstIndex;
			firstIndex = temp*pageSize;
			pageSize = (temp+1)*pageSize;
		}
		query = "select table1_1.* from(" + query + ") table1_1 limit " + firstIndex + "," + pageSize;
		try {
			ps=con.prepareStatement(query);
			rs = ps.executeQuery();
			while(rs.next()) {
				if(strings != null && strings.length > 0) {
					Object[] obj = new Object[strings.length];
					for(int i=0;i<strings.length;i++) {
						obj[i] = rs.getString(strings[i]);
					}
					list.add(obj);
				}
			}
		} catch(SQLException sqlexce){   
		    System.out.println("sql语句执行失败:"+sqlexce);   
		    sqlexce.printStackTrace() ;   
		} finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	/**
	 * 获取数据库方言的方法
	 * @return ORACLE SQLSERVER MYSQL
	 */
	public static String getDriver() {
		String driver = "";
		try {
			//读取属性文件.properties,未填写则取默认值/data/jdbc.properties
			InputStream resourceAsStream = null;
			resourceAsStream = JDBCUtil.class.getClassLoader().getResourceAsStream("/intg/jdbc.properties");
			//加载属性列表
			prop.load(resourceAsStream);
			Iterator<String> it=prop.stringPropertyNames().iterator();
			while(it.hasNext()){
				String key=it.next();
				if(key.equals("mysql.driverClassName")) {
					String driverClassName = prop.getProperty(key);
					if("oracle.jdbc.driver.OracleDriver".equals(driverClassName)) {
						driver = "ORACLE";
					}else if("net.sourceforge.jtds.jdbc.Driver".equals(driverClassName)) {
						driver = "SQLSERVER";
					}else if("com.mysql.jdbc.Driver".equals(driverClassName)) {
						driver = "MYSQL";
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return driver; 
	}
}
