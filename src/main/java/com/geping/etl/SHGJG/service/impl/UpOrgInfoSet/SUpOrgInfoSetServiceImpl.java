package com.geping.etl.SHGJG.service.impl.UpOrgInfoSet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.repository.UpOrgInfoSet.SUpOrgInfoSetRepository;

@Transactional
@Service
public class SUpOrgInfoSetServiceImpl implements com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService {

	@Autowired
	private SUpOrgInfoSetRepository sr;
	
	@Override
	public List<SUpOrgInfoSet> findAll() {
		
		return sr.findAll();
	}

	@Override
	public SUpOrgInfoSet save(SUpOrgInfoSet entity) {
		
		return sr.save(entity);
	}

}
