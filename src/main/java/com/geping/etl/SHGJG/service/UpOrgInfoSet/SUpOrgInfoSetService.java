package com.geping.etl.SHGJG.service.UpOrgInfoSet;

import java.util.List;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;

public interface SUpOrgInfoSetService {
	
	List<SUpOrgInfoSet> findAll();
	
	SUpOrgInfoSet save(SUpOrgInfoSet entity);
}
