package com.geping.etl.SHGJG.entity.UpOrgInfoSet;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="suporginfo")
public class SUpOrgInfoSet {
	
	@Id
	private String id;
	private String bankcodefaren;
	private String bankcodelei;
	private String bankcodewangdian;
	private String bankname;
	private String isusedelete;
	private String isuseupdate;
	private String messagepath;
	private String isusedepart;
	private String isusedepartformessage;
	private String shifoushenheziji;
	private String checklimit;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getBankcodefaren() {
		return bankcodefaren;
	}
	public void setBankcodefaren(String bankcodefaren) {
		this.bankcodefaren = bankcodefaren;
	}
	public String getBankcodelei() {
		return bankcodelei;
	}
	public void setBankcodelei(String bankcodelei) {
		this.bankcodelei = bankcodelei;
	}
	public String getBankcodewangdian() {
		return bankcodewangdian;
	}
	public void setBankcodewangdian(String bankcodewangdian) {
		this.bankcodewangdian = bankcodewangdian;
	}
	public String getBankname() {
		return bankname;
	}
	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	public String getIsusedelete() {
		return isusedelete;
	}
	public void setIsusedelete(String isusedelete) {
		this.isusedelete = isusedelete;
	}
	public String getIsuseupdate() {
		return isuseupdate;
	}
	public void setIsuseupdate(String isuseupdate) {
		this.isuseupdate = isuseupdate;
	}
	public String getMessagepath() {
		return messagepath;
	}
	public void setMessagepath(String messagepath) {
		this.messagepath = messagepath;
	}
	public String getIsusedepart() {
		return isusedepart;
	}
	public void setIsusedepart(String isusedepart) {
		this.isusedepart = isusedepart;
	}
	public String getIsusedepartformessage() {
		return isusedepartformessage;
	}
	public void setIsusedepartformessage(String isusedepartformessage) {
		this.isusedepartformessage = isusedepartformessage;
	}
	public String getShifoushenheziji() {
		return shifoushenheziji;
	}
	public void setShifoushenheziji(String shifoushenheziji) {
		this.shifoushenheziji = shifoushenheziji;
	}
	public String getChecklimit() {
		return checklimit;
	}
	public void setChecklimit(String checklimit) {
		this.checklimit = checklimit;
	}
	
}
