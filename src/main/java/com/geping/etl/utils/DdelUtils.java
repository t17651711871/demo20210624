package com.geping.etl.utils;

import org.apache.commons.lang.StringUtils;

/**
 * Dde 防注入 工具类
 * @author  liang.xu
 * @date 2021年5月10日
 */
public class DdelUtils {

    public static String EQUAL="=";

    public static  String PLUS="+";

    public static  String  HYPERLINK="HYPERLINK";

    public  static String  EmptyString="";

    /**
     * 过滤特殊
     * @param source
     * @return
     */
    public static String filterDde(String source) {
       if(StringUtils.isNotBlank(source)){
           return source.replace(EQUAL,EmptyString).replace(PLUS,EmptyString).replace(HYPERLINK,EmptyString);
       }
       return source;
    }

    public static void main(String[] args) {
        String v="+=测试 HYPERLINK--- 999";
        System.out.println(DdelUtils.filterDde(v));
    }
}