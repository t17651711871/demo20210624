package com.geping.etl.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**  
 * 常用加密算法工具类  
 * @author 
 */    

public class EncryptUtils {

	/**  
	     * 用MD5算法进行加密  
	     * @param str 需要加密的字符串  
	     * @return MD5加密后的结果  
	     */ 

	public static String encodeMD5String(String str) {
		
	       return encode(str, "MD5");    
	}  

	
	
	private static String encode(String str, String method) { 
		
		MessageDigest md = null;    
	    String dstr = null;    
        try {    
		    md = MessageDigest.getInstance(method);    
		    md.update(str.getBytes());    
		    dstr = new BigInteger(1, md.digest()).toString(16);    
	    } catch (NoSuchAlgorithmException e) {    
		    e.printStackTrace();    
	    }    
	   return dstr;    
     }    

	
	//加密解密算法 执行一次加密，两次解密 
	public static String convertMD5(String inStr){  
		  
        char[] a = inStr.toCharArray();  
        for (int i = 0; i < a.length; i++){  
            a[i] = (char) (a[i] ^ 't');  
        }  
        String s = new String(a);  
        return s;  
  
    }  
}
