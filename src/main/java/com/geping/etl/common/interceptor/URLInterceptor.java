package com.geping.etl.common.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;



public class URLInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		String url = request.getRequestURI();//获取请求路径
		if(url.endsWith("/login") || url.endsWith("/loginUi")){
			return true;
		}
		
		//如果用户未登录
        if(whetherLogin(request)){
        	response.sendRedirect(request.getContextPath()+"/loginUi");
        	return false;
        }
        
        return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	//验证用户是否登录
	public boolean whetherLogin(HttpServletRequest request){
			
		Sys_UserAndOrgDepartment user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		return user == null;
	} 
	
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub

	}

}
