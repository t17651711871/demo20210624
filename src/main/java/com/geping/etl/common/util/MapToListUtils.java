package com.geping.etl.common.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

//为传进来的map集合把值存到list集合中
public class MapToListUtils {
	
	public List<String> getListOfMap(Map<Integer,String> map){
		
		if(!map.isEmpty()){ //如果集合不为空
			List<String> list = new ArrayList<String>();
	        Map<Integer, String> resultMap = sortMapByKey(map);
			Iterator<Map.Entry<Integer,String>> entries = resultMap.entrySet().iterator();  
			while (entries.hasNext()){
			    Map.Entry<Integer,String> entry = entries.next();
			    list.add(entry.getValue());
			}
			return list;
		}else{
			return null;
		}
	}
	
	
	 /**
     * 使用 Map按key进行排序
     * @param map
     * @return
     */
    public Map<Integer, String> sortMapByKey(Map<Integer, String> map){
        if (map == null || map.isEmpty()){
            return null;
        }

        Map<Integer, String> sortMap = new TreeMap<Integer, String>();
        sortMap.putAll(map);
        return sortMap;
    }
}
