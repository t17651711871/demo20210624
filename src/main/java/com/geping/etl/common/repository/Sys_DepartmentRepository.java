package com.geping.etl.common.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.geping.etl.common.entity.Sys_Department;

@Repository
public interface Sys_DepartmentRepository extends PagingAndSortingRepository<Sys_Department, String>{
	
	
	//查询所有未删除的部门
	@Query("select s from Sys_Department s where s.isDelete = 'N'")
	public List<Sys_Department> findAllSysDepartment();
	
	//根据用户名模糊查询部门信息
	@Query("select s from Sys_Department s where s.isDelete = 'N' and s.departmentName like CONCAT('%',?1,'%')")
	public List<Sys_Department> getSys_DepartmentByLikeDepartmentName(String departmentName);
	
	//获取最大ID数
	@Query("select max(s.id) from Sys_Department s")
	public String findMaxId();
	
	//修改部门信息
	@Modifying
	@Query("update Sys_Department s set s.departmentName = ?2,s.description = ?3 where s.id = ?1")
	public int updateSys_Department(String id_edit,String departmentName,String description);
	
	//逻辑删除部门
	@Modifying
	@Query("update Sys_Department s set s.isDelete = 'Y' where s.id = ?1")
	public int deleteSys_Department(String id);

}
