package com.geping.etl.common.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.geping.etl.common.entity.Sys_Menu;

/**
 * 
 * @author Mr.chen
 *	菜单权限  Sys_Menu
 */
@Repository
public interface Sys_MenuRepository extends PagingAndSortingRepository<Sys_Menu, String>{
	
	@Query("select sm from Sys_Menu sm where sm.subjectId = ?1 order by id ASC")
	public List<Sys_Menu> getMenuBySubjectId(String subjectId);
} 
