package com.geping.etl.common.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.geping.etl.common.entity.Sys_Auth_Role_User;

@Repository
public interface Sys_Auth_Role_UserRepository extends PagingAndSortingRepository<Sys_Auth_Role_User, String>{
	
	//点击角色分配中的保存按钮，将之前分配的角色全部清空，本次实现真实删除，并非逻辑上删除
	@Modifying
	@Query("delete from Sys_Auth_Role_User where userId = ?1")
	public Integer deleteSysAuthRoleUser(String userId);
	//通过userId查询，是否已经存在角色分配，如果已经存在的话进行回显	
	@Query("select s from Sys_Auth_Role_User s where userId = ?1")
	public List<Sys_Auth_Role_User> findSysAuthRoleUserByRoleId(String userId);
	
	@Query("select s from Sys_Auth_Role_User s where roleId = ?1")
	public List<Sys_Auth_Role_User> findRoleUserByRoleId(String roleId);
}
