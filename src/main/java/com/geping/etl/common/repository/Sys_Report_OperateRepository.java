package com.geping.etl.common.repository;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.geping.etl.common.entity.Sys_Report_Operate;

/**
 * 
 * @author Mr.chen
 *	报表操作权限
 *	表：Sys_Report_Operate
 */

@Repository
public interface Sys_Report_OperateRepository extends PagingAndSortingRepository<Sys_Report_Operate, Integer>{

}
