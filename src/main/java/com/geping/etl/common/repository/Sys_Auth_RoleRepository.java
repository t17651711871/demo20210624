package com.geping.etl.common.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.geping.etl.common.entity.Sys_Auth_Role;

/**
 * 
 * @author Mr.chen
 *	角色管理模块
 *	表：Sys_Auth_Role
 */
@Repository
public interface Sys_Auth_RoleRepository extends PagingAndSortingRepository<Sys_Auth_Role, String>{
	
	
	//获取最大ID数
	@Query("select max(s.id) from Sys_Auth_Role s")
	public String findMaxId();
	
	@Query("select s from Sys_Auth_Role s")
	public List<Sys_Auth_Role> getAllSys_Auth_Role();
	
	//显示子系统名称,需要和Sys_Org两表联查才行！
	/*@Query("select org.ORG_NAME from Sys_Org as org,Sys_Auth_Role as role where org.SUBJECT_ID = role.SUBJECT_ID")
	public List<Sys_Org> getSSName();*/
	
	//根据角色名称查询，避免新增角色时系统中的角色名称中文名重复
	@Query("select s from Sys_Auth_Role s where s.isDelete = 'N' and  s.roleName = ?1")
	public Sys_Auth_Role getRoleName(String roleName);
	
	//根据角色名模糊查询角色
	@Query("select s from Sys_Auth_Role s where s.isDelete = 'N' and s.subjectId = ?1 and s.roleName like CONCAT('%',?2,'%')")
	public List<Sys_Auth_Role> getSys_Auth_RoleByLikeRoleName(String subjectId,String roleName);
	
	//修改角色名称和角色描述
	//本方法中使用参数封装成对象，用@Param("setCanshu")Sys_Auth_Role setCanshu作为参数
	//给参数赋值：s.roleName = :#{#setCanshu.roleName} , s.description = :#{#setCanshu.description}  where s.id = :#{#setCanshu.id}
	@Modifying
	@Query("update Sys_Auth_Role s set s.roleName = :#{#setCanshu.roleName} , s.description = :#{#setCanshu.description}  where s.id = :#{#setCanshu.id}")
	public Integer updateRoleNameAndRoleDesc(@Param("setCanshu")Sys_Auth_Role setCanshu);
	
	//删除角色管理（逻辑删除，数据库中并未删除）
	@Modifying
	@Query("update Sys_Auth_Role s set s.isDelete = 'Y' where id = ?1")
	public Integer deleteSys_Auth_Role(String id);
	
	@Query("select s from Sys_Auth_Role s where s.isDelete = 'N'")
	public List<Sys_Auth_Role> getSysAuthRoleNotStop();
	
	@Query("select s from Sys_Auth_Role s where s.subjectId = ?1")
	public List<Sys_Auth_Role> findRoleBySubjectId(String subjectId);
}
