package com.geping.etl.common.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.geping.etl.common.entity.Sys_Auth_Role_Resource;

/**
 * 
 * @author Mr.chen
 *	角色资源对照表
 *	表：sys_auth_role_resource
 */
@Repository
public interface Sys_Auth_Role_ResourceRepository extends PagingAndSortingRepository<Sys_Auth_Role_Resource , String>{
	@Modifying
	@Query("delete from Sys_Auth_Role_Resource where roleId = ?1")
	public Integer deleteSysAuthRoleResource(String id);
	
	//查询出已经有角色权限的数据回显到页面
	@Query("select s from Sys_Auth_Role_Resource s where roleId = ?1")
	public List<Sys_Auth_Role_Resource> findAllByRoleId(String roleId);
	
	@Query(value="SELECT a.* FROM sys_auth_role_resource a" 
	            +"  WHERE a.role_id in (SELECT ROLE_ID FROM sys_auth_role_user WHERE USER_ID = ?1)"
			    +"  and a.SUBJECT_ID = ?2"
			    +"  ORDER BY a.ID",nativeQuery=true)
	public List<Sys_Auth_Role_Resource> findResourceByRoleAndSubject(String userId,String subjectId);
}
