package com.geping.etl.common.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.geping.etl.common.entity.Sys_Subject;

@Repository
public interface Sys_SubjectRepository extends PagingAndSortingRepository<Sys_Subject, String>,JpaSpecificationExecutor<Sys_Subject>{
	@Query("select s from Sys_Subject s order by id ASC")
	public List<Sys_Subject> findAllSubject();
	
	@Query(value="SELECT * FROM sys_subject a"
	       +" RIGHT JOIN (SELECT DISTINCT c.SUBJECT_ID FROM sys_auth_role c RIGHT JOIN  (SELECT * FROM sys_auth_role_user saru where saru.USER_ID = ?1) d ON c.ID = d.ROLE_ID) b ON a.ID = b.SUBJECT_ID order by a.ID ASC",nativeQuery=true)
	public List<Sys_Subject> findSubjectByUserId(String userId);
	
	@Query("select s.id from Sys_Subject s")
	public List<String> getAllId();
}
