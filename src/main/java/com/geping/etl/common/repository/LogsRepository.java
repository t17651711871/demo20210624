package com.geping.etl.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.geping.etl.common.entity.Logs;

@Repository
public interface LogsRepository extends JpaRepository<Logs,String>,JpaSpecificationExecutor<Logs> {

}
