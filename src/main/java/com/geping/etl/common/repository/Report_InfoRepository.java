package com.geping.etl.common.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.geping.etl.common.entity.Report_Info;

@Repository
public interface Report_InfoRepository extends JpaRepository<Report_Info,Integer>,JpaSpecificationExecutor<Report_Info>{
	
	@Query("select g from Report_Info g where subjectId = ?1 order by id ASC")
	public List<Report_Info> getReportBySubjectId(String subjectId);
	
    //修改待提交数量
    @Modifying
	@Query("update Report_Info g set g.pending = :#{#ri.pending} where g.code = :#{#ri.code}")
    public void updateReportInfoPending(@Param("ri")Report_Info ri);
    
    //修改待审核数量
    @Modifying
	@Query("update Report_Info g set g.pendingAudit = :#{#ri.pendingAudit} where g.code = :#{#ri.code}")
    public void updateReportInfoPendingAudit(@Param("ri")Report_Info ri);
    
    //修改审核不通过数量
    @Modifying
	@Query("update Report_Info g set g.checkFail = :#{#ri.checkFail} where g.code = :#{#ri.code}")
    public void updateReportInfoCheckFail(@Param("ri")Report_Info ri);
    
    //修改审核通过数量
    @Modifying
	@Query("update Report_Info g set g.checkSuccess = :#{#ri.checkSuccess},g.noMessageName = :#{#ri.noMessageName} where g.code = :#{#ri.code}")
    public void updateReportInfoCheckSuccess(@Param("ri")Report_Info ri);
}
