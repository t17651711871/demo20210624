package com.geping.etl.common.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * 
 * @author Mr.chen
 *	报表信息表 -- 报表权限
 */
@Entity(name="Report_Info")
@Table(name="SYS_REPORT_INFO")
public class Report_Info {
	@Id
	private Integer id;
	private String subjectId;//报表所属项目编号
	private String code;//报表代码
	private String name;//报表名称
	private String freq;//报表频度
	private String unit;//报表基本单位
	private BigDecimal errorRange;//容差范围
	private String instruction;//报表说明
	private String createUser;//创建记录的用户
	private String createTime;//创建时间
	private String updateUser;//更新记录的用户
	private String updateTime;//更新时间
	private BigDecimal batch;//批次
	private byte[] template;//报表模板
	private String pending;//待提交数据量
	private String pendingAudit;//待审核数据量
	private String checkSuccess;//审核通过数据量
	private String checkFail;//审核不通过数据量
	private String noMessageName;// 未生成报文名数量
	private String module; //所属模块
	private String cursorUrl; //查询临时表待提交，待审核，审核不通过数据量链接
	private String url;  //查询正式表审核通过数据量链接
	private String generateMessageUrl; //生成报文页面查询未生成报文数据量链接
	private String historyDataUrl;  //历史数据页面查询已生成报文数据量链接
	private String enName;//报表名对应字符串
	
	public Report_Info() {
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFreq() {
		return freq;
	}
	public void setFreq(String freq) {
		this.freq = freq;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getInstruction() {
		return instruction;
	}
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public BigDecimal getErrorRange() {
		return errorRange;
	}
	public void setErrorRange(BigDecimal errorRange) {
		this.errorRange = errorRange;
	}
	public BigDecimal getBatch() {
		return batch;
	}
	public void setBatch(BigDecimal batch) {
		this.batch = batch;
	}
	public byte[] getTemplate() {
		return template;
	}
	public void setTemplate(byte[] template) {
		this.template = template;
	}
	

	public String getPending() {
		return pending;
	}
	public void setPending(String pending) {
		this.pending = pending;
	}
	public String getPendingAudit() {
		return pendingAudit;
	}
	public void setPendingAudit(String pendingAudit) {
		this.pendingAudit = pendingAudit;
	}
	public String getCheckSuccess() {
		return checkSuccess;
	}
	public void setCheckSuccess(String checkSuccess) {
		this.checkSuccess = checkSuccess;
	}
	public String getCheckFail() {
		return checkFail;
	}
	public void setCheckFail(String checkFail) {
		this.checkFail = checkFail;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getCursorUrl() {
		return cursorUrl;
	}
	public void setCursorUrl(String cursorUrl) {
		this.cursorUrl = cursorUrl;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getNoMessageName() {
		return noMessageName;
	}
	public void setNoMessageName(String noMessageName) {
		this.noMessageName = noMessageName;
	}
	public String getGenerateMessageUrl() {
		return generateMessageUrl;
	}
	public void setGenerateMessageUrl(String generateMessageUrl) {
		this.generateMessageUrl = generateMessageUrl;
	}
	public String getHistoryDataUrl() {
		return historyDataUrl;
	}
	public void setHistoryDataUrl(String historyDataUrl) {
		this.historyDataUrl = historyDataUrl;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}
}
