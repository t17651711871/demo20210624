package com.geping.etl.common.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity(name="Logs")
@Table(name="SYS_LOGS")
public class Logs {
	@Id
	private String id;
	private String loginId;   //操作人
	private String logContent; //日志内容
	private String logType;   //日志类型
	private String logOrgCode; //银行机构代码
	private String logDate;   //日志生成时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getLogContent() {
		return logContent;
	}
	public void setLogContent(String logContent) {
		this.logContent = logContent;
	}
	public String getLogType() {
		return logType;
	}
	public void setLogType(String logType) {
		this.logType = logType;
	}
	public String getLogOrgCode() {
		return logOrgCode;
	}
	public void setLogOrgCode(String logOrgCode) {
		this.logOrgCode = logOrgCode;
	}
	public String getLogDate() {
		return logDate;
	}
	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}
	
	
}
