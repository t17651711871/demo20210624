package com.geping.etl.common.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 * @author Mr.chen
 * 机构管理表
 */
@Entity(name="Sys_Org")
@Table(name="SYS_ORG")
public class Sys_Org {
	@Id
	private String id;
	private String subjectId;//机构所属项目编号
	private String orgId;//机构编号   （银行机构代码）
	private String orgName;//机构名称（银行机构名称）
	private String orgInsideCode; //内部机构号
	private String licenseNumber;  //金融许可证号
	private String orgSortName;//机构简称
	private String orgParentId;//上级机构号
	private String orgRegion;//机构所属区域
	private String orgLayer;//机构层次
	private String orgPath;//机构层级path
	private String orgLevel;//机构等级
	private String orgAddress;//机构地址
	private String enabled;//是否可用
	private String isHead;//是否顶级机构
	private String tel;//电话(保存联系人电话)
	private String fax;//传真
	private String email;//电子邮件
	private String isBussiness;//是否业务机构 
	private String description;//描述信息
	private String startDt;//开始日期
	private String endDt;//关闭日期
	private String createTime;//创建时间
	private String orderNum;//排序
	private String option_;//可选字段（用作保存联系人）
	
	public Sys_Org(){
		this.isHead = "N";
		this.enabled = "Y";
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgSortName() {
		return orgSortName;
	}

	public void setOrgSortName(String orgSortName) {
		this.orgSortName = orgSortName;
	}

	public String getOrgInsideCode() {
		return orgInsideCode;
	}

	public void setOrgInsideCode(String orgInsideCode) {
		this.orgInsideCode = orgInsideCode;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getOrgParentId() {
		return orgParentId;
	}

	public void setOrgParentId(String orgParentId) {
		this.orgParentId = orgParentId;
	}

	public String getOrgRegion() {
		return orgRegion;
	}

	public void setOrgRegion(String orgRegion) {
		this.orgRegion = orgRegion;
	}



	public String getOrgPath() {
		return orgPath;
	}

	public void setOrgPath(String orgPath) {
		this.orgPath = orgPath;
	}

	public String getOrgLevel() {
		return orgLevel;
	}

	public void setOrgLevel(String orgLevel) {
		this.orgLevel = orgLevel;
	}

	public String getOrgAddress() {
		return orgAddress;
	}

	public void setOrgAddress(String orgAddress) {
		this.orgAddress = orgAddress;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getIsHead() {
		return isHead;
	}

	public void setIsHead(String isHead) {
		this.isHead = isHead;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIsBussiness() {
		return isBussiness;
	}

	public void setIsBussiness(String isBussiness) {
		this.isBussiness = isBussiness;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDt() {
		return startDt;
	}

	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}

	public String getEndDt() {
		return endDt;
	}

	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getOrgLayer() {
		return orgLayer;
	}

	public void setOrgLayer(String orgLayer) {
		this.orgLayer = orgLayer;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getOption_() {
		return option_;
	}

	public void setOption_(String option_) {
		this.option_ = option_;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	
}
