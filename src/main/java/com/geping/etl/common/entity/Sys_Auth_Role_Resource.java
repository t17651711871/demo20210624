package com.geping.etl.common.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 * @author Mr.chen
 *	角色资源对照
 *	表：sys_auth_role_resource
 */
@Entity(name="Sys_Auth_Role_Resource")
@Table(name="SYS_AUTH_ROLE_RESOURCE")
public class Sys_Auth_Role_Resource implements Comparable<Sys_Auth_Role_Resource>{
	@Id
	private String id;
	private String subjectId;  //角色资源所属的项目编号
	private String roleId;     //角色编号
	private String resType;    //资源类型
	private String resId;      //资源数据编号
	private String resValue;   //资源英文名
	private String resValueName;  //资源中文名
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getResType() {
		return resType;
	}
	public void setResType(String resType) {
		this.resType = resType;
	}
	public String getResId() {
		return resId;
	}
	public void setResId(String resId) {
		this.resId = resId;
	}
	public String getResValue() {
		return resValue;
	}
	public void setResValue(String resValue) {
		this.resValue = resValue;
	}
	public String getResValueName() {
		return resValueName;
	}
	public void setResValueName(String resValueName) {
		this.resValueName = resValueName;
	}
	
	@Override
	public int compareTo(Sys_Auth_Role_Resource o) {
		int i = Integer.parseInt(this.getResId()) - Integer.parseInt(o.getResId());
		return i;
	}
	
}
