package com.geping.etl.common.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 * @author Mr.chen
 * 报表操作按钮信息表：Sys_Report_Operate -- 报表操作权限 
 */
@Entity(name="Sys_Report_Operate")
@Table(name="SYS_REPORT_OPERATE")
public class Sys_Report_Operate {
	@Id
	private Integer id;
	private String subjectId;//操作代码所属项目编号
	private String reportOperateCode;//报表操作代码
	private String reportOperateName;//报表操作名称
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getReportOperateCode() {
		return reportOperateCode;
	}
	public void setReportOperateCode(String reportOperateCode) {
		this.reportOperateCode = reportOperateCode;
	}
	public String getReportOperateName() {
		return reportOperateName;
	}
	public void setReportOperateName(String reportOperateName) {
		this.reportOperateName = reportOperateName;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	
	
	
}
