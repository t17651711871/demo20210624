package com.geping.etl.common.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 * @author Mr.chen
 *	用户角色对照表
 *
 */
@Entity(name="Sys_Auth_Role_User")
@Table(name="SYS_AUTH_ROLE_USER")
public class Sys_Auth_Role_User {
	@Id
	private String id;
	private String roleId;  //用户编号
	private String userId;  //角色编号
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	
}
