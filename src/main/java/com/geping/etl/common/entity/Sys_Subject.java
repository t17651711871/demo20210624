package com.geping.etl.common.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 * @author Mr.chen
 *	项目信息表
 */
@Entity(name="Sys_Subject")
@Table(name="SYS_SUBJECT")
public class Sys_Subject implements Comparable<Sys_Subject>{
	@Id
	private String id;   //项目编号
	private String subjectName;//主题名称
	private String psubjectid;//父级项目编号
	private String icon;      //图标
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getPsubjectid() {
		return psubjectid;
	}
	public void setPsubjectid(String psubjectid) {
		this.psubjectid = psubjectid;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	@Override
	public int compareTo(Sys_Subject o) {
		int result = 0;
		if(Integer.parseInt(this.getId()) < Integer.parseInt(o.getId())) {
			result = -1;
		}
		
		else if(Integer.parseInt(this.getId()) > Integer.parseInt(o.getId())) {
			result = 1;
		}
		else if(Integer.parseInt(this.getId()) == Integer.parseInt(o.getId())) {
			result = 0;
		}
		return result;
	}
}
