package com.geping.etl.common.safe.channel;

/**
 * 通道service
 * @author  liang.xu
 * @date 2021年5月10日
 */
public interface DataChannelService {


    /**
     * 通道处理
     * @param source
     * @return
     */
    String   channel(String source);

    String source=null;


}