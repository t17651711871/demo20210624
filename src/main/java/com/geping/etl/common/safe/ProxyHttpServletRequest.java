package com.geping.etl.common.safe;

import com.geping.etl.common.safe.channel.DataChannelContainer;
import com.geping.etl.common.safe.channel.DataChannelService;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * 代理httpservlet
 * @author  liang.xu
 * @date 2021年5月8日
 */
public class ProxyHttpServletRequest extends HttpServletRequestWrapper {


    /**
     * 数据通道处理器
     */
    private DataChannelService dataChannelContainer=new DataChannelContainer();

    public ProxyHttpServletRequest(HttpServletRequest request) {
        super(request);
    }

    /**
     * 根据参数名获取数值
     * @param name
     * @return
     */
    @Override
    public String getParameter(String name) {
        String targetValue=this.getRequest().getParameter(name);
        if(StringUtils.isBlank(targetValue)){
            return targetValue;
        }
        return dataChannelContainer.channel(targetValue);
    }

    /**
     * 根据参数名获取数组
     * @param name
     * @return
     */
    @Override
    public String[] getParameterValues(String name) {
        String[]targetValues=this.getRequest().getParameterValues(name);
        if(targetValues==null||targetValues.length==0){
            return targetValues;
        }
        String []resultValues=targetValues;
        for(int i=0;i<targetValues.length;i++){
          String value=targetValues[i];
          resultValues[i]= dataChannelContainer.channel(value);
        }
        return resultValues;
    }
}