package com.geping.etl.common.safe.channel;

import com.geping.etl.common.safe.channel.impl.DdeDataValueChannelService;
import com.geping.etl.common.safe.channel.impl.XssDataValueChannelService;

import java.util.ArrayList;
import java.util.List;

/**
 * 通道容器
 * @author  liang.xu
 * @date 2021年5月10日
 */
public class DataChannelContainer  implements DataChannelService {

    private List<DataChannelService> dataChannelService=new ArrayList<>();

    public DataChannelContainer(){
        dataChannelService.add(new XssDataValueChannelService());
        dataChannelService.add(new DdeDataValueChannelService());
    }

    @Override
    public String channel(String source) {
        String _source=source;
        for(int i=0;i<dataChannelService.size();i++){
            _source=dataChannelService.get(i).channel(_source);
        }
        return _source;
    }
}