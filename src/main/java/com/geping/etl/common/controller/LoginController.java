package com.geping.etl.common.controller;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.common.entity.*;
import com.geping.etl.common.service.*;
import com.geping.etl.common.util.VariableUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class LoginController {
	
	@Autowired
	private Sys_UserService sys_UserService;
	@Autowired
	private Sys_UserAndOrgDepartmentService sod;
	@Autowired
	private LogsService ls;
	@Autowired
	private Sys_SubjectService sss;
	@Autowired
	private Sys_MenuService sms;
	@Autowired
	private Sys_Auth_Role_ResourceService sarrs;
	@Autowired
	private SUpOrgInfoSetService ss;
	
	private Sys_UserAndOrgDepartment sys_User;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
    //访问登录页面
	@RequestMapping(value="/loginUi",method=RequestMethod.GET)
	public ModelAndView loginUi(HttpServletRequest request){
		ModelAndView model = new ModelAndView();
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		if(sys_User != null) {
			String ip = "192.168.1.110";
			if(ip == null || sys_User.getIp() == null || sys_User.getIp().equals("")) {
				model.addObject("message","ip为空,请联系管理员");
				model.setViewName("common/login");
			}else if(!ip.equals(sys_User.getIp())){
				model.addObject("message","该用户登录ip地址与绑定ip地址不符");
				model.setViewName("common/login");
			}else {
				sys_UserService.updateLastLoginDate(sdf.format(new Date()),sys_User.getId());
				List<Sys_Subject> subjectList = sss.findSubjectByUserId(sys_User.getId());
				
				try {
					int days = (int)((new Date().getTime() - sdf.parse(sys_User.getLastmodifydate()).getTime()) / (1000*60*60*24));
					if(days >= 30 || sys_User.getIsfirstlogin().equals("Y")) {
						request.setAttribute("needEditPwd","Y");
					}else {
						request.setAttribute("needEditPwd","N");
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				request.getSession().setAttribute("sys_User",sys_User);
				request.setAttribute("sys_User",sys_User);
				model.addObject("subjectList", subjectList);
				model.setViewName("common/main");
		    }
		}else {
			model.setViewName("common/login");	
		}
		
		return model;
	}
	
	
	//处理登录请求
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public ModelAndView login(HttpServletRequest request,HttpServletResponse response){
		response.setContentType("text/html");
	    response.setContentType("text/plain; charset=utf-8");
	    ModelAndView model = new ModelAndView();
	    Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		if(sys_User != null) {
			String ip = "192.168.1.110";
			if(ip == null || sys_User.getIp() == null || sys_User.getIp().equals("")) {
				model.addObject("message","ip为空,请联系管理员");
				model.setViewName("common/login");
			}else if(!ip.equals(sys_User.getIp())){
				model.addObject("message","该用户登录ip地址与绑定ip地址不符");
				model.setViewName("common/login");
			}else {
				sys_UserService.updateLastLoginDate(sdf.format(new Date()),sys_User.getId());
				List<Sys_Subject> subjectList = sss.findSubjectByUserId(sys_User.getId());
				
				try {
					int days = (int)((new Date().getTime() - sdf.parse(sys_User.getLastmodifydate()).getTime()) / (1000*60*60*24));
					if(days >= 30 || sys_User.getIsfirstlogin().equals("Y")) {
						request.setAttribute("needEditPwd","Y");
					}else {
						request.setAttribute("needEditPwd","N");
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				request.getSession().setAttribute("sys_User",sys_User);
				request.setAttribute("sys_User",sys_User);
				model.addObject("subjectList", subjectList);
				model.setViewName("common/main");
		    }
		}else {
			String loginId = request.getParameter("loginId");
			//String password = SM3PrintUtil.getSM3Str(request.getParameter("password"));
			String password = request.getParameter("password").toUpperCase();
			sys_User = sod.findSysUserLogin(loginId, password);
			if(sys_User != null){
				//String ip = getIp(request);
				String ip = "192.168.1.110";
				if(ip == null || sys_User.getIp() == null || sys_User.getIp().equals("")) {
					model.addObject("message","ip为空,请联系管理员");
					model.setViewName("common/login");
				}else if(!ip.equals(sys_User.getIp())){
					model.addObject("message","该用户登录ip地址与绑定ip地址不符");
					model.setViewName("common/login");
				}else {
					sys_UserService.updateLastLoginDate(sdf.format(new Date()),sys_User.getId());
					List<Sys_Subject> subjectList = sss.findSubjectByUserId(sys_User.getId());
					Collections.sort(subjectList);
					
					//将登录信息加到日志中
					Logs logs = new Logs();
					logs.setId(String.valueOf(System.currentTimeMillis()));
					logs.setLoginId(sys_User.getLoginid());
					logs.setLogContent(sys_User.getLoginid()+"登录系统");
					logs.setLogDate(sdf.format(new Date()));
					logs.setLogOrgCode(sys_User.getOrgid());
					logs.setLogType("登录");
					ls.save(logs);   //添加日志信息
					
					try {
						int days = (int)((new Date().getTime() - sdf.parse(sys_User.getLastmodifydate()).getTime()) / (1000*60*60*24));
						if(days >= 30 || sys_User.getIsfirstlogin().equals("Y")) {
							request.setAttribute("needEditPwd","Y");
						}else {
							request.setAttribute("needEditPwd","N");
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
					
					request.getSession().setAttribute("sys_User",sys_User);
					request.setAttribute("sys_User",sys_User);
					model.addObject("subjectList", subjectList);
					model.setViewName("common/main");
				}
			}else{
				//将登录信息加到日志中
				Logs logs = new Logs();
				logs.setId(String.valueOf(System.currentTimeMillis()));
				logs.setLoginId(loginId);
				logs.setLogContent("登录账号或密码错误");
				logs.setLogDate(sdf.format(new Date()));
				logs.setLogOrgCode("");
				logs.setLogType("登录");
				ls.save(logs);   //添加日志信息
				model.addObject("message","登录账号或密码错误！");
				model.setViewName("common/login");
			}
		}
		return model;
	}
	
	
	//访问主页
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request,HttpServletResponse response) throws UnsupportedEncodingException{
		response.setContentType("text/html");
	    response.setContentType("text/plain; charset=utf-8");
		ModelAndView model = new ModelAndView();
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
	    String userId = request.getParameter("userId");
	    String subjectId = request.getParameter("subjectId");//系统编号
	    String subjectName = "";//系统名称
	    if(request.getParameter("subjectName") != null) {
	    	subjectName = URLDecoder.decode(request.getParameter("subjectName"),"UTF-8");
	    }
	    //通过用户id和系统id获取所有权限数据
	    List<Sys_Auth_Role_Resource> resourceList = sarrs.findResourceByRoleAndSubject(userId, subjectId);
	    Sys_Auth_Role_Resource resource = null;
	    TreeSet<Sys_Auth_Role_Resource> menuSet = new TreeSet<Sys_Auth_Role_Resource>();  //菜单集
	    TreeSet<Sys_Auth_Role_Resource> reportSet = new TreeSet<Sys_Auth_Role_Resource>();  //报表集
	    TreeSet<Sys_Auth_Role_Resource> operateReportSet = new TreeSet<Sys_Auth_Role_Resource>();  //操作按钮集
	    for(int i = 0;i<resourceList.size();i++) {
	    	resource = resourceList.get(i);
	    	if(resource.getResType().equals("Menu")) {
	    		menuSet.add(resource);
	    	}else if(resource.getResType().equals("Report")) {
	    		reportSet.add(resource);
	    	}else if(resource.getResType().equals("OperateReport")) {
	    		operateReportSet.add(resource);
	    	}else {
	    		
	    	}
	    }
	    
	    VariableUtils vu = new VariableUtils();
		vu.setReportSet(reportSet);
		vu.setOperateReportSet(operateReportSet);
		vu.setLoginId(sys_User.getLoginid());
		
		/**
		 * 针对企业征信和个人征信 
		 */
		if(subjectId.equals("4") || subjectId.equals("5")) {
			if(operateReportSet != null){
		          if(!operateReportSet.isEmpty()){
		        	  Iterator<Sys_Auth_Role_Resource> it = operateReportSet.iterator();
		        	  int makerNum = 0;
	        		  int checkerNum = 0;
		        	  while(it.hasNext()){
		        		  Sys_Auth_Role_Resource operatesource = it.next();
		        		  if(operatesource.getResType().equals("OperateReport") && (operatesource.getSubjectId().equals("4") || operatesource.getSubjectId().equals("5"))) {
		        			  if(operatesource.getResValue().equals("CHECK")) {
		        				  checkerNum = checkerNum + 1;
		  					  }
		        			  
		        			  if(operatesource.getResValue().equals("IMPORT") || operatesource.getResValue().equals("ADD") || operatesource.getResValue().equals("EDIT") || operatesource.getResValue().equals("CHECKOUT")) {
		        				  makerNum = makerNum + 1;
		        			  }
		        		  }
		    	     }
		        	  
		        	  if(checkerNum > 0 && makerNum == 0){
	                	  vu.setRole("checker");
	                  }
	                  if(checkerNum == 0 && makerNum > 0){
	                	  vu.setRole("maker");
	                  }
	                  if(checkerNum > 0 && makerNum > 0){
	                	  vu.setRole("makerAndChecker");
	                  }
	           }
		   }
		}

        /**
         * 百行征信
         */
        if(subjectId.equals("10")) {
            if(operateReportSet != null){
                if(!operateReportSet.isEmpty()){
                    Iterator<Sys_Auth_Role_Resource> it = operateReportSet.iterator();
                    int makerNum = 0;
                    int checkerNum = 0;
                    while(it.hasNext()){
                        Sys_Auth_Role_Resource operatesource = it.next();
                        if(operatesource.getResType().equals("OperateReport") && (operatesource.getSubjectId().equals("10"))) {
                            if(operatesource.getResValue().equals("CHECK")) {
                                checkerNum = checkerNum + 1;
                            }

                            if(operatesource.getResValue().equals("ADD") || operatesource.getResValue().equals("EDIT") || operatesource.getResValue().equals("APPLYEDIT") || operatesource.getResValue().equals("APPLYDELETE") || operatesource.getResValue().equals("APPLYREPORT") || operatesource.getResValue().equals("REPORT") || operatesource.getResValue().equals("SEEMESSAGE") || operatesource.getResValue().equals("SEERESULT")) {
                                makerNum = makerNum + 1;
                            }
                        }
                    }

                    if(checkerNum > 0 && makerNum == 0){
                        vu.setRole("checker");
                    }
                    if(checkerNum == 0 && makerNum > 0){
                        vu.setRole("maker");
                    }
                    if(checkerNum > 0 && makerNum > 0){
                        vu.setRole("makerAndChecker");
                    }
                }
            }
        }
		
		
		/**
		 * 如果是汽车金融east，赋一些值给全局变量
		 */
		if(subjectId.equals("8")) {
			vu.setLoginId(sys_User.getLoginid());
			vu.setOrgId(sys_User.getOrgid());
			vu.setOrgName(sys_User.getOrgname());
			vu.setOrgInsideCode(sys_User.getOrginsidecode());
			vu.setLicenseNumber(sys_User.getLicensenumber());
			
			if(operateReportSet != null){
		          if(!operateReportSet.isEmpty()){
		        	  Iterator<Sys_Auth_Role_Resource> it = operateReportSet.iterator();
		        	  int makerNum = 0;
	        		  int checkerNum = 0;
		        	  while(it.hasNext()){
		        		  Sys_Auth_Role_Resource operatesource = it.next();
		        		  if(operatesource.getResType().equals("OperateReport")) {
		        			  if(operatesource.getResValue().equals("ADD_REPORT")){
			                	  makerNum = makerNum + 1;
			                  }else if(operatesource.getResValue().equals("EDIT_REPORT")){
			                	  makerNum = makerNum + 1;
			                  }else if(operatesource.getResValue().equals("DELETE_REPORT")){
			                	  makerNum = makerNum + 1;
			                  }else if(operatesource.getResValue().equals("EXAMINE_REPORT")){
			                	  checkerNum = checkerNum + 1;
			                  }else if(operatesource.getResValue().equals("IMPORT_REPORT")){
			                	  makerNum = makerNum + 1;
			                  }else if(operatesource.getResValue().equals("GENERATE_MESSAGE")){
			                	  makerNum = makerNum + 1;
			                  }else if(operatesource.getResValue().equals("REPORT_SUBMIT")){
			                	  makerNum = makerNum + 1;
			                  }else{
			                	  
			                  }
		        		  }
		                  
		                  if(checkerNum > 0 && makerNum == 0){
		                	  vu.setRoleStyle("checker");
		                  }
		                  if(checkerNum == 0 && makerNum > 0){
		                	  vu.setRoleStyle("maker");
		                  }
		                  if(checkerNum > 0 && makerNum > 0){
		                	  vu.setRoleStyle("makerAndChecker");
		                  }
		    	  }
	           }
		   }
		}
	    
		
		/**
		 * 支付结算监管系统
		 */
		if(subjectId.equals("19") || subjectId.equals("25")) {
			if(operateReportSet != null){
		          if(!operateReportSet.isEmpty()){
		        	  Iterator<Sys_Auth_Role_Resource> it = operateReportSet.iterator();
		        	  int makerNum = 0;
	        		  int checkerNum = 0;
		        	  while(it.hasNext()){
		        		  Sys_Auth_Role_Resource operatesource = it.next();
		        		  if(operatesource.getResType().equals("OperateReport") &&
								  (operatesource.getSubjectId().equals("19") || operatesource.getSubjectId().equals("25"))) {
		        			  if(operatesource.getResValue().equals("CHECK")) {
		        				  checkerNum = checkerNum + 1;
		  					  }

		        			  if(operatesource.getResValue().equals("IMPORT") || operatesource.getResValue().equals("ADD") || operatesource.getResValue().equals("EDIT") || operatesource.getResValue().equals("CHECKOUT")) {
		        				  makerNum = makerNum + 1;
		        			  }
		        		  }
		    	     }

		        	  if(checkerNum > 0 && makerNum == 0){
	                	  vu.setRole("checker");
	                  }
	                  if(checkerNum == 0 && makerNum > 0){
	                	  vu.setRole("maker");//
	                  }
	                  if(checkerNum > 0 && makerNum > 0){
	                	  vu.setRole("makerAndChecker");
	                  }
	           }
		   }
			List<SUpOrgInfoSet> list = ss.findAll();	
			if(list.size() > 0) {
	            request.getSession().setAttribute("isusedelete", list.get(0).getIsusedelete());
	            request.getSession().setAttribute("isuseupdate", list.get(0).getIsuseupdate());
	            request.getSession().setAttribute("isusedepart", list.get(0).getIsusedepart());
	            request.getSession().setAttribute("isusedepartformessage", list.get(0).getBankcodefaren());
	            request.getSession().setAttribute("shifoushenheziji", list.get(0).getShifoushenheziji());
	        }else {
	        	request.getSession().setAttribute("isusedelete", "no");
	        	request.getSession().setAttribute("isuseupdate", "no");
	        	request.getSession().setAttribute("isusedepart", "no");
	        	request.getSession().setAttribute("isusedepartformessage", "no");
	        	request.getSession().setAttribute("shifoushenheziji","no");
	        }

			if(StringUtils.isBlank(vu.getRole())){
				vu.setRole("");
			}
		}
		
	    List<Sys_Menu> menuListAll = sms.getMenuBySubjectId(subjectId);
	    List<Sys_Menu> oneLevelMenu = new ArrayList<Sys_Menu>();//一级菜单
	    List<Sys_Menu> twoLevelMenu = new ArrayList<Sys_Menu>();//二级菜单
	    List<Sys_Menu> threeLevelMenu = new ArrayList<Sys_Menu>();//三级菜单
	    StringBuffer menuHtml = new StringBuffer();
	    for(Sys_Auth_Role_Resource sr : menuSet) {
	    	for(Sys_Menu sm : menuListAll) {
	    		if(sm.getId().equals(sr.getResId())) {
	    			/*if(sm.getPmenuid() == null || sm.getPmenuid().equals("")) {
			    		oneLevelMenu.add(sm);
			    	}else {
			    		twoLevelMenu.add(sm);
			    	}*/
	    			
	    			if(sm.getOrderNum().equals("0")) {
			    		oneLevelMenu.add(sm);
			    	}else if(Integer.parseInt(sm.getOrderNum()) > 0 && sm.getOrderNum().length() <= 2){
			    		twoLevelMenu.add(sm);
			    	}else if(sm.getOrderNum().length() == 3){
			    		threeLevelMenu.add(sm);
			    	}
	    			continue;
	    		}
	    	}
	    }
	    
	    for(int i = 0;i<oneLevelMenu.size();i++) {
	    	menuHtml.append("<div title=\""+oneLevelMenu.get(i).getName()+"\" style=\"overflow:auto;padding:10px;\">\r\n");
	    	menuHtml.append("<ul class=\"easyui-tree\">\r\n");
	    	for(int j = 0;j<twoLevelMenu.size();j++) {
	    		if(oneLevelMenu.get(i).getId().equals(twoLevelMenu.get(j).getPmenuid())) {
	    			if(Integer.parseInt(twoLevelMenu.get(j).getOrderNum()) > 0 && twoLevelMenu.get(j).getUrl().equals("#")) {   //判断是否有三级菜单，如果有
	    				menuHtml.append("<li data-options=\"state:'closed'\">\r\n");
	    		    	menuHtml.append("<span>"+twoLevelMenu.get(j).getName()+"</span>\r\n");
	    		    	menuHtml.append("<ul\r\n>");
	    		    	for(int k = 0;k<threeLevelMenu.size();k++) {
	    		    		if(twoLevelMenu.get(j).getId().equals(threeLevelMenu.get(k).getPmenuid())) {
	    		    			menuHtml.append("<li data-options=\"iconCls:'"+threeLevelMenu.get(k).getIcon()+"'\">\r\n");
	    		    			menuHtml.append("<a onclick=\"addTab('"+threeLevelMenu.get(k).getName()+"','"+threeLevelMenu.get(k).getUrl()+"')\"><span>"+threeLevelMenu.get(k).getName()+"</span></a>\r\n");
	    		    			menuHtml.append("</li>\r\n");
	    		    		}
	    		    	}
	    		    	menuHtml.append("</ul>\r\n");
	    		    	menuHtml.append("</li>\r\n");
	    			}else {
	    				menuHtml.append("<li data-options=\"iconCls:'"+twoLevelMenu.get(j).getIcon()+"'\">\r\n");
		    			menuHtml.append("<a onclick=\"addTab('"+twoLevelMenu.get(j).getName()+"','"+twoLevelMenu.get(j).getUrl()+"')\"><span>"+twoLevelMenu.get(j).getName()+"</span></a>\r\n");
		    			menuHtml.append("</li>\r\n");
	    			}
	    		}
	    	}
	    	menuHtml.append("</ul>\r\n");
	    	menuHtml.append("</div>\r\n");
	    }
	    
	    request.getSession().setAttribute("vu",vu);
	    model.addObject("menuHtml",menuHtml.toString());
	    model.addObject("subjectName",subjectName);
	    request.setAttribute("subjectId",subjectId);
		model.setViewName("common/index");
	    
	    if(resourceList != null) {
	    	resourceList.clear();
	    	resourceList = null;
	    }
	    
	    if(menuSet != null) {
	    	menuSet.clear();
	    	menuSet = null;
	    }
	    
	   /* if(reportSet != null) {
	    	reportSet.clear();
	    	reportSet = null;
	    }
	    
	    if(operateReportSet != null) {
	    	operateReportSet.clear();
	    	operateReportSet = null;
	    }*/
	    
	    if(menuListAll != null) {
	    	menuListAll.clear();
	    	menuListAll = null;
	    }
	    
	    if(oneLevelMenu != null) {
	    	oneLevelMenu.clear();
	    	oneLevelMenu = null;
	    }
	    
	    if(twoLevelMenu != null) {
	    	twoLevelMenu.clear();
	    	twoLevelMenu = null;
	    }
	    
		return model;
	}
	
	
	//访问首页
	@RequestMapping(value="/home",method=RequestMethod.GET)
	public ModelAndView home(){
		ModelAndView model = new ModelAndView();
		model.setViewName("common/home");
		return model;
	}
	
	
	//安全退出
	@RequestMapping(value="/userOut",method=RequestMethod.GET)
	public ModelAndView userOut(HttpServletRequest request){
		ModelAndView model = new ModelAndView();
		request.getSession().removeAttribute("sys_User");  //将用户信息从session中删除
		model.setViewName("common/login");
		return model;
	}
	
	//获取ip地址
	public String getIp(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if(null == ip || 0 == ip.length() || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("Proxy-Client-IP");
	    }
		
		if(null == ip || 0 == ip.length() || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("WL-Proxy-Client-IP");
	    }
		
		if(null == ip || 0 == ip.length() || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("X-Real-IP");
	    }
		
		if(null == ip || 0 == ip.length() || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getRemoteAddr();
	    }
		return ip;
	}
	
}
