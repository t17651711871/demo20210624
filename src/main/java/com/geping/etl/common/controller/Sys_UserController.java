package com.geping.etl.common.controller;

import com.geping.etl.common.entity.*;
import com.geping.etl.common.service.*;
import com.geping.etl.utils.sm3.SM3PrintUtil;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class Sys_UserController {

	@Autowired
	private Sys_UserService sys_UserService;

	@Autowired
	private Sys_DepartmentService sys_DepartmentService;
	
	@Autowired
	private LogsService ls;
	
	@Autowired
	private Sys_UserAndOrgDepartmentService sys_UserAndOrgDepartmentService;

	@Autowired
	private Sys_OrgService sys_OrgService;


	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	//查询所有用户信息
	@RequestMapping(value="/sys_user",method=RequestMethod.GET)
	public ModelAndView getAllSys_user(){
		ModelAndView model = new ModelAndView();

		List<Sys_Org> orgList = sys_OrgService.findAllSysOrg();     //查询所有机构
		List<Sys_Department> departmentList = sys_DepartmentService.findAllSys_Department();   //查询所有部门
		List<Sys_UserAndOrgDepartment> list = sys_UserAndOrgDepartmentService.findAllSysUser();   //查询所有用户(多表联查)
		model.addObject("list", list);
		model.addObject("orgList", orgList);
		model.addObject("departmentList",departmentList);
		model.setViewName("common/sys_user");
		return model;
	}

	//根据用户名模糊查询用户信息
	@RequestMapping(value="/getSys_user",method=RequestMethod.GET)
	public ModelAndView getSys_user(HttpServletRequest request){
		ModelAndView model = new ModelAndView();
		String userCname = "";
		try {
			if(request.getParameter("userCname") != null) {
				userCname = URLDecoder.decode(request.getParameter("userCname"),"UTF-8");
			}
			List<Sys_UserAndOrgDepartment> list = sys_UserAndOrgDepartmentService.findSysUserByName("%"+userCname+"%");
			List<Sys_Org> orgList = sys_OrgService.findAllSysOrg();     //查询所有机构
			List<Sys_Department> departmentList = sys_DepartmentService.findAllSys_Department();   //查询所有部门
			model.addObject("list",list);
			model.addObject("orgList", orgList);
			model.addObject("departmentList",departmentList);
			model.addObject("userCname",userCname);
			model.setViewName("common/sys_user");
		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return model;
	}


	//新增用户
	@RequestMapping(value="/addSys_User",method=RequestMethod.POST)
	public void addSys_User(HttpServletRequest request,HttpServletResponse response){
		String loginId_add = request.getParameter("loginId_add");
		String userEname_add = request.getParameter("userEname_add"); 
		String userCname_add = request.getParameter("userCname_add"); 
		String orgId_add = request.getParameter("orgId_add");
		String departId_add = request.getParameter("departId_add");
		String tel_add = request.getParameter("tel_add");
		String mobile_add = request.getParameter("mobile_add"); 
		String address_add = request.getParameter("address_add"); 
		String email_add = request.getParameter("email_add"); 
		String ip_add = "192.168.1.110";


		Sys_User sysIsRepeat = sys_UserService.getOneByLoginId(loginId_add);
		PrintWriter out;
		try {
			out = response.getWriter();
			if(sysIsRepeat != null){
				out.write("2");
				out.flush();
				out.close();
			}else{
				Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Sys_User sys = new Sys_User();
				sys.setId(String.valueOf(System.currentTimeMillis()));
				sys.setLoginId(loginId_add);
				sys.setUserEname(userEname_add);
				sys.setUserCname(userCname_add);
				sys.setPassword(SM3PrintUtil.getSM3Str("12345678"));
				sys.setOrgId(orgId_add);
				sys.setDepartId(departId_add);
				sys.setTel(tel_add);
				sys.setMobile(mobile_add);
				sys.setAddress(address_add);
				sys.setEmail(email_add);
				sys.setCreateTime(sdf.format(new Date()));
				sys.setLastModifyDate(sdf.format(new Date()));
				sys.setIp(ip_add);
				sys.setHandleperson(sys_User.getLoginid());
				sys.setHandledate(sdf.format(new Date()));
				sys.setLastModifyDate(sdf.format(new Date()));
				sys.setIsLocked("N");
				Sys_User user = sys_UserService.save(sys);
				try {
					out = response.getWriter();
					if(user != null){
						out.write("1");
					}else{
						out.write("0");
					}
					out.flush();
					out.close();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}


	//修改用户(不包括修改密码)
	@RequestMapping(value="/editSys_User",method=RequestMethod.POST)
	public void editSys_User(HttpServletRequest request,HttpServletResponse response){
		String id_edit = request.getParameter("editId");
		String loginId_edit = request.getParameter("loginId_edit");
		String userEname_edit = request.getParameter("userEname_edit");
		String userCname_edit = request.getParameter("userCname_edit"); 
		String orgId_edit = request.getParameter("orgId_edit"); 
		String departId_edit = request.getParameter("departId_edit"); 
		String tel_edit = request.getParameter("tel_edit");
		String mobile_edit = request.getParameter("mobile_edit");
		String address_edit = request.getParameter("address_edit");
		String email_edit = request.getParameter("email_edit");
		String ip_edit = "192.168.1.110";
		String isEditLoginId = request.getParameter("isEditLoginId");

		PrintWriter out;
		try {
			out = response.getWriter();
			if(isEditLoginId.equals("1")){  //用户修改了登录账号
				Sys_User sysIsRepeat = sys_UserService.getOneByLoginId(loginId_edit);
				if(sysIsRepeat != null){  //如果返回对象不为空，即登录账号已存在 
					out.write("2");   //2表示登录账号重复
					out.flush();
					out.close();
				}else{   //如果返回对象为空，即登录账号不存在
					int result = sys_UserService.updateSysUser(id_edit,loginId_edit,userEname_edit,userCname_edit,orgId_edit,departId_edit,tel_edit,mobile_edit,address_edit,email_edit,ip_edit);
					if(result > 0){
						out.write("1");
					}else{
						out.write("0");
					}
				}
			}else if(isEditLoginId.equals("0")){  //用户未修改登录账号
				int result = sys_UserService.updateSysUser(id_edit,loginId_edit,userEname_edit,userCname_edit,orgId_edit,departId_edit,tel_edit,mobile_edit,address_edit,email_edit,ip_edit);
				if(result > 0){
					out.write("1");
				}else{
					out.write("0");
				}
			}else{
				out.write("0");
			}
			out.flush();
			out.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	
	
	//申请修改
	@RequestMapping(value="/applyEdit",method=RequestMethod.POST)
	public void applyEdit(HttpServletRequest request,HttpServletResponse response){
			PrintWriter out;
			try {
				request.setCharacterEncoding("utf-8");
				out = response.getWriter();
				String id = request.getParameter("id");
				String desc = request.getParameter("desc");
				Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				int result = sys_UserService.applyEdit(id, desc,"申请修改",sys_User.getLoginid(),sdf.format(new Date()));
				if(result > 0) {
					out.write("1");
				}else {
					out.write("0");
				}
				out.flush();
				out.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	}
	
	
	//申请重置密码
	@RequestMapping(value="/applyReaptPwd",method=RequestMethod.GET)
	public void applyReaptPwd(HttpServletRequest request,HttpServletResponse response){
			PrintWriter out;
			try {
				request.setCharacterEncoding("utf-8");
				out = response.getWriter();
				String id = request.getParameter("id");
				String pwd = SM3PrintUtil.getSM3Str(request.getParameter("pwd"));
				Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				int result = sys_UserService.applyReaptPwd(id, pwd,"",sys_User.getLoginid(),sdf.format(new Date()));
				if(result > 0) {
					out.write("1");
				}else {
					out.write("0");
				}
				out.flush();
				out.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	}
	
	
	//修改用户密码
	@RequestMapping(value="/editPasswordSys_User",method=RequestMethod.GET)
	public void editPasswordSys_User(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		try {
			out = response.getWriter();
			//String editPasswordId = request.getParameter("editPasswordId");
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User");
			String editPasswordId=sys_User.getId();
			String password_edit = SM3PrintUtil.getSM3Str(request.getParameter("password_edit"));
			String isFirstLogin = sys_User.getIsfirstlogin();
			/*if(request.getParameter("isFirstLogin") != null){
				isFirstLogin = request.getParameter("isFirstLogin");
			}*/
			Sys_User user = sys_UserService.findUserById(editPasswordId);
			String str = null;
			boolean isrepeat = false;
			int result = 0;
			if(user != null) {
				 str = user.getPwdstr();
				 if(str == null || str.equals("")) {
					 str = password_edit;
				 }else {
					 String [] pwdArray = str.split(",");
					 for(int i = 0;i<pwdArray.length;i++) {
						 if(password_edit.equals(pwdArray[i])) {
							 isrepeat = true;
							 break;
						 }
					 }
					 str = str + "," + password_edit;
				 }
			}
			
			if(isrepeat) {
				out.write("2");
			}else {
				result = sys_UserService.editPasswordSys_User(editPasswordId,password_edit,sdf.format(new Date()),str);
				if(isFirstLogin.equals("Y")){  //首次登录
					sys_UserService.updateSomeSysUser(sdf.format(new Date()),"N",editPasswordId);
				}
				if(result > 0){
					out.write("1");
				}else{
					out.write("0");
				}
			}
		}catch (IOException e) {
			e.printStackTrace();
		}finally {
			if(out != null) {
				out.flush();
				out.close();
			}
		}
	}

	
	//申请解锁用户
	@RequestMapping(value="/applyNoLock",method=RequestMethod.POST)
	public void applyNoLock(HttpServletRequest request,HttpServletResponse response){
			PrintWriter out;
			try {
				request.setCharacterEncoding("utf-8");
				out = response.getWriter();
				String id = request.getParameter("id");
				Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				int result = sys_UserService.applyNoLock(id,"",sys_User.getLoginid(),sdf.format(new Date()));
				if(result > 0) {
					out.write("1");
				}else {
					out.write("0");
				}
				out.flush();
				out.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	}
	
	
	//申请锁定用户
	@RequestMapping(value="/applyDoLock",method=RequestMethod.POST)
	public void applyDoLock(HttpServletRequest request,HttpServletResponse response){
				PrintWriter out;
				try {
					request.setCharacterEncoding("utf-8");
					out = response.getWriter();
					String id = request.getParameter("id");
					String reason = request.getParameter("reason");
					Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User");
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					int result = sys_UserService.applyDoLock(id,"",sys_User.getLoginid(),sdf.format(new Date()),reason);
					if(result > 0) {
						out.write("1");
					}else {
						out.write("0");
					}
					out.flush();
					out.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
	}
	

	//修改用户是否锁定
	@RequestMapping(value="/editIsLockedSys_User",method=RequestMethod.POST)
	public void editIsLockedSys_User(HttpServletRequest request,HttpServletResponse response){
		String id = request.getParameter("id");
		String isLocked = request.getParameter("isLocked");
		int result = 0;
		JSONObject permision = new JSONObject();
		PrintWriter out;
		try {
			request.setCharacterEncoding("UTF-8");
			out = response.getWriter();
			if(isLocked.equals("Y")){
				String reason = request.getParameter("reason");
				result = sys_UserService.lockedUser(id, isLocked, reason);
				permision.put("result",result);
				out.print(permision.toString());
			}else if(isLocked.equals("N")){
				result = sys_UserService.editIsLockedSys_User(id, isLocked);
				permision.put("result",result);
				out.print(permision.toString());
			}else{
				permision.put("result",result);
				out.print(permision.toString());
			}
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	//逻辑删除用户
	@RequestMapping(value="/deleteSys_User",method=RequestMethod.POST)
	public void deleteSys_User(HttpServletRequest request,HttpServletResponse response){
		String id = request.getParameter("id");
		int result = 0;
		result = sys_UserService.deleteSys_User(id);
		JSONObject permision = new JSONObject();
		PrintWriter out;
		try {
			out = response.getWriter();
			permision.put("result",result);
			out.print(permision.toString());
			out.flush();
			out.close();
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	
	
	//同意申请
	@RequestMapping(value="/agreenApply",method=RequestMethod.POST)
	public void agreenApply(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		try {
			out = response.getWriter();
			request.setCharacterEncoding("utf-8");
			String id = request.getParameter("id");
			String applyname = request.getParameter("applyname");
			int result = 0;
			if(applyname.equals("申请新增")){
				result = sys_UserService.agreenAddOrEdit(id," ");
			}else if(applyname.equals("申请修改")){
				result = sys_UserService.agreenAddOrEdit(id,"同意修改");
			}else if(applyname.equals("申请重置密码")){
				result = sys_UserService.agreenReapt(id);
			}else if(applyname.equals("申请锁定")){
				result = sys_UserService.agreenLock(id,"Y");
			}else if(applyname.equals("申请解锁")){
				result = sys_UserService.agreenNoLock(id,"N");
			}else{
				
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	
	//拒绝申请
	@RequestMapping(value="/refuseApply",method=RequestMethod.POST)
	public void refuseApply(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		try {
			out = response.getWriter();
			request.setCharacterEncoding("utf-8");
			String id = request.getParameter("id");
			String applyname = request.getParameter("applyname");
			int result = 0;
			if(applyname.equals("申请新增")){
				result = sys_UserService.refuseAddOrEdit(id);
			}else if(applyname.equals("申请修改")){
				result = sys_UserService.refuseAddOrEdit(id);
			}else if(applyname.equals("申请重置密码")){
				result = sys_UserService.refuseSomeHandle(id);
			}else if(applyname.equals("申请锁定")){
				result = sys_UserService.refuseSomeHandle(id);
			}else if(applyname.equals("申请解锁")){
				result = sys_UserService.refuseSomeHandle(id);
			}else{
				
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	
	//Excel导出用户信息
	/*@RequestMapping(value="/exportExcelSys_User",method=RequestMethod.GET)
	public void exportExcelSys_User(HttpServletRequest request,HttpServletResponse response){
		//headers:excel中表列标题名
		String[] headers ={"ID","登录账号","用户英文名","用户中文名","密码","机构编号","机构名称","部门ID","部门名称","联系电话","移动电话","地址","邮箱","是否锁定用户","锁定原因","开始时间","结束时间","创建时间","描述","启用标识","是否删除","最后登录时间","最后修改密码日期","是否为首次登录"};  
		List<Sys_UserAndOrgDepartment> list = sys_UserAndOrgDepartmentService.findAllSysUser();   //查询所有用户(多表联查)
		ExcelExportUtils<Sys_UserAndOrgDepartment> eeu = new ExcelExportUtils<Sys_UserAndOrgDepartment>();
		String fileName = "人员信息记录.xls";   //设置下载的文件名称
		String title = "人员信息记录";          //Excel表格sheet名称
		eeu.exportExcel(request,response,fileName,title,headers,list);
	}*/

	//Excel导入用户信息
	@RequestMapping(value="/importExcelSys_User",method=RequestMethod.POST)
	public String importExcelSys_User(HttpServletRequest request,HttpServletResponse response,@RequestParam("fileToUpload") MultipartFile file) throws IOException{
		//判断文件是否是以规定的格式结尾的（excel格式）
		if(file.getSize()>1048576){
			return "上传的文件大小不能超过2MB,请重新上传！";
		
		}
		if(!file.getOriginalFilename().endsWith(".xls")){
			
			return "请上传正确的文件格式！";
		}else {
			InputStream inputStream=file.getInputStream();
			//记录总的成功数
			int successNum=0;
			int linknum=0;
			//记录用户的操作信息
			String recording="";
			//格式化文件为正确的文件格式
					HSSFWorkbook workbook=null; 
					try {
						//创建一个工作薄
						workbook=new HSSFWorkbook(inputStream);
						//创建一张工作表
						HSSFSheet sheet=workbook.getSheetAt(0);
						//获取到文件的最后一行
						int lastRowNum=sheet.getLastRowNum();
						String[] firstCellName={"ID","登录账号","用户英文名","用户中文名","密码","机构编号","机构名称","部门ID","部门名称","联系电话","移动电话","地址","邮箱","是否锁定用户","锁定原因","开始时间","结束时间","创建时间","描述","启用标识","是否删除","最后登录时间","最后修改密码日期","是否为首次登录"};
						//判断用户是否上传的是一个空文件
						if(lastRowNum==0){
							return recording="文件导入模板有误，请在页面点击导出下载正确的模板!";
						}
						for (int i = 0; i<=lastRowNum; i++) {
							//判断是否为空行
							HSSFRow hssfrow=sheet.getRow(i);
							if(hssfrow==null)
							{
								continue;
							}
							HSSFRow row =sheet.getRow(i);
							//判断文件是否是合格的模板在进行导入
							if (i==0) {
								if(row.getLastCellNum()==firstCellName.length){
									for (int j = 0; j <row.getLastCellNum(); j++) {
										if(!firstCellName[j].equals(row.getCell(j)+"")){
											return recording="文件导入模板有误，请在页面点击导出下载正确的模板!";
										}
									}
									continue;
								}else {
									
									return recording="文件导入模板有误，请在页面点击导出下载正确的模板!";
								}
							}
							
							String user3=sys_UserService.findLoginIdSys_User(String.valueOf(row.getCell(1)));
							//判读用户是否已经存在数据库之中，如果没有则执行下一步操作，不予许用户名相同的用户出现
							if(user3==null){
								//将excel表中的数据写入到对象中
								Sys_User user=new Sys_User();
								HSSFCell cell=row.getCell(1);
								//使用正则表达式对用户输入的信息进行校验

								//判断用户名
								String regex = "^([a-zA-Z][0-9]){1,10}$|^([\\u4e00-\\u9fa5]){1,10}$"; 
								Matcher m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(1)).trim()); 
								if(m.find() ){
									cell=row.getCell(1);
									user.setLoginId(String.valueOf(cell));
								}else {
									
									linknum++;
									recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因用户名为数字英文组合或中文!"+"</br>";
									continue;
								}

								//判断用户英文名
								regex ="^[a-zA-Z]{1,10}"; 
								m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(2)).trim());
								if(m.find()){
									cell=row.getCell(2);
									user.setUserEname(String.valueOf(cell));
								}else {
									linknum++;
									recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因英文名不能为空，长度不能超过10位!"+"</br>";
									continue;
								}


								//判断用户中文名
								regex ="^[\\u4e00-\\u9fa5]{1,10}$"; 
								m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(3)).trim());
								if(m.find()){
									cell=row.getCell(3);
									user.setUserCname(String.valueOf(cell));
								}else {
									linknum++;
									recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因中文名不能为空,长度不能超过10位!"+"</br>";
									continue;
								}


								//判断用户密码
								regex = "^[a-zA-Z]\\w{5,10}"; 
								m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(4)).trim());
								if(m.find()){
									cell=row.getCell(4);
									user.setPassword(String.valueOf(cell));
								}else {
									linknum++;
									recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因密码不能为空，长度在5到10位之间!"+"</br>";
									continue;
								}

								//判断机构编号
								regex = "[0-9]{0,10}"; 
								m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(5)).trim());
								if(m.find()){
									cell=row.getCell(5);
									user.setOrgId(String.valueOf(cell));
								}else {
									linknum++;
									recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因机构编号必须由数字组成，且长度不超过10位!"+"</br>";
									continue;
								}

								//判断部门编号
								regex = "[0-9]{0,10}"; 
								m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(7)).trim());
								if(m.find()){
									cell=row.getCell(7);
									user.setDepartId(String.valueOf(cell));
								}else {
									linknum++;
									recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因部门编号必须由数字组成，且长度不超过10位!"+"</br>";
									continue;
								}

								//判断联系电话
								if((!String.valueOf(row.getCell(9)).trim().isEmpty())){
									user.setTel("");
								}else{
									regex = "^((13\\d{9}$)|(15[0,1,2,3,5,6,7,8,9]\\d{8}$)|(18[0,2,5,6,7,8,9]\\d{8}$)|(147\\d{8})$)"; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(9)).trim());
									if(m.find()){

										cell=row.getCell(9);
										user.setTel(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因电话号码格式有误!"+"</br>";
										continue;
									}
								}

								//判断移动电话
								if((!String.valueOf(row.getCell(10)).trim().isEmpty())){
									user.setMobile("");
								}else{
									regex = "^((13\\d{9}$)|(15[0,1,2,3,5,6,7,8,9]\\d{8}$)|(18[0,2,5,6,7,8,9]\\d{8}$)|(147\\d{8})$)"; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(10)).trim());
									if(m.find()){

										cell=row.getCell(10);
										user.setMobile(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因移动电话号码格式有误!"+"</br>";
										continue;
									}
								}

								//判断地址
								if((!String.valueOf(row.getCell(11)).trim().isEmpty())){
									user.setAddress(null);
								}else{
									cell=row.getCell(11);
									user.setAddress(String.valueOf(cell));
									
								}
								
								//判断邮箱
								if((!String.valueOf(row.getCell(12)).trim().isEmpty())){
									user.setEmail(null);
								}else{
									regex = "^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+"; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(12)).trim());
									if(m.find()){
										cell=row.getCell(12);
										user.setEmail(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因邮箱格式有误!"+"</br>";
										continue;
									}
								}

								//判断是否锁定
								if((!String.valueOf(row.getCell(13)).trim().isEmpty())){
									user.setIsLocked("N");
								}else{
									regex = "^Y$|^N$"; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(13)).trim());
									if(m.find()){
										cell=row.getCell(13);
										user.setIsLocked(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因只能选择Y或N!"+"</br>";
										continue;
									}
								}
								
								//判断锁定原因
								if((!String.valueOf(row.getCell(14)).trim().isEmpty())){
									user.setUserLockedReson(null);
								}else{
									regex = "^([a-zA-z][\\u4e00-\\u9fa5]){0,25}"; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(14)).trim());
									if(m.find()){
										cell=row.getCell(14);
										user.setUserLockedReson(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因长度超出范围!"+"</br>";
										continue;
									}
								}
								
								
								//判断开始时间
								if((!String.valueOf(row.getCell(15)).trim().isEmpty())){
									user.setStartDate(null);
								}else{
									regex = ""; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(15)).trim());
									if(m.find()){
										cell=row.getCell(15);
										user.setStartDate(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因开始时间格式有误!"+"</br>";
										continue;
									}
								}
								
								//判读结束时间
								if((!String.valueOf(row.getCell(16)).trim().isEmpty())){
									user.setEndDate(null);
								}else{
									regex = ""; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(16)).trim());
									if(m.find()){
										cell=row.getCell(16);
										user.setEndDate(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因结束时间格式有误!"+"</br>";
										continue;
									}
								}
								
								//判断创建时间
								if((!String.valueOf(row.getCell(17)).trim().isEmpty())){
									user.setCreateTime(null);
								}else{
									regex = ""; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(17)).trim());
									if(m.find()){
										cell=row.getCell(17);
										user.setCreateTime(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因创建时间格式有误!"+"</br>";
										continue;
									}
								}
								
								//判断描述
								if((!String.valueOf(row.getCell(18)).trim().isEmpty())){
									user.setDescription(null);
								}else{
									regex = ""; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(18)).trim());
									if(m.find()){
										cell=row.getCell(18);
										user.setDescription(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因添加描述失败!"+"</br>";
										continue;
									}
								}
								
								
								//判断启用标识
								if((!String.valueOf(row.getCell(19)).trim().isEmpty())){
									user.setEnabled(null);
								}else{
									regex = ""; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(19)).trim());
									if(m.find()){
										cell=row.getCell(19);
										user.setEnabled(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因启用标识有误!"+"</br>";
										continue;
									}
								}
								
								//判断是否删除
								if((!String.valueOf(row.getCell(20)).trim().isEmpty())){
									user.setIsDelete("N");
								}else{
									regex = ""; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(20)).trim());
									if(m.find()){
										cell=row.getCell(20);
										user.setIsDelete(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因添加是否删除时失败!"+"</br>";
										continue;
									}
								}
							
								
								//判读最后登录时间
								if((!String.valueOf(row.getCell(21)).trim().isEmpty())){
									user.setLastLoginDate(null);
								}else{
									regex = ""; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(21)).trim());
									if(m.find()){
										cell=row.getCell(21);
										user.setLastLoginDate(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"添加用户名为“"+String.valueOf(cell)+"”时失败原因最后登录时间格式有误!"+"</br>";
										continue;
									}
								}
								
								//判断最后修改密码日期
								if((!String.valueOf(row.getCell(22)).trim().isEmpty())){
									user.setLastModifyDate(null);
								}else{
									regex = ""; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(22)).trim());
									if(m.find()){
										cell=row.getCell(22);
										user.setLastModifyDate(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败原因最后修改密码日期格式有误!"+"</br>";
										continue;
									}
								}
								
								//判断是否首次登录
								if((!String.valueOf(row.getCell(23)).trim().isEmpty())){
									user.setIsFirstLogin(null);
								}else{
									regex = ""; 
									m = Pattern.compile(regex).matcher(String.valueOf(row.getCell(23)).trim());
									if(m.find()){
										cell=row.getCell(23);
										user.setIsFirstLogin(String.valueOf(cell));
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败请检查首次登录字段!"+"</br>";
										continue;
									}
								}
								
								
								
									//如果上述条件都满足，那么进行数据的添加
									if(sys_UserService.save(user)!=null){
										successNum++;
									}else {
										linknum++;
										recording+=linknum+"、添加用户名为“"+String.valueOf(cell)+"”时失败遇到未知错误!";
									}
							}else{
								if(String.valueOf(user3).isEmpty()){
									
									recording+="添加第："+row+"条数据时失败用户名不能为空!"+"</br>";
								}else {
									linknum++;
									recording+=linknum+"、添加用户名为“"+String.valueOf(user3)+"”时失败用户已经存在!"+"</br>";
								}
							}
							
						}
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}finally{
						//关闭流
						try {
							//workbook.close();
							inputStream.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
			//日志操作
			 Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User");
			 Logs logs = new Logs();
			 logs.setLoginId(sys_User.getLoginid());
			 logs.setLogContent("进行excel导入用户操作");
			 logs.setLogDate(sdf.format(new Date()));
			 logs.setLogOrgCode(sys_User.getOrgid());
			 logs.setLogType("进行用户导入操作");
			 ls.save(logs);   //添加日志信息
			return recording+"成功"+successNum+"条";
		}

	}

}
