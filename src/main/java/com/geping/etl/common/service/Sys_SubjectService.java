package com.geping.etl.common.service;

import java.util.List;
import com.geping.etl.common.entity.Sys_Subject;

public interface Sys_SubjectService {

	public List<Sys_Subject> findAllSubject();
	
	public List<Sys_Subject> findSubjectByUserId(String userId);
}
