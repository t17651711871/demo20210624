package com.geping.etl.common.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.common.entity.Sys_Department;
import com.geping.etl.common.repository.Sys_DepartmentRepository;
import com.geping.etl.common.service.Sys_DepartmentService;

@Service
@Transactional
public class Sys_DepartmentServiceImpl implements Sys_DepartmentService {

	@Autowired
	private Sys_DepartmentRepository sys_DepartmentRepository;

	@Override
	public List<Sys_Department> findAllSys_Department() {
		
		return sys_DepartmentRepository.findAllSysDepartment();
	}
	
	
	//根据用户名模糊查询部门信息
	public List<Sys_Department> getSys_DepartmentByLikeDepartmentName(String departmentName){
		
		return sys_DepartmentRepository.getSys_DepartmentByLikeDepartmentName(departmentName);
	}
	
	
	//插入
	public Sys_Department save(Sys_Department sys_Department){
		
		return sys_DepartmentRepository.save(sys_Department);
	}
	
	
	//获取最大ID数
	public String findMaxId(){
		
		return sys_DepartmentRepository.findMaxId();
	}
	
	
	//修改部门信息
    public int updateSys_Department(String id_edit,String departmentName,String description){
    	
    	return sys_DepartmentRepository.updateSys_Department(id_edit, departmentName, description);
    }
    
    
  //逻辑删除部门
    public int deleteSys_Department(String id){
    	
    	return sys_DepartmentRepository.deleteSys_Department(id);
    }
}
