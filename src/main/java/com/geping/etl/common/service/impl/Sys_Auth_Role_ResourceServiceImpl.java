package com.geping.etl.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.common.entity.Sys_Auth_Role_Resource;
import com.geping.etl.common.repository.Sys_Auth_Role_ResourceRepository;
import com.geping.etl.common.service.Sys_Auth_Role_ResourceService;

@Service
@Transactional
public class Sys_Auth_Role_ResourceServiceImpl implements Sys_Auth_Role_ResourceService{
	
	@Autowired
	private Sys_Auth_Role_ResourceRepository sysauthroleresourcerepository;
	
	//点击角色分配中的保存按钮
	@Override
	public Sys_Auth_Role_Resource save(Sys_Auth_Role_Resource sysauthroleresource) {
		
		return sysauthroleresourcerepository.save(sysauthroleresource);
	}

	@Override
	public Integer deleteSysAuthRoleResource(String id) {
		
		return sysauthroleresourcerepository.deleteSysAuthRoleResource(id);
	}

	@Override
	public List<Sys_Auth_Role_Resource> findAllByRoleId(String roleId) {
		
		return sysauthroleresourcerepository.findAllByRoleId(roleId);
	}
	
	
	//根据用户编号和子系统编号查询出所有的权限
	@Override
	public List<Sys_Auth_Role_Resource> findResourceByRoleAndSubject(String userId,String subjectId){
		
		return sysauthroleresourcerepository.findResourceByRoleAndSubject(userId, subjectId);
	}
}
