package com.geping.etl.common.service;

import java.util.List;

import com.geping.etl.common.entity.Sys_Org;

public interface Sys_OrgService {
	
	public Iterable<Sys_Org> findAll();
	
	//查询所有未删除的机构
	public List<Sys_Org> findAllSysOrg();

	
	//根据机构名称模糊查询机构信息
	public List<Sys_Org> getSys_UserByLikeOrgName(String orgName);

	
	//插入
	public Sys_Org save(Sys_Org sys_Org);
	
	
	//根据机构名称和机构编号是否存在同名的机构信息
	public List<Sys_Org> getOrgNameAndOrgId(String orgName,String orgId);
	
	//修改机构信息
	public int updateSys_Org(String id,String orgId,String orgName,String orgSortName,String orgParentId,String orgRegion,String isBussiness,String isHead,String orgLevel,String orgInsideCode,String licenseNumber,String option_,String tel);
		
	
	//逻辑删除机构
	public int deleteSys_Org(String id,String enabled);
	
	//根据orgId查询条数
	public Integer selectCountById(String refNo);
	
	//根据 机构代码 查机构
	public List<Sys_Org> selectOrgById(String orgId);
}
