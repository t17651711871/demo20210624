package com.geping.etl.common.service.impl;

import com.geping.etl.common.entity.Report_Info;
import com.geping.etl.common.repository.Report_InfoRepository;
import com.geping.etl.common.service.Report_InfoService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class Report_InfoServiceImpl implements Report_InfoService{
	@Autowired
	private Report_InfoRepository reportinfopository;
	
	//显示所有报表权限
	@Override
	public List<Report_Info> findAll() {
		
		return reportinfopository.findAll();
	}

	@Override
	public List<Report_Info> findAllBySome(Specification<Report_Info> spec) {
		
		return reportinfopository.findAll(spec);
	}

	@Override
	public void updateReportInfoPending(Report_Info ri) {
		
		reportinfopository.updateReportInfoPending(ri);
	}

	@Override
	public void updateReportInfoPendingAudit(Report_Info ri) {
		
		reportinfopository.updateReportInfoPendingAudit(ri);
	}

	@Override
	public void updateReportInfoCheckFail(Report_Info ri) {
		
		reportinfopository.updateReportInfoCheckFail(ri);
	}

	@Override
	public void updateReportInfoCheckSuccess(Report_Info ri) {
		
		reportinfopository.updateReportInfoCheckSuccess(ri);
	}

	@Override
	public Page<Report_Info> findAll(Specification<Report_Info> spec, Pageable pageable) {
		
		return reportinfopository.findAll(spec, pageable);
	}

	@Override
	public List<Report_Info> getReportBySubjectId(String subjectId) {
		
		return reportinfopository.getReportBySubjectId(subjectId);
	}

	@Override
	public Report_Info findByCode(String code) {
		List<Report_Info> reportInfos = findAllBySome((Specification) (root, criteriaQuery, criteriaBuilder) -> {
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(criteriaBuilder.equal(root.get("code"),code));
			return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
		});
		if(CollectionUtils.isNotEmpty(reportInfos)){
			return reportInfos.get(0);
		}
		return null;
	}
}
