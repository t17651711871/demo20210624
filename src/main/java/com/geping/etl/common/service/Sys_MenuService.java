package com.geping.etl.common.service;

import java.util.List;

import com.geping.etl.common.entity.Sys_Menu;

public interface Sys_MenuService {
	//显示所有菜单权限
	public Iterable<Sys_Menu> findAll();
	
	public List<Sys_Menu> getMenuBySubjectId(String subjectId);
	
}
