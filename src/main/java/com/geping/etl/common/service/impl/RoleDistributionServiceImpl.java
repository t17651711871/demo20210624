package com.geping.etl.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.common.repository.RoleDistributionRepository;
import com.geping.etl.common.service.RoleDistributionService;


@Service
@Transactional
public class RoleDistributionServiceImpl implements RoleDistributionService {

	
	@Autowired
	private RoleDistributionRepository roleDistributionRepository;
    	
	
	//根据机构编号查询属于该机构下的用户信息(用户信息属于三表联查)
	@Override
	public List<Sys_UserAndOrgDepartment> findUserOrgDepartmentByOrgId(String orgid) {
		
		return roleDistributionRepository.findUserOrgDepartmentByOrgId(orgid);
	}

}
