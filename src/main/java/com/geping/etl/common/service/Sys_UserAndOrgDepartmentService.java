package com.geping.etl.common.service;

import java.util.List;

import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;


public interface Sys_UserAndOrgDepartmentService {

	//查询所有未删除的用户
	public List<Sys_UserAndOrgDepartment> findAllSysUser();
	
	//模糊查询(在未删除的用户中查询)
	public List<Sys_UserAndOrgDepartment> findSysUserByName(String usercname);
	
	//模糊查询，角色分配中的根据用户ID查询
	public List<Sys_UserAndOrgDepartment> findSysUserByLoginID(String LoginId);
	
	//登录
	public Sys_UserAndOrgDepartment findSysUserLogin(String loginId,String password);
}
