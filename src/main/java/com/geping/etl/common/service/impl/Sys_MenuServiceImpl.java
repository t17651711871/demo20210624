package com.geping.etl.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.common.entity.Sys_Menu;
import com.geping.etl.common.repository.Sys_MenuRepository;
import com.geping.etl.common.service.Sys_MenuService;

@Service
@Transactional
public class Sys_MenuServiceImpl implements Sys_MenuService{
	@Autowired
	private Sys_MenuRepository sys_menuRepository;


	@Override
	public Iterable<Sys_Menu> findAll() {
		
		return sys_menuRepository.findAll();
	}


	@Override
	public List<Sys_Menu> getMenuBySubjectId(String subjectId) {
		
		return sys_menuRepository.getMenuBySubjectId(subjectId);
	}

}
