package com.geping.etl.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.common.entity.Sys_Auth_Role_User;
import com.geping.etl.common.repository.Sys_Auth_Role_UserRepository;
import com.geping.etl.common.service.Sys_Auth_Role_UserService;

@Service
@Transactional
public class Sys_Auth_Role_UserServiceImpl implements Sys_Auth_Role_UserService{
	@Autowired
	private Sys_Auth_Role_UserRepository sys_Auth_Role_UserRepository;
	
	@Override
	public Sys_Auth_Role_User save(Sys_Auth_Role_User sys_Auth_Role_User) {
		// TODO Auto-generated method stub
		return sys_Auth_Role_UserRepository.save(sys_Auth_Role_User);
	}

	@Override
	public Integer deleteSysAuthRoleUser(String userId) {
		// TODO Auto-generated method stub
		return sys_Auth_Role_UserRepository.deleteSysAuthRoleUser(userId);
	}

	@Override
	public List<Sys_Auth_Role_User> findSysAuthRoleUserByRoleId(String userId) {
		// TODO Auto-generated method stub
		return sys_Auth_Role_UserRepository.findSysAuthRoleUserByRoleId(userId);
	}

	@Override
	public List<Sys_Auth_Role_User> findRoleUserByRoleId(String roleId) {
		
		return sys_Auth_Role_UserRepository.findRoleUserByRoleId(roleId);
	}

}
