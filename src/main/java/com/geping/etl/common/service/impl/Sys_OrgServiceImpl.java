package com.geping.etl.common.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.common.entity.Sys_Org;
import com.geping.etl.common.repository.Sys_OrgRepository;
import com.geping.etl.common.service.Sys_OrgService;

@Service
@Transactional
public class Sys_OrgServiceImpl implements Sys_OrgService {

	
	@Autowired
	private Sys_OrgRepository sys_OrgRepository;
	
	@Override
	public Iterable<Sys_Org> findAll() {
		// TODO Auto-generated method stub
		return sys_OrgRepository.findAll();
	}
	
	
	@Override
	public List<Sys_Org> findAllSysOrg() {
		
		return sys_OrgRepository.findAllSysOrg();
	}

	
	//根据机构名称模糊查询机构信息
	public List<Sys_Org> getSys_UserByLikeOrgName(String orgName){
		
		return sys_OrgRepository.getSys_UserByLikeOrgName(orgName);
	}
	
	
	//插入
	public Sys_Org save(Sys_Org sys_Org){
		
		return sys_OrgRepository.save(sys_Org);
	}
	
	
	//根据机构名称和机构编号是否存在同名的机构信息
	public List<Sys_Org> getOrgNameAndOrgId(String orgName,String orgId){
		
		return sys_OrgRepository.getOrgNameAndOrgId(orgName, orgId);
	}
	
	//修改机构信息
	public int updateSys_Org(String id,String orgId,String orgName,String orgSortName,String orgParentId,String orgRegion,String isBussiness,String isHead,String orgLevel,String orgInsideCode,String licenseNumber,String option_,String tel){
		
		return sys_OrgRepository.updateSys_Org(id, orgId, orgName, orgSortName, orgParentId, orgRegion, isBussiness, isHead, orgLevel,orgInsideCode,licenseNumber,option_,tel);
	}
		
	
	//逻辑删除机构
	public int deleteSys_Org(String id,String enabled){
		
		return sys_OrgRepository.deleteSys_Org(id,enabled);
	}
	
	//查询条数
	@Override
	public Integer selectCountById(String orgId){
		return  sys_OrgRepository.selectCountById(orgId);
	}
	
	//根据机构代码 查机构
	public List<Sys_Org> selectOrgById(String orgId){
		
		return sys_OrgRepository.selectOrgById(orgId);
	}
}
