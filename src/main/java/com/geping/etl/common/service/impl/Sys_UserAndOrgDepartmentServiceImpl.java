package com.geping.etl.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.common.repository.Sys_UserAndOrgDepartmentRepository;
import com.geping.etl.common.service.Sys_UserAndOrgDepartmentService;

@Service
@Transactional
public class Sys_UserAndOrgDepartmentServiceImpl implements Sys_UserAndOrgDepartmentService {

	
	@Autowired
	private Sys_UserAndOrgDepartmentRepository sys_UserAndDepartmentRepository;
	
	@Override
	public List<Sys_UserAndOrgDepartment> findAllSysUser() {
		
		return sys_UserAndDepartmentRepository.findAllSysUser();
	}

	
	
	//模糊查询(在未删除的用户中查询)
    public List<Sys_UserAndOrgDepartment> findSysUserByName(String usercname){
    	
    	return sys_UserAndDepartmentRepository.findSysUserByName(usercname);
    }



	@Override
	public List<Sys_UserAndOrgDepartment> findSysUserByLoginID(String LoginId) {
		// TODO Auto-generated method stub
		return sys_UserAndDepartmentRepository.findSysUserByLoginID(LoginId);
	}


	//登录
	public Sys_UserAndOrgDepartment findSysUserLogin(String loginId,String password){
		
		return sys_UserAndDepartmentRepository.findSysUserLogin(loginId, password);
	}
	
}
