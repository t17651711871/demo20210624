package com.geping.etl.common.service;

import com.geping.etl.common.entity.Sys_Report_Operate;

public interface Sys_Report_OperateService {
	
	public Iterable<Sys_Report_Operate> findAll();
	
}
