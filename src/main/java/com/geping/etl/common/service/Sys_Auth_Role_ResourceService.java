package com.geping.etl.common.service;

import java.util.List;

import com.geping.etl.common.entity.Sys_Auth_Role_Resource;

/**
 * 
 * @author Mr.chen
 *	角色资源对照
 *	表：sys_auth_role_resource
 */
public interface Sys_Auth_Role_ResourceService {
	//点击角色分配中的保存按钮，将之前分配的角色权限全部清空，本次实现真实删除，并非逻辑上删除
	public Integer deleteSysAuthRoleResource(String id);
	//点击角色分配中的保存按钮
	public Sys_Auth_Role_Resource save(Sys_Auth_Role_Resource sysauthroleresource);
	
	//查询出已经有角色权限的数据回显到页面
	public List<Sys_Auth_Role_Resource> findAllByRoleId(String roleId);
	
	//根据用户编号和子系统编号查询出所有的权限
	public List<Sys_Auth_Role_Resource> findResourceByRoleAndSubject(String userId,String subjectId);
}
