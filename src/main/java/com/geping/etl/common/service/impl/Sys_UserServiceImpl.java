package com.geping.etl.common.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.common.entity.Sys_User;
import com.geping.etl.common.repository.Sys_UserRepository;
import com.geping.etl.common.service.Sys_UserService;


@Service
@Transactional
public class Sys_UserServiceImpl implements Sys_UserService {

	@Autowired
	private Sys_UserRepository sys_UserRepository;
	
	
	@Override
	public Sys_User getOne(String userCname, String password) {

		return sys_UserRepository.getOne(userCname, password);
	}

	@Override
	public int updatePassword(String password, String loginId) {

		return sys_UserRepository.updatePassword(password, loginId);
	}


	@Override
	public String findMaxId() {

		return sys_UserRepository.findMaxId();
	}

	@Override
	public Iterable<Sys_User> findAll() {
		Iterable<Sys_User> all = sys_UserRepository.findAll();
		return all;
	}

	@Override
	public Sys_User save(Sys_User sys_User) {
		return sys_UserRepository.save(sys_User);
	}

	@Override
	public int updateSomeSysUser(String lastLoginDate, String isFirstLogin,String id) {
		return sys_UserRepository.updateSomeSysUser(lastLoginDate, isFirstLogin,id);
	}


	//修改登录时的登录时间
	public int updateLastLoginDate(String lastLoginDate,String id){

		return sys_UserRepository.updateLastLoginDate(lastLoginDate, id);
	}

	//根据用户名模糊查询用户信息
	public List<Sys_User> getSys_UserByLikeUserCname(String userCname){

		return sys_UserRepository.getSys_UserByLikeUserCname(userCname);
	}

	//修改用户信息(不包括修改密码)
	public int updateSysUser(String id_edit,String loginId_edit,String userEname_edit,String userCname_edit,String orgId_edit,String departId_edit,String tel_edit,String mobile_edit,String address_edit,String email_edit,String description_edit){

		return sys_UserRepository.updateSysUser(id_edit, loginId_edit, userEname_edit, userCname_edit, orgId_edit, departId_edit,tel_edit, mobile_edit, address_edit, email_edit, description_edit);
	}


	//修改用户密码
	public int editPasswordSys_User(String editPasswordId,String password_edit,String date,String str){

		return sys_UserRepository.editPasswordSys_User(editPasswordId, password_edit,date,str);
	}


	//根据登录账号查询用户，避免新增用户时系统中的登录账号重复
	public Sys_User getOneByLoginId(String loginId){

		return sys_UserRepository.getOneByLoginId(loginId);
	}


	//修改用户是否锁定
	public int editIsLockedSys_User(String id,String isLocked){

		return sys_UserRepository.editIsLockedSys_User(id, isLocked);
	}

	//查询所有未删除的用户
	public List<Sys_User> findAllSysUser(){

		return sys_UserRepository.findAllSysUser();
	}


	//逻辑删除用户
	public int deleteSys_User(String id){

		return sys_UserRepository.deleteSys_User(id);
	}


	//通过登录用户的id查询该用户所有的角色资源信息
	public List getAllRoleResource(String userId){

		return sys_UserRepository.getAllRoleResource(userId);
	}

	

	@Override
	public String findLoginIdSys_User(String valueOf) {
		
		return sys_UserRepository.findLoginIdSys_User(valueOf);
	}

	@Override
	public String findSys_UserById(String id) {
		
		return sys_UserRepository.findSys_UserById(id);
	}

	@Override
	public int lockedUser(String id, String isLocked, String reason) {
		
		return sys_UserRepository.lockedUser(id, isLocked, reason);
	}

	@Override
	public int applyEdit(String id,String desc,String handlename,String handleperson,String handledate){
		
		return sys_UserRepository.applyEdit(id, desc, handlename, handleperson, handledate);
	}

	@Override
	public int applyReaptPwd(String id, String pwd, String handlename, String handleperson, String handledate) {
		
		return sys_UserRepository.applyReaptPwd(id, pwd, handlename, handleperson, handledate);
	}

	@Override
	public int applyNoLock(String id, String handlename, String handleperson, String handledate) {
		
		return sys_UserRepository.applyNoLock(id, handlename, handleperson, handledate);
	}

	@Override
	public int applyDoLock(String id, String handlename, String handleperson, String handledate,String reason) {
		
		return sys_UserRepository.applyDoLock(id, handlename, handleperson, handledate,reason);
	}

	@Override
	public int agreenAddOrEdit(String id, String handlename) {
		
		return sys_UserRepository.agreenAddOrEdit(id, handlename);
	}

	@Override
	public int agreenReapt(String id) {
		
		return sys_UserRepository.agreenReapt(id);
	}

	@Override
	public int agreenLock(String id, String lock) {
		
		return sys_UserRepository.agreenLock(id, lock);
	}

	@Override
	public int agreenNoLock(String id, String lock) {
		
		return sys_UserRepository.agreenNoLock(id, lock);
	}

	@Override
	public int refuseAddOrEdit(String id) {
		
		return sys_UserRepository.refuseAddOrEdit(id);
	}

	@Override
	public int refuseSomeHandle(String id) {
		
		return sys_UserRepository.refuseSomeHandle(id);
	}

	@Override
	public Sys_User findUserById(String id) {
		
		return sys_UserRepository.findUserById(id);
	}
}
