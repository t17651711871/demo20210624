package com.geping.etl.common.service;

import java.util.List;

import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;

public interface RoleDistributionService {
	
	//根据机构编号查询属于该机构下的用户信息(用户信息属于三表联查)
	public List<Sys_UserAndOrgDepartment> findUserOrgDepartmentByOrgId(String orgid);

}
