package com.geping.etl.common.service;


import java.util.List;

import com.geping.etl.common.entity.Sys_User;


public interface Sys_UserService {
	
	//根据id查询用户
	public Sys_User findUserById(String id);
	
	//查询所有未删除的用户
	public List<Sys_User> findAllSysUser();
	
	//获取单条数据
	public Sys_User getOne(String userCname,String password);
	    
	//密码重置
	public int updatePassword(String password,String loginId);
	
	//获取最大ID数
    public String findMaxId();
	 
	 //显示所有user的信息
	 public Iterable<Sys_User> findAll();
	 
	 //插入
	 public Sys_User save(Sys_User sys_User);
	 
	//修改登录时的登录时间和是否为首次登录
	public int updateSomeSysUser(String lastLoginDate,String isFirstLogin,String id);
	
	//修改登录时的登录时间
	public int updateLastLoginDate(String lastLoginDate,String id);
	
	//根据用户名模糊查询用户信息
	public List<Sys_User> getSys_UserByLikeUserCname(String userCname);
	
	//修改用户信息(不包括修改密码)
	public int updateSysUser(String id_edit,String loginId_edit,String userEname_edit,String userCname_edit,String orgId_edit,String departId_edit,String tel_edit,String mobile_edit,String address_edit,String email_edit,String description_edit);

	//修改用户密码
	public int editPasswordSys_User(String editPasswordId,String password_edit,String date,String str);
	
	//根据登录账号查询用户，避免新增用户时系统中的登录账号重复
	public Sys_User getOneByLoginId(String loginId);
	
	//修改用户是否锁定
	public int editIsLockedSys_User(String id,String isLocked);
	
	public int lockedUser(String id,String isLocked,String reason);
	
	//逻辑删除用户
	public int deleteSys_User(String id);
	
	//通过登录用户的id查询该用户所有的角色资源信息
	public List getAllRoleResource(String userId);
	

	//根据id来查询用户
	public String findLoginIdSys_User(String valueOf);
	
	public String findSys_UserById(String id);
	
	public int applyEdit(String id,String desc,String handlename,String handleperson,String handledate);
	
	public int applyReaptPwd(String id,String pwd,String handlename,String handleperson,String handledate);
	
	public int applyNoLock(String id,String handlename,String handleperson,String handledate);
	
	public int applyDoLock(String id,String handlename,String handleperson,String handledate,String reason);
	
	public int agreenAddOrEdit(String id,String handlename);
	
	public int agreenReapt(String id);
	
	public int agreenLock(String id,String lock);
	
	public int agreenNoLock(String id,String lock);
	
	public int refuseAddOrEdit(String id);
	
	public int refuseSomeHandle(String id);
}
