package com.geping.etl.common.service;

import com.geping.etl.common.entity.Report_Info;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface Report_InfoService {
	//显示所有报表
	public List<Report_Info> findAll();
	
	//条件查询报表
	public List<Report_Info> findAllBySome(Specification<Report_Info> spec);
	
	 //修改待提交数量
    public void updateReportInfoPending(Report_Info ri);
    
    //修改待审核数量
    public void updateReportInfoPendingAudit(Report_Info ri);
    
    //修改审核不通过数量
    public void updateReportInfoCheckFail(Report_Info ri);
    
    //修改审核通过数量
    public void updateReportInfoCheckSuccess(Report_Info ri);
    
    public Page<Report_Info> findAll(Specification<Report_Info> spec, Pageable pageable);
    
    public List<Report_Info> getReportBySubjectId(String subjectId);

    /**
     * 根据code查询报表
     * @param code
     * @return
     */
    public Report_Info findByCode(String code);
}
