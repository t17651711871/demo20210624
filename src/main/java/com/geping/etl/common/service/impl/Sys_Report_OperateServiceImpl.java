package com.geping.etl.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.common.entity.Sys_Report_Operate;
import com.geping.etl.common.repository.Sys_Report_OperateRepository;
import com.geping.etl.common.service.Sys_Report_OperateService;
@Service
@Transactional
public class Sys_Report_OperateServiceImpl implements Sys_Report_OperateService{

	@Autowired
	private Sys_Report_OperateRepository sysreportoperaterepository;
	
	@Override
	public Iterable<Sys_Report_Operate> findAll() {
		// TODO Auto-generated method stub
		return sysreportoperaterepository.findAll();
	}

}
