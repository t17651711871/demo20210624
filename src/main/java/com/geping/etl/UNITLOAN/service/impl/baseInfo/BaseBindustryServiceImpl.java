package com.geping.etl.UNITLOAN.service.impl.baseInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry;
import com.geping.etl.UNITLOAN.repository.baseInfo.BaseBindustryRepository;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseBindustryService;
@Service
@Transactional
public class BaseBindustryServiceImpl implements BaseBindustryService{

	@Autowired
	private BaseBindustryRepository bbr;

	@Override
	public List<BaseBindustry> findAll() {
		Sort sort = new Sort(Sort.Direction.ASC, "bindustrycode");
		return bbr.findAll(sort);
	}

	@Override
	public List<BaseBindustry> findAll(Specification<BaseBindustry> spec) {
		return bbr.findAll(spec);
	}

	@Override
	public Page<BaseBindustry> findAll(Specification<BaseBindustry> spec, Pageable page) {
		return bbr.findAll(spec, page);
	}

	@Override
	public void daoRuFuGai(List<BaseBindustry> list) {
		bbr.deleteAll();
		bbr.save(list);
	}
}
