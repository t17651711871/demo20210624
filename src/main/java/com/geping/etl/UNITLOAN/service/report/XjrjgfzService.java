package com.geping.etl.UNITLOAN.service.report;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.report.Xjrjgfz;

public interface XjrjgfzService {

	//查询指定客户
	public Xjrjgfz findOne(String id);
	//查询所有
	public List<Xjrjgfz> findAll();
	//查询所有带条件
	public List<Xjrjgfz> findAll(Specification<Xjrjgfz> spec);
	//查询所有带条件并分页
	public Page<Xjrjgfz> findAll(Specification<Xjrjgfz> spec,Pageable page);
	//保存单个实体
	public Xjrjgfz Save(Xjrjgfz Xjrjgfz);
	//批量保存实体
	public Integer Save(List<Xjrjgfz> list);
	//保存或更新实体
	public Xjrjgfz SaveOrUpdate(Xjrjgfz Xjrjgfz);
	//批量更新校验状态
	public Integer updateXjrjgfzOnCheckstatus(String checkStatus);
	//批量更新校验状态
	public Integer updateXjrjgfzOnCheckstatus(String checkStatus,List<String> idList);
	//批量更新审核状态
	public Integer updateXjrjgfzOnDatastatus(String dataStatus,String user,String departid);
	//批量更新审核状态
	public Integer updateXjrjgfzOnDatastatus(String dataStatus,String user,List<String> idList);
	//删除所有
	public Integer delete();
	//删除单个实体
	public Integer delete(String id);
	//批量删除实体
	public Integer delete(List<String> list);
}
