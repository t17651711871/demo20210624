package com.geping.etl.UNITLOAN.service.report;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.JdbcTemplate;

import com.geping.etl.UNITLOAN.entity.report.XCheckRule;

/**    
*  
* @author liuweixin  
* @date 2020年9月21日 上午10:09:22  
*/
public interface XCheckRuleService {

	Page<XCheckRule> findAll(Specification specification, PageRequest pageRequest);

	void importXCheckRuleList(List<XCheckRule> list);
	
	List<String> getReportnameList();
	
	List<String> getFieldnameList(String reportname);
	
	void deleteAll();
	
	List<String> getChecknum(String tablename);
}
