package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.Xzqtzfsexx;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.service.report
 * @USER: tangshuai
 * @DATE: 2021/3/30
 * @TIME: 16:20
 * @描述:
 */
public interface XzqtzfsexxService {
    Page<Xzqtzfsexx> findAll(Specification specification, PageRequest pageRequest);
}
