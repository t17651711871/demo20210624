package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.XreportInfo;
import com.geping.etl.UNITLOAN.repository.report.XreportInfoRepository;
import com.geping.etl.UNITLOAN.service.report.XreportInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class XreportInfoServiceImpl implements XreportInfoService {

    @Autowired
    private XreportInfoRepository reportInfoRepository;

    @Override
    public Page<XreportInfo> findAll(Specification specification,PageRequest pageRequest){
        return reportInfoRepository.findAll(specification,pageRequest);
    }

    @Override
    public XreportInfo getById(String id) {
        return reportInfoRepository.findOne(id);
    }
}
