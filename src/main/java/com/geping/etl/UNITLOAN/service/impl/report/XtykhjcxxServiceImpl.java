package com.geping.etl.UNITLOAN.service.impl.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.report.Xtykhjcxx;
import com.geping.etl.UNITLOAN.repository.report.XtykhjcxxRepository;
import com.geping.etl.UNITLOAN.service.report.XtykhjcxxService;

/**    
*  
* @author liuweixin  
* @date 2021年1月6日 上午10:16:33  
*/
@Service
@Transactional
public class XtykhjcxxServiceImpl implements XtykhjcxxService {
	@Autowired
	private XtykhjcxxRepository xtykhjcxxRepository;

	@Override
	public Xtykhjcxx save(Xtykhjcxx tykhjcxx) {
		return xtykhjcxxRepository.save(tykhjcxx);
	}

	@Override
	public List<Xtykhjcxx> saveAll(List<Xtykhjcxx> list) {
		return xtykhjcxxRepository.save(list);
	}

	@Override
	public Page<Xtykhjcxx> findAll(Specification specification, PageRequest pageRequest) {
		return xtykhjcxxRepository.findAll(specification, pageRequest);
	}

}
