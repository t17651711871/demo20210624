package com.geping.etl.UNITLOAN.service.report;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.report.XjrjgfrAssets;

public interface XjrjgfrAssetsService {

	//查询指定客户
	public XjrjgfrAssets findOne(String id);
	//查询所有
	public List<XjrjgfrAssets> findAll();
	//查询所有带条件
	public List<XjrjgfrAssets> findAll(Specification<XjrjgfrAssets> spec);
	//查询所有带条件并分页
	public Page<XjrjgfrAssets> findAll(Specification<XjrjgfrAssets> spec,Pageable page);
	//查询数量
	public Integer findTotal();
	//保存单个实体
	public XjrjgfrAssets Save(XjrjgfrAssets XjrjgfrAssets);
	//批量保存实体
	public Integer Save(List<XjrjgfrAssets> list);
	//保存或更新实体
	public XjrjgfrAssets SaveOrUpdate(XjrjgfrAssets XjrjgfrAssets);
	//批量更新校验状态
	public Integer updateXjrjgfrAssetsOnCheckstatus(String checkStatus);
	//批量更新校验状态
	public Integer updateXjrjgfrAssetsOnCheckstatus(String checkStatus,List<String> idList);
	//批量更新审核状态
	public Integer updateXjrjgfrAssetsOnDatastatus(String dataStatus,String user,String departId);
	//批量更新审核状态
	public Integer updateXjrjgfrAssetsOnDatastatus(String dataStatus,String user,List<String> idList);
	//删除所有
	public Integer delete();
	//删除单个实体
	public Integer delete(String id);
	//批量删除实体
	public Integer delete(List<String> list);
}
