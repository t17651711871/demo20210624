package com.geping.etl.UNITLOAN.service.report;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.report.XjrjgfrProfit;

public interface XjrjgfrProfitService {

	//查询指定客户
	public XjrjgfrProfit findOne(String id);
	//查询所有
	public List<XjrjgfrProfit> findAll();
	//查询所有带条件
	public List<XjrjgfrProfit> findAll(Specification<XjrjgfrProfit> spec);
	//查询所有带条件并分页
	public Page<XjrjgfrProfit> findAll(Specification<XjrjgfrProfit> spec,Pageable page);
	//查询数量
	public Integer findTotal();
	//保存单个实体
	public XjrjgfrProfit Save(XjrjgfrProfit XjrjgfrProfit);
	//批量保存实体
	public Integer Save(List<XjrjgfrProfit> list);
	//保存或更新实体
	public XjrjgfrProfit SaveOrUpdate(XjrjgfrProfit XjrjgfrProfit);
	//批量更新校验状态
	public Integer updateXjrjgfrProfitOnCheckstatus(String checkStatus);
	//批量更新校验状态
	public Integer updateXjrjgfrProfitOnCheckstatus(String checkStatus,List<String> idList);
	//批量更新审核状态
	public Integer updateXjrjgfrProfitOnDatastatus(String dataStatus,String user,String departId);
	//批量更新审核状态
	public Integer updateXjrjgfrProfitOnDatastatus(String dataStatus,String user,List<String> idList);
	//删除所有
	public Integer delete();
	//删除单个实体
	public Integer delete(String id);
	//批量删除实体
	public Integer delete(List<String> list);
}
