package com.geping.etl.UNITLOAN.service.check.processors.gqtzxx;


import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.report.Xgqtzfsexx;
import com.geping.etl.UNITLOAN.enums.DataTypeEnum;
import com.geping.etl.UNITLOAN.service.check.processors.CheckProcessorTemplateService;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.EnhanceDataDictionary;
import com.geping.etl.UNITLOAN.util.XDataBaseTypeUtil;
import com.geping.etl.UNITLOAN.util.check.CheckXgqtzfsexxRowData;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 股权投资发生额信息
 */

@Service
public class XgqtzfsexxCheckProcessorServiceImpl extends CheckProcessorTemplateService {
    @Autowired
    private DepartUtil departUtil;
    @Autowired
    private CheckHelper checkHelper;
    @Autowired
    private XDataBaseTypeUtil dataBaseTypeUtil;
    @Autowired
    private CheckXgqtzfsexxRowData checkXgqtzfsexxRowData;
    @Autowired
    private EnhanceDataDictionary enhanceDataDictionary;
    @Autowired
    private CustomSqlUtil customSqlUtil;
    /**
     * 获取实体类
     * @return
     */
    @Override
    public String getKey() {
        return Xgqtzfsexx.class.getSimpleName();
    }

    /**
     *获取准备SQL
     * @param checkParamContext
     */
    @Override
    protected void prepareSQl(CheckParamContext checkParamContext) {
        String whereStr=" where a.datastatus='0' and a.checkstatus='0'";
        String departId = departUtil.getDepart(checkParamContext.getSysUser());
        String id=checkParamContext.getId();
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            System.out.println(id);
            whereStr = whereStr + " and a.id in(" + id + ")";
        }
        whereStr = whereStr + " and a.departid like '%"+ departId +"%'";
        String sql="select * from xgqtzfsexx a"+whereStr;
        String sqlcount="select count(*) from xgqtzfsexx a"+whereStr;
        checkParamContext.setCountsql(sqlcount);
        checkParamContext.setSql(sql);
        checkParamContext.setWhereCond(whereStr);
    }

    /**
     * 获取错误头
     * @return
     */
    @Override
    protected String getHeaderTpl() {
        return  "金融机构代码"+ SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.COMMA+"凭证编码"+SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.ERROR_TIP_SPLIT;
    }

    /**
     * 3.校验行
     * @param checkParamContext
     * @param t
     */
    @Override
    protected void checkRow(CheckParamContext checkParamContext,Object t) {
        Xgqtzfsexx xgqtzfsexx= (Xgqtzfsexx) t;
        checkXgqtzfsexxRowData.checkRow(getHeaderTpl(),checkParamContext,xgqtzfsexx);
    }

    /**
     * 表间校验
     * @param checkParamContext
     */
    @Override
    protected void tableCalibration(CheckParamContext checkParamContext) {
        //数据日期+金融机构+凭证编码+交易日期+交易方向
        tableCalibrationJS2322(checkParamContext);
        //金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
        tableCalibrationJS1804(checkParamContext);
        //股权类型与存量股权投资信息的股权类型应该一致
        tableCalibrationJS1755(checkParamContext);
        //机构证件代码与存量股权投资信息的机构证件代码应该一致
        tableCalibrationJS1756(checkParamContext);
        //机构证件代码应该在同业客户基础信息.客户代码或者非同业单位客户基础信息.客户证件代码中存在
        tableCalibrationJS1917(checkParamContext);
    }

    private void tableCalibrationJS1917(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS1917")){
            String existsSql;
            boolean b = dataBaseTypeUtil.equalsOracle();
            if (b){
                existsSql=(String) SpringContextUtil.getBean("Xgqtzfsexx.Ftkfora");
            }else{
                existsSql=(String) SpringContextUtil.getBean("Xgqtzfsexx.Ftkf");
            }
            String format = String.format(existsSql,checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"机构证件代码应该在同业客户基础信息.客户代码或者非同业单位客户基础信息.客户证件代码中存在");
        }
    }

    private void tableCalibrationJS1756(CheckParamContext checkParamContext) {
        if (checkParamContext.getCheckNums().contains("JS1756")) {
            String existsSql = (String) SpringContextUtil.getBean("Xgqtzfsexx.Jgzjdm");
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(), checkParamContext, format, "机构证件代码与存量股权投资信息的机构证件代码应该一致");
        }
    }

    private void tableCalibrationJS1755(CheckParamContext checkParamContext) {
        if (checkParamContext.getCheckNums().contains("JS1755")) {
            String existsSql = (String) SpringContextUtil.getBean("Xgqtzfsexx.Gg");
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(), checkParamContext, format, "股权类型与存量股权投资信息的股权类型应该一致");
        }
    }

    /**
     * 金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
     * @param checkParamContext
     */
    private void tableCalibrationJS1804(CheckParamContext checkParamContext) {
        if (checkParamContext.getCheckNums().contains("JS1804")) {
            String existsSql = (String) SpringContextUtil.getBean("Xgqtzfsexx.Jj");
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(), checkParamContext, format, "金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致");
        }
    }

    /**
     * 数据日期+金融机构+凭证编码+交易日期+交易方向 唯一
     * @param checkParamContext
     */
    private void tableCalibrationJS2322(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS2322")){
            String existsSql;
            boolean b = dataBaseTypeUtil.equalsOracle();
            if (b){
                existsSql= (String) SpringContextUtil.getBean("Xgqtzfsexx.Sjpjjora");
            }else {
                existsSql= (String) SpringContextUtil.getBean("Xgqtzfsexx.Sjpjj");
            }
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"数据日期+金融机构+凭证编码+交易日期+交易方向应唯一");
        }
    }

    @Override
    protected  void enhanceCheckParamContext(CheckParamContext checkParamContext){
        //股权信息
        List<String> gqlxList = enhanceDataDictionary.findCode(DataTypeEnum.GQLX.getType());
        //机构类型
        List<String> jglxList = enhanceDataDictionary.findCode(DataTypeEnum.JGLX.getType());
        //交易方向
        List<String> jyfxList = enhanceDataDictionary.findCode(DataTypeEnum.JYFX.getType());
        //币种
        List baseCode = customSqlUtil.getBaseCode(BaseCurrency.class);
        //国家地区代码C0013
        List baseCodeGj = customSqlUtil.getBaseCode(BaseCountry.class);
        //行政区划代码C0009
        List baseCodeXz = customSqlUtil.getBaseCode(BaseArea.class);
        Map<String,List> map = new HashMap<>();
        map.put("gqlxList",gqlxList);
        map.put("jglxList",jglxList);
        map.put("jyfxList",jyfxList);
        map.put("baseCode",baseCode);
        map.put("baseCodeGj",baseCodeGj);
        map.put("baseCodeXz",baseCodeXz);
        checkParamContext.setDataDictionarymap(map);
    }
}