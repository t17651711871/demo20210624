package com.geping.etl.UNITLOAN.service.impl.baseInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry;
import com.geping.etl.UNITLOAN.repository.baseInfo.BaseAindustryRepository;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseAindustryService;
@Service
@Transactional
public class BaseAindustryServiceImpl implements BaseAindustryService{

	@Autowired
	private BaseAindustryRepository bar;

	@Override
	public List<BaseAindustry> findAll() {
		Sort sort = new Sort(Sort.Direction.ASC, "aindustrycode");
		return bar.findAll(sort);
	}

	@Override
	public List<BaseAindustry> findAll(Specification<BaseAindustry> spec) {
		return bar.findAll(spec);
	}

	@Override
	public Page<BaseAindustry> findAll(Specification<BaseAindustry> spec, Pageable page) {
		return bar.findAll(spec, page);
	}
	
	@Override
	public void daoRuFuGai(List<BaseAindustry> list) {
		bar.deleteAll();
		bar.save(list);
	}
}
