package com.geping.etl.UNITLOAN.service.impl.baseInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.repository.baseInfo.BaseCurrencyRepository;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseCurrencyService;
@Service
@Transactional
public class BaseCurrencyServiceImpl implements BaseCurrencyService{

	@Autowired
	private BaseCurrencyRepository bcr;

	@Override
	public List<BaseCurrency> findAll() {
		Sort sort = new Sort(Sort.Direction.ASC, "currencycode");
		return bcr.findAll(sort);
	}

	@Override
	public List<BaseCurrency> findAll(Specification<BaseCurrency> spec) {
		return bcr.findAll(spec);
	}

	@Override
	public Page<BaseCurrency> findAll(Specification<BaseCurrency> spec, Pageable page) {
		return bcr.findAll(spec, page);
	}

	@Override
	public void daoRuFuGai(List<BaseCurrency> list) {
		bcr.deleteAll();
		bcr.save(list);
	}
}
