package com.geping.etl.UNITLOAN.service.impl.baseInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseNation;
import com.geping.etl.UNITLOAN.repository.baseInfo.BaseNationRepository;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseNationService;

/**    
*  
* @author liuweixin  
* @date 2020年11月4日 下午3:53:19  
*/
@Service
@Transactional
public class BaseNationServiceImpl implements BaseNationService{

	@Autowired
	private BaseNationRepository bnr;
	
	@Override
	public List<BaseNation> findAll() {
		return bnr.getAll();
	}

	@Override
	public List<BaseNation> findAll(Specification<BaseNation> spec) {
		return bnr.findAll(spec);
	}

	@Override
	public Page<BaseNation> findAll(Specification<BaseNation> spec, Pageable page) {
		return bnr.findAll(spec, page);
	}

	@Override
	public void daoRuFuGai(List<BaseNation> list) {
		bnr.deleteAll();
		bnr.save(list);
		
	}

}
