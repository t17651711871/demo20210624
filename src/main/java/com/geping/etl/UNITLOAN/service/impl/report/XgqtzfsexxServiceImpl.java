package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xclgqtzxx;
import com.geping.etl.UNITLOAN.entity.report.Xgqtzfsexx;
import com.geping.etl.UNITLOAN.repository.report.XgqtzfsexxRepository;
import com.geping.etl.UNITLOAN.service.report.XgqtzfsexxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.service.impl.report
 * @USER: tangshuai
 * @DATE: 2021/5/17
 * @TIME: 16:44
 * @描述:
 */
@Transactional
@Service
public class XgqtzfsexxServiceImpl implements XgqtzfsexxService {
    @Autowired
    private XgqtzfsexxRepository xclgqtzxxRepository;

    @Override
    public Page<Xgqtzfsexx> findAll(Specification specification, PageRequest pageRequest) {
        return xclgqtzxxRepository.findAll(specification,pageRequest);
    }
}
