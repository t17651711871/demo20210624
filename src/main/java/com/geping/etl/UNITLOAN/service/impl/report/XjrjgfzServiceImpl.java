package com.geping.etl.UNITLOAN.service.impl.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.report.Xjrjgfz;
import com.geping.etl.UNITLOAN.repository.report.XjrjgfzRepository;
import com.geping.etl.UNITLOAN.service.report.XjrjgfzService;
@Service
@Transactional
public class XjrjgfzServiceImpl implements XjrjgfzService{

	@Autowired
	private XjrjgfzRepository xr;
	
	@Override
	public Xjrjgfz findOne(String id) {
		return xr.findOne(id);
	}

	@Override
	public List<Xjrjgfz> findAll() {
		return xr.findAll();
	}

	@Override
	public List<Xjrjgfz> findAll(Specification<Xjrjgfz> spec) {
		return xr.findAll(spec);
	}

	@Override
	public Page<Xjrjgfz> findAll(Specification<Xjrjgfz> spec, Pageable page) {
		return xr.findAll(spec, page);
	}

	@Override
	public Xjrjgfz Save(Xjrjgfz Xjrjgfz) {
		return xr.save(Xjrjgfz);
	}

	@Override
	public Integer Save(List<Xjrjgfz> list) {
		if(list!=null&&list.size()>0) {
			xr.save(list);
			return 1;
		}else {
			return 0;
		}
	}

	@Override
	public Xjrjgfz SaveOrUpdate(Xjrjgfz Xjrjgfz) {
		return xr.save(Xjrjgfz);
	}

	@Override
	public Integer updateXjrjgfzOnCheckstatus(String checkStatus) {
		return xr.updateXjrjgfzOnCheckstatus(checkStatus);
	}

	@Override
	public Integer updateXjrjgfzOnCheckstatus(String checkStatus, List<String> idList) {
		return xr.updateXjrjgfzOnCheckstatus(checkStatus, idList);
	}
	
	@Override
	public Integer updateXjrjgfzOnDatastatus(String dataStatus,String user,String departid) {
		return xr.updateXjrjgfzOnDatastatus(dataStatus,user,departid);
	}

	@Override
	public Integer updateXjrjgfzOnDatastatus(String dataStatus,String user,List<String> idList) {
		return xr.updateXjrjgfzOnDatastatus(dataStatus,user,idList);
	}
	
	@Override
	public Integer delete() {
		xr.deleteAll();
		return 1;
	}

	@Override
	public Integer delete(String id) {
		xr.delete(id);
		return 1;
	}

	@Override
	public Integer delete(List<String> list) {
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				xr.delete(list.get(i));
			}
			return 1;
		}else {
			return 0;
		}
	}

}
