package com.geping.etl.UNITLOAN.service.impl.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.report.XjrjgfrBaseinfo;
import com.geping.etl.UNITLOAN.repository.report.XjrjgfrBaseinfoRepository;
import com.geping.etl.UNITLOAN.service.report.XjrjgfrBaseinfoService;
@Service
@Transactional
public class XjrjgfrBaseinfoServiceImpl implements XjrjgfrBaseinfoService{

	@Autowired
	private XjrjgfrBaseinfoRepository xfr;
	
	@Override
	public XjrjgfrBaseinfo findOne(String id) {
		return xfr.findOne(id);
	}

	@Override
	public List<XjrjgfrBaseinfo> findAll() {
		return xfr.findAll();
	}

	@Override
	public List<XjrjgfrBaseinfo> findAll(Specification<XjrjgfrBaseinfo> spec) {
		return xfr.findAll(spec);
	}

	@Override
	public Page<XjrjgfrBaseinfo> findAll(Specification<XjrjgfrBaseinfo> spec, Pageable page) {
		return xfr.findAll(spec, page);
	}

	@Override
	public Integer findTotal() {
		return xfr.findTotal();
	}
	
	@Override
	public XjrjgfrBaseinfo Save(XjrjgfrBaseinfo XjrjgfrBaseinfo) {
		return xfr.save(XjrjgfrBaseinfo);
	}

	@Override
	public Integer Save(List<XjrjgfrBaseinfo> list) {
		if(list!=null&&list.size()>0) {
			xfr.save(list);
			return 1;
		}else {
			return 0;
		}
	}

	@Override
	public XjrjgfrBaseinfo SaveOrUpdate(XjrjgfrBaseinfo XjrjgfrBaseinfo) {
		return xfr.save(XjrjgfrBaseinfo);
	}

	@Override
	public Integer updateXjrjgfrBaseinfoOnCheckstatus(String checkStatus) {
		return xfr.updateXjrjgfrBaseinfoOnCheckstatus(checkStatus);
	}

	@Override
	public Integer updateXjrjgfrBaseinfoOnCheckstatus(String checkStatus, List<String> idList) {
		return xfr.updateXjrjgfrBaseinfoOnCheckstatus(checkStatus,idList);
	}
	
	@Override
	public Integer updateXjrjgfrBaseinfoOnDatastatus(String dataStatus,String user,String departId) {
		return xfr.updateXjrjgfrBaseinfoOnDatastatus(dataStatus,user,departId);
	}

	@Override
	public Integer updateXjrjgfrBaseinfoOnDatastatus(String dataStatus,String user,List<String> idList) {
		return xfr.updateXjrjgfrBaseinfoOnDatastatus(dataStatus,user,idList);
	}
	
	@Override
	public Integer delete() {
		xfr.deleteAll();
		return 1;
	}

	@Override
	public Integer delete(String id) {
		xfr.delete(id);
		return 1;
	}

	@Override
	public Integer delete(List<String> list) {
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				xfr.delete(list.get(i));
			}
			return 1;
		}else {
			return 0;
		}
	}

}
