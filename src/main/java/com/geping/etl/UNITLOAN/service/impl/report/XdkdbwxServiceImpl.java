package com.geping.etl.UNITLOAN.service.impl.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.report.Xdkdbwx;
import com.geping.etl.UNITLOAN.repository.report.XdkdbwxRepository;
import com.geping.etl.UNITLOAN.service.report.XdkdbwxService;
@Service
@Transactional
public class XdkdbwxServiceImpl implements XdkdbwxService {

	@Autowired
	private XdkdbwxRepository xr;
	
	@Override
	public Xdkdbwx findOne(String id) {
		return xr.findOne(id);
	}

	@Override
	public List<Xdkdbwx> findAll() {
		return xr.findAll();
	}

	@Override
	public List<Xdkdbwx> findAll(Specification<Xdkdbwx> spec) {
		return xr.findAll(spec);
	}

	@Override
	public Page<Xdkdbwx> findAll(Specification<Xdkdbwx> spec, Pageable page) {
		return xr.findAll(spec, page);
	}

	@Override
	public Xdkdbwx Save(Xdkdbwx Xdkdbwx) {
		return xr.save(Xdkdbwx);
	}

	@Override
	public Integer Save(List<Xdkdbwx> list) {
		if(list!=null&&list.size()>0) {
			/*for(int i=0;i<list.size();i++) {
				xr.saveAndFlush(list.get(i));
			}*/
			xr.save(list);
			return 1;
		}else {
			return 0;
		}
	}

	@Override
	public Xdkdbwx SaveOrUpdate(Xdkdbwx Xdkdbwx) {
		return xr.save(Xdkdbwx);
	}

	@Override
	public Integer delete() {
		xr.deleteAll();
		return 1;
	}

	@Override
	public Integer delete(String id) {
		xr.delete(id);
		return 1;
	}

	@Override
	public Integer delete(List<String> list) {
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				xr.delete(list.get(i));
			}
			return 1;
		}else {
			return 0;
		}
	}

	@Override
	public Integer updateXdkdbwxOnCheckstatus(String checkStatus) {
		return xr.updateXdkdbwxOnCheckstatus(checkStatus);
	}
	
	@Override
	public Integer updateXdkdbwxOnCheckstatus(String checkStatus, List<String> idList) {
		return xr.updateXdkdbwxOnCheckstatus(checkStatus, idList);
	}

	@Override
	public Integer updateXdkdbwxOnDatastatus(String dataStatus,String user,String departId) {
		return xr.updateXdkdbwxOnDatastatus(dataStatus,user,departId);
	}

	@Override
	public Integer updateXdkdbwxOnDatastatus(String dataStatus,String user,List<String> idList) {
		return xr.updateXdkdbwxOnDatastatus(dataStatus,user,idList);
	}
	
}
