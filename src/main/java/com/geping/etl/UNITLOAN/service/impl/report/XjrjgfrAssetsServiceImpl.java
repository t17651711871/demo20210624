package com.geping.etl.UNITLOAN.service.impl.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.report.XjrjgfrAssets;
import com.geping.etl.UNITLOAN.repository.report.XjrjgfrAssetsRepository;
import com.geping.etl.UNITLOAN.service.report.XjrjgfrAssetsService;
@Service
@Transactional
public class XjrjgfrAssetsServiceImpl implements XjrjgfrAssetsService{

	@Autowired
	private XjrjgfrAssetsRepository xar;
	
	@Override
	public XjrjgfrAssets findOne(String id) {
		return xar.findOne(id);
	}

	@Override
	public List<XjrjgfrAssets> findAll() {
		return xar.findAll();
	}

	@Override
	public List<XjrjgfrAssets> findAll(Specification<XjrjgfrAssets> spec) {
		return xar.findAll(spec);
	}

	@Override
	public Page<XjrjgfrAssets> findAll(Specification<XjrjgfrAssets> spec, Pageable page) {
		return xar.findAll(spec, page);
	}

	@Override
	public Integer findTotal() {
		return xar.findTotal();
	}
	
	@Override
	public XjrjgfrAssets Save(XjrjgfrAssets XjrjgfrAssets) {
		return xar.save(XjrjgfrAssets);
	}

	@Override
	public Integer Save(List<XjrjgfrAssets> list) {
		if(list!=null&&list.size()>0) {
			xar.save(list);
			return 1;
		}else {
			return 0;
		}		 
	}

	@Override
	public XjrjgfrAssets SaveOrUpdate(XjrjgfrAssets XjrjgfrAssets) {
		return xar.save(XjrjgfrAssets);
	}

	@Override
	public Integer updateXjrjgfrAssetsOnCheckstatus(String checkStatus) {
		return xar.updateXjrjgfrAssetsOnCheckstatus(checkStatus);
	}
	
	@Override
	public Integer updateXjrjgfrAssetsOnCheckstatus(String checkStatus, List<String> idList) {
		return xar.updateXjrjgfrAssetsOnCheckstatus(checkStatus,idList);
	}

	@Override
	public Integer updateXjrjgfrAssetsOnDatastatus(String dataStatus,String user,String departId) {
		return xar.updateXjrjgfrAssetsOnDatastatus(dataStatus,user,departId);
	}

	@Override
	public Integer updateXjrjgfrAssetsOnDatastatus(String dataStatus,String user,List<String> idList) {
		return xar.updateXjrjgfrAssetsOnDatastatus(dataStatus,user,idList);
	}
	
	@Override
	public Integer delete() {
		xar.deleteAll();
		return 1;
	}

	@Override
	public Integer delete(String id) {
		xar.delete(id);
		return 1;
	}

	@Override
	public Integer delete(List<String> list) {
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				xar.delete(list.get(i));
			}
			return 1;
		}else {
			return 0;
		}
	}

}
