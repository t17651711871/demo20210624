package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.Xclgqtzxx;
import com.geping.etl.UNITLOAN.entity.report.Xzqfxfsexx;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.service.report
 * @USER: tangshuai
 * @DATE: 2021/5/17
 * @TIME: 15:55
 * @描述:
 */
public interface XclgqtzxxService {
    Page<Xclgqtzxx> findAll(Specification specification, PageRequest pageRequest);
}
