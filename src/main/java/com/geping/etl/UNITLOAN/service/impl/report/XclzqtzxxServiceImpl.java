package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xclzqtzxx;
import com.geping.etl.UNITLOAN.repository.report.XclzqtzxxRepository;
import com.geping.etl.UNITLOAN.service.report.XclzqtzxxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.service.impl.report
 * @USER: tangshuai
 * @DATE: 2021/3/30
 * @TIME: 16:13
 * @描述:
 */
@Service
@Transactional
public class XclzqtzxxServiceImpl implements XclzqtzxxService {
    @Autowired
    private XclzqtzxxRepository xclzqtzxxRepository;

    @Override
    public Page<Xclzqtzxx> findAll(Specification specification, PageRequest pageRequest) {
        return xclzqtzxxRepository.findAll(specification,pageRequest);
    }
}
