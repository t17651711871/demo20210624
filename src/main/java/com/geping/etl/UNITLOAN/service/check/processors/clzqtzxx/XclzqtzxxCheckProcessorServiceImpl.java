package com.geping.etl.UNITLOAN.service.check.processors.clzqtzxx;


import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.entity.report.Xclzqtzxx;
import com.geping.etl.UNITLOAN.service.check.processors.CheckProcessorTemplateService;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.XDataBaseTypeUtil;
import com.geping.etl.UNITLOAN.util.check.CheckXclzqtzxxRowData;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 存量债券投资信息
 */

@Service
public class XclzqtzxxCheckProcessorServiceImpl extends CheckProcessorTemplateService {
    @Autowired
    private DepartUtil departUtil;
    @Autowired
    private CheckHelper checkHelper;
    @Autowired
    private CheckXclzqtzxxRowData checkXclzqtzxxRowData;
    @Autowired
    private XDataBaseTypeUtil dataBaseTypeUtil;

    /**
     * 获取实体类
     * @return
     */
    @Override
    public String getKey() {
        return Xclzqtzxx.class.getSimpleName();
    }

    /**
     *获取准备SQL
     * @param checkParamContext
     */
    @Override
    protected void prepareSQl(CheckParamContext checkParamContext) {
        String departId = departUtil.getDepart(checkParamContext.getSysUser());
        String whereStr=" where a.datastatus='0' and a.checkstatus='0'";
        String id=checkParamContext.getId();
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            System.out.println(id);
            whereStr = whereStr + " and a.id in(" + id + ")";
        }
        whereStr = whereStr + " and a.departid like '%"+ departId +"%'";
        String sql="select * from xclzqtzxx a "+whereStr;
        String sqlcount="select count(*) from xclzqtzxx a "+whereStr;
        checkParamContext.setCountsql(sqlcount);
        checkParamContext.setSql(sql);
        checkParamContext.setWhereCond(whereStr);
    }

    /**
     * 获取错误头
     * @return
     */
    @Override
    protected String getHeaderTpl() {
        return  "金融机构代码"+ SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.COMMA+"债券代码"+SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.ERROR_TIP_SPLIT;
    }

    /**
     * 3.校验行
     * @param checkParamContext
     * @param t
     */
    @Override
    protected void checkRow(CheckParamContext checkParamContext,Object t) {
        Xclzqtzxx xclzqtzxx= (Xclzqtzxx) t;
        checkXclzqtzxxRowData.checkRow(getHeaderTpl(),checkParamContext,xclzqtzxx);
    }

    /**
     * 表间校验
     * @param checkParamContext
     */
    @Override
    protected void tableCalibration(CheckParamContext checkParamContext) {
        //数据日期+金融机构代码+债券代码应唯一
        tableCalibrationJS2317(checkParamContext);
        //金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
        tableCalibrationJS1789(checkParamContext);
        //发行人证件代码应该在同业客户基础信息.客户代码或者非同业单位客户基础信息.客户证件代码中存在
        tableCalibrationJS1914(checkParamContext);
    }

    /**
     * 发行人证件代码应该在同业客户基础信息.客户代码或者非同业单位客户基础信息.客户证件代码中存在
     * @param checkParamContext
     */
    private void tableCalibrationJS1914(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS1914")){
            String existsSql;
            boolean b = dataBaseTypeUtil.equalsOracle();
            if (b){
                existsSql=(String) SpringContextUtil.getBean("Xclzqtzxx.Ftkfora");
            }else{
                existsSql=(String) SpringContextUtil.getBean("Xclzqtzxx.Ftkf");
            }
            String format = String.format(existsSql,checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"发行人证件代码应该在同业客户基础信息.客户代码或者非同业单位客户基础信息.客户证件代码中存在");
        }
    }

    /**
     * 金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
     * @param checkParamContext
     */
    private void tableCalibrationJS1789(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS1789")){
            String existsSql= (String) SpringContextUtil.getBean("Xclzqtzxx.Jj");
            String format = String.format(existsSql,checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致");
        }
    }

    /**
     * 数据日期+金融机构代码+债券代码应唯一
     * @param checkParamContext
     */
    private void tableCalibrationJS2317(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS2317")){
            String existsSql;
            boolean b = dataBaseTypeUtil.equalsOracle();
            if (b){
                 existsSql= (String) SpringContextUtil.getBean("Xclzqtzxx.Sjzora");
            }else {
                 existsSql= (String) SpringContextUtil.getBean("Xclzqtzxx.Sjz");
            }

            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"数据日期+金融机构代码+债券代码应唯一");
        }
    }


}