package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.Xcldkxx;
import com.geping.etl.UNITLOAN.entity.report.Xclgrdkxx;
import com.geping.etl.UNITLOAN.entity.report.Xgrkhxx;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 16:07 2020/6/9
 */
public interface XclgrdkxxService {

    Xclgrdkxx save(Xclgrdkxx Xclgrdkxx);

    List<Xclgrdkxx> saveAll(List<Xclgrdkxx> list);

    Page<Xclgrdkxx> findAll(Specification specification, PageRequest pageRequest);

    int batchInsert(List<Xclgrdkxx> list);


}
