package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xcldkxx;
import com.geping.etl.UNITLOAN.entity.report.Xclwtdkxx;
import com.geping.etl.UNITLOAN.repository.report.XcldkxxRepository;
import com.geping.etl.UNITLOAN.repository.report.XclwtdkxxRepository;
import com.geping.etl.UNITLOAN.service.report.XcldkxxService;
import com.geping.etl.UNITLOAN.service.report.XclwtdkxxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 16:08 2020/6/9
 */
@Service
@Transactional
public class XclwtdkxxServiceImpl implements XclwtdkxxService {

    @Autowired
    private XclwtdkxxRepository xclwtdkxxRepository;

    @Override
    public Xclwtdkxx save(Xclwtdkxx xclwtdkxx) {
        return xclwtdkxxRepository.save(xclwtdkxx);
    }

    @Override
    public List<Xclwtdkxx> saveAll(List<Xclwtdkxx> list) {
        return xclwtdkxxRepository.save(list);
    }



    @Override
    public Page<Xclwtdkxx> findAll(Specification specification,PageRequest pageRequest){
        return xclwtdkxxRepository.findAll(specification,pageRequest);
    }


}
