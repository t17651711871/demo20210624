package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xcldkxx;
import com.geping.etl.UNITLOAN.entity.report.Xclgrdkxx;
import com.geping.etl.UNITLOAN.entity.report.Xgrkhxx;
import com.geping.etl.UNITLOAN.repository.report.XcldkxxRepository;
import com.geping.etl.UNITLOAN.repository.report.XclgrdkxxRepository;
import com.geping.etl.UNITLOAN.service.report.XcldkxxService;
import com.geping.etl.UNITLOAN.service.report.XclgrdkxxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * @Author: wangzd
 * @Date: 16:08 2020/6/9
 */
@Service
@Transactional
public class XclgrdkxxServiceImpl implements XclgrdkxxService {

    @Autowired
    private XclgrdkxxRepository xclgrdkxxRepository;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Override
    public Xclgrdkxx save(Xclgrdkxx xclgrdkxx) {
        return xclgrdkxxRepository.save(xclgrdkxx);
    }

    @Override
    public List<Xclgrdkxx> saveAll(List<Xclgrdkxx> list) {
        return xclgrdkxxRepository.save(list);
    }

    @Override
    public Page<Xclgrdkxx> findAll(Specification specification,PageRequest pageRequest){
        return xclgrdkxxRepository.findAll(specification,pageRequest);
    }

    @Override
    public int batchInsert(List<Xclgrdkxx> list) {
        int result = jdbcTemplate.batchUpdate("INSERT INTO xclgrdkxx (id,financeorgcode,financeorginnum,financeorgareacode,isfarmerloan,brroweridnum,brrowerareacode,receiptcode,contractcode,productcetegory,loanstartdate,loanenddate,extensiondate,currency,receiptbalance,receiptcnybalance,interestisfixed,interestislevel,fixpricetype,baseinterest,loanfinancesupport,loaninterestrepricedate,guaranteemethod,isplatformloan,loanquality,loanstatus,overduetype,issupportliveloan,sjrq,checkstatus,datastatus,operator,operationname,operationtime,orgid,departid,nopassreason) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setInt(1,list.get(i).getId());
                ps.setString(2,list.get(i).getFinanceorgcode());
                ps.setString(3,list.get(i).getFinanceorginnum());
                ps.setString(4,list.get(i).getFinanceorgareacode());
                ps.setString(5,list.get(i).getIsfarmerloan());
                ps.setString(6,list.get(i).getBrroweridnum());
                ps.setString(7,list.get(i).getBrrowerareacode());
                ps.setString(8,list.get(i).getReceiptcode());
                ps.setString(9,list.get(i).getContractcode());
                ps.setString(10,list.get(i).getProductcetegory());
                ps.setString(11,list.get(i).getLoanstartdate());
                ps.setString(12,list.get(i).getLoanenddate());
                ps.setString(13,list.get(i).getExtensiondate());
                ps.setString(14,list.get(i).getCurrency());
                ps.setString(15,list.get(i).getReceiptbalance());
                ps.setString(16,list.get(i).getReceiptcnybalance());
                ps.setString(17,list.get(i).getInterestisfixed());
                ps.setString(18,list.get(i).getInterestislevel());
                ps.setString(19,list.get(i).getFixpricetype());
                ps.setString(20,list.get(i).getBaseinterest());
                ps.setString(21,list.get(i).getLoanfinancesupport());
                ps.setString(22,list.get(i).getLoaninterestrepricedate());
                ps.setString(23,list.get(i).getGuaranteemethod());
                ps.setString(24,list.get(i).getIsplatformloan());
                ps.setString(25,list.get(i).getLoanquality());
                ps.setString(26,list.get(i).getLoanstatus());
                ps.setString(27,list.get(i).getOverduetype());
                ps.setString(28,list.get(i).getIssupportliveloan());
                ps.setString(29,list.get(i).getSjrq());
                ps.setString(30,list.get(i).getCheckstatus());
                ps.setString(31,list.get(i).getDatastatus());
                ps.setString(32,list.get(i).getOperator());
                ps.setString(33,list.get(i).getOperationname());
                ps.setString(34,list.get(i).getOperationtime());
                ps.setString(35,list.get(i).getOrgid());
                ps.setString(36,list.get(i).getDepartid());
                ps.setString(37,list.get(i).getNopassreason());
            }
            @Override
            public int getBatchSize() {
                return list.size();
            }
        }).length;
        return result;
    }
}
