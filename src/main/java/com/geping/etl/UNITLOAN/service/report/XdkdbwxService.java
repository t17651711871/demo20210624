package com.geping.etl.UNITLOAN.service.report;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.report.Xdkdbwx;

public interface XdkdbwxService {

	//查询指定客户
	public Xdkdbwx findOne(String id);
	//查询所有
	public List<Xdkdbwx> findAll();
	//查询所有带条件
	public List<Xdkdbwx> findAll(Specification<Xdkdbwx> spec);
	//查询所有带条件并分页
	public Page<Xdkdbwx> findAll(Specification<Xdkdbwx> spec,Pageable page);
	//保存单个实体
	public Xdkdbwx Save(Xdkdbwx Xdkdbwx);
	//批量保存实体
	public Integer Save(List<Xdkdbwx> list);
	//保存或更新实体
	public Xdkdbwx SaveOrUpdate(Xdkdbwx Xdkdbwx);
	//批量更新校验状态
	public Integer updateXdkdbwxOnCheckstatus(String checkStatus);
	//批量更新校验状态
	public Integer updateXdkdbwxOnCheckstatus(String checkStatus,List<String> idList);
	//批量更新审核状态
	public Integer updateXdkdbwxOnDatastatus(String dataStatus,String user,String departId);
	//批量更新审核状态
	public Integer updateXdkdbwxOnDatastatus(String dataStatus,String user,List<String> idList);
	//删除所有
	public Integer delete();
	//删除单个实体
	public Integer delete(String id);
	//批量删除实体
	public Integer delete(List<String> list);
}
