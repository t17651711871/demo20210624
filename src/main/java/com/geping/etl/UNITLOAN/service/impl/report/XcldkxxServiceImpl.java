package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xcldkxx;
import com.geping.etl.UNITLOAN.repository.report.XcldkxxRepository;
import com.geping.etl.UNITLOAN.service.report.XcldkxxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 16:08 2020/6/9
 */
@Service
@Transactional
public class XcldkxxServiceImpl implements XcldkxxService {

    @Autowired
    private XcldkxxRepository xcldkxxRepository;

    @Override
    public Xcldkxx save(Xcldkxx xcldkxx) {
        return xcldkxxRepository.save(xcldkxx);
    }

    @Override
    public List<Xcldkxx> saveAll(List<Xcldkxx> list) {
        return xcldkxxRepository.save(list);
    }

    @Override
    public Page<Xcldkxx> findAll(Specification specification,PageRequest pageRequest){
        return xcldkxxRepository.findAll(specification,pageRequest);
    }


}
