package com.geping.etl.UNITLOAN.service.impl.report;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.report.Xgrkhxx;
import com.geping.etl.UNITLOAN.repository.report.XgrkhxxRepository;
import com.geping.etl.UNITLOAN.service.report.XgrkhxxService;

/**    
*  
* @author liuweixin  
* @date 2020年11月3日 下午2:22:38  
*/
@Service
@Transactional
public class XgrkhxxServiceImpl implements XgrkhxxService{

	@Autowired
    private XgrkhxxRepository xgrkhxxRepository;
	@Autowired
	private JdbcTemplate jdbcTemplate;

    @Override
    public Xgrkhxx save(Xgrkhxx Xgrkhxx) {
        return xgrkhxxRepository.save(Xgrkhxx);
    }

    @Override
    public List<Xgrkhxx> saveAll(List<Xgrkhxx> list) {
        return xgrkhxxRepository.save(list);
    }

    @Override
    public Page<Xgrkhxx> findAll(Specification specification,PageRequest pageRequest){
        return xgrkhxxRepository.findAll(specification,pageRequest);
    }

	@Override
	public int batchInsert(List<Xgrkhxx> list) {
		int result = jdbcTemplate.batchUpdate("INSERT INTO xgrkhxx (id,finorgcode,regamtcreny,customercode,country,nation,sex,education,birthday,regareacode,grincome,familyincome,marriage,isrelation,creditamt,usedamt,grkhsfbs,gtgshyyzzdm,xwqyshtyxydm,workareacode,actctrltype,checkstatus,datastatus,operator,operationname,operationtime,orgid,departid,nopassreason,sjrq) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
            	ps.setInt(1,list.get(i).getId());
            	ps.setString(2,list.get(i).getFinorgcode());
            	ps.setString(3,list.get(i).getRegamtcreny());
            	ps.setString(4,list.get(i).getCustomercode());
            	ps.setString(5,list.get(i).getCountry());
            	ps.setString(6,list.get(i).getNation());
            	ps.setString(7,list.get(i).getSex());
            	ps.setString(8,list.get(i).getEducation());
            	ps.setString(9,list.get(i).getBirthday());
            	ps.setString(10,list.get(i).getRegareacode());
            	ps.setString(11,list.get(i).getGrincome());
            	ps.setString(12,list.get(i).getFamilyincome());
            	ps.setString(13,list.get(i).getMarriage());
            	ps.setString(14,list.get(i).getIsrelation());
            	ps.setString(15,list.get(i).getCreditamt());
            	ps.setString(16,list.get(i).getUsedamt());
            	ps.setString(17,list.get(i).getGrkhsfbs());
            	ps.setString(18,list.get(i).getGtgshyyzzdm());
            	ps.setString(19,list.get(i).getXwqyshtyxydm());
            	ps.setString(20,list.get(i).getWorkareacode());
            	ps.setString(21,list.get(i).getActctrltype());
            	ps.setString(22,list.get(i).getCheckstatus());
            	ps.setString(23,list.get(i).getDatastatus());
            	ps.setString(24,list.get(i).getOperator());
            	ps.setString(25,list.get(i).getOperationname());
            	ps.setString(26,list.get(i).getOperationtime());
            	ps.setString(27,list.get(i).getOrgid());
            	ps.setString(28,list.get(i).getDepartid());
            	ps.setString(29,list.get(i).getNopassreason());
            	ps.setString(30,list.get(i).getSjrq());
            }
            @Override
            public int getBatchSize() {
                return list.size();
            }
        }).length;
        return result;
	}
}
