package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.Xclzqfxxx;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.service.report
 * @USER: tangshuai
 * @DATE: 2021/3/30
 * @TIME: 15:40
 * @描述:
 */
public interface XclzqfxxxService {

    public Page<Xclzqfxxx> findAll(Specification specification, PageRequest pageRequest);
}
