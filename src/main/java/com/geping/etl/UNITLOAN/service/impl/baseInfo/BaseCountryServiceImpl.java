package com.geping.etl.UNITLOAN.service.impl.baseInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.repository.baseInfo.BaseCountryRepository;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseCountryService;
@Service
@Transactional
public class BaseCountryServiceImpl implements BaseCountryService{

	@Autowired
	private BaseCountryRepository bcr;

	@Override
	public List<BaseCountry> findAll() {
		return bcr.findAll();
	}

	@Override
	public List<BaseCountry> findAll(Specification<BaseCountry> spec) {
		return bcr.findAll(spec);
	}

	@Override
	public Page<BaseCountry> findAll(Specification<BaseCountry> spec, Pageable page) {
		return bcr.findAll(spec, page);
	}

	@Override
	public void daoRuFuGai(List<BaseCountry> list) {
		bcr.deleteAll();
		bcr.save(list);
	}
}
