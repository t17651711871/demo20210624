package com.geping.etl.UNITLOAN.service.baseInfo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;

public interface BaseAreaService {
	
	//查询所有
	public List<BaseArea> findAll();
	//查询所有带条件
	public List<BaseArea> findAll(Specification<BaseArea> spec);
	//查询所有带条件并分页
	public Page<BaseArea> findAll(Specification<BaseArea> spec,Pageable page);
	//导入覆盖
	public void daoRuFuGai(List<BaseArea> list);
}
