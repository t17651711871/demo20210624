package com.geping.etl.UNITLOAN.service.impl.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.report.Xcltyjdxx;
import com.geping.etl.UNITLOAN.repository.report.XcltyjdxxRepository;
import com.geping.etl.UNITLOAN.service.report.XcltyjdxxService;

/**    
*  
* @author liuweixin  
* @date 2021年1月6日 上午10:16:33  
*/
@Service
@Transactional
public class XcltyjdxxServiceImpl implements XcltyjdxxService {
	@Autowired
	private XcltyjdxxRepository xcltyjdxxRepository;

	@Override
	public Xcltyjdxx save(Xcltyjdxx cltyjdxx) {
		return xcltyjdxxRepository.save(cltyjdxx);
	}

	@Override
	public List<Xcltyjdxx> saveAll(List<Xcltyjdxx> list) {
		return xcltyjdxxRepository.save(list);
	}

	@Override
	public Page<Xcltyjdxx> findAll(Specification specification, PageRequest pageRequest) {
		return xcltyjdxxRepository.findAll(specification, pageRequest);
	}

}
