package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.Xftykhxbl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 2020/6/30
 */
public interface XftykhxblService {
    Xftykhxbl save(Xftykhxbl xftykhx);

    List<Xftykhxbl> saveAll(List<Xftykhxbl> list);

    Page<Xftykhxbl> findAll(Specification specification, PageRequest pageRequest);

    List<Xftykhxbl> getInfoByCustomernum(String customernum, String orgId, String departId);

    List<Xftykhxbl> findAll();
}
