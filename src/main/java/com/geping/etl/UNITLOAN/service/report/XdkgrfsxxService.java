package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.Xclgrdkxx;
import com.geping.etl.UNITLOAN.entity.report.Xdkfsxx;
import com.geping.etl.UNITLOAN.entity.report.Xgrdkfsxx;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author: chenggen
 * @Date: 2020/11/06
 */
public interface XdkgrfsxxService {
    Xgrdkfsxx save(Xgrdkfsxx Xgrdkfsxx);

    List<Xgrdkfsxx> saveAll(List<Xgrdkfsxx> list);

    Page<Xgrdkfsxx> findAll(Specification specification, PageRequest pageRequest);

    int batchInsert(List<Xgrdkfsxx> list);
}
