package com.geping.etl.UNITLOAN.service.baseInfo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;

public interface BaseAindustryService {
	
	//查询所有
	public List<BaseAindustry> findAll();
	//查询所有带条件
	public List<BaseAindustry> findAll(Specification<BaseAindustry> spec);
	//查询所有带条件并分页
	public Page<BaseAindustry> findAll(Specification<BaseAindustry> spec,Pageable page);
	//导入覆盖
	public void daoRuFuGai(List<BaseAindustry> list);
}
