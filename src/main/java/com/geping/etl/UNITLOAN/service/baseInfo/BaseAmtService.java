package com.geping.etl.UNITLOAN.service.baseInfo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAmt;

public interface BaseAmtService {
	
	//查询所有
	public List<BaseAmt> findAll();
	//查询所有带条件
	public List<BaseAmt> findAll(Specification<BaseAmt> spec);
	//查询所有带条件并分页
	public Page<BaseAmt> findAll(Specification<BaseAmt> spec,Pageable page);
	//查询数量
	public Integer findTotal();
}
