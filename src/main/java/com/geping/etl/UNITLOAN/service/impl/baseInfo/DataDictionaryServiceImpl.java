package com.geping.etl.UNITLOAN.service.impl.baseInfo;

import com.geping.etl.UNITLOAN.entity.baseInfo.DataDictionary;
import com.geping.etl.UNITLOAN.repository.baseInfo.DataDictionaryRepository;
import com.geping.etl.UNITLOAN.service.baseInfo.DataDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataDictionaryServiceImpl implements DataDictionaryService {

    @Autowired
    private DataDictionaryRepository   dataDictionaryRepository;

    @Override
    public List<DataDictionary> findAll(Integer type) {
        Specification specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("type"),type));
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
        Sort sort = new Sort(Sort.Direction.ASC, "px");
        return  dataDictionaryRepository.findAll(specification,sort);
    }

    @Override
    public Map<String, String> getKv(Integer type) {
        List<DataDictionary>typeData=findAll(type);
        Map<String,String>kv=new HashMap<>();
        typeData.forEach(item->{
            kv.put(item.getCode(),item.getText());
        });
        return kv;
    }
}