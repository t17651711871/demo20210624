package com.geping.etl.UNITLOAN.service.report;

import java.util.List;
import java.util.Map;

import com.geping.etl.common.entity.Report_Info;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;

public interface XcommonService<T> {
   <T> void save(T t);

   void updateReportInfo(String tableName);

   void deleteDataAll(String className, Map<String, String> param, String handlename, String orgId, String datastatus, String departId);

   void deleteDataById(String className, String ids);

   List<T> findByFielOut(Map<String, String> param, Class clazz);

   void noCheckStatus(String noId, String handlename, String handledate, String tableName);

   void yesCheckStatus(String yesId, String handlename, String handledate, String tableName);

   <T> int batchInsert(List<T> list, String tableName, String handlename, String handletime, String departid);

   void applyDataById(String className, String ids, String handlename, String handledate, String type);

   void applyDataAll(String className, Map<String, String> param, String handlename, String orgId, String datastatus, String handledate, String type, String departId);

    void agreeApplyDataById(String className, String ids, String handlename, String handledate);

   void agreeApplyDataAll(String className, Map<String, String> param, String handlename, String orgId, String handledate, String departId, boolean shifoushenheziji);

    void noAgreeApplyDataById(String className, String ids, String handlename, String handledate);

   void noAgreeApplyDataAll(String className, Map<String, String> param, String handlename, String orgId, String handledate, String departId, boolean shifoushenheziji);

    void agreeAuditDataById(String className, String ids, String handlename, String handledate);

    void agreeAuditDataAll(String className, Map<String, String> param, String handlename, String orgId, String handledate, String departId, boolean shifoushenheziji);

    void noAgreeAuditDataById(String className, String ids, String handlename, String handledate, String reason);

    void noAgreeAuditDataAll(String className, Map<String, String> param, String handlename, String orgId, String handledate, String reason, String departId, boolean shifoushenheziji);

    List<String> findByFileNamePre(String preName, String orgCode, String className);

    List<String> findByFileNamePreWithT(String ttFilePreName, String orgCode, String className);

    void submitDataById(String className, String ids, String handlename, String handledate);

    void submitDataAll(String className, Map<String, String> param, String handlename, String orgId, String datastatus, String departId, String handledate);

    List<Map<String, String>> getHistoryInfo(String orgid, String departId,String operationtime);

    List<Report_Info> getReportHistoryInfo(String orgid, String departId, String operationtime);

    Map<String, String> getDtjDshCount(String orgid, String departId);

    Map<String, String> getdscbwCount(String orgid, String departId);

    void moveToHistoryTable(String tableName, String orgid, String departId);
    public void moveToHistoryTable(Sys_UserAndOrgDepartment user,String... tables);

    void gobackDataById(String className, String id, String handlename, String handledate);

    void ngobackDataAll(String className, Map<String, String> param, String handlename, String orgId, String handledate, String departId);

    void deleteRepeatData(String className, String field);

    void synDataById(String className, String id);

    void synDataAll(String className, Map<String, String> param, String handlename, String orgId, String departId);

    void updateXcldkxxByCustomernum(String className);

    void deleteDataAllWhithOutDatastatus(String className, Map<String, String> param, String handlename, String orgId, String departId);

	void updateXdkfsxxByCustomernum(String className);

	int moveToTemporaryTable(String tableName, String handlename, String handledate, String date, String orgId,
			String departId);
	
	void importDelete(String tableName);
	
	void importDelete(String tableName,String departId);
	
	int getMaxId(String className);
}
