package com.geping.etl.UNITLOAN.service.check.processors.clzqfxxx;


import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.entity.report.Xclzqfxxx;
import com.geping.etl.UNITLOAN.service.check.processors.CheckProcessorTemplateService;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.XDataBaseTypeUtil;
import com.geping.etl.UNITLOAN.util.check.CheckXclzqfxxxRowData;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/***
 *   存量债券发行信息 报表校验处理器
 * @author liang.xu
 * @date 2021.4.10
 */
@Service
public class XclzqfxxxCheckProcessorServiceImpl extends CheckProcessorTemplateService {
    @Autowired
    private DepartUtil departUtil;
    @Autowired
    private CheckHelper checkHelper;
    @Autowired
    private CheckXclzqfxxxRowData checkRow;
    @Autowired
    private XDataBaseTypeUtil dataBaseTypeUtil;
    @Override
    public String getKey() {
        return Xclzqfxxx.class.getSimpleName();
    }

    /**
     * 1.准备SQL
     * @param checkParamContext
     */
    @Override
    protected void prepareSQl(CheckParamContext checkParamContext) {
        String departId = departUtil.getDepart(checkParamContext.getSysUser());
        String whereStr=" where a.datastatus='0' and a.checkstatus='0'";
        String id=checkParamContext.getId();
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            System.out.println(id);
            whereStr = whereStr + " and a.id in(" + id + ")";
        }
        whereStr = whereStr + " and a.departid like '%"+ departId +"%'";
        String sql="select * from xclzqfxxx a"+whereStr;
        String sqlcount="select count(*) from xclzqfxxx a"+whereStr;
        checkParamContext.setCountsql(sqlcount);
        checkParamContext.setSql(sql);
        checkParamContext.setWhereCond(whereStr);
    }

    /**
     * 2.准备错误头
     * @return
     */
    @Override
    protected String getHeaderTpl() {
        return  "金融机构代码"+ SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.COMMA+"债券代码"+SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.ERROR_TIP_SPLIT;
    }

    /**
     * 3.校验行
     * @param checkParamContext
     * @param t
     */
    @Override
    protected void checkRow(CheckParamContext checkParamContext,Object t) {
        Xclzqfxxx xclzqfxxx= (Xclzqfxxx) t;
        checkRow.checkRow(getHeaderTpl(),checkParamContext,xclzqfxxx);
    }

    //4.标间校验
    @Override
    protected void tableCalibration(CheckParamContext checkParamContext) {
        //数据日期+金融机构代码+债券代码应唯一
        tableCalibrationJS2319(checkParamContext);
    }


    /**
     * 数据日期+金融机构代码+债券代码应唯一
     * @param  checkParamContext
     */
    private void tableCalibrationJS2319(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS2319")){
            String existsSql;
            boolean b = dataBaseTypeUtil.equalsOracle();
            if (b){
                existsSql= (String) SpringContextUtil.getBean("Xclzqfxxx.Sjzora");
            }else {
                existsSql= (String) SpringContextUtil.getBean("Xclzqfxxx.Sjz");
            }
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"数据日期+金融机构代码+债券代码应唯一");
        }
    }
}