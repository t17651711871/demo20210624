package com.geping.etl.UNITLOAN.service.report;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.report.Xcltyckxx;

/**    
*  
* @author liuweixin  
* @date 2021年1月6日 上午10:13:37  
*/
public interface XcltyckxxService {

	Xcltyckxx save(Xcltyckxx cltyckxx);

    List<Xcltyckxx> saveAll(List<Xcltyckxx> list);

    Page<Xcltyckxx> findAll(Specification specification, PageRequest pageRequest);
}
