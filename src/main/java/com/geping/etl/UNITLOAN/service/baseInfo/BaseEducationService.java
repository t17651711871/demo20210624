package com.geping.etl.UNITLOAN.service.baseInfo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseEducation;

/**    
*  
* @author liuweixin  
* @date 2020年11月4日 下午3:50:21  
*/
public interface BaseEducationService {

	//查询所有
			public List<BaseEducation> findAll();
			//查询所有带条件
			public List<BaseEducation> findAll(Specification<BaseEducation> spec);
			//查询所有带条件并分页
			public Page<BaseEducation> findAll(Specification<BaseEducation> spec,Pageable page);
			//导入覆盖
			public void daoRuFuGai(List<BaseEducation> list);
}
