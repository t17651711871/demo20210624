package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.XreportInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

public interface XreportInfoService {
    Page<XreportInfo> findAll(Specification specification, PageRequest pageRequest);

    XreportInfo getById(String id);
}
