package com.geping.etl.UNITLOAN.service.report;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.report.Xcltyjdxx;

/**    
*  
* @author liuweixin  
* @date 2021年1月6日 上午10:13:37  
*/
public interface XcltyjdxxService {

	Xcltyjdxx save(Xcltyjdxx cltyjdxx);

    List<Xcltyjdxx> saveAll(List<Xcltyjdxx> list);

    Page<Xcltyjdxx> findAll(Specification specification, PageRequest pageRequest);
}
