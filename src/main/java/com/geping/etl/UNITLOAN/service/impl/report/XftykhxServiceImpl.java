package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xftykhx;
import com.geping.etl.UNITLOAN.repository.report.XftykhxRepository;
import com.geping.etl.UNITLOAN.service.report.XftykhxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 16:08 2020/6/9
 */
@Service
@Transactional
public class XftykhxServiceImpl implements XftykhxService {

    @Autowired
    private XftykhxRepository xftykhxRepository;

    @Override
    public Xftykhx save(Xftykhx xftykhx) {
        return xftykhxRepository.save(xftykhx);
    }

    @Override
    public List<Xftykhx> saveAll(List<Xftykhx> list) {
        return xftykhxRepository.save(list);
    }

    @Override
    public Page<Xftykhx> findAll(Specification specification,PageRequest pageRequest){
        return xftykhxRepository.findAll(specification,pageRequest);
    }
}
