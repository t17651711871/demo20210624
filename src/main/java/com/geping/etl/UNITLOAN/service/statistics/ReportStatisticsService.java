package com.geping.etl.UNITLOAN.service.statistics;

import com.geping.etl.common.entity.Report_Info;

import java.util.List;

/***
 * 报表统计
 * @author liang.xu
 * @date 2021.4.9
 */
public interface ReportStatisticsService {

    /**
     * 统计报表
     *
     * @param reports
     */
    void reportStatistics(String orgid, String departId,List<Report_Info> reports);

    /**
     * 统计已审核报表
     * @param orgid
     * @param departId
     * @param reports
     */
    void yshReportStatistics(String orgid, String departId,List<Report_Info> reports);
}