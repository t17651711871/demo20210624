package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xclzqfxxx;
import com.geping.etl.UNITLOAN.repository.report.XclzqfxxxRepository;
import com.geping.etl.UNITLOAN.service.report.XclzqfxxxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.service.impl.report
 * @USER: tangshuai
 * @DATE: 2021/3/30
 * @TIME: 15:41
 * @描述:
 */
@Service
@Transactional
public class XclzqfxxxServiceImpl implements XclzqfxxxService {

    @Autowired
    private XclzqfxxxRepository xclzqfxxxRepository;

    @Override
    public Page<Xclzqfxxx> findAll(Specification specification, PageRequest pageRequest) {
        return xclzqfxxxRepository.findAll(specification,pageRequest);
    }
}
