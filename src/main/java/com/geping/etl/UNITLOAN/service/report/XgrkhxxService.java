package com.geping.etl.UNITLOAN.service.report;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.report.Xgrkhxx;

/**    
*  
* @author liuweixin  
* @date 2020年11月3日 下午2:19:36  
*/
public interface XgrkhxxService {

	Xgrkhxx save(Xgrkhxx xgrkhxx);

    List<Xgrkhxx> saveAll(List<Xgrkhxx> list);

    Page<Xgrkhxx> findAll(Specification specification, PageRequest pageRequest);
    
    int batchInsert(List<Xgrkhxx> list);
    
}
