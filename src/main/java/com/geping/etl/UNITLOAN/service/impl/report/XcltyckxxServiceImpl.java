package com.geping.etl.UNITLOAN.service.impl.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.report.Xcltyckxx;
import com.geping.etl.UNITLOAN.repository.report.XcltyckxxRepository;
import com.geping.etl.UNITLOAN.service.report.XcltyckxxService;

/**    
*  
* @author liuweixin  
* @date 2021年1月6日 上午10:16:33  
*/
@Service
@Transactional
public class XcltyckxxServiceImpl implements XcltyckxxService {
	@Autowired
	private XcltyckxxRepository xcltyckxxRepository;

	@Override
	public Xcltyckxx save(Xcltyckxx cltyckxx) {
		return xcltyckxxRepository.save(cltyckxx);
	}

	@Override
	public List<Xcltyckxx> saveAll(List<Xcltyckxx> list) {
		return xcltyckxxRepository.save(list);
	}

	@Override
	public Page<Xcltyckxx> findAll(Specification specification, PageRequest pageRequest) {
		return xcltyckxxRepository.findAll(specification, pageRequest);
	}

}
