package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.Xcldkxx;
import com.geping.etl.UNITLOAN.entity.report.Xclwtdkxx;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 16:07 2020/6/9
 */
public interface XclwtdkxxService {

    Xclwtdkxx save(Xclwtdkxx xclwtdkxx);

    List<Xclwtdkxx> saveAll(List<Xclwtdkxx> list);

    Page<Xclwtdkxx> findAll(Specification specification, PageRequest pageRequest);


}
