package com.geping.etl.UNITLOAN.service.impl.baseInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAmt;
import com.geping.etl.UNITLOAN.repository.baseInfo.BaseAmtRepository;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseAmtService;
@Service
@Transactional
public class BaseAmtServiceImpl implements BaseAmtService{

	@Autowired
	private BaseAmtRepository bar;
	
	@Override
	public List<BaseAmt> findAll() {
		return bar.findAll();
	}

	@Override
	public List<BaseAmt> findAll(Specification<BaseAmt> spec) {
		return bar.findAll(spec);
	}

	@Override
	public Page<BaseAmt> findAll(Specification<BaseAmt> spec, Pageable page) {
		return bar.findAll(spec, page);
	}

	@Override
	public Integer findTotal() {
		return bar.findTotal();
	}

}
