package com.geping.etl.UNITLOAN.service.report;

import com.geping.etl.UNITLOAN.entity.report.Xclgqtzxx;
import com.geping.etl.UNITLOAN.entity.report.Xgqtzfsexx;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.service.report
 * @USER: tangshuai
 * @DATE: 2021/5/17
 * @TIME: 16:42
 * @描述:
 */
public interface XgqtzfsexxService {
    Page<Xgqtzfsexx> findAll(Specification specification, PageRequest pageRequest);
}
