package com.geping.etl.UNITLOAN.service.impl.baseInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseEducation;
import com.geping.etl.UNITLOAN.repository.baseInfo.BaseEducationRepository;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseEducationService;

/**    
*  
* @author liuweixin  
* @date 2020年11月4日 下午3:53:07  
*/
@Service
@Transactional
public class BaseEducationServiceImpl implements BaseEducationService{

	@Autowired
	private BaseEducationRepository ber;
	
	@Override
	public List<BaseEducation> findAll() {
		return ber.getAll();
	}

	@Override
	public List<BaseEducation> findAll(Specification<BaseEducation> spec) {
		return ber.findAll(spec);
	}

	@Override
	public Page<BaseEducation> findAll(Specification<BaseEducation> spec, Pageable page) {
		return ber.findAll(spec, page);
	}

	@Override
	public void daoRuFuGai(List<BaseEducation> list) {
		ber.deleteAll();
		ber.save(list);
		
	}

	
}
