package com.geping.etl.UNITLOAN.service.check.processors.tdtzxx;


import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.report.Xtdmdzttzfsexx;
import com.geping.etl.UNITLOAN.enums.DataTypeEnum;
import com.geping.etl.UNITLOAN.service.check.processors.CheckProcessorTemplateService;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.EnhanceDataDictionary;
import com.geping.etl.UNITLOAN.util.XDataBaseTypeUtil;
import com.geping.etl.UNITLOAN.util.check.CheckXtdmdzttzfsexxRowData;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 债券投资发生额信息
 */

@Service
public class XtdmdzttzfsexxCheckProcessorServiceImpl extends CheckProcessorTemplateService {
    @Autowired
    private DepartUtil departUtil;
    @Autowired
    private CheckHelper checkHelper;
    @Autowired
    private XDataBaseTypeUtil dataBaseTypeUtil;
    @Autowired
    private CheckXtdmdzttzfsexxRowData checkXtdmdzttzfsexxRowData;
    @Autowired
    private EnhanceDataDictionary enhanceDataDictionary;
    @Autowired
    private CustomSqlUtil customSqlUtil;

    /**
     * 获取实体类
     * @return
     */
    @Override
    public String getKey() {
        return Xtdmdzttzfsexx.class.getSimpleName();
    }

    /**
     *获取准备SQL
     * @param checkParamContext
     */
    @Override
    protected void prepareSQl(CheckParamContext checkParamContext) {
        String whereStr=" where a.datastatus='0' and a.checkstatus='0'";
        String departId = departUtil.getDepart(checkParamContext.getSysUser());
        String id=checkParamContext.getId();
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            System.out.println(id);
            whereStr = whereStr + " and a.id in(" + id + ")";
        }
        whereStr = whereStr + " and a.departid like '%"+ departId +"%'";
        String sql="select * from xtdmdzttzfsexx a"+whereStr;
        String sqlcount="select count(*) from xtdmdzttzfsexx a"+whereStr;
        checkParamContext.setCountsql(sqlcount);
        checkParamContext.setSql(sql);
        checkParamContext.setWhereCond(whereStr);
    }

    /**
     * 获取错误头
     * @return
     */
    @Override
    protected String getHeaderTpl() {
        return  "金融机构代码"+ SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.COMMA+"特定目的载体代码"+SysConstants.SEMICOLON+SysConstants.PERCENTS+SysConstants.ERROR_TIP_SPLIT;
    }

    /**
     * 3.校验行
     * @param checkParamContext
     * @param t
     */
    @Override
    protected void checkRow(CheckParamContext checkParamContext,Object t) {
        Xtdmdzttzfsexx xtdmdzttzfsexx= (Xtdmdzttzfsexx) t;
        checkXtdmdzttzfsexxRowData.checkRow(getHeaderTpl(),checkParamContext,xtdmdzttzfsexx);
    }

    /**
     * 表间校验
     * @param checkParamContext
     */
    @Override
    protected void tableCalibration(CheckParamContext checkParamContext) {
        //数据日期+金融机构代码+特定目的载体代码+交易日期+交易方向应唯一
        tableCalibrationJS2323(checkParamContext);
        //特定目的载体类型与存量特定目的载体投资信息的特定目的载体类型应该一致
        tableCalibrationJS1761(checkParamContext);
        //当特定目的载体代码不为空时，特定目的载体代码与存量特定目的载体投资信息的特定目的载体代码应该一致
        tableCalibrationJS1762(checkParamContext);
        //发行人代码与存量特定目的载体投资信息的发行人代码应该一致
        tableCalibrationJS1763(checkParamContext);
        //发行人代码应该在同业客户基础信息.客户代码中存在
        tableCalibrationJS1919(checkParamContext);
        //金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
        tableCalibrationJS1805(checkParamContext);
    }
    private void tableCalibrationJS1805(CheckParamContext checkParamContext) {
        if (checkParamContext.getCheckNums().contains("JS1781")) {
            String existsSql = (String) SpringContextUtil.getBean("Xtdmdzttzfsexx.Jj");
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(), checkParamContext, format, "金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致");
        }
    }
    private void tableCalibrationJS1919(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS1919")){
            String existsSql;
            boolean b = dataBaseTypeUtil.equalsOracle();
            if (b){
                existsSql=(String) SpringContextUtil.getBean("Xtdmdzttzfsexx.Ftkfora");
            }else{
                existsSql=(String) SpringContextUtil.getBean("Xtdmdzttzfsexx.Ftkf");
            }
            String format = String.format(existsSql,checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"发行人代码应该在同业客户基础信息.客户代码中存在");
        }
    }

    private void tableCalibrationJS1763(CheckParamContext checkParamContext) {
        if (checkParamContext.getCheckNums().contains("JS1763")) {
            String existsSql = (String) SpringContextUtil.getBean("Xtdmdzttzfsexx.Fxrdm");
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(), checkParamContext, format, "发行人代码与存量特定目的载体投资信息的发行人代码应该一致");
        }
    }

    private void tableCalibrationJS1762(CheckParamContext checkParamContext) {
        if (checkParamContext.getCheckNums().contains("JS1762")) {
            String existsSql = (String) SpringContextUtil.getBean("Xtdmdzttzfsexx.Ztdm");
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(), checkParamContext, format, "当特定目的载体代码不为空时，特定目的载体代码与存量特定目的载体投资信息的特定目的载体代码应该一致");
        }
    }

    private void tableCalibrationJS1761(CheckParamContext checkParamContext) {
        if (checkParamContext.getCheckNums().contains("JS1761")) {
            String existsSql = (String) SpringContextUtil.getBean("Xtdmdzttzfsexx.Ztlx");
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(), checkParamContext, format, "特定目的载体代码与存量特定目的载体投资信息的特定目的载体类型应该一致");
        }
    }

    private void tableCalibrationJS2323(CheckParamContext checkParamContext) {
        if(checkParamContext.getCheckNums().contains("JS2324")){
            String existsSql;
            boolean b = dataBaseTypeUtil.equalsOracle();
            if (b){
                existsSql= (String) SpringContextUtil.getBean("Xtdmdzttzfsexx.Sjpora");
            }else {
                existsSql= (String) SpringContextUtil.getBean("Xtdmdzttzfsexx.Sjp");
            }
            String format = String.format(existsSql, checkParamContext.getWhereCond());
            checkHelper.inTableCalibration(getHeaderTpl(),checkParamContext,format,"数据日期+金融机构代码+特定目的载体代码+交易日期+交易方向应唯一");
        }
    }

    @Override
    protected  void enhanceCheckParamContext(CheckParamContext checkParamContext){
        //股权信息
        List<String> ztlxList = enhanceDataDictionary.findCode(DataTypeEnum.ZTLX.getType());
        //交易方向
        List<String> jyfxList = enhanceDataDictionary.findCode(DataTypeEnum.JYFX.getType());
        //机构类型
        List<String> yxfsList = enhanceDataDictionary.findCode(DataTypeEnum.YXFS.getType());
        //币种
        List baseCode = customSqlUtil.getBaseCode(BaseCurrency.class);
        //国家地区代码C0013
        List baseCodeGj = customSqlUtil.getBaseCode(BaseCountry.class);
        //行政区划代码C0009
        List baseCodeXz = customSqlUtil.getBaseCode(BaseArea.class);
        Map<String,List> map = new HashMap<>();
        map.put("jyfxList",jyfxList);
        map.put("ztlxList",ztlxList);
        map.put("yxfsList",yxfsList);
        map.put("baseCode",baseCode);
        map.put("baseCodeGj",baseCodeGj);
        map.put("baseCodeXz",baseCodeXz);
        checkParamContext.setDataDictionarymap(map);
    }
}