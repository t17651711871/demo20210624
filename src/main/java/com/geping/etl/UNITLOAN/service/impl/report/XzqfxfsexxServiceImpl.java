package com.geping.etl.UNITLOAN.service.impl.report;

import com.geping.etl.UNITLOAN.entity.report.Xzqfxfsexx;
import com.geping.etl.UNITLOAN.entity.report.Xzqtzfsexx;
import com.geping.etl.UNITLOAN.repository.report.XclzqfxxxRepository;
import com.geping.etl.UNITLOAN.repository.report.XzqfxfsexxRepository;
import com.geping.etl.UNITLOAN.service.report.XzqfxfsexxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.service.impl.report
 * @USER: tangshuai
 * @DATE: 2021/3/30
 * @TIME: 16:17
 * @描述:
 */
@Service
@Transactional
public class XzqfxfsexxServiceImpl implements XzqfxfsexxService {
    @Autowired
    private XzqfxfsexxRepository xzqfxfsexxRepository;

    @Override
    public Page<Xzqfxfsexx> findAll(Specification specification, PageRequest pageRequest) {
        return xzqfxfsexxRepository.findAll(specification,pageRequest);
    }
}