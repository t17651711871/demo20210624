package com.geping.etl.UNITLOAN.service.impl.statistics;


import com.geping.etl.UNITLOAN.common.report.ReportInfo;
import com.geping.etl.UNITLOAN.service.statistics.ReportStatisticsService;
import com.geping.etl.UNITLOAN.util.ReflectionUtils;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.common.entity.Report_Info;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class ReportStatisticsServiceImpl  implements ReportStatisticsService {


    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public void reportStatistics(String orgid, String departId,List<Report_Info> reports) {
        dtjReportStatistics(orgid,departId,reports);
        dshReportStatistics(orgid,departId,reports);
        yshReportStatistics(orgid,departId,reports);
    }

    private void dtjReportStatistics(String orgid, String departId,List<Report_Info> reports){
        StringBuilder sqldtj = new StringBuilder("");
        int flag=0;
        Report_Info jrjgfrReport=null;
        for(int i=0;i<reports.size()-1;i++) {
            if(ReportInfo.getReport(reports.get(i).getCode())==null){
                flag=i;
                jrjgfrReport=reports.get(i);
                continue;
            }
            sqldtj.append("select count(1) from "+ ReflectionUtils.getTableName(ReportInfo.getReport(reports.get(i).getCode())) +" where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' union all  ");
        }
        String code=reports.get(reports.size()-1).getCode();
        sqldtj.append(" select count(1) from "+ReflectionUtils.getTableName(ReportInfo.getReport(code))+" where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0' ");
        List<Object> resultdtj = entityManager.createNativeQuery(sqldtj.toString()).getResultList();
        resultdtj.add(flag,0);
        for(int i=0;i<reports.size();i++) {
            reports.get(i).setPending(String.valueOf(resultdtj.get(i)));
        }
        //处理机构机构法人报表
        String sqldtj7_1 = "select count(1) from XjrjgfrAssets where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0'";
        String sqldtj7_2 = "select count(1) from XjrjgfrBaseinfo where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0'";
        String sqldtj7_3 = "select count(1) from XjrjgfrProfit where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '0'";

        Object resultdtj7_1 = entityManager.createQuery(sqldtj7_1).getSingleResult();
        Object resultdtj7_2 = entityManager.createQuery(sqldtj7_2).getSingleResult();
        Object resultdtj7_3 = entityManager.createQuery(sqldtj7_3).getSingleResult();
        jrjgfrReport.setPending(Stringutil.panDuanShuZhiWeiLing(resultdtj7_1.toString(),resultdtj7_2.toString(),resultdtj7_3.toString()));
    }

    private void dshReportStatistics(String orgid, String departId,List<Report_Info> reports){
        StringBuilder sqldsh = new StringBuilder("");
        int flag=0;
        Report_Info jrjgfrReport=null;
        for(int i=0;i<reports.size()-1;i++) {
            if(ReportInfo.getReport(reports.get(i).getCode())==null){
                flag=i;
                jrjgfrReport=reports.get(i);
                continue;
            }
            sqldsh.append("select count(1) from "+ ReflectionUtils.getTableName(ReportInfo.getReport(reports.get(i).getCode())) +" where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' union all  ");
        }
        String code=reports.get(reports.size()-1).getCode();
        sqldsh.append(" select count(1) from "+ReflectionUtils.getTableName(ReportInfo.getReport(code))+" where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1' ");
        List<Object> resultdtj = entityManager.createNativeQuery(sqldsh.toString()).getResultList();
        resultdtj.add(flag,0);
        for(int i=0;i<reports.size();i++) {
            reports.get(i).setPendingAudit(String.valueOf(resultdtj.get(i)));
        }

        //处理机构机构法人报表
        String sqldtj7_1 = "select count(1) from XjrjgfrAssets where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1'";
        String sqldtj7_2 = "select count(1) from XjrjgfrBaseinfo where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1'";
        String sqldtj7_3 = "select count(1) from XjrjgfrProfit where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '1'";

        Object resultdtj7_1 = entityManager.createQuery(sqldtj7_1).getSingleResult();
        Object resultdtj7_2 = entityManager.createQuery(sqldtj7_2).getSingleResult();
        Object resultdtj7_3 = entityManager.createQuery(sqldtj7_3).getSingleResult();
        jrjgfrReport.setPendingAudit(Stringutil.panDuanShuZhiWeiLing(resultdtj7_1.toString(),resultdtj7_2.toString(),resultdtj7_3.toString()));
    }


   public void yshReportStatistics(String orgid, String departId,List<Report_Info> reports){
        StringBuilder sqlysh = new StringBuilder("");
        int flag=0;
        Report_Info jrjgfrReport=null;
        for(int i=0;i<reports.size()-1;i++) {
            if(ReportInfo.getReport(reports.get(i).getCode())==null){
                flag=i;
                jrjgfrReport=reports.get(i);
                continue;
            }
            sqlysh.append("select count(1) from "+ ReflectionUtils.getTableName(ReportInfo.getReport(reports.get(i).getCode())) +" where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' union all  ");
        }
        String code=reports.get(reports.size()-1).getCode();
        sqlysh.append(" select count(1) from "+ReflectionUtils.getTableName(ReportInfo.getReport(code))+" where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3' ");
        List<Object> resultdtj = entityManager.createNativeQuery(sqlysh.toString()).getResultList();
        resultdtj.add(flag,0);
        for(int i=0;i<reports.size();i++) {
            reports.get(i).setCheckSuccess(String.valueOf(resultdtj.get(i)));
        }

        //处理机构机构法人报表
        String sqldtj7_1 = "select count(1) from XjrjgfrAssets where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3'";
        String sqldtj7_2 = "select count(1) from XjrjgfrBaseinfo where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3'";
        String sqldtj7_3 = "select count(1) from XjrjgfrProfit where orgid = '"+orgid+"' and departid like '"+departId+"' and datastatus = '3'";

        Object resultdtj7_1 = entityManager.createQuery(sqldtj7_1).getSingleResult();
        Object resultdtj7_2 = entityManager.createQuery(sqldtj7_2).getSingleResult();
        Object resultdtj7_3 = entityManager.createQuery(sqldtj7_3).getSingleResult();
        jrjgfrReport.setCheckSuccess(Stringutil.panDuanShuZhiWeiLing(resultdtj7_1.toString(),resultdtj7_2.toString(),resultdtj7_3.toString()));
    }
}