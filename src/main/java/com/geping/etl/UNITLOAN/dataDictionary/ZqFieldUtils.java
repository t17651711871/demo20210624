package com.geping.etl.UNITLOAN.dataDictionary;

import java.util.*;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.dataDictionary
 * @USER: tangshuai
 * @DATE: 2021/4/26
 * @TIME: 15:14
 * @描述:
 */
public interface ZqFieldUtils {
    /**
     * 发行人行业
     * @return
     */
    List<String> fxrhyList =Arrays.asList("9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T");

    /**
     * 发行人经济成分
     * @return
     */
    List<String> fxrjjcfList =Arrays.asList("A01","A02","B01","B02","B03");

    /**
     * 发行人企业规模
     * @return
     */
    List<String> fxrqygmList = Arrays.asList("CS01", "CS02", "CS03", "CS04", "CS05");

    /**
     * 债券品种
     * @return
     */
     List<String> zqpzList = Arrays.asList("GB01", "GB02", "GB03", "GB041", "GB042", "CBN", "FB00", "FB01",
            "FB02", "FB03", "FB04", "FB05", "FB06", "MTN", "CP", "SCP", "SMECN1",
            "SMECN2", "PPN", "ABN", "PRB", "PRN", "CB01", "CB02", "CB03", "CB04",
            "CB05", "CB06", "CB07", "CB08", "MDBB", "ABS01", "ABS02", "TB99");

    /**
     * 债券信用级别
     * @return
     */
     static List<String> zqxyjbList() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            list.add("C0" + i);
        }
        for (int i = 10; i < 25; i++) {
            list.add("C0" + i);
        }
        list.add("C00");
        list.add("C99");
        return list;
    }

    /**
     * 债券信用级别
     * @return
     */
    List<String> fxrgmjjbmList =  Arrays.asList("A","A01","A02","A03","A04","A05","A06","A99","B","B01","B02","B03","B04","B05","B06","B07","B08","B09","B99","C","C01","C02","C99","D","D02","E","E01","E02","E03","E04");

    /**
     * 付息方式
     * @return
     */
    List<String> fxfsList = Arrays.asList("01","02","03","04","05");
    /**
     * 股权类型
     */
    List<String> gqlxList = Arrays.asList("1","2","3");


}
