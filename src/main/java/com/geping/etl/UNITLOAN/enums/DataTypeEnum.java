package com.geping.etl.UNITLOAN.enums;

/***
 * 数据类型枚举
 * @author liang.xu
 * @date 2021.4.29
 */
public enum DataTypeEnum {

    /**
     * 债券瓶种
     */
    ZQPZ(1, "债券品种","zqpzData"),

    /**
     * 债券总托管机构
     */
    ZQZTGJG(2,"债券总托管机构","zqztgjgData"),

    /**
     * 债券信用级别
     */
    ZQXYDJ(3,"债券信用级别","zqxydjData"),
    /**
     * 发行人企业规模
     */
    ZQQIGM(4,"发行人企业规模","zqqygmData"),
    /**
     * 发行人经济成分
     */
    ZQFXRJJCF(5,"发行人经济成分","zqfxrjjcfData"),

    /**
     * 发行人国民经济部门
     */
    ZQFXGMJJBM(6,"发行人国民经济部门","zqfxrjjbmData"),
    /**
     * 买入/卖出标志
     */
    ZQMMBZ(7,"买入/卖出标志","zqmmbzData"),

    /**
     * 付息方式
     */
    ZQFXFS(8,"付息方式","zqfxfsData"),
    /**
     * 股权类型
     */
    GQLX(9,"股权类型","gqlxData"),
    /**
     *  机构类型
     */
    JGLX(10,"机构类型","jglxData"),
    /**
     * 交易方向
     */
    JYFX(11,"交易方向","jyfxData"),
    /**
     * 载体类型
     */
    ZTLX(12,"载体类型","ztlxData"),
    /**
     * 运行方式
     */
    YXFS(13,"运行方式","yxfsData"),
    /**
     * 付息方式
     */
    FXRHY(14,"发行人行业","fxrhyData");

    private Integer type;
    private String text;
    private String key;

    private DataTypeEnum(Integer type, String text,String key) {
        this.type = type;
        this.text = text;
        this.key=key;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static DataTypeEnum getDataType(Integer  type){
        for(DataTypeEnum dataType: DataTypeEnum.values()){
            if(dataType.getType().intValue()==type.intValue()){
                return dataType;
            }
        }
        return null;
    }
}
