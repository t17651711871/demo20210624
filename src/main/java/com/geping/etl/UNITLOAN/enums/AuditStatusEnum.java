package com.geping.etl.UNITLOAN.enums;

/***
 * 审核状态枚举
 * @author liang.xu
 * @date 2021.4.6
 */
public enum AuditStatusEnum {

    /**
     * 待提交
     */
    DTJ(0, "dtj"),

    /**
     * 待审核
     */
    DSH(1, "dsh"),
    /**
     * 已审核
     */
    YSH(3, "ysh");


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    private Integer status;
    private String text;

    private AuditStatusEnum(Integer status, String text) {
        this.status = status;
        this.text = text;
    }

}
