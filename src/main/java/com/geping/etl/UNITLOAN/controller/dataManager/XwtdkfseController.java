package com.geping.etl.UNITLOAN.controller.dataManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.report.Xwtdkfse;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XwtdkfseService;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.DownloadUtil;
import com.geping.etl.UNITLOAN.util.ExcelUploadUtil;
import com.geping.etl.UNITLOAN.util.ResponseResult;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.UNITLOAN.util.XApplicationRunnerImpl;
import com.geping.etl.UNITLOAN.util.XDataBaseTypeUtil;
import com.geping.etl.UNITLOAN.util.check.CheckAllData2;
import com.geping.etl.UNITLOAN.util.check.CheckAllDataWtdkfse;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.utils.jdbc.JDBCUtils;

import net.sf.json.JSONObject;

/**    
*  
* @author liuweixin  
* @date 2021年1月6日 上午10:30:09  
*/
@RestController
public class XwtdkfseController {

	@Autowired
	XwtdkfseService xwtdkfseService;
	
	@Autowired
    private SUpOrgInfoSetService suisService;
	
	@Autowired
    private ExcelUploadUtil excelUploadUtil;
	
	@Autowired
    private CustomSqlUtil customSqlUtil;
	
	@Autowired
    private XcommonService commonService;
	
	@Autowired
    private XCheckRuleService checkRuleService;
	
	@Autowired
    private  XDataBaseTypeUtil dataBaseTypeUtil;
	
	@Autowired
    private DepartUtil departUtil;
	
	private final String tableName="xwtdkfse";
	
	private final String fileName="委托贷款发生额信息";
	
	private static String target="";
	
	@GetMapping("/XGetXwtdkfseDataUi")
    public ModelAndView XGoDataTwoUi (String status){
        ModelAndView modelAndView=new ModelAndView();
        List<BaseArea> list = XApplicationRunnerImpl.baseAreaList;
        List<BaseCountry> countryList = XApplicationRunnerImpl.baseCountryList;
        for (BaseCountry country : countryList) {
        	BaseArea area = new BaseArea();
        	area.setAreacode(country.getCountrycode());
        	area.setAreaname(country.getCountryname());
        	list.add(area);
        }
        modelAndView.addObject("baseAreaAndCountryList",list);
        modelAndView.addObject("baseAreaList",XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseAindustryList",XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseBindustryList",XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList",XApplicationRunnerImpl.baseCurrencyList);
        if ("dtj".equals(status)){
            modelAndView.addObject("datastatus","0");
            modelAndView.setViewName("unitloan/datamanage/wtdkfse/wtdkfsedtj");
        }else if ("dsh".equals(status)){
            modelAndView.addObject("datastatus","1");
            modelAndView.setViewName("unitloan/datamanage/wtdkfse/wtdkfsedtj");
        }else {
            modelAndView.addObject("datastatus","3");
            modelAndView.addObject("datamanege",status);
            modelAndView.setViewName("unitloan/createreport/wtdkfsebw");
        }
        return modelAndView;
    }
	
	//查询数据
    @PostMapping("XGetXwtdkfseData")
    public ResponseResult XGetXdkfsxxData(int page, int rows,String finorgcodeParam,String loancontractcodeParam,String operationnameParam,String loanbrowcodeParam,
       String browidcodeParam,String isfarmerloanParam,String checkstatusParam,String datastatus,HttpServletRequest request){
        page = page - 1;
        Sys_UserAndOrgDepartment sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");

        String departId = departUtil.getDepart(sys_user);
        Specification specification=new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                //待提交
                predicates.add(criteriaBuilder.equal(root.get("datastatus"), datastatus));

                predicates.add(criteriaBuilder.like(root.get("departid"), departId));
                
                if (StringUtils.isNotBlank(finorgcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("finorgcode"), "%"+finorgcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(loancontractcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("loancontractcode"), "%"+loancontractcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(loanbrowcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("loanbrowcode"), "%"+loanbrowcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(browidcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("browidcode"), "%"+browidcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(isfarmerloanParam)){
                    predicates.add(criteriaBuilder.like(root.get("isfarmerloan"), "%"+isfarmerloanParam+"%"));
                }
                if (StringUtils.isNotBlank(checkstatusParam)){
                    predicates.add(criteriaBuilder.equal(root.get("checkstatus"), checkstatusParam));
                }
                if (StringUtils.isNotBlank(operationnameParam)){
                    predicates.add(criteriaBuilder.equal(root.get("operationname"), operationnameParam));
                }
                predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
        PageRequest pageRequest = new PageRequest(page, rows, sort);
        Page<Xwtdkfse> all = xwtdkfseService.findAll(specification, pageRequest);
        List<Xwtdkfse> content = all.getContent();
        long totalCount = all.getTotalElements();
        return ResponseResult.success(totalCount,content);
    }
    
  //导入
    @PostMapping(value = "XimmportExcelwtdkfse",produces = "text/plain;charset=UTF-8")
    public void XimmportExcelTwo(HttpServletRequest request,HttpServletResponse response){
    	Sys_UserAndOrgDepartment sys_user=((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        File uploadFile = null;
        OPCPackage opcPackage=null;
        PrintWriter out = null;
        try {
            out = response.getWriter();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multiRequest.getFile("excelfile");
            JSONObject json = new JSONObject();
            if (file == null) {
                json.put("msg", "导入文件为空");
            } else {
                uploadFile = excelUploadUtil.uploadFile(file);
                opcPackage = OPCPackage.open(uploadFile);
                String[] headers = {"金融机构代码","内部机构号", "金融机构地区代码", "借款人证件类型", "借款人证件代码", "借款人国民经济部门",
                        "借款人行业", "借款人地区代码", "借款人经济成分", "借款人企业规模", "委托贷款借据编码", "委托贷款合同编码", "贷款实际投向",
                        "委托贷款发放日期", "委托贷款到期日期", "交易流水号", "币种", "委托贷款发生金额", "委托贷款发生金额折人民币", "利率是否固定",
                        "利率水平", "手续费金额折人民币", "贷款担保方式", "委托人国民经济部门", "委托人证件类型", "委托人证件代码", "委托人行业",
                        "委托人地区代码", "委托人经济成分", "委托人企业规模", "发放/收回标识", "贷款用途","数据日期"
                };
                String[] values = {"finorgcode", "finorgincode", "finorgareacode", "isfarmerloan", "browidcode", "isgreenloan",
                		"browinds", "browareacode","entpczjjcf", "entpmode", "loanbrowcode", "loancontractcode", "loanactdect", 
                		"loanstartdate", "loanenddate", "transactionnum", "loancurrency", "loanamt", "loancnyamt", "rateisfix",
                		"ratelevel", "sxfcny", "gteemethod", "wtrgmjjbm", "wtrtype", "wtrcode", "wtrhy",
                		"wtrdqdm", "wtrjjcf", "wtrqygm", "givetakeid", "issupportliveloan", "sjrq"
                };
                String[] rateFields = {"ratelevel"};
                String[] amountFields = {"loanamt","loancnyamt"};
                String[] dateFields = {"loanstartdate", "loanenddate","sjrq"};
                XCommonExcel commonExcel = new XCommonExcel(sys_user, opcPackage, customSqlUtil, new Xwtdkfse(),headers,values, amountFields,dateFields, null,rateFields);
                commonExcel.process(0);
                String departId = departUtil.getDepart(sys_user);
                if (commonExcel.msg.toString().length() == 0) {
                    commonService.importDelete(tableName,departId);
                    commonExcel.process(1);
                    json.put("msg","导入成功");
                }else {
                    json.put("msg",commonExcel.msg.toString());
                }


            }
            String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
            customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
            out.write(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                opcPackage.close();
                //将上传的文件删除
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    
    //校验
    @PostMapping("XCheckDataXwtdkfse")
    public void XCheckDataTwo(HttpServletRequest request, HttpServletResponse response,String finorgcodeParam,String loancontractcodeParam,String loanbrowcodeParam){
        long a = System.currentTimeMillis();
        //记录是否有错
        boolean b=false;
        boolean oracle = dataBaseTypeUtil.equalsOracle();
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        List<SUpOrgInfoSet> sUpOrgInfoSets = suisService.findAll();
        target=sUpOrgInfoSets.get(0).getMessagepath();
        String departId = departUtil.getDepart(sys_user);

        JSONObject json = new JSONObject();
        String id = request.getParameter("id");

        LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
        List<String> errorId = new ArrayList<>();
        List<String> rightId = new ArrayList<>();
        Map<String,String> whereMap=new HashMap<>();
        Map<String,String> setMap=new HashMap<>();
        setMap.put("operator",sys_user.getLoginid());
        setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        String whereStr=" where datastatus='0' and checkstatus='0' ";
        String whereStra=" where a.datastatus='0' and a.checkstatus='0' ";
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            whereStr = whereStr + " and id in(" + id + ")";
            whereStra = whereStra + " and a.id in(" + id + ")";
        }else {
            if (StringUtils.isNotBlank(finorgcodeParam)){
                whereStr = whereStr + " and finorgcode like '%"+ finorgcodeParam +"%'";
                whereStra = whereStra + " and a.finorgcode like '%"+ finorgcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(loancontractcodeParam)){
                whereStr = whereStr + " and loancontractcode like '%"+ loancontractcodeParam +"%'";
                whereStra = whereStra + " and a.loancontractcode like '%"+ loancontractcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(loanbrowcodeParam)){
                whereStr = whereStr + " and loanbrowcode like '%"+ loanbrowcodeParam +"%'";
                whereStra = whereStra + " and a.loanbrowcode like '%"+ loanbrowcodeParam +"%'";
            }
            whereStr = whereStr + " and departid like '%"+ departId +"%'";
            whereStra = whereStra + " and a.departid like '%"+ departId +"%'";
        }
        String sql="select * from xwtdkfse "+whereStr;
        Connection connection = null;
        ResultSet resultSet=null;
        
        try {
            connection = JDBCUtils.getConnection();
            resultSet = JDBCUtils.Query(connection, sql);
            if (!resultSet.next()) {	//没有可校验的数据
            	json.put("msg","-1");
                try {
                    PrintWriter out = response.getWriter();
                    out.write(json.toString());
                    out.flush();
                    out.close();
                } catch (IOException e) {
                	e.printStackTrace();
                }
                return;
            }else {
                //标识名称
                String errorcode = "委托贷款合同编码";
                String errorcode2="委托贷款借据编号";


                //校验开关集合
                List<String> checknumList = checkRuleService.getChecknum("xwtdkfse");

                if (checknumList.contains("JS2306")){
                    if (!oracle){
                        String errorinfo  = "数据日期+金融机构代码+委托贷款借据编码+交易流水号应唯一";
                        String sqlexist ="select id,loancontractcode,loanbrowcode from xwtdkfse t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,finorgincode,loanbrowcode,transactionnum from xwtdkfse where operationname != '申请删除' group by sjrq,finorgincode,loanbrowcode,transactionnum having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.finorgincode = t2.finorgincode and t1.loanbrowcode = t2.loanbrowcode and t1.transactionnum = t2.transactionnum)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                        errorinfo = "数据日期+金融机构代码+委托贷款借据编码+交易流水号应唯一（历史表）";
                        sqlexist ="select id,loancontractcode,loanbrowcode from xwtdkfse t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,finorgincode,loanbrowcode,transactionnum from xwtdkfseh group by sjrq,finorgincode,loanbrowcode,transactionnum having count(1) > 0) t2 where t1.sjrq = t2.sjrq and t1.finorgincode = t2.finorgincode  and t1.loanbrowcode = t2.loanbrowcode and t1.transactionnum = t2.transactionnum)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }else {
                        String errorinfo  = "数据日期+金融机构代码+委托贷款借据编码+交易流水号应唯一";
                        String sqlexist ="select id,loancontractcode,loanbrowcode from xwtdkfse "+whereStr+" and (sjrq,finorgincode,loanbrowcode,transactionnum) in (select sjrq,finorgincode,loanbrowcode,transactionnum from xwtdkfse where operationname != '申请删除' group by sjrq,finorgincode,loanbrowcode,transactionnum having  count(1) > 1)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                        errorinfo = "数据日期+金融机构代码+委托贷款借据编码+交易流水号应唯一（历史表）";
                        sqlexist = "select id,loancontractcode,loanbrowcode from xwtdkfse "+whereStr+" and (sjrq,finorgincode,loanbrowcode,transactionnum) in (select sjrq,finorgincode,loanbrowcode,transactionnum from xwtdkfseh group by sjrq,finorgincode,loanbrowcode,transactionnum having  count(1) > 0)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }
                }

                //借款人证件类型与存量委托贷款信息的借款人证件类型应该一致
                if (checknumList.contains("JS1708")){
                    String  errorinfo = "借款人证件类型与存量委托贷款信息的借款人证件类型应该一致";
                    String  sqlexist = "select a.id,a.loancontractcode,a.loanbrowcode from xwtdkfse a INNER JOIN xclwtdkxx b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.isfarmerloan != b.isfarmerloan and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    errorinfo = "借款人证件类型与存量委托贷款信息的借款人证件类型应该一致（历史表）";
                    sqlexist ="select a.id,a.loancontractcode,a.loanbrowcode from xwtdkfse a INNER JOIN xclwtdkxxh b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.isfarmerloan != b.isfarmerloan";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                //借款人证件代码与存量委托贷款信息的借款人证件代码应该一致
                if (checknumList.contains("JS1709")){
                    String  errorinfo = "借款人证件代码与存量委托贷款信息的借款人证件代码应该一致";
                    String  sqlexist = "select a.id,a.loancontractcode,a.loanbrowcode from xwtdkfse a INNER JOIN xclwtdkxx b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.browidcode != b.brroweridnum and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    errorinfo = "借款人证件代码与存量委托贷款信息的借款人证件代码应该一致（历史表）";
                    sqlexist ="select a.id,a.loancontractcode,a.loanbrowcode from xwtdkfse a INNER JOIN xclwtdkxxh b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.browidcode != b.brroweridnum";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                //委托人证件类型与存量委托贷款信息的委托人证件类型应该一致
                if (checknumList.contains("JS1719")){
                    String  errorinfo = "委托人证件类型与存量委托贷款信息的委托人证件类型应该一致";
                    String  sqlexist = "select a.id,a.loancontractcode,a.loanbrowcode from xwtdkfse a INNER JOIN xclwtdkxx b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.wtrtype != b.bailorgfarmerloan and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    errorinfo = "委托人证件类型与存量委托贷款信息的委托人证件类型应该一致（历史表）";
                    sqlexist ="select a.id,a.loancontractcode,a.loanbrowcode from xwtdkfse a INNER JOIN xclwtdkxxh b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.wtrtype != b.bailorgfarmerloan";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                //委托人证件代码与存量委托贷款信息的委托人证件代码应该一致
                if (checknumList.contains("JS1720")){
                    String  errorinfo = "委托人证件代码与存量委托贷款信息的委托人证件代码应该一致";
                    String  sqlexist = "select a.id,a.loancontractcode,a.loanbrowcode from xwtdkfse a INNER JOIN xclwtdkxx b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.wtrcode != b.bailorgidnum and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    errorinfo = "委托人证件代码与存量委托贷款信息的委托人证件代码应该一致（历史表）";
                    sqlexist ="select a.id,a.loancontractcode,a.loanbrowcode from xwtdkfse a INNER JOIN xclwtdkxxh b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.wtrcode != b.bailorgidnum";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                //委托人为非同业单位客户的证件代码应该在非同业单位客户基础信息.客户证件代码中存在
                if (checknumList.contains("JS1901")){
                    String errorinfo = "委托人为非同业单位客户的证件代码应该在非同业单位客户基础信息.客户证件代码中存在";
                    String sqlexist = "select a.id,a.loancontractcode,a.loanbrowcode from xwtdkfse a "+whereStra+" and a.wtrgmjjbm not like 'B%' and a.wtrgmjjbm != 'E03' and a.wtrgmjjbm != 'D01' and a.wtrtype like 'A%' and (a.wtrcode not in (select b.customercode from xftykhx b INNER JOIN xwtdkfse c on b.sjrq = c.sjrq where b.operationname != '申请删除')  and a.wtrcode not in (select e.customercode from xftykhxh e INNER JOIN xwtdkfse f on e.sjrq = f.sjrq ))";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                //委托人为个人客户的证件代码应该在个人客户基础信息.客户证件代码中存在
                if (checknumList.contains("JS1903")){
            		String errorinfo = "委托人为个人客户的证件代码应该在个人客户基础信息.客户证件代码中存在";
                    String sqlexist = "select a.id,a.loancontractcode,a.loanbrowcode from xwtdkfse a "+whereStra+" and a.wtrtype like 'B%' and not exists (select 1 from xgrkhxx b where a.sjrq = b.sjrq and b.operationname != '申请删除' and a.wtrtype = b.regamtcreny and a.wtrcode = b.customercode) and not exists (select 1 from xgrkhxxh b where a.sjrq = b.sjrq and a.wtrtype = b.regamtcreny and a.wtrcode = b.customercode)";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                	
                }
                
                //借款人为非同业单位客户的证件代码应该在非同业单位客户基础信息.客户证件代码中存在
                if (checknumList.contains("JS1905")){
                    String errorinfo = "借款人为非同业单位客户的证件代码应该在非同业单位客户基础信息.客户证件代码中存在";
                    String sqlexist = "select a.id,a.loancontractcode,a.loanbrowcode from xwtdkfse a "+whereStra+" and a.isgreenloan not like 'B%' and a.isgreenloan != 'E03' and a.isgreenloan != 'D01' and a.isfarmerloan like 'A%' and (a.browidcode not in (select b.customercode from xftykhx b INNER JOIN xwtdkfse c on b.sjrq = c.sjrq where b.operationname != '申请删除')  and a.browidcode not in (select e.customercode from xftykhxh e INNER JOIN xwtdkfse f on e.sjrq = f.sjrq ))";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                //借款人为个人客户的证件代码应该在个人客户基础信息.客户证件代码中存在
                if (checknumList.contains("JS1907")){
            		String errorinfo = "借款人为个人客户的证件代码应该在个人客户基础信息.客户证件代码中存在";
                    String sqlexist = "select a.id,a.loancontractcode,a.loanbrowcode from xwtdkfse a "+whereStra+" and a.isfarmerloan like 'B%' and not exists (select 1 from xgrkhxx b where a.sjrq = b.sjrq and b.operationname != '申请删除' and a.isfarmerloan = b.regamtcreny and a.browidcode = b.customercode) and not exists (select 1 from xgrkhxxh b where a.sjrq = b.sjrq and a.isfarmerloan = b.regamtcreny and a.browidcode = b.customercode)";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                	
                }
                
            	resultSet = JDBCUtils.Query(connection, sql);
            	while (true) {
                    if (!resultSet.next()) break;
                     Xwtdkfse xwtdkfse = new Xwtdkfse();
                    Stringutil.getEntity(resultSet, xwtdkfse);
                    CheckAllDataWtdkfse.checkXwtdkfse(customSqlUtil,checknumList, xwtdkfse, errorMsg, errorId, rightId);
                }
            }
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }finally {
            if (connection!=null){
                JDBCUtils.close();
            }
            if(CheckAllData2.fzList != null) {
                CheckAllData2.fzList.clear();
                CheckAllData2.fzList = null;
            }
            if(CheckAllData2.frList != null) {
                CheckAllData2.frList.clear();
                CheckAllData2.frList = null;
            }
        }

        //校验结束

        //修改状态 和 下载
        if (errorId!=null && errorId.size()>0){
            //-下载到本地
            StringBuffer errorMsgAll = new StringBuffer("");
            for(Map.Entry<String, String> entry : errorMsg.entrySet()) {
                errorMsgAll.append(entry.getValue()+"\r\n");
            }
            DownloadUtil.downLoad(errorMsgAll.toString(),target+File.separator+"checkout",fileName+".txt");
            errorMsg.clear();

            setMap.put("checkStatus","2");
            setMap.put("operationname"," ");
            customSqlUtil.updateByWhereBatch(tableName,setMap,errorId);
            errorId.clear();
            json.put("msg","0");
        }else {
            if (b){
                json.put("msg","0");
            }else {
                json.put("msg","1");
            }
        }
        if (rightId!=null && rightId.size()>0){
            setMap.put("checkStatus","1");
            setMap.put("operationname"," ");
            customSqlUtil.updateByWhereBatch(tableName,setMap,rightId);
            rightId.clear();
        }

        customSqlUtil.saveLog("委托贷款发生额校验成功","校验");
        long b2 = System.currentTimeMillis();
        System.out.println("校验用时： "+(b2-a)/1000+"秒");
        try {
            PrintWriter out = response.getWriter();
            out.write(json.toString());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("XDownLoadCheckXwtdkfse")
    public void XDownLoadCheckTwo(HttpServletResponse response) {
        if (StringUtils.isBlank(target)){
            target=suisService.findAll().get(0).getMessagepath();
        }
        DownloadUtil.downLoadCheckFile(response,target + File.separator + "checkout",fileName+".txt");
    }
    
    // 表间校验
    public void inTableCalibration(Map<String,String> setMap,List<String> errorId,Map<String,String> whereMap,LinkedHashMap<String, String> errorMsg,String sql,String errorcode,String errorinfo,String errorcode2) {
        List<Object[]> exist = customSqlUtil.executeQuery(sql);
	    if (exist!=null && exist.size()>0){
	        setMap.put("checkStatus","2");
	        setMap.put("operationname"," ");
            exist.forEach(errId->{
	            errorId.add(String.valueOf(errId[0]));
	            whereMap.put("id", String.valueOf(errId[0]));
                if(!errorMsg.containsKey(String.valueOf(errId[0]))) {
                    errorMsg.put(String.valueOf(errId[0]), errorcode+":"+String.valueOf(errId[1])+"，"+errorcode2+":"+String.valueOf(errId[2])+"]->\r\n");

                }
                String str = errorMsg.get(String.valueOf(errId[0]));
                str = str + errorinfo+"|";
                errorMsg.put(String.valueOf(errId[0]), str);
	        });
            whereMap.clear();
            setMap.clear();
            exist.clear();
	    }
    }
}
