package com.geping.etl.UNITLOAN.controller.downloadreport;


import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.entity.report.XreportInfo;
import com.geping.etl.UNITLOAN.service.report.XreportInfoService;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class XDownloadReportController {

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private XreportInfoService reportInfoService;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    //跳转到下载报文页面
    @GetMapping(value = "/XDownMessageUi")
    public ModelAndView XDownMessageUi(HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        ModelAndView model = new ModelAndView();
        model.setViewName("unitloan/downloadreport/XreportView");
        return model;
    }

    //显示
    @PostMapping(value = "XdownloadreportView")
    public Map<String,Object> XdownloadreportView(HttpServletRequest request){
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User"));
        List<SUpOrgInfoSet> list1 = suisService.findAll();

        String currentPageNumber = request.getParameter("page"); // 获取查询页数
        String currentPageSize = request.getParameter("rows"); // 获取每页条数
        String reportdateParam = request.getParameter("reportdateParam");//帐号
        String reportnameParam = request.getParameter("reportnameParam");
        int page = Integer.valueOf(currentPageNumber) - 1;
        int size = Integer.valueOf(currentPageSize);
        
        Sort sort = new Sort(Direction.DESC, "reportdate");
        Specification<XreportInfo> querySpecifi = new Specification<XreportInfo>() {
            @Override
            public Predicate toPredicate(Root<XreportInfo> root, CriteriaQuery<?> criteriaQuery,
                                         CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                if (StringUtils.isNotBlank(reportdateParam)) {
                	String reportdateParam2 = reportdateParam.replace("-", "");
                    predicates.add(criteriaBuilder.like(root.get("reportname"), "%" + reportdateParam2.trim() + "%"));
                }
                if (StringUtils.isNotBlank(reportnameParam)) {
                    predicates.add(criteriaBuilder.like(root.get("reportname"), "%" + reportnameParam.trim() + "%"));
                }
                predicates.add(criteriaBuilder.equal(root.get("orgid").as(String.class), sys_user.getOrgid()));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

        // Pageable 接口通常使用的其 PageRequest 实现类. 其中封装了需要分页的信息
        PageRequest pr = new PageRequest(page, size, sort); // intPage从0开始
        List<XreportInfo> list = null;
        // 分页查询
        Page<XreportInfo> gcPage = reportInfoService.findAll(querySpecifi, pr);
        list = gcPage.getContent();

        Map<String,Object> map = new HashMap<String, Object>();
        map.put("rows",list);
        map.put("total",gcPage.getTotalElements());
        return map;
    }

    //下载报文
    @GetMapping(value = "/XReportDownload")
    public void XReportDownload(HttpServletRequest request, HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        String id = request.getParameter("id");

        OutputStream out = null;
        FileInputStream in = null;
        try {
            response.setCharacterEncoding("utf-8");
            response.setHeader("Pragma", "No-Cache");
            response.setHeader("Cache-Control", "No-Cache");
            response.setDateHeader("Expires", 0);
            response.setContentType("application/msexcel; charset=UTF-8");
            XreportInfo reportInfo = reportInfoService.getById(id);
            response.setHeader("Content-disposition","attachment; filename=" + URLEncoder.encode(reportInfo.getReportname(), "UTF-8"));// 设定输出文件头
            in = new FileInputStream(reportInfo.getReporturl()); // 读入文件
            out = response.getOutputStream();
            out.flush();
            int aRead = 0;
            while ((aRead = in.read()) != -1 & in != null) {
                out.write(aRead);
            }
            out.flush();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class HistoryMsg{
        private int id;
        private String reportname;
        private String reportdate;
        private String reportnamec;

        public HistoryMsg(String reportname, String reportdate, String reportnamec) {
            this.reportname = reportname;
            this.reportdate = reportdate;
            this.reportnamec = reportnamec;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getReportname() {
            return reportname;
        }

        public void setReportname(String reportname) {
            this.reportname = reportname;
        }

        public String getReportdate() {
            return reportdate;
        }

        public void setReportdate(String reportdate) {
            this.reportdate = reportdate;
        }

        public String getReportnamec() {
            return reportnamec;
        }

        public void setReportnamec(String reportnamec) {
            this.reportnamec = reportnamec;
        }
    }
}
