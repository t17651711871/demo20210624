package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.enums.DataTypeEnum;
import com.geping.etl.UNITLOAN.service.baseInfo.DataDictionaryService;
import com.geping.etl.UNITLOAN.util.XApplicationRunnerImpl;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/***
 *   存量债券发行信息报表
 * @author liang.xu
 * @date 2021.4.12
 */
public class BaseDataController {

    @Autowired
    private DataDictionaryService dataDictionaryService;

    public void beforeRender(HttpServletRequest request, ModelAndView modelAndView) {
        List<BaseArea> list = XApplicationRunnerImpl.baseAreaList;
        List<BaseCountry> countryList = XApplicationRunnerImpl.baseCountryList;
        for (BaseCountry country : countryList) {
            BaseArea area = new BaseArea();
            area.setAreacode(country.getCountrycode());
            area.setAreaname(country.getCountryname());
            list.add(area);
        }
        modelAndView.addObject("baseAreaAndCountryList", list);
        modelAndView.addObject("baseAreaList", XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseCountryList", XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseAindustryList", XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseBindustryList", XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList", XApplicationRunnerImpl.baseCurrencyList);
        modelAndView.addObject("baseCountryTwoList", XApplicationRunnerImpl.baseCountryTwoList);
        modelAndView.addObject("baseEducationList", XApplicationRunnerImpl.baseEducationList);
        modelAndView.addObject("baseNationList", XApplicationRunnerImpl.baseNationList);

        //币种
        List<BaseCurrency> baseCurrencyList = XApplicationRunnerImpl.baseCurrencyList;
        JSONArray baseCurrencys = JSONArray.fromObject(baseCurrencyList);
        modelAndView.addObject("baseCurrencys", baseCurrencys);
        //发行人地区代码  国家地区代码加+行政地区代码
        List<BaseCountry> baseCountryList = XApplicationRunnerImpl.baseCountryList;//国家地区代码
        JSONArray baseCountrys = JSONArray.fromObject(baseCountryList);
        modelAndView.addObject("baseCountrys", baseCountrys);
        List<BaseArea> baseAreaList = XApplicationRunnerImpl.baseAreaList;//国家地区代码
        JSONArray baseAreas = JSONArray.fromObject(baseAreaList);
        modelAndView.addObject("baseAreas", baseAreas);
        //发行人行业 BaseAindustry
        List<BaseAindustry> baseAindustryList = XApplicationRunnerImpl.baseAindustryList;
        JSONArray baseAindustrys = JSONArray.fromObject(baseAindustryList);
        modelAndView.addObject("baseAindustrys", baseAindustrys);
        //初始化数据字典
        initDataDictionary(request, modelAndView);
    }

    /**
     * 初始化数据字典
     *
     * @param request
     * @param modelAndView
     */
    private void initDataDictionary(HttpServletRequest request, ModelAndView modelAndView) {
        Object objDk = request.getAttribute(SysConstants.dk);
        if (objDk == null) {
            objDk = request.getParameter(SysConstants.dk);
        }
        if (objDk == null) {
            return;
        }
        String strObjDk = String.valueOf(objDk);
        String[] ddArr = strObjDk.split(SysConstants.EN_COMMA);
        for (String type : ddArr) {
            DataTypeEnum dataTypeEnum = DataTypeEnum.getDataType(Integer.valueOf(type));
            if (dataTypeEnum != null) {
                modelAndView.addObject(dataTypeEnum.getKey(), JSONObject.fromObject(dataDictionaryService.getKv(dataTypeEnum.getType())).toString());
            }
        }
}
}