package com.geping.etl.UNITLOAN.controller.baseInfo;

import com.geping.etl.UNITLOAN.entity.baseInfo.DataDictionary;
import com.geping.etl.UNITLOAN.service.baseInfo.DataDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/***
 * 数据字典控制器
 * @author liang.xu
 * @date 2021.4.29
 */
@RestController
public class DataDictionaryController {

    @Autowired
    private DataDictionaryService  dataDictionaryService;

    //查询数据字典
    @GetMapping("/queryDataDictionary")
    public List<DataDictionary> queryataDictionary(Integer type) {
           return dataDictionaryService.findAll(type);
    }
}