package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.UNITLOAN.entity.report.Xgrkhxx;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.util.XgetterAndSetter;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.*;

public class XCommenFileOut {

    public static <T> void fileOutToExcel(HttpServletResponse response, HttpServletRequest request, XcommonService commonService, String datastatus, String departId) {
        String url = request.getRequestURI();

        // headers:excel中表列标题名
        Map<String,String> headers = new LinkedHashMap<>();
        String fileName = "";
        String className = url.substring(url.lastIndexOf("/") + 8);
        if ("Xcldkxx".equals(className)||"XcldkxxH".equals(className)){
            fileName = "存量单位贷款基础数据信息.xlsx";
            headers = XfieldEnum.Xcldkxx.getAllEnumToMap();
        }else if ("Xdkfsxx".equals(className)||"XdkfsxxH".equals(className)){
            fileName = "单位贷款发生额信息.xlsx";
            headers = XfieldEnum.Xdkfsxx.getAllEnumToMap();
        }else if ("Xdkdbht".equals(className)||"XdkdbhtH".equals(className)){
            fileName = "单位贷款担保合同信息.xlsx";
            headers = XfieldEnum.Xdkdbht.getAllEnumToMap();
        }else if ("Xftykhx".equals(className)||"XftykhxH".equals(className)){
            fileName = "非同业单位客户基础信息.xlsx";
            headers = XfieldEnum.Xftykhx.getAllEnumToMap();
        }else if ("Xftykhxbl".equals(className)){
            fileName = "非同业单位客户基础信息补录.xlsx";
            headers = XfieldEnum.Xftykhxbl.getAllEnumToMap();
        }else if ("Xclgrdkxx".equals(className) ||"XclgrdkxxH".equals(className)){
            fileName = "存量个人贷款信息.xlsx";
            headers = XfieldEnum.Xclgrdkxx.getAllEnumToMap();
        }else if ("Xgrkhxx".equals(className)){
            fileName = "个人客户基础信息.xlsx";
            headers = XfieldEnum.Xgrkhxx.getAllEnumToMap();
        }else if ("Xgrdkfsxx".equals(className) ||"XgrdkfsxxH".equals(className)){
            fileName = "个人贷款发生额信息.xlsx";
            headers = XfieldEnum.Xgrdkfsxx.getAllEnumToMap();
        }else if ("Xclwtdkxx".equals(className) ||"XclwtdkxxH".equals(className)){
            fileName = "存量委托贷款基础数据信息.xlsx";
            headers = XfieldEnum.Xclwtdkxx.getAllEnumToMap();
        }else if ("Xwtdkfse".equals(className) ||"XwtdkfseH".equals(className)){
            fileName = "委托贷款发生额信息.xlsx";
            headers = XfieldEnum.Xwtdkfse.getAllEnumToMap();
        }else if ("Xcltyjdxx".equals(className) ||"XcltyjdxxH".equals(className)){
            fileName = "存量同业借贷信息.xlsx";
            headers = XfieldEnum.Xcltyjdxx.getAllEnumToMap();
        }else if ("Xtyjdfsexx".equals(className) ||"XtyjdfsexxH".equals(className)){
            fileName = "同业借贷发生额信息.xlsx";
            headers = XfieldEnum.Xtyjdfsexx.getAllEnumToMap();
        }else if ("Xtykhjcxx".equals(className) ||"XtykhjcxxH".equals(className)){
            fileName = "同业客户基础信息.xlsx";
            headers = XfieldEnum.Xtykhjcxx.getAllEnumToMap();
        }else if ("Xcltyckxx".equals(className) ||"XcltyckxxH".equals(className)){
            fileName = "存量同业存款信息.xlsx";
            headers = XfieldEnum.Xcltyckxx.getAllEnumToMap();
        }else if ("Xtyckfsexx".equals(className) ||"XtyckfsexxH".equals(className)){
            fileName = "同业存款发生额信息.xlsx";
            headers = XfieldEnum.Xtyckfsexx.getAllEnumToMap();
        }
        try {
            Enumeration<String> enumeration = request.getParameterNames();
            Map<String,String> param = new HashMap<>();
            while (enumeration.hasMoreElements()){
                String key = enumeration.nextElement();
                if (key.endsWith("Param") && StringUtils.isNotBlank(request.getParameter(key))){
                    param.put(key.substring(0,key.length() - 5),request.getParameter(key));
                }
            }
            Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User"));
            String orgId= sys_user.getOrgid();
            param.put("departid",departId);
            param.put("orgid",orgId);
            String id = request.getParameter("id");
            if (StringUtils.isNotBlank(id)){
                id = id.substring(0,id.length()-1);
                id = "'" + id.replace(",","','")  + "'";
                param.put("id",id);
            }
            if (StringUtils.isNotBlank(datastatus)){
                param.put("datastatus",datastatus);
            }
            Class clazz = Class.forName("com.geping.etl.UNITLOAN.entity.report."+className);
            List<T> list = commonService.findByFielOut(param,clazz);
            fileOut(request,response,list,fileName,headers,className);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static <T> void fileOut(HttpServletRequest request, HttpServletResponse response, List<T> list, String fileName, Map<String, String> headers, String className) throws Exception {
    	Workbook wb = new SXSSFWorkbook(); // 创建一个新的excel
        try {
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setContentType("octets/stream");
            response.addHeader("Content-Disposition",
                    "attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));

            Sheet sheet = wb.createSheet("sheet1"); // 创建sheet页
            Font font = wb.createFont();
            font.setFontName("仿宋_GB2312");
            font.setFontHeightInPoints((short) 14);
            font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            CellStyle style = wb.createCellStyle();
            style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            style.setFont(font);

            Row row = sheet.createRow(0); // 创建第一行
            // 创建第一行第i格,循环创建表头标题行
            int n = 0;
            for (String s : headers.values()){
                Cell cell = row.createCell(n);
                cell.setCellValue(s);
                cell.setCellStyle(style);
                n++;
            }

            Cell cell;
            T t;
            Row rowPlus;
            String value;
            for (int i = 0; i < list.size(); i++) {
            	rowPlus = sheet.createRow(i + 1); // 从第二行开始创建
                t =  list.get(i); // 获取集合中的数据
                int m = 0;
                for (Map.Entry<String,String> entry : headers.entrySet()){
                	cell = rowPlus.createCell(m);
                	value = XgetterAndSetter.getter(t,entry.getKey());
                    cell.setCellValue(value);
                    m++;
                }
            }

            OutputStream out = response.getOutputStream();
            wb.write(out);
            out.flush();
            out.close();
            wb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
		Xgrkhxx x = new Xgrkhxx();
		Class clazz = x.getClass();
		Field[] fields = clazz.getDeclaredFields();
		for(Field f : fields) {
			System.out.println("String "+f.getName()+" = null;");
		}
		for(Field f : fields) {
			System.out.println(f.getName()+" = resultSet.getString(\""+f.getName()+"\");");
		}
	}
}
