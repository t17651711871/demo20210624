package com.geping.etl.UNITLOAN.controller.uporg;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.ResponseResult;
import com.geping.etl.UNITLOAN.util.XApplicationRunnerImpl;
import com.geping.etl.common.entity.Report_Info;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.common.service.Report_InfoService;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 上报结构管理
 * @Author: wangzd
 * @Date: 17:51 2020/6/8
 */

@RestController
public class UpOrgController {

    @Autowired
    private SUpOrgInfoSetService ss;


    @RequestMapping(value = "/SUpOrgInfoSetUi", method = RequestMethod.GET)
    public ModelAndView SUpOrgInfoSetUi(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();
        List<SUpOrgInfoSet> list = ss.findAll();
        if(list.size() > 0) {
            request.setAttribute("orginfo", list.get(0));
        }
        model.setViewName("shgjg/uporginfoset/upOrgInfoSet");
        return model;
    }


    @RequestMapping(value = "/saddinfoset", method = RequestMethod.POST)
    public synchronized void saddinfoset(HttpServletRequest request, HttpServletResponse response, SUpOrgInfoSet info) {
        PrintWriter out = null;
        try {
            request.setCharacterEncoding("utf-8");
            List<SUpOrgInfoSet> list = ss.findAll();
            if(list.size() > 0) {
                info.setId(list.get(0).getId());
            }else {
                info.setId(UUID.randomUUID().toString());
            }

            if(info.getMessagepath().contains("\\")) {
                info.setMessagepath(info.getMessagepath().replace("\\", "\\\\"));
            }

            SUpOrgInfoSet result = ss.save(info);
            request.getSession().setAttribute("isusedelete",info.getIsusedelete());
            request.getSession().setAttribute("isuseupdate",info.getIsuseupdate());
            request.getSession().setAttribute("isusedepart",info.getIsusedepart());
            request.getSession().setAttribute("isusedepartformessage",info.getBankcodefaren());
            request.getSession().setAttribute("checklimit",info.getChecklimit());
            out = response.getWriter();
            if(result != null) {
                out.write("0");
            }else {
                out.write("1");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(out != null) {
                out.flush();
                out.close();
            }
        }
    }
}
