package com.geping.etl.UNITLOAN.controller.dataManager.immportExcel;

import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.ObjectUtils;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.SAXHelper;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author: wangzd
 * @Date: 2020/6/29
 */
public class CommonExcel<T> {

    private PrintStream output = System.out;

    public StringBuffer msg = new StringBuffer();

    public int count;

    private OPCPackage xlsxPackage;

    private int time1;

    private Sys_UserAndOrgDepartment sys_user;

    private CustomSqlUtil customSqlUtil;

    private T t;

    private Class aClass;

    private String[] headers;

    private String[] values;

    public CommonExcel(Sys_UserAndOrgDepartment sys_user, OPCPackage xlsxPackage, CustomSqlUtil customSqlUtil,T t,String[] headers,String[] values) {
        this.sys_user = sys_user;
        this.xlsxPackage = xlsxPackage;
        this.customSqlUtil = customSqlUtil;
        this.t=t;
        this.headers=headers;
        this.values=values;
    }

    //读取sheet
    public void processSheet(StylesTable styles, ReadOnlySharedStringsTable strings, XSSFSheetXMLHandler.SheetContentsHandler sheetHandler, InputStream sheetInputStream) throws IOException, ParserConfigurationException, SAXException {
        DataFormatter formatter = new DataFormatter();                  //其中格式化了储存在cell中的值的方法
        InputSource sheetSource = new InputSource(sheetInputStream);
        try {
            XMLReader sheetParser = SAXHelper.newXMLReader();    //读取XML文件
            ContentHandler handler = new XSSFSheetXMLHandler(styles, null, strings, sheetHandler, formatter, false);  //解析时所接受的对象 StylesTable,CommentsTable,ReadOnlySharedStringsTable,SheetContentsHandler,InputStream,boolean formulasNotResults
            sheetParser.setContentHandler(handler);
            sheetParser.parse(sheetSource);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException("SAX parser appears to be broken - " + e.getMessage());
        }
    }

    public void process(int time) throws Exception {
        ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(this.xlsxPackage);
        XSSFReader xssfreader = new XSSFReader(this.xlsxPackage);    //读取文件
        StylesTable styles = xssfreader.getStylesTable();     //表格样式
        XSSFReader.SheetIterator iter = (XSSFReader.SheetIterator) xssfreader.getSheetsData();
        int index = 0;
        time1 = time;
        while (iter.hasNext()) {   //循环迭代Excel，从第一个sheet开始
            InputStream stream = iter.next();
            String sheetName = iter.getSheetName();  //获取sheet的名字
            this.output.println(sheetName + " [index=" + index + "]:");
            processSheet(styles, strings, new CommonExcel.ExcelToCSV(), stream); //访问sheet并读取sheet的内容
            stream.close();//关闭输入流
            ++index;
        }
    }

    public class ExcelToCSV implements XSSFSheetXMLHandler.SheetContentsHandler {
        private int currentRow = -1;                   //开始的行
        private int currentCol = -1;                   //开始的列
        private boolean templateError = true;          //导入的表格模板是否正确
        private int res = 1;                           //导入错误的位置
//        String[] headers = {"金融机构代码","金融机构内部机构号", "金融机构地区代码", "借款人证件代码",
//                "借款人行业", "借款人地区代码", "企业出资人经济成分", "企业规模", "贷款借据编码", "贷款合同编码", "贷款产品类别",
//                "贷款实际投向", "贷款发放日期", "贷款到期日期", "贷款实际终止日期", "贷款币种", "贷款发生金额", "贷款发生金额折人民币",
//                "利率是否固定", "利率水平", "贷款定价基准类型", "基准利率", "贷款财政扶持方式", "贷款利率重新定价日", "贷款担保方式",
//                "贷款状态", "发放/收回标识", "是否涉农贷款", "是否绿色贷款", "是否平台贷款", "是否保障性安居工程贷款"
//        };
//        String[] values = {"finorgcode", "finorgincode", "finorgareacode", "browidcode", "browinds", "browareacode",
//                "entpczjjcf", "entpmode", "loanbrowcode", "loancontractcode", "loanprocode", "loanactdect",
//                "loanstartdate", "loanenddate", "loanactenddate", "loancurrency", "loanamt", "loancnyamt", "rateisfix",
//                "ratelevel", "loanfixamttype", "rate", "loanfinancesupport", "loanraterepricedate", "gteemethod",
//                "loanstatus", "givetakeid", "isfarmerloan", "isgreenloan", "isplatformloan", "issupportliveloan"
//        };

        List<String> fieldList = new ArrayList<>();

        @Override
        public void startRow(int rowNum) {
            currentRow = rowNum;
            currentCol = -1;
            if (currentRow != 0) {
                try {
                    t = (T) t.getClass().newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //xdkfsxx = new Xdkfsxx();
                res++;
            }
        }

        @Override
        public void endRow(int rowNum) {
            if (t != null && templateError && currentRow != 0) {
                if (time1 == 1) {
                    String format = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                    String[] baseFields={"id","checkstatus", "datastatus", "operator", "operationname",
                            "operationtime", "orgid", "departid", "nopassreason",
                    };
                    String[] baseFieldValues={Stringutil.getUUid(),"0","0",sys_user.getLoginid(),"",
                            format,sys_user.getOrgid(),sys_user.getDepartid(),""};
                    for (int i = 0; i < baseFields.length; i++) {
                        try {
                            Field declaredField = aClass.getDeclaredField(baseFields[i]);
                            Class<?> type = declaredField.getType();
                            Method method = aClass.getDeclaredMethod("set" + Stringutil.getMethodName(baseFields[i]),type);
                            method.setAccessible(true);
                            method.invoke(t, baseFieldValues[i]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    customSqlUtil.saveT(ObjectUtils.zhengLiDuiXiang(t,sys_user));
                    count++;
                }
            }
        }

        @Override
        public void cell(String cellReference, String formattedValue, XSSFComment comment) {
            try {
                if (StringUtils.isBlank(formattedValue)) {
                    return;
                }
                //校验第一行表头是否匹配
                if (currentRow == 0 && templateError) {
                    int thisCol = (new CellReference(cellReference)).getCol();
                    if (!Arrays.asList(headers).contains(formattedValue.trim())) {
                        templateError = false;
                        msg.append("导入模板不正确->"+formattedValue);
                        return;
                    }
                    for (int i = 0; i < headers.length; i++) {
                        if (headers[i].equals(formattedValue)) {
                            fieldList.add(values[i]);
                        }
                    }
                } else {
                    //当不是第一行时且模板正确时，就开始读取Excel的值
                    if (currentRow > 0 && templateError) {
                        if (cellReference == null) {
                            cellReference = new CellAddress(currentRow, currentCol).formatAsString();
                        }
                        int thisCol = (new CellReference(cellReference)).getCol();
                        currentCol = thisCol;
                        if (templateError) {
                            String field = fieldList.get(thisCol);
                            aClass = t.getClass();
                            Field declaredField = aClass.getDeclaredField(field);
                            Class[] parameterTypes = new Class[1];
                            Class<?> type = declaredField.getType();
                            parameterTypes[0] = type;
                            Method method = aClass.getDeclaredMethod("set" + Stringutil.getMethodName(field), parameterTypes);
                            method.setAccessible(true);
                            if (type == Integer.class) {
                                method.invoke(t, Integer.parseInt(formattedValue));
                            } else if (type == BigDecimal.class) {
                                method.invoke(t, new BigDecimal(formattedValue));
                            } else {
                                method.invoke(t, formattedValue);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void headerFooter(String text, boolean isHeader, String tagName) {

        }
    }
}
