package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.CommonExcel;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAmt;
import com.geping.etl.UNITLOAN.entity.report.XjrjgfrAssets;
import com.geping.etl.UNITLOAN.entity.report.XjrjgfrBaseinfo;
import com.geping.etl.UNITLOAN.entity.report.XjrjgfrProfit;
import com.geping.etl.UNITLOAN.entity.report.Xjrjgfz;
import com.geping.etl.UNITLOAN.entity.report.XreportInfo;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseAmtService;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XjrjgfrAssetsService;
import com.geping.etl.UNITLOAN.service.report.XjrjgfrBaseinfoService;
import com.geping.etl.UNITLOAN.service.report.XjrjgfrProfitService;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.UNITLOAN.util.*;
import com.geping.etl.UNITLOAN.util.IOUtil.ReadMoreFileUtil;
import com.geping.etl.UNITLOAN.util.response.CodeDetail;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 金融机构（法人）基础信息控制器
 * @author WuZengWen
 * @date 2020年6月9日 下午2:52:20
 */
@RestController
public class JRJGFRJCXXController {

	@Autowired
	private XjrjgfrAssetsService xas;
	@Autowired
	private XjrjgfrBaseinfoService xbs;
	@Autowired
	private XjrjgfrProfitService xps;
	@Autowired
	private SUpOrgInfoSetService sus;
	@Autowired
	private CalcCountUtil cct;
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private BaseAmtService bas;
	@Autowired
	private CustomSqlUtil customSqlUtil;
	@Autowired
	private ExcelUploadUtil excelUploadUtil;
	@Autowired
	private JdbcTemplate jt;
	private Sys_UserAndOrgDepartment sys_user;
    @Autowired
    private XcommonService commonService;
    @Autowired
    private CalcCountUtil countUtil;
    @Autowired
    private XCheckRuleService xCheckRuleService;
    @Autowired
    private DepartUtil departUtil;
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	//导入rar文件
	@PostMapping("/XImportIJFile")
	public ResponseResult XImportIJFile(@RequestParam(value = "file", required = false) MultipartFile file){
		if (file == null || file.isEmpty()) {
			return ResponseResult.success(CodeDetail.OPERATEERROR);
		}
		String filename = file.getOriginalFilename();
		int lastIndexOf = filename.lastIndexOf("\\");
		if (lastIndexOf>0){
			filename = filename.substring(lastIndexOf+1);
		}
		if (filename.endsWith(".rar") || filename.endsWith(".ZIP") || filename.endsWith(".zip")){
			String realPath = request.getSession().getServletContext().getRealPath("/");
			String rarDir = realPath+"temp"+File.separator+Stringutil.getUUid()+"_"+filename;
			String tempDir = realPath + "temp"+File.separator+"rar"+File.separator;
			File file1 = new File(rarDir);
			File tempFile = new File(tempDir);
			if (!tempFile.exists()) tempFile.mkdirs();
			try {
				file.transferTo(file1);
				/*if(filename.endsWith(".rar")) {
					//ReadMoreFileUtil.readRar(rarDir,tempDir);
					ReadMoreFileUtil.JieYaRAR(file1,tempDir);
				}else if(filename.endsWith(".ZIP") || filename.endsWith(".zip")) {
					ReadMoreFileUtil.JieYaZIP(rarDir,tempDir);
				}*/
				if(filename.endsWith(".ZIP") || filename.endsWith(".zip")) {
					ReadMoreFileUtil.JieYaZIP(rarDir,tempDir);
				}
				
				File[] files = tempFile.listFiles();
				if (files!=null && files.length>0){
					//先清空之前的数据
					customSqlUtil.deleteAll(BaseAmt.class);
					String code="";
					List<BaseAmt> amtList=new ArrayList<>();
					for (File file2 : files) {
						String name = file2.getName();
						String endName = name.substring(name.indexOf("."));
						if (".IDX".equalsIgnoreCase(endName)) {
							List<String> strings = ReadMoreFileUtil.readTxt(tempDir + file2.getName());
							for (String value : strings) {
								String[] split = value.split("\\|");
								if ("BWB0001".equalsIgnoreCase(split[3])) {
									code = split[0];
								}
							}
						}
					}
					for (File file2 : files) {
						String name = file2.getName();
						String endName = name.substring(name.indexOf("."));
						if (".DAT".equalsIgnoreCase(endName)){
							List<String> strings = ReadMoreFileUtil.readTxt(tempDir + file2.getName());
							for (String value : strings) {
								String[] split = value.split("\\|");
								if (code.equalsIgnoreCase(split[0])){
									BaseAmt baseAmt=new BaseAmt();
									baseAmt.setId(Stringutil.getUUid());
									baseAmt.setCode(code);
									baseAmt.setNumber(split[1]);
									baseAmt.setAmt(split[2]);
									amtList.add(baseAmt);
								}
							}
						}
						file2.delete();
					}
					if (amtList!=null && amtList.size()>0){
						customSqlUtil.saveAllT(amtList);
						customSqlUtil.saveLog("导入IJ文件","导入");
						return ResponseResult.success(CodeDetail.OPERATESUCCESS);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				file1.delete();
				tempFile.delete();
			}
		}else {
			return ResponseResult.success(CodeDetail.OPERATEERROR);
		}
		return ResponseResult.success(CodeDetail.OPERATEERROR);
	}

	//导入资产
	@PostMapping(value = "XdaorujrjgfrAssets",produces = "text/plain;charset=UTF-8")
	public void XdaorujrjgfrAssets(HttpServletRequest request,HttpServletResponse response){
		sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		File uploadFile = null;
		OPCPackage opcPackage=null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multiRequest.getFile("excelfile");
			JSONObject json = new JSONObject();
			if (file == null) {
				json.put("msg", "导入文件为空");
			} else {
				xas.delete();
				int total = xas.findTotal();
				if(total > 0) {
					json.put("msg","导入失败，已有一条数据不能再导入");
				}else {
					int result = Excel2007Utils.panduandaoruwenjian(file,TableCodeEnum.ZCFZJFXTJ,customSqlUtil,sys_user);
					if(result == 0) {
						/*uploadFile = excelUploadUtil.uploadFile(file);
						opcPackage = OPCPackage.open(uploadFile);
						String[] headers = {"各项存款", "各项贷款", "资产总计", "负债总计", "所有者权益合计","生息资产",
								"付息负债", "流动性资产", "流动性负债","正常类贷款","关注类贷款","次级类贷款",
								"可疑类贷款", "损失类贷款", "逾期贷款", "逾期90天以上贷款", "贷款减值准备"
						};
						String[] values = {"gxck", "gxdk", "zczj", "fzzj", "syzqyhj","sxzc",
								"fxzc", "ldxzc", "ldxfz", "zcldk","gzldk","cjldk",
								"jyldk", "ssldk", "yqdk", "yqninetytysdk", "dkjzzb"
						};
						CommonExcel<XjrjgfrAssets> commonExcel = new CommonExcel<>(sys_user, opcPackage, customSqlUtil, new XjrjgfrAssets(), headers, values);
						commonExcel.process(0);
						if (commonExcel.msg.toString().length() == 0) {
							commonExcel.process(1);
							json.put("msg","导入成功");
							cct.handleCount("xjrjgfrassets",0,"add",commonExcel.count);//更新数量
						} else {
							json.put("msg",commonExcel.msg.toString());
						}*/
						json.put("msg","导入成功");
						cct.handleCount("xjrjgfrassets",0,"add",1);//更新数量
					}else if(result == 1){
						json.put("msg","导入文件失败,文件为空");
					}else if(result == 2){
						json.put("msg","导入失败，总sheet>1");
					}else if(result == 3){
						json.put("msg","导入失败，第一行长度不对");
					}else if(result == 4){
						json.put("msg","导入失败，行数不对");
					}else if(result == 5){
						json.put("msg","导入失败，第一行名称不对");
					}else if(result == -1){
						json.put("msg","导入失败，程序异常");
					}else {
						json.put("msg","导入失败，未知错误");
					}
				}
			}
			String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
			customSqlUtil.saveLog("金融机构（法人）基础信息-资产负债及风险统计表->"+logContext,"导入");
			out.write(json.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				//opcPackage.close();
				//将上传的文件删除
				if (uploadFile != null && uploadFile.exists()) {
					uploadFile.delete();
				}
				if (out != null) {
					out.flush();
					out.close();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	//导入基础
	@PostMapping(value = "XdaorujrjgfrBaseinfo",produces = "text/plain;charset=UTF-8")
	public void XdaorujrjgfrBaseinfo(HttpServletRequest request,HttpServletResponse response){
		sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		File uploadFile = null;
		OPCPackage opcPackage=null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multiRequest.getFile("excelfile");
			JSONObject json = new JSONObject();
			if (file == null) {
				json.put("msg", "导入文件为空");
			} else {
				xbs.delete();
				int total = xbs.findTotal();
				if(total > 0) {
					json.put("msg","导入失败，已有一条数据不能再导入");
				}else {
					int result = Excel2007Utils.panduandaoruwenjian(file,TableCodeEnum.JCQKTJ,customSqlUtil,sys_user);
					if(result == 0) {
						/*uploadFile = excelUploadUtil.uploadFile(file);
						opcPackage = OPCPackage.open(uploadFile);
						String[] headers = {"金融机构名称", "金融机构代码", "金融机构编码", "注册地址", "地区代码","注册资本","成立日期","联系电话", 
								"机构经营状态","经济成分","行业分类","企业规模","实际控制人名称","实际控制人证件类型","实际控制人证件代码","从业人员数",
								"第一大股东代码", "第二大股东代码", "第三大股东代码", "第四大股东代码", "第五大股东代码", "第六大股东代码","第七大股东代码","第八大股东代码",
								"第九大股东代码","第十大股东代码","第一大股东持股比例","第二大股东持股比例","第三大股东持股比例","第四大股东持股比例","第五大股东持股比例","第六大股东持股比例",
								"第七大股东持股比例","第八大股东持股比例","第九大股东持股比例","第十大股东持股比例"
						};
						String[] values = {"finorgname", "finorgcode", "finorgnum", "regaddress", "regarea","regamt","setupdate","phone", 
								"orgmanagestatus", "orgstoreconomy", "industrycetegory","invermodel","actctrlidtype","actctrlname","actctrlcode","personnum",
								"onestockcode", "twostockcode", "threestockcode", "fourstockcode", "fivestockcode","sixstockcode","sevenstockcode","eightstockcode",
								"ninestockcode","tenstockcode","onestockprop","twostockprop","threestockprop","fourstockprop","fivestockprop","sixstockprop",
								"sevenstockprop","eightstockprop","ninestockprop","tenstockprop"
						};
						CommonExcel<XjrjgfrBaseinfo> commonExcel = new CommonExcel<>(sys_user, opcPackage, customSqlUtil, new XjrjgfrBaseinfo(), headers, values);
						commonExcel.process(0);
						if (commonExcel.msg.toString().length() == 0) {
							commonExcel.process(1);
							json.put("msg","导入成功");
							cct.handleCount("xjrjgfrbaseinfo",0,"add",commonExcel.count);//更新数量
						} else {
							json.put("msg",commonExcel.msg.toString());
						}*/
						json.put("msg","导入成功");
						cct.handleCount("xjrjgfrbaseinfo",0,"add",1);//更新数量
					}else if(result == 1){
						json.put("msg","导入文件失败,文件为空");
					}else if(result == 2){
						json.put("msg","导入失败，总sheet>1");
					}else if(result == 3){
						json.put("msg","导入失败，第一行长度不对");
					}else if(result == 4){
						json.put("msg","导入失败，行数不对");
					}else if(result == 5){
						json.put("msg","导入失败，第一行名称不对");
					}else if(result == -1){
						json.put("msg","导入失败，程序异常");
					}else {
						json.put("msg","导入失败，未知错误");
					}
				}
			}
			String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
			customSqlUtil.saveLog("金融机构（法人）基础信息-基础情况统计表 ->"+logContext,"导入");
			out.write(json.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				//opcPackage.close();
				//将上传的文件删除
				if (uploadFile != null && uploadFile.exists()) {
					uploadFile.delete();
				}
				if (out != null) {
					out.flush();
					out.close();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	//导入利润
	@PostMapping(value = "XdaorujrjgfrProfit",produces = "text/plain;charset=UTF-8")
	public void XdaorujrjgfrProfit(HttpServletRequest request,HttpServletResponse response){
		sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		File uploadFile = null;
		//OPCPackage opcPackage=null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multiRequest.getFile("excelfile");
			JSONObject json = new JSONObject();
			if (file == null) {
				json.put("msg", "导入文件为空");
			} else {
				xps.delete();
				int total = xps.findTotal();
				if(total > 0) {
					json.put("msg","导入失败，已有一条数据不能再导入");
				}else {
					int result = Excel2007Utils.panduandaoruwenjian(file,TableCodeEnum.LRJZBTJ,customSqlUtil,sys_user);
					if(result == 0) {
						/*uploadFile = excelUploadUtil.uploadFile(file);
						opcPackage = OPCPackage.open(uploadFile);
						String[] headers = {"营业收入", "利息净收入", "利息收入", "金融机构往来利息收入", "其中：系统内往来利息收入","各项贷款利息收入","债券利息收入","其他利息收入",
								"利息支出", "金融机构往来利息支出", "其中：系统内往来利息支出","各项存款利息支出","债券利息支出","其他利息支出","手续费及佣金净收入","手续费及佣金收入",
								"手续费及佣金支出", "租赁收益", "投资收益", "债券投资收益", "股权投资收益","其他投资收益","公允价值变动收益","汇兑净收益",
								"其他业务收入","营业支出","业务及管理费","其中:职工工资","福利费","住房公积金和住房补贴","税金及附加","资产减值损失",
								"其他业务支出","营业利润","营业外收入（加）","营业外支出（减）","利润总额","所得税（减）","净利润","年度损益调整（加）",
								"留存利润","未分配利润","应纳增值税","核心一级资本净额","一级资本净额","资本净额","应用资本底线及校准后的风险加权资产合计"
						};
						String[] values = {"yysr", "lxjsr", "lxsr", "jrjgwllxsr", "xtnwllxsr","gxdklxsr","zqlxsr","qtlxsr",
								"lxzc", "jrjgwllxzc", "xtnwllxzc", "gxcklxzc","zqlxzc","qtlxzc","sxfjyjjsr","sxfjyjsr",
								"jxfjyjzc", "zlsy", "tzsy", "zqtzsy", "gqtzsy","qttzsy","gyjzbdsy","hdjsy",
								"qtywsr","yyzc","ywjglf","zggz","flf","zfgjjhzfbt","sjjfj","zcjzss",
								"qtywzc","yylr","yywsr","yywzc","lrze","sds","jlr","ndsytz",
								"lclr","wfplr","ynzzs","hxyjzbje","yjzbje","zbje","ygzbjfxjqzchj"
						};
						CommonExcel<XjrjgfrProfit> commonExcel = new CommonExcel<>(sys_user, opcPackage, customSqlUtil, new XjrjgfrProfit(), headers, values);
						commonExcel.process(0);
						if (commonExcel.msg.toString().length() == 0) {
							commonExcel.process(1);
							json.put("msg","导入成功");
							cct.handleCount("xjrjgfrprofit",0,"add",commonExcel.count);//更新数量
						} else {
							json.put("msg",commonExcel.msg.toString());
						}*/
						json.put("msg","导入成功");
						cct.handleCount("xjrjgfrprofit",0,"add",1);//更新数量
					}else if(result == 1){
						json.put("msg","导入文件失败,文件为空");
					}else if(result == 2){
						json.put("msg","导入失败，总sheet>1");
					}else if(result == 3){
						json.put("msg","导入失败，第一行长度不对");
					}else if(result == 4){
						json.put("msg","导入失败，行数不对");
					}else if(result == 5){
						json.put("msg","导入失败，第一行名称不对");
					}else if(result == -1){
						json.put("msg","导入失败，程序异常");
					}else {
						json.put("msg","导入失败，未知错误");
					}
				}
			}
			String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
			customSqlUtil.saveLog("金融机构（法人基础信息）-利润及资本统计表->"+logContext,"导入");
			out.write(json.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				//opcPackage.close();
				//将上传的文件删除
				if (uploadFile != null && uploadFile.exists()) {
					uploadFile.delete();
				}
				if (out != null) {
					out.flush();
					out.close();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	//导出资产
	@RequestMapping(value = "/XdaochujrjgfrAssets", method = RequestMethod.GET)
	public void XdaochujrjgfrAssets(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select gxck,gxdk,zczj,fzzj,syzqyhj,sxzc,fxzc,ldxzc,ldxfz,"
				+ "zcldk,gzldk,cjldk,jyldk,ssldk,yqdk,yqninetytysdk,dkjzzb,sjrq from xjrjgfrassets";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				if(i == 0) {
					query = query + "'" + idarr[i] + "'";
				}else {
					query = query + ",'" + idarr[i] + "'";
				}
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			query = query + " where (datastatus='0' or datastatus='2') and orgid='" + sys_User.getOrgid() + "'";
			String departId = departUtil.getDepart(sys_User);
			query = query + " and departid like '" + departId + "'";
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("gxck"));
			dataA.add(smo.get("gxdk"));
			dataA.add(smo.get("zczj"));
			dataA.add(smo.get("fzzj"));
			dataA.add(smo.get("syzqyhj"));
			dataA.add(smo.get("sxzc"));
			dataA.add(smo.get("fxzc"));
			dataA.add(smo.get("ldxzc"));
			dataA.add(smo.get("ldxfz"));
			dataA.add(smo.get("zcldk"));
			dataA.add(smo.get("gzldk"));
			dataA.add(smo.get("cjldk"));
			dataA.add(smo.get("jyldk"));
			dataA.add(smo.get("ssldk"));
			dataA.add(smo.get("yqdk"));
			dataA.add(smo.get("yqninetytysdk"));
			dataA.add(smo.get("dkjzzb"));
			dataA.add(smo.get("sjrq"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("各项存款");
		columnNames.add("各项贷款");
		columnNames.add("资产总计");
		columnNames.add("负债总计");
		columnNames.add("所有者权益合计");
		columnNames.add("生息资产");
		columnNames.add("付息负债");
		columnNames.add("流动性资产");
		columnNames.add("流动性负债");
		columnNames.add("正常类贷款");
		columnNames.add("关注类贷款");
		columnNames.add("次级类贷款");
		columnNames.add("可疑类贷款");
		columnNames.add("损失类贷款");
		columnNames.add("逾期贷款");
		columnNames.add("逾期90天以上贷款");
		columnNames.add("贷款减值准备");
		columnNames.add("数据日期");
		List<String> heards = new LinkedList<>();
		heards.add("指标编码");
		heards.add("指标名称");
		heards.add("本外币金额");
		try {
			String fileName = "金融机构（法人）基础信息-资产负债及风险统计表.xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects,heards);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	//导出基础
	@RequestMapping(value = "/XdaochujrjgfrBaseinfo", method = RequestMethod.GET)
	public void XdaochujrjgfrBaseinfo(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select finorgname,finorgcode,finorgnum,jglb,regaddress,regarea,regamt,setupdate,lxr,phone," + 
				"orgmanagestatus,orgstoreconomy,invermodel,actctrlname,actctrlidtype,actctrlcode,personnum," + 
				"onestockcode,twostockcode,threestockcode,fourstockcode,fivestockcode,sixstockcode,sevenstockcode,eightstockcode," + 
				"ninestockcode,tenstockcode,onestockprop,twostockprop,threestockprop,fourstockprop,fivestockprop,sixstockprop," + 
				"sevenstockprop,eightstockprop,ninestockprop,tenstockprop,sjrq from xjrjgfrbaseinfo";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				if(i == 0) {
					query = query + "'" + idarr[i] + "'";
				}else {
					query = query + ",'" + idarr[i] + "'";
				}
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			query = query + " where (datastatus='0' or datastatus='2') and orgid='" + sys_User.getOrgid() + "'";
			String departId = departUtil.getDepart(sys_User);
			query = query + " and departid like '" + departId + "'";
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("finorgname"));
			dataA.add(smo.get("finorgcode"));
			dataA.add(smo.get("finorgnum"));
			dataA.add(smo.get("jglb"));
			dataA.add(smo.get("regaddress"));
			dataA.add(smo.get("regarea"));
			dataA.add(smo.get("regamt"));
			dataA.add(smo.get("setupdate"));
			dataA.add(smo.get("lxr"));
			dataA.add(smo.get("phone"));
			dataA.add(smo.get("orgmanagestatus"));
			dataA.add(smo.get("orgstoreconomy"));
			//dataA.add(smo.get("industrycetegory"));
			dataA.add(smo.get("invermodel"));			
			dataA.add(smo.get("actctrlname"));
			dataA.add(smo.get("actctrlidtype"));
			dataA.add(smo.get("actctrlcode"));
			dataA.add(smo.get("personnum"));
			dataA.add(smo.get("onestockcode"));
			dataA.add(smo.get("twostockcode"));
			dataA.add(smo.get("threestockcode"));
			dataA.add(smo.get("fourstockcode"));
			dataA.add(smo.get("fivestockcode"));
			dataA.add(smo.get("sixstockcode"));
			dataA.add(smo.get("sevenstockcode"));
			dataA.add(smo.get("eightstockcode"));
			dataA.add(smo.get("ninestockcode"));
			dataA.add(smo.get("tenstockcode"));
			dataA.add(smo.get("onestockprop"));
			dataA.add(smo.get("twostockprop"));
			dataA.add(smo.get("threestockprop"));
			dataA.add(smo.get("fourstockprop"));
			dataA.add(smo.get("fivestockprop"));
			dataA.add(smo.get("sixstockprop"));
			dataA.add(smo.get("sevenstockprop"));
			dataA.add(smo.get("eightstockprop"));
			dataA.add(smo.get("ninestockprop"));
			dataA.add(smo.get("tenstockprop"));
			dataA.add(smo.get("sjrq"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("金融机构名称");
		columnNames.add("金融机构代码");
		columnNames.add("金融机构编码");
		columnNames.add("机构类别");
		columnNames.add("注册地址");
		columnNames.add("地区代码");
		columnNames.add("注册资本");
		columnNames.add("成立日期");
		columnNames.add("联系人");
		columnNames.add("联系电话");
		columnNames.add("经营状态");
		columnNames.add("经济成分");
		//columnNames.add("行业分类");		
		columnNames.add("企业规模");
		columnNames.add("实际控制人名称");
		columnNames.add("实际控制人证件类型");
		columnNames.add("实际控制人证件代码");
		columnNames.add("从业人员数");
		columnNames.add("第一大股东代码");
		columnNames.add("第二大股东代码");
		columnNames.add("第三大股东代码");
		columnNames.add("第四大股东代码");
		columnNames.add("第五大股东代码");
		columnNames.add("第六大股东代码");
		columnNames.add("第七大股东代码");
		columnNames.add("第八大股东代码");
		columnNames.add("第九大股东代码");
		columnNames.add("第十大股东代码");
		columnNames.add("第一大股东持股比例");
		columnNames.add("第二大股东持股比例");
		columnNames.add("第三大股东持股比例");
		columnNames.add("第四大股东持股比例");
		columnNames.add("第五大股东持股比例");
		columnNames.add("第六大股东持股比例");
		columnNames.add("第七大股东持股比例");
		columnNames.add("第八大股东持股比例");
		columnNames.add("第九大股东持股比例");
		columnNames.add("第十大股东持股比例");
		columnNames.add("数据日期");
		List<String> heards = new LinkedList<>();
		heards.add("指标编码");
		heards.add("指标名称");
		heards.add("指标值");
		try {
			String fileName = "金融机构（法人）基础信息-基础情况统计表 .xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects,heards);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	//导出利润
	@RequestMapping(value = "/XdaochujrjgfrProfit", method = RequestMethod.GET)
	public void XdaochujrjgfrProfit(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select yysr,lxjsr,lxsr,jrjgwllxsr,xtnwllxsr,gxdklxsr,zqlxsr,qtlxsr," + 
				"lxzc,jrjgwllxzc,xtnwllxzc,gxcklxzc,zqlxzc,qtlxzc,sxfjyjjsr,sxfjyjsr," + 
				"jxfjyjzc,zlsy,tzsy,zqtzsy,gqtzsy,qttzsy,gyjzbdsy,hdjsy," + 
				"zcczsy,qtywsr,yyzc,ywjglf,zggz,flf,zfgjjhzfbt,sjjfj,zcjzss," + 
				"qtywzc,yylr,yywsr,yywzc,lrze,sds,jlr,ndsytz," + 
				"lclr,wfplr,ynzzs,hxyjzbje,yjzbje,zbje,ygzbjfxjqzchj,sjrq from xjrjgfrprofit";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				if(i == 0) {
					query = query + "'" + idarr[i] + "'";
				}else {
					query = query + ",'" + idarr[i] + "'";
				}
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			String departId = departUtil.getDepart(sys_User);
			query = query + " and departid like '" + departId + "'";
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("yysr"));
			dataA.add(smo.get("lxjsr"));
			dataA.add(smo.get("lxsr"));
			dataA.add(smo.get("jrjgwllxsr"));
			dataA.add(smo.get("xtnwllxsr"));
			dataA.add(smo.get("gxdklxsr"));
			dataA.add(smo.get("zqlxsr"));
			dataA.add(smo.get("qtlxsr"));
			dataA.add(smo.get("lxzc"));
			dataA.add(smo.get("jrjgwllxzc"));
			dataA.add(smo.get("xtnwllxzc"));
			dataA.add(smo.get("gxcklxzc"));
			dataA.add(smo.get("zqlxzc"));
			dataA.add(smo.get("qtlxzc"));
			dataA.add(smo.get("sxfjyjjsr"));
			dataA.add(smo.get("sxfjyjsr"));
			dataA.add(smo.get("jxfjyjzc"));
			dataA.add(smo.get("zlsy"));
			dataA.add(smo.get("tzsy"));
			dataA.add(smo.get("zqtzsy"));
			dataA.add(smo.get("gqtzsy"));
			dataA.add(smo.get("qttzsy"));
			dataA.add(smo.get("gyjzbdsy"));
			dataA.add(smo.get("hdjsy"));
			dataA.add(smo.get("zcczsy"));
			dataA.add(smo.get("qtywsr"));
			dataA.add(smo.get("yyzc"));
			dataA.add(smo.get("ywjglf"));
			dataA.add(smo.get("zggz"));
			dataA.add(smo.get("flf"));
			dataA.add(smo.get("zfgjjhzfbt"));
			dataA.add(smo.get("sjjfj"));
			dataA.add(smo.get("zcjzss"));
			dataA.add(smo.get("qtywzc"));
			dataA.add(smo.get("yylr"));
			dataA.add(smo.get("yywsr"));
			dataA.add(smo.get("yywzc"));
			dataA.add(smo.get("lrze"));
			dataA.add(smo.get("sds"));
			dataA.add(smo.get("jlr"));
			dataA.add(smo.get("ndsytz"));
			dataA.add(smo.get("lclr"));
			dataA.add(smo.get("wfplr"));
			dataA.add(smo.get("ynzzs"));
			dataA.add(smo.get("hxyjzbje"));
			dataA.add(smo.get("yjzbje"));
			dataA.add(smo.get("zbje"));
			dataA.add(smo.get("ygzbjfxjqzchj"));
			dataA.add(smo.get("sjrq"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("一、营业收入");
		columnNames.add("1.利息净收入");
		columnNames.add("利息收入");
		columnNames.add("金融机构往来利息收入");
		columnNames.add("其中：系统内往来利息收入");
		columnNames.add("各项贷款利息收入");
		columnNames.add("债券利息收入");
		columnNames.add("其他利息收入");
		columnNames.add("利息支出");
		columnNames.add("金融机构往来利息支出");
		columnNames.add("其中：系统内往来利息支出");
		columnNames.add("各项存款利息支出");
		columnNames.add("债券利息支出");
		columnNames.add("其他利息支出");
		columnNames.add("2.手续费及佣金净收入");
		columnNames.add("手续费及佣金收入");
		columnNames.add("手续费及佣金支出");
		columnNames.add("3.租赁收益");
		columnNames.add("4.投资收益");
		columnNames.add("债券投资收益");
		columnNames.add("股权投资收益");
		columnNames.add("其他投资收益");
		columnNames.add("5.公允价值变动收益");
		columnNames.add("6.汇兑净收益");
		columnNames.add("7.资产处置收益");
		columnNames.add("8.其他业务收入");
		columnNames.add("二、营业支出");
		columnNames.add("1.业务及管理费");
		columnNames.add("其中:职工工资");
		columnNames.add("福利费");
		columnNames.add("住房公积金和住房补贴");
		columnNames.add("2.税金及附加");
		columnNames.add("3.资产减值损失");
		columnNames.add("4.其他业务支出");
		columnNames.add("三、营业利润");
		columnNames.add("营业外收入（加）");
		columnNames.add("营业外支出（减）");
		columnNames.add("四、利润总额");
		columnNames.add("所得税（减）");
		columnNames.add("五、净利润");
		columnNames.add("年度损益调整（加）");
		columnNames.add("留存利润");
		columnNames.add("六、未分配利润");
		columnNames.add("应纳增值税");
		columnNames.add("核心一级资本净额");
		columnNames.add("一级资本净额");
		columnNames.add("资本净额");
		columnNames.add("应用资本底线及校准后的风险加权资产合计");
		columnNames.add("数据日期");
		List<String> heards = new LinkedList<>();
		heards.add("指标编码");
		heards.add("指标名称");
		heards.add("本外币金额");
		try {
			String fileName = "金融机构（法人基础信息）-利润及资本统计表.xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects,heards);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//导出模版资产
	@RequestMapping(value = "/XdaochumobanjrjgfrAssets", method = RequestMethod.GET)
	public void XdaochumobanjrjgfrAssets(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select gxck,gxdk,zczj,fzzj,syzqyhj,sxzc,fxzc,ldxzc,ldxfz,"
				+ "zcldk,gzldk,cjldk,jyldk,ssldk,yqdk,yqninetytysdk,dkjzzb,sjrq from xjrjgfrassets";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				if(i == 0) {
					query = query + "'" + idarr[i] + "'";
				}else {
					query = query + ",'" + idarr[i] + "'";
				}
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			query = query + " where (datastatus='0' or datastatus='2') and orgid='" + sys_User.getOrgid() + "'";
			String departId = departUtil.getDepart(sys_User);
			query = query + " and departid like '" + departId + "'";
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<List<Object>> objects = new LinkedList<>();
		/*for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("gxck"));
			dataA.add(smo.get("gxdk"));
			dataA.add(smo.get("zczj"));
			dataA.add(smo.get("fzzj"));
			dataA.add(smo.get("syzqyhj"));
			dataA.add(smo.get("sxzc"));
			dataA.add(smo.get("fxzc"));
			dataA.add(smo.get("ldxzc"));
			dataA.add(smo.get("ldxfz"));
			dataA.add(smo.get("zcldk"));
			dataA.add(smo.get("gzldk"));
			dataA.add(smo.get("cjldk"));
			dataA.add(smo.get("jyldk"));
			dataA.add(smo.get("ssldk"));
			dataA.add(smo.get("yqdk"));
			dataA.add(smo.get("yqninetytysdk"));
			dataA.add(smo.get("dkjzzb"));
			objects.add(dataA);
		}*/
		List<String> columnNames = new LinkedList<>();
		columnNames.add("各项存款");
		columnNames.add("各项贷款");
		columnNames.add("资产总计");
		columnNames.add("负债总计");
		columnNames.add("所有者权益合计");
		columnNames.add("生息资产");
		columnNames.add("付息负债");
		columnNames.add("流动性资产");
		columnNames.add("流动性负债");
		columnNames.add("正常类贷款");
		columnNames.add("关注类贷款");
		columnNames.add("次级类贷款");
		columnNames.add("可疑类贷款");
		columnNames.add("损失类贷款");
		columnNames.add("逾期贷款");
		columnNames.add("逾期90天以上贷款");
		columnNames.add("贷款减值准备");
		columnNames.add("数据日期");
		List<String> heards = new LinkedList<>();
		heards.add("指标编码");
		heards.add("指标名称");
		heards.add("本外币金额");
		try {
			String fileName = "金融机构（法人）基础信息-资产负债及风险统计表.xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects,heards);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	//导出模版基础
	@RequestMapping(value = "/XdaochumobanjrjgfrBaseinfo", method = RequestMethod.GET)
	public void XdaochumobanjrjgfrBaseinfo(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select finorgname,finorgcode,finorgnum,jglb,regaddress,regarea,regamt,setupdate,lxr,phone," + 
				"orgmanagestatus,orgstoreconomy,invermodel,actctrlname,actctrlidtype,actctrlcode,personnum," + 
				"onestockcode,twostockcode,threestockcode,fourstockcode,fivestockcode,sixstockcode,sevenstockcode,eightstockcode," + 
				"ninestockcode,tenstockcode,onestockprop,twostockprop,threestockprop,fourstockprop,fivestockprop,sixstockprop," + 
				"sevenstockprop,eightstockprop,ninestockprop,tenstockprop,sjrq from xjrjgfrbaseinfo";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				if(i == 0) {
					query = query + "'" + idarr[i] + "'";
				}else {
					query = query + ",'" + idarr[i] + "'";
				}
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			query = query + " where (datastatus='0' or datastatus='2') and orgid='" + sys_User.getOrgid() + "'";
			String departId = departUtil.getDepart(sys_User);
			query = query + " and departid like '" + departId + "'";
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<List<Object>> objects = new LinkedList<>();
		
		List<String> columnNames = new LinkedList<>();
		columnNames.add("金融机构名称");
		columnNames.add("金融机构代码");
		columnNames.add("金融机构编码");
		columnNames.add("机构类别");
		columnNames.add("注册地址");
		columnNames.add("地区代码");
		columnNames.add("注册资本");
		columnNames.add("成立日期");
		columnNames.add("联系人");
		columnNames.add("联系电话");
		columnNames.add("经营状态");
		columnNames.add("经济成分");
		//columnNames.add("行业分类");
		columnNames.add("企业规模");
		columnNames.add("实际控制人名称");
		columnNames.add("实际控制人证件类型");
		columnNames.add("实际控制人证件代码");
		columnNames.add("从业人员数");
		columnNames.add("第一大股东代码");
		columnNames.add("第二大股东代码");
		columnNames.add("第三大股东代码");
		columnNames.add("第四大股东代码");
		columnNames.add("第五大股东代码");
		columnNames.add("第六大股东代码");
		columnNames.add("第七大股东代码");
		columnNames.add("第八大股东代码");
		columnNames.add("第九大股东代码");
		columnNames.add("第十大股东代码");
		columnNames.add("第一大股东持股比例");
		columnNames.add("第二大股东持股比例");
		columnNames.add("第三大股东持股比例");
		columnNames.add("第四大股东持股比例");
		columnNames.add("第五大股东持股比例");
		columnNames.add("第六大股东持股比例");
		columnNames.add("第七大股东持股比例");
		columnNames.add("第八大股东持股比例");
		columnNames.add("第九大股东持股比例");
		columnNames.add("第十大股东持股比例");
		columnNames.add("数据日期");
		List<String> heards = new LinkedList<>();
		heards.add("指标编码");
		heards.add("指标名称");
		heards.add("指标值");
		try {
			String fileName = "金融机构（法人）基础信息-基础情况统计表 .xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects,heards);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	//导出模版利润
	@RequestMapping(value = "/XdaochumobanjrjgfrProfit", method = RequestMethod.GET)
	public void XdaochumobanjrjgfrProfit(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select yysr,lxjsr,lxsr,jrjgwllxsr,xtnwllxsr,gxdklxsr,zqlxsr,qtlxsr," + 
				"lxzc,jrjgwllxzc,xtnwllxzc,gxcklxzc,zqlxzc,qtlxzc,sxfjyjjsr,sxfjyjsr," + 
				"jxfjyjzc,zlsy,tzsy,zqtzsy,gqtzsy,qttzsy,gyjzbdsy,hdjsy," + 
				"zcczsy,qtywsr,yyzc,ywjglf,zggz,flf,zfgjjhzfbt,sjjfj,zcjzss," + 
				"qtywzc,yylr,yywsr,yywzc,lrze,sds,jlr,ndsytz," + 
				"lclr,wfplr,ynzzs,hxyjzbje,yjzbje,zbje,ygzbjfxjqzchj,sjrq from xjrjgfrprofit";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				if(i == 0) {
					query = query + "'" + idarr[i] + "'";
				}else {
					query = query + ",'" + idarr[i] + "'";
				}
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			query = query + " where (datastatus='0' or datastatus='2') and orgid='" + sys_User.getOrgid() + "'";
			String departId = departUtil.getDepart(sys_User);
			query = query + " and departid like '" + departId + "'";
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<List<Object>> objects = new LinkedList<>();
		/*for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("yysr"));
			dataA.add(smo.get("lxjsr"));
			dataA.add(smo.get("lxsr"));
			dataA.add(smo.get("jrjgwllxsr"));
			dataA.add(smo.get("xtnwllxsr"));
			dataA.add(smo.get("gxdklxsr"));
			dataA.add(smo.get("zqlxsr"));
			dataA.add(smo.get("qtlxsr"));
			dataA.add(smo.get("lxzc"));
			dataA.add(smo.get("jrjgwllxzc"));
			dataA.add(smo.get("xtnwllxzc"));
			dataA.add(smo.get("gxcklxzc"));
			dataA.add(smo.get("zqlxzc"));
			dataA.add(smo.get("qtlxzc"));
			dataA.add(smo.get("sxfjyjjsr"));
			dataA.add(smo.get("sxfjyjsr"));
			dataA.add(smo.get("jxfjyjzc"));
			dataA.add(smo.get("zlsy"));
			dataA.add(smo.get("tzsy"));
			dataA.add(smo.get("zqtzsy"));
			dataA.add(smo.get("gqtzsy"));
			dataA.add(smo.get("qttzsy"));
			dataA.add(smo.get("gyjzbdsy"));
			dataA.add(smo.get("hdjsy"));
			dataA.add(smo.get("qtywsr"));
			dataA.add(smo.get("yyzc"));
			dataA.add(smo.get("ywjglf"));
			dataA.add(smo.get("zggz"));
			dataA.add(smo.get("flf"));
			dataA.add(smo.get("zfgjjhzfbt"));
			dataA.add(smo.get("sjjfj"));
			dataA.add(smo.get("zcjzss"));
			dataA.add(smo.get("qtywzc"));
			dataA.add(smo.get("yylr"));
			dataA.add(smo.get("yywsr"));
			dataA.add(smo.get("yywzc"));
			dataA.add(smo.get("lrze"));
			dataA.add(smo.get("sds"));
			dataA.add(smo.get("jlr"));
			dataA.add(smo.get("ndsytz"));
			dataA.add(smo.get("lclr"));
			dataA.add(smo.get("wfplr"));
			dataA.add(smo.get("ynzzs"));
			dataA.add(smo.get("hxyjzbje"));
			dataA.add(smo.get("yjzbje"));
			dataA.add(smo.get("zbje"));
			dataA.add(smo.get("ygzbjfxjqzchj"));
			objects.add(dataA);
		}*/
		List<String> columnNames = new LinkedList<>();
		columnNames.add("一、营业收入");
		columnNames.add("1.利息净收入");
		columnNames.add("利息收入");
		columnNames.add("金融机构往来利息收入");
		columnNames.add("其中：系统内往来利息收入");
		columnNames.add("各项贷款利息收入");
		columnNames.add("债券利息收入");
		columnNames.add("其他利息收入");
		columnNames.add("利息支出");
		columnNames.add("金融机构往来利息支出");
		columnNames.add("其中：系统内往来利息支出");
		columnNames.add("各项存款利息支出");
		columnNames.add("债券利息支出");
		columnNames.add("其他利息支出");
		columnNames.add("2.手续费及佣金净收入");
		columnNames.add("手续费及佣金收入");
		columnNames.add("手续费及佣金支出");
		columnNames.add("3.租赁收益");
		columnNames.add("4.投资收益");
		columnNames.add("债券投资收益");
		columnNames.add("股权投资收益");
		columnNames.add("其他投资收益");
		columnNames.add("5.公允价值变动收益");
		columnNames.add("6.汇兑净收益");
		columnNames.add("7.资产处置收益");
		columnNames.add("8.其他业务收入");
		columnNames.add("二、营业支出");
		columnNames.add("1.业务及管理费");
		columnNames.add("其中:职工工资");
		columnNames.add("福利费");
		columnNames.add("住房公积金和住房补贴");
		columnNames.add("2.税金及附加");
		columnNames.add("3.资产减值损失");
		columnNames.add("4.其他业务支出");
		columnNames.add("三、营业利润");
		columnNames.add("营业外收入（加）");
		columnNames.add("营业外支出（减）");
		columnNames.add("四、利润总额");
		columnNames.add("所得税（减）");
		columnNames.add("五、净利润");
		columnNames.add("年度损益调整（加）");
		columnNames.add("留存利润");
		columnNames.add("六、未分配利润");
		columnNames.add("应纳增值税");
		columnNames.add("核心一级资本净额");
		columnNames.add("一级资本净额");
		columnNames.add("资本净额");
		columnNames.add("应用资本底线及校准后的风险加权资产合计");
		columnNames.add("数据日期");
		List<String> heards = new LinkedList<>();
		heards.add("指标编码");
		heards.add("指标名称");
		heards.add("本外币金额");
		try {
			String fileName = "金融机构（法人基础信息）-利润及资本统计表.xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects,heards);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//导出资产H
	@RequestMapping(value = "/XdaochujrjgfrAssetsH", method = RequestMethod.GET)
	public void XdaochujrjgfrAssetsH(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select gxck,gxdk,zczj,fzzj,syzqyhj,sxzc,fxzc,ldxzc,ldxfz,"
				+ "zcldk,gzldk,cjldk,jyldk,ssldk,yqdk,yqninetytysdk,dkjzzb,sjrq from xjrjgfrassetsH";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				if(i == 0) {
					query = query + "'" + idarr[i] + "'";
				}else {
					query = query + ",'" + idarr[i] + "'";
				}
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			query = query + " where orgid='" + sys_User.getOrgid() + "'";
			String departId = departUtil.getDepart(sys_User);
			query = query + " and departid like '" + departId + "'";
			String sjrq = request.getParameter("sjrqParam");
			if(StringUtils.isNotBlank(sjrq)) {
				query = query + " and sjrq like '%" + sjrq + "%'";
			}
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("gxck"));
			dataA.add(smo.get("gxdk"));
			dataA.add(smo.get("zczj"));
			dataA.add(smo.get("fzzj"));
			dataA.add(smo.get("syzqyhj"));
			dataA.add(smo.get("sxzc"));
			dataA.add(smo.get("fxzc"));
			dataA.add(smo.get("ldxzc"));
			dataA.add(smo.get("ldxfz"));
			dataA.add(smo.get("zcldk"));
			dataA.add(smo.get("gzldk"));
			dataA.add(smo.get("cjldk"));
			dataA.add(smo.get("jyldk"));
			dataA.add(smo.get("ssldk"));
			dataA.add(smo.get("yqdk"));
			dataA.add(smo.get("yqninetytysdk"));
			dataA.add(smo.get("dkjzzb"));
			dataA.add(smo.get("sjrq"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("各项存款");
		columnNames.add("各项贷款");
		columnNames.add("资产总计");
		columnNames.add("负债总计");
		columnNames.add("所有者权益合计");
		columnNames.add("生息资产");
		columnNames.add("付息负债");
		columnNames.add("流动性资产");
		columnNames.add("流动性负债");
		columnNames.add("正常类贷款");
		columnNames.add("关注类贷款");
		columnNames.add("次级类贷款");
		columnNames.add("可疑类贷款");
		columnNames.add("损失类贷款");
		columnNames.add("逾期贷款");
		columnNames.add("逾期90天以上贷款");
		columnNames.add("贷款减值准备");
		columnNames.add("数据日期");
		List<String> heards = new LinkedList<>();
		heards.add("指标编码");
		heards.add("指标名称");
		heards.add("本外币金额");
		try {
			String fileName = "金融机构（法人）基础信息-资产负债及风险统计表.xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects,heards);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}		
	//导出基础H
	@RequestMapping(value = "/XdaochujrjgfrBaseinfoH", method = RequestMethod.GET)
	public void XdaochujrjgfrBaseinfoH(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select finorgname,finorgcode,finorgnum,regaddress,regarea,regamt,setupdate,phone," + 
				"orgmanagestatus,orgstoreconomy,invermodel,actctrlname,actctrlidtype,actctrlcode,personnum," + 
				"onestockcode,twostockcode,threestockcode,fourstockcode,fivestockcode,sixstockcode,sevenstockcode,eightstockcode," + 
				"ninestockcode,tenstockcode,onestockprop,twostockprop,threestockprop,fourstockprop,fivestockprop,sixstockprop," + 
				"sevenstockprop,eightstockprop,ninestockprop,tenstockprop,sjrq from xjrjgfrbaseinfoH";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				if(i == 0) {
					query = query + "'" + idarr[i] + "'";
				}else {
					query = query + ",'" + idarr[i] + "'";
				}
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			query = query + " where orgid='" + sys_User.getOrgid() + "'";
			String departId = departUtil.getDepart(sys_User);
			query = query + " and departid like '" + departId + "'";
			String sjrq = request.getParameter("sjrqParam");
			if(StringUtils.isNotBlank(sjrq)) {
				query = query + " and sjrq like '%" + sjrq + "%'";
			}
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("finorgname"));
			dataA.add(smo.get("finorgcode"));
			dataA.add(smo.get("finorgnum"));
			dataA.add(smo.get("regaddress"));
			dataA.add(smo.get("regarea"));
			dataA.add(smo.get("regamt"));
			dataA.add(smo.get("setupdate"));
			dataA.add(smo.get("phone"));
			dataA.add(smo.get("orgmanagestatus"));
			dataA.add(smo.get("orgstoreconomy"));
			//dataA.add(smo.get("industrycetegory"));
			dataA.add(smo.get("invermodel"));
			dataA.add(smo.get("actctrlidtype"));
			dataA.add(smo.get("actctrlname"));
			dataA.add(smo.get("actctrlcode"));
			dataA.add(smo.get("personnum"));
			dataA.add(smo.get("onestockcode"));
			dataA.add(smo.get("twostockcode"));
			dataA.add(smo.get("threestockcode"));
			dataA.add(smo.get("fourstockcode"));
			dataA.add(smo.get("fivestockcode"));
			dataA.add(smo.get("sixstockcode"));
			dataA.add(smo.get("sevenstockcode"));
			dataA.add(smo.get("eightstockcode"));
			dataA.add(smo.get("ninestockcode"));
			dataA.add(smo.get("tenstockcode"));
			dataA.add(smo.get("onestockprop"));
			dataA.add(smo.get("twostockprop"));
			dataA.add(smo.get("threestockprop"));
			dataA.add(smo.get("fourstockprop"));
			dataA.add(smo.get("fivestockprop"));
			dataA.add(smo.get("sixstockprop"));
			dataA.add(smo.get("sevenstockprop"));
			dataA.add(smo.get("eightstockprop"));
			dataA.add(smo.get("ninestockprop"));
			dataA.add(smo.get("tenstockprop"));
			dataA.add(smo.get("sjrq"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("金融机构名称");
		columnNames.add("金融机构代码");
		columnNames.add("金融机构编码");
		columnNames.add("注册地址");
		columnNames.add("地区代码");
		columnNames.add("注册资本");
		columnNames.add("成立日期");
		columnNames.add("联系电话");
		columnNames.add("机构经营状态");
		columnNames.add("经济成分");
		//columnNames.add("行业分类");
		columnNames.add("企业规模");
		columnNames.add("实际控制人名称");
		columnNames.add("实际控制人证件类型");
		columnNames.add("实际控制人证件代码");
		columnNames.add("从业人员数");
		columnNames.add("第一大股东代码");
		columnNames.add("第二大股东代码");
		columnNames.add("第三大股东代码");
		columnNames.add("第四大股东代码");
		columnNames.add("第五大股东代码");
		columnNames.add("第六大股东代码");
		columnNames.add("第七大股东代码");
		columnNames.add("第八大股东代码");
		columnNames.add("第九大股东代码");
		columnNames.add("第十大股东代码");
		columnNames.add("第一大股东持股比例");
		columnNames.add("第二大股东持股比例");
		columnNames.add("第三大股东持股比例");
		columnNames.add("第四大股东持股比例");
		columnNames.add("第五大股东持股比例");
		columnNames.add("第六大股东持股比例");
		columnNames.add("第七大股东持股比例");
		columnNames.add("第八大股东持股比例");
		columnNames.add("第九大股东持股比例");
		columnNames.add("第十大股东持股比例");
		columnNames.add("数据日期");
		List<String> heards = new LinkedList<>();
		heards.add("指标编码");
		heards.add("指标名称");
		heards.add("指标值");
		try {
			String fileName = "金融机构（法人）基础信息-基础情况统计表 .xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects,heards);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}		
	//导出利润H
	@RequestMapping(value = "/XdaochujrjgfrProfitH", method = RequestMethod.GET)
	public void XdaochujrjgfrProfitH(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select yysr,lxjsr,lxsr,jrjgwllxsr,xtnwllxsr,gxdklxsr,zqlxsr,qtlxsr," + 
				"lxzc,jrjgwllxzc,xtnwllxzc,gxcklxzc,zqlxzc,qtlxzc,sxfjyjjsr,sxfjyjsr," + 
				"jxfjyjzc,zlsy,tzsy,zqtzsy,gqtzsy,qttzsy,gyjzbdsy,hdjsy," + 
				"zcczsy,qtywsr,yyzc,ywjglf,zggz,flf,zfgjjhzfbt,sjjfj,zcjzss," + 
				"qtywzc,yylr,yywsr,yywzc,lrze,sds,jlr,ndsytz," + 
				"lclr,wfplr,ynzzs,hxyjzbje,yjzbje,zbje,ygzbjfxjqzchj,sjrq from xjrjgfrprofitH";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				if(i == 0) {
					query = query + "'" + idarr[i] + "'";
				}else {
					query = query + ",'" + idarr[i] + "'";
				}
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			query = query + " where orgid='" + sys_User.getOrgid() + "'";
			String departId = departUtil.getDepart(sys_User);
			query = query + " and departid like '" + departId + "'";
			String sjrq = request.getParameter("sjrqParam");
			if(StringUtils.isNotBlank(sjrq)) {
				query = query + " and sjrq like '%" + sjrq + "%'";
			}
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("yysr"));
			dataA.add(smo.get("lxjsr"));
			dataA.add(smo.get("lxsr"));
			dataA.add(smo.get("jrjgwllxsr"));
			dataA.add(smo.get("xtnwllxsr"));
			dataA.add(smo.get("gxdklxsr"));
			dataA.add(smo.get("zqlxsr"));
			dataA.add(smo.get("qtlxsr"));
			dataA.add(smo.get("lxzc"));
			dataA.add(smo.get("jrjgwllxzc"));
			dataA.add(smo.get("xtnwllxzc"));
			dataA.add(smo.get("gxcklxzc"));
			dataA.add(smo.get("zqlxzc"));
			dataA.add(smo.get("qtlxzc"));
			dataA.add(smo.get("sxfjyjjsr"));
			dataA.add(smo.get("sxfjyjsr"));
			dataA.add(smo.get("jxfjyjzc"));
			dataA.add(smo.get("zlsy"));
			dataA.add(smo.get("tzsy"));
			dataA.add(smo.get("zqtzsy"));
			dataA.add(smo.get("gqtzsy"));
			dataA.add(smo.get("qttzsy"));
			dataA.add(smo.get("gyjzbdsy"));
			dataA.add(smo.get("hdjsy"));
			dataA.add(smo.get("zcczsy"));
			dataA.add(smo.get("qtywsr"));
			dataA.add(smo.get("yyzc"));
			dataA.add(smo.get("ywjglf"));
			dataA.add(smo.get("zggz"));
			dataA.add(smo.get("flf"));
			dataA.add(smo.get("zfgjjhzfbt"));
			dataA.add(smo.get("sjjfj"));
			dataA.add(smo.get("zcjzss"));
			dataA.add(smo.get("qtywzc"));
			dataA.add(smo.get("yylr"));
			dataA.add(smo.get("yywsr"));
			dataA.add(smo.get("yywzc"));
			dataA.add(smo.get("lrze"));
			dataA.add(smo.get("sds"));
			dataA.add(smo.get("jlr"));
			dataA.add(smo.get("ndsytz"));
			dataA.add(smo.get("lclr"));
			dataA.add(smo.get("wfplr"));
			dataA.add(smo.get("ynzzs"));
			dataA.add(smo.get("hxyjzbje"));
			dataA.add(smo.get("yjzbje"));
			dataA.add(smo.get("zbje"));
			dataA.add(smo.get("ygzbjfxjqzchj"));
			dataA.add(smo.get("sjrq"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("一、营业收入");
		columnNames.add("1.利息净收入");
		columnNames.add("利息收入");
		columnNames.add("金融机构往来利息收入");
		columnNames.add("其中：系统内往来利息收入");
		columnNames.add("各项贷款利息收入");
		columnNames.add("债券利息收入");
		columnNames.add("其他利息收入");
		columnNames.add("利息支出");
		columnNames.add("金融机构往来利息支出");
		columnNames.add("其中：系统内往来利息支出");
		columnNames.add("各项存款利息支出");
		columnNames.add("债券利息支出");
		columnNames.add("其他利息支出");
		columnNames.add("2.手续费及佣金净收入");
		columnNames.add("手续费及佣金收入");
		columnNames.add("手续费及佣金支出");
		columnNames.add("3.租赁收益");
		columnNames.add("4.投资收益");
		columnNames.add("债券投资收益");
		columnNames.add("股权投资收益");
		columnNames.add("其他投资收益");
		columnNames.add("5.公允价值变动收益");
		columnNames.add("6.汇兑净收益");
		columnNames.add("7.资产处置收益");
		columnNames.add("8.其他业务收入");
		columnNames.add("二、营业支出");
		columnNames.add("1.业务及管理费");
		columnNames.add("其中:职工工资");
		columnNames.add("福利费");
		columnNames.add("住房公积金和住房补贴");
		columnNames.add("2.税金及附加");
		columnNames.add("3.资产减值损失");
		columnNames.add("4.其他业务支出");
		columnNames.add("三、营业利润");
		columnNames.add("营业外收入（加）");
		columnNames.add("营业外支出（减）");
		columnNames.add("四、利润总额");
		columnNames.add("所得税（减）");
		columnNames.add("五、净利润");
		columnNames.add("年度损益调整（加）");
		columnNames.add("留存利润");
		columnNames.add("六、未分配利润");
		columnNames.add("应纳增值税");
		columnNames.add("核心一级资本净额");
		columnNames.add("一级资本净额");
		columnNames.add("资本净额");
		columnNames.add("应用资本底线及校准后的风险加权资产合计");
		columnNames.add("数据日期");
		List<String> heards = new LinkedList<>();
		heards.add("指标编码");
		heards.add("指标名称");
		heards.add("本外币金额");
		try {
			String fileName = "金融机构（法人基础信息）-利润及资本统计表.xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects,heards);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}		
	
	
	//访问金融机构（法人）基础信息页面
	@RequestMapping(value="/XshowJRJGFRJCXX",method=RequestMethod.GET)
	public ModelAndView XshowJRJGFRJCXXUi(HttpServletRequest request,String status){
		ModelAndView model = new ModelAndView();
		if("dtj".equals(status)) {
			model.setViewName("unitloan/datamanage/jrjgfrjcxx_dtj");
		}else if("dsh".equals(status)) {
			model.setViewName("unitloan/datamanage/jrjgfrjcxx_dsh");
		}
		return model;
	}

	//金融机构（法人）基础信息-基础待提交页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindJRJGFRJCXXJCdtj", method = RequestMethod.POST)
	public void XfindJRJGFRJCXXJCdtj(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		String selectdbhtbm = request.getParameter("selectdbhtbm");
		String zhuangtai = "0";
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		List<XjrjgfrBaseinfo> list = null;
		Page<XjrjgfrBaseinfo> gcPage = (Page<XjrjgfrBaseinfo>) getPageData(TableCodeEnum.JCQKTJ,selectdbhtbm,zhuangtai,page,size);
		JSONObject result = new JSONObject();
		if(gcPage != null) {
			list = gcPage.getContent();
			result.put("rows", list);
			result.put("total", gcPage.getTotalElements());
		}else {
			result.put("rows", new ArrayList<>());
			result.put("total", 0);
		}
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

	//金融机构（法人）基础信息-资产待提交页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindJRJGFRJCXXZCdtj", method = RequestMethod.POST)
	public void XfindJRJGFRJCXXZCdtj(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		String selectdbhtbm = request.getParameter("selectdbhtbm");
		String zhuangtai = "0";
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		List<XjrjgfrAssets> list = null;
		Page<XjrjgfrAssets> gcPage = (Page<XjrjgfrAssets>) getPageData(TableCodeEnum.ZCFZJFXTJ,selectdbhtbm,zhuangtai,page,size);
		JSONObject result = new JSONObject();
		if(gcPage != null) {
			list = gcPage.getContent();
			result.put("rows", list);
			result.put("total", gcPage.getTotalElements());
		}else {
			result.put("rows", new ArrayList<>());
			result.put("total", 0);
		}
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

	//金融机构（法人）基础信息-利润待提交页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindJRJGFRJCXXLRdtj", method = RequestMethod.POST)
	public void XfindJRJGFRJCXXLRdtj(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		String selectdbhtbm = request.getParameter("selectdbhtbm");
		String zhuangtai = "0";
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		List<XjrjgfrProfit> list = null;
		Page<XjrjgfrProfit> gcPage = (Page<XjrjgfrProfit>) getPageData(TableCodeEnum.LRJZBTJ,selectdbhtbm,zhuangtai,page,size);
		JSONObject result = new JSONObject();
		if(gcPage != null) {
			list = gcPage.getContent();
			result.put("rows", list);
			result.put("total", gcPage.getTotalElements());
		}else {
			result.put("rows", new ArrayList<>());
			result.put("total", 0);
		}
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

	//金融机构（法人）基础信息-基础待审核页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindJRJGFRJCXXJCdsh", method = RequestMethod.POST)
	public void XfindJRJGFRJCXXJCdsh(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		String selectdbhtbm = request.getParameter("selectdbhtbm");
		String zhuangtai = "1";
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		List<XjrjgfrBaseinfo> list = null;
		Page<XjrjgfrBaseinfo> gcPage = (Page<XjrjgfrBaseinfo>) getPageData(TableCodeEnum.JCQKTJ,selectdbhtbm,zhuangtai,page,size);
		JSONObject result = new JSONObject();
		if(gcPage != null) {
			list = gcPage.getContent();
			result.put("rows", list);
			result.put("total", gcPage.getTotalElements());
		}else {
			result.put("rows", new ArrayList<>());
			result.put("total", 0);
		}
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

	//金融机构（法人）基础信息-资产待审核页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindJRJGFRJCXXZCdsh", method = RequestMethod.POST)
	public void XfindJRJGFRJCXXZCdsh(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		String selectdbhtbm = request.getParameter("selectdbhtbm");
		String zhuangtai = "1";
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		List<XjrjgfrAssets> list = null;
		Page<XjrjgfrAssets> gcPage = (Page<XjrjgfrAssets>) getPageData(TableCodeEnum.ZCFZJFXTJ,selectdbhtbm,zhuangtai,page,size);
		JSONObject result = new JSONObject();
		if(gcPage != null) {
			list = gcPage.getContent();
			result.put("rows", list);
			result.put("total", gcPage.getTotalElements());
		}else {
			result.put("rows", new ArrayList<>());
			result.put("total", 0);
		}
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

	//金融机构（法人）基础信息-利润待审核页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindJRJGFRJCXXLRdsh", method = RequestMethod.POST)
	public void XfindJRJGFRJCXXLRdsh(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		String selectdbhtbm = request.getParameter("selectdbhtbm");
		String zhuangtai = "1";
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		List<XjrjgfrProfit> list = null;
		Page<XjrjgfrProfit> gcPage = (Page<XjrjgfrProfit>) getPageData(TableCodeEnum.LRJZBTJ,selectdbhtbm,zhuangtai,page,size);
		JSONObject result = new JSONObject();
		if(gcPage != null) {
			list = gcPage.getContent();
			result.put("rows", list);
			result.put("total", gcPage.getTotalElements());
		}else {
			result.put("rows", new ArrayList<>());
			result.put("total", 0);
		}
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

	//获取分页数据方法
	public Page<?> getPageData(TableCodeEnum code,String selectdbhtbm,String zhuangtai,Integer page,Integer size){
		sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		List<SUpOrgInfoSet> list = sus.findAll();
		String departId = departUtil.getDepart(sys_user);
		if(TableCodeEnum.JCQKTJ.equals(code)) {
			Specification<XjrjgfrBaseinfo> querySpecifi = new Specification<XjrjgfrBaseinfo>() {
				@Override
				public Predicate toPredicate(Root<XjrjgfrBaseinfo> root, CriteriaQuery<?> criteriaQuery,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					if (StringUtils.isNotBlank(selectdbhtbm)) {
						predicates.add(criteriaBuilder.like(root.get("gteecontractcode"), "%" + selectdbhtbm.trim() + "%"));
					}
					if("0".equals(zhuangtai)) {
						Predicate p1 = null;
						Predicate p2 = null;
						p1 = criteriaBuilder.equal(root.get("datastatus"),"0");
						p2 = criteriaBuilder.equal(root.get("datastatus"),"2");
						predicates.add(criteriaBuilder.or(p1, p2));
					}else {
						predicates.add(criteriaBuilder.equal(root.get("datastatus"),zhuangtai));
					}
					predicates.add(criteriaBuilder.like(root.get("departid"), departId));
					predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
			// Pageable 接口通常使用的其 PageRequest 实现类. 其中封装了需要分页的信息
			PageRequest pr = new PageRequest(page, size,sort); // intPage从0开始
			return xbs.findAll(querySpecifi, pr);
		}else if(TableCodeEnum.ZCFZJFXTJ.equals(code)) {
			Specification<XjrjgfrAssets> querySpecifi = new Specification<XjrjgfrAssets>() {
				@Override
				public Predicate toPredicate(Root<XjrjgfrAssets> root, CriteriaQuery<?> criteriaQuery,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					if (StringUtils.isNotBlank(selectdbhtbm)) {
						predicates.add(criteriaBuilder.like(root.get("gteecontractcode"), "%" + selectdbhtbm.trim() + "%"));
					}
					if("0".equals(zhuangtai)) {
						Predicate p1 = null;
						Predicate p2 = null;
						p1 = criteriaBuilder.equal(root.get("datastatus"),"0");
						p2 = criteriaBuilder.equal(root.get("datastatus"),"2");
						predicates.add(criteriaBuilder.or(p1, p2));
					}else {
						predicates.add(criteriaBuilder.equal(root.get("datastatus"),zhuangtai));
					}
					predicates.add(criteriaBuilder.like(root.get("departid"), departId));
					predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
			// Pageable 接口通常使用的其 PageRequest 实现类. 其中封装了需要分页的信息
			PageRequest pr = new PageRequest(page, size,sort); // intPage从0开始
			return xas.findAll(querySpecifi, pr);
		}else if(TableCodeEnum.LRJZBTJ.equals(code)) {
			Specification<XjrjgfrProfit> querySpecifi = new Specification<XjrjgfrProfit>() {
				@Override
				public Predicate toPredicate(Root<XjrjgfrProfit> root, CriteriaQuery<?> criteriaQuery,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					if (StringUtils.isNotBlank(selectdbhtbm)) {
						predicates.add(criteriaBuilder.like(root.get("gteecontractcode"), "%" + selectdbhtbm.trim() + "%"));
					}
					if("0".equals(zhuangtai)) {
						Predicate p1 = null;
						Predicate p2 = null;
						p1 = criteriaBuilder.equal(root.get("datastatus"),"0");
						p2 = criteriaBuilder.equal(root.get("datastatus"),"2");
						predicates.add(criteriaBuilder.or(p1, p2));
					}else {
						predicates.add(criteriaBuilder.equal(root.get("datastatus"),zhuangtai));
					}
					predicates.add(criteriaBuilder.like(root.get("departid"), departId));
					predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
			// Pageable 接口通常使用的其 PageRequest 实现类. 其中封装了需要分页的信息
			PageRequest pr = new PageRequest(page, size,sort); // intPage从0开始
			return xps.findAll(querySpecifi, pr);
		}else {
			return null;
		}

	}

	//新增或修改金融机构（法人）基础信息-基础情况统计表
	@RequestMapping(value="/XsaveOrUpdatejrjgfrBaseinfo",method=RequestMethod.POST)
	public void XsaveOrUpdatejrjgfrBaseinfo(HttpServletRequest request,HttpServletResponse response,XjrjgfrBaseinfo XjrjgfrBaseinfo){
		PrintWriter out = null;
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
		    response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			/*JSONObject json = new JSONObject();
			if("1".equals(result)) {
				json.put("msg","1");
				out.write(json.toString());
			}else {
				json.put("msg",result);
				out.write(json.toString());
			}*/
			boolean cunzai = false;
			int total = xbs.findTotal();
			if(total > 1) {
				out.write("2");
			}else {
				if(XjrjgfrBaseinfo.getId().length() == 0 || StringUtils.isBlank(XjrjgfrBaseinfo.getId())) {
					if(total == 1) {
						cunzai = true;
					}else {
						String uuid = Stringutil.getUUid();
						XjrjgfrBaseinfo.setId(uuid);
						XjrjgfrBaseinfo.setDepartid(sys_User.getDepartid());
						XjrjgfrBaseinfo.setOrgid(sys_User.getOrgid());
					}
				}
				if(cunzai) {
					out.write("2");
				}else {
					XjrjgfrBaseinfo.setCheckstatus("0");
					XjrjgfrBaseinfo.setDatastatus("0");
					XjrjgfrBaseinfo.setOperationname("");
					XjrjgfrBaseinfo.setOperator(sys_User.getLoginid());
					XjrjgfrBaseinfo.setOperationtime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
					XjrjgfrBaseinfo xjrjgfrBaseinfo = xbs.SaveOrUpdate(XjrjgfrBaseinfo);
					cct.handleCount("jrjgfrBaseinfo", 0, "add", 1);
					if(xjrjgfrBaseinfo!=null) {
						out.write("1");
					}else {
						out.write("0");
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//新增或修改金融机构（法人）基础信息-资产负债及风险统计表
	@RequestMapping(value="/XsaveOrUpdatejrjgfrAssets",method=RequestMethod.POST)
	public void XsaveOrUpdatejrjgfrAssets(HttpServletRequest request,HttpServletResponse response,XjrjgfrAssets XjrjgfrAssets){
		PrintWriter out = null;
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
		    response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			/*JSONObject json = new JSONObject();
			if("1".equals(result)) {
				json.put("msg","1");
				out.write(json.toString());
			}else {
				json.put("msg",result);
				out.write(json.toString());
			}*/
			boolean cunzai = false;
			int total = xas.findTotal();
			if(total > 1) {
				out.write("2");
			}else {
				if(XjrjgfrAssets.getId().length() == 0 || StringUtils.isBlank(XjrjgfrAssets.getId())) {
					if(total == 1) {
						cunzai = true;
					}else {
						String uuid = Stringutil.getUUid();
						XjrjgfrAssets.setId(uuid);
						XjrjgfrAssets.setDepartid(sys_User.getDepartid());
						XjrjgfrAssets.setOrgid(sys_User.getOrgid());
					}
				}
				if(cunzai) {
					out.write("2");
				}else {
					XjrjgfrAssets.setCheckstatus("0");
					XjrjgfrAssets.setDatastatus("0");
					XjrjgfrAssets.setOperationname("");
					XjrjgfrAssets.setOperator(sys_User.getLoginid());
					XjrjgfrAssets.setOperationtime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
					XjrjgfrAssets xjrjgfrAssets = xas.SaveOrUpdate(XjrjgfrAssets);
					cct.handleCount("jrjgfrAssets", 0, "add", 1);
					if(xjrjgfrAssets!=null) {
						out.write("1");
					}else {
						out.write("0");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//新增或修改金融机构（法人基础信息）-利润及资本统计表
	@RequestMapping(value="/XsaveOrUpdatejrjgfrProfit",method=RequestMethod.POST)
	public void XsaveOrUpdatejrjgfrProfit(HttpServletRequest request,HttpServletResponse response,XjrjgfrProfit XjrjgfrProfit){
		PrintWriter out = null;
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
		    response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			/*JSONObject json = new JSONObject();
			if("1".equals(result)) {
				json.put("msg","1");
				out.write(json.toString());
			}else {
				json.put("msg",result);
				out.write(json.toString());
			}*/
			boolean cunzai = false;
			int total = xps.findTotal();
			if(total > 1) {
				out.write("2");
			}else {
				if(XjrjgfrProfit.getId().length() == 0 || StringUtils.isBlank(XjrjgfrProfit.getId())) {
					if(total == 1) {
						cunzai = true;
					}else {
						String uuid = Stringutil.getUUid();
						XjrjgfrProfit.setId(uuid);
						XjrjgfrProfit.setDepartid(sys_User.getDepartid());
						XjrjgfrProfit.setOrgid(sys_User.getOrgid());
					}
				}
				if(cunzai) {
					out.write("2");
				}else {
					XjrjgfrProfit.setCheckstatus("0");
					XjrjgfrProfit.setDatastatus("0");
					XjrjgfrProfit.setOperationname("");
					XjrjgfrProfit.setOperator(sys_User.getLoginid());
					XjrjgfrProfit.setOperationtime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
					XjrjgfrProfit xjrjgfrProfit = xps.SaveOrUpdate(XjrjgfrProfit);
					cct.handleCount("jrjgfrProfit", 0, "add", 1);
					if(xjrjgfrProfit!=null) {
						out.write("1");
					}else {
						out.write("0");
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//删除金融机构（法人）基础信息-基础情况统计表
	@RequestMapping(value="/XdeletejrjgfrBaseinfo",method=RequestMethod.POST)
	public void XdeletejrjgfrBaseinfo(HttpServletRequest request,HttpServletResponse response,String deletetype,String deleteid){
		PrintWriter out = null;
		try {
			response.setContentType("text/html");
		    response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//result = xs.delete();
				String sql = "delete from xjrjgfrBaseinfo where datastatus='0' or datastatus='2' and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "delete from xjrjgfrBaseinfo where id in(";
					for (int i = 0; i < idarr.length; i++) {
						//xs.delete(idarr[i]);
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
					//result = 1;
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfrBaseinfo", 0, "reduce", 1);
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfrBaseinfo", 0, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//删除金融机构（法人）基础信息-资产负债及风险统计表
	@RequestMapping(value="/XdeletejrjgfrAssets",method=RequestMethod.POST)
	public void XdeletejrjgfrAssets(HttpServletRequest request,HttpServletResponse response,String deletetype,String deleteid){
		PrintWriter out = null;
		try {
			response.setContentType("text/html");
		    response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//result = xs.delete();
				String sql = "delete from xjrjgfrAssets where datastatus='0' or datastatus='2' and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "delete from xjrjgfrAssets where id in(";
					for (int i = 0; i < idarr.length; i++) {
						//xs.delete(idarr[i]);
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
					//result = 1;
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfrAssets", 0, "reduce", 1);
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfrAssets", 0, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//删除金融机构（法人基础信息）-利润及资本统计表
	@RequestMapping(value="/XdeletejrjgfrProfit",method=RequestMethod.POST)
	public void XdeletejrjgfrProfit(HttpServletRequest request,HttpServletResponse response,String deletetype,String deleteid){
		PrintWriter out = null;
		try {
			response.setContentType("text/html");
		    response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//result = xs.delete();
				String sql = "delete from xjrjgfrProfit where datastatus='0' or datastatus='2' and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "delete from xjrjgfrProfit where id in(";
					for (int i = 0; i < idarr.length; i++) {
						//xs.delete(idarr[i]);
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
					//result = 1;
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfrProfit", 0, "reduce", 1);
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfrProfit", 0, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//校验金融机构（法人）基础信息-资产负债及风险统计表
	@RequestMapping(value="/XcheckjrjgfrAssets",method=RequestMethod.POST)
	public void XcheckjrjgfrAssets(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String checktype = request.getParameter("checktype");

		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			String result = "0";
			String departId = departUtil.getDepart(sys_user);
			List<String> checknumList = xCheckRuleService.getChecknum("xjrjgfrassets");
			if("all".equals(checktype)) {
				result = CheckDataUtils.jiaoYanShuJu(TableCodeEnum.ZCFZJFXTJ,xas,customSqlUtil,checknumList,departId);
				if("1".equals(result)) {
					//xas.updateXjrjgfrAssetsOnCheckstatus("1");
					//xas.updateXjrjgfrAssetsOnDatastatus("3");
					out.write(result);
				}else {
					//xas.updateXjrjgfrAssetsOnCheckstatus("2");
					List<SUpOrgInfoSet> list = sus.findAll();
					String messagepath = "C:";
					if(list.size() > 0) {
						messagepath = list.get(0).getMessagepath();
					}
					request.getSession().setAttribute("messagepath",messagepath);
					DownloadUtil.downLoad("校验结果:"+result, messagepath+File.separator+"checkout", "金融机构（法人）基础信息-资产负债及风险统计表校验结果.txt");
					out.write(result);
				}
			}else if("some".equals(checktype)) {
				String rows = request.getParameter("rows");
				// 先转成json数组再将json转化成list数组
				JSONArray jsonArray = JSONArray.fromObject(rows);
				@SuppressWarnings("unchecked")
				List<XjrjgfrAssets> list = JSONArray.toList(jsonArray, new XjrjgfrAssets(), new JsonConfig());
				result = CheckDataUtils.jiaoYanShuJu(list,TableCodeEnum.ZCFZJFXTJ,xas,customSqlUtil,checknumList);
				if("1".equals(result)) {
					/*for(int i=0;i<list.size();i++) {
						list.get(i).setCheckstatus("1");
						//list.get(i).setDatastatus("3");
					}
					xas.Save(list);*/
					out.write(result);
				}else {
					/*for(int i=0;i<list.size();i++) {
						list.get(i).setCheckstatus("2");
					}
					xas.Save(list);*/
					List<SUpOrgInfoSet> olist = sus.findAll();
					String messagepath = "C:";
					if(olist.size() > 0) {
						messagepath = olist.get(0).getMessagepath();
					}
					request.getSession().setAttribute("messagepath",messagepath);
					DownloadUtil.downLoad("校验结果:"+result, messagepath+File.separator+"checkout", "金融机构（法人）基础信息-资产负债及风险统计表校验结果.txt");
					out.write(result);
				}
			}

			//out.write(result);
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}

	//下载校验金融机构（法人）基础信息-资产负债及风险统计表结果
	@RequestMapping(value="/XdownFileCheckjrjgfrAssets",method=RequestMethod.GET)
	public void XdownFileCheckjrjgfrAssets(HttpServletRequest request,HttpServletResponse response){
		InputStream input = null;
		OutputStream output = null;
		try {
			//String name = request.getParameter("name");
			//String messagepath = request.getParameter("messagepath");
			Object messagepath = request.getSession().getAttribute("messagepath");
			String filename = "金融机构（法人）基础信息-资产负债及风险统计表校验结果.txt";
			File file = new File(messagepath.toString()+File.separator+"checkout"+File.separator+filename);
			if (file.exists()) {
				response.setCharacterEncoding("GBK");
				response.setContentType("application/octet-stream;charset=GBK");
				response.setHeader("Content-Disposition","attachment;filename="+ new String(filename.getBytes("GBK"), "ISO8859-1"));
				input = new FileInputStream(messagepath+File.separator+"checkout"+File.separator+filename);
				file.setWritable(true, false);
				int len = 0;
				byte[] buffer = new byte[1024];
				output = response.getOutputStream();
				while ((len = input.read(buffer)) > 0) {
					output.write(buffer, 0, len);
				}
				output.flush();
				output.close();
				input.close();
				file.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (input != null) {
					input.close();
				}

				if (output != null) {
					output.flush();
					output.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	//校验金融机构（法人）基础信息-基础情况统计表
	@RequestMapping(value="/XcheckjrjgfrBaseinfo",method=RequestMethod.POST)
	public void XcheckjrjgfrBaseinfo(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String checktype = request.getParameter("checktype");

		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			String result = "0";
			String departId = departUtil.getDepart(sys_user);
			List<String> checknumList = xCheckRuleService.getChecknum("xjrjgfrbaseinfo");
			if("all".equals(checktype)) {
				result = CheckDataUtils.jiaoYanShuJu(TableCodeEnum.JCQKTJ,xbs,customSqlUtil,checknumList,departId);
				if("1".equals(result)) {
					out.write(result);
				}else {
					List<SUpOrgInfoSet> list = sus.findAll();
					String messagepath = "C:";
					if(list.size() > 0) {
						messagepath = list.get(0).getMessagepath();
					}
					request.getSession().setAttribute("messagepath",messagepath);
					DownloadUtil.downLoad("校验结果:"+result, messagepath+File.separator+"checkout", "金融机构（法人）基础信息-基础情况统计表校验结果.txt");
					out.write(result);
				}
			}else if("some".equals(checktype)) {
				String rows = request.getParameter("rows");
				// 先转成json数组再将json转化成list数组
				JSONArray jsonArray = JSONArray.fromObject(rows);
				@SuppressWarnings("unchecked")
				List<XjrjgfrBaseinfo> list = JSONArray.toList(jsonArray, new XjrjgfrBaseinfo(), new JsonConfig());
				result = CheckDataUtils.jiaoYanShuJu(list,TableCodeEnum.JCQKTJ,xbs,customSqlUtil,checknumList);
				if("1".equals(result)) {

					out.write(result);
				}else {

					List<SUpOrgInfoSet> olist = sus.findAll();
					String messagepath = "C:";
					if(olist.size() > 0) {
						messagepath = olist.get(0).getMessagepath();
					}
					request.getSession().setAttribute("messagepath",messagepath);
					DownloadUtil.downLoad("校验结果:"+result, messagepath+File.separator+"checkout", "金融机构（法人）基础信息-基础情况统计表校验结果.txt");
					out.write(result);
				}
			}

			//out.write(result);
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}

	//下载校验金融机构（法人）基础信息-基础情况统计表 结果
	@RequestMapping(value="/XdownFileCheckjrjgfrBaseinfo",method=RequestMethod.GET)
	public void XdownFileCheckjrjgfrBaseinfo(HttpServletRequest request,HttpServletResponse response){
		InputStream input = null;
		OutputStream output = null;
		try {
			//String name = request.getParameter("name");
			//String messagepath = request.getParameter("messagepath");
			Object messagepath = request.getSession().getAttribute("messagepath");
			String filename = "金融机构（法人）基础信息-基础情况统计表校验结果.txt";
			File file = new File(messagepath.toString()+File.separator+"checkout"+File.separator+filename);
			if (file.exists()) {
				response.setCharacterEncoding("GBK");
				response.setContentType("application/octet-stream;charset=GBK");
				response.setHeader("Content-Disposition","attachment;filename="+ new String(filename.getBytes("GBK"), "ISO8859-1"));
				input = new FileInputStream(messagepath+File.separator+"checkout"+File.separator+filename);
				file.setWritable(true, false);
				int len = 0;
				byte[] buffer = new byte[1024];
				output = response.getOutputStream();
				while ((len = input.read(buffer)) > 0) {
					output.write(buffer, 0, len);
				}
				output.flush();
				output.close();
				input.close();
				file.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (input != null) {
					input.close();
				}

				if (output != null) {
					output.flush();
					output.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	//校验金融机构（法人基础信息）-利润及资本统计表
	@RequestMapping(value="/XcheckjrjgfrProfit",method=RequestMethod.POST)
	public void XcheckjrjgfrProfit(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String checktype = request.getParameter("checktype");

		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			String result = "0";
			String departId = departUtil.getDepart(sys_user);
			int total = bas.findTotal();
			List<String> checknumList = xCheckRuleService.getChecknum("xjrjgfrprofit");
			if(total > 0) {
				if("all".equals(checktype)) {
					result = CheckDataUtils.jiaoYanShuJu(TableCodeEnum.LRJZBTJ,xps,customSqlUtil,checknumList,departId);
					if("1".equals(result)) {
						//xps.updateXjrjgfrProfitOnCheckstatus("1");
						//xps.updateXjrjgfrProfitOnDatastatus("3");
						out.write(result);
					}else {
						//xps.updateXjrjgfrProfitOnCheckstatus("2");
						List<SUpOrgInfoSet> list = sus.findAll();
						String messagepath = "C:";
						if(list.size() > 0) {
							messagepath = list.get(0).getMessagepath();
						}
						request.getSession().setAttribute("messagepath",messagepath);
						DownloadUtil.downLoad("校验结果:"+result, messagepath+File.separator+"checkout", "金融机构（法人基础信息）-利润及资本统计表校验结果.txt");
						out.write(result);
					}
				}else if("some".equals(checktype)) {
					String rows = request.getParameter("rows");
					// 先转成json数组再将json转化成list数组
					JSONArray jsonArray = JSONArray.fromObject(rows);
					@SuppressWarnings("unchecked")
					List<XjrjgfrProfit> list = JSONArray.toList(jsonArray, new XjrjgfrProfit(), new JsonConfig());
					result = CheckDataUtils.jiaoYanShuJu(list,TableCodeEnum.LRJZBTJ,xps,customSqlUtil,checknumList);
					if("1".equals(result)) {
						/*for(int i=0;i<list.size();i++) {
							list.get(i).setCheckstatus("1");
							//list.get(i).setDatastatus("3");
						}
						xps.Save(list);*/
						out.write(result);
					}else {
						/*for(int i=0;i<list.size();i++) {
							list.get(i).setCheckstatus("2");
						}
						xps.Save(list);*/
						List<SUpOrgInfoSet> olist = sus.findAll();
						String messagepath = "C:";
						if(olist.size() > 0) {
							messagepath = olist.get(0).getMessagepath();
						}
						request.getSession().setAttribute("messagepath",messagepath);
						DownloadUtil.downLoad("校验结果:"+result, messagepath+File.separator+"checkout", "金融机构（法人基础信息）-利润及资本统计表校验结果.txt");
						out.write(result);
					}
				}
			}else {
				out.write("2");
			}

			//out.write(result);
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}

	//下载校验金融机构（法人基础信息）-利润及资本统计表结果
	@RequestMapping(value="/XdownFileCheckjrjgfrProfit",method=RequestMethod.GET)
	public void XdownFileCheckjrjgfrProfit(HttpServletRequest request,HttpServletResponse response){
		InputStream input = null;
		OutputStream output = null;
		try {
			//String name = request.getParameter("name");
			//String messagepath = request.getParameter("messagepath");
			Object messagepath = request.getSession().getAttribute("messagepath");
			String filename = "金融机构（法人基础信息）-利润及资本统计表校验结果.txt";
			File file = new File(messagepath.toString()+File.separator+"checkout"+File.separator+filename);
			if (file.exists()) {
				response.setCharacterEncoding("GBK");
				response.setContentType("application/octet-stream;charset=GBK");
				response.setHeader("Content-Disposition","attachment;filename="+ new String(filename.getBytes("GBK"), "ISO8859-1"));
				input = new FileInputStream(messagepath+File.separator+"checkout"+File.separator+filename);
				file.setWritable(true, false);
				int len = 0;
				byte[] buffer = new byte[1024];
				output = response.getOutputStream();
				while ((len = input.read(buffer)) > 0) {
					output.write(buffer, 0, len);
				}
				output.flush();
				output.close();
				input.close();
				file.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (input != null) {
					input.close();
				}

				if (output != null) {
					output.flush();
					output.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	//提交资产
	@RequestMapping(value="/XtijiaojrjgfrAssets",method=RequestMethod.POST)
	public void XtijiaojrjgfrAssets(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				result = xas.updateXjrjgfrAssetsOnDatastatus("1",sys_User.getLoginid(),departId);
				//result = ServiceTemplate.YeWuMoBan(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					List<String> idList = new ArrayList<>();
					for (int i = 0; i < idarr.length; i++) {
						idList.add(idarr[i]);
					}
					result = xas.updateXjrjgfrAssetsOnDatastatus("1",sys_User.getLoginid(),idList);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfrAssets", 1, "reduce", 1);
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfrAssets", 1, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//提交基础
	@RequestMapping(value="/XtijiaojrjgfrBaseinfo",method=RequestMethod.POST)
	public void XtijiaojrjgfrBaseinfo(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				result = xbs.updateXjrjgfrBaseinfoOnDatastatus("1",sys_User.getLoginid(),departId);
				//result = ServiceTemplate.YeWuMoBan(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					List<String> idList = new ArrayList<>();
					for (int i = 0; i < idarr.length; i++) {
						idList.add(idarr[i]);
					}
					result = xbs.updateXjrjgfrBaseinfoOnDatastatus("1",sys_User.getLoginid(),idList);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfrBaseinfo", 1, "reduce", 1);
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfrBaseinfo", 1, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//提交利润
	@RequestMapping(value="/XtijiaojrjgfrProfit",method=RequestMethod.POST)
	public void XtijiaojrjgfrProfit(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				result = xps.updateXjrjgfrProfitOnDatastatus("1",sys_User.getLoginid(),departId);
				//result = ServiceTemplate.YeWuMoBan(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					List<String> idList = new ArrayList<>();
					for (int i = 0; i < idarr.length; i++) {
						idList.add(idarr[i]);
					}
					result = xps.updateXjrjgfrProfitOnDatastatus("1",sys_User.getLoginid(),idList);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfrProfit", 1, "reduce", 1);
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfrProfit", 1, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//申请删除资产
	@RequestMapping(value="/XshenqingshanchujrjgfrAssets",method=RequestMethod.POST)
	public void XshenqingshanchujrjgfrAssets(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xjrjgfrAssets set operationname='申请删除',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and (operationname is null or operationname='') and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xjrjgfrAssets set operationname='申请删除',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}	
	
	//申请删除基础
	@RequestMapping(value="/XshenqingshanchujrjgfrBaseinfo",method=RequestMethod.POST)
	public void XshenqingshanchujrjgfrBaseinfo(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xjrjgfrBaseinfo set operationname='申请删除',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and (operationname is null or operationname='') and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xjrjgfrBaseinfo set operationname='申请删除',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}		
		
		//申请删除利润
		@RequestMapping(value="/XshenqingshanchujrjgfrProfit",method=RequestMethod.POST)
		public void XshenqingshanchujrjgfrProfit(HttpServletRequest request,HttpServletResponse response){
			PrintWriter out = null;
			String deletetype = request.getParameter("deletetype");
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			try {
				response.setContentType("text/html");
				response.setContentType("text/plain; charset=utf-8");
				out = response.getWriter();
				Integer result = 0;
				String departId = departUtil.getDepart(sys_user);
				if("all".equals(deletetype)) {
					String sql = "update xjrjgfrProfit set operationname='申请删除',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and (operationname is null or operationname='') and departid like '"+departId+"'";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}else if("some".equals(deletetype)) {
					String deleteid = request.getParameter("deleteid");
					if (StringUtils.isNotBlank(deleteid)) {
						String[] idarr = deleteid.split("-");
						String sql = "update xjrjgfrProfit set operationname='申请删除',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
						for (int i = 0; i < idarr.length; i++) {
							if(i == 0) {
								sql = sql + "'" + idarr[i] + "'";
							}else {
								sql = sql +"," + "'" + idarr[i] + "'";
							}
						}
						sql = sql +")";
						//result = ServiceTemplate.YeWuMoBan(sql);
						result = jt.update(sql);
					}
				}
				if(result > 0) {
					out.write("1");
				}else {
					out.write("0");
				}
			}catch (Exception e) {
				e.printStackTrace();
				out.write("0");
			}
		}
	
	//申请修改资产
	@RequestMapping(value="/XshenqingxiugaijrjgfrAssets",method=RequestMethod.POST)
	public void XshenqingxiugaijrjgfrAssets(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xjrjgfrAssets set operationname='申请修改',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and (operationname is null or operationname='') and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xjrjgfrAssets set operationname='申请修改',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//申请修改基础
	@RequestMapping(value="/XshenqingxiugaijrjgfrBaseinfo",method=RequestMethod.POST)
	public void XshenqingxiugaijrjgfrBaseinfo(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xjrjgfrBaseinfo set operationname='申请修改',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and (operationname is null or operationname='') and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xjrjgfrBaseinfo set operationname='申请修改',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}		
		
	//申请修改利润
	@RequestMapping(value="/XshenqingxiugaijrjgfrProfit",method=RequestMethod.POST)
	public void XshenqingxiugaijrjgfrProfit(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xjrjgfrProfit set operationname='申请修改',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and (operationname is null or operationname='') and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xjrjgfrProfit set operationname='申请修改',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}				
	
	//同意申请资产
	@RequestMapping(value="/XtongyishenqingjrjgfrAssets",method=RequestMethod.POST)
	public void XtongyishenqingjrjgfrAssets(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xjrjgfrAssets set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where datastatus='1' and and operator !='"+sys_User.getLoginid()+"' and (operationname is not null or operationname!='')";
				String sql = "delete from xjrjgfrAssets where datastatus='1' and operator<>'"+sys_User.getLoginid()+"' and operationname='申请删除' and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					//String sql = "update xjrjgfrAssets set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where id in(";
					String sql = "delete from xjrjgfrAssets where datastatus='1' and operator<>'"+sys_User.getLoginid()+"' and operationname='申请删除' and id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//同意申请基础
	@RequestMapping(value="/XtongyishenqingjrjgfrBaseinfo",method=RequestMethod.POST)
	public void XtongyishenqingjrjgfrBaseinfo(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xjrjgfrBaseinfo set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where datastatus='1' and operator !='"+sys_User.getLoginid()+"' and (operationname is not null or operationname!='')";
				String sql = "delete from xjrjgfrBaseinfo where datastatus='1' and operator<>'"+sys_User.getLoginid()+"' and operationname='申请删除' and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					//String sql = "update xjrjgfrBaseinfo set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where id in(";
					String sql = "delete from xjrjgfrBaseinfo where datastatus='1' and operator<>'"+sys_User.getLoginid()+"' and operationname='申请删除' and id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}		
		
	//同意申请利润
	@RequestMapping(value="/XtongyishenqingjrjgfrProfit",method=RequestMethod.POST)
	public void XtongyishenqingjrjgfrProfit(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xjrjgfrProfit set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where datastatus='1' and operator !='"+sys_User.getLoginid()+"' and (operationname is not null or operationname!='')";
				String sql = "delete from xjrjgfrProfit where datastatus='1' and operator<>'"+sys_User.getLoginid()+"' and operationname='申请删除' and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					//String sql = "update xjrjgfrProfit set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where id in(";
					String sql = "delete from xjrjgfrProfit where datastatus='1' and operator <>'"+sys_User.getLoginid()+"' and operationname='申请删除' and id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}				
		
	//拒绝申请资产
	@RequestMapping(value="/XjujueshenqingjrjgfrAssets",method=RequestMethod.POST)
	public void XjujueshenqingjrjgfrAssets(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xjrjgfrAssets set operationname='',datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and operator <>'"+sys_User.getLoginid()+"' and operationname='申请删除' and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xjrjgfrAssets set operationname='',datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//拒绝申请基础
	@RequestMapping(value="/XjujueshenqingjrjgfrBaseinfo",method=RequestMethod.POST)
	public void XjujueshenqingjrjgfrBaseinfo(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xjrjgfrBaseinfo set operationname='',datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and operator <>'"+sys_User.getLoginid()+"' and operationname='申请删除' and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xjrjgfrBaseinfo set operationname='',datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}		
		
	//拒绝申请利润
	@RequestMapping(value="/XjujueshenqingjrjgfrProfit",method=RequestMethod.POST)
	public void XjujueshenqingjrjgfrProfit(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xjrjgfrProfit set operationname='',datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and operator <>'"+sys_User.getLoginid()+"' and operationname='申请删除' and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xjrjgfrProfit set operationname='',datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//审核通过资产
	@RequestMapping(value="/XshenhetongguojrjgfrAssets",method=RequestMethod.POST)
	public void XshenhetongguojrjgfrAssets(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xjrjgfrAssets set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and operator <>'"+sys_User.getLoginid()+"' and (operationname='' or operationname is null)";
				String sql = "update xjrjgfrAssets set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and (operationname='' or operationname is null)"+ " and departid like '"+departId+"' and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "update xjrjgfrAssets set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfrAssets", 3, "add", result);
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfrAssets", 3, "add", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//审核通过基础
	@RequestMapping(value="/XshenhetongguojrjgfrBaseinfo",method=RequestMethod.POST)
	public void XshenhetongguojrjgfrBaseinfo(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xjrjgfrBaseinfo set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and operator <>'"+sys_User.getLoginid()+"' and (operationname='' or operationname is null)";
				String sql = "update xjrjgfrBaseinfo set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and departid like '"+departId+"' and (operationname='' or operationname is null)" + " and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "update xjrjgfrBaseinfo set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfrBaseinfo", 3, "add", result);
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfrBaseinfo", 3, "add", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}		

	//审核通过利润
	@RequestMapping(value="/XshenhetongguojrjgfrProfit",method=RequestMethod.POST)
	public void XshenhetongguojrjgfrProfit(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xjrjgfrProfit set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and operator <>'"+sys_User.getLoginid()+"' and (operationname='' or operationname is null)";
				String sql = "update xjrjgfrProfit set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and departid like '"+departId+"' and (operationname='' or operationname is null)" + " and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "update xjrjgfrProfit set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfrProfit", 3, "add", result);
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfrProfit", 3, "add", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}							

	//审核不通过资产
	@RequestMapping(value="/XshenhebutongguojrjgfrAssets",method=RequestMethod.POST)
	public void XshenhebutongguojrjgfrAssets(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		String yuanyin = request.getParameter("yuanyin");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xjrjgfrAssets set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and operator !='"+sys_User.getLoginid()+"' and (operationname='' or operationname is null)";
				String sql = "update xjrjgfrAssets set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and departid like '"+departId+"' and (operationname='' or operationname is null)"+ " and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xjrjgfrAssets set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//审核不通过基础
	@RequestMapping(value="/XshenhebutongguojrjgfrBaseinfo",method=RequestMethod.POST)
	public void XshenhebutongguojrjgfrBaseinfo(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		String yuanyin = request.getParameter("yuanyin");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xjrjgfrBaseinfo set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and operator !='"+sys_User.getLoginid()+"' and (operationname='' or operationname is null)";
				String sql = "update xjrjgfrBaseinfo set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and departid like '"+departId+"' and (operationname='' or operationname is null)"+ " and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xjrjgfrBaseinfo set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//审核不通过利润
	@RequestMapping(value="/XshenhebutongguojrjgfrProfit",method=RequestMethod.POST)
	public void XshenhebutongguojrjgfrProfit(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		String yuanyin = request.getParameter("yuanyin");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xjrjgfrProfit set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and operator !='"+sys_User.getLoginid()+"' and (operationname='' or operationname is null)";
				String sql = "update xjrjgfrProfit set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and departid like '"+departId+"' and (operationname='' or operationname is null)"+ " and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xjrjgfrProfit set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	
	//访问单位贷款担保物信息生成报文页面
	@RequestMapping(value="/XshowJRJGFRJCXXRP",method=RequestMethod.GET)
	public ModelAndView XshowJRJGFRJCXXRPUi(HttpServletRequest request){
		String status = request.getParameter("status");
		ModelAndView model = new ModelAndView();
		model.addObject("datamanege",status);
		model.setViewName("unitloan/createreport/jrjgfrjcxx_bw");
		return model;
	}

	//金融机构（法人）基础信息-基础生成报文页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindJRJGFRJCXXJCscbw", method = RequestMethod.POST)
	public void XfindJRJGFRJCXXJCscbw(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		//String selectdbhtbm = request.getParameter("selectdbhtbm");
		String zhuangtai = "3";
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		Specification<XjrjgfrBaseinfo> querySpecifi = new Specification<XjrjgfrBaseinfo>() {
			@Override
			public Predicate toPredicate(Root<XjrjgfrBaseinfo> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				/*if (StringUtils.isNotBlank(selectdbhtbm)) {
					predicates.add(criteriaBuilder.like(root.get("gteecontractcode"), "%" + selectdbhtbm.trim() + "%"));
				}*/
				predicates.add(criteriaBuilder.equal(root.get("datastatus"),zhuangtai));
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		// Pageable 接口通常使用的其 PageRequest 实现类. 其中封装了需要分页的信息
		PageRequest pr = new PageRequest(page, size); // intPage从0开始
		List<XjrjgfrBaseinfo> list = null;
		Page<XjrjgfrBaseinfo> gcPage = xbs.findAll(querySpecifi, pr);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		result.put("rows", list);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}
	//金融机构（法人）基础信息-资产生成报文页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindJRJGFRJCXXZCscbw", method = RequestMethod.POST)
	public void XfindJRJGFRJCXXZCscbw(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		//String selectdbhtbm = request.getParameter("selectdbhtbm");
		String zhuangtai = "3";
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		Specification<XjrjgfrAssets> querySpecifi = new Specification<XjrjgfrAssets>() {
			@Override
			public Predicate toPredicate(Root<XjrjgfrAssets> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				/*if (StringUtils.isNotBlank(selectdbhtbm)) {
					predicates.add(criteriaBuilder.like(root.get("gteecontractcode"), "%" + selectdbhtbm.trim() + "%"));
				}*/
				predicates.add(criteriaBuilder.equal(root.get("datastatus"),zhuangtai));
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		// Pageable 接口通常使用的其 PageRequest 实现类. 其中封装了需要分页的信息
		PageRequest pr = new PageRequest(page, size); // intPage从0开始
		List<XjrjgfrAssets> list = null;
		Page<XjrjgfrAssets> gcPage = xas.findAll(querySpecifi, pr);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		result.put("rows", list);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

	//金融机构（法人）基础信息-利润生成报文页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindJRJGFRJCXXLRscbw", method = RequestMethod.POST)
	public void XfindJRJGFRJCXXLRscbw(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		//String selectdbhtbm = request.getParameter("selectdbhtbm");
		String zhuangtai = "3";
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		Specification<XjrjgfrProfit> querySpecifi = new Specification<XjrjgfrProfit>() {
			@Override
			public Predicate toPredicate(Root<XjrjgfrProfit> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				/*if (StringUtils.isNotBlank(selectdbhtbm)) {
					predicates.add(criteriaBuilder.like(root.get("gteecontractcode"), "%" + selectdbhtbm.trim() + "%"));
				}*/
				predicates.add(criteriaBuilder.equal(root.get("datastatus"),zhuangtai));
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		// Pageable 接口通常使用的其 PageRequest 实现类. 其中封装了需要分页的信息
		PageRequest pr = new PageRequest(page, size); // intPage从0开始
		List<XjrjgfrProfit> list = null;
		Page<XjrjgfrProfit> gcPage = xps.findAll(querySpecifi, pr);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		result.put("rows", list);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

	//生成报文
	@RequestMapping(value = "/Xcreatemessagejrjgfr", method = RequestMethod.POST)
    public void Xcreatemessagejrjgfr(HttpServletRequest request,HttpServletResponse response,HttpSession session){
		PrintWriter out = null;
		String checktype = request.getParameter("checktype");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			int result = 0;
			List<SUpOrgInfoSet> olist = sus.findAll();
			String messagepath = "C:";
			//String bankcodewangdian = "";
			String bsjgdm = "91310000607342023U";
			int bianhao = 0;
			if(olist.size() > 0) {
				messagepath = olist.get(0).getMessagepath();
				//bankcodewangdian = olist.get(0).getBankcodewangdian();
				bsjgdm = olist.get(0).getBankcodewangdian();
			}
			String date = request.getParameter("date");
			String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
			String target=messagepath+ File.separator+"report"+File.separator+localDate;
			//String filename = bsjgdm+"_JRJGFR_"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))+".xls";

			String fileName = "B" + "J" + bsjgdm + localDate;
			String iFileName = "B" + "I" + bsjgdm + localDate;
			
			String ywsjbz = "1";//业务数据标志
			
			List<Object> list = null;
			
			//金融机构（法人）基础信息-基础情况统计表
			list = Stringutil.getReportList(new XjrjgfrBaseinfo(), "xjrjgfrbaseinfo");
			String fileNameBase = fileName + "20201.dat";
			String iFileNameBase = iFileName + "20201.idx";
			fileNameBase =DownloadUtil.writeZIP1(list,target,fileNameBase,iFileNameBase,ywsjbz,bsjgdm);
			
			Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
//            boolean isDepart="yes".equals(olist.get(0).getIsusedepart())?true:false;
            String departId = "%%";
//            if (isDepart){
//                departId =sys_user.getDepartid();
//            }
            String orgid = sys_user.getOrgid();
            commonService.moveToHistoryTable("xjrjgfrbaseinfo",orgid,departId);
            XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileNameBase,target+ File.separator + fileNameBase,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
            commonService.save(reportInfo);
            countUtil.handleCount("xjrjgfrbaseinfo",3,"reduce",list.size());
            result += list.size();
            list.clear();
            
            //金融机构（法人）基础信息-资产负债及风险统计表
			list = Stringutil.getReportList(new XjrjgfrAssets(), "xjrjgfrassets");
			String fileNameAssets = fileName + "20202.dat";
			String iFileNameAssets = iFileName + "20202.idx";
			fileNameAssets =DownloadUtil.writeZIP1(list,target,fileNameAssets,iFileNameAssets,ywsjbz,bsjgdm);
			
            commonService.moveToHistoryTable("xjrjgfrassets",orgid,departId);
            XreportInfo reportInfo2 = new XreportInfo(Stringutil.getUUid(),fileNameAssets,target+ File.separator + fileNameAssets,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
            commonService.save(reportInfo2);
            countUtil.handleCount("xjrjgfrassets",3,"reduce",list.size());
            result += list.size();
            list.clear();
            
            //金融机构（法人基础信息）-利润及资本统计表
            list = Stringutil.getReportList(new XjrjgfrProfit(), "xjrjgfrprofit");
			String fileNameProfit = fileName + "20203.dat";
			String iFileNameProfit = iFileName + "20203.idx";
			fileNameProfit =DownloadUtil.writeZIP1(list,target,fileNameProfit,iFileNameProfit,ywsjbz,bsjgdm);
			
            commonService.moveToHistoryTable("xjrjgfrprofit",orgid,departId);
            XreportInfo reportInfo3 = new XreportInfo(Stringutil.getUUid(),fileNameProfit,target+ File.separator + fileNameProfit,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
            commonService.save(reportInfo3);
            countUtil.handleCount("xjrjgfrprofit",3,"reduce",list.size());
            result += list.size();
            list.clear();
            
            out.write(String.valueOf(result));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//查询进度
	@RequestMapping(value = "/exportJinDuTiao", method = RequestMethod.POST)
	public void exportJinDuTiao(HttpServletResponse response,HttpServletRequest request) {
		Object exportedFlag = request.getSession().getAttribute("exportedFlag");
		PrintWriter out = null;
		try {
			out = response.getWriter();
	        if("success".equals(exportedFlag)){
	        	request.getSession().setAttribute("exportedFlag", "error");
	        	out.write("success");
	        }else{
	        	out.write("error");
	        }
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			out.flush();
			out.close();
		}
	}
	
	public void tianChongShuJu(HSSFSheet sheet,int rowCount,int bianhao,String lie1,String lie2) {
		HSSFRow rowPlus = sheet.createRow(rowCount); //从第二行开始创建真实数据
		HSSFCell cell0 = rowPlus.createCell(0);
		cell0.setCellValue(bianhao);
		HSSFCell cell1 = rowPlus.createCell(1);
		cell1.setCellValue(lie1);
		HSSFCell cell2 = rowPlus.createCell(2);
		cell2.setCellValue(lie2);
	}
}
