package com.geping.etl.UNITLOAN.controller.dataManager;

import java.util.*;

public class XfieldEnum {

    public enum Xcldkxx {
        financeorgcode("financeorgcode","金融机构代码","A"),
        financeorginnum("financeorginnum","内部机构号","B"),
        financeorgareacode("financeorgareacode","金融机构地区代码","C"),
        isfarmerloan("isfarmerloan","借款人证件类型","D"),
        brroweridnum("brroweridnum","借款人证件代码","E"),
        isgreenloan("isgreenloan","借款人国民经济部门","F"),
        brrowerindustry("brrowerindustry","借款人行业","G"),
        brrowerareacode("brrowerareacode","借款人地区代码","H"),
        inverstoreconomy("inverstoreconomy","借款人经济成分","I"),
        enterprisescale("enterprisescale","借款人企业规模","J"),
        receiptcode("receiptcode","贷款借据编码","K"),
        contractcode("contractcode","贷款合同编码","L"),
        productcetegory("productcetegory","贷款产品类别","M"),
        loanactualdirection("loanactualdirection","贷款实际投向","N"),
        loanstartdate("loanstartdate","贷款发放日期","O"),
        loanenddate("loanenddate","贷款到期日期","P"),
        extensiondate("extensiondate","贷款展期到期日期","Q"),
        currency("currency","币种","R"),
        receiptbalance("receiptbalance","贷款余额","S"),
        receiptcnybalance("receiptcnybalance","贷款余额折人民币","T"),
        interestisfixed("interestisfixed","利率是否固定","U"),
        interestislevel("interestislevel","利率水平","V"),
        fixpricetype("fixpricetype","贷款定价基准类型","W"),
        baseinterest("baseinterest","基准利率","X"),
        loanfinancesupport("loanfinancesupport","贷款财政扶持方式","Y"),
        loaninterestrepricedate("loaninterestrepricedate","贷款利率重新定价日","Z"),
        guaranteemethod("guaranteemethod","贷款担保方式","AA"),
        isplatformloan("isplatformloan","是否首次贷款","AB"),
        loanquality("loanquality","贷款质量","AC"),
        loanstatus("loanstatus","贷款状态","AD"),
        overduetype("overduetype","逾期类型","AE"),
        issupportliveloan("issupportliveloan","贷款用途","AF"),
        customernum("customernum","客户号码","AG"),
        sjrq("sjrq","数据日期","AH");

        /**
         * 构造方法
         * @param code
         * @param message
         */
        Xcldkxx(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xcldkxx enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xcldkxx getByCode(String code) {
            for (Xcldkxx enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xcldkxx> getAllEnum() {
            List<Xcldkxx> list = new ArrayList<>(values().length);
            for (Xcldkxx enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xcldkxx enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xcldkxx enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xcldkxx enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xcldkxx enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xcldkxx enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xcldkxx enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }

    public enum Xdkfsxx {
        finorgcode("finorgcode","金融机构代码","A"),
        finorgincode("finorgincode","内部机构号","B"),
        finorgareacode("finorgareacode","金融机构地区代码","C"),
        isfarmerloan("isfarmerloan","借款人证件类型","D"),
        browidcode("browidcode","借款人证件代码","E"),
        isgreenloan("isgreenloan","借款人国民经济部门","F"),
        browinds("browinds","借款人行业","G"),
        browareacode("browareacode","借款人地区代码","H"),
        entpczjjcf("entpczjjcf","借款人经济成分","I"),
        entpmode("entpmode","借款人企业规模","J"),
        loanbrowcode("loanbrowcode","贷款借据编码","K"),
        loancontractcode("loancontractcode","贷款合同编码","L"),
        loanprocode("loanprocode","贷款产品类别","M"),
        loanactdect("loanactdect","贷款实际投向","N"),
        loanstartdate("loanstartdate","贷款发放日期","O"),
        loanenddate("loanenddate","贷款到期日期","P"),
        loanactenddate("loanactenddate","贷款实际终止日期","Q"),
        loancurrency("loancurrency","币种","R"),
        loanamt("loanamt","贷款发生金额","S"),
        loancnyamt("loancnyamt","贷款发生金额折人民币","T"),
        rateisfix("rateisfix","利率是否固定","U"),
        ratelevel("ratelevel","利率水平","V"),
        loanfixamttype("loanfixamttype","贷款定价基准类型","W"),
        rate("rate","基准利率","X"),
        loanfinancesupport("loanfinancesupport","贷款财政扶持方式","Y"),
        loanraterepricedate("loanraterepricedate","贷款利率重新定价日","Z"),
        gteemethod("gteemethod","贷款担保方式","AA"),
        isplatformloan("isplatformloan","是否首次贷款","AB"),
        loanstatus("loanstatus","贷款状态","AC"),
        assetproductcode("assetproductcode","资产证券化产品代码","AD"),
        loanrestructuring("loanrestructuring","贷款重组方式","AE"),
        givetakeid("givetakeid","发放/收回标识","AF"),
        transactionnum("transactionnum","交易流水号","AG"),
        issupportliveloan("issupportliveloan","贷款用途","AH"),
        customernum("customernum","客户号码","AI"),
    	sjrq("sjrq","数据日期","AJ");
        /**
         * 构造方法
         * @param code
         * @param message
         */
        Xdkfsxx(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xdkfsxx enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xdkfsxx getByCode(String code) {
            for (Xdkfsxx enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xdkfsxx> getAllEnum() {
            List<Xdkfsxx> list = new ArrayList<>(values().length);
            for (Xdkfsxx enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xdkfsxx enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xdkfsxx enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xdkfsxx enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xdkfsxx enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xdkfsxx enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xdkfsxx enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }

    public enum Xdkdbht {
    	financeorgcode("financeorgcode","金融机构代码","A"),
    	financeorginnum("financeorginnum","内部机构号","B"),
        gteecontractcode("gteecontractcode","担保合同编码","C"),
        loancontractcode("loancontractcode","被担保合同编码","D"),
        gteecontracttype("gteecontracttype","担保合同类型","E"),
        transactiontype("transactiontype","交易类型","F"),
        gteestartdate("gteestartdate","担保合同起始日期","G"),
        gteeenddate("gteeenddate","担保合同到期日期","H"),
        gteecurrency("gteecurrency","币种","I"),
        gteeamount("gteeamount","担保合同金额","J"),
        gteecnyamount("gteecnyamount","担保合同金额折人民币","K"),
        pledgerate("pledgerate","抵质押率","L"),
        gteeidtype("gteeidtype","担保人证件类型","M"),
        gteeidnum("gteeidnum","担保人证件代码","N"),                
        isgreenloan("isgreenloan","担保人国民经济部门","O"),
        brrowerindustry("brrowerindustry","担保人行业","P"),
        brrowerareacode("brrowerareacode","担保人地区代码","Q"),
        enterprisescale("enterprisescale","担保人企业规模","R"),
    	sjrq("sjrq","数据日期","S");
        /**
         * 构造方法
         * @param code
         * @param message
         */
        Xdkdbht(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xdkdbht enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xdkdbht getByCode(String code) {
            for (Xdkdbht enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xdkdbht> getAllEnum() {
            List<Xdkdbht> list = new ArrayList<>(values().length);
            for (Xdkdbht enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xdkdbht enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xdkdbht enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xdkdbht enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xdkdbht enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xdkdbht enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xdkdbht enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }

    public enum Xftykhx {
        finorgcode("finorgcode","金融机构代码","A"),
        customername("customername","客户名称","B"),
        regamtcreny("regamtcreny","客户证件类型","C"),
        customercode("customercode","客户证件代码","D"),
        depositnum("depositnum","基本存款账号","E"),
        countopenbankname("countopenbankname","基本账户开户行名称","F"),
        netamt("netamt","经营范围","G"),
        regamt("regamt","注册资本","H"),
        actamt("actamt","实收资本","I"),
        totalamt("totalamt","总资产","J"),
        workarea("workarea","营业收入","K"),
        jobpeoplenum("jobpeoplenum","从业人员数","L"),
        islistcmpy("islistcmpy","是否上市公司","M"),
        fistdate("fistdate","首次建立信贷关系日期","N"),
        regarea("regarea","注册地址","O"),
        regareacode("regareacode","地区代码","P"),
        mngmestus("mngmestus","经营状态","Q"),
        setupdate("setupdate","成立日期","R"),
        industry("industry","所属行业","S"),
        entpmode("entpmode","企业规模","T"),
        entpczjjcf("entpczjjcf","客户经济成分","U"),
        actamtcreny("actamtcreny","客户国民经济部门","V"),
        creditamt("creditamt","授信额度","W"),
        usedamt("usedamt","已用额度","X"),
        isrelation("isrelation","是否关联方","Y"),
        actctrlidtype("actctrlidtype","实际控制人证件类型","Z"),
        actctrlidcode("actctrlidcode","实际控制人证件代码","AA"),
        workareacode("workareacode","客户信用级别总等级数","AB"),
        actctrltype("actctrltype","客户信用评级","AC"),
    	sjrq("sjrq","数据日期","AD");
        /**
         * 构造方法
         * @param code
         * @param message
         */
        Xftykhx(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xftykhx enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xftykhx getByCode(String code) {
            for (Xftykhx enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xftykhx> getAllEnum() {
            List<Xftykhx> list = new ArrayList<>(values().length);
            for (Xftykhx enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xftykhx enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xftykhx enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xftykhx enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xftykhx enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xftykhx enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xftykhx enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }

    public enum Xftykhxbl {
    	finorgcode("finorgcode","金融机构代码","A"),
        customername("customername","客户名称","B"),
        regamtcreny("regamtcreny","客户证件类型","C"),
        customercode("customercode","客户证件代码","D"),
        depositnum("depositnum","基本存款账号","E"),
        countopenbankname("countopenbankname","基本账户开户行名称","F"),
        netamt("netamt","经营范围","G"),
        regamt("regamt","注册资本","H"),
        actamt("actamt","实收资本","I"),
        totalamt("totalamt","总资产","J"),
        workarea("workarea","营业收入","K"),
        jobpeoplenum("jobpeoplenum","从业人员数","L"),
        islistcmpy("islistcmpy","是否上市公司","M"),
        fistdate("fistdate","首次建立信贷关系日期","N"),
        regarea("regarea","注册地址","O"),
        regareacode("regareacode","地区代码","P"),
        mngmestus("mngmestus","经营状态","Q"),
        setupdate("setupdate","成立日期","R"),
        industry("industry","所属行业","S"),
        entpmode("entpmode","企业规模","T"),
        entpczjjcf("entpczjjcf","客户经济成分","U"),
        actamtcreny("actamtcreny","客户国民经济部门","V"),
        creditamt("creditamt","授信额度","W"),
        usedamt("usedamt","已用额度","X"),
        isrelation("isrelation","是否关联方","Y"),
        actctrlidtype("actctrlidtype","实际控制人证件类型","Z"),
        actctrlidcode("actctrlidcode","实际控制人证件代码","AA"),
        workareacode("workareacode","客户信用级别总等级数","AB"),
        actctrltype("actctrltype","客户信用评级","AC"),
        customernum("customernum","客户号码","AD"),
        customerfinorgcode("customerfinorgcode","客户金融机构代码","AE"),
        customerfinorginside("customerfinorginside","客户金融机构内部机构号","AF"),
        customerfinorgarea("customerfinorgarea","客户金融机构地区代码","AG");
        /**
         * 构造方法
         * @param code
         * @param message
         */
        Xftykhxbl(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xftykhxbl enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xftykhxbl getByCode(String code) {
            for (Xftykhxbl enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xftykhxbl> getAllEnum() {
            List<Xftykhxbl> list = new ArrayList<>(values().length);
            for (Xftykhxbl enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xftykhxbl enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xftykhxbl enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xftykhxbl enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xftykhxbl enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xftykhxbl enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xftykhxbl enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }

    public enum Xclgrdkxx {
        financeorgcode("financeorgcode","金融机构代码","A"),
        financeorginnum("financeorginnum","内部机构号","B"),
        financeorgareacode("financeorgareacode","金融机构地区代码","C"),
        isfarmerloan("isfarmerloan","借款人证件类型","D"),
        brroweridnum("brroweridnum","借款人证件代码","E"),
        brrowerareacode("brrowerareacode","借款人地区代码","F"),
        receiptcode("receiptcode","贷款借据编码","G"),
        contractcode("contractcode","贷款合同编码","H"),
        productcetegory("productcetegory","贷款产品类别","I"),
        loanstartdate("loanstartdate","贷款发放日期","J"),
        loanenddate("loanenddate","贷款到期日期","K"),
        extensiondate("extensiondate","贷款展期到期日期","L"),
        currency("currency","币种","M"),
        receiptbalance("receiptbalance","贷款余额","N"),
        receiptcnybalance("receiptcnybalance","贷款余额折人民币","O"),
        interestisfixed("interestisfixed","利率是否固定","P"),
        interestislevel("interestislevel","利率水平","Q"),
        fixpricetype("fixpricetype","贷款定价基准类型","R"),
        baseinterest("baseinterest","基准利率","S"),
        loanfinancesupport("loanfinancesupport","贷款财政扶持方式","T"),
        loaninterestrepricedate("loaninterestrepricedate","贷款利率重新定价日","U"),
        guaranteemethod("guaranteemethod","贷款担保方式","V"),
        isplatformloan("isplatformloan","是否首次贷款","W"),
        loanquality("loanquality","贷款质量","X"),
        loanstatus("loanstatus","贷款状态","Y"),
        overduetype("overduetype","逾期类型","Z"),
        issupportliveloan("issupportliveloan","贷款用途","AA"),
        sjrq("sjrq","数据日期","BB");

        /**
         * 构造方法
         * @param code
         * @param message
         */
        Xclgrdkxx(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xclgrdkxx enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xclgrdkxx getByCode(String code) {
            for (Xclgrdkxx enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xclgrdkxx> getAllEnum() {
            List<Xclgrdkxx> list = new ArrayList<>(values().length);
            for (Xclgrdkxx enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xclgrdkxx enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xclgrdkxx enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xclgrdkxx enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xclgrdkxx enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xclgrdkxx enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xclgrdkxx enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }
    
    public enum Xgrkhxx {
    	finorgcode("finorgcode","金融机构代码","A"),
        regamtcreny("regamtcreny","客户证件类型","B"),
        customercode("customercode","客户证件代码","C"),
        country("country","国籍","D"),
        nation("nation","民族","E"),
        sex("sex","性别","F"),
        education("education","最高学历","G"),
        birthday("birthday","出生日期","H"),
        regareacode("regareacode","地区代码","I"),
        grincome("grincome","个人年收入","J"),
        familyincome("familyincome","家庭年收入","K"),
        marriage("marriage","婚姻情况","L"),
        isrelation("isrelation","是否关联方","M"),
        creditamt("creditamt","授信额度","N"),
        usedamt("usedamt","已用额度","O"),
        grkhsfbs("grkhsfbs","个人客户身份标识","P"),
        gtgshyyzzdm("gtgshyyzzdm","个体工商户营业执照代码","Q"),
        xwqyshtyxydm("xwqyshtyxydm","小微企业社会统一信用代码","R"),
        workareacode("workareacode","客户信用级别总等级数","S"),
        actctrltype("actctrltype","客户信用评级","T"),
        sjrq("sjrq","数据日期","U");
        /**
         * 构造方法
         * @param code
         * @param message
         */
    	Xgrkhxx(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xgrkhxx enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xgrkhxx getByCode(String code) {
            for (Xgrkhxx enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xgrkhxx> getAllEnum() {
            List<Xgrkhxx> list = new ArrayList<>(values().length);
            for (Xgrkhxx enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xgrkhxx enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xgrkhxx enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xgrkhxx enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xgrkhxx enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xgrkhxx enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xgrkhxx enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }

    public enum Xgrdkfsxx {
        finorgcode("finorgcode","金融机构代码","A"),
        finorgincode("finorgincode","内部机构号","B"),
        finorgareacode("finorgareacode","金融机构地区代码","C"),
        isfarmerloan("isfarmerloan","借款人证件类型","D"),
        browidcode("browidcode","借款人证件代码","E"),
        browareacode("browareacode","借款人地区代码","F"),
        loanbrowcode("loanbrowcode","贷款借据编码","G"),
        loancontractcode("loancontractcode","贷款合同编码","H"),
        loanprocode("loanprocode","贷款产品类别","I"),
        loanstartdate("loanstartdate","贷款发放日期","J"),
        loanenddate("loanenddate","贷款到期日期","K"),
        loanactenddate("loanactenddate","贷款实际终止日期","L"),
        loancurrency("loancurrency","币种","M"),
        loanamt("loanamt","贷款发生金额","N"),
        loancnyamt("loancnyamt","贷款发生金额折人民币","O"),
        rateisfix("rateisfix","利率是否固定","P"),
        ratelevel("ratelevel","利率水平","Q"),
        loanfixamttype("loanfixamttype","贷款定价基准类型","R"),
        rate("rate","基准利率","S"),
        loanfinancesupport("loanfinancesupport","贷款财政扶持方式","T"),
        loanraterepricedate("loanraterepricedate","贷款利率重新定价日","U"),
        gteemethod("gteemethod","贷款担保方式","V"),
        isplatformloan("isplatformloan","是否首次贷款","W"),
        loanstatus("loanstatus","贷款状态","X"),
        assetproductcode("assetproductcode","资产证券化产品代码","Y"),
        loanrestructuring("loanrestructuring","贷款重组方式","Z"),
        givetakeid("givetakeid","发放/收回标识","AA"),
        transactionnum("transactionnum","交易流水号","AB"),
        issupportliveloan("issupportliveloan","贷款用途","AC"),
        sjrq("sjrq","数据日期","AD");
        /**
         * 构造方法
         * @param code
         * @param message
         */
        Xgrdkfsxx(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xgrdkfsxx enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xgrdkfsxx getByCode(String code) {
            for (Xgrdkfsxx enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xgrdkfsxx> getAllEnum() {
            List<Xgrdkfsxx> list = new ArrayList<>(values().length);
            for (Xgrdkfsxx enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xgrdkfsxx enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xgrdkfsxx enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xgrdkfsxx enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xgrdkfsxx enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xgrdkfsxx enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xgrdkfsxx enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }


    public enum Xclwtdkxx {
        financeorgcode("financeorgcode","金融机构代码","A"),
        financeorginnum("financeorginnum","内部机构号","B"),
        financeorgareacode("financeorgareacode","金融机构地区代码","C"),
        isfarmerloan("isfarmerloan","借款人证件类型","D"),
        brroweridnum("brroweridnum","借款人证件代码","E"),
        isgreenloan("isgreenloan","借款人国民经济部门","F"),
        brrowerindustry("brrowerindustry","借款人行业","G"),
        brrowerareacode("brrowerareacode","借款人地区代码","H"),
        inverstoreconomy("inverstoreconomy","借款人经济成分","I"),
        enterprisescale("enterprisescale","借款人企业规模","J"),
        receiptcode("receiptcode","委托贷款借据编码","K"),
        contractcode("contractcode","委托贷款合同编码","L"),
        loanactualdirection("loanactualdirection","贷款实际投向","M"),
        loanstartdate("loanstartdate","委托贷款发放日期","N"),
        loanenddate("loanenddate","委托贷款到期日期","O"),
        extensiondate("extensiondate","委托贷款展期到期日期","P"),
        currency("currency","币种","Q"),
        receiptbalance("receiptbalance","委托贷款余额","R"),
        receiptcnybalance("receiptcnybalance","委托贷款余额折人民币","S"),
        interestisfixed("interestisfixed","利率是否固定","T"),
        interestislevel("interestislevel","利率水平","U"),
        procedurebalance("procedurebalance","手续费金额折人民币","V"),
        guaranteemethod("guaranteemethod","贷款担保方式","W"),
        loanquality("loanquality","贷款质量","X"),
        loanstatus("loanstatus","贷款状态","Y"),
        bailorgreenloan("bailorgreenloan","委托人国民经济部门","Z"),
        bailorgfarmerloan("bailorgfarmerloan","委托人证件类型","AA"),
        bailorgidnum("bailorgidnum","委托人证件代码","AB"),
        bailorgindustry("bailorgindustry","委托人行业","AC"),
        bailorgareacode("bailorgareacode","委托人地区代码","AD"),
        bailorgeconomy("bailorgeconomy","委托人经济成分","AE"),
        bailorgenterprise("bailorgenterprise","委托人企业规模","AF"),
        issupportliveloan("issupportliveloan","贷款用途","AG"),
        sjrq("sjrq","数据日期","AH");

        /**
         * 构造方法
         * @param code
         * @param message
         */
        Xclwtdkxx(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xclwtdkxx enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xclwtdkxx getByCode(String code) {
            for (Xclwtdkxx enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xclwtdkxx> getAllEnum() {
            List<Xclwtdkxx> list = new ArrayList<>(values().length);
            for (Xclwtdkxx enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xclwtdkxx enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xclwtdkxx enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xclwtdkxx enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xclwtdkxx enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xclwtdkxx enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xclwtdkxx enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }
    

    public enum Xwtdkfse {
        finorgcode("finorgcode","金融机构代码","A"),
        finorgincode("finorgincode","内部机构号","B"),
        finorgareacode("finorgareacode","金融机构地区代码","C"),
        isfarmerloan("isfarmerloan","借款人证件类型","D"),
        browidcode("browidcode","借款人证件代码","E"),
        isgreenloan("isgreenloan","借款人国民经济部门","F"),
        browinds("browinds","借款人行业","G"),
        browareacode("browareacode","借款人地区代码","H"),
        entpczjjcf("entpczjjcf","借款人经济成分","I"),
        entpmode("entpmode","借款人企业规模","J"),
        loanbrowcode("loanbrowcode","委托贷款借据编码","K"),
        loancontractcode("loancontractcode","委托贷款合同编码","L"),
        loanactdect("loanactdect","贷款实际投向","M"),
        loanstartdate("loanstartdate","委托贷款发放日期","N"),
        loanenddate("loanenddate","委托贷款到期日期","O"),
        transactionnum("transactionnum","交易流水号","P"),
        loancurrency("loancurrency","币种","Q"),
        loanamt("loanamt","委托贷款发生金额","R"),
        loancnyamt("loancnyamt","委托贷款发生金额折人民币","S"),
        rateisfix("rateisfix","利率是否固定","T"),
        ratelevel("ratelevel","利率水平","U"),
        sxfcny("sxfcny","手续费金额折人民币","V"),
        gteemethod("gteemethod","贷款担保方式","W"),
        wtrgmjjbm("wtrgmjjbm","委托人国民经济部门","X"),
        wtrtype("wtrtype","委托人证件类型","Y"),
        wtrcode("wtrcode","委托人证件代码","Z"),
        wtrhy("wtrhy","委托人行业","AA"),
        wtrdqdm("wtrdqdm","委托人地区代码","AB"),
        wtrjjcf("wtrjjcf","委托人经济成分","AC"),
        wtrqygm("wtrqygm","委托人企业规模","AD"),       
        givetakeid("givetakeid","发放/收回标识","AE"),
        issupportliveloan("issupportliveloan","贷款用途","AF"),
    	sjrq("sjrq","数据日期","AG");
        /**
         * 构造方法
         * @param code
         * @param message
         */
        Xwtdkfse(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xwtdkfse enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xwtdkfse getByCode(String code) {
            for (Xwtdkfse enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xwtdkfse> getAllEnum() {
            List<Xwtdkfse> list = new ArrayList<>(values().length);
            for (Xwtdkfse enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xwtdkfse enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xwtdkfse enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xwtdkfse enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xwtdkfse enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xwtdkfse enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xwtdkfse enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }
    
    public enum Xcltyjdxx {
    	financeorgcode("financeorgcode","金融机构代码","A"),
    	financeorginnum("financeorginnum","内部机构号","B"),
    	jydsdm("jydsdm","交易对手代码","C"),
    	jydsdmlb("jydsdmlb","交易对手代码类别","D"),
    	contractcode("contractcode","合同编码","E"),
    	zcfzlx("zcfzlx","资产负债类型","F"),
    	productcetegory("productcetegory","产品类别","G"),
    	startdate("startdate","合同起始日期","H"),
    	enddate("enddate","合同到期日期","I"),
    	currency("currency","币种","J"),
    	receiptbalance("receiptbalance","合同余额","K"),
    	receiptcnybalance("receiptcnybalance","合同余额折人民币","L"),
    	interestisfixed("interestisfixed","利率是否固定","M"),
    	interestislevel("interestislevel","利率水平","N"),
    	fixpricetype("fixpricetype","定价基准类型","O"),
    	baseinterest("baseinterest","基准利率","P"),
    	jxfs("jxfs","计息方式","Q"),
    	sjrq("sjrq","数据日期","R");
        /**
         * 构造方法
         * @param code
         * @param message
         */
    	Xcltyjdxx(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xcltyjdxx enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xcltyjdxx getByCode(String code) {
            for (Xcltyjdxx enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xcltyjdxx> getAllEnum() {
            List<Xcltyjdxx> list = new ArrayList<>(values().length);
            for (Xcltyjdxx enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xcltyjdxx enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xcltyjdxx enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xcltyjdxx enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xcltyjdxx enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xcltyjdxx enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xcltyjdxx enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }
    
    public enum Xtyjdfsexx {
    	financeorgcode("financeorgcode","金融机构代码","A"),
    	financeorginnum("financeorginnum","内部机构号","B"),
    	jydsdm("jydsdm","交易对手代码","C"),
    	jydsdmlb("jydsdmlb","交易对手代码类别","D"),
    	contractcode("contractcode","合同编码","E"),
    	jylsh("jylsh","交易流水号","F"),
    	zcfzlx("zcfzlx","资产负债类型","G"),
    	productcetegory("productcetegory","产品类别","H"),
    	startdate("startdate","合同起始日期","I"),
    	enddate("enddate","合同到期日期","J"),
    	finaldate("finaldate","合同实际终止日期","K"),
    	currency("currency","币种","L"),
    	receiptbalance("receiptbalance","发生金额","M"),
    	receiptcnybalance("receiptcnybalance","发生金额折人民币","N"),
    	interestisfixed("interestisfixed","利率是否固定","O"),
    	interestislevel("interestislevel","利率水平","P"),
    	fixpricetype("fixpricetype","定价基准类型","Q"),
    	baseinterest("baseinterest","基准利率","R"),
        jxfs("jxfs","计息方式","S"),
        fsjqbs("fsjqbs","发生/结清标识","T"),
        sjrq("sjrq","数据日期","U");
        /**
         * 构造方法
         * @param code
         * @param message
         */
    	Xtyjdfsexx(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xtyjdfsexx enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xtyjdfsexx getByCode(String code) {
            for (Xtyjdfsexx enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xtyjdfsexx> getAllEnum() {
            List<Xtyjdfsexx> list = new ArrayList<>(values().length);
            for (Xtyjdfsexx enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xtyjdfsexx enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xtyjdfsexx enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xtyjdfsexx enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xtyjdfsexx enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xtyjdfsexx enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xtyjdfsexx enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }
    
    public enum Xtykhjcxx {
    	financeorgcode("financeorgcode","金融机构代码","A"),
    	khname("khname","客户名称","B"),
    	khcode("khcode","客户代码","C"),
    	khjrjgbm("khjrjgbm","客户金融机构编码","D"),
    	khnbbm("khnbbm","客户内部编码","E"),
    	jbckzh("jbckzh","基本存款账号","F"),
    	jbzhkhhmc("jbzhkhhmc","基本账户开户行名称","G"),
    	zcdz("zcdz","注册地址","H"),
    	dqdm("dqdm","地区代码","I"),
    	khtype("khtype","客户类别","J"),
    	setupdate("setupdate","成立日期","K"),
    	sfglf("sfglf","是否关联方","L"),
    	khjjcf("khjjcf","客户经济成分","M"),
    	khgmjjbm("khgmjjbm","客户国民经济部门","N"),
    	khxyjbzdjs("khxyjbzdjs","客户信用级别总等级数","O"),
    	khxypj("khxypj","客户信用评级","P"),
    	sjrq("sjrq","数据日期","Q");
        /**
         * 构造方法
         * @param code
         * @param message
         */
    	Xtykhjcxx(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xtykhjcxx enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xtykhjcxx getByCode(String code) {
            for (Xtykhjcxx enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xtykhjcxx> getAllEnum() {
            List<Xtykhjcxx> list = new ArrayList<>(values().length);
            for (Xtykhjcxx enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xtykhjcxx enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xtykhjcxx enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xtykhjcxx enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xtykhjcxx enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xtykhjcxx enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xtykhjcxx enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }
    
    public enum Xcltyckxx {
    	financeorgcode("financeorgcode","金融机构代码","A"),
    	financeorginnum("financeorginnum","内部机构号","B"),
    	ywlx("ywlx","业务类型","C"),
    	jydszjlx("jydszjlx","交易对手证件类型","D"),
    	jydsdm("jydsdm","交易对手代码","E"),
    	ckzhbm("ckzhbm","存款账户编码","F"),
    	ckxydm("ckxydm","存款协议代码","G"),
    	startdate("startdate","协议起始日期","H"),
    	enddate("enddate","协议到期日期","I"),
    	currency("currency","币种","J"),
    	receiptbalance("receiptbalance","存款余额","K"),
    	receiptcnybalance("receiptcnybalance","存款余额折人民币","L"),
    	interestislevel("interestislevel","利率水平","M"),
    	jczbjfs("jczbjfs","缴存准备金方式","N"),
    	sjrq("sjrq","数据日期","O");
        /**
         * 构造方法
         * @param code
         * @param message
         */
    	Xcltyckxx(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xcltyckxx enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xcltyckxx getByCode(String code) {
            for (Xcltyckxx enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xcltyckxx> getAllEnum() {
            List<Xcltyckxx> list = new ArrayList<>(values().length);
            for (Xcltyckxx enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xcltyckxx enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xcltyckxx enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xcltyckxx enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xcltyckxx enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xcltyckxx enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xcltyckxx enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }
    
    public enum Xtyckfsexx {
    	financeorgcode("financeorgcode","金融机构代码","A"),
    	financeorginnum("financeorginnum","内部机构号","B"),
    	ywlx("ywlx","业务类型","C"),
    	jydszjlx("jydszjlx","交易对手证件类型","D"),
    	jydsdm("jydsdm","交易对手代码","E"),
    	ckzhbm("ckzhbm","存款账户编码","F"),
    	ckxydm("ckxydm","存款协议代码","G"),
    	startdate("startdate","协议起始日期","H"),
    	enddate("enddate","协议到期日期","I"),
    	currency("currency","币种","J"),
    	receiptbalance("receiptbalance","交易金额","K"),
    	receiptcnybalance("receiptcnybalance","交易金额折人民币","L"),
    	jyrq("jyrq","交易日期","M"),
    	jylsh("jylsh","交易流水号","N"),
    	llsp("llsp","利率水平","O"),
    	jyzhh("jyzhh","交易账户号","P"),
    	jyzhkhhh("jyzhkhhh","交易账户开户行号","Q"),
    	jydszhh("jydszhh","交易对手账户号","R"),
    	jyfx("jyfx","交易方向","S"),
    	sjrq("sjrq","数据日期","T");
        /**
         * 构造方法
         * @param code
         * @param message
         */
    	Xtyckfsexx(String code, String message,String col) {
            this.code = code;
            this.message = message;
            this.col = col;
        }

        /** 字段 */
        private final String code;

        /** 字段中文 */
        private final String message;

        /** 字段列号 */
        private final String col;



        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getCol() {
            return col;
        }

        /**
         * 通过code获取msg
         *
         * @param code 枚举值
         *
         * @return
         */
        public static String getMsgByCode(String code) {
            if (code == null) {
                return null;
            }
            Xtyckfsexx enumList = getByCode(code);
            if (enumList == null) {
                return null;
            }
            return enumList.getMessage();
        }

        /**
         * 通过枚举<code>code</code>获得枚举
         *
         * values() 方法将枚举转变为数组
         *
         * @return AuthGradeEnum
         */
        public static Xtyckfsexx getByCode(String code) {
            for (Xtyckfsexx enumList : values()) {
                if (enumList.getCode().equals(code)) {
                    return enumList;
                }
            }
            return null;
        }

        /**
         * 获取全部枚举
         *
         * @return List<AuthGradeEnum>
         */
        public static List<Xtyckfsexx> getAllEnum() {
            List<Xtyckfsexx> list = new ArrayList<>(values().length);
            for (Xtyckfsexx enumList : values()) {
                list.add(enumList);
            }
            return list;
        }

        /**
         * 获取全部枚举转map
         *
         * @return Map<String,String>
         */
        public static Map<String,String> getAllEnumToMap() {
            Map<String,String> map = new LinkedHashMap();
            for (Xtyckfsexx enumList : values()) {
                map.put(enumList.code,enumList.message);
            }
            return map;
        }

        /**
         * 获取全部message转List
         *
         * @return List
         */
        public static List<String> getMessageToList() {
            List<String> list = new ArrayList<>();
            for (Xtyckfsexx enumList : values()) {
                list.add(enumList.message);
            }
            return list;
        }

        /**
         * 获取全部code转List
         *
         * @return List
         */
        public static List<String> getCodeToList() {
            List<String> list = new ArrayList<>();
            for (Xtyckfsexx enumList : values()) {
                list.add(enumList.code);
            }
            return list;
        }

        /**
         * 获取全部col转List
         *
         * @return List
         */
        public static List<String> getColToList() {
            List<String> list = new ArrayList<>();
            for (Xtyckfsexx enumList : values()) {
                list.add(enumList.col);
            }
            return list;
        }

        /**
         * 获取全部code和col转Map
         *
         * @return Map
         */
        public static Map<String,String> getCodeColToMap() {
            Map<String,String> map = new HashMap<>();
            for (Xtyckfsexx enumList : values()) {
                map.put(enumList.code,enumList.col);
            }
            return map;
        }

        /**
         * 获取全部枚举值
         *
         * @return List<String>
         */
        public static List<String> getAllEnumCode() {
            List<String> list = new ArrayList<>(values().length);
            for (Xtyckfsexx enumList : values()) {
                list.add(enumList.getCode());
            }
            return list;
        }
    }
 }