package com.geping.etl.UNITLOAN.controller.dataManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;

import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.entity.report.Xcldkxx;
import com.geping.etl.UNITLOAN.entity.report.Xdkdbwx;

import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XdkdbwxService;
import com.geping.etl.UNITLOAN.util.*;
import com.geping.etl.UNITLOAN.util.check.CheckAllData3;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;

import com.geping.etl.utils.jdbc.JDBCUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * 单位贷款担保物信息控制器
 * @author WuZengWen
 * @date 2020年6月9日 下午2:49:41
 */
@RestController
public class DWDKDBWXXController {

	@Autowired
	private XdkdbwxService xs;
	@Autowired
	private SUpOrgInfoSetService sus;
	@Autowired
	private CalcCountUtil countUtil;
	@Autowired
	private ExcelUploadUtil excelUploadUtil;
	@Autowired
	private CustomSqlUtil customSqlUtil;

	@Autowired
	private SUpOrgInfoSetService suisService;
	@Autowired
	private XDataBaseTypeUtil dataBaseTypeUtil;

    @Autowired
    private XCheckRuleService checkRuleService;
	@Autowired
	private XcommonService commonService;

	@Autowired
	private HttpServletRequest request;
	private Sys_UserAndOrgDepartment sys_user;
	private final String tableName="xdkdbwx";
	private final String fileName="单位贷款担保物信息";
	@Autowired
	private JdbcTemplate jt;
	@Autowired
    private DepartUtil departUtil;
	private static String target="";
	//查询数据
	@PostMapping("XGetXdkdbwxData")
	public ResponseResult XGetXdkdbwxData(int page, int rows){
		page = page - 1;
		sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		List<SUpOrgInfoSet> list = sus.findAll();
		String departId = departUtil.getDepart(sys_user);
		Specification specification=new Specification() {
			@Override
			public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				//待提交
				predicates.add(criteriaBuilder.equal(root.get("datastatus"), "0"));

				predicates.add(criteriaBuilder.like(root.get("departid"), departId));
				predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
		PageRequest pageRequest = new PageRequest(page, rows, sort);
		Page<Xcldkxx> all = xs.findAll(specification, pageRequest);
		List<Xcldkxx> content = all.getContent();
		long totalCount = all.getTotalElements();
		return ResponseResult.success(totalCount,content);
	}

	//导入
	@PostMapping(value = "Xdaorudkdbwx",produces = "text/plain;charset=UTF-8")
	public void Xdaorudkdbwx(HttpServletRequest request,HttpServletResponse response){
		sys_user=((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		File uploadFile = null;
		OPCPackage opcPackage=null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multiRequest.getFile("excelfile");
			uploadFile = excelUploadUtil.uploadFile(file);
			JSONObject json = new JSONObject();
			if (file == null) {
				json.put("msg", "导入文件为空");
			} else {
				opcPackage = OPCPackage.open(uploadFile);
				String[] headers = {"金融机构代码", "内部机构号","担保合同编码", "被担保合同编码",
						"担保物编码", "担保物类别", "权证编号", "是否第一顺位", "评估方式", "评估方法",
						"评估价值", "评估基准日", "担保物票面价值", "优先受偿权数额", "估值周期","数据日期"
				};
				String[] values = {"financeorgcode","financeorginnum","gteecontractcode","loancontractcode","gteegoodscode","gteegoodscategory",
						"warrantcode", "isfirst", "assessmode", "assessmethod", "assessvalue",
						"assessdate","gteegoodsamt","firstrightamt","gzzq","sjrq"
				};
				String[] amountFields = {"assessvalue","gteegoodsamt","firstrightamt"};
				String[] dateFields = {"assessdate","sjrq"};
				XCommonExcel<Xdkdbwx> commonExcel = new XCommonExcel<>(sys_user, opcPackage, customSqlUtil, new Xdkdbwx(), headers, values,amountFields,dateFields,null,null);
				commonExcel.process(0);
				String departId = departUtil.getDepart(sys_user);
				if (commonExcel.msg.toString().length() == 0) {
					commonService.importDelete(tableName,departId);
					commonExcel.process(1);
					json.put("msg","导入成功");
//					countUtil.handleCount("xdkdbwx",0,"add",commonExcel.count);//更新数量
				} else {
					json.put("msg",commonExcel.msg.toString());
				}
			}
			String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
			customSqlUtil.saveLog("单位贷款担保物信息->"+logContext,"导入");
			out.write(json.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				opcPackage.close();
				//将上传的文件删除
				if (uploadFile.exists()) {
					uploadFile.delete();
				}
				if (out != null) {
					out.flush();
					out.close();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	//导出
	@RequestMapping(value = "/Xdaochudkdbwx", method = RequestMethod.GET)
	public void Xdaochudkdbwx(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select financeorgcode,financeorginnum,gteecontractcode,loancontractcode,gteegoodscode,gteegoodscategory,warrantcode,isfirst,"
				+ "assessmode,assessmethod,assessvalue,assessdate,gteegoodsamt,firstrightamt,gzzq,sjrq from xdkdbwx";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		String parameter = request.getParameter("zuhagntai");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				if(i == 0) {
					query = query + "'" + idarr[i] + "'";
				}else {
					query = query + ",'" + idarr[i] + "'";
				}
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			String departId = departUtil.getDepart(sys_User);
			//判断导出 1为待审核
			if (parameter.equals("dtj")){
				query = query + " where (datastatus='0' or datastatus='2') and orgid='" + sys_User.getOrgid() + "'";
				query = query + " and departid like '" + departId + "'";
			}else {
				query = query + " where datastatus='1' and orgid='" + sys_User.getOrgid() + "'";
				query = query + " and departid like '" + departId + "'";
			}

		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("financeorgcode"));
			dataA.add(smo.get("financeorginnum"));
			dataA.add(smo.get("gteecontractcode"));
			dataA.add(smo.get("loancontractcode"));
			dataA.add(smo.get("gteegoodscode"));
			dataA.add(smo.get("gteegoodscategory"));
			dataA.add(smo.get("warrantcode"));
			dataA.add(smo.get("isfirst"));
			dataA.add(smo.get("assessmode"));
			dataA.add(smo.get("assessmethod"));
			dataA.add(smo.get("assessvalue"));
			dataA.add(smo.get("assessdate"));
			dataA.add(smo.get("gteegoodsamt"));
			dataA.add(smo.get("firstrightamt"));
			dataA.add(smo.get("gzzq"));
			dataA.add(smo.get("sjrq"));
			//dataA.add(smo.get("gteegoodsstataus"));
			//dataA.add(smo.get("mortgagepgerate"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("金融机构代码");
		columnNames.add("内部机构号");
		columnNames.add("担保合同编码");
		columnNames.add("被担保合同编码");
		columnNames.add("担保物编码");
		columnNames.add("担保物类别");
		columnNames.add("权证编号");
		columnNames.add("是否第一顺位");
		columnNames.add("评估方式");
		columnNames.add("评估方法");
		columnNames.add("评估价值");
		columnNames.add("评估基准日");
		columnNames.add("担保物票面价值");
		columnNames.add("优先受偿权数额");
		columnNames.add("估值周期");
		columnNames.add("数据日期");
		//columnNames.add("担保物状态");
		//columnNames.add("抵质押率");
		try {
			String fileName = "单位贷款担保物信息.xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//导出模版
	@RequestMapping(value = "/Xdaochumobandkdbwx", method = RequestMethod.GET)
	public void Xdaochumobandkdbwx(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select financeorgcode,financeorginnum,gteecontractcode,loancontractcode,gteegoodscode,gteegoodscategory,warrantcode,isfirst,"
				+ "assessmode,assessmethod,assessvalue,assessdate,gteegoodsamt,firstrightamt from xdkdbwx";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				query = query + "'" + idarr[i] + "'";
			}
			query = query + ");";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			Object isusedepart = request.getSession().getAttribute("isusedepart");
			query = query + " where (datastatus='0' or datastatus='2') and orgid='" + sys_User.getOrgid() + "'";
			if("yes".equals(isusedepart)) {
				query = query + " and departid='" + sys_User.getDepartid() + "'";
			}
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<List<Object>> objects = new LinkedList<>();
		/*for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("gteecontractcode"));
			dataA.add(smo.get("loancontractcode"));
			dataA.add(smo.get("gteegoodscode"));
			dataA.add(smo.get("gteegoodscategory"));
			dataA.add(smo.get("warrantcode"));
			dataA.add(smo.get("isfirst"));
			dataA.add(smo.get("assessmode"));
			dataA.add(smo.get("assessmethod"));
			dataA.add(smo.get("assessvalue"));
			dataA.add(smo.get("assessdate"));
			dataA.add(smo.get("gteegoodsamt"));
			dataA.add(smo.get("firstrightamt"));
			dataA.add(smo.get("gteegoodsstataus"));
			dataA.add(smo.get("mortgagepgerate"));
			objects.add(dataA);
		}*/
		List<String> columnNames = new LinkedList<>();
		columnNames.add("金融机构代码");
		columnNames.add("内部机构号");
		columnNames.add("担保合同编码");
		columnNames.add("被担保合同编码");
		columnNames.add("担保物编码");
		columnNames.add("担保物类别");
		columnNames.add("权证编号");
		columnNames.add("是否第一顺位");
		columnNames.add("评估方式");
		columnNames.add("评估方法");
		columnNames.add("评估价值");
		columnNames.add("评估基准日");
		columnNames.add("担保物票面价值");
		columnNames.add("优先受偿权数额");
		columnNames.add("估值周期");
		columnNames.add("数据日期");
		//columnNames.add("担保物状态");
		//columnNames.add("抵质押率");
		try {
			String fileName = "单位贷款担保物信息.xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}		

	//导出H
	@RequestMapping(value = "/XdaochudkdbwxH", method = RequestMethod.GET)
	public void XdaochudkdbwxH(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select financeorgcode,financeorginnum,gteecontractcode,loancontractcode,gteegoodscode,gteegoodscategory,warrantcode,isfirst,"
				+ "assessmode,assessmethod,assessvalue,assessdate,gteegoodsamt,firstrightamt,gzzq,sjrq from xdkdbwxH";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				if(i == 0) {
					query = query + "'" + idarr[i] + "'";
				}else {
					query = query + ",'" + idarr[i] + "'";
				}
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			String departId = departUtil.getDepart(sys_user);
			query = query + " where orgid='" + sys_User.getOrgid() + "'";
			query = query + " and departid like '" + departId + "'";
			String gteecontractcode = request.getParameter("gteecontractcode");
			String loancontractcode = request.getParameter("loancontractcode");
			String operationtime = request.getParameter("operationtime");
			if(StringUtils.isNotBlank(gteecontractcode)) {
				query = query + " and gteecontractcode like '%" + gteecontractcode + "%'";
			}
			if(StringUtils.isNotBlank(loancontractcode)) {
				query = query + " and loancontractcode like '%" + loancontractcode + "%'";
			}
			if(StringUtils.isNotBlank(operationtime)) {
				query = query + " and sjrq like '%" + operationtime + "%'";
			}
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("financeorgcode"));
			dataA.add(smo.get("financeorginnum"));
			dataA.add(smo.get("gteecontractcode"));
			dataA.add(smo.get("loancontractcode"));
			dataA.add(smo.get("gteegoodscode"));
			dataA.add(smo.get("gteegoodscategory"));
			dataA.add(smo.get("warrantcode"));
			dataA.add(smo.get("isfirst"));
			dataA.add(smo.get("assessmode"));
			dataA.add(smo.get("assessmethod"));
			dataA.add(smo.get("assessvalue"));
			dataA.add(smo.get("assessdate"));
			dataA.add(smo.get("gteegoodsamt"));
			dataA.add(smo.get("firstrightamt"));
			dataA.add(smo.get("gzzq"));
			dataA.add(smo.get("sjrq"));
			//dataA.add(smo.get("gteegoodsstataus"));
			//dataA.add(smo.get("mortgagepgerate"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("金融机构代码");
		columnNames.add("内部机构号");
		columnNames.add("担保合同编码");
		columnNames.add("被担保合同编码");
		columnNames.add("担保物编码");
		columnNames.add("担保物类别");
		columnNames.add("权证编号");
		columnNames.add("是否第一顺位");
		columnNames.add("评估方式");
		columnNames.add("评估方法");
		columnNames.add("评估价值");
		columnNames.add("评估基准日");
		columnNames.add("担保物票面价值");
		columnNames.add("优先受偿权数额");
		columnNames.add("估值周期");
		columnNames.add("数据日期");
		//columnNames.add("担保物状态");
		//columnNames.add("抵质押率");
		try {
			String fileName = "单位贷款担保物信息.xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}		
	
	
	//待提交页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindDWDKDBWXXdtj", method = RequestMethod.POST)
	public void XfindDWDKDBWXXdtj(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		String selectdbhtbm = request.getParameter("selectdbhtbm");
		String selectdkhtbm = request.getParameter("selectdkhtbm");
		String selectdkdbwbm = request.getParameter("selectdkdbwbm");
		String selectjylx = request.getParameter("selectjylx");
		String zhuangtai = "0";
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		List<Xdkdbwx> list = null;
		Page<Xdkdbwx> gcPage = getPageData(selectdbhtbm,selectdkhtbm,selectdkdbwbm,selectjylx,zhuangtai,page,size);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		result.put("rows", list);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//待审核页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindDWDKDBWXXdsh", method = RequestMethod.POST)
	public void XfindDWDKDBWXXdsh(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		String selectdbhtbm = request.getParameter("gteecontractcodeParam");
		String selectdkhtbm = request.getParameter("loancontractcodeParam");
		String selectdkdbwbm = request.getParameter("gteegoodscodeParam");
		String selectjylx = request.getParameter("checkstatusParam");
		String selectczm = request.getParameter("operationnameParam");
		String zhuangtai;
		if(StringUtils.isNotBlank(selectczm)&&selectczm.equals("1")) {
			zhuangtai = "3";
		}else{
			zhuangtai="1";
		}
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		List<Xdkdbwx> list = null;
		Page<Xdkdbwx> gcPage = getPageData(selectdbhtbm,selectdkhtbm,selectdkdbwbm,selectjylx,zhuangtai,page,size);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		result.put("rows", list);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//获取分页数据方法
	public Page<Xdkdbwx> getPageData(String selectdbhtbm,String selectdkhtbm,String selectdkdbwbm,String selectjylx,String zhuangtai,Integer page,Integer size){
		sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");

		List<SUpOrgInfoSet> list = sus.findAll();
		String departId = departUtil.getDepart(sys_user);
		Specification<Xdkdbwx> querySpecifi = new Specification<Xdkdbwx>() {
			@Override
			public Predicate toPredicate(Root<Xdkdbwx> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				if (StringUtils.isNotBlank(selectdbhtbm)) {
					predicates.add(criteriaBuilder.like(root.get("gteecontractcode"), "%" + selectdbhtbm.trim() + "%"));
				}
				if (StringUtils.isNotBlank(selectdkhtbm)) {
					predicates.add(criteriaBuilder.like(root.get("loancontractcode"), "%" + selectdkhtbm.trim() + "%"));
				}
				if (StringUtils.isNotBlank(selectdkdbwbm)) {
					predicates.add(criteriaBuilder.like(root.get("gteegoodscode"), "%" + selectdkdbwbm.trim() + "%"));
				}
				if (StringUtils.isNotBlank(selectjylx)) {
					predicates.add(criteriaBuilder.equal(root.get("checkstatus"), selectjylx.trim()));
				}
				if("0".equals(zhuangtai)) {
					Predicate p1 = null;
					Predicate p2 = null;
					p1 = criteriaBuilder.equal(root.get("datastatus"),"0");
					p2 = criteriaBuilder.equal(root.get("datastatus"),"2");
					predicates.add(criteriaBuilder.or(p1, p2));
				}else if("3".equals(zhuangtai)){
					predicates.add(criteriaBuilder.equal(root.get("operationname"),"申请删除"));
					predicates.add(criteriaBuilder.equal(root.get("datastatus"),"1"));
				}else {
					predicates.add(criteriaBuilder.equal(root.get("datastatus"),zhuangtai));
				}
				predicates.add(criteriaBuilder.like(root.get("departid"), departId));
				predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
		// Pageable 接口通常使用的其 PageRequest 实现类. 其中封装了需要分页的信息
		PageRequest pr = new PageRequest(page, size,sort); // intPage从0开始
		return xs.findAll(querySpecifi, pr);
	}
	
	//新增或修改单位贷款担保物信息
	@RequestMapping(value="/XsaveOrUpdatedkdbwx",method=RequestMethod.POST)
	public void XsaveOrUpdatedkdbwx(HttpServletRequest request,HttpServletResponse response,Xdkdbwx Xdkdbwx){
		PrintWriter out = null;
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
		    response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			if(Xdkdbwx.getId().length() == 0 || StringUtils.isBlank(Xdkdbwx.getId())) {
				String uuid = Stringutil.getUUid();
				Xdkdbwx.setId(uuid);
				Xdkdbwx.setDepartid(sys_User.getDepartid());
				Xdkdbwx.setOrgid(sys_User.getOrgid());
			}
			Xdkdbwx.setCheckstatus("0");
			Xdkdbwx.setDatastatus("0");
			Xdkdbwx.setOperationname(" ");
			Xdkdbwx.setOperator(sys_User.getLoginid());
			Xdkdbwx.setOperationtime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			System.out.println(Xdkdbwx);
			Xdkdbwx xdkdbwx = xs.SaveOrUpdate(Xdkdbwx);
			if(xdkdbwx!=null) {
				if(Xdkdbwx.getId().length() == 0 || StringUtils.isNotBlank(Xdkdbwx.getId())) {
					countUtil.handleCount("xdkdbwx", 0, "add", 1);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//删除单位贷款担保物信息
	@RequestMapping(value="/Xdeletedkdbwx",method=RequestMethod.POST)
	public void Xdeletedkdbwx(HttpServletRequest request,HttpServletResponse response,String deletetype,String deleteid){
		PrintWriter out = null;
		try {
			response.setContentType("text/html");
		    response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			if("all".equals(deletetype)) {
				//result = xs.delete();
				String sql = "delete from xdkdbwx where datastatus='0' and operationname='同意删除'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
				//result = xs.delete();
			}else if("some".equals(deletetype)) {
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "delete from xdkdbwx where id in(";
					for (int i = 0; i < idarr.length; i++) {
						//xs.delete(idarr[i]);
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
						//xs.delete(idarr[i]);
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					countUtil.handleCount("xdkdbwx", 0, "reduce", 1);
				}else if("some".equals(deletetype)) {
					countUtil.handleCount("xdkdbwx", 0, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//申请删除单位贷款担保物信息
	@RequestMapping(value="/Xshenqingdeletedkdbwx",method=RequestMethod.POST)
	public void Xshenqingdeletedkdbwx(HttpServletRequest request,HttpServletResponse response,String deletetype,String deleteid){
		PrintWriter out = null;
		try {
			response.setContentType("text/html");
		    response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//result = xs.delete();
				//String sql = "delete from xdkdbwx where datastatus='0' and operationname='同意删除'";
				String sql = "update xdkdbwx set operationname='申请删除',datastatus='1' where (datastatus='0' or datastatus='2') and departid like '"+departId+"'";
				result = jt.update(sql);
				//result = ServiceTemplate.YeWuMoBan(sql);
				//result = xs.delete();
			}else if("some".equals(deletetype)) {
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					//String sql = "delete from xdkdbwx where id in(";
					String sql = "update xdkdbwx set operationname='申请删除',datastatus='1' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						//xs.delete(idarr[i]);
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
						//xs.delete(idarr[i]);
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					countUtil.handleCount("xdkdbwx", 0, "reduce", result);
				}else if("some".equals(deletetype)) {
					countUtil.handleCount("xdkdbwx", 0, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//校验单位贷款担保物信息
//	@RequestMapping(value="/Xcheckdkdbwx",method=RequestMethod.POST)
//	public void Xcheckdkdbwx(HttpServletRequest request,HttpServletResponse response,String gteecontractcodeParam,String loancontractcodeParam){
//		PrintWriter out = null;
//		String checktype = request.getParameter("checktype");
//		String id = request.getParameter("id");
//		LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
//		List<String> errorId = new ArrayList<>();
//		List<String> rightId = new ArrayList<>();
//		Map<String,String> whereMap=new HashMap<>();
//		Map<String,String> setMap=new HashMap<>();
//		setMap.put("operator",sys_user.getLoginid());
//		setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//		try {
//			response.setContentType("text/html");
//			response.setContentType("text/plain; charset=utf-8");
//			out = response.getWriter();
//			String result = "0";
//			String whereStra=" where a.datastatus='0' and a.checkstatus='0' ";
//			if (StringUtils.isNotBlank(id)){
//				id = id.substring(0,id.length()-1);
//				id = "'" + id.replace(",","','")  + "'";
//				whereStra = whereStra + " and a.id in(" + id + ")";
//			}else {
//				if (StringUtils.isNotBlank(gteecontractcodeParam)){
//					whereStra = whereStra + " and a.gteecontractcode like '%"+ gteecontractcodeParam +"%'";
//				}
//				if (StringUtils.isNotBlank(loancontractcodeParam)){
//					whereStra = whereStra + " and a.loancontractcode like '%"+ loancontractcodeParam +"%'";
//				}
//			}
//
//			String errorcode = "贷款合同编码";
//
//			String errorinfo  = "数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号不唯一（临时表）";
//			String sqlexist ="SELECT a.id,a.loancontractcode FROM xdkfsxx a " + whereStra + " and (SELECT count(*) FROM xdkfsxx b WHERE a.finorgincode = b.finorgincode AND a.loancontractcode = b.loancontractcode AND a.loanbrowcode = b.loanbrowcode AND a.sjrq = b.sjrq and b.operationname !='申请删除') > 1";
//			inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//
//
//			errorinfo = "数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号不唯一（历史表）";
//			sqlexist = "SELECT a.id,a.loancontractcode FROM xdkfsxx a INNER JOIN xdkfsxxh b on a.finorgincode = b.finorgincode AND a.loancontractcode = b.loancontractcode AND a.loanbrowcode = b.loanbrowcode AND a.sjrq = b.sjrq" + whereStra + " and b.operationname !='申请删除'";
//			inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//
//
//			if("all".equals(checktype)) {
//				result = CheckDataUtils2.jiaoYanShuJu(TableCodeEnum.DWDKDBWXX,xs,errorMsg, errorId, rightId);
//				if("1".equals(result)) {
//					out.write(result);
//				}else if ("13".equals(result)){
//					out.write(result);
//				}else{
//					List<SUpOrgInfoSet> list = sus.findAll();
//					String messagepath = "C:";
//					if(list.size() > 0) {
//						messagepath = list.get(0).getMessagepath();
//					}
//					request.getSession().setAttribute("messagepath",messagepath);
//					DownloadUtil.downLoad("校验结果:"+result, messagepath+"\\checkout", "单位贷款担保物信息校验结果.txt");
//					out.write(result);
//				}
//			}else if("some".equals(checktype)) {
//				String rows = request.getParameter("rows");
//				// 先转成json数组再将json转化成list数组
//				JSONArray jsonArray = JSONArray.fromObject(rows);
//				@SuppressWarnings("unchecked")
//				List<Xdkdbwx> list = JSONArray.toList(jsonArray, new Xdkdbwx(), new JsonConfig());
//				result = CheckDataUtils2.jiaoYanShuJu(list,TableCodeEnum.DWDKDBWXX,xs,errorMsg, errorId, rightId);
//				if("1".equals(result)) {
//					out.write(result);
//				}else {
//					List<SUpOrgInfoSet> olist = sus.findAll();
//					String messagepath = "C:";
//					if(olist.size() > 0) {
//						messagepath = olist.get(0).getMessagepath();
//					}
//					request.getSession().setAttribute("messagepath",messagepath);
//					DownloadUtil.downLoad("校验结果:"+result, messagepath+"\\checkout", "单位贷款担保物信息校验结果.txt");
//					out.write(result);
//				}
//			}
//
//			//out.write(result);
//		} catch (Exception e) {
//			e.printStackTrace();
//			out.write("0");
//		} finally {
//			if(out!=null) {
//				out.flush();
//				out.close();
//			}
//		}
//	}

	@RequestMapping(value="/Xcheckdkdbwx",method=RequestMethod.POST)
	public void Xcheckdkdbwx(HttpServletRequest request,HttpServletResponse response,String gteecontractcodeParam,String loancontractcodeParam){
		//记录是否有错
		boolean b=false;
		sys_user = ((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));
        boolean sqlServer = dataBaseTypeUtil.equalsSqlServer();
		List<SUpOrgInfoSet> sUpOrgInfoSets = suisService.findAll();
		target=sUpOrgInfoSets.get(0).getMessagepath();

		JSONObject json = new JSONObject();
		String id = request.getParameter("id");
		String departId = departUtil.getDepart(sys_user);

		LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
		List<String> errorId = new ArrayList<>();
		List<String> rightId = new ArrayList<>();
		Map<String,String> whereMap=new HashMap<>();
		Map<String,String> setMap=new HashMap<>();
		setMap.put("operator",sys_user.getLoginid());
		setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

		String whereStr=" where datastatus='0' and checkstatus='0' ";
		String whereStra=" where a.datastatus='0' and a.checkstatus='0' ";
		if (StringUtils.isNotBlank(id)){
			id = id.substring(0,id.length()-1);
			id = "'" + id.replace(",","','")  + "'";
			whereStr = whereStr + " and id in(" + id + ")";
			whereStra = whereStra + " and a.id in(" + id + ")";
		}else {
			if (StringUtils.isNotBlank(gteecontractcodeParam)){
				whereStr = whereStr + " and gteecontractcode like '%"+ gteecontractcodeParam +"%'";
				whereStra = whereStra + " and a.gteecontractcode like '%"+ gteecontractcodeParam +"%'";
			}
			if (StringUtils.isNotBlank(loancontractcodeParam)){
				whereStr = whereStr + " and loancontractcode like '%"+ loancontractcodeParam +"%'";
				whereStra = whereStra + " and a.loancontractcode like '%"+ loancontractcodeParam +"%'";
			}
			whereStr = whereStr + " and departid like '%"+ departId +"%'";
            whereStra = whereStra + " and a.departid like '%"+ departId +"%'";
		}
		String sql="select * from xdkdbwx "+whereStr;
		Connection connection = null;
		ResultSet resultSet=null;

		try {
			connection = com.geping.etl.utils.jdbc.JDBCUtils.getConnection();
			resultSet = com.geping.etl.utils.jdbc.JDBCUtils.Query(connection, sql);
			if (!resultSet.next()) {	//没有可校验的数据
				json.put("msg","-1");
				try {
					PrintWriter out = response.getWriter();
					out.write(json.toString());
					out.flush();
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return;
			}else {
			    //校验开关集合
                List<String> checknumList = checkRuleService.getChecknum("xdkdbwx");
                //在校验之前查重
				//标识名称
				String errorcode = "担保合同编码";
				String errorcode2 = "担保物编码";

				if (checknumList.contains("JS2304")){
				    if (sqlServer){
				        String sqlexist ="select id,gteecontractcode,gteegoodscode from xdkdbwx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,financeorgcode,gteecontractcode,loancontractcode,gteegoodscode from xdkdbwx where operationname != '申请删除' group by sjrq,financeorgcode,gteecontractcode,loancontractcode,gteegoodscode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.gteecontractcode = t2.gteecontractcode and t1.loancontractcode = t2.loancontractcode and t1.gteegoodscode=t2.gteegoodscode)";
                        String errorinfo  = "数据日期+金融机构代码+担保合同编码+被担保合同编码+担保物编码应唯一不唯一";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

						errorinfo = "数据日期+金融机构代码+担保合同编码+被担保合同编码+担保物编码应唯一不唯一（历史表）";
				        sqlexist = "select id,gteecontractcode,gteegoodscode from xdkdbwx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,financeorgcode,gteecontractcode,loancontractcode,gteegoodscode from xdkdbwxh  group by sjrq,financeorgcode,gteecontractcode,loancontractcode,gteegoodscode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.gteecontractcode = t2.gteecontractcode and t1.loancontractcode = t2.loancontractcode and t1.gteegoodscode=t2.gteegoodscode)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }else {
                        String sqlexist ="select id,gteecontractcode,gteegoodscode from xdkdbwx "+whereStr+" and (sjrq,financeorgcode,gteecontractcode,loancontractcode,gteegoodscode) in (select sjrq,financeorgcode,gteecontractcode,loancontractcode,gteegoodscode from xdkdbwx  where operationname != '申请删除'  group by sjrq,financeorgcode,gteecontractcode,loancontractcode,gteegoodscode having  count(1) > 1)";
                        String errorinfo  = "数据日期+金融机构代码+担保合同编码+被担保合同编码+担保物编码应唯一不唯一";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                        errorinfo = "数据日期+金融机构代码+担保合同编码+被担保合同编码+担保物编码应唯一不唯一（历史表）";
                        sqlexist = "select id,gteecontractcode,gteegoodscode from xdkdbwx "+whereStr+" and (sjrq,financeorgcode,gteecontractcode,loancontractcode,gteegoodscode) in (select sjrq,financeorgcode,gteecontractcode,loancontractcode,gteegoodscode from xdkdbwxh group by sjrq,financeorgcode,gteecontractcode,loancontractcode,gteegoodscode having count(1) > 0)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }
                }

                // 表间校验
                if (checknumList.contains("JS1897")){
                    String  errorinfo  = "担保合同编码应该在担保合同信息.担保合同编码中存在";
                    String  sqlexist ="select a.id,a.gteecontractcode,a.gteegoodscode from xdkdbwx a "+whereStra+"  and a.operationname!='申请删除' and  (a.gteecontractcode not in (select b.gteecontractcode from xdkdbht b INNER JOIN xdkdbwx c on b.sjrq = c.sjrq where b.operationname != '申请删除') and a.gteecontractcode not in (select e.gteecontractcode from xdkdbhth e INNER JOIN xdkdbwx f on e.sjrq = f.sjrq))";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }

				if (checknumList.contains("JS2778")){
					String  errorinfo  = "评估价值一般在1000元至100亿元范围内";
                    if (sqlServer){
                        String  sqlexist ="select a.id id,a.gteecontractcode,a.gteegoodscode from xdkdbwx a "+whereStra+" and exists(select 1 from (select sjrq,financeorgcode,gteecontractcode ,gteegoodscode from xdkdbwx where ltrim(assessvalue) is null group by sjrq,financeorgcode,gteecontractcode,gteegoodscode having sum(cast(assessvalue as bigint)) < 1000 or sum(cast(assessvalue as bigint)) > 1000000000) b where a.sjrq = b.sjrq and a.financeorgcode = b.financeorgcode and a.gteecontractcode = b.gteecontractcode and a.gteegoodscode=b.gteegoodscode)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }else {
                        String  sqlexist ="select a.id id,a.gteecontractcode,a.gteegoodscode from xdkdbwx a "+whereStra+" and exists(select 1 from (select sjrq,financeorgcode,gteecontractcode ,gteegoodscode from xdkdbwx where ltrim(assessvalue) is null group by sjrq,financeorgcode,gteecontractcode,gteegoodscode having sum(assessvalue) < 1000 or sum(assessvalue) > 1000000000) b where a.sjrq = b.sjrq and a.financeorgcode = b.financeorgcode and a.gteecontractcode = b.gteecontractcode and a.gteegoodscode=b.gteegoodscode)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }

				}

				if(checknumList.contains("JS2779")){
					String  errorinfo  = "当担保物票面价值不为空时，担保物票面价值一般在1000元至100亿元范围内";
					if (sqlServer){
                        String  sqlexist ="select a.id id,a.gteecontractcode,a.gteegoodscode from xdkdbwx a "+whereStra+" and exists(select 1 from (select sjrq,financeorgcode,gteecontractcode ,gteegoodscode from xdkdbwx where ltrim(gteegoodsamt) is null group by sjrq,financeorgcode,gteecontractcode,gteegoodscode having sum(cast(gteegoodsamt as bigint)) < 1000 or sum(cast(gteegoodsamt as bigint)) > 10000000000) b where a.sjrq = b.sjrq and a.financeorgcode = b.financeorgcode and a.gteecontractcode = b.gteecontractcode and a.gteegoodscode=b.gteegoodscode)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    }else {
                        String  sqlexist ="select a.id id,a.gteecontractcode,a.gteegoodscode from xdkdbwx a "+whereStra+" and exists(select 1 from (select sjrq,financeorgcode,gteecontractcode ,gteegoodscode from xdkdbwx where ltrim(gteegoodsamt) is null group by sjrq,financeorgcode,gteecontractcode,gteegoodscode having sum(gteegoodsamt) < 1000 or sum(gteegoodsamt) > 10000000000) b where a.sjrq = b.sjrq and a.financeorgcode = b.financeorgcode and a.gteecontractcode = b.gteecontractcode and a.gteegoodscode=b.gteegoodscode)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    }
				}


//				errorinfo="金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致";
//				sqlexist ="select a.id,a.gteecontractcode,a.gteegoodscode from xdkdbwx a  "+whereStra+" and a.operationname!='申请删除' and (a.financeorgcode <> (select b.finorgcode from xjrjgfz b INNER JOIN xdkdbwx c on b.sjrq = c.sjrq and b.inorgnum = c.financeorginnum where b.operationname != '申请删除') or a.financeorgcode <>(select e.finorgcode from xjrjgfzh e INNER JOIN xdkdbwx f on e.sjrq = f.sjrq and e.inorgnum = f.financeorginnum))";
//				inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

				resultSet = com.geping.etl.utils.jdbc.JDBCUtils.Query(connection, sql);
				while (true) {
					if (!resultSet.next()) break;
					Xdkdbwx xdkdbwx=new Xdkdbwx();
					Stringutil.getEntity(resultSet, xdkdbwx);
					CheckxdkdbwxUtils.guiZeJiaoYanForDWDKDBWXX(customSqlUtil,xdkdbwx,checknumList, errorMsg, errorId, rightId);

				}
			}
		} catch (Exception throwables) {
			throwables.printStackTrace();
		}finally {
			if (connection!=null){
				JDBCUtils.close();
			}
			if(CheckxdkdbwxUtils.fzList != null) {
				CheckxdkdbwxUtils.fzList.clear();
				CheckxdkdbwxUtils.fzList = null;
			}
			if(CheckxdkdbwxUtils.frList != null) {
				CheckxdkdbwxUtils.frList.clear();
				CheckxdkdbwxUtils.frList = null;
			}
		}

		//校验结束

		//修改状态 和 下载
		if (errorId!=null && errorId.size()>0){
			//-下载到本地
			StringBuffer errorMsgAll = new StringBuffer("");
			for(Map.Entry<String, String> entry : errorMsg.entrySet()) {
				errorMsgAll.append(entry.getValue()+"\r\n");
			}
			DownloadUtil.downLoad(errorMsgAll.toString(),target+File.separator+"checkout",fileName+".txt");
			errorMsg.clear();

			setMap.put("checkStatus","2");
			setMap.put("operationname"," ");

			customSqlUtil.updateByWhereBatch(tableName,setMap,errorId);
//			errorId.forEach(errId->{
//				whereMap.put("id",errId);
//				customSqlUtil.updateByWhere(tableName,setMap,whereMap);
//			});
			errorId.clear();
			json.put("msg","0");
		}else {
			if (b){
				json.put("msg","0");
			}else {
				json.put("msg","1");
			}
		}
		if (rightId!=null && rightId.size()>0){
			setMap.put("checkStatus","1");
			setMap.put("operationname"," ");

			customSqlUtil.updateByWhereBatch(tableName,setMap,rightId);
			rightId.clear();
		}

		customSqlUtil.saveLog("单位担保物信息校验成功","校验");
		try {
			PrintWriter out = response.getWriter();
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GetMapping("XDownLoadCheckDBW")
	public void XDownLoadCheckThree(HttpServletResponse response) {
		if (StringUtils.isBlank(target)){
			target=suisService.findAll().get(0).getMessagepath();
		}
		DownloadUtil.downLoadCheckFile(response,target + File.separator + "checkout",fileName+".txt");
	}
	//下载校验结果
	@RequestMapping(value="/XdownFileCheckdkdbwx",method=RequestMethod.GET)
	public void XdownFileCheckdkdbwx(HttpServletRequest request,HttpServletResponse response){
		InputStream input = null;
		OutputStream output = null;
		try {
			//String name = request.getParameter("name");
			//String messagepath = request.getParameter("messagepath");
			Object messagepath = request.getSession().getAttribute("messagepath");
			String filename = "单位贷款担保物信息校验结果.txt";
			File file = new File(messagepath.toString()+"\\checkout\\"+filename);
			if (file.exists()) {
				response.setCharacterEncoding("GBK");
				response.setContentType("application/octet-stream;charset=GBK");
				response.setHeader("Content-Disposition","attachment;filename="+ new String(filename.getBytes("GBK"), "ISO8859-1"));
				input = new FileInputStream(messagepath.toString()+"\\checkout\\"+filename);
				file.setWritable(true, false);
				int len = 0;
				byte[] buffer = new byte[1024];
				output = response.getOutputStream();
				while ((len = input.read(buffer)) > 0) {
					output.write(buffer, 0, len);
				}
				output.flush();
				output.close();
				input.close();
				file.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (input != null) {
					input.close();
				}
				
				if (output != null) {
					output.flush();
					output.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	//提交
	@RequestMapping(value="/Xtijiaodkdbwx",method=RequestMethod.POST)
	public void Xtijiaodkdbwx(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				result = xs.updateXdkdbwxOnDatastatus("1",sys_User.getLoginid(),departId);
				//result = ServiceTemplate.YeWuMoBan(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					List<String> idList = new ArrayList<>();
					for (int i = 0; i < idarr.length; i++) {
						idList.add(idarr[i]);
					}
					result = xs.updateXdkdbwxOnDatastatus("1",sys_User.getLoginid(),idList);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					countUtil.handleCount("xdkdbwx", 1, "add", 1);
				}else if("some".equals(deletetype)) {
					countUtil.handleCount("xdkdbwx", 1, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}		
	
	//申请删除
	@RequestMapping(value="/Xshenqingshanchudkdbwx",method=RequestMethod.POST)
	public void Xshenqingshanchudkdbwx(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xdkdbwx set operationname='申请删除',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and (operationname is null or operationname=' ') and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xdkdbwx set operationname='申请删除',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}	
	
	//申请修改
	@RequestMapping(value="/Xshenqingxiugaidkdbwx",method=RequestMethod.POST)
	public void Xshenqingxiugaidkdbwx(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xdkdbwx set operationname='申请修改',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and (operationname is null or operationname=' ') and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xdkdbwx set operationname='申请修改',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//同意申请
	@RequestMapping(value="/Xtongyishenqingdkdbwx",method=RequestMethod.POST)
	public void Xtongyishenqingdkdbwx(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xdkdbwx set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where datastatus='1' and operator !='"+sys_User.getLoginid()+"' and (operationname is not null or operationname!=' ') and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xdkdbwx set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//同意申请删除
	@RequestMapping(value="/Xtongyishenqingshanchudkdbwx",method=RequestMethod.POST)
	public void Xtongyishenqingshanchudkdbwx(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xdkdbwx set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where datastatus='1' and (operationname is not null or operationname!='');";
				//String sql = "delete from xdkdbwx where datastatus='1' and operator <>'"+sys_User.getLoginid()+"' and operationname='申请删除'";
				String sql = "delete from xdkdbwx where datastatus='1' and operationname='申请删除'" + " and departid like '"+departId+"' and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					//String sql = "update xdkdbwx set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where id in(";
					String sql = "delete from xdkdbwx where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					countUtil.handleCount("xdkdbwx", 3, "reduce", result);
				}else if("some".equals(deletetype)) {
					countUtil.handleCount("xdkdbwx", 3, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//拒绝申请
	@RequestMapping(value="/Xjujueshenqingdkdbwx",method=RequestMethod.POST)
	public void Xjujueshenqingdkdbwx(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xdkdbwx set operationname='',datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and operator <>'"+sys_User.getLoginid()+"' and operationname='申请删除'";
				String sql = "update xdkdbwx set operationname=' ',datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and operationname='申请删除'" + " and departid like '"+departId+"' and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xdkdbwx set operationname=' ',datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//审核通过
	@RequestMapping(value="/Xshenhetongguodkdbwx",method=RequestMethod.POST)
	public void Xshenhetongguodkdbwx(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xdkdbwx set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and operator <>'"+sys_User.getLoginid()+"' and (operationname='' or operationname is null)";
				String sql = "update xdkdbwx set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and (operationname=' ' or operationname is null) and departid like '"+departId+"' and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "update xdkdbwx set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					countUtil.handleCount("xdkdbwx", 3, "reduce", result);
				}else if("some".equals(deletetype)) {
					countUtil.handleCount("xdkdbwx", 3, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//审核不通过
	@RequestMapping(value="/Xshenhebutongguodkdbwx",method=RequestMethod.POST)
	public void Xshenhebutongguodkdbwx(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		String yuanyin = request.getParameter("yuanyin");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xdkdbwx set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and operator <>'"+sys_User.getLoginid()+"' and (operationname='' or operationname is null)";
				String sql = "update xdkdbwx set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and (operationname=' ' or operationname is null) and departid like '"+departId+"' and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "update xdkdbwx set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					countUtil.handleCount("xdkdbwx", 2, "reduce", result);
				}else if("some".equals(deletetype)) {
					countUtil.handleCount("xdkdbwx", 2, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	
	//访问单位贷款担保物信息生成报文页面
	@RequestMapping(value="/XshowDWDKDBWXXRP",method=RequestMethod.GET)
	public ModelAndView XshowDWDKDBWXXRPUi(HttpServletRequest request){
		String status = request.getParameter("status");
		ModelAndView model = new ModelAndView();
		model.addObject("datamanege",status);
		model.setViewName("unitloan/createreport/dwdkdbwxx_bw");
		return model;
	}
	
	//生成报文页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindDWDKDBWXXscbw", method = RequestMethod.POST)
	public void XfindDWDKDBWXXscbw(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		String selectdbhtbm = request.getParameter("selectdbhtbm");
		String selectdkhtbm = request.getParameter("selectdkhtbm");
		String selectdkdbwbm = request.getParameter("selectdkdbwbm");
		String zhuangtai = "3";
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		String departId = departUtil.getDepart(sys_user);
		Specification<Xdkdbwx> querySpecifi = new Specification<Xdkdbwx>() {
			@Override
			public Predicate toPredicate(Root<Xdkdbwx> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				predicates.add(criteriaBuilder.like(root.get("departid"), departId));
				if (StringUtils.isNotBlank(selectdbhtbm)) {
					predicates.add(criteriaBuilder.like(root.get("gteecontractcode"), "%" + selectdbhtbm.trim() + "%"));
				}
				if (StringUtils.isNotBlank(selectdkhtbm)) {
					predicates.add(criteriaBuilder.like(root.get("loancontractcode"), "%" + selectdkhtbm.trim() + "%"));
				}
				if (StringUtils.isNotBlank(selectdkdbwbm)) {
					predicates.add(criteriaBuilder.like(root.get("gteegoodscode"), "%" + selectdkdbwbm.trim() + "%"));
				}
				predicates.add(criteriaBuilder.equal(root.get("datastatus"),zhuangtai));
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		// Pageable 接口通常使用的其 PageRequest 实现类. 其中封装了需要分页的信息
		PageRequest pr = new PageRequest(page, size); // intPage从0开始
		List<Xdkdbwx> list = null;
		Page<Xdkdbwx> gcPage = xs.findAll(querySpecifi, pr);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		result.put("rows", list);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//数据打回
	@RequestMapping(value="/Xdahuidkdbwx",method=RequestMethod.POST)
	public void Xdahuidkdbwx(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String checktype = request.getParameter("checktype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(checktype)) {
				String sql = "update xdkdbwx set checkstatus='0',datastatus='0',operationname=' ',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='3' and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(checktype)) {
				String selectid = request.getParameter("selectid");
				if (StringUtils.isNotBlank(selectid)) {
					idarr = selectid.split("-");
					String sql = "update xdkdbwx set checkstatus='0',datastatus='0',operationname=' ',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='3' and id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				if("all".equals(checktype)) {
					countUtil.handleCount("xdkdbwx", 0, "add", result);
				}else if("some".equals(checktype)) {
					countUtil.handleCount("xdkdbwx", 0, "add", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}		
	
	//生成报文
//	@RequestMapping(value = "/Xcreatemessagedkdbwx", method = RequestMethod.POST)
//    public void Xcreatemessagedkdbwx(HttpServletRequest request,HttpServletResponse response){
//		PrintWriter out = null;
//		String checktype = request.getParameter("checktype");
//		try {
//			response.setContentType("text/html");
//			response.setContentType("text/plain; charset=utf-8");
//			out = response.getWriter();
//			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
//			String result = "0";
//			List<SUpOrgInfoSet> olist = sus.findAll();
//			String messagepath = "C:";
//			String bsjgdm = "91310000607342023U";
//			if(olist.size() > 0) {
//				messagepath = olist.get(0).getMessagepath();
//				bsjgdm = olist.get(0).getBankcodewangdian();
//			}
//			//DBWXX
//			String filename = bsjgdm+"_DBWXX_"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))+".csv";
//			
//			if("all".equals(checktype)) {
//				result = CreateMessageUtils.shengChengBaoWen(messagepath,filename,TableCodeEnum.DWDKDBWXX);
//				if("1".equals(result)) {
//					xs.updateXdkdbwxOnDatastatus("4",sys_User.getLoginid());
//					out.write("1");
//				}else {
//					out.write("0");
//				}
//			}else if("some".equals(checktype)) {
//				String rows = request.getParameter("rows");
//				// 先转成json数组再将json转化成list数组
//				JSONArray jsonArray = JSONArray.fromObject(rows);
//				@SuppressWarnings("unchecked")
//				List<Xdkdbwx> list = JSONArray.toList(jsonArray, new Xdkdbwx(), new JsonConfig());
//				result = CreateMessageUtils.shengChengBaoWen(messagepath,filename,list,TableCodeEnum.DWDKDBWXX);
//				if("1".equals(result)) {
//					for(int i=0;i<list.size();i++) {
//						list.get(i).setDatastatus("4");
//					}
//					xs.Save(list);
//					out.write("1");
//				}else {
//					out.write("0");
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	// 表间校验
	public void inTableCalibration(Map<String,String> setMap,List<String> errorId,Map<String,String> whereMap,LinkedHashMap<String, String> errorMsg,String sql,String errorcode,String errorinfo,String errorcode2) {
		List<Object[]> exist = customSqlUtil.executeQuery(sql);
		if (exist!=null && exist.size()>0){
			setMap.put("checkStatus","2");
			setMap.put("operationname"," ");
			exist.forEach(errId->{
				errorId.add(String.valueOf(errId[0]));
				whereMap.put("id", String.valueOf(errId[0]));
				if(!errorMsg.containsKey(String.valueOf(errId[0]))) {
					errorMsg.put(String.valueOf(errId[0]), errorcode+":"+String.valueOf(errId[1])+"，"+errorcode2+":"+String.valueOf(errId[2])+"]->\r\n");
				}
				String str = errorMsg.get(String.valueOf(errId[0]));
				str = str + errorinfo+"|";
				errorMsg.put(String.valueOf(errId[0]), str);
			});
			whereMap.clear();
			setMap.clear();
			exist.clear();
		}
	}
	
}
