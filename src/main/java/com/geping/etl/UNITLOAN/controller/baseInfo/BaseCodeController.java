package com.geping.etl.UNITLOAN.controller.baseInfo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountryTwo;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseEducation;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseNation;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseAindustryService;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseAreaService;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseBindustryService;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseCountryService;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseCountryTwoService;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseCurrencyService;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseEducationService;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseNationService;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.Excel2007Utils;
import com.geping.etl.UNITLOAN.util.ExcelUploadUtil;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.UNITLOAN.util.TableCodeEnum;

import net.sf.json.JSON;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

/**
 * 基础代码控制器
 * @author WuZengWen
 * @date 2020年6月12日 上午11:07:26
 */
@RestController
public class BaseCodeController {

	@Autowired
	private BaseAindustryService yjhy;
	@Autowired
	private BaseAreaService xzqh;
	@Autowired
	private BaseCountryService gjdq;
	@Autowired
	private BaseAindustryService hydl;
	@Autowired
	private BaseBindustryService ejhy;
	@Autowired
	private BaseCountryService gjdm;
	@Autowired
	private BaseCurrencyService bzdm;
	@Autowired
	private BaseCountryTwoService bcts;
	@Autowired
	private BaseEducationService bes;
	@Autowired
	private BaseNationService bns;
	@Autowired
	private ExcelUploadUtil excelUploadUtil;
	@Autowired
	private CustomSqlUtil customSqlUtil;
	@Autowired
	private JdbcTemplate jt;
	
	//跳转到数据币种页面
    @GetMapping(value = "/XCurrencyUi")
    public ModelAndView XCurrencyUi(HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        ModelAndView model = new ModelAndView();
        model.setViewName("unitloan/basecode/shujubizhong");
        return model;
    }
    
    //跳转到地区代码页面
    @GetMapping(value = "/XAreaUi")
    public ModelAndView XAreaUi(HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        ModelAndView model = new ModelAndView();
        model.setViewName("unitloan/basecode/diqudaima");
        return model;
    }
    
    //跳转到国家代码页面
    @GetMapping(value = "/XCountryUi")
    public ModelAndView XCountryUi(HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        ModelAndView model = new ModelAndView();
        model.setViewName("unitloan/basecode/guojiadaima");
        return model;
    }
    
    //跳转到行业大类页面
    @GetMapping(value = "/XAIndustryUi")
    public ModelAndView XAIndustryUi(HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        ModelAndView model = new ModelAndView();
        model.setViewName("unitloan/basecode/hangyedalei");
        return model;
    }
    
    //跳转到行业分类页面
    @GetMapping(value = "/XIndustryUi")
    public ModelAndView XIndustryUi(HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        ModelAndView model = new ModelAndView();
        model.setViewName("unitloan/basecode/hangyefenlei");
        return model;
    }
    
    //跳转到企业规模页面
    @GetMapping(value = "/XEnterpriseUi")
    public ModelAndView XEnterpriseUi(HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        ModelAndView model = new ModelAndView();
        model.setViewName("unitloan/basecode/qiyeguimo");
        return model;
    }
    
    //跳转到国籍代码页面
    @GetMapping(value = "/XCountryTwoUi")
    public ModelAndView XCountryTwoUi(HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        ModelAndView model = new ModelAndView();
        model.setViewName("unitloan/basecode/guojidaima");
        return model;
    }
    
    //跳转到学历代码页面
    @GetMapping(value = "/XEducationUi")
    public ModelAndView XEducationUi(HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        ModelAndView model = new ModelAndView();
        model.setViewName("unitloan/basecode/xuelidaima");
        return model;
    }
    
    //跳转到民族代码页面
    @GetMapping(value = "/XNationUi")
    public ModelAndView XNationUi(HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        ModelAndView model = new ModelAndView();
        model.setViewName("unitloan/basecode/minzudaima");
        return model;
    }
	
	// 页面easyui自加载请求
	@RequestMapping(value = "/XfindCurrency", method = RequestMethod.POST)
	public void XfindCurrency(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);

		Specification<BaseCurrency> querySpecifi = new Specification<BaseCurrency>() {
			@Override
			public Predicate toPredicate(Root<BaseCurrency> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		PageRequest pr = new PageRequest(page, size); // intPage从0开始
		List<BaseCurrency> list = null;
		Page<BaseCurrency> gcPage = (Page<BaseCurrency>) bzdm.findAll(querySpecifi, pr);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		JSON json = JSONSerializer.toJSON(list);
		result.put("rows", json);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 最终将无用对象置空
			if (pr != null) {
				pr = null;
			}
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (json != null) {
				json = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	// 页面easyui自加载请求
	@RequestMapping(value = "/XfindBaseArea", method = RequestMethod.POST)
	public void XfindBaseArea(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);

		Specification<BaseArea> querySpecifi = new Specification<BaseArea>() {
			@Override
			public Predicate toPredicate(Root<BaseArea> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		PageRequest pr = new PageRequest(page, size); // intPage从0开始
		List<BaseArea> list = null;
		Page<BaseArea> gcPage = (Page<BaseArea>) xzqh.findAll(querySpecifi, pr);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		JSON json = JSONSerializer.toJSON(list);
		result.put("rows", json);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 最终将无用对象置空
			if (pr != null) {
				pr = null;
			}
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (json != null) {
				json = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	// 页面easyui自加载请求
	@RequestMapping(value = "/XfindBaseCountry", method = RequestMethod.POST)
	public void XfindBaseCountry(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);

		Specification<BaseCountry> querySpecifi = new Specification<BaseCountry>() {
			@Override
			public Predicate toPredicate(Root<BaseCountry> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		PageRequest pr = new PageRequest(page, size); // intPage从0开始
		List<BaseCountry> list = null;
		Page<BaseCountry> gcPage = (Page<BaseCountry>) gjdq.findAll(querySpecifi, pr);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		JSON json = JSONSerializer.toJSON(list);
		result.put("rows", json);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 最终将无用对象置空
			if (pr != null) {
				pr = null;
			}
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (json != null) {
				json = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	// 页面easyui自加载请求
	@RequestMapping(value = "/XfindBaseAindustry", method = RequestMethod.POST)
	public void XfindBaseAindustry(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);

		Specification<BaseAindustry> querySpecifi = new Specification<BaseAindustry>() {
			@Override
			public Predicate toPredicate(Root<BaseAindustry> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		PageRequest pr = new PageRequest(page, size); // intPage从0开始
		List<BaseAindustry> list = null;
		Page<BaseAindustry> gcPage = (Page<BaseAindustry>) hydl.findAll(querySpecifi, pr);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		JSON json = JSONSerializer.toJSON(list);
		result.put("rows", json);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 最终将无用对象置空
			if (pr != null) {
				pr = null;
			}
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (json != null) {
				json = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	// 页面easyui自加载请求
	@RequestMapping(value = "/XfindBaseBindustry", method = RequestMethod.POST)
	public void XfindBaseBindustry(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);

		Specification<BaseBindustry> querySpecifi = new Specification<BaseBindustry>() {
			@Override
			public Predicate toPredicate(Root<BaseBindustry> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		PageRequest pr = new PageRequest(page, size); // intPage从0开始
		List<BaseBindustry> list = null;
		Page<BaseBindustry> gcPage = (Page<BaseBindustry>) ejhy.findAll(querySpecifi, pr);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		JSON json = JSONSerializer.toJSON(list);
		result.put("rows", json);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 最终将无用对象置空
			if (pr != null) {
				pr = null;
			}
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (json != null) {
				json = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	// 页面easyui自加载请求
	@RequestMapping(value = "/XfindEnterprise", method = RequestMethod.POST)
	public void XfindEnterprise(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);

		/*Specification<AreaCode> querySpecifi = new Specification<AreaCode>() {
			@Override
			public Predicate toPredicate(Root<AreaCode> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};*/
		PageRequest pr = new PageRequest(page, size); // intPage从0开始
		//List<AreaCode> list = null;
		//Page<AreaCode> gcPage = (Page<AreaCode>) as.findAll(querySpecifi, pr);
		//list = gcPage.getContent();
		JSONObject result = new JSONObject();
		//JSON json = JSONSerializer.toJSON(list);
		result.put("rows", null);
		result.put("total", 0);
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 最终将无用对象置空
			if (pr != null) {
				pr = null;
			}
			/*if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (json != null) {
				json = null;
			}*/
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	// 页面easyui自加载请求
		@RequestMapping(value = "/XfindBaseCountryTwo", method = RequestMethod.POST)
		public void XfindBaseCountryTwo(HttpServletResponse response, HttpServletRequest request) {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			String currentPageNumber = request.getParameter("page"); // 获取查询页数
			String currentPageSize = request.getParameter("rows"); // 获取每页条数
			int page = Integer.valueOf(currentPageNumber) - 1;
			int size = Integer.valueOf(currentPageSize);

			Specification<BaseCountryTwo> querySpecifi = new Specification<BaseCountryTwo>() {
				@Override
				public Predicate toPredicate(Root<BaseCountryTwo> root, CriteriaQuery<?> criteriaQuery,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Sort sort = new Sort(Sort.Direction.ASC, "countrytwocode");
			PageRequest pr = new PageRequest(page, size,sort); // intPage从0开始
			List<BaseCountryTwo> list = null;
			Page<BaseCountryTwo> gcPage = (Page<BaseCountryTwo>) bcts.findAll(querySpecifi, pr);
			list = gcPage.getContent();
			JSONObject result = new JSONObject();
			JSON json = JSONSerializer.toJSON(list);
			result.put("rows", json);
			result.put("total", gcPage.getTotalElements());
			PrintWriter out = null;
			try {
				out = response.getWriter();
				out.write(result.toString());
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				// 最终将无用对象置空
				if (pr != null) {
					pr = null;
				}
				if (gcPage != null) {
					gcPage = null;
				}
				if (list != null) {
					list = null;
				}
				if (json != null) {
					json = null;
				}
				if (result != null) {
					result = null;
				}
				if (out != null) {
					out.flush();
					out.close();
				}
			}
		}
		
		// 页面easyui自加载请求
		@RequestMapping(value = "/XfindBaseEducation", method = RequestMethod.POST)
		public void XfindBaseEducation(HttpServletResponse response, HttpServletRequest request) {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			String currentPageNumber = request.getParameter("page"); // 获取查询页数
			String currentPageSize = request.getParameter("rows"); // 获取每页条数
			int page = Integer.valueOf(currentPageNumber) - 1;
			int size = Integer.valueOf(currentPageSize);

			Specification<BaseEducation> querySpecifi = new Specification<BaseEducation>() {
				@Override
				public Predicate toPredicate(Root<BaseEducation> root, CriteriaQuery<?> criteriaQuery,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Sort sort = new Sort(Sort.Direction.ASC, "educationcode");
			PageRequest pr = new PageRequest(page, size, sort); // intPage从0开始
			List<BaseEducation> list = null;
			Page<BaseEducation> gcPage = (Page<BaseEducation>) bes.findAll(querySpecifi, pr);
			list = gcPage.getContent();
			JSONObject result = new JSONObject();
			JSON json = JSONSerializer.toJSON(list);
			result.put("rows", json);
			result.put("total", gcPage.getTotalElements());
			PrintWriter out = null;
			try {
				out = response.getWriter();
				out.write(result.toString());
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				// 最终将无用对象置空
				if (pr != null) {
					pr = null;
				}
				if (gcPage != null) {
					gcPage = null;
				}
				if (list != null) {
					list = null;
				}
				if (json != null) {
					json = null;
				}
				if (result != null) {
					result = null;
				}
				if (out != null) {
					out.flush();
					out.close();
				}
			}
		}
		
		// 页面easyui自加载请求
		@RequestMapping(value = "/XfindBaseNation", method = RequestMethod.POST)
		public void XfindBaseNation(HttpServletResponse response, HttpServletRequest request) {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			String currentPageNumber = request.getParameter("page"); // 获取查询页数
			String currentPageSize = request.getParameter("rows"); // 获取每页条数
			int page = Integer.valueOf(currentPageNumber) - 1;
			int size = Integer.valueOf(currentPageSize);

			Specification<BaseNation> querySpecifi = new Specification<BaseNation>() {
				@Override
				public Predicate toPredicate(Root<BaseNation> root, CriteriaQuery<?> criteriaQuery,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Sort sort = new Sort(Sort.Direction.ASC, "nationcode");
			PageRequest pr = new PageRequest(page, size,sort); // intPage从0开始
			List<BaseNation> list = null;
			Page<BaseNation> gcPage = (Page<BaseNation>) bns.findAll(querySpecifi, pr);
			list = gcPage.getContent();
			JSONObject result = new JSONObject();
			JSON json = JSONSerializer.toJSON(list);
			result.put("rows", json);
			result.put("total", gcPage.getTotalElements());
			PrintWriter out = null;
			try {
				out = response.getWriter();
				out.write(result.toString());
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				// 最终将无用对象置空
				if (pr != null) {
					pr = null;
				}
				if (gcPage != null) {
					gcPage = null;
				}
				if (list != null) {
					list = null;
				}
				if (json != null) {
					json = null;
				}
				if (result != null) {
					result = null;
				}
				if (out != null) {
					out.flush();
					out.close();
				}
			}
		}
	
    
	@RequestMapping(value="/getAreaCode",method=RequestMethod.POST)
	public void getAreaCode(HttpServletResponse response){
		PrintWriter out = null;
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			List<BaseArea> list = (List<BaseArea>) huoQuJiChuShuJu(TableCodeEnum.XZQHDM);
			JSONObject json = new JSONObject();
	        out = response.getWriter();
			json.put("list",list);
			out.write(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if(out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	@RequestMapping(value="/getAindustryCode",method=RequestMethod.POST)
	public void getAindustryCode(HttpServletResponse response){
		PrintWriter out = null;
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			List<BaseAindustry> list = (List<BaseAindustry>) huoQuJiChuShuJu(TableCodeEnum.YJHYDM);
			JSONObject json = new JSONObject();
	        out = response.getWriter();
			json.put("list",list);
			out.write(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if(out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//导入地区
	@PostMapping(value = "Xdaorudiqudaima",produces = "text/plain;charset=UTF-8")
	public void Xdaorudiqudaima(HttpServletRequest request,HttpServletResponse response){
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		//File uploadFile = null;
		//OPCPackage opcPackage=null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multiRequest.getFile("excelfile");
			//uploadFile = excelUploadUtil.uploadFile(file);
			JSONObject json = new JSONObject();
			String[] heards = {"行政区划代码", "地区名称", "地区描述"};
			json = Excel2007Utils.daoruwenjian(file, heards);
			if("0".equals(json.get("code"))) {
				List<List<Object>> objects = (List<List<Object>>) json.get("data");
				List<BaseArea> list = new ArrayList<>();
				for(int j=0;j<objects.size();j++) {
					BaseArea ba = new BaseArea();
					ba.setId(Stringutil.getUUid());
					ba.setAreacode(objects.get(j).get(0).toString());
					ba.setAreaname(objects.get(j).get(1).toString());
					ba.setAreadesc(objects.get(j).get(2).toString());
					list.add(ba);
				}
				/*for(List<Object> ol : objects) {
					BaseArea ba = new BaseArea();
					ba.setId(Stringutil.getUUid());
					ba.setAreacode(ol.get(0).toString());
					ba.setAreaname(ol.get(1).toString());
					ba.setAreadesc(ol.get(2).toString());
					list.add(ba);
				}*/
				xzqh.daoRuFuGai(list);
				json.put("msg","导入成功");
			}
			customSqlUtil.saveLog("行政区划代码->"+json.get("msg"),"导入");
			out.write(json.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (out != null) {
					out.flush();
					out.close();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	//导入国家
	@PostMapping(value = "Xdaoruguojiadaima",produces = "text/plain;charset=UTF-8")
	public void Xdaoruguojiadaima(HttpServletRequest request,HttpServletResponse response){
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		//File uploadFile = null;
		//OPCPackage opcPackage=null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multiRequest.getFile("excelfile");
			//uploadFile = excelUploadUtil.uploadFile(file);
			JSONObject json = new JSONObject();
			String[] heards = {"国家地区代码", "国家名称", "国家描述"};
			json = Excel2007Utils.daoruwenjian(file, heards);
			if("0".equals(json.get("code"))) {
				List<List<Object>> objects = (List<List<Object>>) json.get("data");
				List<BaseCountry> list = new ArrayList<>();
				for(int j=0;j<objects.size();j++) {
					BaseCountry ba = new BaseCountry();
					ba.setId(Stringutil.getUUid());
					ba.setCountrycode(objects.get(j).get(0).toString());
					ba.setCountryname(objects.get(j).get(1).toString());
					ba.setCountrydesc(objects.get(j).get(2).toString());
					list.add(ba);
				}
				/*for(List<Object> ol : objects) {
					BaseArea ba = new BaseArea();
					ba.setId(Stringutil.getUUid());
					ba.setAreacode(ol.get(0).toString());
					ba.setAreaname(ol.get(1).toString());
					ba.setAreadesc(ol.get(2).toString());
					list.add(ba);
				}*/
				gjdq.daoRuFuGai(list);
				json.put("msg","导入成功");
			}
			customSqlUtil.saveLog("国家地区代码->"+json.get("msg"),"导入");
			out.write(json.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (out != null) {
					out.flush();
					out.close();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	//导入一级行业代码
	@PostMapping(value = "Xdaoruhangyedalei",produces = "text/plain;charset=UTF-8")
	public void Xdaoruhangyedalei(HttpServletRequest request,HttpServletResponse response){
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		//File uploadFile = null;
		//OPCPackage opcPackage=null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multiRequest.getFile("excelfile");
			//uploadFile = excelUploadUtil.uploadFile(file);
			JSONObject json = new JSONObject();
			String[] heards = {"一级行业代码", "一级行业名称", "一级行业描述"};
			json = Excel2007Utils.daoruwenjian(file, heards);
			if("0".equals(json.get("code"))) {
				List<List<Object>> objects = (List<List<Object>>) json.get("data");
				List<BaseAindustry> list = new ArrayList<>();
				for(int j=0;j<objects.size();j++) {
					BaseAindustry bb = new BaseAindustry();
					bb.setId(Stringutil.getUUid());
					bb.setAindustrycode(objects.get(j).get(0).toString());
					bb.setAindustryname(objects.get(j).get(1).toString());
					bb.setAindustrydesc(objects.get(j).get(2).toString());
					list.add(bb);
				}
				/*for(List<Object> ol : objects) {
					BaseBindustry bb = new BaseBindustry();
					bb.setId(Stringutil.getUUid());
					bb.setBindustrycode(ol.get(0).toString());
					bb.setBindustryname(ol.get(1).toString());
					bb.setBindustrydesc(ol.get(2).toString());
					list.add(bb);
				}*/
				hydl.daoRuFuGai(list);
				json.put("msg","导入成功");
			}
			customSqlUtil.saveLog("一级行业代码->"+json.get("msg"),"导入");
			out.write(json.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (out != null) {
					out.flush();
					out.close();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}	

	//导入二级行业代码
	@PostMapping(value = "Xdaoruhangyefenlei",produces = "text/plain;charset=UTF-8")
	public void Xdaoruhangyefenlei(HttpServletRequest request,HttpServletResponse response){
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		//File uploadFile = null;
		//OPCPackage opcPackage=null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multiRequest.getFile("excelfile");
			//uploadFile = excelUploadUtil.uploadFile(file);
			JSONObject json = new JSONObject();
			String[] heards = {"二级行业代码", "二级行业名称", "二级行业描述"};
			json = Excel2007Utils.daoruwenjian(file, heards);
			if("0".equals(json.get("code"))) {
				List<List<Object>> objects = (List<List<Object>>) json.get("data");
				List<BaseBindustry> list = new ArrayList<>();
				for(int j=0;j<objects.size();j++) {
					BaseBindustry bb = new BaseBindustry();
					bb.setId(Stringutil.getUUid());
					bb.setBindustrycode(objects.get(j).get(0).toString());
					bb.setBindustryname(objects.get(j).get(1).toString());
					bb.setBindustrydesc(objects.get(j).get(2).toString());
					list.add(bb);
				}
				/*for(List<Object> ol : objects) {
					BaseBindustry bb = new BaseBindustry();
					bb.setId(Stringutil.getUUid());
					bb.setBindustrycode(ol.get(0).toString());
					bb.setBindustryname(ol.get(1).toString());
					bb.setBindustrydesc(ol.get(2).toString());
					list.add(bb);
				}*/
				ejhy.daoRuFuGai(list);
				json.put("msg","导入成功");
			}
			customSqlUtil.saveLog("二级行业代码->"+json.get("msg"),"导入");
			out.write(json.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (out != null) {
					out.flush();
					out.close();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}		
		
	//导入币种
	@PostMapping(value = "Xdaorushujubizhong",produces = "text/plain;charset=UTF-8")
	public void Xdaorushujubizhong(HttpServletRequest request,HttpServletResponse response){
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		//File uploadFile = null;
		//OPCPackage opcPackage=null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multiRequest.getFile("excelfile");
			//uploadFile = excelUploadUtil.uploadFile(file);
			JSONObject json = new JSONObject();
			String[] heards = {"币种代码", "币种名称", "币种描述"};
			json = Excel2007Utils.daoruwenjian(file, heards);
			if("0".equals(json.get("code"))) {
				List<List<Object>> objects = (List<List<Object>>) json.get("data");
				List<BaseCurrency> list = new ArrayList<>();
				for(int j=0;j<objects.size();j++) {
					BaseCurrency bc = new BaseCurrency();
					bc.setId(Stringutil.getUUid());
					bc.setCurrencycode(objects.get(j).get(0).toString());
					bc.setCurrencyname(objects.get(j).get(1).toString());
					bc.setCurrencydesc(objects.get(j).get(2).toString());
					list.add(bc);
				}
				/*for(List<Object> ol : objects) {
					BaseCurrency bc = new BaseCurrency();
					bc.setId(Stringutil.getUUid());
					bc.setCurrencycode(ol.get(0).toString());
					bc.setCurrencyname(ol.get(1).toString());
					bc.setCurrencydesc(ol.get(2).toString());
					list.add(bc);
				}*/
				bzdm.daoRuFuGai(list);
				json.put("msg","导入成功");
			}
			customSqlUtil.saveLog("币种代码->"+json.get("msg"),"导入");
			out.write(json.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (out != null) {
					out.flush();
					out.close();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}		
	
	//导入国籍
		@PostMapping(value = "Xdaoruguojidaima",produces = "text/plain;charset=UTF-8")
		public void Xdaoruguojidaima(HttpServletRequest request,HttpServletResponse response){
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			//File uploadFile = null;
			//OPCPackage opcPackage=null;
			PrintWriter out = null;
			try {
				out = response.getWriter();
				MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
				MultipartFile file = multiRequest.getFile("excelfile");
				//uploadFile = excelUploadUtil.uploadFile(file);
				JSONObject json = new JSONObject();
				String[] heards = {"国籍代码", "国籍名称", "国籍描述"};
				json = Excel2007Utils.daoruwenjian(file, heards);
				if("0".equals(json.get("code"))) {
					List<List<Object>> objects = (List<List<Object>>) json.get("data");
					List<BaseCountryTwo> list = new ArrayList<>();
					for(int j=0;j<objects.size();j++) {
						BaseCountryTwo ba = new BaseCountryTwo();
						ba.setId(Stringutil.getUUid());
						ba.setCountrytwocode(objects.get(j).get(0).toString());
						ba.setCountrytwoname(objects.get(j).get(1).toString());
						ba.setCountrytwodesc(objects.get(j).get(2).toString());
						list.add(ba);
					}
					/*for(List<Object> ol : objects) {
						BaseArea ba = new BaseArea();
						ba.setId(Stringutil.getUUid());
						ba.setAreacode(ol.get(0).toString());
						ba.setAreaname(ol.get(1).toString());
						ba.setAreadesc(ol.get(2).toString());
						list.add(ba);
					}*/
					bcts.daoRuFuGai(list);
					json.put("msg","导入成功");
				}
				customSqlUtil.saveLog("国籍代码->"+json.get("msg"),"导入");
				out.write(json.toString());
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if (out != null) {
						out.flush();
						out.close();
					}
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}
		
		//导入学历
		@PostMapping(value = "Xdaoruxuelidaima",produces = "text/plain;charset=UTF-8")
		public void Xdaoruxuelidaima(HttpServletRequest request,HttpServletResponse response){
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			//File uploadFile = null;
			//OPCPackage opcPackage=null;
			PrintWriter out = null;
			try {
				out = response.getWriter();
				MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
				MultipartFile file = multiRequest.getFile("excelfile");
				//uploadFile = excelUploadUtil.uploadFile(file);
				JSONObject json = new JSONObject();
				String[] heards = {"学历代码", "学历名称", "学历描述"};
				json = Excel2007Utils.daoruwenjian(file, heards);
				if("0".equals(json.get("code"))) {
					List<List<Object>> objects = (List<List<Object>>) json.get("data");
					List<BaseEducation> list = new ArrayList<>();
					for(int j=0;j<objects.size();j++) {
						BaseEducation ba = new BaseEducation();
						ba.setId(Stringutil.getUUid());
						ba.setEducationcode(objects.get(j).get(0).toString());
						ba.setEducationname(objects.get(j).get(1).toString());
						ba.setEducationdesc(objects.get(j).get(2).toString());
						list.add(ba);
					}
					/*for(List<Object> ol : objects) {
						BaseArea ba = new BaseArea();
						ba.setId(Stringutil.getUUid());
						ba.setAreacode(ol.get(0).toString());
						ba.setAreaname(ol.get(1).toString());
						ba.setAreadesc(ol.get(2).toString());
						list.add(ba);
					}*/
					bes.daoRuFuGai(list);
					json.put("msg","导入成功");
				}
				customSqlUtil.saveLog("学历代码->"+json.get("msg"),"导入");
				out.write(json.toString());
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if (out != null) {
						out.flush();
						out.close();
					}
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}
		
		//导入民族
		@PostMapping(value = "Xdaoruminzudaima",produces = "text/plain;charset=UTF-8")
		public void Xdaoruminzudaima(HttpServletRequest request,HttpServletResponse response){
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			//File uploadFile = null;
			//OPCPackage opcPackage=null;
			PrintWriter out = null;
			try {
				out = response.getWriter();
				MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
				MultipartFile file = multiRequest.getFile("excelfile");
				//uploadFile = excelUploadUtil.uploadFile(file);
				JSONObject json = new JSONObject();
				String[] heards = {"民族代码", "民族名称", "民族描述"};
				json = Excel2007Utils.daoruwenjian(file, heards);
				if("0".equals(json.get("code"))) {
					List<List<Object>> objects = (List<List<Object>>) json.get("data");
					List<BaseNation> list = new ArrayList<>();
					for(int j=0;j<objects.size();j++) {
						BaseNation ba = new BaseNation();
						ba.setId(Stringutil.getUUid());
						ba.setNationcode(objects.get(j).get(0).toString());
						ba.setNationname(objects.get(j).get(1).toString());
						ba.setNationdesc(objects.get(j).get(2).toString());
						list.add(ba);
					}
					/*for(List<Object> ol : objects) {
						BaseArea ba = new BaseArea();
						ba.setId(Stringutil.getUUid());
						ba.setAreacode(ol.get(0).toString());
						ba.setAreaname(ol.get(1).toString());
						ba.setAreadesc(ol.get(2).toString());
						list.add(ba);
					}*/
					bns.daoRuFuGai(list);
					json.put("msg","导入成功");
				}
				customSqlUtil.saveLog("民族代码->"+json.get("msg"),"导入");
				out.write(json.toString());
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if (out != null) {
						out.flush();
						out.close();
					}
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}
	
	//导出地区
	@RequestMapping(value = "/Xdaochudiqudaima", method = RequestMethod.GET)
	public void Xdaochudiqudaima(HttpServletRequest request, HttpServletResponse response) {
		//设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		String query = "select areacode,areaname,areadesc from basearea where 1=1";
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("areacode"));
			dataA.add(smo.get("areaname"));
			dataA.add(smo.get("areadesc"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("行政区划代码");
		columnNames.add("地区名称");
		columnNames.add("地区描述");
		try {
			String fileName = "行政区划代码 .xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//导出国家
	@RequestMapping(value = "/Xdaochuguojiadaima", method = RequestMethod.GET)
	public void Xdaochuguojiadaima(HttpServletRequest request, HttpServletResponse response) {
		//设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		String query = "select countrycode,countryname,countrydesc from basecountry where 1=1";
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("countrycode"));
			dataA.add(smo.get("countryname"));
			dataA.add(smo.get("countrydesc"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("国家地区代码");
		columnNames.add("国家名称");
		columnNames.add("国家描述");
		try {
			String fileName = "国家地区代码 .xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//导出行业分类
	@RequestMapping(value = "/Xdaochuhangyedalei", method = RequestMethod.GET)
	public void Xdaochuhangyedalei(HttpServletRequest request, HttpServletResponse response) {
		//设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		String query = "select aindustrycode,aindustryname,aindustrydesc from baseaindustry where 1=1";
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("aindustrycode"));
			dataA.add(smo.get("aindustryname"));
			dataA.add(smo.get("aindustrydesc"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("一级行业代码");
		columnNames.add("一级行业名称");
		columnNames.add("一级行业描述");
		try {
			String fileName = "一级行业代码 .xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	
	//导出行业分类
	@RequestMapping(value = "/Xdaochuhangyefenlei", method = RequestMethod.GET)
	public void Xdaochuhangyefenlei(HttpServletRequest request, HttpServletResponse response) {
		//设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		String query = "select bindustrycode,bindustryname,bindustrydesc from basebindustry where 1=1";
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("bindustrycode"));
			dataA.add(smo.get("bindustryname"));
			dataA.add(smo.get("bindustrydesc"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("二级行业代码");
		columnNames.add("二级行业名称");
		columnNames.add("二级行业描述");
		try {
			String fileName = "二级行业代码 .xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}		
		
	//导出数据币种
	@RequestMapping(value = "/Xdaochushujubizhong", method = RequestMethod.GET)
	public void Xdaochushujubizhong(HttpServletRequest request, HttpServletResponse response) {
		//设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		String query = "select currencycode,currencyname,currencydesc from basecurrency where 1=1";
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("currencycode"));
			dataA.add(smo.get("currencyname"));
			dataA.add(smo.get("currencydesc"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("币种代码");
		columnNames.add("币种名称");
		columnNames.add("币种描述");
		try {
			String fileName = "币种代码 .xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}		
	
	//导出国籍
		@RequestMapping(value = "/Xdaochuguojidaima", method = RequestMethod.GET)
		public void Xdaochuguojidaima(HttpServletRequest request, HttpServletResponse response) {
			//设置编码
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			response.setContentType("octets/stream");
			String query = "select countrytwocode,countrytwoname,countrytwodesc from basecountrytwo where 1=1";
			List<Map<String, Object>> listM = jt.queryForList(query);
			List<List<Object>> objects = new LinkedList<>();
			for(Map<String, Object> smo : listM) {
				List<Object> dataA = new LinkedList<>();
				dataA.add(smo.get("countrytwocode"));
				dataA.add(smo.get("countrytwoname"));
				dataA.add(smo.get("countrytwodesc"));
				objects.add(dataA);
			}
			List<String> columnNames = new LinkedList<>();
			columnNames.add("国籍代码");
			columnNames.add("国籍名称");
			columnNames.add("国籍描述");
			try {
				String fileName = "国籍代码 .xlsx";
				response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
				Excel2007Utils.writeExcelResponse(response,columnNames,objects);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//导出学历
		@RequestMapping(value = "/Xdaochuxuelidaima", method = RequestMethod.GET)
		public void Xdaochuxuelidaima(HttpServletRequest request, HttpServletResponse response) {
			//设置编码
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			response.setContentType("octets/stream");
			String query = "select educationcode,educationname,educationdesc from baseeducation where 1=1";
			List<Map<String, Object>> listM = jt.queryForList(query);
			List<List<Object>> objects = new LinkedList<>();
			for(Map<String, Object> smo : listM) {
				List<Object> dataA = new LinkedList<>();
				dataA.add(smo.get("educationcode"));
				dataA.add(smo.get("educationname"));
				dataA.add(smo.get("educationdesc"));
				objects.add(dataA);
			}
			List<String> columnNames = new LinkedList<>();
			columnNames.add("学历代码");
			columnNames.add("学历名称");
			columnNames.add("学历描述");
			try {
				String fileName = "学历代码 .xlsx";
				response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
				Excel2007Utils.writeExcelResponse(response,columnNames,objects);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//导出民族
		@RequestMapping(value = "/Xdaochuminzudaima", method = RequestMethod.GET)
		public void Xdaochuminzudaima(HttpServletRequest request, HttpServletResponse response) {
			//设置编码
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			response.setContentType("octets/stream");
			String query = "select nationcode,nationname,nationdesc from basenation where 1=1";
			List<Map<String, Object>> listM = jt.queryForList(query);
			List<List<Object>> objects = new LinkedList<>();
			for(Map<String, Object> smo : listM) {
				List<Object> dataA = new LinkedList<>();
				dataA.add(smo.get("nationcode"));
				dataA.add(smo.get("nationname"));
				dataA.add(smo.get("nationdesc"));
				objects.add(dataA);
			}
			List<String> columnNames = new LinkedList<>();
			columnNames.add("民族代码");
			columnNames.add("民族名称");
			columnNames.add("民族描述");
			try {
				String fileName = "民族代码 .xlsx";
				response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
				Excel2007Utils.writeExcelResponse(response,columnNames,objects);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	/**
	 * 根据传入的基础代码返回相应的基础代码数据
	 * @param code 要获取的基础代码
	 * @return 基础代码数据
	 */
	public List<?> huoQuJiChuShuJu(TableCodeEnum code){
		if(TableCodeEnum.YJHYDM.equals(code)) {
			return yjhy.findAll();
		}else if(TableCodeEnum.XZQHDM.equals(code)) {
			return xzqh.findAll();
		}else if(TableCodeEnum.EJHYDM.equals(code)) {
			return ejhy.findAll();
		}else if(TableCodeEnum.GJDM.equals(code)) {
			return gjdm.findAll();
		}else if(TableCodeEnum.BZDM.equals(code)) {
			return bzdm.findAll();
		}else {
			return new ArrayList<>();
		}
	}
}
