package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;

import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XgrcldkxxCommonExcel;

import com.geping.etl.UNITLOAN.entity.report.Xclgrdkxx;
import com.geping.etl.UNITLOAN.entity.report.Xftykhxbl;
import com.geping.etl.UNITLOAN.service.report.*;
import com.geping.etl.UNITLOAN.util.*;
import com.geping.etl.UNITLOAN.util.check.CheckAllData;
import com.geping.etl.UNITLOAN.util.check.CheckAllData3;
import com.geping.etl.UNITLOAN.util.check.CheckAllDataGr;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;

import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @Author: chenggen
 * @Date: 17:03 2020/11/4
 */
@RestController
public class XCLGRDKXXController {

    @Autowired
    private XclgrdkxxService xclgrdkxxService;

    @Autowired
    private XCheckRuleService checkRuleService;

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private ExcelUploadUtil excelUploadUtil;

    @Autowired
    private XcommonService commonService;


    @Autowired
    private CustomSqlUtil customSqlUtil;


    @Autowired
    private HttpServletRequest request;

    @Autowired
    private XDataBaseTypeUtil dataBaseTypeUtil;
    @Autowired
    private DepartUtil departUtil;


    private Sys_UserAndOrgDepartment sys_user;

    private final String fileName="存量个人贷款基础数据信息";

    private String target="";

    private final String tableName="xclgrdkxx";
    //查询数据
    @PostMapping("XGetXclgrdkxxData")
    public ResponseResult XGetXcldkxxData(int page, int rows,String financeorgcodeParam,String contractcodeParam,String receiptcodeParam,
        String brroweridnumParam,String isfarmerloanParam,String checkstatusParam,String operationnameParam,String datastatus){
        page = page - 1;
        sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");

        String departId = departUtil.getDepart(sys_user);
        Specification specification=new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("datastatus"), datastatus));

                predicates.add(criteriaBuilder.like(root.get("departid"), departId));
                if (StringUtils.isNotBlank(financeorgcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("financeorgcode"), "%"+financeorgcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(contractcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("contractcode"), "%"+contractcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(receiptcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("receiptcode"), "%"+receiptcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(brroweridnumParam)){
                    predicates.add(criteriaBuilder.like(root.get("brroweridnum"), "%"+brroweridnumParam+"%"));
                }
                if (StringUtils.isNotBlank(isfarmerloanParam)){
                    predicates.add(criteriaBuilder.like(root.get("isfarmerloan"), "%"+isfarmerloanParam+"%"));
                }
                if (StringUtils.isNotBlank(checkstatusParam)){
                    predicates.add(criteriaBuilder.equal(root.get("checkstatus"), checkstatusParam));
                }
                if (StringUtils.isNotBlank(operationnameParam)){
                    predicates.add(criteriaBuilder.equal(root.get("operationname"), operationnameParam));
                }
                predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
        PageRequest pageRequest = new PageRequest(page, rows, sort);
        Page<Xclgrdkxx> all = xclgrdkxxService.findAll(specification, pageRequest);
        List<Xclgrdkxx> content = all.getContent();
        long totalCount = all.getTotalElements();
        return ResponseResult.success(totalCount,content);
    }

    //导入
    @PostMapping(value = "XimmportExcelFour",produces = "text/plain;charset=UTF-8")
    public void XimmportExcel(HttpServletRequest request,HttpServletResponse response){
        sys_user=((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        File uploadFile = null;
        OPCPackage opcPackage=null;
        PrintWriter out = null;
        try {
            out = response.getWriter();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multiRequest.getFile("excelfile");
            JSONObject json = new JSONObject();
            if (file == null) {
                json.put("msg", "导入文件为空");
            } else {
                uploadFile = excelUploadUtil.uploadFile(file);
                opcPackage = OPCPackage.open(uploadFile);
                String[] headers = {"金融机构代码", "内部机构号", "金融机构地区代码","借款人证件类型", "借款人证件代码",
                         "借款人地区代码",  "贷款借据编码", "贷款合同编码", "贷款产品类别",
                        "贷款发放日期", "贷款到期日期", "贷款展期到期日期", "币种", "贷款余额", "贷款余额折人民币",
                        "利率是否固定", "利率水平", "贷款定价基准类型", "基准利率", "贷款财政扶持方式", "贷款利率重新定价日",
                        "贷款担保方式","是否首次贷款", "贷款质量", "贷款状态", "逾期类型",  "贷款用途","数据日期"
                };
                String[] values = {"financeorgcode", "financeorginnum", "financeorgareacode", "isfarmerloan",
                        "brroweridnum", "brrowerareacode", "receiptcode", "contractcode", "productcetegory",
                        "loanstartdate", "loanenddate", "extensiondate", "currency", "receiptbalance",
                        "receiptcnybalance", "interestisfixed", "interestislevel", "fixpricetype", "baseinterest", "loanfinancesupport",
                        "loaninterestrepricedate", "guaranteemethod", "isplatformloan", "loanquality", "loanstatus","overduetype", "issupportliveloan","sjrq"
                };
                String[] amountFields = {"receiptbalance", "receiptcnybalance"};
                String[] dateFields = {"loanstartdate", "loanenddate", "extensiondate", "loaninterestrepricedate","sjrq"};

                String[] rateFields={"baseinterest","interestislevel"};
//                List<Xftykhxbl> xftykhxblList = ftykhxblService.findAll();
//                if (xftykhxblList.size()>0){
//                    aBoolean = knowledgeIsRepeat(xftykhxblList);
//                }
                int id = commonService.getMaxId("xclgrdkxxh");
                XgrcldkxxCommonExcel commonExcel = new XgrcldkxxCommonExcel(sys_user, opcPackage, customSqlUtil, new Xclgrdkxx(), headers, values, amountFields,dateFields, null,rateFields,id,xclgrdkxxService);
                commonExcel.process(0);
                String departId = departUtil.getDepart(sys_user);
                if (commonExcel.msg.toString().length() == 0) {
                	commonService.importDelete(tableName,departId);
                    commonExcel.process(1);
                    json.put("msg","导入成功");
                } else {
                    json.put("msg",commonExcel.msg.toString());
                }
//                if (aBoolean && json.getString("msg").equals("导入成功")){
//                    json.put("msg","补录表中有相同客户号码的数据");
//                }
            }
            String logContext="导入成功".equals(json.getString("msg")) || "补录表中有相同客户号码的数据".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
            customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
            out.write(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                opcPackage.close();
                //将上传的文件删除
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    //校验
    @PostMapping("XCheckDataFour")
    public void XCheckDataOne(HttpServletRequest request, HttpServletResponse response,String financeorgcodeParam,String contractcodeParam,String checkstatusParam){
        //记录是否有错
        long a = System.currentTimeMillis();
        boolean sqlServer = dataBaseTypeUtil.equalsSqlServer();
        boolean b=false;
        boolean isFirst = true;
        sys_user = ((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        List<SUpOrgInfoSet> sUpOrgInfoSets = suisService.findAll();
        target=sUpOrgInfoSets.get(0).getMessagepath();
        String departId = departUtil.getDepart(sys_user);

        String checklimit = sUpOrgInfoSets.get(0).getChecklimit();
        int checklimitNum = 0;
        Map<String,String> indexMap = null;//索引map
        try {
            new BigDecimal(checklimit);
            checklimitNum = Integer.valueOf(checklimit).intValue();
        }catch(Exception e) {

        }
        int count = 0;
        StringBuffer limitBuffer  = new StringBuffer("");
        Map<String,String> limitMap = new HashMap<String,String>();

        JSONObject json = new JSONObject();
        String id = request.getParameter("id");
        System.out.println(id);

        LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
        LinkedHashMap<String, String> errorMsg2 = new LinkedHashMap<String, String>();
        List<Integer> errorId0 = new ArrayList<>();//表间校验的错误id
        List<String> errorId = new ArrayList<>();
        List<String> rightId = new ArrayList<>();
        Map<String,String> whereMap=new HashMap<>();
        Map<String,String> setMap=new HashMap<>();
        setMap.put("operator",sys_user.getLoginid());
        setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

//        List<String> splitList = Arrays.asList(id.split(","));//返回固定长度的ArrayList
        String whereStr=" where datastatus='0' and checkstatus='0'";
        String whereStra=" where a.datastatus='0' and a.checkstatus='0' ";

        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            System.out.println(id);
            whereStr = whereStr + " and id in(" + id + ")";
            whereStra =whereStra+" and a.id in(" + id + ")";
        }else{
            if (StringUtils.isNotBlank(financeorgcodeParam)){
                whereStr = whereStr + " and financeorgcode like '%"+ financeorgcodeParam +"%'";
                whereStra= whereStra+" and a.financeorgcode like '%"+ financeorgcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(contractcodeParam)){
                whereStr = whereStr + " and contractcode like '%"+ contractcodeParam +"%'";
                whereStra=whereStra +" and a.contractcode like '%"+ contractcodeParam +"%'";
            }
            whereStr = whereStr + " and departid like '%"+ departId +"%'";
            whereStra = whereStra + " and a.departid like '%"+ departId +"%'";
        }

        String sql="select * from xclgrdkxx "+whereStr;
        String sqlcount="select count(*) from "+tableName+" "+whereStr;
        Connection connection = null;
        ResultSet resultSet=null;
        ResultSet rsCount=null;
        PreparedStatement ps = null;
        int total = 0;
        int size = 100000;
        int page = 0;
        String[] errorcode = {"贷款合同编码","贷款借据编号"};
        try {
            connection = JDBCUtils.getConnection();
            rsCount = JDBCUtils.Query(connection, ps, sqlcount);

            if (rsCount.next()) {	//没有可校验的数据
                total = rsCount.getInt(1);
            }
            if(total == 0) {
                json.put("msg","-1");
                try {
                    PrintWriter out = response.getWriter();
                    out.write(json.toString());
                    out.flush();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }
                page = total % size == 0 ? total/size : total/size+1;

                long aaa = System.currentTimeMillis();
                //校验规则list
                List<String> xcldkxx1List = checkRuleService.getChecknum("xclgrdkxx");

                String sqlexist;
                String errorinfo;


/*
            if(dataBaseTypeUtil.equalsMySql()) {
                //需要加索引的字段
                Map<String,String> fieldMap = new HashMap<String,String>();
                //存量个人贷款
                fieldMap.put("sjrq", "xclgrdkxx");
                fieldMap.put("financeorgcode", "xclgrdkxx");
                fieldMap.put("receiptcode", "xclgrdkxx");
                fieldMap.put("contractcode", "xclgrdkxx");
                fieldMap.put("brroweridnum", "xclgrdkxx");

                //存量个人贷款历史
                fieldMap.put("sjrq#", "xclgrdkxxh");
                fieldMap.put("financeorgcode#", "xclgrdkxxh");
                fieldMap.put("receiptcode#", "xclgrdkxxh");


                //担保物信息
                fieldMap.put("sjrq##", "xdkdbwx");
                fieldMap.put("loancontractcode", "xdkdbwx");
                //担保物信息历史
                fieldMap.put("sjrq###", "xdkdbwxh");
                fieldMap.put("loancontractcode#", "xdkdbwxh");


                //个人客户信息
                fieldMap.put("sjrq####", "xgrkhxx");
                fieldMap.put("customercode", "xgrkhxx");
                //个人客户信息历史
                fieldMap.put("sjrq#####", "xgrkhxxh");
                fieldMap.put("customercode#", "xgrkhxxh");


                //贷款担保合同
                fieldMap.put("sjrq######", "xdkdbht");
                //贷款担保合同l历史
                fieldMap.put("sjrq#######", "xdkdbhth");

                //担保物信息
                fieldMap.put("sjrq########", "xdkdbwx");
                //担保物信息历史
                fieldMap.put("sjrq#########", "xdkdbwxh");

                indexMap = addIndex(connection, fieldMap);

                long bindex = System.currentTimeMillis();
                System.out.println("创建索引耗时："+(bindex -a) +"毫秒");
            }
*/



            if(checklimitNum == 0) {
                if (xcldkxx1List.contains("JS2404")){
                    if (sqlServer){
                        errorinfo = "数据日期+金融机构代码+贷款借据编码不唯一";
                        sqlexist = "select id,contractcode,receiptcode from xclgrdkxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,financeorgcode,receiptcode from xclgrdkxx where operationname != '申请删除' group by sjrq,financeorgcode,receiptcode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.receiptcode = t2.receiptcode)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);

                        sqlexist = "select id,contractcode,receiptcode from xclgrdkxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,financeorgcode,receiptcode from xclgrdkxxh group by sjrq,financeorgcode,receiptcode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.receiptcode = t2.receiptcode)";
                        errorinfo = "数据日期+金融机构代码+贷款借据编码不唯一（历史表）";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                    }else {
                        errorinfo = "数据日期+金融机构代码+贷款借据编码不唯一";
                        sqlexist = "select id,contractcode,receiptcode from xclgrdkxx "+whereStr+" and (sjrq,financeorgcode,receiptcode) in (select sjrq,financeorgcode,receiptcode from xcldkxx where operationname != '申请删除' group by sjrq,financeorgcode,receiptcode HAVING COUNT(1)>1)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);

                        sqlexist = "select id,contractcode,receiptcode from xclgrdkxxh "+whereStr+" and (sjrq,financeorgcode,receiptcode) in (select sjrq,financeorgcode,receiptcode from xcldkxxh group by sjrq,financeorgcode,receiptcode HAVING COUNT(1)>0)";
                        errorinfo = "数据日期+金融机构代码+贷款借据编码不唯一（历史表）";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                    }
                }




                if (xcldkxx1List.contains("JS2194")){
                    errorinfo="当担保物信息中担保物类别包含C时，则贷款担保方式应该为B01-房地产抵押";
                    sqlexist="select a.id,a.contractcode,a.receiptcode  from xclgrdkxx a  inner join  xdkdbwx b on a.sjrq = b.sjrq and a.contractcode=b.loancontractcode  " +whereStra+ " and a.guaranteemethod !='B01' and b.gteegoodscategory like 'C%' and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);

                    errorinfo="当担保物信息中担保物类别包含C时，则贷款担保方式应该为B01-房地产抵押（历史表）";
                    sqlexist="select a.id,a.contractcode,a.receiptcode  from xclgrdkxx a   inner join  xdkdbwxh b on a.sjrq = b.sjrq and a.contractcode=b.loancontractcode " +whereStra+ " and a.guaranteemethod !='B01' and b.gteegoodscategory like 'C%'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                }


                if (xcldkxx1List.contains("JS2168")){
                    errorinfo = "个人客户的借款人证件代码应该在个人客户基础信息.客户证件代码中存在";
                    sqlexist="select a.id,a.contractcode,a.receiptcode  from xclgrdkxx a" +whereStra+ "and (NOT EXISTS (SELECT 1 FROM xgrkhxx b WHERE a.sjrq=b.sjrq AND a.brroweridnum =b.customercode  AND b.operationname != '申请删除') and NOT EXISTS (SELECT 1 FROM xgrkhxxh b WHERE a.sjrq=b.sjrq AND a.brroweridnum =b.customercode))";
                inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            }

                if (xcldkxx1List.contains("JS2170")){
                    errorinfo = "存量个人贷款信息的贷款担保方式不是信用/免担保贷款的，贷款合同编码应该在担保合同信息.被担保合同编码中存在";
                    sqlexist="select a.id,a.contractcode,a.receiptcode  from xclgrdkxx a" + whereStra + "and a.guaranteemethod!='D' AND (a.contractcode not in(SELECT b.loancontractcode FROM xdkdbht b INNER JOIN xclgrdkxx c on c.sjrq = b.sjrq  where b.operationname != '申请删除')AND a.contractcode not in(SELECT e.loancontractcode FROM xdkdbhth e INNER JOIN xclgrdkxx f on f.sjrq = e.sjrq))";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                }

                if (xcldkxx1List.contains("JS2171")){
                    errorinfo = "存量个人贷款信息的贷款担保方式包含抵押、质押等情况的，贷款合同编码应该在担保物信息.被担保合同编码中存在";
                    sqlexist="SELECT a.id,a.contractcode,a.receiptcode  FROM xclgrdkxx a" +whereStra+ "and (a.guaranteemethod LIKE 'B%' OR a.guaranteemethod = 'A') and(a.contractcode not in (SELECT b.loancontractcode FROM xdkdbwx b INNER JOIN xclgrdkxx c on c.sjrq = b.sjrq  where  b.operationname != '申请删除') AND a.contractcode not in (SELECT e.loancontractcode FROM xdkdbwxh e INNER JOIN xclgrdkxx f on f.sjrq = e.sjrq))";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                }
                errorId.clear();
            }else {
                if (xcldkxx1List.contains("JS2404")){
                    if (sqlServer){
                        errorinfo = "数据日期+金融机构代码+贷款借据编码不唯一";
                        sqlexist = "select id,contractcode,receiptcode from xclgrdkxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,financeorgcode,receiptcode from xclgrdkxx where operationname != '申请删除' group by sjrq,financeorgcode,receiptcode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.receiptcode = t2.receiptcode)";
                        executeCheckSqlLimit("数据日期+金融机构代码+贷款借据编码不唯一",limitMap, checklimitNum, errorId0, sqlexist);

                        sqlexist = "select id,contractcode,receiptcode from xclgrdkxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,financeorgcode,receiptcode from xclgrdkxxh group by sjrq,financeorgcode,receiptcode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.receiptcode = t2.receiptcode)";
                        errorinfo = "数据日期+金融机构代码+贷款借据编码不唯一（历史表）";
                        executeCheckSqlLimit("数据日期+金融机构代码+贷款借据编码不唯一（历史表）",limitMap, checklimitNum, errorId0, sqlexist);
                    }else {
                        errorinfo = "数据日期+金融机构代码+贷款借据编码不唯一";
                        sqlexist = "select id,contractcode,receiptcode from xclgrdkxx "+whereStr+" and (sjrq,financeorgcode,receiptcode) in (select sjrq,financeorgcode,receiptcode from xcldkxx where operationname != '申请删除' group by sjrq,financeorgcode,receiptcode HAVING COUNT(1)>1)";
                        executeCheckSqlLimit("数据日期+金融机构代码+贷款借据编码不唯一", limitMap, checklimitNum, errorId0, sqlexist);

                        sqlexist = "select id,contractcode,receiptcode from xclgrdkxxh "+whereStr+" and (sjrq,financeorgcode,receiptcode) in (select sjrq,financeorgcode,receiptcode from xcldkxxh group by sjrq,financeorgcode,receiptcode HAVING COUNT(1)>0)";
                        errorinfo = "数据日期+金融机构代码+贷款借据编码不唯一（历史表）";
                        executeCheckSqlLimit("数据日期+金融机构代码+贷款借据编码不唯一（历史表）", limitMap, checklimitNum, errorId0, sqlexist);
                    }
                }




                if (xcldkxx1List.contains("JS2194")){
                    errorinfo="当担保物信息中担保物类别包含C时，则贷款担保方式应该为B01-房地产抵押";
                    sqlexist="select a.id,a.contractcode,a.receiptcode  from xclgrdkxx a  inner join  xdkdbwx b on a.sjrq = b.sjrq and a.contractcode=b.loancontractcode  " +whereStra+ " and a.guaranteemethod !='B01' and b.gteegoodscategory like 'C%' and b.operationname != '申请删除'";
                    executeCheckSqlLimit("当担保物信息中担保物类别包含C时，则贷款担保方式应该为B01-房地产抵押",limitMap, checklimitNum, errorId0, sqlexist);

                    errorinfo="当担保物信息中担保物类别包含C时，则贷款担保方式应该为B01-房地产抵押（历史表）";
                    sqlexist="select a.id,a.contractcode,a.receiptcode  from xclgrdkxx a  inner join  xdkdbwxh b on a.sjrq = b.sjrq and a.contractcode=b.loancontractcode  " +whereStra+ " and a.guaranteemethod !='B01' and b.gteegoodscategory like 'C%'";
                    executeCheckSqlLimit("当担保物信息中担保物类别包含C时，则贷款担保方式应该为B01-房地产抵押（历史表）",limitMap, checklimitNum, errorId0, sqlexist);
                }


                if (xcldkxx1List.contains("JS2168")){
                    errorinfo = "个人客户的借款人证件代码应该在个人客户基础信息.客户证件代码中存在";
                    sqlexist="select a.id,a.contractcode,a.receiptcode  from xclgrdkxx a" +whereStra+ "and (NOT EXISTS (SELECT 1 FROM xgrkhxx b WHERE a.sjrq=b.sjrq AND a.brroweridnum =b.customercode  AND b.operationname != '申请删除') and NOT EXISTS (SELECT 1 FROM xgrkhxxh b WHERE a.sjrq=b.sjrq AND a.brroweridnum =b.customercode))";
                    executeCheckSqlLimit("个人客户的借款人证件代码应该在个人客户基础信息.客户证件代码中存在",limitMap, checklimitNum, errorId0, sqlexist);
                }

                if (xcldkxx1List.contains("JS2170")){
                    errorinfo = "存量个人贷款信息的贷款担保方式不是信用/免担保贷款的，贷款合同编码应该在担保合同信息.被担保合同编码中存在";
                    sqlexist="select a.id,a.contractcode,a.receiptcode  from xclgrdkxx a" + whereStra + "and a.guaranteemethod!='D' AND (a.contractcode not in(SELECT b.loancontractcode FROM xdkdbht b INNER JOIN xclgrdkxx c on c.sjrq = b.sjrq  where b.operationname != '申请删除')AND a.contractcode not in(SELECT e.loancontractcode FROM xdkdbhth e INNER JOIN xclgrdkxx f on f.sjrq = e.sjrq))";
                    executeCheckSqlLimit("存量个人贷款信息的贷款担保方式不是信用/免担保贷款的，贷款合同编码应该在担保合同信息.被担保合同编码中存在", limitMap, checklimitNum, errorId0, sqlexist);
                }

                if (xcldkxx1List.contains("JS2171")){
                    errorinfo = "存量个人贷款信息的贷款担保方式包含抵押、质押等情况的，贷款合同编码应该在担保物信息.被担保合同编码中存在";
                    sqlexist="SELECT a.id,a.contractcode,a.receiptcode  FROM xclgrdkxx a" +whereStra+ "and (a.guaranteemethod LIKE 'B%' OR a.guaranteemethod = 'A') and(a.contractcode not in (SELECT b.loancontractcode FROM xdkdbwx b INNER JOIN xclgrdkxx c on c.sjrq = b.sjrq  where  b.operationname != '申请删除') AND a.contractcode not in (SELECT e.loancontractcode FROM xdkdbwxh e INNER JOIN xclgrdkxx f on f.sjrq = e.sjrq))";
                    executeCheckSqlLimit("存量个人贷款信息的贷款担保方式包含抵押、质押等情况的，贷款合同编码应该在担保物信息.被担保合同编码中存在", limitMap, checklimitNum, errorId0, sqlexist);
                }
            }



//                errorinfo="金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致";
//                sqlexist ="select a.id,a.contractcode,a.receiptcode from xcldkxx a  "+whereStra+" and a.operationname!='申请删除' and (a.financeorgcode <> (select b.finorgcode from xjrjgfz b INNER JOIN xdkdbwx c on b.sjrq = c.sjrq and b.inorgnum = c.financeorginnum where b.operationname != '申请删除') or a.financeorgcode <>(select e.finorgcode from xjrjgfzh e INNER JOIN xdkdbwx f on e.sjrq = f.sjrq and e.inorgnum = f.financeorginnum))";
//                inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);


                long bbb = System.currentTimeMillis();
                System.out.println("表间校验用时："+(bbb-aaa)/1000+"秒");
            outer:for(int i = 0; i < page; i++) {
                if(i != 0) {
                    isFirst = false;
                }
                long axun = System.currentTimeMillis();
                resultSet = JDBCUtils.Query(sql,0, size);

                Xclgrdkxx xclgrdkxx=null;
                if(checklimitNum == 0) {
                    while (true) {
                        if (!resultSet.next()) break;
                        xclgrdkxx = new Xclgrdkxx();
                        Stringutil.getEntity(resultSet, xclgrdkxx);
                        CheckAllData.checkXclgrdkxx(customSqlUtil,xcldkxx1List,xclgrdkxx, errorMsg, errorMsg2,errorId, rightId);
                    }
                }else {
                    while (true) {
                        if (!resultSet.next()) break;
                        xclgrdkxx = new Xclgrdkxx();
                        Stringutil.getEntity(resultSet, xclgrdkxx);
                        CheckAllDataGr.checkXclgrdkxxLimit(customSqlUtil, xclgrdkxx, errorId0, errorId, rightId, xcldkxx1List, limitMap, checklimitNum);
                    }
                }
                resultSet.close();

                //修改状态 和 下载
                    if (errorId!=null && errorId.size()>0){
                        b = true;
                        if(checklimitNum == 0) {
                        //-下载到本地
                        StringBuffer errorMsgAll = new StringBuffer("");
                        for(Map.Entry<String, String> entry : errorMsg2.entrySet()) {
                            errorMsgAll.append(entry.getValue()+"\r\n");
                        }
                        DownloadUtil.downLoad2(errorMsgAll.toString(),target+File.separator+"checkout",fileName+".txt",isFirst);
                        errorMsg2.clear();
                        errorMsgAll.setLength(0);
                        errorMsgAll= null;
                        }
                        errorMsg2.clear();
                        setMap.put("checkStatus","2");
                        setMap.put("operationname"," ");

                        customSqlUtil.updateByWhereBatchGR(tableName, setMap, errorId);
                        errorId.clear();
                    }

                    if (rightId!=null && rightId.size()>0){
                        setMap.put("checkStatus","1");
                        setMap.put("operationname"," ");
                        customSqlUtil.updateByWhereBatchGR(tableName, setMap, rightId);
                        rightId.clear();
                    }
                    long bxun = System.currentTimeMillis();
                    System.out.println("第"+(i+1)+"批10w数据用时"+(bxun-axun)/1000+"秒");
            }


        } catch (Exception throwables) {
            throwables.printStackTrace();
        }finally {
            if(indexMap != null && indexMap.size() != 0) {
                dropIndex(connection, indexMap);
            }
            if(rsCount != null){
                try {
                    rsCount.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (resultSet!=null){
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(CheckAllData.fzList != null) {
                CheckAllData.fzList.clear();
                CheckAllData.fzList = null;
            }
            if(CheckAllData.frList != null) {
                CheckAllData.frList.clear();
                CheckAllData.frList = null;
            }
        }

        //校验结束
            if (b){
                json.put("msg","0");
            }else {
                json.put("msg","1");
            }
        if(checklimitNum != 0) {
            if(CheckAllDataGr.limitFlag) {
                //limitBuffer.append("阈值量:"+checklimitNum+"\r\n");
                limitBuffer.append("提示错误种类数量:"+limitMap.size()+"\r\n");
                limitBuffer.append("因错误数据较多,目前只显示错误示例数据；\r\n");
                limitBuffer.append("\r\n");
            }
            for(Map.Entry<String, String> entry: limitMap.entrySet()) {
                limitBuffer.append(entry.getKey()+"："+errorcode[0]+"+"+errorcode[1]+"->\r\n");
                limitBuffer.append(entry.getValue().substring(entry.getValue().lastIndexOf("#")+1));
            }
        }
        if(limitBuffer.length() != 0) {
            //-下载到本地
            DownloadUtil.downLoad2(limitBuffer.toString(),target+File.separator+"checkout",fileName+".txt",isFirst);
        }
        limitBuffer.setLength(0);
//        if (rightId!=null && rightId.size()>0){
//            setMap.put("checkStatus","1");
//            setMap.put("operationname"," ");
//            customSqlUtil.updateByWhereBatchGR(tableName, setMap, rightId);
//            rightId.clear();
//        }

        customSqlUtil.saveLog("存量个人贷款基础数据校验成功","校验");
        long b1 = System.currentTimeMillis();
        System.out.println("校验用时："+(b1-a)/1000+"秒");
        try {
            PrintWriter out = response.getWriter();
            out.write(json.toString());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            CheckAllDataGr.limitFlag = false;
        }
    }

    @GetMapping("XDownLoadCheckFour")
    public void XDownLoadCheckOne(HttpServletResponse response) {
        if (StringUtils.isBlank(target)){
            target=suisService.findAll().get(0).getMessagepath();
        }
        DownloadUtil.downLoadCheckFile(response,target + File.separator + "checkout",fileName+".txt");
    }


    // 表间校验
    public void inTableCalibration(Map<String,String> setMap,List<String> errorId,Map<String,String> whereMap,LinkedHashMap<String, String> errorMsg,String sql,String[] errorcode,String errorinfo) {
        List<Object[]> exist = customSqlUtil.executeQuery(sql);
        if (exist!=null && exist.size()>0){
            setMap.put("checkStatus","2");
            setMap.put("operationname"," ");
            exist.forEach(errId->{
                errorId.add(String.valueOf(errId[0]));
                whereMap.put("id", String.valueOf(errId[0]));
                if(!errorMsg.containsKey(String.valueOf(errId[0]))) {
                    errorMsg.put(String.valueOf(errId[0]), errorcode[0]+":"+String.valueOf(errId[1])+"，"+errorcode[1]+":"+String.valueOf(errId[2])+"]->\r\n");
                }
                String str = errorMsg.get(String.valueOf(errId[0]));
                str = str + errorinfo+"|";
                errorMsg.put(String.valueOf(errId[0]), str);
            });
            whereMap.clear();
            setMap.clear();
            exist.clear();
        }
    }
    //lsit数据字段重复 返回true
    private Boolean knowledgeIsRepeat(List<Xftykhxbl> orderList) {
        Set<Xftykhxbl> set = new TreeSet<Xftykhxbl>(new Comparator<Xftykhxbl>() {
            public int compare(Xftykhxbl a, Xftykhxbl b) {
                // 字符串则按照asicc码升序排列
                if (StringUtils.isNotBlank(a.getCustomernum())&&StringUtils.isNotBlank(b.getCustomernum())){
                    return a.getCustomernum().compareTo(b.getCustomernum());
                }

                return 100000;
            }
        });
        set.addAll(orderList);
        if (set.size() < orderList.size()) {
            return true;
        }
        return false;
    }


    public void executeCheckSqlLimit(String rule,Map<String,String> map,int checknumLimit,List<Integer> errorId,String sql) {
        List<Object[]> exist = customSqlUtil.executeQuery(sql);
        if (exist!=null && exist.size()>0){
            exist.forEach(errId->{
                errorId.add((Integer) errId[0]);
                CheckAllDataGr.putMap(rule, String.valueOf(errId[1]),String.valueOf(errId[2]), map, checknumLimit);
            });
            exist.clear();
        }
    }


    //创建索引
    public Map<String,String> addIndex(Connection conn,Map<String,String> fieldMap) {
        Statement ps = null;
        Map<String,String> indexMap = new HashMap<String,String>();
        try {
            ps = conn.createStatement();
            for(Map.Entry<String, String> entry : fieldMap.entrySet()) {
                String index = "index_"+Stringutil.getUUid();
                String sql = "CREATE INDEX "+index+" ON "+entry.getValue()+" ("+entry.getKey().replaceAll("#", "")+")";
                ps.addBatch(sql);
                indexMap.put(index, entry.getValue());
            }
            ps.executeBatch();
        }catch(Exception e) {
            e.printStackTrace();
            System.out.println("创建索引失败...");
        }finally {
            if(ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return indexMap;
    }

    //删除索引
    public void dropIndex(Connection conn,Map<String,String> indexMap) {
        Statement ps = null;
        try {
            ps = conn.createStatement();
            for(Map.Entry<String, String> entry : indexMap.entrySet()) {
                ps.addBatch("drop index "+entry.getKey()+" on "+entry.getValue()+"");
            }
            ps.executeBatch();
        }catch(Exception e) {
            e.printStackTrace();
            System.out.println("删除索引失败...");
        }finally {
            if(ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
