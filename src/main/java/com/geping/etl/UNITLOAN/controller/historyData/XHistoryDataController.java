package com.geping.etl.UNITLOAN.controller.historyData;

import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.BaseDataController;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.JDBCUtils;
import com.geping.etl.common.entity.Report_Info;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class XHistoryDataController  extends BaseDataController {

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private XcommonService commonService;
    
    @Autowired
    private DepartUtil departUtil;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    //跳转到历史数据查询页面
    @GetMapping(value = "/XHistoryDataUi")
    public ModelAndView XHistoryDataUi(HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        ModelAndView model = new ModelAndView();
        model.setViewName("unitloan/historydata/XHistoryData");
        return model;
    }

    //显示历史数据
    @PostMapping(value = "XHistoryView")
    public Map<String,Object> XHistoryDataView(HttpServletRequest request){
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User"));
        String departId = departUtil.getDepart(sys_user);
        String operationtime = request.getParameter("operationtime");
        request.getSession().setAttribute("operationtimeParam",operationtime);
        if(StringUtils.isNotBlank(operationtime)) {
        	operationtime = " and sjrq like '"+operationtime+"%'";
        }else {
        	operationtime = "";
        }
        List<Report_Info> historyInfo = commonService.getReportHistoryInfo(sys_user.getOrgid(), departId,operationtime);
        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put("rows",historyInfo);
        objectMap.put("total",historyInfo.size());
        return objectMap;
    }

    //跳转历史数据页面
    @GetMapping(value = "/XHistoryData*")
    public ModelAndView XHistoryData(HttpServletRequest request, HttpServletResponse response){
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        ModelAndView modelAndView=new ModelAndView();
        beforeRender(request,modelAndView);
/*        List<BaseArea> list = XApplicationRunnerImpl.baseAreaList;
        List<BaseCountry> countryList = XApplicationRunnerImpl.baseCountryList;
        for (BaseCountry country : countryList) {
        	BaseArea area = new BaseArea();
        	area.setAreacode(country.getCountrycode());
        	area.setAreaname(country.getCountryname());
        	list.add(area);
        }
        modelAndView.addObject("baseAreaAndCountryList",list);
        modelAndView.addObject("baseAreaList", XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseAindustryList",XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseBindustryList",XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList",XApplicationRunnerImpl.baseCurrencyList);
        modelAndView.addObject("baseCountryTwoList",XApplicationRunnerImpl.baseCountryTwoList);
        modelAndView.addObject("baseEducationList",XApplicationRunnerImpl.baseEducationList);
        modelAndView.addObject("baseNationList",XApplicationRunnerImpl.baseNationList);*/
        String uri = request.getRequestURI();
        String jsp = uri.substring(uri.lastIndexOf("/")+13);
        modelAndView.setViewName("unitloan/historydata/" + jsp);
        return modelAndView;
    }

    //显示具体历史数据
    @PostMapping(value = "/XGetHistory*")
    public void XGetHistory(HttpServletRequest request,String pages,HttpServletResponse response){
        //判断登录账号
        Sys_UserAndOrgDepartment user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
        JDBCUtils.getConnection();
        PrintWriter out = null;
        try {
            request.setCharacterEncoding("UTF-8");
            response.setContentType("text/html");
            response.setContentType("text/plain; charset=utf-8");
            String departId = departUtil.getDepart(user);
            Enumeration<String> enumeration = request.getParameterNames();
            List<String> paramList = new ArrayList<>();
            while (enumeration.hasMoreElements()){
                String key = enumeration.nextElement();
                if (key.endsWith("Param")){
                    paramList.add(key);
                }
            }
            String currentPageNumber = request.getParameter("page");
            String currentPageSize = request.getParameter("rows");
            int page = Integer.parseInt(currentPageNumber) - 1;
            int size = Integer.parseInt(currentPageSize);
            String whereSql = "orgid ='" + user.getOrgid() + "' and departid like '" + departId + "'";
            if(paramList.contains("sjrqParam")&&StringUtils.isNotBlank(request.getParameter("sjrqParam"))) {
            	paramList.remove("operationtimeParam");
            }
            for (String param : paramList) {
                String value = request.getParameter(param);
                if(param.equals("operationtimeParam")) {
                	param = "sjrqParam";
                }
                if (StringUtils.isNotBlank(value)) {
                    whereSql = whereSql + " and ";
                    whereSql =whereSql + param.substring(0,param.length() - 5) + " like '%" + value + "%'";
                }
            }
            String uri = request.getRequestURI();
            String tableName = uri.substring(uri.lastIndexOf("/") + 12);
            String sql = "select * from " + tableName + " where " + whereSql;
            String countId = "select count(id) from "+tableName + " where " + whereSql;
            ResultSet count = JDBCUtils.Query(countId);
            count.next();
            int total = count.getInt(1);

            ResultSet resultSet = JDBCUtils.Query(sql,page,size);
            Class clazz = Class.forName("com.geping.etl.UNITLOAN.entity.report."+tableName);
            Field[] fields = clazz.getDeclaredFields();

            StringBuffer rows = new StringBuffer("[");
            while (resultSet.next()){
                rows.append("{");
                for (Field field : fields) {
                    if (StringUtils.isNotBlank(resultSet.getString(field.getName()))){
                        rows.append(field.getName() + ":\"" + resultSet.getString(field.getName()) + "\",");
                    }
                }
                rows.deleteCharAt(rows.length()-1);
                rows.append("},");
            }
            rows.deleteCharAt(rows.length()-1);
            rows.append("]");
            if (rows.length()<3){
                rows = new StringBuffer();
            }
            JSONObject json = new JSONObject();
            out = response.getWriter();
            request.getSession().setAttribute("operationtimeParam","");
            json.put("rows", rows.toString());
            json.put("total", total); // 将总页数传到页面上显示
            out.write(json.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (out != null) {
                out.flush();
                out.close();
            }
            JDBCUtils.close();
        }
    }
}
