package com.geping.etl.UNITLOAN.controller.dataManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XgrkhxxCommonExcel;
import com.geping.etl.UNITLOAN.entity.report.Xgrkhxx;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XgrkhxxService;
import com.geping.etl.UNITLOAN.util.CalcCountUtil;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.DownloadUtil;
import com.geping.etl.UNITLOAN.util.ExcelUploadUtil;
import com.geping.etl.UNITLOAN.util.JDBCUtils;
import com.geping.etl.UNITLOAN.util.ResponseResult;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.UNITLOAN.util.XApplicationRunnerImpl;
import com.geping.etl.UNITLOAN.util.XDataBaseTypeUtil;
import com.geping.etl.UNITLOAN.util.check.CheckAllData3;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;

import net.sf.json.JSONObject;

/**
 * @Author: wangzd
 * @Date: 17:03 2020/6/9
 */
@RestController
public class XgrkhxxController {

    @Autowired
    private XgrkhxxService xgrkhxxService;

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private ExcelUploadUtil excelUploadUtil;

    @Autowired
    private CustomSqlUtil customSqlUtil;

    @Autowired
    private CalcCountUtil countUtil;

    @Autowired
    private HttpServletRequest request;
    
    @Autowired
    private XDataBaseTypeUtil xDataBaseTypeUtil;
    
    @Autowired
    private XCheckRuleService xCheckRuleService;
    
    @Autowired
    private XcommonService commonService;
    
    @Autowired
    private DepartUtil departUtil;

    private Sys_UserAndOrgDepartment sys_user;
    
    private final String tableName="xgrkhxx";

    private final String fileName="个人客户基础信息";

    private static String target="";
    
    //数字跳转显示数据
    @GetMapping("/XGoDataXgrkhxxUi")
    public ModelAndView XGoDataSixUi (String status){
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addObject("baseAreaList",XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseAindustryList",XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseBindustryList",XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList",XApplicationRunnerImpl.baseCurrencyList);
        modelAndView.addObject("baseCountryTwoList",XApplicationRunnerImpl.baseCountryTwoList);
        modelAndView.addObject("baseEducationList",XApplicationRunnerImpl.baseEducationList);
        modelAndView.addObject("baseNationList",XApplicationRunnerImpl.baseNationList);
        if ("dtj".equals(status)){
            modelAndView.addObject("datastatus","0");
            modelAndView.setViewName("unitloan/datamanage/grkhxx/grkhxxdtj");
        }else if ("dsh".equals(status)){
            modelAndView.addObject("datastatus","1");
            modelAndView.setViewName("unitloan/datamanage/grkhxx/grkhxxdtj");
        }else {
            modelAndView.addObject("datastatus","3");
            modelAndView.addObject("datamanege",status);
            modelAndView.setViewName("unitloan/createreport/grkhxxbw");
        }
        return modelAndView;
    }
    
    //查询数据
    @PostMapping("XGetXgrkhxxData")
    public ResponseResult XGetXgrkhxxData(int page, int rows,String finorgcodeParam,String customercodeParam,String regamtcrenyParam,String operationnameParam,String checkstatusParam,String datastatus){
        page = page - 1;
        sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
        String departId = departUtil.getDepart(sys_user);
        Specification specification=new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("datastatus"), datastatus));

                predicates.add(criteriaBuilder.like(root.get("departid"), departId));
                if (StringUtils.isNotBlank(finorgcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("finorgcode"), "%"+finorgcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(customercodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("customercode"), "%"+customercodeParam+"%"));
                }
                if (StringUtils.isNotBlank(regamtcrenyParam)){
                    predicates.add(criteriaBuilder.like(root.get("regamtcreny"), "%"+regamtcrenyParam+"%"));
                }
                if (StringUtils.isNotBlank(checkstatusParam)){
                    predicates.add(criteriaBuilder.equal(root.get("checkstatus"), checkstatusParam));
                }
                if (StringUtils.isNotBlank(operationnameParam)){
                    predicates.add(criteriaBuilder.equal(root.get("operationname"), operationnameParam));
                }
                predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        PageRequest pageRequest = new PageRequest(page, rows, sort);
        Page<Xgrkhxx> all = xgrkhxxService.findAll(specification, pageRequest);
        List<Xgrkhxx> content = all.getContent();
        long totalCount = all.getTotalElements();
        return ResponseResult.success(totalCount,content);
    }

    //导入
    @PostMapping(value = "XimmportExcelXgrkhxx",produces = "text/plain;charset=UTF-8")
    public void XimmportExcelSix(HttpServletRequest request,HttpServletResponse response){
        sys_user=((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        File uploadFile = null;
        OPCPackage opcPackage=null;
        PrintWriter out = null;
        try {
        	long a = System.currentTimeMillis();
            out = response.getWriter();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multiRequest.getFile("excelfile");
            JSONObject json = new JSONObject();
            if (file == null) {
                json.put("msg", "导入文件为空");
            } else {
                uploadFile = excelUploadUtil.uploadFile(file);
                opcPackage = OPCPackage.open(uploadFile);
                String[] headers = {"金融机构代码", "客户证件类型", "客户证件代码", "国籍",
                        "民族", "性别", "最高学历", "出生日期", "地区代码", "个人年收入", "家庭年收入", "婚姻情况",
                        "是否关联方", "授信额度", "已用额度", "个人客户身份标识", "个体工商户营业执照代码", "小微企业社会统一信用代码", "客户信用级别总等级数", "客户信用评级", "数据日期"
                };
                String[] values = {"finorgcode", "regamtcreny", "customercode", "country", "nation",
                        "sex", "education", "birthday", "regareacode", "grincome", "familyincome",
                        "marriage", "isrelation", "creditamt", "usedamt", "grkhsfbs", "gtgshyyzzdm",
                        "xwqyshtyxydm", "workareacode", "actctrltype", "sjrq"
                };
                String[] amountFields = {"grincome","familyincome","creditamt","usedamt"};
                String[] dateFields = {"birthday","sjrq"};
                int id = commonService.getMaxId("Xgrkhxxh");
                XgrkhxxCommonExcel commonExcel = new XgrkhxxCommonExcel(sys_user, opcPackage, customSqlUtil, new Xgrkhxx(), headers, values, amountFields,dateFields, null,null,id,xgrkhxxService);
                commonExcel.process(0);
                String departId = departUtil.getDepart(sys_user);
                if (commonExcel.msg.toString().length() == 0) {
                	commonService.importDelete(tableName,departId);
                    commonExcel.process(1);
                    json.put("msg","导入成功");
                    //countUtil.handleCount("xgrkhxx",0,"add",commonExcel.count);//更新数量
                } else {
                    json.put("msg",commonExcel.msg.toString());
                }
            }
            String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
            customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
            out.write(json.toString());
            long b = System.currentTimeMillis();
            System.out.println("导入用时："+(b-a)/1000+"秒");
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                opcPackage.close();
                //将上传的文件删除
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    //校验
    @PostMapping("XCheckDataXgrkhxx")
    public void XCheckDataSix(HttpServletRequest request, HttpServletResponse response,String finorgcodeParam){
        //记录是否有错
    	long a = System.currentTimeMillis();
        boolean b=false;
        boolean isFirst = true;
        sys_user = ((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));
        List<SUpOrgInfoSet> sUpOrgInfoSets = suisService.findAll();
        target=sUpOrgInfoSets.get(0).getMessagepath();
        String departId = departUtil.getDepart(sys_user);
        String checklimit = sUpOrgInfoSets.get(0).getChecklimit();
        int checklimitNum = 0;
        Map<String,String> indexMap = null;//索引map
        try {
        	new BigDecimal(checklimit);
        	checklimitNum = Integer.valueOf(checklimit).intValue();
        }catch(Exception e) {
        	
        }
        int count = 0;
        StringBuffer limitBuffer  = new StringBuffer("");
        Map<String,String> limitMap = new HashMap<String,String>();

        JSONObject json = new JSONObject();
        String id = request.getParameter("id");

        LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
        LinkedHashMap<String, String> errorMsg2 = new LinkedHashMap<String, String>();
        List<Integer> errorId0 = new ArrayList<>();//表间校验的错误id
        List<String> errorId = new ArrayList<>();
        List<String> rightId = new ArrayList<>();
        Map<String,String> whereMap=new HashMap<>();
        Map<String,String> setMap=new HashMap<>();
        setMap.put("operator",sys_user.getLoginid());
        setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//        List<String> splitList = Arrays.asList(id.split(","));//返回固定长度的ArrayList
        String whereStr=" where datastatus='0' and checkstatus='0' ";
        String whereStr2=" where a.datastatus='0' and a.checkstatus='0' ";
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            whereStr = whereStr + " and id in(" + id + ")";
            whereStr2 = whereStr2 + " and a.id in(" + id + ")";
        }else{
            if (StringUtils.isNotBlank(finorgcodeParam)){
                whereStr = whereStr + " and finorgcode like '%"+ finorgcodeParam +"%'";
                whereStr2 = whereStr2 + " and a.finorgcode like '%"+ finorgcodeParam +"%'";
            }
            whereStr = whereStr + " and departid like '%"+ departId +"%'";
            whereStr2 = whereStr2 + " and a.departid like '%"+ departId +"%'";
        }
        String sql="select * from "+tableName+" "+whereStr;
        String sqlcount="select count(*) from "+tableName+" "+whereStr;
        Connection connection = null;
        ResultSet resultSet=null;
        ResultSet rsCount=null;
        PreparedStatement ps = null;
        int total = 0;
        int size = 100000;
        int page = 0;
        //标识名称
        String[] errorcode = new String[] {"客户证件代码"};
        String sqlexist;
        String errorinfo;
        
        List<String> checknumList = xCheckRuleService.getChecknum("xgrkhxx");

        try {
        	connection = JDBCUtils.getConnection();
        	rsCount = JDBCUtils.Query(connection, ps, sqlcount);           
            
            if (rsCount.next()) {	//没有可校验的数据
            	total = rsCount.getInt(1);
            }
           
            if(total == 0) {
            	json.put("msg","-1");
                try {
                    PrintWriter out = response.getWriter();
                    out.write(json.toString());
                    out.flush();
                    out.close();
                } catch (IOException e) {
                	e.printStackTrace();
                }
                return;
            }
            page = total % size == 0 ? total/size : total/size+1;
                    	
              
        long aaa = System.currentTimeMillis();
/*        
        if(xDataBaseTypeUtil.equalsMySql()) {
        	//需要加索引的字段
            Map<String,String> fieldMap = new HashMap<String,String>();
            fieldMap.put("sjrq", "xgrkhxx");
            fieldMap.put("finorgcode", "xgrkhxx");
            fieldMap.put("regamtcreny", "xgrkhxx");
            fieldMap.put("customercode", "xgrkhxx");
            fieldMap.put("sjrq#", "xgrkhxxh");
            fieldMap.put("finorgcode#", "xgrkhxxh");
            fieldMap.put("regamtcreny#", "xgrkhxxh");
            fieldMap.put("customercode#", "xgrkhxxh");
            fieldMap.put("sjrq##", "xclgrdkxx");
            fieldMap.put("isfarmerloan", "xclgrdkxx");
            fieldMap.put("brroweridnum", "xclgrdkxx");
            fieldMap.put("sjrq###", "xclgrdkxxh");
            fieldMap.put("isfarmerloan#", "xclgrdkxxh");
            fieldMap.put("brroweridnum#", "xclgrdkxxh");
            
            indexMap = addIndex(connection, fieldMap);
            
            long bindex = System.currentTimeMillis();
            System.out.println("创建索引耗时："+(bindex -a) +"毫秒");
        }
 */       
        
        
        if(checklimitNum == 0) {
        	if(checknumList.contains("JS2312")) {
        		if(xDataBaseTypeUtil.equalsSqlServer()) {
    		    	//数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
    		    	sqlexist = "select id,customercode from xgrkhxx t1 "+whereStr2.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,regamtcreny,customercode from xgrkhxx where operationname != '申请删除' group by sjrq,regamtcreny,customercode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.regamtcreny = t2.regamtcreny and t1.customercode = t2.customercode)";
    		        errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一";
    		        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
    		        
    		        //数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
    		        sqlexist = "select id,customercode from xgrkhxx t1 "+whereStr2.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,regamtcreny,customercode from xgrkhxxh group by sjrq,regamtcreny,customercode having count(1) > 0) t2 where t1.sjrq = t2.sjrq and t1.regamtcreny = t2.regamtcreny and t1.customercode = t2.customercode)";
    		        errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一（历史表）";
    		        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
    	    	}else {
    	    		//数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
    		    	sqlexist = "select id,customercode from xgrkhxx "+whereStr+" and (sjrq,regamtcreny,customercode) in (select sjrq,regamtcreny,customercode from xgrkhxx where operationname != '申请删除' group by sjrq,regamtcreny,customercode having count(1) > 1)";
    		        errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一";
    		        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
    		        
    		        //数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
    		        sqlexist = "select id,customercode from xgrkhxx "+whereStr+" and (sjrq,regamtcreny,customercode) in (select sjrq,regamtcreny,customercode from xgrkhxxh group by sjrq,regamtcreny,customercode having count(1) > 0)";
    		        errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一（历史表）";
    		        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
    	    	}
        	}
        	
//        	if(checknumList.contains("JS2195")) {
//        		//个人客户的国籍与地区代码报送保持一致
//        		sqlexist = "select t4.aid id,t4.acustomercode customercode from (select t1.id aid,t3.countrycode acountry,t1.sjrq asjrq,t1.regamtcreny aregamtcreny,t1.customercode acustomercode from xgrkhxx t1 inner join basecountrytwo t2 on t1.country = t2.countrytwocode inner join basecountry t3 on t2.countrytwoname = t3.countryname "+whereStr2.replace("a.", "t1.")+") t4 where exists (select 1 from xclgrdkxx t5 where t4.asjrq = t5.sjrq and t4.aregamtcreny = t5.isfarmerloan and t4.acustomercode = t5.brroweridnum and t5.brrowerareacode like '000%' and t4.acountry <> t5.brrowerareacode)";
//        		errorinfo = "个人客户的国籍与地区代码报送保持一致";
//                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//        	}
        	if(xDataBaseTypeUtil.equalsOracle()) {
        		if(checknumList.contains("JS1291")) {
            		//当客户存在信贷业务时，个人年收入不能为空
            		sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+whereStr2+" and exists (select 1 from xclgrdkxx b where a.sjrq = b.sjrq and a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and nvl(ltrim(a.grincome),' ') = ' ')";
            		errorinfo = "当客户存在信贷业务时，个人年收入不能为空";
                    executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
            	
            	if(checknumList.contains("JS1292")) {
            		//当客户存在信贷业务时，家庭年收入不能为空
            		sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+whereStr2+" and exists (select 1 from xclgrdkxx b where a.sjrq = b.sjrq and a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and nvl(ltrim(a.familyincome),' ') = ' ')";
            		errorinfo = "当客户存在信贷业务时，家庭年收入不能为空";
                    executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
            	
            	if(checknumList.contains("JS1555")) {
                //当客户有存续的贷款业务时，授信额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ "and exists (select 1 from xclgrdkxx b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and nvl(ltrim(a.creditamt),' ') = ' ' and b.operationname != '申请删除')";
                errorinfo = "当客户有存续的贷款业务时，授信额度不能为空";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //当客户有存续的贷款业务时，授信额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ "and exists (select 1 from xclgrdkxxh b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and nvl(ltrim(a.creditamt),' ') = ' ')";
                errorinfo = "当客户有存续的贷款业务时，授信额度不能为空（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
            	
            	if(checknumList.contains("JS1556")) {
                //当客户有存续的贷款业务时，已用额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ "and exists (select 1 from xclgrdkxx b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and nvl(ltrim(a.usedamt),' ') = ' ' and b.operationname != '申请删除')";
                errorinfo = "当客户有存续的贷款业务时，已用额度不能为空";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //当客户有存续的贷款业务时，已用额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ "and exists (select 1 from xclgrdkxxh b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and nvl(ltrim(a.usedamt),' ') = ' ')";
                errorinfo = "当客户有存续的贷款业务时，已用额度不能为空（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
        	}else {
        		if(checknumList.contains("JS1291")) {
            		//当客户存在信贷业务时，个人年收入不能为空
            		sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+whereStr2+" and exists (select 1 from xclgrdkxx b where a.sjrq = b.sjrq and a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and ltrim(a.grincome) is null)";
            		errorinfo = "当客户存在信贷业务时，个人年收入不能为空";
                    executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
            	
            	if(checknumList.contains("JS1292")) {
            		//当客户存在信贷业务时，家庭年收入不能为空
            		sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+whereStr2+" and exists (select 1 from xclgrdkxx b where a.sjrq = b.sjrq and a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and ltrim(a.familyincome) is null)";
            		errorinfo = "当客户存在信贷业务时，家庭年收入不能为空";
                    executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
            	
            	if(checknumList.contains("JS1555")) {
                //当客户有存续的贷款业务时，授信额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ "and exists (select 1 from xclgrdkxx b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.creditamt) is null and b.operationname != '申请删除')";
                errorinfo = "当客户有存续的贷款业务时，授信额度不能为空";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //当客户有存续的贷款业务时，授信额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ "and exists (select 1 from xclgrdkxxh b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.creditamt) is null)";
                errorinfo = "当客户有存续的贷款业务时，授信额度不能为空（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
            	
            	if(checknumList.contains("JS1556")) {
                //当客户有存续的贷款业务时，已用额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ "and exists (select 1 from xclgrdkxx b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.usedamt) is null and b.operationname != '申请删除')";
                errorinfo = "当客户有存续的贷款业务时，已用额度不能为空";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //当客户有存续的贷款业务时，已用额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ "and exists (select 1 from xclgrdkxxh b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.usedamt) is null)";
                errorinfo = "当客户有存续的贷款业务时，已用额度不能为空（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
        	}
        	
        	
//        	if(xDataBaseTypeUtil.equalsSqlServer()) {
//        		if(checknumList.contains("JS1613")) {
//        	        //当客户发生过贷款业务时，客户信用评级不能为空
//        	        sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
//        	        		+ "and exists (select 1 from xgrdkfsxx b where a.customercode = b.browidcode and a.sjrq = b.sjrq and ltrim(a.actctrltype) is null and substring(b.loanprocode,1,3) not in ('F07','F06','F03','F01') and b.operationname != '申请删除')";
//        	        errorinfo = "当客户发生过贷款业务时，客户信用评级不能为空";
//        	        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//        	        
//        	        //当客户发生过贷款业务时，客户信用评级不能为空
//        	        sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
//        	        		+ "and exists (select 1 from xgrdkfsxxh b where a.customercode = b.browidcode and a.sjrq = b.sjrq and ltrim(a.actctrltype) is null and substring(b.loanprocode,1,3) not in ('F07','F06','F03','F01'))";
//        	        errorinfo = "当客户发生过贷款业务时，客户信用评级不能为空（历史表）";
//        	        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//        	    	}
//        	    	
//        	    	if(checknumList.contains("JS1641")) {
//        	        //当客户还有存量贷款时，客户信用评级不能为空
//        	        sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
//        	        		+ "and exists (select 1 from xclgrdkxx b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and substring(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and ltrim(a.actctrltype) is null and b.operationname != '申请删除')";
//        	        errorinfo = "当客户还有存量贷款时，客户信用评级不能为空";
//        	        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo); 
//        	        
//        	        //当客户还有存量贷款时，客户信用评级不能为空
//        	        sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
//        	        		+ "and exists (select 1 from xclgrdkxxh b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and substring(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and ltrim(a.actctrltype) is null)";
//        	        errorinfo = "当客户还有存量贷款时，客户信用评级不能为空（历史表）";
//        	        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo); 
//        	    	} 
//        	}else {
//        		if(checknumList.contains("JS1613")) {
//        	        //当客户发生过贷款业务时，客户信用评级不能为空
//        	        sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
//        	        		+ "and exists (select 1 from xgrdkfsxx b where a.customercode = b.browidcode and a.sjrq = b.sjrq and ltrim(a.actctrltype) is null and substr(b.loanprocode,1,3) not in ('F07','F06','F03','F01') and b.operationname != '申请删除')";
//        	        errorinfo = "当客户发生过贷款业务时，客户信用评级不能为空";
//        	        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//        	        
//        	        //当客户发生过贷款业务时，客户信用评级不能为空
//        	        sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
//        	        		+ "and exists (select 1 from xgrdkfsxxh b where a.customercode = b.browidcode and a.sjrq = b.sjrq and ltrim(a.actctrltype) is null and substr(b.loanprocode,1,3) not in ('F07','F06','F03','F01'))";
//        	        errorinfo = "当客户发生过贷款业务时，客户信用评级不能为空（历史表）";
//        	        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//        	    	}
//        	    	
//        	    	if(checknumList.contains("JS1641")) {
//        	        //当客户还有存量贷款时，客户信用评级不能为空
//        	        sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
//        	        		+ "and exists (select 1 from xclgrdkxx b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and substr(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and ltrim(a.actctrltype) is null and b.operationname != '申请删除')";
//        	        errorinfo = "当客户还有存量贷款时，客户信用评级不能为空";
//        	        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo); 
//        	        
//        	        //当客户还有存量贷款时，客户信用评级不能为空
//        	        sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
//        	        		+ "and exists (select 1 from xclgrdkxxh b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and substr(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and ltrim(a.actctrltype) is null)";
//        	        errorinfo = "当客户还有存量贷款时，客户信用评级不能为空（历史表）";
//        	        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo); 
//        	    	} 
//        	}
        	
        	
        	errorId.clear();
        }else {
        	if(checknumList.contains("JS2312")) {
        		if(xDataBaseTypeUtil.equalsSqlServer()) {
    		    	//数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
    		    	sqlexist = "select id,customercode from xgrkhxx t1 "+whereStr2.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,finorgcode,regamtcreny,customercode from xgrkhxx where operationname != '申请删除' group by sjrq,finorgcode,regamtcreny,customercode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.finorgcode = t2.finorgcode and t1.regamtcreny = t2.regamtcreny and t1.customercode = t2.customercode)";
    		        errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一";
    		        executeCheckSqlLimit("数据日期+金融机构代码+客户证件类型+客户证件代码应唯一", limitMap, checklimitNum, errorId0, sqlexist);
    		        
    		        //数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
    		        sqlexist = "select id,customercode from xgrkhxx t1 "+whereStr2.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,finorgcode,regamtcreny,customercode from xgrkhxxh group by sjrq,finorgcode,regamtcreny,customercode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.finorgcode = t2.finorgcode and t1.regamtcreny = t2.regamtcreny and t1.customercode = t2.customercode)";
    		        errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一（历史表）";
    		        executeCheckSqlLimit("数据日期+金融机构代码+客户证件类型+客户证件代码应唯一（历史表）", limitMap, checklimitNum, errorId0, sqlexist);
    	    	}else {
    	    		//数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
    		    	sqlexist = "select id,customercode from xgrkhxx "+whereStr+" and (sjrq,finorgcode,regamtcreny,customercode) in (select sjrq,finorgcode,regamtcreny,customercode from xgrkhxx where operationname != '申请删除' group by sjrq,finorgcode,regamtcreny,customercode having count(1) > 1)";
    		        errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一";
    		        executeCheckSqlLimit("数据日期+金融机构代码+客户证件类型+客户证件代码应唯一", limitMap, checklimitNum, errorId0, sqlexist);
    		        
    		        //数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
    		        sqlexist = "select id,customercode from xgrkhxx "+whereStr+" and (sjrq,finorgcode,regamtcreny,customercode) in (select sjrq,finorgcode,regamtcreny,customercode from xgrkhxxh group by sjrq,finorgcode,regamtcreny,customercode having count(1) > 0)";
    		        errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一（历史表）";
    		        executeCheckSqlLimit("数据日期+金融机构代码+客户证件类型+客户证件代码应唯一（历史表）", limitMap, checklimitNum, errorId0, sqlexist);
    	    	}
        	}
        	
//        	if(checknumList.contains("JS2195")) {
//        		//个人客户的国籍与地区代码报送保持一致
//        		sqlexist = "select t4.aid id,t4.acustomercode customercode from (select t1.id aid,t3.countrycode acountry,t1.sjrq asjrq,t1.regamtcreny aregamtcreny,t1.customercode acustomercode from xgrkhxx t1 inner join basecountrytwo t2 on t1.country = t2.countrytwocode inner join basecountry t3 on t2.countrytwoname = t3.countryname "+whereStr2.replace("a.", "t1.")+") t4 where exists (select 1 from xclgrdkxx t5 where t4.asjrq = t5.sjrq and t4.aregamtcreny = t5.isfarmerloan and t4.acustomercode = t5.brroweridnum and t5.brrowerareacode like '000%' and t4.acountry <> t5.brrowerareacode)";
//        		errorinfo = "个人客户的国籍与地区代码报送保持一致";
//                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//        	}
        	
        	if(xDataBaseTypeUtil.equalsOracle()) {
        		if(checknumList.contains("JS1291")) {
            		//当客户存在信贷业务时，个人年收入不能为空
            		sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+whereStr2+" and exists (select 1 from xclgrdkxx b where a.sjrq = b.sjrq and a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and nvl(ltrim(a.grincome),' ') = ' ')";
            		errorinfo = "当客户存在信贷业务时，个人年收入不能为空";
            		executeCheckSqlLimit("当客户存在信贷业务时，个人年收入不能为空", limitMap, checklimitNum, errorId0, sqlexist);
            	}
            	
            	if(checknumList.contains("JS1292")) {
            		//当客户存在信贷业务时，家庭年收入不能为空
            		sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+whereStr2+" and exists (select 1 from xclgrdkxx b where a.sjrq = b.sjrq and a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and nvl(ltrim(a.familyincome),' ') = ' ')";
            		errorinfo = "当客户存在信贷业务时，家庭年收入不能为空";
            		executeCheckSqlLimit("当客户存在信贷业务时，家庭年收入不能为空", limitMap, checklimitNum, errorId0, sqlexist);
            	}
            	
            	if(checknumList.contains("JS1555")) {
                    //当客户有存续的贷款业务时，授信额度不能为空
                    sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                    		+ "and exists (select 1 from xclgrdkxx b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and b.operationname != '申请删除') and trim(a.creditamt) is null";
                    errorinfo = "当客户有存续的贷款业务时，授信额度不能为空";
                    executeCheckSqlLimit("当客户有存续的贷款业务时，授信额度不能为空", limitMap, checklimitNum, errorId0, sqlexist);
                    
                    //当客户有存续的贷款业务时，授信额度不能为空
                    sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                    		+ "and exists (select 1 from xclgrdkxxh b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq) and trim(a.creditamt) is null";
                    errorinfo = "当客户有存续的贷款业务时，授信额度不能为空（历史表）";
                    executeCheckSqlLimit("当客户有存续的贷款业务时，授信额度不能为空（历史表）", limitMap, checklimitNum, errorId0, sqlexist);
                	}
                	
                	if(checknumList.contains("JS1556")) {
                    //当客户有存续的贷款业务时，已用额度不能为空
                    sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                    		+ "and exists (select 1 from xclgrdkxx b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and b.operationname != '申请删除') and trim(a.usedamt) is null";
                    errorinfo = "当客户有存续的贷款业务时，已用额度不能为空";
                    executeCheckSqlLimit("当客户有存续的贷款业务时，已用额度不能为空", limitMap, checklimitNum, errorId0, sqlexist);
                    
                    //当客户有存续的贷款业务时，已用额度不能为空
                    sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                    		+ "and exists (select 1 from xclgrdkxxh b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq) and trim(a.usedamt) is null";
                    errorinfo = "当客户有存续的贷款业务时，已用额度不能为空（历史表）";
                    executeCheckSqlLimit("当客户有存续的贷款业务时，已用额度不能为空（历史表）", limitMap, checklimitNum, errorId0, sqlexist);
                	}   
            	/*
            	if(checknumList.contains("JS1555")) {
                //当客户有存续的贷款业务时，授信额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ " and nvl(ltrim(a.creditamt),' ') = ' ' and a.regamtcreny||a.customercode||a.sjrq in (select isfarmerloan||brroweridnum||sjrq from xclgrdkxx where operationname != '申请删除')";
                errorinfo = "当客户有存续的贷款业务时，授信额度不能为空";
                executeCheckSqlLimit("当客户有存续的贷款业务时，授信额度不能为空", limitMap, checklimitNum, errorId0, sqlexist);
                
                //当客户有存续的贷款业务时，授信额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ " and nvl(ltrim(a.creditamt),' ') = ' ' and a.regamtcreny||a.customercode||a.sjrq in (select isfarmerloan||brroweridnum||sjrq from xclgrdkxxh)";
                errorinfo = "当客户有存续的贷款业务时，授信额度不能为空（历史表）";
                executeCheckSqlLimit("当客户有存续的贷款业务时，授信额度不能为空（历史表）", limitMap, checklimitNum, errorId0, sqlexist);
            	}
            	
            	if(checknumList.contains("JS1556")) {
                //当客户有存续的贷款业务时，已用额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ " and nvl(ltrim(a.usedamt),' ') = ' ' and a.regamtcreny||a.customercode||a.sjrq in (select isfarmerloan||brroweridnum||sjrq from xclgrdkxx where operationname != '申请删除')";
                errorinfo = "当客户有存续的贷款业务时，已用额度不能为空";
                executeCheckSqlLimit("当客户有存续的贷款业务时，已用额度不能为空", limitMap, checklimitNum, errorId0, sqlexist);
                
                //当客户有存续的贷款业务时，已用额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ " and nvl(ltrim(a.usedamt),' ') = ' ' and a.regamtcreny||a.customercode||a.sjrq in (select isfarmerloan||brroweridnum||sjrq from xclgrdkxxh)";
                errorinfo = "当客户有存续的贷款业务时，已用额度不能为空（历史表）";
                executeCheckSqlLimit("当客户有存续的贷款业务时，已用额度不能为空（历史表）", limitMap, checklimitNum, errorId0, sqlexist);
            	}   
            	*/
        	}else {
        		if(checknumList.contains("JS1291")) {
            		//当客户存在信贷业务时，个人年收入不能为空
            		sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+whereStr2+" and exists (select 1 from xclgrdkxx b where a.sjrq = b.sjrq and a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and ltrim(a.grincome) is null)";
            		errorinfo = "当客户存在信贷业务时，个人年收入不能为空";
            		executeCheckSqlLimit("当客户存在信贷业务时，个人年收入不能为空", limitMap, checklimitNum, errorId0, sqlexist);
            	}
            	
            	if(checknumList.contains("JS1292")) {
            		//当客户存在信贷业务时，家庭年收入不能为空
            		sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+whereStr2+" and exists (select 1 from xclgrdkxx b where a.sjrq = b.sjrq and a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and ltrim(a.familyincome) is null)";
            		errorinfo = "当客户存在信贷业务时，家庭年收入不能为空";
            		executeCheckSqlLimit("当客户存在信贷业务时，家庭年收入不能为空", limitMap, checklimitNum, errorId0, sqlexist);
            	}
            	
            	if(checknumList.contains("JS1555")) {
                //当客户有存续的贷款业务时，授信额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ "and exists (select 1 from xclgrdkxx b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.creditamt) is null and b.operationname != '申请删除')";
                errorinfo = "当客户有存续的贷款业务时，授信额度不能为空";
                executeCheckSqlLimit("当客户有存续的贷款业务时，授信额度不能为空", limitMap, checklimitNum, errorId0, sqlexist);
                
                //当客户有存续的贷款业务时，授信额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ "and exists (select 1 from xclgrdkxxh b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.creditamt) is null)";
                errorinfo = "当客户有存续的贷款业务时，授信额度不能为空（历史表）";
                executeCheckSqlLimit("当客户有存续的贷款业务时，授信额度不能为空（历史表）", limitMap, checklimitNum, errorId0, sqlexist);
            	}
            	
            	if(checknumList.contains("JS1556")) {
                //当客户有存续的贷款业务时，已用额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ "and exists (select 1 from xclgrdkxx b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.usedamt) is null and b.operationname != '申请删除')";
                errorinfo = "当客户有存续的贷款业务时，已用额度不能为空";
                executeCheckSqlLimit("当客户有存续的贷款业务时，已用额度不能为空", limitMap, checklimitNum, errorId0, sqlexist);
                
                //当客户有存续的贷款业务时，已用额度不能为空
                sqlexist = "select a.id id,a.customercode customercode from xgrkhxx a "+ whereStr2
                		+ "and exists (select 1 from xclgrdkxxh b where a.regamtcreny = b.isfarmerloan and a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.usedamt) is null)";
                errorinfo = "当客户有存续的贷款业务时，已用额度不能为空（历史表）";
                executeCheckSqlLimit("当客户有存续的贷款业务时，已用额度不能为空（历史表）", limitMap, checklimitNum, errorId0, sqlexist);
            	}   
        	}
        	
        	     	
        	
        	
        }
    	
        long bbb = System.currentTimeMillis();
        System.out.println("表间校验用时："+(bbb-aaa)/1000+"秒");
        outer:for(int i = 0; i < page; i++) { 
    		if(i != 0) {
    			isFirst = false;
    		}
    		long axun = System.currentTimeMillis();
            resultSet = JDBCUtils.Query(sql,0, size);
    	
        
        Xgrkhxx xgrkhxx = null;
        if(checklimitNum == 0) {
        	while (true) {
                if (!resultSet.next()) break;               
                xgrkhxx = new Xgrkhxx();
                Stringutil.getEntity(resultSet, xgrkhxx);
                CheckAllData3.checkXgrkhxx(customSqlUtil, xgrkhxx, errorMsg,errorMsg2, errorId, rightId,checknumList);
            }
        }else {
        	while (true) {
                if (!resultSet.next()) break;               
                xgrkhxx = new Xgrkhxx();
                Stringutil.getEntity(resultSet, xgrkhxx);
                CheckAllData3.checkXgrkhxxLimit(customSqlUtil, xgrkhxx, errorId0, errorId, rightId, checknumList, limitMap, checklimitNum);
            }
        }
            resultSet.close();
            if (errorId!=null && errorId.size()>0){
            	b = true;
            	if(checklimitNum == 0) {
            		//-下载到本地
                	StringBuffer errorMsgAll = new StringBuffer("");
                	for(Map.Entry<String, String> entry : errorMsg2.entrySet()) {
                		errorMsgAll.append(entry.getValue()+"\r\n");
                	}
                    DownloadUtil.downLoad2(errorMsgAll.toString(),target+File.separator+"checkout",fileName+".txt",isFirst);
                    errorMsgAll.setLength(0);
                    errorMsgAll= null;
            	}
            	errorMsg2.clear();

                setMap.put("checkStatus","2");
                setMap.put("operationname"," ");
                customSqlUtil.updateByWhereBatchGR(tableName, setMap, errorId);
                errorId.clear();
            }
            if (rightId!=null && rightId.size()>0){
                setMap.put("checkStatus","1");
                setMap.put("operationname"," ");
                customSqlUtil.updateByWhereBatchGR(tableName, setMap, rightId);
                rightId.clear();
            }
            long bxun = System.currentTimeMillis();
            System.out.println("第"+(i+1)+"批10w数据用时"+(bxun-axun)/1000+"秒");
            }
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }finally {
        	if(indexMap != null && indexMap.size() != 0) {
        		dropIndex(connection, indexMap);
        	}
        	if(rsCount != null){
        		try {
					rsCount.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
            if (resultSet!=null){
            	try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
            }
            if(ps != null) {
            	try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
            }
            if(connection != null) {
            	try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
            }
            if(CheckAllData3.ftyList != null) {
            	CheckAllData3.ftyList.clear();
            	CheckAllData3.ftyList = null;
            }
        }
        //校验结束


        if (b){
            json.put("msg","0");
        }else {
            json.put("msg","1");
        }
        
        if(checklimitNum != 0) {
        	if(CheckAllData3.limitFlag) {
        		//limitBuffer.append("阈值量:"+checklimitNum+"\r\n");
            	limitBuffer.append("提示错误种类数量:"+limitMap.size()+"\r\n");
            	limitBuffer.append("因错误数据较多,目前只显示错误示例数据；\r\n");
            	limitBuffer.append("\r\n");
        	}
        	for(Map.Entry<String, String> entry: limitMap.entrySet()) {
        		limitBuffer.append(entry.getKey()+"："+errorcode[0]+"->\r\n");
        		limitBuffer.append(entry.getValue().substring(entry.getValue().lastIndexOf("#")+1));
        	}
        }
        if(limitBuffer.length() != 0) {
            	//-下载到本地
           DownloadUtil.downLoad2(limitBuffer.toString(),target+File.separator+"checkout",fileName+".txt",true);

        }
        limitBuffer.setLength(0);

        //校验结束之后 没有写审核-暂时在这里修改状态为 审核通过
        /*setMap.put("datastatus","3");
        setMap.put("operationname","");
        whereMap.put("datastatus","0");
        whereMap.put("checkstatus","1");
        int xdkdbht1 = customSqlUtil.updateByWhere(tableName, setMap, whereMap);
        Map<Integer,String> countMap=new HashMap<>();
        countMap.put(0,"reduce");
        countMap.put(3,"add");
        countUtil.handleMoreCount(tableName,countMap,xdkdbht1);*/
        customSqlUtil.saveLog("个人客户基础信息校验成功","校验");
        long b1 = System.currentTimeMillis();
        		System.out.println("校验用时："+(b1-a)/1000+"秒");
        try {
            PrintWriter out = response.getWriter();
            out.write(json.toString());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
        	CheckAllData3.limitFlag = false;
        }
    }

    @GetMapping("XDownLoadCheckXgrkhxx")
    public void XDownLoadCheckXgrkhxx(HttpServletResponse response) {
        if (StringUtils.isBlank(target)){
            target=suisService.findAll().get(0).getMessagepath();
        }
        DownloadUtil.downLoadCheckFile(response,target + File.separator + "checkout" ,fileName+".txt");
    }
    
    public void executeCheckSql(Map<String,String> setMap,List<String> errorId,Map<String,String> whereMap,LinkedHashMap<String, String> errorMsg,String sql,String[] errorcode,String errorinfo) {
    	List<Object[]> exist = customSqlUtil.executeQuery(sql);
        if (exist!=null && exist.size()>0){
            setMap.put("checkStatus","2");
            setMap.put("operationname"," ");
            exist.forEach(errId->{
                errorId.add(String.valueOf(errId[0]));
                whereMap.put("id", String.valueOf(errId[0]));
                //customSqlUtil.updateByWhere(tableName,setMap,whereMap);
                if(!errorMsg.containsKey(String.valueOf(errId[0]))) {
                	errorMsg.put(String.valueOf(errId[0]), errorcode[0]+":"+String.valueOf(errId[1])+"]->\r\n");
                }
                String str = errorMsg.get(String.valueOf(errId[0]));
                str = str + errorinfo+"|";
                errorMsg.put(String.valueOf(errId[0]), str);
            });
            whereMap.clear();
            setMap.clear();
            exist.clear();
        }
    }
    
    public void executeCheckSqlLimit(String rule,Map<String,String> map,int checknumLimit,List<Integer> errorId,String sql) {
    	List<Object[]> exist = customSqlUtil.executeQuery(sql);
        if (exist!=null && exist.size()>0){
            exist.forEach(errId->{
                errorId.add((Integer) errId[0]);
                CheckAllData3.putMap(rule, String.valueOf(errId[1]), map, checknumLimit);
            });
            exist.clear();
        }
    }
    
    //创建索引
    public Map<String,String> addIndex(Connection conn,Map<String,String> fieldMap) {
    	Statement ps = null;
    	Map<String,String> indexMap = new HashMap<String,String>();
    	try {
    		ps = conn.createStatement();
    		for(Map.Entry<String, String> entry : fieldMap.entrySet()) {
    			String index = "index_"+Stringutil.getUUid();
    			String sql = "CREATE INDEX "+index+" ON "+entry.getValue()+" ("+entry.getKey().replaceAll("#", "")+")";
    			ps.addBatch(sql);
    			indexMap.put(index, entry.getValue());
    		}
    		ps.executeBatch();
    	}catch(Exception e) {
    		e.printStackTrace();
    		System.out.println("创建索引失败...");
    	}finally {
    		if(ps != null) {
    			try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
    		}
    	}
		return indexMap;
    }
    
    //删除索引
    public void dropIndex(Connection conn,Map<String,String> indexMap) {
    	Statement ps = null;
    	try {
    		ps = conn.createStatement();
    		for(Map.Entry<String, String> entry : indexMap.entrySet()) {
    			ps.addBatch("drop index "+entry.getKey()+" on "+entry.getValue()+"");
    		}
    		ps.executeBatch();
    	}catch(Exception e) {
    		e.printStackTrace();
    		System.out.println("删除索引失败...");
    	}finally {
    		if(ps != null) {
    			try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
    		}
    	}
    }
}
