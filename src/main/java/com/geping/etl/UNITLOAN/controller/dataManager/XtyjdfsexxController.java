package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.report.Xtyjdfsexx;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XtyjdfsexxService;
import com.geping.etl.UNITLOAN.util.*;
import com.geping.etl.UNITLOAN.util.check.CheckAllDatatyModule;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.utils.jdbc.JDBCUtils;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**    
*  
* @author liuweixin  
* @date 2021年1月6日 上午10:30:09  
*/
@RestController
public class XtyjdfsexxController {

	@Autowired
	XtyjdfsexxService xtyjdfsexxService;
	
	@Autowired
    private SUpOrgInfoSetService suisService;
	
	@Autowired
    private ExcelUploadUtil excelUploadUtil;
	
	@Autowired
    private CustomSqlUtil customSqlUtil;
	
	@Autowired
    private XcommonService commonService;
	
	@Autowired
    private XCheckRuleService checkRuleService;
	
	@Autowired
    private  XDataBaseTypeUtil dataBaseTypeUtil;
	
	@Autowired
    private DepartUtil departUtil;
	
	private final String tableName="xtyjdfsexx";
	
	private final String fileName="同业借贷发生额信息";
	
	private static String target="";
	
	@GetMapping("/XGetXtyjdfsexxDataUi")
    public ModelAndView XGoDataTwoUi (String status){
        ModelAndView modelAndView=new ModelAndView();
        List<BaseArea> list = XApplicationRunnerImpl.baseAreaList;
        List<BaseCountry> countryList = XApplicationRunnerImpl.baseCountryList;
        for (BaseCountry country : countryList) {
        	BaseArea area = new BaseArea();
        	area.setAreacode(country.getCountrycode());
        	area.setAreaname(country.getCountryname());
        	list.add(area);
        }
        modelAndView.addObject("baseAreaAndCountryList",list);
        modelAndView.addObject("baseAreaList",XApplicationRunnerImpl.baseAreaList);
        modelAndView.addObject("baseCountryList",XApplicationRunnerImpl.baseCountryList);
        modelAndView.addObject("baseAindustryList",XApplicationRunnerImpl.baseAindustryList);
        modelAndView.addObject("baseBindustryList",XApplicationRunnerImpl.baseBindustryList);
        modelAndView.addObject("baseCurrencyList",XApplicationRunnerImpl.baseCurrencyList);
        if ("dtj".equals(status)){
            modelAndView.addObject("datastatus","0");
            modelAndView.setViewName("unitloan/datamanage/tymodule/tyjdfsexxdtj");
        }else if ("dsh".equals(status)){
            modelAndView.addObject("datastatus","1");
            modelAndView.setViewName("unitloan/datamanage/tymodule/tyjdfsexxdtj");
        }else {
            modelAndView.addObject("datastatus","3");
            modelAndView.addObject("datamanege",status);
            modelAndView.setViewName("unitloan/createreport/tyjdfsexxbw");
        }
        return modelAndView;
    }
	
	//查询数据
    @PostMapping("XGetXtyjdfsexxData")
    public ResponseResult XGetXdkfsxxData(int page, int rows,String financeorgcodeParam,String contractcodeParam,String operationnameParam,String jydsdmlbParam,
       String jydsdmParam,String checkstatusParam,String datastatus,HttpServletRequest request){
        page = page - 1;
        Sys_UserAndOrgDepartment sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");

        String departId = departUtil.getDepart(sys_user);
        Specification specification=new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                //待提交
                predicates.add(criteriaBuilder.equal(root.get("datastatus"), datastatus));

                predicates.add(criteriaBuilder.like(root.get("departid"), departId));
                if (StringUtils.isNotBlank(financeorgcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("financeorgcode"), "%"+financeorgcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(contractcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("contractcode"), "%"+contractcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(jydsdmlbParam)){
                    predicates.add(criteriaBuilder.like(root.get("jydsdmlb"), "%"+jydsdmlbParam+"%"));
                }
                if (StringUtils.isNotBlank(jydsdmParam)){
                    predicates.add(criteriaBuilder.like(root.get("jydsdm"), "%"+jydsdmParam+"%"));
                }
                if (StringUtils.isNotBlank(checkstatusParam)){
                    predicates.add(criteriaBuilder.equal(root.get("checkstatus"), checkstatusParam));
                }
                if (StringUtils.isNotBlank(operationnameParam)){
                    predicates.add(criteriaBuilder.equal(root.get("operationname"), operationnameParam));
                }
                predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
        PageRequest pageRequest = new PageRequest(page, rows, sort);
        Page<Xtyjdfsexx> all = xtyjdfsexxService.findAll(specification, pageRequest);
        List<Xtyjdfsexx> content = all.getContent();
        long totalCount = all.getTotalElements();
        return ResponseResult.success(totalCount,content);
    }
    
  //导入
    @PostMapping(value = "XimmportExceltyjdfsexx",produces = "text/plain;charset=UTF-8")
    public void XimmportExcelTwo(HttpServletRequest request,HttpServletResponse response){
    	Sys_UserAndOrgDepartment sys_user=((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        File uploadFile = null;
        OPCPackage opcPackage=null;
        PrintWriter out = null;
        try {
            out = response.getWriter();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multiRequest.getFile("excelfile");
            JSONObject json = new JSONObject();
            if (file == null) {
                json.put("msg", "导入文件为空");
            } else {
                uploadFile = excelUploadUtil.uploadFile(file);
                opcPackage = OPCPackage.open(uploadFile);
                String[] headers = {"金融机构代码","内部机构号","交易对手代码","交易对手代码类别","合同编码","交易流水号","资产负债类型","产品类别","合同起始日期","合同到期日期","合同实际终止日期","币种","发生金额","发生金额折人民币","利率是否固定","利率水平","定价基准类型","基准利率","计息方式","发生/结清标识","数据日期"};
                String[] values = {"financeorgcode","financeorginnum","jydsdm","jydsdmlb","contractcode","jylsh","zcfzlx","productcetegory","startdate","enddate","finaldate","currency","receiptbalance","receiptcnybalance","interestisfixed","interestislevel","fixpricetype","baseinterest","jxfs","fsjqbs","sjrq"};
                String[] rateFields = {"interestislevel","baseinterest"};
                String[] amountFields = {"receiptbalance","receiptcnybalance"};
                String[] dateFields = {"startdate", "enddate","finaldate","sjrq"};
                XCommonExcel commonExcel = new XCommonExcel(sys_user, opcPackage, customSqlUtil, new Xtyjdfsexx(),headers,values, amountFields,dateFields, null,rateFields);
                commonExcel.process(0);
                String departId = departUtil.getDepart(sys_user);
                if (commonExcel.msg.toString().length() == 0) {
                	commonService.importDelete(tableName,departId);
                    commonExcel.process(1);
                    json.put("msg","导入成功");
                }else {
                    json.put("msg",commonExcel.msg.toString());
                }


            }
            String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
            customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
            out.write(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                opcPackage.close();
                //将上传的文件删除
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    
    //校验
    @PostMapping("XCheckDataXtyjdfsexx")
    public void XCheckDataTwo(HttpServletRequest request, HttpServletResponse response,String financeorgcodeParam,String contractcodeParam,String jydsdmlbParam,String jydsdmParam){
        long a = System.currentTimeMillis();
        //记录是否有错
        boolean b=false;
        boolean oracle = dataBaseTypeUtil.equalsOracle();
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        List<SUpOrgInfoSet> sUpOrgInfoSets = suisService.findAll();
        target=sUpOrgInfoSets.get(0).getMessagepath();
        String departId = departUtil.getDepart(sys_user);

        JSONObject json = new JSONObject();
        String id = request.getParameter("id");

        LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
        List<String> errorId = new ArrayList<>();
        List<String> rightId = new ArrayList<>();
        Map<String,String> whereMap=new HashMap<>();
        Map<String,String> setMap=new HashMap<>();
        setMap.put("operator",sys_user.getLoginid());
        setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        String whereStr=" where datastatus='0' and checkstatus='0' ";
        String whereStra=" where a.datastatus='0' and a.checkstatus='0' ";
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            whereStr = whereStr + " and id in(" + id + ")";
            whereStra = whereStra + " and a.id in(" + id + ")";
        }else {
        	if (StringUtils.isNotBlank(financeorgcodeParam)){
                whereStr = whereStr + " and financeorgcode like '%"+ financeorgcodeParam +"%'";
                whereStra = whereStra + " and a.financeorgcode like '%"+ financeorgcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(contractcodeParam)){
                whereStr = whereStr + " and contractcode like '%"+ contractcodeParam +"%'";
                whereStra = whereStra + " and a.contractcode like '%"+ contractcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(jydsdmlbParam)){
                whereStr = whereStr + " and jydsdmlb like '%"+ jydsdmlbParam +"%'";
                whereStra = whereStra + " and a.jydsdmlb like '%"+ jydsdmlbParam +"%'";
            }
            if (StringUtils.isNotBlank(jydsdmParam)){
                whereStr = whereStr + " and jydsdm like '%"+ jydsdmParam +"%'";
                whereStra = whereStra + " and a.jydsdm like '%"+ jydsdmParam +"%'";
            }
            whereStr = whereStr + " and departid like '%"+ departId +"%'";
            whereStra = whereStra + " and a.departid like '%"+ departId +"%'";
        }
        String sql="select * from xtyjdfsexx "+whereStr;
        Connection connection = null;
        ResultSet resultSet=null;
        
        try {
            connection = JDBCUtils.getConnection();
            resultSet = JDBCUtils.Query(connection, sql);
            if (!resultSet.next()) {	//没有可校验的数据
            	json.put("msg","-1");
                try {
                    PrintWriter out = response.getWriter();
                    out.write(json.toString());
                    out.flush();
                    out.close();
                } catch (IOException e) {
                	e.printStackTrace();
                }
                return;
            }else {
                //标识名称
            	String errorcode = "合同编码";
                String errorcode2="交易对手代码";


                //校验开关集合
                List<String> checknumList = checkRuleService.getChecknum("xtyjdfsexx");

                //数据日期+金融机构代码+合同编码应唯一
                if (checknumList.contains("JS2300")){
                    if (!oracle){
                        String errorinfo  = "数据日期+金融机构代码+合同编码+交易流水号应唯一";
                        String sqlexist ="select id,contractcode,jydsdm from xtyjdfsexx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,financeorgcode,contractcode,jylsh from xtyjdfsexx where operationname != '申请删除' group by sjrq,financeorgcode,contractcode,jylsh having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.contractcode = t2.contractcode and t1.jylsh = t2.jylsh)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                        errorinfo = "数据日期+金融机构代码+合同编码+交易流水号应唯一（历史表）";
                        sqlexist ="select id,contractcode,jydsdm from xtyjdfsexx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,financeorgcode,contractcode,jylsh from xtyjdfsexxh group by sjrq,financeorgcode,contractcode,jylsh having count(1) > 0) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode  and t1.contractcode = t2.contractcode and t1.jylsh = t2.jylsh)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }else {
                        String errorinfo  = "数据日期+金融机构代码+合同编码+交易流水号应唯一";
                        String sqlexist ="select id,contractcode,jydsdm from xtyjdfsexx "+whereStr+" and (sjrq,financeorgcode,contractcode,jylsh) in (select sjrq,financeorgcode,contractcode,jylsh from xtyjdfsexx where operationname != '申请删除' group by sjrq,financeorgcode,contractcode,jylsh having  count(1) > 1)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                        errorinfo = "数据日期+金融机构代码+合同编码+交易流水号应唯一";
                        sqlexist = "select id,contractcode,jydsdm from xtyjdfsexx "+whereStr+" and (sjrq,financeorgcode,contractcode,jylsh) in (select sjrq,financeorgcode,contractcode,jylsh from xtyjdfsexxh group by sjrq,financeorgcode,contractcode,jylsh having  count(1) > 0)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }
                }
                
                //交易对手代码与存量同业借贷信息的交易对手代码应该一致
                if(checknumList.contains("JS1669")) {
                	String errorinfo  = "交易对手代码与存量同业借贷信息的交易对手代码应该一致";
                    String sqlexist ="select a.id,a.contractcode,a.jydsdm from xtyjdfsexx a INNER JOIN xcltyjdxx b on a.financeorginnum = b.financeorginnum and a.sjrq = b.sjrq and a.contractcode = b.contractcode "+whereStra+" and a.jydsdm != b.jydsdm and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    
                    errorinfo  = "交易对手代码与存量同业借贷信息的交易对手代码应该一致（历史表）";
                    sqlexist ="select a.id,a.contractcode,a.jydsdm from xtyjdfsexx a INNER JOIN xcltyjdxxh b on a.financeorginnum = b.financeorginnum and a.sjrq = b.sjrq and a.contractcode = b.contractcode "+whereStra+" and a.jydsdm != b.jydsdm";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                //产品类别与存量同业借贷信息的产品类别应该一致
                if(checknumList.contains("JS1671")) {
                	String errorinfo  = "产品类别与存量同业借贷信息的产品类别应该一致";
                    String sqlexist ="select a.id,a.contractcode,a.jydsdm from xtyjdfsexx a INNER JOIN xcltyjdxx b on a.financeorginnum = b.financeorginnum and a.sjrq = b.sjrq and a.contractcode = b.contractcode "+whereStra+" and a.productcetegory != b.productcetegory and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    
                    errorinfo  = "产品类别与存量同业借贷信息的产品类别应该一致（历史表）";
                    sqlexist ="select a.id,a.contractcode,a.jydsdm from xtyjdfsexx a INNER JOIN xcltyjdxxh b on a.financeorginnum = b.financeorginnum and a.sjrq = b.sjrq and a.contractcode = b.contractcode "+whereStra+" and a.productcetegory != b.productcetegory";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                //金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
                if(checknumList.contains("JS1808")) {
                	String errorinfo  = "金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致";
                    String sqlexist ="select a.id,a.contractcode,a.jydsdm from xtyjdfsexx a INNER JOIN xjrjgfz b on a.financeorginnum = b.inorgnum and a.sjrq = b.sjrq "+whereStra+" and a.financeorgcode != b.finorgcode and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    
                    errorinfo  = "金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致（历史表）";
                    sqlexist ="select a.id,a.contractcode,a.jydsdm from xtyjdfsexx a INNER JOIN xjrjgfzh b on a.financeorginnum = b.inorgnum and a.sjrq = b.sjrq "+whereStra+" and a.financeorgcode != b.finorgcode";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                //金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致
                if(checknumList.contains("JS1890")) {
                	String errorinfo  = "除SPV以外的其他金融机构的交易对手代码应该在同业客户基础信息.客户代码中存在";
                    String sqlexist ="select a.id,a.contractcode,a.jydsdm from xtyjdfsexx a "+whereStra+" and a.jydsdmlb in ('A01','A02','L01') and (a.jydsdm not in (select b.khcode from xtykhjcxx b INNER JOIN xtyjdfsexx c on b.sjrq = c.sjrq where b.operationname != '申请删除')  and a.jydsdm not in (select e.khcode from xtykhjcxxh e INNER JOIN xtyjdfsexx f on e.sjrq = f.sjrq ))";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                //境内同业客户的交易对手代码类别应该是统一社会信用代码或者组织机构代码或者SPV代码
                if(checknumList.contains("JS2086")) {
                	String errorinfo  = "境内同业客户的交易对手代码类别应该是统一社会信用代码或者组织机构代码或者SPV代码";
                    String sqlexist ="select a.id,a.contractcode,a.jydsdm from xtyjdfsexx a INNER JOIN xtykhjcxx b on a.jydsdm = b.khcode and a.sjrq = b.sjrq "+whereStra+" and b.dqdm not like '000%' and b.operationname != '申请删除' and a.jydsdmlb not in ('A01','A02','C01','C02','Z99')";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    
                    errorinfo  = "境内同业客户的交易对手代码类别应该是统一社会信用代码或者组织机构代码或者SPV代码（历史表）";
                    sqlexist ="select a.id,a.contractcode,a.jydsdm from xtyjdfsexx a INNER JOIN xtykhjcxxh b on a.jydsdm = b.khcode and a.sjrq = b.sjrq "+whereStra+" and b.dqdm not like '000%' and a.jydsdmlb not in ('A01','A02','C01','C02','Z99')";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                //境内同业客户的交易对手代码类别应该是统一社会信用代码或者组织机构代码或者SPV代码
                if(checknumList.contains("JS2087")) {
                	String errorinfo  = "境外同业客户的交易对手代码类别不应该是统一社会信用代码或者组织机构代码或者SPV代码";
                    String sqlexist ="select a.id,a.contractcode,a.jydsdm from xtyjdfsexx a INNER JOIN xtykhjcxx b on a.jydsdm = b.khcode and a.sjrq = b.sjrq "+whereStra+" and b.dqdm like '000%' and b.operationname != '申请删除' and b.dqdm != '000156' and a.jydsdmlb in ('A01','A02','C01','C02')";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    
                    errorinfo  = "境外同业客户的交易对手代码类别不应该是统一社会信用代码或者组织机构代码或者SPV代码（历史表）";
                    sqlexist ="select a.id,a.contractcode,a.jydsdm from xtyjdfsexx a INNER JOIN xtykhjcxxh b on a.jydsdm = b.khcode and a.sjrq = b.sjrq "+whereStra+" and b.dqdm like '000%' and b.dqdm != '000156' and  a.jydsdmlb in ('A01','A02','C01','C02')";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
            	resultSet = JDBCUtils.Query(connection, sql);
            	while (true) {
                    if (!resultSet.next()) break;
                     Xtyjdfsexx xtyjdfsexx = new Xtyjdfsexx();
                    Stringutil.getEntity(resultSet, xtyjdfsexx);
                    CheckAllDatatyModule.checkXtyjdfsexx(customSqlUtil, checknumList, xtyjdfsexx, errorMsg, errorId, rightId);
                }
            }
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }finally {
            if (connection!=null){
                JDBCUtils.close();
            }
        }

        //校验结束

        //修改状态 和 下载
        if (errorId!=null && errorId.size()>0){
            //-下载到本地
            StringBuffer errorMsgAll = new StringBuffer("");
            for(Map.Entry<String, String> entry : errorMsg.entrySet()) {
                errorMsgAll.append(entry.getValue()+"\r\n");
            }
            DownloadUtil.downLoad(errorMsgAll.toString(),target+File.separator+"checkout",fileName+".txt");
            errorMsg.clear();

            setMap.put("checkStatus","2");
            setMap.put("operationname"," ");
            customSqlUtil.updateByWhereBatch(tableName,setMap,errorId);
            errorId.clear();
            json.put("msg","0");
        }else {
            if (b){
                json.put("msg","0");
            }else {
                json.put("msg","1");
            }
        }
        if (rightId!=null && rightId.size()>0){
            setMap.put("checkStatus","1");
            setMap.put("operationname"," ");
            customSqlUtil.updateByWhereBatch(tableName,setMap,rightId);
            rightId.clear();
        }

        customSqlUtil.saveLog("同业借贷发生额信息校验成功","校验");
        long b2 = System.currentTimeMillis();
        System.out.println("校验用时： "+(b2-a)/1000+"秒");
        try {
            PrintWriter out = response.getWriter();
            out.write(json.toString());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("XDownLoadCheckXtyjdfsexx")
    public void XDownLoadCheckTwo(HttpServletResponse response) {
        if (StringUtils.isBlank(target)){
            target=suisService.findAll().get(0).getMessagepath();
        }
        DownloadUtil.downLoadCheckFile(response,target + File.separator + "checkout",fileName+".txt");
    }
    
    // 表间校验
    public void inTableCalibration(Map<String,String> setMap,List<String> errorId,Map<String,String> whereMap,LinkedHashMap<String, String> errorMsg,String sql,String errorcode,String errorinfo,String errorcode2) {
        List<Object[]> exist = customSqlUtil.executeQuery(sql);
	    if (exist!=null && exist.size()>0){
	        setMap.put("checkStatus","2");
	        setMap.put("operationname"," ");
            exist.forEach(errId->{
	            errorId.add(String.valueOf(errId[0]));
	            whereMap.put("id", String.valueOf(errId[0]));
                if(!errorMsg.containsKey(String.valueOf(errId[0]))) {
                    errorMsg.put(String.valueOf(errId[0]), errorcode+":"+String.valueOf(errId[1])+"，"+errorcode2+":"+String.valueOf(errId[2])+"]->\r\n");

                }
                String str = errorMsg.get(String.valueOf(errId[0]));
                str = str + errorinfo+"|";
                errorMsg.put(String.valueOf(errId[0]), str);
	        });
            whereMap.clear();
            setMap.clear();
            exist.clear();
	    }
    }
}
