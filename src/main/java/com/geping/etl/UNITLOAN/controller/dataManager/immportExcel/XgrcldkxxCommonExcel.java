package com.geping.etl.UNITLOAN.controller.dataManager.immportExcel;

import com.geping.etl.UNITLOAN.entity.report.Xclgrdkxx;
import com.geping.etl.UNITLOAN.entity.report.Xftykhxbl;
import com.geping.etl.UNITLOAN.entity.report.Xgrkhxx;
import com.geping.etl.UNITLOAN.service.report.XclgrdkxxService;
import com.geping.etl.UNITLOAN.service.report.XgrkhxxService;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.UNITLOAN.util.check.CheckUtil;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.SAXHelper;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author: wangzd
 * @Date: 2020/6/29
 */
public class XgrcldkxxCommonExcel {

	private XclgrdkxxService service;

    private PrintStream output = System.out;

    public StringBuffer msg = new StringBuffer();

    public int count;

    private OPCPackage xlsxPackage;

    private int time1;

    private Sys_UserAndOrgDepartment sys_user;

    private CustomSqlUtil customSqlUtil;

    private Xclgrdkxx t;

    private List<Xclgrdkxx> entityList = new ArrayList<Xclgrdkxx>();

    private Class aClass;

    private String[] headers;

    private String[] values;

    private String[] amountFields;

    private String[] dateFields;

    private String[] rateFields;

    private List<Xftykhxbl> xftykhxblList;

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private static List<String> EXCEL_FORMAT_INDEX_DATE_NYR_STRING = Arrays.asList(
            "m/d/yy", "[$-F800]dddd\\,\\ mmmm\\ dd\\,\\ yyyy",
            "[DBNum1][$-804]yyyy\"年\"m\"月\"d\"日\";@", "yyyy\"年\"m\"月\"d\"日\";@", "yyyy/m/d;@", "yy/m/d;@", "m/d/yy;@",
            "[$-409]d/mmm/yy", "[$-409]dd/mmm/yy;@", "reserved-0x1F", "reserved-0x1E", "mm/dd/yy;@", "yyyy/mm/dd", "d-mmm-yy",
            "[$-409]d\\-mmm\\-yy;@", "[$-409]d\\-mmm\\-yy", "[$-409]dd\\-mmm\\-yy;@", "[$-409]dd\\-mmm\\-yy",
            "[DBNum1][$-804]yyyy\"年\"m\"月\"d\"日\"", "yy/m/d", "mm/dd/yy", "dd\\-mmm\\-yy"
    );

    private static List<String> EXCEL_FORMAT_INDEX_DATE_NYRSFM_STRING = Arrays.asList(
            "yyyy/m/d\\ h:mm;@", "m/d/yy h:mm", "yyyy/m/d\\ h:mm\\ AM/PM",
            "[$-409]yyyy/m/d\\ h:mm\\ AM/PM;@", "yyyy/mm/dd\\ hh:mm:dd", "yyyy/mm/dd\\ hh:mm", "yyyy/m/d\\ h:m", "yyyy/m/d\\ h:m:s",
            "yyyy/m/d\\ h:mm", "m/d/yy h:mm;@", "yyyy/m/d\\ h:mm\\ AM/PM;@"
    );

    public XgrcldkxxCommonExcel(Sys_UserAndOrgDepartment sys_user, OPCPackage xlsxPackage, CustomSqlUtil customSqlUtil, Xclgrdkxx t, String[] headers, String[] values, String[] amountFields, String[] dateFields, List<Xftykhxbl> xftykhxblList, String[] rateFields, int id, XclgrdkxxService xclgrdkxxService) {
        this.sys_user = sys_user;
        this.xlsxPackage = xlsxPackage;
        this.customSqlUtil = customSqlUtil;
        this.t=t;
        this.headers=headers;
        this.values=values;
        this.amountFields=amountFields;
        this.xftykhxblList=xftykhxblList;
        this.dateFields=dateFields;
        this.rateFields=rateFields;
        this.count = id;
        this.service = xclgrdkxxService;
    }

    //读取sheet
    public void processSheet(StylesTable styles, ReadOnlySharedStringsTable strings, XSSFSheetXMLHandler.SheetContentsHandler sheetHandler, InputStream sheetInputStream) throws IOException, ParserConfigurationException, SAXException {
    	DataFormatter formatter = new DataFormatter(){
    		@Override
    		public String formatRawCellContents(double value, int formatIndex, String formatString, boolean use1904Windowing) {
    			if (EXCEL_FORMAT_INDEX_DATE_NYR_STRING.contains(formatString)||EXCEL_FORMAT_INDEX_DATE_NYRSFM_STRING.contains(formatString)) {
    				formatString = "yyyy-MM-dd";
    	        }
    			return super.formatRawCellContents(value, formatIndex, formatString, use1904Windowing);
    		}
		};
        InputSource sheetSource = new InputSource(sheetInputStream);
        try {
            XMLReader sheetParser = SAXHelper.newXMLReader();    //读取XML文件
            ContentHandler handler = new XSSFSheetXMLHandler(styles, null, strings, sheetHandler, formatter, false);  //解析时所接受的对象 StylesTable,CommentsTable,ReadOnlySharedStringsTable,SheetContentsHandler,InputStream,boolean formulasNotResults
            sheetParser.setContentHandler(handler);
            sheetParser.parse(sheetSource);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException("SAX parser appears to be broken - " + e.getMessage());
        }
    }

    public void process(int time) throws Exception {
        ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(this.xlsxPackage);
        XSSFReader xssfreader = new XSSFReader(this.xlsxPackage);    //读取文件
        StylesTable styles = xssfreader.getStylesTable();     //表格样式
        XSSFReader.SheetIterator iter = (XSSFReader.SheetIterator) xssfreader.getSheetsData();
        int index = 0;
        time1 = time;
        while (iter.hasNext()) {   //循环迭代Excel，从第一个sheet开始
            InputStream stream = iter.next();
            String sheetName = iter.getSheetName();  //获取sheet的名字
            this.output.println(sheetName + " [index=" + index + "]:");
            processSheet(styles, strings, new XgrcldkxxCommonExcel.ExcelToCSV(), stream); //访问sheet并读取sheet的内容
            stream.close();//关闭输入流
            ++index;
        }
        
        if(entityList.size() != 0) {
        	service.batchInsert(entityList);
        	entityList.clear();
        	entityList = null;
        }
    }

    public class ExcelToCSV implements XSSFSheetXMLHandler.SheetContentsHandler {
        private int currentRow = -1;                   //开始的行
        private int currentCol = -1;                   //开始的列
        private boolean templateError = true;          //导入的表格模板是否正确
        private int res = 1;                           //导入错误的位置
        private boolean flag = true;				   //标记是否为空行

        List<String> fieldList = new ArrayList<>();

        @Override
        public void startRow(int rowNum) {
        	flag = true;
            currentRow = rowNum;
            currentCol = -1;
            if (currentRow != 0 && time1 == 1) {
                try {
                    t = new Xclgrdkxx();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //xdkfsxx = new Xdkfsxx();
                res++;
            }
        }

        @Override
        public void endRow(int rowNum) {
            if (t != null && templateError && currentRow != 0 && !flag) {
                if (time1 == 1) {
                    String format = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                    String[] baseFields;
                    Object[] baseFieldValues;
                	count++;
                	baseFields=new String[]{"id","checkstatus", "datastatus", "operator", "operationname",
                            "operationtime", "orgid", "departid", "nopassreason",
                    };
                	baseFieldValues=new Object[]{count,"0","0",sys_user.getLoginid()," ",
                            format,sys_user.getOrgid(),sys_user.getDepartid()," "};
                    
                    for (int i = 0; i < baseFields.length; i++) {
                        try {
                            Field declaredField = aClass.getDeclaredField(baseFields[i]);
                            Class<?> type = declaredField.getType();
                            Method method = aClass.getDeclaredMethod("set" + Stringutil.getMethodName(baseFields[i]),type);
                            method.setAccessible(true);
                            method.invoke(t, baseFieldValues[i]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    if(entityList.size() < 100000) {
                    	entityList.add(t);
                    }else {
                    	service.batchInsert(entityList);
                    	entityList.clear();
                    	entityList.add(t);
                    }
                    
                    //customSqlUtil.saveT(t);
                }
            }
        }

        @Override
        public void cell(String cellReference, String formattedValue, XSSFComment comment) {
            try {
                if (StringUtils.isBlank(formattedValue)) {
                    return;
                }
                flag = false;
                //校验第一行表头是否匹配
                if (currentRow == 0 && templateError) {
                    int thisCol = (new CellReference(cellReference)).getCol();
                    if (!Arrays.asList(headers).contains(formattedValue.trim())) {
                        templateError = false;
                        msg.append("导入模板不正确->"+formattedValue);
                        return;
                    }
                    for (int i = 0; i < headers.length; i++) {
                        if (headers[i].equals(formattedValue)) {
                            fieldList.add(values[i]);
                        }
                    }
                } else {
                    //当不是第一行时且模板正确时，就开始读取Excel的值
                    if (currentRow > 0 && templateError && time1 == 1) {
                        if (cellReference == null) {
                            cellReference = new CellAddress(currentRow, currentCol).formatAsString();
                        }
                        int thisCol = (new CellReference(cellReference)).getCol();
                        currentCol = thisCol;
                        if (templateError) {
                            String field = fieldList.get(thisCol);
                            aClass = t.getClass();
                            Field declaredField = aClass.getDeclaredField(field);
                            Class[] parameterTypes = new Class[1];
                            Class<?> type = declaredField.getType();
                            parameterTypes[0] = type;
                            Method method = aClass.getDeclaredMethod("set" + Stringutil.getMethodName(field), parameterTypes);
                            method.setAccessible(true);
                            if (ArrayUtils.contains(amountFields,field)){
                                try {
                                	formattedValue = StringUtils.deleteWhitespace(formattedValue);
                                	formattedValue = formattedValue.replaceAll(",", "");
                                	formattedValue = formattedValue.replaceAll("￥", "");
                                	formattedValue = formattedValue.replaceAll("¥", "");
                                	formattedValue = formattedValue.replaceAll("$", "");
                                	formattedValue = formattedValue.replaceAll("＄", "");
                                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
                                    formattedValue =decimalFormat.format(new BigDecimal(formattedValue));
                                }catch (Exception e){
                                    formattedValue = "";
                                }
                            }
                            if (ArrayUtils.contains(rateFields,field)){
                                try {
                                	formattedValue = StringUtils.deleteWhitespace(formattedValue);
                                	formattedValue = formattedValue.replaceAll(",", "");
                                	formattedValue = formattedValue.replaceAll("￥", "");
                                	formattedValue = formattedValue.replaceAll("¥", "");
                                	formattedValue = formattedValue.replaceAll("$", "");
                                	formattedValue = formattedValue.replaceAll("＄", "");
                                	DecimalFormat decimalFormat = new DecimalFormat("0.00000");
                                    formattedValue =decimalFormat.format(new BigDecimal(formattedValue));
                                }catch (Exception e){
                                    formattedValue = "";
                                }
                            }
                            if (ArrayUtils.contains(dateFields,field)){
                            	if(formattedValue.contains(".")) {
                            		formattedValue = new SimpleDateFormat("yyyy-MM-dd").format(DateUtil.getJavaDate(Double.valueOf(formattedValue)));
                            	}else {
                            		formattedValue = CheckUtil.checkDateDefaultFormat(formattedValue);
                            	}
                                
                            }
                            if (type == Integer.class) {
                                method.invoke(t, Integer.parseInt(formattedValue));
                            } else if (type == BigDecimal.class) {
                                method.invoke(t, new BigDecimal(formattedValue));
                            } else {
                                method.invoke(t, formattedValue);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void headerFooter(String text, boolean isHeader, String tagName) {

        }
    }
}
