package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.entity.report.Xdkfsxx;
import com.geping.etl.UNITLOAN.entity.report.Xftykhxbl;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XdkfsxxService;
import com.geping.etl.UNITLOAN.service.report.XftykhxblService;
import com.geping.etl.UNITLOAN.util.*;
import com.geping.etl.UNITLOAN.util.check.CheckAllData;
import com.geping.etl.UNITLOAN.util.check.CheckAllData2;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.utils.jdbc.JDBCUtils;

import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @Author: wangzd
 * @Date: 17:03 2020/6/9
 */
@RestController
public class XdkfsxxController {

    @Autowired
    private XdkfsxxService xdkfsxxService;

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private ExcelUploadUtil excelUploadUtil;

    @Autowired
    private CustomSqlUtil customSqlUtil;

    @Autowired
    private XftykhxblService ftykhxblService;

    @Autowired
    private XcommonService commonService;


    @Autowired
    private HttpServletRequest request;

    @Autowired
    private XCheckRuleService checkRuleService;

    @Autowired
    private  XDataBaseTypeUtil dataBaseTypeUtil;
    
    @Autowired
    private DepartUtil departUtil;

    private Sys_UserAndOrgDepartment sys_user;

    private final String tableName="xdkfsxx";

    private final String fileName="单位贷款发生额信息";

    private static String target="";

    //查询数据
    @PostMapping("XGetXdkfsxxData")
    public ResponseResult XGetXdkfsxxData(int page, int rows,String finorgcodeParam,String loancontractcodeParam,String operationnameParam,String loanbrowcodeParam,
       String browidcodeParam,String isfarmerloanParam,String checkstatusParam,String datastatus){
        page = page - 1;
        sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");

        String departId = departUtil.getDepart(sys_user);
        Specification specification=new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                //待提交
                predicates.add(criteriaBuilder.equal(root.get("datastatus"), datastatus));

                predicates.add(criteriaBuilder.like(root.get("departid"), departId));
                if (StringUtils.isNotBlank(finorgcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("finorgcode"), "%"+finorgcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(loancontractcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("loancontractcode"), "%"+loancontractcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(loanbrowcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("loanbrowcode"), "%"+loanbrowcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(browidcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("browidcode"), "%"+browidcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(isfarmerloanParam)){
                    predicates.add(criteriaBuilder.like(root.get("isfarmerloan"), "%"+isfarmerloanParam+"%"));
                }
                if (StringUtils.isNotBlank(checkstatusParam)){
                    predicates.add(criteriaBuilder.equal(root.get("checkstatus"), checkstatusParam));
                }
                if (StringUtils.isNotBlank(operationnameParam)){
                    predicates.add(criteriaBuilder.equal(root.get("operationname"), operationnameParam));
                }
                predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
        PageRequest pageRequest = new PageRequest(page, rows, sort);
        Page<Xdkfsxx> all = xdkfsxxService.findAll(specification, pageRequest);
        List<Xdkfsxx> content = all.getContent();
        long totalCount = all.getTotalElements();
        return ResponseResult.success(totalCount,content);
    }

    //导入
    @PostMapping(value = "XimmportExcelTwo",produces = "text/plain;charset=UTF-8")
    public void XimmportExcelTwo(HttpServletRequest request,HttpServletResponse response){
        sys_user=((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        File uploadFile = null;
        OPCPackage opcPackage=null;
        PrintWriter out = null;
        Boolean aBoolean=false;
        try {
            out = response.getWriter();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multiRequest.getFile("excelfile");
            JSONObject json = new JSONObject();
            if (file == null) {
                json.put("msg", "导入文件为空");
            } else {
                uploadFile = excelUploadUtil.uploadFile(file);
                opcPackage = OPCPackage.open(uploadFile);
                String[] headers = {"金融机构代码","内部机构号", "金融机构地区代码", "借款人证件类型", "借款人证件代码", "借款人国民经济部门",
                        "借款人行业", "借款人地区代码", "借款人经济成分", "借款人企业规模", "贷款借据编码", "贷款合同编码", "贷款产品类别",
                        "贷款实际投向", "贷款发放日期", "贷款到期日期", "贷款实际终止日期", "币种", "贷款发生金额", "贷款发生金额折人民币",
                        "利率是否固定", "利率水平", "贷款定价基准类型", "基准利率", "贷款财政扶持方式", "贷款利率重新定价日", "贷款担保方式",
                        "是否首次贷款", "贷款状态", "资产证券化产品代码", "贷款重组方式", "发放/收回标识", "交易流水号", "贷款用途","客户号码","数据日期"
                };
                String[] values = {"finorgcode", "finorgincode", "finorgareacode", "isfarmerloan", "browidcode", "isgreenloan",
                		"browinds", "browareacode","entpczjjcf", "entpmode", "loanbrowcode", "loancontractcode", "loanprocode", 
                		"loanactdect", "loanstartdate", "loanenddate", "loanactenddate", "loancurrency", "loanamt", "loancnyamt",
                		"rateisfix", "ratelevel", "loanfixamttype", "rate", "loanfinancesupport", "loanraterepricedate", "gteemethod",
                		"isplatformloan", "loanstatus", "assetproductcode", "loanrestructuring", "givetakeid", "transactionnum", "issupportliveloan", "customernum","sjrq"
                };
                String[] rateFields = {"rate","ratelevel"};
                String[] amountFields = {"loanamt","loancnyamt"};
                String[] dateFields = {"loanstartdate", "loanenddate", "loanactenddate","loanraterepricedate","sjrq"};
                List<Xftykhxbl> xftykhxblList = ftykhxblService.findAll();
                if (xftykhxblList.size()>0){
                    aBoolean = knowledgeIsRepeat(xftykhxblList);
                }
                XCommonExcel commonExcel = new XCommonExcel(sys_user, opcPackage, customSqlUtil, new Xdkfsxx(),headers,values, amountFields,dateFields, xftykhxblList,rateFields);
                commonExcel.process(0);
                String departId = departUtil.getDepart(sys_user);
                if (commonExcel.msg.toString().length() == 0) {
                	commonService.importDelete(tableName,departId);
                    commonExcel.process(1);
                    json.put("msg","导入成功");
                }else {
                    json.put("msg",commonExcel.msg.toString());
                }
                if (aBoolean && json.getString("msg").equals("导入成功")){
                    json.put("msg","补录表中有相同客户号码的数据");
                }


            }
            String logContext="导入成功".equals(json.getString("msg")) || "补录表中有相同客户号码的数据".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
            customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
            out.write(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                opcPackage.close();
                //将上传的文件删除
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    //校验
    @PostMapping("XCheckDataTwo")
    public void XCheckDataTwo(HttpServletRequest request, HttpServletResponse response,String finorgcodeParam,String loancontractcodeParam,String loanbrowcodeParam){
        long a = System.currentTimeMillis();
        //记录是否有错
        boolean b=false;
        boolean sqlServer = dataBaseTypeUtil.equalsSqlServer();
        sys_user = ((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        List<SUpOrgInfoSet> sUpOrgInfoSets = suisService.findAll();
        target=sUpOrgInfoSets.get(0).getMessagepath();
        String departId = departUtil.getDepart(sys_user);

        JSONObject json = new JSONObject();
        String id = request.getParameter("id");

        LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
        List<String> errorId = new ArrayList<>();
        List<String> rightId = new ArrayList<>();
        Map<String,String> whereMap=new HashMap<>();
        Map<String,String> setMap=new HashMap<>();
        setMap.put("operator",sys_user.getLoginid());
        setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//        List<String> splitList = Arrays.asList(id.split(","));//返回固定长度的ArrayList
        String whereStr=" where datastatus='0' and checkstatus='0' ";
        String whereStra=" where a.datastatus='0' and a.checkstatus='0' ";
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            whereStr = whereStr + " and id in(" + id + ")";
            whereStra = whereStra + " and a.id in(" + id + ")";
        }else {
            if (StringUtils.isNotBlank(finorgcodeParam)){
                whereStr = whereStr + " and finorgcode like '%"+ finorgcodeParam +"%'";
                whereStra = whereStra + " and a.finorgcode like '%"+ finorgcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(loancontractcodeParam)){
                whereStr = whereStr + " and loancontractcode like '%"+ loancontractcodeParam +"%'";
                whereStra = whereStra + " and a.loancontractcode like '%"+ loancontractcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(loanbrowcodeParam)){
                whereStr = whereStr + " and loanbrowcode like '%"+ loanbrowcodeParam +"%'";
                whereStra = whereStra + " and a.loanbrowcode like '%"+ loanbrowcodeParam +"%'";
            }
            whereStr = whereStr + " and departid like '%"+ departId +"%'";
            whereStra = whereStra + " and a.departid like '%"+ departId +"%'";
        }
        String sql="select * from xdkfsxx "+whereStr;
        Connection connection = null;
        ResultSet resultSet=null;
        
        try {
            connection = JDBCUtils.getConnection();
            resultSet = JDBCUtils.Query(connection, sql);
            if (!resultSet.next()) {	//没有可校验的数据
            	json.put("msg","-1");
                try {
                    PrintWriter out = response.getWriter();
                    out.write(json.toString());
                    out.flush();
                    out.close();
                } catch (IOException e) {
                	e.printStackTrace();
                }
                return;
            }else {
                List<Object[]> repeat1 =  new ArrayList<>();
                List<Object[]> repeat2 =  new ArrayList<>();
            	//在校验之前查重
                //标识名称
                String errorcode = "贷款合同编码";
                String errorcode2="贷款借据编号";


                //校验开关集合
                List<String> checknumList = checkRuleService.getChecknum("xdkfsxx");

                if (checknumList.contains("JS2302")){
                    if (sqlServer){
                        String errorinfo  = "数据日期+金融机构代码+贷款借据编码+交易流水号不唯一";
                        String sqlexist ="select id,loancontractcode,loanbrowcode from xdkfsxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,finorgincode,loanbrowcode,transactionnum from xdkfsxx where operationname != '申请删除' group by sjrq,finorgincode,loanbrowcode,transactionnum having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.finorgincode = t2.finorgincode and t1.loanbrowcode = t2.loanbrowcode and t1.transactionnum = t2.transactionnum)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                        errorinfo = "数据日期+金融机构代码+贷款借据编码+交易流水号不唯一（历史表）";
                        sqlexist ="select id,loancontractcode,loanbrowcode from xdkfsxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,finorgincode,loanbrowcode,transactionnum from xdkfsxxh group by sjrq,finorgincode,loanbrowcode,transactionnum having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.finorgincode = t2.finorgincode  and t1.loanbrowcode = t2.loanbrowcode and t1.transactionnum = t2.transactionnum)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }else {
                        String errorinfo  = "数据日期+金融机构代码+贷款借据编码+交易流水号不唯一";
                        String sqlexist ="select id,loancontractcode,loanbrowcode from xdkfsxx "+whereStr+" and (sjrq,finorgincode,loanbrowcode,transactionnum) in (select sjrq,finorgincode,loanbrowcode,transactionnum from xdkfsxx where operationname != '申请删除' group by sjrq,finorgincode,loanbrowcode,transactionnum having  count(1) > 1)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                        errorinfo = "数据日期+金融机构代码+贷款借据编码+交易流水号不唯一（历史表）";
                        sqlexist = "select id,loancontractcode,loanbrowcode from xdkfsxx "+whereStr+" and (sjrq,finorgincode,loanbrowcode,transactionnum) in (select sjrq,finorgincode,loanbrowcode,transactionnum from xdkfsxxh group by sjrq,finorgincode,loanbrowcode,transactionnum having  count(1) > 0)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }
                }



                // 表间校验
                // 1.贷款产品类别与存量单位贷款信息的贷款产品类别应该一致(单位贷款发生额信息 a AND 存量单位贷款信息 b)
                if (checknumList.contains("JS1689")){
                    String errorinfo = "贷款产品类别与存量单位贷款信息的贷款产品类别应该一致";
                    String sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN  xcldkxx b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.loanprocode != b.productcetegory  and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    errorinfo = "贷款产品类别与存量单位贷款信息的贷款产品类别应该一致（历史表）";
                    sqlexist ="select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN  xcldkxxh b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.loanprocode != b.productcetegory";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                // 9.借款人证件代码与存量单位贷款信息的借款人证件代码应该一致(单位贷款发生额信息 a AND 存量单位贷款信息 b)
                if (checknumList.contains("JS1685")){
                    String  errorinfo = "借款人证件代码与存量单位贷款信息的借款人证件代码应该一致";
                    String  sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN xcldkxx b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.browidcode != b.brroweridnum and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    errorinfo = "借款人证件代码与存量单位贷款信息的借款人证件代码应该一致（历史表）";
                    sqlexist ="select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN xcldkxxh b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.browidcode != b.brroweridnum";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                // 11.借款人证件类型与存量单位贷款信息的借款人证件类型应该一致(单位贷款发生额信息 a AND 存量单位贷款信息 b)
                if (checknumList.contains("JS1684")){
                    String errorinfo = "借款人证件类型与存量单位贷款信息的借款人证件类型应该一致";
                    String sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN xcldkxx  b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.isfarmerloan != b.isfarmerloan and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    errorinfo = "借款人证件类型与存量单位贷款信息的借款人证件类型应该一致（历史表）";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN xcldkxxh b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.isfarmerloan != b.isfarmerloan";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
            	
                // 2.当担保合同信息中担保人证件代码不为空时，贷款担保方式应该为保证或组合(单位贷款发生额信息 a AND 担保合同信息 b)
//                errorinfo = "当担保合同信息中担保人证件代码不为空时，贷款担保方式应该为保证或组合";
//                sqlexist = "select a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN (select * from xdkdbht group by loancontractcode,sjrq,financeorginnum) b on a.loancontractcode = b.loancontractcode and a.finorgincode = b.financeorginnum and a.sjrq = b.sjrq "+whereStra+" and (a.gteemethod not like 'C%' and a.gteemethod not like 'E%') and (b.gteeidnum is not null and ltrim(b.gteeidnum) != '') and b.operationname != '申请删除'";
//                inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
//
//                errorinfo = "当担保合同信息中担保人证件代码不为空时，贷款担保方式应该为保证或组合（历史表）";
//                sqlexist = "select a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN (select * from xdkdbhth group by loancontractcode,sjrq,financeorginnum) b on a.loancontractcode = b.loancontractcode and a.finorgincode = b.financeorginnum and a.sjrq = b.sjrq "+whereStra+" and (a.gteemethod not like 'C%' and a.gteemethod not like 'E%') and (b.gteeidnum is not null and ltrim(b.gteeidnum) != '')";
//                inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
//
//                // 3.当担保合同信息中担保人证件类型不为空时，贷款担保方式应该为保证或组合(单位贷款发生额信息 a AND 担保合同信息 b)
//                errorinfo = "当担保合同信息中担保人证件类型不为空时，贷款担保方式应该为保证或组合";
//                sqlexist = "select a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN (select * from xdkdbht group by loancontractcode,sjrq,financeorginnum) b on a.loancontractcode = b.loancontractcode and a.finorgincode = b.financeorginnum and a.sjrq = b.sjrq "+whereStra+" and (a.gteemethod not like 'C%' and a.gteemethod not like 'E%') and (b.gteeidtype is not null and ltrim(b.gteeidtype) != '') and b.operationname != '申请删除'";
//                inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
//
//                errorinfo = "当担保合同信息中担保人证件类型不为空时，贷款担保方式应该为保证或组合（历史表）";
//                sqlexist = "select a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN (select * from xdkdbhth group by loancontractcode,sjrq,financeorginnum) b on a.loancontractcode = b.loancontractcode and a.finorgincode = b.financeorginnum and a.sjrq = b.sjrq "+whereStra+" and (a.gteemethod not like 'C%' and a.gteemethod not like 'E%') and (b.gteeidtype is not null and ltrim(b.gteeidtype) != '')";
//                inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                
                // 4.借款人地区代码与非同业单位客户基础信息的地区代码应该一致(单位贷款发生额信息 a AND 非同业单位客户基础信息 b)


                if (checknumList.contains("JS1687")){
                    String errorinfo = "借款人地区代码与非同业单位客户基础信息的地区代码应该一致";
                    String sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN  xftykhx b on a.browidcode = b.customercode and a.sjrq = b.sjrq "+whereStra+" and a.browareacode != b.regareacode and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    errorinfo = "借款人地区代码与非同业单位客户基础信息的地区代码应该一致（历史表）";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN  xftykhxh b on a.browidcode = b.customercode and a.sjrq = b.sjrq "+whereStra+" and a.browareacode != b.regareacode";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                // 5.借款人国民经济部门与非同业单位客户基础信息的客户国民经济部门应该一致(单位贷款发生额信息 a AND 非同业单位客户基础信息 b)
                if (checknumList.contains("JS1821")){
                    String errorinfo = "借款人国民经济部门与非同业单位客户基础信息的客户国民经济部门应该一致";
                    String sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN  xftykhx b on a.browidcode = b.customercode and a.sjrq = b.sjrq "+whereStra+" and a.isgreenloan != b.actamtcreny and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    errorinfo = "借款人国民经济部门与非同业单位客户基础信息的客户国民经济部门应该一致（历史表）";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN  xftykhxh b on a.browidcode = b.customercode and a.sjrq = b.sjrq "+whereStra+" and a.isgreenloan != b.actamtcreny";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                // 6.借款人行业与非同业单位客户基础信息的所属行业应该一致(单位贷款发生额信息 a AND 非同业单位客户基础信息 b)
                if (checknumList.contains("JS1686")){
                    String errorinfo = "借款人行业与非同业单位客户基础信息的所属行业应该一致";
                    String sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN xftykhx  b on a.browidcode = b.customercode and a.sjrq = b.sjrq "+whereStra+" and a.browinds != b.industry and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    errorinfo = "借款人行业与非同业单位客户基础信息的所属行业应该一致（历史表）";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN  xftykhxh b on a.browidcode = b.customercode and a.sjrq = b.sjrq "+whereStra+" and a.browinds != b.industry";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                // 7.当借款人经济成分不为空时，借款人经济成分与非同业单位客户基础信息的客户经济成分应该一致(单位贷款发生额信息 a AND 非同业单位客户基础信息 b)
                if (checknumList.contains("JS1793")){
                    String errorinfo = "当借款人经济成分不为空时，借款人经济成分与非同业单位客户基础信息的客户经济成分应该一致";
                    String sqlexist ="select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN  xftykhx b on a.browidcode = b.customercode and a.sjrq = b.sjrq "+whereStra+" and a.entpczjjcf != b.entpczjjcf and a.entpczjjcf is not null and b.entpczjjcf is not null and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    errorinfo = "当借款人经济成分不为空时，借款人经济成分与非同业单位客户基础信息的客户经济成分应该一致（历史表）";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN xftykhxh b on a.browidcode = b.customercode and a.sjrq = b.sjrq "+whereStra+" and a.entpczjjcf != b.entpczjjcf and a.entpczjjcf is not null and b.entpczjjcf is not null";
//                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN xftykhxh b on a.browidcode = b.customercode and a.sjrq = b.sjrq "+whereStra+" and a.entpczjjcf != b.entpczjjcf and (a.entpczjjcf is not null and ltrim(a.entpczjjcf) != '') and (b.entpczjjcf is not null and ltrim(b.entpczjjcf) != '')";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
                // 8.当借款人企业规模不为空时，借款人企业规模与非同业单位客户基础信息的企业规模应该一致(单位贷款发生额信息 a AND 非同业单位客户基础信息 b)
                if (checknumList.contains("JS1688")){
                    String errorinfo = "当借款人企业规模不为空时，借款人企业规模与非同业单位客户基础信息的企业规模应该一致";
                    String sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN xftykhx b on a.browidcode = b.customercode and a.sjrq = b.sjrq "+whereStra+" and a.entpmode != b.entpmode and a.entpmode is not null and b.entpmode is not null and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                    errorinfo = "当借款人企业规模不为空时，借款人企业规模与非同业单位客户基础信息的企业规模应该一致（历史表）";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a INNER JOIN xftykhxh b on a.browidcode = b.customercode and a.sjrq = b.sjrq "+whereStra+" and a.entpmode != b.entpmode and a.entpmode is not null and b.entpmode is not null";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }
                
//                // TODO
//                // 10.借款人证件代码应该在非同业单位客户基础信息.客户证件代码中存在(单位贷款发生额信息 a AND 非同业单位客户基础信息 b)
                if (checknumList.contains("JS1892")){
                    String errorinfo = "借款人证件代码应该在非同业单位客户基础信息.客户证件代码中存在";
                    String sqlexist = "select a.id,a.loancontractcode,a.loanbrowcode from xdkfsxx a "+whereStra+" and a.isgreenloan not like 'B%' and a.isgreenloan != 'E03' and not exists (select 1 from xftykhx b where a.sjrq = b.sjrq and a.browidcode = b.customercode and b.operationname != '申请删除') and not exists (select 1 from xftykhxh b where a.sjrq = b.sjrq and a.browidcode = b.customercode)";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }


                // 12.金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致(单位贷款发生额信息 a AND 金融机构（分支机构）基础信息 b)

//                errorinfo = "金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致";
//                sqlexist = "select a.id,a.loancontractcode from xdkfsxx a INNER JOIN xjrjgfz b on a.finorgincode = b.inorgnum and a.sjrq = b.sjrq "+whereStra+" and a.finorgcode != b.finorgcode";
//                inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//
//                errorinfo = "金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致（历史表）";
//                sqlexist = "select a.id,a.loancontractcode from xdkfsxx a INNER JOIN xjrjgfzh b on a.finorgincode = b.inorgnum and a.sjrq = b.sjrq "+whereStra+" and a.finorgcode != b.finorgcode";
//                inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);

                
            	resultSet = JDBCUtils.Query(connection, sql);
            	while (true) {
                    if (!resultSet.next()) break;
                    Xdkfsxx xdkfsxx = new Xdkfsxx();
                    Stringutil.getEntity(resultSet, xdkfsxx);
                    CheckAllData2.checkXdkfsxx(customSqlUtil,checknumList, xdkfsxx, errorMsg, errorId, rightId);
                }
            }
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }finally {
            if (connection!=null){
                JDBCUtils.close();
            }
            if(CheckAllData2.fzList != null) {
                CheckAllData2.fzList.clear();
                CheckAllData2.fzList = null;
            }
            if(CheckAllData2.frList != null) {
                CheckAllData2.frList.clear();
                CheckAllData2.frList = null;
            }
        }

        //校验结束

        //修改状态 和 下载
        if (errorId!=null && errorId.size()>0){
            //-下载到本地
            StringBuffer errorMsgAll = new StringBuffer("");
            for(Map.Entry<String, String> entry : errorMsg.entrySet()) {
                errorMsgAll.append(entry.getValue()+"\r\n");
            }
            DownloadUtil.downLoad(errorMsgAll.toString(),target+File.separator+"checkout",fileName+".txt");
            errorMsg.clear();

            setMap.put("checkStatus","2");
            setMap.put("operationname"," ");
            customSqlUtil.updateByWhereBatch(tableName,setMap,errorId);
            errorId.clear();
            json.put("msg","0");
        }else {
            if (b){
                json.put("msg","0");
            }else {
                json.put("msg","1");
            }
        }
        if (rightId!=null && rightId.size()>0){
            setMap.put("checkStatus","1");
            setMap.put("operationname"," ");
            customSqlUtil.updateByWhereBatch(tableName,setMap,rightId);
            rightId.clear();
        }

        customSqlUtil.saveLog("单位贷款发生额校验成功","校验");
        long b2 = System.currentTimeMillis();
        System.out.println("校验用时： "+(b2-a)/1000+"秒");
        try {
            PrintWriter out = response.getWriter();
            out.write(json.toString());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("XDownLoadCheckTwo")
    public void XDownLoadCheckTwo(HttpServletResponse response) {
        if (StringUtils.isBlank(target)){
            target=suisService.findAll().get(0).getMessagepath();
        }
        DownloadUtil.downLoadCheckFile(response,target + File.separator + "checkout",fileName+".txt");
    }
    
    // 表间校验
    public void inTableCalibration(Map<String,String> setMap,List<String> errorId,Map<String,String> whereMap,LinkedHashMap<String, String> errorMsg,String sql,String errorcode,String errorinfo,String errorcode2) {
        List<Object[]> exist = customSqlUtil.executeQuery(sql);
	    if (exist!=null && exist.size()>0){
	        setMap.put("checkStatus","2");
	        setMap.put("operationname"," ");
            exist.forEach(errId->{
	            errorId.add(String.valueOf(errId[0]));
	            whereMap.put("id", String.valueOf(errId[0]));
                if(!errorMsg.containsKey(String.valueOf(errId[0]))) {
                    errorMsg.put(String.valueOf(errId[0]), errorcode+":"+String.valueOf(errId[1])+"，"+errorcode2+":"+String.valueOf(errId[2])+"]->\r\n");

                }
                String str = errorMsg.get(String.valueOf(errId[0]));
                str = str + errorinfo+"|";
                errorMsg.put(String.valueOf(errId[0]), str);
	        });
            whereMap.clear();
            setMap.clear();
            exist.clear();
	    }
    }

    //lsit数据字段重复 返回true
    private Boolean knowledgeIsRepeat(List<Xftykhxbl> orderList) {
        Set<Xftykhxbl> set = new TreeSet<Xftykhxbl>(new Comparator<Xftykhxbl>() {
            public int compare(Xftykhxbl a, Xftykhxbl b) {
                if (StringUtils.isNotBlank(a.getCustomernum())&&StringUtils.isNotBlank(b.getCustomernum())){
                    return a.getCustomernum().compareTo(b.getCustomernum());
                }

                return 100000;
            }
        });
        set.addAll(orderList);
        if (set.size() < orderList.size()) {
            return true;
        }
        return false;
    }
}
