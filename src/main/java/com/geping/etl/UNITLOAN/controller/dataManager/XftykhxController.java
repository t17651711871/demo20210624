package com.geping.etl.UNITLOAN.controller.dataManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.entity.report.Xftykhx;
import com.geping.etl.UNITLOAN.entity.report.Xftykhxbl;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XftykhxService;
import com.geping.etl.UNITLOAN.util.CalcCountUtil;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.DownloadUtil;
import com.geping.etl.UNITLOAN.util.ExcelUploadUtil;
import com.geping.etl.UNITLOAN.util.ResponseResult;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.UNITLOAN.util.XDataBaseTypeUtil;
import com.geping.etl.UNITLOAN.util.check.CheckAllData3;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.utils.jdbc.JDBCUtils;

import net.sf.json.JSONObject;

/**
 * @Author: wangzd
 * @Date: 17:03 2020/6/9
 */
@RestController
public class XftykhxController {

    @Autowired
    private XftykhxService xftykhxService;

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private ExcelUploadUtil excelUploadUtil;

    @Autowired
    private CustomSqlUtil customSqlUtil;

    @Autowired
    private CalcCountUtil countUtil;

    @Autowired
    private HttpServletRequest request;
    
    @Autowired
    private XDataBaseTypeUtil xDataBaseTypeUtil;
    
    @Autowired
    private XCheckRuleService xCheckRuleService;
    
    @Autowired
    private XcommonService commonService;
    
    @Autowired
    private DepartUtil departUtil;

    private Sys_UserAndOrgDepartment sys_user;
    
    private final String tableName="xftykhx";

    private final String fileName="非同业单位客户基础信息";

    private static String target="";
    //查询数据
    @PostMapping("XGetXftykhxData")
    public ResponseResult XGetXftykhxData(int page, int rows,String finorgcodeParam,String customernameParam,String customercodeParam,String regamtcrenyParam,String operationnameParam,String checkstatusParam,String datastatus){
        page = page - 1;
        sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
        String departId = departUtil.getDepart(sys_user);
        Specification specification=new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("datastatus"), datastatus));

                predicates.add(criteriaBuilder.like(root.get("departid"), departId));
                if (StringUtils.isNotBlank(finorgcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("finorgcode"), "%"+finorgcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(customernameParam)){
                    predicates.add(criteriaBuilder.like(root.get("customername"), "%"+customernameParam+"%"));
                }
                if (StringUtils.isNotBlank(customercodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("customercode"), "%"+customercodeParam+"%"));
                }
                if (StringUtils.isNotBlank(regamtcrenyParam)){
                    predicates.add(criteriaBuilder.like(root.get("regamtcreny"), "%"+regamtcrenyParam+"%"));
                }
                if (StringUtils.isNotBlank(checkstatusParam)){
                    predicates.add(criteriaBuilder.equal(root.get("checkstatus"), checkstatusParam));
                }
                if (StringUtils.isNotBlank(operationnameParam)){
                    predicates.add(criteriaBuilder.equal(root.get("operationname"), operationnameParam));
                }
                predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        PageRequest pageRequest = new PageRequest(page, rows, sort);
        Page<Xftykhx> all = xftykhxService.findAll(specification, pageRequest);
        List<Xftykhx> content = all.getContent();
        long totalCount = all.getTotalElements();
        return ResponseResult.success(totalCount,content);
    }

    //导入
    @PostMapping(value = "XimmportExcelSix",produces = "text/plain;charset=UTF-8")
    public void XimmportExcelSix(HttpServletRequest request,HttpServletResponse response){
    	sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        File uploadFile = null;
        OPCPackage opcPackage=null;
        PrintWriter out = null;
        try {
            out = response.getWriter();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multiRequest.getFile("excelfile");
            JSONObject json = new JSONObject();
            if (file == null) {
                json.put("msg", "导入文件为空");
            } else {
                uploadFile = excelUploadUtil.uploadFile(file);
                opcPackage = OPCPackage.open(uploadFile);
                String[] headers = {"金融机构代码", "客户名称", "客户证件代码", "基本存款账号",
                        "基本账户开户行名称", "注册资本", "客户证件类型", "实收资本", "客户国民经济部门", "总资产", "经营范围", "是否上市公司",
                        "首次建立信贷关系日期", "从业人员数", "注册地址", "地区代码", "营业收入","客户信用级别总等级数",
                        "经营状态", "成立日期", "所属行业", "企业规模", "客户经济成分", "授信额度",
                        "已用额度", "是否关联方", "客户信用评级", "实际控制人证件类型", "实际控制人证件代码","数据日期"
                };
                String[] values = {"finorgcode", "customername", "customercode", "depositnum", "countopenbankname",
                        "regamt", "regamtcreny", "actamt", "actamtcreny", "totalamt", "netamt",
                        "islistcmpy", "fistdate", "jobpeoplenum", "regarea", "regareacode", "workarea",
                        "workareacode", "mngmestus", "setupdate", "industry", "entpmode", "entpczjjcf",
                        "creditamt", "usedamt", "isrelation", "actctrltype", "actctrlidtype", "actctrlidcode","sjrq"
                };
                String[] amountFields = {"regamt","workarea","actamt","creditamt","usedamt","totalamt"};
                String[] dateFields = {"fistdate", "setupdate","sjrq"};
                List<Xftykhxbl> xftykhxblList = new ArrayList<>();
                XCommonExcel<Xftykhx> commonExcel = new XCommonExcel<>(sys_user, opcPackage, customSqlUtil, new Xftykhx(), headers, values, amountFields,dateFields, xftykhxblList,null);
                commonExcel.process(0);
                String departId = departUtil.getDepart(sys_user);
                if (commonExcel.msg.toString().length() == 0) {
                	commonService.importDelete(tableName,departId);
                    commonExcel.process(1);
                    json.put("msg","导入成功");
                    //countUtil.handleCount("xftykhx",0,"add",commonExcel.count);//更新数量
                } else {
                    json.put("msg",commonExcel.msg.toString());
                }
            }
            String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
            customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
            out.write(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                opcPackage.close();
                //将上传的文件删除
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    //校验
    @PostMapping("XCheckDataSix")
    public void XCheckDataSix(HttpServletRequest request, HttpServletResponse response,String finorgcodeParam,String customernameParam){
        //记录是否有错
        boolean b=false;
        sys_user=(Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
        List<SUpOrgInfoSet> sUpOrgInfoSets = suisService.findAll();
        target=sUpOrgInfoSets.get(0).getMessagepath();
        String departId = departUtil.getDepart(sys_user);

        JSONObject json = new JSONObject();
        String id = request.getParameter("id");

        LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
        List<String> errorId = new ArrayList<>();
        List<String> rightId = new ArrayList<>();
        Map<String,String> whereMap=new HashMap<>();
        Map<String,String> setMap=new HashMap<>();
        setMap.put("operator",sys_user.getLoginid());
        setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//        List<String> splitList = Arrays.asList(id.split(","));//返回固定长度的ArrayList
        String whereStr=" where datastatus='0' and checkstatus='0' ";
        String whereStr2=" where a.datastatus='0' and a.checkstatus='0' ";
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            whereStr = whereStr + " and id in(" + id + ")";
            whereStr2 = whereStr2 + " and a.id in(" + id + ")";
        }else{
            if (StringUtils.isNotBlank(finorgcodeParam)){
                whereStr = whereStr + " and finorgcode like '%"+ finorgcodeParam +"%'";
                whereStr2 = whereStr2 + " and a.finorgcode like '%"+ finorgcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(customernameParam)){
                whereStr = whereStr + " and customername like '%"+ customernameParam +"%'";
                whereStr2 = whereStr2 + " and a.customername like '%"+ customernameParam +"%'";
            }
            whereStr = whereStr + " and departid like '%"+ departId +"%'";
            whereStr2 = whereStr2 + " and a.departid like '%"+ departId +"%'";
        }
        String sql="select * from "+tableName+" "+whereStr;
        Connection connection = null;
        ResultSet resultSet=null;

        try {
        	connection = JDBCUtils.getConnection();
            resultSet = JDBCUtils.Query(connection, sql);
            if (!resultSet.next()) {	//没有可校验的数据
            	json.put("msg","-1");
                try {
                    PrintWriter out = response.getWriter();
                    out.write(json.toString());
                    out.flush();
                    out.close();
                } catch (IOException e) {
                	e.printStackTrace();
                }
                return;
            }else {
            	
            
        //标识名称
        String[] errorcode = new String[] {"客户名称","客户证件代码"};
        String sqlexist;
        String errorinfo;
        
        List<String> checknumList = xCheckRuleService.getChecknum("xftykhx");
        
        if(xDataBaseTypeUtil.equalsMySql()) {
        	if(checknumList.contains("JS2311")) {
        	//数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
            sqlexist = "select id,customername,customercode from xftykhx "+whereStr+" and "
            		+ "(CONCAT(ifnull(sjrq,' '),ifnull(regamtcreny,' '),ifnull(customercode,' '))) in (select CONCAT(ifnull(sjrq,' '),ifnull(regamtcreny,' '),ifnull(customercode,' ')) from xftykhx where operationname != '申请删除' group by sjrq,regamtcreny,customercode having count(1) > 1)";
            errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
            sqlexist = "select id,customername,customercode from xftykhx "+whereStr+" and "
            		+ "(CONCAT(ifnull(sjrq,' '),ifnull(regamtcreny,' '),ifnull(customercode,' '))) in (select CONCAT(ifnull(sjrq,' '),ifnull(regamtcreny,' '),ifnull(customercode,' ')) from xftykhxh group by sjrq,regamtcreny,customercode having count(1) > 0)";
            errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS1269")) {
            //当客户有存续的贷款时，首次建立信贷关系日期不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ " and exists(select 1 from xcldkxx b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and (a.fistdate is null or ltrim(a.fistdate) = '') and b.operationname != '申请删除')";
            errorinfo = "当客户有存续的贷款时，首次建立信贷关系日期不能为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当客户有存续的贷款时，首次建立信贷关系日期不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ " and exists(select 1 from xcldkxxh b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and (a.fistdate is null or ltrim(a.fistdate) = ''))";
            errorinfo = "当客户有存续的贷款时，首次建立信贷关系日期不能为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS1269")) {
            //当客户有贷款发生额时，首次建立信贷关系日期不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ " and exists(select 1 from xdkfsxx b where a.customercode = b.browidcode and a.sjrq = b.sjrq and (a.fistdate is null or ltrim(a.fistdate) = '')  and b.operationname != '申请删除')";
            errorinfo = "当客户有贷款发生额时，首次建立信贷关系日期不能为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当客户有贷款发生额时，首次建立信贷关系日期不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ " and exists(select 1 from xdkfsxxh b where a.customercode = b.browidcode and a.sjrq = b.sjrq and (a.fistdate is null or ltrim(a.fistdate) = ''))";
            errorinfo = "当客户有贷款发生额时，首次建立信贷关系日期不能为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS1278")) {
            //当客户有存续的贷款业务时，授信额度不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xcldkxx b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and (a.creditamt is null or ltrim(a.creditamt) = '') and b.operationname != '申请删除')";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xcldkxx where operationname != '申请删除' group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and (a.creditamt is null or ltrim(a.creditamt) = '') and b.operationname != '申请删除'";
            errorinfo = "当客户有存续的贷款业务时，授信额度不能为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当客户有存续的贷款业务时，授信额度不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xcldkxxh b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and (a.creditamt is null or ltrim(a.creditamt) = ''))";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhxh a inner join (select * from xcldkxxh group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and a.creditamt is null or ltrim(a.creditamt) = ''";
            errorinfo = "当客户有存续的贷款业务时，授信额度不能为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS1279")) {
            //当客户有存续的贷款业务时，已用额度不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xcldkxx b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and (a.usedamt is null or ltrim(a.usedamt) = '') and b.operationname != '申请删除')";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xcldkxx where operationname != '申请删除' group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and (a.usedamt is null or ltrim(a.usedamt) = '') and b.operationname != '申请删除'";
            errorinfo = "当客户有存续的贷款业务时，已用额度不能为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当客户有存续的贷款业务时，已用额度不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xcldkxxh b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and (a.usedamt is null or ltrim(a.usedamt) = ''))";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhxh a inner join (select * from xcldkxxh group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and a.usedamt is null or ltrim(a.usedamt) = ''";
            errorinfo = "当客户有存续的贷款业务时，已用额度不能为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS1522")) {
            //当客户发生过贷款业务时，客户信用评级不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xdkfsxx b where a.customercode = b.browidcode and a.sjrq = b.sjrq and (a.actctrltype is null or ltrim(a.actctrltype) = '') and substr(b.loanprocode,1,3) not in ('F07','F06','F03','F01') and b.operationname != '申请删除')";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xdkfsxx group by browidcode,sjrq) b on a.customercode = b.browidcode and a.sjrq = b.sjrq "+whereStr2+" and substr(b.loanprocode,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = '')";
            errorinfo = "当客户发生过贷款业务时，客户信用评级不能为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当客户发生过贷款业务时，客户信用评级不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xdkfsxxh b where a.customercode = b.browidcode and a.sjrq = b.sjrq and (a.actctrltype is null or ltrim(a.actctrltype) = '') and substr(b.loanprocode,1,3) not in ('F07','F06','F03','F01'))";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xdkfsxxh group by browidcode,sjrq) b on a.customercode = b.browidcode and a.sjrq = b.sjrq "+whereStr2+" and substr(b.loanprocode,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = '')";
            errorinfo = "当客户发生过贷款业务时，客户信用评级不能为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS1523")) {
            //当客户还有存量贷款时，客户信用评级不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xcldkxx b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and substr(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = '') and b.operationname != '申请删除')";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xcldkxx group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and substr(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = '')";
            errorinfo = "当客户还有存量贷款时，客户信用评级不能为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo); 
            
            //当客户还有存量贷款时，客户信用评级不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xcldkxxh b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and substr(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = ''))";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xcldkxxh group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and substr(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = '')";
            errorinfo = "当客户还有存量贷款时，客户信用评级不能为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        }else if(xDataBaseTypeUtil.equalsOracle()){
        	if(checknumList.contains("JS2311")) {
        	//数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
        	sqlexist = "select id,customername,customercode from xftykhx "+whereStr+" and "
            		+ "(sjrq,regamtcreny,customercode) in (select sjrq,regamtcreny,customercode from xftykhx where operationname != '申请删除' group by sjrq,regamtcreny,customercode having count(1) > 1)";
            errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
            sqlexist = "select id,customername,customercode from xftykhx "+whereStr+" and "
            		+ "(sjrq,regamtcreny,customercode) in (select sjrq,regamtcreny,customercode from xftykhxh group by sjrq,regamtcreny,customercode having count(1) > 1)";
            errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS1269")) {
            //当客户有存续的贷款时，首次建立信贷关系日期不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ " and exists(select 1 from xcldkxx b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.fistdate) is null and b.operationname != '申请删除')";
            errorinfo = "当客户有存续的贷款时，首次建立信贷关系日期不能为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当客户有存续的贷款时，首次建立信贷关系日期不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ " and exists(select 1 from xcldkxxh b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.fistdate) is null)";
            errorinfo = "当客户有存续的贷款时，首次建立信贷关系日期不能为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS1269")) {
            //当客户有贷款发生额时，首次建立信贷关系日期不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ " and exists(select 1 from xdkfsxx b where a.customercode = b.browidcode and a.sjrq = b.sjrq and ltrim(a.fistdate) is null and b.operationname != '申请删除')";
            errorinfo = "当客户有贷款发生额时，首次建立信贷关系日期不能为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当客户有贷款发生额时，首次建立信贷关系日期不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ " and exists(select 1 from xdkfsxxh b where a.customercode = b.browidcode and a.sjrq = b.sjrq and ltrim(a.fistdate) is null)";
            errorinfo = "当客户有贷款发生额时，首次建立信贷关系日期不能为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS1278")) {
            //当客户有存续的贷款业务时，授信额度不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xcldkxx b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.creditamt) is null and b.operationname != '申请删除')";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xcldkxx where operationname != '申请删除' group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and (a.creditamt is null or ltrim(a.creditamt) = '') and b.operationname != '申请删除'";
            errorinfo = "当客户有存续的贷款业务时，授信额度不能为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当客户有存续的贷款业务时，授信额度不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xcldkxxh b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.creditamt) is null)";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhxh a inner join (select * from xcldkxxh group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and a.creditamt is null or ltrim(a.creditamt) = ''";
            errorinfo = "当客户有存续的贷款业务时，授信额度不能为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS1279")) {
            //当客户有存续的贷款业务时，已用额度不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xcldkxx b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.usedamt) is null and b.operationname != '申请删除')";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xcldkxx where operationname != '申请删除' group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and (a.usedamt is null or ltrim(a.usedamt) = '') and b.operationname != '申请删除'";
            errorinfo = "当客户有存续的贷款业务时，已用额度不能为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当客户有存续的贷款业务时，已用额度不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xcldkxxh b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.usedamt) is null)";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhxh a inner join (select * from xcldkxxh group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and a.usedamt is null or ltrim(a.usedamt) = ''";
            errorinfo = "当客户有存续的贷款业务时，已用额度不能为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS1522")) {
            //当客户发生过贷款业务时，客户信用评级不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xdkfsxx b where a.customercode = b.browidcode and a.sjrq = b.sjrq and ltrim(a.actctrltype) is null and substr(b.loanprocode,1,3) not in ('F07','F06','F03','F01') and b.operationname != '申请删除')";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xdkfsxx group by browidcode,sjrq) b on a.customercode = b.browidcode and a.sjrq = b.sjrq "+whereStr2+" and substr(b.loanprocode,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = '')";
            errorinfo = "当客户发生过贷款业务时，客户信用评级不能为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            
            //当客户发生过贷款业务时，客户信用评级不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xdkfsxxh b where a.customercode = b.browidcode and a.sjrq = b.sjrq and ltrim(a.actctrltype) is null and substr(b.loanprocode,1,3) not in ('F07','F06','F03','F01'))";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xdkfsxxh group by browidcode,sjrq) b on a.customercode = b.browidcode and a.sjrq = b.sjrq "+whereStr2+" and substr(b.loanprocode,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = '')";
            errorinfo = "当客户发生过贷款业务时，客户信用评级不能为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        	}
        	if(checknumList.contains("JS1523")) {
            //当客户还有存量贷款时，客户信用评级不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xcldkxx b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and substr(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and ltrim(a.actctrltype) is null and b.operationname != '申请删除')";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xcldkxx group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and substr(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = '')";
            errorinfo = "当客户还有存量贷款时，客户信用评级不能为空";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo); 
            
            //当客户还有存量贷款时，客户信用评级不能为空
            sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
            		+ "and exists (select 1 from xcldkxxh b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and substr(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and ltrim(a.actctrltype) is null)";
            //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xcldkxxh group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and substr(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = '')";
            errorinfo = "当客户还有存量贷款时，客户信用评级不能为空（历史表）";
            executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo); 
        	}
        }else {
        	if(checknumList.contains("JS2311")) {
            	//数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
            	sqlexist = "select t1.id id,t1.customername customername,t1.customercode customercode from xftykhx t1 "+whereStr2.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,regamtcreny,customercode from xftykhx where operationname != '申请删除' group by sjrq,regamtcreny,customercode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.regamtcreny = t2.regamtcreny and t1.customercode = t2.customercode)";
                errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //数据日期+金融机构代码+客户证件类型+客户证件代码应唯一
                sqlexist = "select t1.id id,t1.customername customername,t1.customercode customercode from xftykhx t1 "+whereStr2.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,regamtcreny,customercode from xftykhxh group by sjrq,regamtcreny,customercode having count(1) > 0) t2 where t1.sjrq = t2.sjrq and t1.regamtcreny = t2.regamtcreny and t1.customercode = t2.customercode)";
                errorinfo = "数据日期+金融机构代码+客户证件类型+客户证件代码应唯一（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
            	if(checknumList.contains("JS1269")) {
                //当客户有存续的贷款时，首次建立信贷关系日期不能为空
                sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
                		+ " and exists(select 1 from xcldkxx b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.fistdate) is null and b.operationname != '申请删除')";
                errorinfo = "当客户有存续的贷款时，首次建立信贷关系日期不能为空";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //当客户有存续的贷款时，首次建立信贷关系日期不能为空
                sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
                		+ " and exists(select 1 from xcldkxxh b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.fistdate) is null)";
                errorinfo = "当客户有存续的贷款时，首次建立信贷关系日期不能为空（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
            	if(checknumList.contains("JS1269")) {
                //当客户有贷款发生额时，首次建立信贷关系日期不能为空
                sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
                		+ " and exists(select 1 from xdkfsxx b where a.customercode = b.browidcode and a.sjrq = b.sjrq and ltrim(a.fistdate) is null and b.operationname != '申请删除')";
                errorinfo = "当客户有贷款发生额时，首次建立信贷关系日期不能为空";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //当客户有贷款发生额时，首次建立信贷关系日期不能为空
                sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
                		+ " and exists(select 1 from xdkfsxxh b where a.customercode = b.browidcode and a.sjrq = b.sjrq and ltrim(a.fistdate) is null)";
                errorinfo = "当客户有贷款发生额时，首次建立信贷关系日期不能为空（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
            	if(checknumList.contains("JS1278")) {
                //当客户有存续的贷款业务时，授信额度不能为空
                sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
                		+ "and exists (select 1 from xcldkxx b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.creditamt) is null and b.operationname != '申请删除')";
                //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xcldkxx where operationname != '申请删除' group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and (a.creditamt is null or ltrim(a.creditamt) = '') and b.operationname != '申请删除'";
                errorinfo = "当客户有存续的贷款业务时，授信额度不能为空";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //当客户有存续的贷款业务时，授信额度不能为空
                sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
                		+ "and exists (select 1 from xcldkxxh b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.creditamt) is null)";
                //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhxh a inner join (select * from xcldkxxh group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and a.creditamt is null or ltrim(a.creditamt) = ''";
                errorinfo = "当客户有存续的贷款业务时，授信额度不能为空（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
            	if(checknumList.contains("JS1279")) {
                //当客户有存续的贷款业务时，已用额度不能为空
                sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
                		+ "and exists (select 1 from xcldkxx b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.usedamt) is null and b.operationname != '申请删除')";
                //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xcldkxx where operationname != '申请删除' group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and (a.usedamt is null or ltrim(a.usedamt) = '') and b.operationname != '申请删除'";
                errorinfo = "当客户有存续的贷款业务时，已用额度不能为空";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //当客户有存续的贷款业务时，已用额度不能为空
                sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
                		+ "and exists (select 1 from xcldkxxh b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and ltrim(a.usedamt) is null)";
                //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhxh a inner join (select * from xcldkxxh group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and a.usedamt is null or ltrim(a.usedamt) = ''";
                errorinfo = "当客户有存续的贷款业务时，已用额度不能为空（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
            	if(checknumList.contains("JS1522")) {
                //当客户发生过贷款业务时，客户信用评级不能为空
                sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
                		+ "and exists (select 1 from xdkfsxx b where a.customercode = b.browidcode and a.sjrq = b.sjrq and ltrim(a.actctrltype) is null and substring(b.loanprocode,1,3) not in ('F07','F06','F03','F01') and b.operationname != '申请删除')";
                //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xdkfsxx group by browidcode,sjrq) b on a.customercode = b.browidcode and a.sjrq = b.sjrq "+whereStr2+" and substring(b.loanprocode,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = '')";
                errorinfo = "当客户发生过贷款业务时，客户信用评级不能为空";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                
                //当客户发生过贷款业务时，客户信用评级不能为空
                sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
                		+ "and exists (select 1 from xdkfsxxh b where a.customercode = b.browidcode and a.sjrq = b.sjrq and ltrim(a.actctrltype) is null and substring(b.loanprocode,1,3) not in ('F07','F06','F03','F01'))";
                //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xdkfsxxh group by browidcode,sjrq) b on a.customercode = b.browidcode and a.sjrq = b.sjrq "+whereStr2+" and substring(b.loanprocode,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = '')";
                errorinfo = "当客户发生过贷款业务时，客户信用评级不能为空（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
            	}
            	if(checknumList.contains("JS1523")) {
                //当客户还有存量贷款时，客户信用评级不能为空
                sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
                		+ "and exists (select 1 from xcldkxx b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and substring(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and ltrim(a.actctrltype) is null and b.operationname != '申请删除')";
                //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xcldkxx group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and substring(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = '')";
                errorinfo = "当客户还有存量贷款时，客户信用评级不能为空";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo); 
                
                //当客户还有存量贷款时，客户信用评级不能为空
                sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "+ whereStr2
                		+ "and exists (select 1 from xcldkxxh b where a.customercode = b.brroweridnum and a.sjrq = b.sjrq and substring(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and ltrim(a.actctrltype) is null)";
                //sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a inner join (select * from xcldkxxh group by brroweridnum,sjrq) b on a.customercode = b.brroweridnum and a.sjrq = b.sjrq "+whereStr2+" and substring(b.productcetegory,1,3) not in ('F07','F06','F03','F01') and (a.actctrltype is null or ltrim(a.actctrltype) = '')";
                errorinfo = "当客户还有存量贷款时，客户信用评级不能为空（历史表）";
                executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo); 
            	}
        }
        
//        //金融机构代码必须在金融机构（法人）基础信息表.金融机构代码或者金融机构（分支机构）基础信息表.金融机构代码中存在
//        sqlexist = "select a.id id,a.customername customername,a.customercode customercode from xftykhx a "
//        		+ whereStr2 + " and not exists(select 1 from xjrjgfrbaseinfo b where a.sjrq = b.sjrq and a.finorgcode = b.finorgcode) and not exists(select 1 from xjrjgfz b where a.sjrq = b.sjrq and a.finorgcode = b.finorgcode) and not exists(select 1 from xjrjgfrbaseinfoh b where a.sjrq = b.sjrq and a.finorgcode = b.finorgcode) and not exists(select 1 from xjrjgfzh b where a.sjrq = b.sjrq and a.finorgcode = b.finorgcode)";
//        errorinfo = "金融机构代码必须在金融机构（法人）基础信息表.金融机构代码或者金融机构（分支机构）基础信息表.金融机构代码中存在";
//        executeCheckSql(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
        
            resultSet = JDBCUtils.Query(connection, sql);
            while (true) {
                if (!resultSet.next()) break;
                Xftykhx xftykhx=new Xftykhx();
                Stringutil.getEntity(resultSet, xftykhx);
                CheckAllData3.checkXftykhx(customSqlUtil, xftykhx, errorMsg, errorId, rightId,checknumList);
            }
        }
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }finally {
            if (connection!=null){
                JDBCUtils.close();
            }
        }

        //校验结束

        //修改状态 和 下载
        if (errorId!=null && errorId.size()>0){
        	//-下载到本地
        	StringBuffer errorMsgAll = new StringBuffer("");
        	for(Map.Entry<String, String> entry : errorMsg.entrySet()) {
        		errorMsgAll.append(entry.getValue()+"\r\n");
        	}
            DownloadUtil.downLoad(errorMsgAll.toString(),target+File.separator+"checkout",fileName+".txt");
            errorMsg.clear();

            setMap.put("checkStatus","2");
            setMap.put("operationname"," ");
//            errorId.forEach(errId->{
//                whereMap.put("id",errId);
//                customSqlUtil.updateByWhere(tableName,setMap,whereMap);
//            });
            customSqlUtil.updateByWhereBatch(tableName, setMap, errorId);
            errorId.clear();
            json.put("msg","0");
        }else {
            if (b){
                json.put("msg","0");
            }else {
                json.put("msg","1");
            }
        }
        if (rightId!=null && rightId.size()>0){
            setMap.put("checkStatus","1");
            setMap.put("operationname"," ");
//            rightId.forEach(rigId->{
//                whereMap.put("id",rigId);
//                customSqlUtil.updateByWhere(tableName,setMap,whereMap);
//            });
            customSqlUtil.updateByWhereBatch(tableName, setMap, rightId);
            rightId.clear();
        }

        //校验结束之后 没有写审核-暂时在这里修改状态为 审核通过
        /*setMap.put("datastatus","3");
        setMap.put("operationname","");
        whereMap.put("datastatus","0");
        whereMap.put("checkstatus","1");
        int xdkdbht1 = customSqlUtil.updateByWhere(tableName, setMap, whereMap);
        Map<Integer,String> countMap=new HashMap<>();
        countMap.put(0,"reduce");
        countMap.put(3,"add");
        countUtil.handleMoreCount(tableName,countMap,xdkdbht1);*/
        customSqlUtil.saveLog("非同业单位客户校验成功","校验");
        try {
            PrintWriter out = response.getWriter();
            out.write(json.toString());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("XDownLoadCheckSix")
    public void XDownLoadCheckSix(HttpServletResponse response) {
        if (StringUtils.isBlank(target)){
            target=suisService.findAll().get(0).getMessagepath();
        }
        DownloadUtil.downLoadCheckFile(response,target + File.separator + "checkout" ,fileName+".txt");
    }
    
    public void executeCheckSql(Map<String,String> setMap,List<String> errorId,Map<String,String> whereMap,LinkedHashMap<String, String> errorMsg,String sql,String[] errorcode,String errorinfo) {
    	List<Object[]> exist = customSqlUtil.executeQuery(sql);
        if (exist!=null && exist.size()>0){
            setMap.put("checkStatus","2");
            setMap.put("operationname"," ");
            exist.forEach(errId->{
                errorId.add(String.valueOf(errId[0]));
                whereMap.put("id", String.valueOf(errId[0]));
                //customSqlUtil.updateByWhere(tableName,setMap,whereMap);
                if(!errorMsg.containsKey(String.valueOf(errId[0]))) {
                	errorMsg.put(String.valueOf(errId[0]), errorcode[0]+":"+String.valueOf(errId[1])+"，"+errorcode[1]+":"+errId[2]+"]->\r\n");
                }
                String str = errorMsg.get(String.valueOf(errId[0]));
                str = str + errorinfo+"|";
                errorMsg.put(String.valueOf(errId[0]), str);
            });
            whereMap.clear();
            setMap.clear();
            exist.clear();
        }
    }
}
