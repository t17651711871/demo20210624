package com.geping.etl.UNITLOAN.controller.dataManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.entity.report.Xdkdbht;
import com.geping.etl.UNITLOAN.entity.report.Xjrjgfz;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XjrjgfzService;
import com.geping.etl.UNITLOAN.util.*;
import com.geping.etl.UNITLOAN.util.check.CheckAllData3;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.utils.jdbc.JDBCUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

/**
 * 金融机构（分支机构）基础信息控制器
 * @author WuZengWen
 * @date 2020年6月9日 下午2:51:21
 */
@RestController
public class JRJGFZJCXXController {

	@Autowired
	private XjrjgfzService xs;
	@Autowired
	private SUpOrgInfoSetService sus;
	@Autowired
	private CalcCountUtil cct;
	@Autowired
	private SUpOrgInfoSetService suisService;

	@Autowired
	private ExcelUploadUtil excelUploadUtil;
	@Autowired
	private CustomSqlUtil customSqlUtil;
	@Autowired
	private HttpServletRequest request;
    @Autowired
    private XCheckRuleService checkRuleService;
	@Autowired
	private JdbcTemplate jt;
	@Autowired
	private XDataBaseTypeUtil xDataBaseTypeUtil;

	@Autowired
	private XcommonService commonService;
	@Autowired
    private DepartUtil departUtil;

	private static String target="";
	private Sys_UserAndOrgDepartment sys_user;
	private final String tableName="xjrjgfz";
	private final String fileName="金融机构(分支机构)基础信息";
	
	//访问金融机构（分支机构）基础信息页面
	@RequestMapping(value="/XshowJRJGFZJCXX",method=RequestMethod.GET)
	public ModelAndView XshowJRJGFZJCXXUi(HttpServletRequest request,String status){
		ModelAndView model = new ModelAndView();
		if("dtj".equals(status)) {
			model.setViewName("unitloan/datamanage/jrjgfzjcxx_dtj");
		}else if("dsh".equals(status)) {
			model.setViewName("unitloan/datamanage/jrjgfzjcxx_dsh");
		}
		return model;
	}	

	//查询数据
	@PostMapping("XGetXjrjgfzData")
	public ResponseResult XGetXjrjgfzData(int page, int rows){
		page = page - 1;
		sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");

		List<SUpOrgInfoSet> list = sus.findAll();
		String departId = departUtil.getDepart(sys_user);
		Specification specification=new Specification() {
			@Override
			public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				//待提交
				predicates.add(criteriaBuilder.equal(root.get("datastatus"), "0"));

				predicates.add(criteriaBuilder.like(root.get("departid"), departId));
				predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
		PageRequest pageRequest = new PageRequest(page, rows, sort);
		Page<Xjrjgfz> all = xs.findAll(specification, pageRequest);
		List<Xjrjgfz> content = all.getContent();
		long totalCount = all.getTotalElements();
		return ResponseResult.success(totalCount,content);
	}

	//导入
	@PostMapping(value = "Xdaorujrjgfz",produces = "text/plain;charset=UTF-8")
	public void Xdaorujrjgfz(HttpServletRequest request,HttpServletResponse response){
		sys_user=((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		File uploadFile = null;
		OPCPackage opcPackage=null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multiRequest.getFile("excelfile");
			JSONObject json = new JSONObject();
			if (file == null) {
				json.put("msg", "导入文件为空");
			} else {
				uploadFile = excelUploadUtil.uploadFile(file);
				opcPackage = OPCPackage.open(uploadFile);
				String[] headers = {"金融机构名称", "金融机构代码", "金融机构编码", "内部机构号", "许可证号","支付行号","机构级别",
						"直属上级管理机构名称", "直属上级管理机构金融机构编码", "直属上级管理机构内部机构号",
						"注册地址", "地区代码", "成立时间", "营业状态","数据日期"
				};
				String[] values = {"finorgname", "finorgcode", "finorgnum", "inorgnum", "xkzh","zfhh","orglevel",
						"highlevelorgname", "highlevelfinorgcode", "highlevelinorgnum", "regarea",
						"regareacode", "setupdate", "mngmestus","sjrq"
				};

				String[] dateFields = {"setupdate","sjrq"};
				XCommonExcel<Xjrjgfz>commonExcel=new XCommonExcel<>(sys_user, opcPackage, customSqlUtil, new Xjrjgfz(), headers, values,null,dateFields,null,null);
				commonExcel.process(0);
				String departId = departUtil.getDepart(sys_user);
				if (commonExcel.msg.toString().length() == 0) {
					commonService.importDelete(tableName,departId);
					commonExcel.process(1);
					json.put("msg","导入成功");

				} else {
					json.put("msg",commonExcel.msg.toString());
				}
			}
			String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
			customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
			out.write(json.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				opcPackage.close();
				//将上传的文件删除
				if (uploadFile.exists()) {
					uploadFile.delete();
				}
				if (out != null) {
					out.flush();
					out.close();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	//导出
	@RequestMapping(value = "/Xdaochujrjgfz", method = RequestMethod.GET)
	public void Xdaochujrjgfz(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select finorgname,finorgcode,finorgnum,inorgnum,xkzh,zfhh,orglevel,highlevelorgname,highlevelfinorgcode,highlevelinorgnum,"
				+ "regarea,regareacode,setupdate,mngmestus,sjrq from xjrjgfz";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		String parameter = request.getParameter("zuhagntai");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				if(i == 0) {
					query = query + "'" + idarr[i] + "'";
				}else {
					query = query + ",'" + idarr[i] + "'";
				}
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			String departId = departUtil.getDepart(sys_User);
			//判断导出 1为待审核
			if (parameter.equals("dtj")){
				query = query + " where (datastatus='0' or datastatus='2') and orgid='" + sys_User.getOrgid() + "'";
				query = query + " and departid like '" + departId + "'";
			}else {
				query = query + " where datastatus='1' and orgid='" + sys_User.getOrgid() + "'";
				query = query + " and departid like '" + departId + "'";
			}
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("finorgname"));
			dataA.add(smo.get("finorgcode"));
			dataA.add(smo.get("finorgnum"));
			dataA.add(smo.get("inorgnum"));
			dataA.add(smo.get("xkzh"));
			dataA.add(smo.get("zfhh"));
			dataA.add(smo.get("orglevel"));
			dataA.add(smo.get("highlevelorgname"));
			dataA.add(smo.get("highlevelfinorgcode"));
			dataA.add(smo.get("highlevelinorgnum"));
			dataA.add(smo.get("regarea"));
			dataA.add(smo.get("regareacode"));
			dataA.add(smo.get("setupdate"));
			dataA.add(smo.get("mngmestus"));
			dataA.add(smo.get("sjrq"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("金融机构名称");
		columnNames.add("金融机构代码");
		columnNames.add("金融机构编码");
		columnNames.add("内部机构号");
		columnNames.add("许可证号");
		columnNames.add("支付行号");
		columnNames.add("机构级别");
		columnNames.add("直属上级管理机构名称");
		columnNames.add("直属上级管理机构金融机构编码");
		columnNames.add("直属上级管理机构内部机构号");
		columnNames.add("注册地址");
		columnNames.add("地区代码");
		columnNames.add("成立时间");
		columnNames.add("营业状态");
		columnNames.add("数据日期");
		try {
			String fileName = "金融机构（分支机构）基础信息.xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//导出模版
	@RequestMapping(value = "/Xdaochumobanjrjgfz", method = RequestMethod.GET)
	public void Xdaochumobanjrjgfz(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select finorgname,finorgcode,finorgnum,inorgnum,xkzh,zfhh,orglevel,highlevelorgname,highlevelfinorgcode,highlevelinorgnum,"
				+ "regarea,regareacode,setupdate,mngmestus from xjrjgfz";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				query = query + "'" + idarr[i] + "'";
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			Object isusedepart = request.getSession().getAttribute("isusedepart");
			query = query + " where (datastatus='0' or datastatus='2') and orgid='" + sys_User.getOrgid() + "'";
			if("yes".equals(isusedepart)) {
				query = query + " and departid='" + sys_User.getDepartid() + "'";
			}
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<List<Object>> objects = new LinkedList<>();
		List<String> columnNames = new LinkedList<>();
		columnNames.add("金融机构名称");
		columnNames.add("金融机构代码");
		columnNames.add("金融机构编码");
		columnNames.add("内部机构号");
		columnNames.add("许可证号");
		columnNames.add("支付行号");
		columnNames.add("机构级别");
		columnNames.add("直属上级管理机构名称");
		columnNames.add("直属上级管理机构金融机构编码");
		columnNames.add("直属上级管理机构内部机构号");
		columnNames.add("注册地址");
		columnNames.add("地区代码");
		columnNames.add("成立时间");
		columnNames.add("营业状态");
		columnNames.add("数据日期");
		try {
			String fileName = "金融机构（分支机构）基础信息.xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}		
	
	//导出H
	@RequestMapping(value = "/XdaochujrjgfzH", method = RequestMethod.GET)
	public void XdaochujrjgfzH(HttpServletRequest request, HttpServletResponse response) {
		// 设置编码
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		response.setContentType("octets/stream");
		request.getSession().setAttribute("exportedFlag", "error");
		String query = "select finorgname,finorgcode,finorgnum,inorgnum,xkzh,zfhh,orglevel,highlevelorgname,highlevelfinorgcode,highlevelinorgnum,"
				+ "regarea,regareacode,setupdate,mngmestus from xjrjgfzH";
		// 获取页面传来要查询的条件
		String selectid = request.getParameter("selectid");
		if (StringUtils.isNotBlank(selectid)) {
			query = query + " where id in (";
			String[] idarr = selectid.split("-");
			for (int i = 0; i < idarr.length; i++) {
				if(i == 0) {
					query = query + "'" + idarr[i] + "'";
				}else {
					query = query + ",'" + idarr[i] + "'";
				}
			}
			query = query + ")";
		}else {
			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
			String departId = departUtil.getDepart(sys_user);
			query = query + " where orgid='" + sys_User.getOrgid() + "'";
			query = query + " and departid like '" + departId + "'";
			String operationtime = request.getParameter("operationtime");
			String selectjrjgmc = request.getParameter("selectjrjgmc");
			if(StringUtils.isNotBlank(operationtime)) {
				query = query + " and sjrq like '%" + operationtime + "%'";
			}
			if(StringUtils.isNotBlank(selectjrjgmc)) {
				query = query + " where selectjrjgmc like'%" + selectjrjgmc + "%'";
			}
		}
		//List<Map<String, Object>> listM = ServiceTemplate.findData(query);
		List<Map<String, Object>> listM = jt.queryForList(query);
		List<List<Object>> objects = new LinkedList<>();
		for(Map<String, Object> smo : listM) {
			List<Object> dataA = new LinkedList<>();
			dataA.add(smo.get("finorgname"));
			dataA.add(smo.get("finorgcode"));
			dataA.add(smo.get("finorgnum"));
			dataA.add(smo.get("inorgnum"));
			dataA.add(smo.get("xkzh"));
			dataA.add(smo.get("zfhh"));
			dataA.add(smo.get("orglevel"));
			dataA.add(smo.get("highlevelorgname"));
			dataA.add(smo.get("highlevelfinorgcode"));
			dataA.add(smo.get("highlevelinorgnum"));
			dataA.add(smo.get("regarea"));
			dataA.add(smo.get("regareacode"));
			dataA.add(smo.get("setupdate"));
			dataA.add(smo.get("mngmestus"));
			dataA.add(smo.get("sjrq"));
			objects.add(dataA);
		}
		List<String> columnNames = new LinkedList<>();
		columnNames.add("金融机构名称");
		columnNames.add("金融机构代码");
		columnNames.add("金融机构编码");
		columnNames.add("内部机构号");
		columnNames.add("许可证号");
		columnNames.add("支付行号");
		columnNames.add("机构级别");
		columnNames.add("直属上级管理机构名称");
		columnNames.add("直属上级管理机构金融机构编码");
		columnNames.add("直属上级管理机构内部机构号");
		columnNames.add("注册地址");
		columnNames.add("地区代码");
		columnNames.add("成立时间");
		columnNames.add("数据日期");

		try {
			String fileName = "金融机构（分支机构）基础信息.xlsx";
			response.addHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
			Excel2007Utils.writeExcelResponse(response,columnNames,objects);
			request.getSession().setAttribute("exportedFlag", "success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	//待提交页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindJRJGFZJCXXdtj", method = RequestMethod.POST)
	public void XfindJRJGFZJCXXdtj(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		String selectjrjgmc = request.getParameter("selectjrjgmc");
		String zhuangtai = "0";
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		List<Xjrjgfz> list = null;
		Page<Xjrjgfz> gcPage = getPageData(selectjrjgmc,"","",zhuangtai,page,size);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		result.put("rows", list);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//待审核页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindJRJGFZJCXXdsh", method = RequestMethod.POST)
	public void XfindJRJGFZJCXXdsh(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		String selectjrjgmc = request.getParameter("selectjrjgmc");
		String selectjrjgdm = request.getParameter("selectjrjgdm");
		String selectjylx = request.getParameter("selectjylx");
		String selectczm = request.getParameter("selectczm");
		String zhuangtai = "1";
		if(StringUtils.isNotBlank(selectczm)) {
			zhuangtai = "3";
		}
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		List<Xjrjgfz> list = null;
		Page<Xjrjgfz> gcPage = getPageData(selectjrjgmc,selectjrjgdm,selectjylx,zhuangtai,page,size);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		result.put("rows", list);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}
	//获取分页数据方法
	public Page<Xjrjgfz> getPageData(String selectjrjgmc,String selectjrjgdm,String selectjylx,String zhuangtai,Integer page,Integer size){
		sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");

		List<SUpOrgInfoSet> list = sus.findAll();
		String departId = departUtil.getDepart(sys_user);
		Specification<Xjrjgfz> querySpecifi = new Specification<Xjrjgfz>() {
			@Override
			public Predicate toPredicate(Root<Xjrjgfz> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				if (StringUtils.isNotBlank(selectjrjgmc)) {
					predicates.add(criteriaBuilder.like(root.get("finorgname"), "%" + selectjrjgmc.trim() + "%"));
				}
				if (StringUtils.isNotBlank(selectjrjgdm)) {
					predicates.add(criteriaBuilder.like(root.get("finorgcode"), "%" + selectjrjgdm.trim() + "%"));
				}
				if (StringUtils.isNotBlank(selectjylx)) {
					predicates.add(criteriaBuilder.equal(root.get("checkstatus"), selectjylx.trim()));
				}
				if("0".equals(zhuangtai)) {
					Predicate p1 = null;
					Predicate p2 = null;
					p1 = criteriaBuilder.equal(root.get("datastatus"),"0");
					p2 = criteriaBuilder.equal(root.get("datastatus"),"2");
					predicates.add(criteriaBuilder.or(p1, p2));
				}else if("3".equals(zhuangtai)){
					predicates.add(criteriaBuilder.equal(root.get("operationname"),"申请删除"));
					predicates.add(criteriaBuilder.equal(root.get("datastatus"),"1"));
				}else {
					predicates.add(criteriaBuilder.equal(root.get("datastatus"),zhuangtai));
				}
				predicates.add(criteriaBuilder.like(root.get("departid"), departId));
				predicates.add(criteriaBuilder.like(root.get("departid"), departId));
				predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
		// Pageable 接口通常使用的其 PageRequest 实现类. 其中封装了需要分页的信息
		PageRequest pr = new PageRequest(page, size,sort); // intPage从0开始
		return xs.findAll(querySpecifi, pr);
	}
	
	//新增或修改金融机构（分支机构）基础信息
	@RequestMapping(value="/XsaveOrUpdatejrjgfz",method=RequestMethod.POST)
	public void XsaveOrUpdatejrjgfz(HttpServletRequest request,HttpServletResponse response,Xjrjgfz Xjrjgfz){
		PrintWriter out = null;
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
		    response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			/*JSONObject json = new JSONObject();
			if("1".equals(result)) {
				json.put("msg","1");
				out.write(json.toString());
			}else {
				json.put("msg",result);
				out.write(json.toString());
			}*/
			if(Xjrjgfz.getId().length() == 0 || StringUtils.isBlank(Xjrjgfz.getId())) {
				String uuid = Stringutil.getUUid();
				Xjrjgfz.setId(uuid);
				Xjrjgfz.setDepartid(sys_User.getDepartid());
				Xjrjgfz.setOrgid(sys_User.getOrgid());
			}
			Xjrjgfz.setCheckstatus("0");
			Xjrjgfz.setDatastatus("0");
			Xjrjgfz.setOperationname(" ");
			Xjrjgfz.setOperator(sys_User.getLoginid());
			Xjrjgfz.setOperationtime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			Xjrjgfz xjrjgfz = xs.SaveOrUpdate(Xjrjgfz);
			if(xjrjgfz!=null) {
				if(Xjrjgfz.getId().length() == 0 || StringUtils.isNotBlank(Xjrjgfz.getId())) {
					cct.handleCount("xjrjgfz", 0, "add", 1);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//删除金融机构（分支机构）基础信息
	@RequestMapping(value="/Xdeletejrjgfz",method=RequestMethod.POST)
	public void Xdeletejrjgfz(HttpServletRequest request,HttpServletResponse response,String deletetype,String deleteid){
		PrintWriter out = null;
		try {
			response.setContentType("text/html");
		    response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//result = xs.delete();
				String sql = "delete from xjrjgfz where datastatus='0' and operationname='同意删除' and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
				//result = xs.delete();
			}else if("some".equals(deletetype)) {
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "delete from xjrjgfz where id in(";
					for (int i = 0; i < idarr.length; i++) {
						//xs.delete(idarr[i]);
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
						//xs.delete(idarr[i]);
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
					//result = 1;
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 0, "reduce", Integer.valueOf(result));
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 0, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//申请删除金融机构（分支机构）基础信息
	@RequestMapping(value="/Xshenqingdeletejrjgfz",method=RequestMethod.POST)
	public void Xshenqingdeletejrjgfz(HttpServletRequest request,HttpServletResponse response,String deletetype,String deleteid){
		PrintWriter out = null;
		try {
			response.setContentType("text/html");
		    response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//result = xs.delete();
				//String sql = "delete from xjrjgfz where datastatus='0' and operationname='同意删除'";
				String sql = "update xjrjgfz set operationname='申请删除',datastatus='1' where datastatus='0' or datastatus='2' and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
				//result = xs.delete();

			}else if("some".equals(deletetype)) {
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "update xjrjgfz set operationname='申请删除',datastatus='1' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						//xs.delete(idarr[i]);
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
						//xs.delete(idarr[i]);
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
					//result = 1;
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 0, "reduce",result);
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 0, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		} finally {
			if(out!=null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//校验金融机构（分支机构）基础信息
//@RequestMapping(value="/Xcheckjrjgfz",method=RequestMethod.POST)
//	public void Xcheckjrjgfz(HttpServletRequest request,HttpServletResponse response){
//		PrintWriter out = null;
//		String checktype = request.getParameter("checktype");
//		LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
//		List<String> errorId = new ArrayList<>();
//		List<String> rightId = new ArrayList<>();
//		Map<String,String> whereMap=new HashMap<>();
//		Map<String,String> setMap=new HashMap<>();
//
//		try {
//			response.setContentType("text/html");
//			response.setContentType("text/plain; charset=utf-8");
//			out = response.getWriter();
//			String result = "0";
//			if("all".equals(checktype)) {
////				result = CheckxdkdbwxUtils.jiaoYanShuJu(TableCodeEnum.JRJGFZJCXX,xs,errorMsg, errorId, rightId);
//				if("1".equals(result)) {
//					//xs.updateXjrjgfzOnCheckstatus("1");
//					//xs.updateXjrjgfzOnDatastatus("3");
//					out.write(result);
//				}else if ("13".equals(result)){
//					out.write(result);
//				}else {
//					//xs.updateXjrjgfzOnCheckstatus("2");
//					List<SUpOrgInfoSet> list = sus.findAll();
//					String messagepath = "C:";
//					if(list.size() > 0) {
//						messagepath = list.get(0).getMessagepath();
//					}
//					request.getSession().setAttribute("messagepath",messagepath);
//					DownloadUtil.downLoad("校验结果:"+result, messagepath+"\\checkout", "金融机构（分支机构）基础信息校验结果.txt");
//					out.write(result);
//				}
//			}else if("some".equals(checktype)) {
//				String rows = request.getParameter("rows");
//				// 先转成json数组再将json转化成list数组
//				JSONArray jsonArray = JSONArray.fromObject(rows);
//				@SuppressWarnings("unchecked")
//				List<Xjrjgfz> list = JSONArray.toList(jsonArray, new Xjrjgfz(), new JsonConfig());
////				result = CheckDataUtils2.jiaoYanShuJu(list,TableCodeEnum.JRJGFZJCXX,xs,errorMsg, errorId, rightId);
//				if("1".equals(result)) {
//					/*for(int i=0;i<list.size();i++) {
//						list.get(i).setCheckstatus("1");
//						//list.get(i).setDatastatus("3");
//					}
//					xs.Save(list);*/
//					out.write(result);
//				}else {
//					/*for(int i=0;i<list.size();i++) {
//						list.get(i).setCheckstatus("2");
//					}
//					xs.Save(list);*/
//					List<SUpOrgInfoSet> olist = sus.findAll();
//					String messagepath = "C:";
//					if(olist.size() > 0) {
//						messagepath = olist.get(0).getMessagepath();
//					}
//					request.getSession().setAttribute("messagepath",messagepath);
//					DownloadUtil.downLoad("校验结果:"+result, messagepath+"\\checkout", "金融机构（分支机构）基础信息校验结果.txt");
//					out.write(result);
//				}
//			}
//
//			//out.write(result);
//		} catch (Exception e) {
//			e.printStackTrace();
//			out.write("0");
//		} finally {
//			if(out!=null) {
//				out.flush();
//				out.close();
//			}
//		}
//	}
	//校验
	@RequestMapping(value="/Xcheckjrjgfz",method=RequestMethod.POST)
	public void XCheckDataThree(HttpServletRequest request, HttpServletResponse response,String finorgnameParam){
		//记录是否有错
		boolean b=false;
		boolean sqlServer = xDataBaseTypeUtil.equalsSqlServer();
		sys_user = ((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

		List<SUpOrgInfoSet> sUpOrgInfoSets = suisService.findAll();
		target=sUpOrgInfoSets.get(0).getMessagepath();

		JSONObject json = new JSONObject();
		String id = request.getParameter("id");
		String departId = departUtil.getDepart(sys_user);

		LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
		List<String> errorId = new ArrayList<>();
		List<String> rightId = new ArrayList<>();
		Map<String,String> whereMap=new HashMap<>();
		Map<String,String> setMap=new HashMap<>();
		setMap.put("operator",sys_user.getLoginid());
		setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

		String whereStr=" where datastatus='0' and checkstatus='0' ";
		String whereStr2=" where a.datastatus='0' and a.checkstatus='0' ";
		if (StringUtils.isNotBlank(id)){
			id = id.substring(0,id.length()-1);
			id = "'" + id.replace(",","','")  + "'";
			whereStr = whereStr + " and id in(" + id + ")";
			whereStr2 = whereStr2 + " and a.id in(" + id + ")";
		}else {
			if (StringUtils.isNotBlank(finorgnameParam)){
				whereStr = whereStr + " and finorgname like '%"+ finorgnameParam +"%'";
				whereStr2 = whereStr2 + " and a.finorgname like '%"+ finorgnameParam +"%'";
			}
			whereStr = whereStr + " and departid like '%"+ departId +"%'";
            whereStr2 = whereStr2 + " and a.departid like '%"+ departId +"%'";
		}
		String sql="select * from "+tableName+" "+whereStr;
		Connection connection = null;
		ResultSet resultSet=null;

        //校验开关集合
        List<String> checknumList = checkRuleService.getChecknum("xjrjgfz");
		//标识名称
		String errorcode = "金融机构名称";
        if (checknumList.contains("JS2309")){
        	if (sqlServer){
				String errorinfo  = "数据日期+金融机构代码+内部机构号应唯一不唯一";
				String sqlexist ="select id,finorgname from xjrjgfz t1 "+whereStr2.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,finorgcode,inorgnum from xjrjgfz where operationname != '申请删除' group by sjrq,finorgcode,inorgnum having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.finorgcode = t2.finorgcode and t1.inorgnum = t2.inorgnum)";
				inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);

				errorinfo = "数据日期+金融机构代码+内部机构号应唯一不唯一（历史表）";
				sqlexist ="select id,finorgname from xjrjgfz t1 "+whereStr2.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,finorgcode,inorgnum from xjrjgfzh group by sjrq,finorgcode,inorgnum having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.finorgcode = t2.finorgcode and t1.inorgnum = t2.inorgnum)";
				inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
			}else {
				String errorinfo  = "数据日期+金融机构代码+内部机构号应唯一不唯一";
				String sqlexist ="select id,finorgname from xjrjgfz "+whereStr+" and (sjrq,finorgcode,inorgnum) in (select sjrq,finorgcode,inorgnum from xjrjgfz  where operationname != '申请删除'  group by sjrq,finorgcode,inorgnum having  count(1) > 1)";
				inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);


				errorinfo = "数据日期+金融机构代码+内部机构号应唯一不唯一（历史表）";
				sqlexist = "select id,finorgname from xjrjgfz "+whereStr+" and (sjrq,finorgcode,inorgnum) in (select sjrq,finorgcode,inorgnum from xjrjgfzh  group by sjrq,finorgcode,inorgnum having  count(1) > 0)";
				inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
			}
        }


//		errorinfo = "数据日期+金融机构代码+内部机构号应唯一不唯一（历史表）";
//		sqlexist = "select id,finorgname from xjrjgfz "+whereStr+" and (CONCAT(ifnull(sjrq,' '),ifnull(finorgcode,' '),ifnull(inorgnum,' '))) in (select CONCAT(ifnull(sjrq,' '),ifnull(finorgcode,' '),ifnull(inorgnum,' ')) from xjrjgfzh   group by sjrq,finorgcode,inorgnum having  count(1) > 1)";
//		inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);


		try {
			connection = com.geping.etl.utils.jdbc.JDBCUtils.getConnection();
			resultSet = com.geping.etl.utils.jdbc.JDBCUtils.Query(connection, sql);
			while (true) {
				if (!resultSet.next()) break;
				Xjrjgfz xjrjgfz=new Xjrjgfz();
				Stringutil.getEntity(resultSet, xjrjgfz);
				CheckxdkdbwxUtils.guiZeJiaoYanForJRJGFZJCXX(xjrjgfz,checknumList,errorMsg, errorId, rightId);
			}
		} catch (Exception throwables) {
			throwables.printStackTrace();
		}finally {
			if (connection!=null){
				JDBCUtils.close();
			}
		}

		//校验结束

		//修改状态 和 下载
		if (errorId!=null && errorId.size()>0){
			//-下载到本地
			StringBuffer errorMsgAll = new StringBuffer("");
			for(Map.Entry<String, String> entry : errorMsg.entrySet()) {
				errorMsgAll.append(entry.getValue()+"\r\n");
			}
			DownloadUtil.downLoad(errorMsgAll.toString(),target+File.separator+"checkout",fileName+".txt");
			errorMsg.clear();

			setMap.put("checkStatus","2");
			setMap.put("operationname"," ");
			customSqlUtil.updateByWhereBatch(tableName,setMap,errorId);

			errorId.clear();
			json.put("msg","0");
		}else {
			if (b){
				json.put("msg","0");
			}else {
				json.put("msg","1");
			}
		}
		if (rightId!=null && rightId.size()>0){
			setMap.put("checkStatus","1");
			setMap.put("operationname"," ");

			customSqlUtil.updateByWhereBatch(tableName,setMap,rightId);
			rightId.clear();
		}

		customSqlUtil.saveLog("金融机构（分支机构）基础信息校验成功","校验");
		try {
			PrintWriter out = response.getWriter();
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GetMapping("XDownLoadCheckJRFZ")
	public void XDownLoadCheckTwo(HttpServletResponse response) {
		if (StringUtils.isBlank(target)){
			target=suisService.findAll().get(0).getMessagepath();
		}
		DownloadUtil.downLoadCheckFile(response,target + File.separator + "checkout",fileName+".txt");
	}
	//下载校验结果
	@RequestMapping(value="/XdownFileCheckjrjgfz",method=RequestMethod.GET)
	public void XdownFileCheckjrjgfz(HttpServletRequest request,HttpServletResponse response){
		InputStream input = null;
		OutputStream output = null;
		try {
			//String name = request.getParameter("name");
			Object messagepath = request.getSession().getAttribute("messagepath");
			String filename = "金融机构（分支机构）基础信息校验结果.txt";
			File file = new File(messagepath.toString()+"\\checkout\\"+filename);
			if (file.exists()) {
				file.setWritable(true, false);
				response.setCharacterEncoding("GBK");
				response.setContentType("application/octet-stream;charset=GBK");
				response.setHeader("Content-Disposition","attachment;filename="+ new String(filename.getBytes("GBK"), "ISO8859-1"));
				input = new FileInputStream(messagepath.toString()+"\\checkout\\"+filename);
				int len = 0;
				byte[] buffer = new byte[1024];
				output = response.getOutputStream();
				while ((len = input.read(buffer)) > 0) {
					output.write(buffer, 0, len);
				}
				output.flush();
				output.close();
				input.close();
				file.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (input != null) {
					input.close();
				}
				
				if (output != null) {
					output.flush();
					output.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	//提交
	@RequestMapping(value="/Xtijiaojrjgfz",method=RequestMethod.POST)
	public void Xtijiaojrjgfz(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				result = xs.updateXjrjgfzOnDatastatus("1",sys_User.getLoginid(),departId);
				//result = ServiceTemplate.YeWuMoBan(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					List<String> idList = new ArrayList<>();
					for (int i = 0; i < idarr.length; i++) {
						idList.add(idarr[i]);
					}
					result = xs.updateXjrjgfzOnDatastatus("1",sys_User.getLoginid(),idList);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 1, "reduce", Integer.valueOf(result));
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 1, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}

	//申请删除
	@RequestMapping(value="/Xshenqingshanchujrjgfz",method=RequestMethod.POST)
	public void Xshenqingshanchujrjgfz(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xjrjgfz set operationname='申请删除',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and (operationname is null or operationname=' ') and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xjrjgfz set operationname='申请删除',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}	
	
	//申请修改
	@RequestMapping(value="/Xshenqingxiugaijrjgfz",method=RequestMethod.POST)
	public void Xshenqingxiugaijrjgfz(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xjrjgfz set operationname='申请修改',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and (operationname is null or operationname=' ') and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					String[] idarr = deleteid.split("-");
					String sql = "update xjrjgfz set operationname='申请修改',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//同意申请
	@RequestMapping(value="/Xtongyishenqingjrjgfz",method=RequestMethod.POST)
	public void Xtongyishenqingjrjgfz(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				String sql = "update xjrjgfz set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where datastatus='1' and operator !='"+sys_User.getLoginid()+"' and (operationname is not null or operationname!=' ') and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "update xjrjgfz set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 3, "reduce", Integer.valueOf(result));
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 3, "reduce", idarr.length);
				}
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//同意申请删除
	@RequestMapping(value="/Xtongyishenqingshanchujrjgfz",method=RequestMethod.POST)
	public void Xtongyishenqingshanchujrjgfz(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xdkdbwx set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where datastatus='1' and (operationname is not null or operationname!='');";
				//String sql = "delete from xjrjgfz where datastatus='1' and operator <>'"+sys_User.getLoginid()+"' and operationname='申请删除'";
				String sql = "delete from xjrjgfz where datastatus='1' and departid like '"+departId+"' and operationname='申请删除'" + " and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					//String sql = "update xdkdbwx set datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"',operationname= case when operationname='申请修改' then '同意修改' when operationname='申请删除' then '同意删除' else '同意申请' end where id in(";
					String sql = "delete from xjrjgfz where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 3, "reduce", result);
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 3, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}		
	
	//拒绝申请
	@RequestMapping(value="/Xjujueshenqingjrjgfz",method=RequestMethod.POST)
	public void Xjujueshenqingjrjgfz(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xjrjgfz set operationname='',datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and operator <>'"+sys_User.getLoginid()+"' and operationname='申请删除'";
				String sql = "update xjrjgfz set operationname=' ',datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and departid like '"+departId+"' and operationname='申请删除'" + " and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "update xjrjgfz set operationname=' ',datastatus='0',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	//审核通过
	@RequestMapping(value="/Xshenhetongguojrjgfz",method=RequestMethod.POST)
	public void Xshenhetongguojrjgfz(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xjrjgfz set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and operator <>'"+sys_User.getLoginid()+"' and (operationname='' or operationname is null)";
				String sql = "update xjrjgfz set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and departid like '"+departId+"' and (operationname=' ' or operationname is null)" + " and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr = deleteid.split("-");
					String sql = "update xjrjgfz set datastatus='3',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 3, "reduce", Integer.valueOf(result));
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 3, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("-1");
		}
	}
	
	//审核不通过
	@RequestMapping(value="/Xshenhebutongguojrjgfz",method=RequestMethod.POST)
	public void Xshenhebutongguojrjgfz(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String deletetype = request.getParameter("deletetype");
		String yuanyin = request.getParameter("yuanyin");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(deletetype)) {
				//String sql = "update xjrjgfz set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and operator !='"+sys_User.getLoginid()+"' and (operationname='' or operationname is null)";
				String sql = "update xjrjgfz set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='1' and checkstatus='1' and departid like '"+departId+"' and (operationname=' ' or operationname is null)" + " and (((select count(id) from suporginfo where shifoushenheziji='no')>0 and operator <>'"+sys_User.getLoginid()+"') or (select count(id) from suporginfo where shifoushenheziji='yes')>0)";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(deletetype)) {
				String deleteid = request.getParameter("deleteid");
				if (StringUtils.isNotBlank(deleteid)) {
					idarr =  deleteid.split("-");
					String sql = "update xjrjgfz set datastatus='2',checkstatus='0',nopassreason='"+yuanyin+"',operationname='审核不通过',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				if("all".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 0, "reduce", Integer.valueOf(result));
				}else if("some".equals(deletetype)) {
					cct.handleCount("xjrjgfz", 0, "reduce", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
	
	//访问单位贷款担保物信息生成报文页面
	@RequestMapping(value="/XshowJRJGFZJCXXRP",method=RequestMethod.GET)
	public ModelAndView XshowJRJGFZJCXXRPUi(HttpServletRequest request){
		String status = request.getParameter("status");
		ModelAndView model = new ModelAndView();
		model.addObject("datamanege",status);
		model.setViewName("unitloan/createreport/jrjgfzjcxx_bw");
		return model;
	}
	
	//生成报文页面easyui自加载请求以及load动态查询
	@RequestMapping(value = "/XfindJRJGFZJCXXscbw", method = RequestMethod.POST)
	public void XfindJRJGFZJCXXscbw(HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("text/html");
		response.setContentType("text/plain; charset=utf-8");
		// 获取页面传来要查询的条件
		String selectjrjgmc = request.getParameter("selectjrjgmc");
		String zhuangtai = "3";
		String currentPageNumber = request.getParameter("page"); // 获取查询页数
		String currentPageSize = request.getParameter("rows"); // 获取每页条数
		int page = Integer.valueOf(currentPageNumber) - 1;
		int size = Integer.valueOf(currentPageSize);
		Specification<Xjrjgfz> querySpecifi = new Specification<Xjrjgfz>() {
			@Override
			public Predicate toPredicate(Root<Xjrjgfz> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				if (StringUtils.isNotBlank(selectjrjgmc)) {
					predicates.add(criteriaBuilder.like(root.get("finorgname"), "%" + selectjrjgmc.trim() + "%"));
				}
				predicates.add(criteriaBuilder.equal(root.get("datastatus"),zhuangtai));
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		// Pageable 接口通常使用的其 PageRequest 实现类. 其中封装了需要分页的信息
		PageRequest pr = new PageRequest(page, size); // intPage从0开始
		List<Xjrjgfz> list = null;
		Page<Xjrjgfz> gcPage = xs.findAll(querySpecifi, pr);
		list = gcPage.getContent();
		JSONObject result = new JSONObject();
		result.put("rows", list);
		result.put("total", gcPage.getTotalElements());
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(result.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gcPage != null) {
				gcPage = null;
			}
			if (list != null) {
				list = null;
			}
			if (result != null) {
				result = null;
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}
	
	//数据打回
	@RequestMapping(value="/Xdahuijrjgfz",method=RequestMethod.POST)
	public void Xdahuijrjgfz(HttpServletRequest request,HttpServletResponse response){
		PrintWriter out = null;
		String checktype = request.getParameter("checktype");
		Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
		try {
			response.setContentType("text/html");
			response.setContentType("text/plain; charset=utf-8");
			out = response.getWriter();
			Integer result = 0;
			String[] idarr = null;
			String departId = departUtil.getDepart(sys_user);
			if("all".equals(checktype)) {
				String sql = "update xjrjgfz set checkstatus='0',datastatus='0',operationname=' ',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='3' and departid like '"+departId+"'";
				//result = ServiceTemplate.YeWuMoBan(sql);
				result = jt.update(sql);
			}else if("some".equals(checktype)) {
				String selectid = request.getParameter("selectid");
				if (StringUtils.isNotBlank(selectid)) {
					idarr = selectid.split("-");
					String sql = "update xjrjgfz set checkstatus='0',datastatus='0',operationname=' ',operator='"+sys_User.getLoginid()+"',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where datastatus='3' and id in(";
					for (int i = 0; i < idarr.length; i++) {
						if(i == 0) {
							sql = sql + "'" + idarr[i] + "'";
						}else {
							sql = sql +"," + "'" + idarr[i] + "'";
						}
					}
					sql = sql +")";
					//result = ServiceTemplate.YeWuMoBan(sql);
					result = jt.update(sql);
				}
			}
			if(result > 0) {
				if("all".equals(checktype)) {
					cct.handleCount("xjrjgfz", 0, "add", result);
				}else if("some".equals(checktype)) {
					cct.handleCount("xjrjgfz", 0, "add", idarr.length);
				}
				out.write("1");
			}else {
				out.write("0");
			}
		}catch (Exception e) {
			e.printStackTrace();
			out.write("0");
		}
	}
	
//	//生成报文
//	@RequestMapping(value = "/Xcreatemessagejrjgfz", method = RequestMethod.POST)
//    public void Xcreatemessagejrjgfz(HttpServletRequest request,HttpServletResponse response){
//		PrintWriter out = null;
//		String checktype = request.getParameter("checktype");
//		try {
//			response.setContentType("text/html");
//			response.setContentType("text/plain; charset=utf-8");
//			out = response.getWriter();
//			Sys_UserAndOrgDepartment sys_User = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
//			String result = "0";
//			List<SUpOrgInfoSet> olist = sus.findAll();
//			String messagepath = "C:";
//			String bsjgdm = "91310000607342023U";
//			if(olist.size() > 0) {
//				messagepath = olist.get(0).getMessagepath();
//				bsjgdm = olist.get(0).getBankcodewangdian();
//			}
//			String filename = bsjgdm+"_JRJGFZ_"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))+".csv";
//			
//			if("all".equals(checktype)) {
//				result = CreateMessageUtils.shengChengBaoWen(messagepath,filename,TableCodeEnum.JRJGFZJCXX);
//				if("1".equals(result)) {
//					xs.updateXjrjgfzOnDatastatus("4",sys_User.getLoginid());
//					out.write("1");
//				}else {
//					out.write("0");
//				}
//			}else if("some".equals(checktype)) {
//				String rows = request.getParameter("rows");
//				// 先转成json数组再将json转化成list数组
//				JSONArray jsonArray = JSONArray.fromObject(rows);
//				@SuppressWarnings("unchecked")
//				List<Xjrjgfz> list = JSONArray.toList(jsonArray, new Xjrjgfz(), new JsonConfig());
//				result = CreateMessageUtils.shengChengBaoWen(messagepath,filename,list,TableCodeEnum.JRJGFZJCXX);
//				if("1".equals(result)) {
//					for(int i=0;i<list.size();i++) {
//						list.get(i).setDatastatus("4");
//					}
//					xs.Save(list);
//					out.write("1");
//				}else {
//					out.write("0");
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	// 表间校验
	public void inTableCalibration(Map<String,String> setMap,List<String> errorId,Map<String,String> whereMap,LinkedHashMap<String, String> errorMsg,String sql,String errorcode,String errorinfo) {
		List<Object[]> exist = customSqlUtil.executeQuery(sql);
		if (exist!=null && exist.size()>0){
			setMap.put("checkStatus","2");
			setMap.put("operationname"," ");
			exist.forEach(errId->{
				errorId.add(String.valueOf(errId[0]));
				whereMap.put("id", String.valueOf(errId[0]));
				if(!errorMsg.containsKey(String.valueOf(errId[0]))) {
					errorMsg.put(String.valueOf(errId[0]), errorcode+":"+String.valueOf(errId[1])+"]->\r\n");
				}
				String str = errorMsg.get(String.valueOf(errId[0]));
				str = str + errorinfo+"|";
				errorMsg.put(String.valueOf(errId[0]), str);
			});
			whereMap.clear();
			setMap.clear();
			exist.clear();
		}
	}
	
}
