package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XdkgrfsxxCommonExcel;
import com.geping.etl.UNITLOAN.entity.report.Xftykhxbl;
import com.geping.etl.UNITLOAN.entity.report.Xgrdkfsxx;
import com.geping.etl.UNITLOAN.service.report.*;
import com.geping.etl.UNITLOAN.util.*;
import com.geping.etl.UNITLOAN.util.check.CheckAllData2;
import com.geping.etl.UNITLOAN.util.check.CheckAllData3;
import com.geping.etl.UNITLOAN.util.check.CheckAllDataGr;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @Author: chenggen
 * @Date: 17:03 2020/11/06
 */
@RestController
public class XdkgrfsxxController {

    @Autowired
    private XdkgrfsxxService xdkgrfsxxService;

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private ExcelUploadUtil excelUploadUtil;

    @Autowired
    private CustomSqlUtil customSqlUtil;


    @Autowired
    private XcommonService commonService;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private XCheckRuleService checkRuleService;

    @Autowired
    private XDataBaseTypeUtil dataBaseTypeUtil;
    
    @Autowired
    private DepartUtil departUtil;

    private Sys_UserAndOrgDepartment sys_user;

    private final String tableName="xgrdkfsxx";

    private final String fileName="个人贷款发生额信息";

    private static String target="";

    //查询数据
    @PostMapping("XGetXgrdkfsxxData")
    public ResponseResult XGetXdkfsxxData(int page, int rows,String finorgcodeParam,String loancontractcodeParam,String operationnameParam,String loanbrowcodeParam,
       String browidcodeParam,String isfarmerloanParam,String checkstatusParam,String datastatus){
        page = page - 1;
        sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");

        String departId = departUtil.getDepart(sys_user);
        Specification specification=new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                //待提交
                predicates.add(criteriaBuilder.equal(root.get("datastatus"), datastatus));

                predicates.add(criteriaBuilder.like(root.get("departid"), departId));
                if (StringUtils.isNotBlank(finorgcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("finorgcode"), "%"+finorgcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(loancontractcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("loancontractcode"), "%"+loancontractcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(loanbrowcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("loanbrowcode"), "%"+loanbrowcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(browidcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("browidcode"), "%"+browidcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(isfarmerloanParam)){
                    predicates.add(criteriaBuilder.like(root.get("isfarmerloan"), "%"+isfarmerloanParam+"%"));
                }
                if (StringUtils.isNotBlank(checkstatusParam)){
                    predicates.add(criteriaBuilder.equal(root.get("checkstatus"), checkstatusParam));
                }
                if (StringUtils.isNotBlank(operationnameParam)){
                    predicates.add(criteriaBuilder.equal(root.get("operationname"), operationnameParam));
                }
                predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
        PageRequest pageRequest = new PageRequest(page, rows, sort);
        Page<Xgrdkfsxx> all = xdkgrfsxxService.findAll(specification, pageRequest);
        List<Xgrdkfsxx> content = all.getContent();
        long totalCount = all.getTotalElements();
        return ResponseResult.success(totalCount,content);
    }

    //导入
    @PostMapping(value = "XimmportExcelSeven",produces = "text/plain;charset=UTF-8")
    public void XimmportExcelTwo(HttpServletRequest request,HttpServletResponse response){
        sys_user=((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        File uploadFile = null;
        OPCPackage opcPackage=null;
        PrintWriter out = null;
        Boolean aBoolean=false;
        try {
            out = response.getWriter();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multiRequest.getFile("excelfile");
            JSONObject json = new JSONObject();
            if (file == null) {
                json.put("msg", "导入文件为空");
            } else {
                uploadFile = excelUploadUtil.uploadFile(file);
                opcPackage = OPCPackage.open(uploadFile);
                String[] headers = {"金融机构代码","内部机构号", "金融机构地区代码", "借款人证件类型", "借款人证件代码",
                        "借款人地区代码","贷款借据编码", "贷款合同编码", "贷款产品类别", "贷款发放日期", "贷款到期日期", "贷款实际终止日期", "币种", "贷款发生金额", "贷款发生金额折人民币",
                        "利率是否固定", "利率水平", "贷款定价基准类型", "基准利率", "贷款财政扶持方式", "贷款利率重新定价日", "贷款担保方式",
                        "是否首次贷款", "贷款状态", "资产证券化产品代码", "贷款重组方式", "发放/收回标识", "交易流水号", "贷款用途","数据日期"
                };
                String[] values = {"finorgcode", "finorgincode", "finorgareacode", "isfarmerloan", "browidcode", "browareacode",
                		"loanbrowcode", "loancontractcode","loanprocode", "loanstartdate", "loanenddate", "loanactenddate", "loancurrency",
                		"loanamt", "loancnyamt", "rateisfix", "ratelevel", "loanfixamttype", "rate", "loanfinancesupport",
                		"loanraterepricedate", "gteemethod", "isplatformloan", "loanstatus", "assetproductcode", "loanrestructuring", "givetakeid",
                		"transactionnum", "issupportliveloan", "sjrq"
                };
                String[] rateFields = {"rate","ratelevel"};
                String[] amountFields = {"loanamt","loancnyamt"};
                String[] dateFields = {"loanstartdate", "loanenddate", "loanactenddate","loanraterepricedate","sjrq"};
//                List<Xftykhxbl> xftykhxblList = ftykhxblService.findAll();
//                if (xftykhxblList.size()>0){
//                    aBoolean = knowledgeIsRepeat(xftykhxblList);
//                }
                int id = commonService.getMaxId("xgrdkfsxxh");
                XdkgrfsxxCommonExcel commonExcel = new XdkgrfsxxCommonExcel(sys_user, opcPackage, customSqlUtil, new Xgrdkfsxx(), headers, values, amountFields,dateFields, null,rateFields,id,xdkgrfsxxService);
                commonExcel.process(0);
                String departId = departUtil.getDepart(sys_user);
                if (commonExcel.msg.toString().length() == 0) {
                	commonService.importDelete(tableName,departId);
                    commonExcel.process(1);
                    json.put("msg","导入成功");
                }else {
                    json.put("msg",commonExcel.msg.toString());
                }
//                if (aBoolean && json.getString("msg").equals("导入成功")){
//                    json.put("msg","补录表中有相同客户号码的数据");
//                }


            }
            String logContext="导入成功".equals(json.getString("msg")) || "补录表中有相同客户号码的数据".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
            customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
            out.write(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                opcPackage.close();
                //将上传的文件删除
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    //校验
    @PostMapping("XCheckDataSeven")
    public void XCheckDataTwo(HttpServletRequest request, HttpServletResponse response,String finorgcodeParam,String loancontractcodeParam,String loanbrowcodeParam){
        long a = System.currentTimeMillis();
        //记录是否有错
        boolean b=false;
        boolean sqlServer = dataBaseTypeUtil.equalsSqlServer();
        boolean isFirst = true;
        sys_user = ((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        List<SUpOrgInfoSet> sUpOrgInfoSets = suisService.findAll();
        target=sUpOrgInfoSets.get(0).getMessagepath();
        String departId = departUtil.getDepart(sys_user);

        String checklimit = sUpOrgInfoSets.get(0).getChecklimit();
        int checklimitNum = 0;
        Map<String,String> indexMap = null;//索引map
        try {
            new BigDecimal(checklimit);
            checklimitNum = Integer.valueOf(checklimit).intValue();
        }catch(Exception e) {

        }
        int count = 0;
        StringBuffer limitBuffer  = new StringBuffer("");
        Map<String,String> limitMap = new HashMap<String,String>();


        JSONObject json = new JSONObject();
        String id = request.getParameter("id");
        LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
        LinkedHashMap<String, String> errorMsg2 = new LinkedHashMap<String, String>();
        List<Integer> errorId0 = new ArrayList<>();//表间校验的错误id

        List<String> errorId = new ArrayList<>();
        List<String> rightId = new ArrayList<>();
        Map<String,String> whereMap=new HashMap<>();
        Map<String,String> setMap=new HashMap<>();
        setMap.put("operator",sys_user.getLoginid());
        setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//        List<String> splitList = Arrays.asList(id.split(","));//返回固定长度的ArrayList
        String whereStr=" where datastatus='0' and checkstatus='0' ";
        String whereStra=" where a.datastatus='0' and a.checkstatus='0' ";
        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            whereStr = whereStr + " and id in(" + id + ")";
            whereStra = whereStra + " and a.id in(" + id + ")";
        }else {
            if (StringUtils.isNotBlank(finorgcodeParam)){
                whereStr = whereStr + " and finorgcode like '%"+ finorgcodeParam +"%'";
                whereStra = whereStra + " and a.finorgcode like '%"+ finorgcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(loancontractcodeParam)){
                whereStr = whereStr + " and loancontractcode like '%"+ loancontractcodeParam +"%'";
                whereStra = whereStra + " and a.loancontractcode like '%"+ loancontractcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(loanbrowcodeParam)){
                whereStr = whereStr + " and loanbrowcode like '%"+ loanbrowcodeParam +"%'";
                whereStra = whereStra + " and a.loanbrowcode like '%"+ loanbrowcodeParam +"%'";
            }
            whereStr = whereStr + " and departid like '%"+ departId +"%'";
            whereStra = whereStra + " and a.departid like '%"+ departId +"%'";
        }
        String sql="select * from xgrdkfsxx "+whereStr;
        String sqlcount="select count(*) from "+tableName+" "+whereStr;
        Connection connection = null;
        ResultSet resultSet=null;
        ResultSet rsCount=null;
        PreparedStatement ps = null;
        int total = 0;
        int size = 100000;
        int page = 0;
        //标识名称
        String[] errorcode = {"贷款合同编码","贷款借据编号"};
        String errorinfo;
        String sqlexist;
        try {
            connection = JDBCUtils.getConnection();
            rsCount = JDBCUtils.Query(connection, ps, sqlcount);

            if (rsCount.next()) {	//没有可校验的数据
                total = rsCount.getInt(1);
            }
            if(total == 0) {
                json.put("msg","-1");
                try {
                    PrintWriter out = response.getWriter();
                    out.write(json.toString());
                    out.flush();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }
            page = total % size == 0 ? total/size : total/size+1;
            long aaa = System.currentTimeMillis();
                //在校验之前查重

                //校验开关集合
                List<String> checknumList = checkRuleService.getChecknum("xgrdkfsxx");

/*
            if(dataBaseTypeUtil.equalsMySql()) {
                //需要加索引的字段
                Map<String,String> fieldMap = new HashMap<String,String>();
                //个人发生额
                fieldMap.put("sjrq", "xgrdkfsxx");
                fieldMap.put("finorgincode", "xgrdkfsxx");
                fieldMap.put("loancontractcode", "xgrdkfsxx");
                fieldMap.put("loanbrowcode", "xgrdkfsxx");
                fieldMap.put("transactionnum", "xgrdkfsxx");
                fieldMap.put("givetakeid", "xgrdkfsxx");
                fieldMap.put("browidcode", "xgrdkfsxx");

                //个人发生额历史
                fieldMap.put("sjrq#", "xgrdkfsxxh");
                fieldMap.put("finorgincode#", "xgrdkfsxxh");
                fieldMap.put("loancontractcode#", "xgrdkfsxxh");
                fieldMap.put("loanbrowcode#", "xgrdkfsxxh");
                fieldMap.put("transactionnum#", "xgrdkfsxxh");
                fieldMap.put("givetakeid#", "xgrdkfsxxh");

                //存量个人信息
                fieldMap.put("financeorginnum", "xclgrdkxx");
                fieldMap.put("contractcode", "xclgrdkxx");
                fieldMap.put("receiptcode", "xclgrdkxx");
                fieldMap.put("sjrq##", "xclgrdkxx");

                //存量个人信息历史
                fieldMap.put("financeorginnum#", "xclgrdkxxh");
                fieldMap.put("contractcode#", "xclgrdkxxh");
                fieldMap.put("receiptcode#", "xclgrdkxxh");
                fieldMap.put("sjrq###", "xclgrdkxxh");


                //个人客户信息表
                fieldMap.put("sjrq####", "xgrkhxx");
                fieldMap.put("customercode", "xgrkhxx");

                //个人客户信息表历史
                fieldMap.put("sjrq#####", "xgrkhxxh");
                fieldMap.put("customercode#", "xgrkhxxh");

                indexMap = addIndex(connection, fieldMap);

                long bindex = System.currentTimeMillis();
                System.out.println("创建索引耗时："+(bindex -a) +"毫秒");
            }
*/





            if(checklimitNum == 0) {
                if (checknumList.contains("JS2391")){
                    if (sqlServer){
                        errorinfo  = "数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号+发放/收回标识不唯一";
                        sqlexist ="select id,loancontractcode,loanbrowcode from xgrdkfsxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid from xgrdkfsxx where operationname != '申请删除' group by sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.finorgincode = t2.finorgincode and t1.loancontractcode = t2.loancontractcode and t1.loanbrowcode = t2.loanbrowcode and t1.transactionnum = t2.transactionnum and t1.givetakeid = t2.givetakeid)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);

                        errorinfo = "数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号+发放/收回标识不唯一（历史表）";
                        sqlexist ="select id,loancontractcode,loanbrowcode from xgrdkfsxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid from xgrdkfsxxh group by sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.finorgincode = t2.finorgincode and t1.loancontractcode = t2.loancontractcode and t1.loanbrowcode = t2.loanbrowcode and t1.transactionnum = t2.transactionnum and t1.givetakeid = t2.givetakeid)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                    }else {
                        errorinfo  = "数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号+发放/收回标识不唯一";
                        sqlexist ="select id,loancontractcode,loanbrowcode from xgrdkfsxx "+whereStr+" and (sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid) in (select sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid from xgrdkfsxx where operationname != '申请删除' group by sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid having  count(1) > 1)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);

                        errorinfo = "数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号+发放/收回标识不唯一（历史表）";
                        sqlexist = "select id,loancontractcode,loanbrowcode from xgrdkfsxx "+whereStr+" and (sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid) in (select sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid from xgrdkfsxxh group by sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid having  count(1) > 0)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                    }
                }



                // 表间校验
                //TODO
                // 1.贷款产品类别与存量个人贷款信息的贷款产品类别应该一致(个人贷款发生额信息 a AND 存量个人贷款信息 b)
                if (checknumList.contains("JS1832")){
                    errorinfo = "贷款产品类别与存量个人贷款信息的贷款产品类别应该一致";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xgrdkfsxx a INNER JOIN  xclgrdkxx b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.loanprocode != b.productcetegory  and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);

                    errorinfo = "贷款产品类别与存量个人贷款信息的贷款产品类别应该一致（历史表）";
                    sqlexist ="select distinct a.id,a.loancontractcode,a.loanbrowcode from xgrdkfsxx a INNER JOIN  xclgrdkxxh b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.loanprocode != b.productcetegory";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                }

                //TODO
                // 9.借款人证件代码与存量个人贷款信息的借款人证件代码应该一致(个人贷款发生额信息 a AND 存量个人贷款信息 b)
                if (checknumList.contains("JS1830")){
                    errorinfo = "借款人证件代码与存量个人贷款信息的借款人证件代码应该一致";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xgrdkfsxx a INNER JOIN xclgrdkxx b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.browidcode != b.brroweridnum and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);

                    errorinfo = "借款人证件代码与存量个人贷款信息的借款人证件代码应该一致（历史表）";
                    sqlexist ="select distinct a.id,a.loancontractcode,a.loanbrowcode from xgrdkfsxx a INNER JOIN xclgrdkxxh b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.browidcode != b.brroweridnum";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                }

                //TODO
                // 11.借款人证件类型与存量个人贷款信息的借款人证件类型应该一致(个人贷款发生额信息 a AND 存量个人贷款信息 b)
                if (checknumList.contains("JS1829")){
                    errorinfo = "借款人证件类型与存量个人贷款信息的借款人证件类型应该一致";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xgrdkfsxx a INNER JOIN xclgrdkxx  b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.isfarmerloan != b.isfarmerloan and b.operationname != '申请删除'";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);

                    errorinfo = "借款人证件类型与存量个人贷款信息的借款人证件类型应该一致（历史表）";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xgrdkfsxx a INNER JOIN xclgrdkxxh b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.isfarmerloan != b.isfarmerloan";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                }



                // 10.个人客户的借款人证件代码应该在个人客户基础信息.客户证件代码中存在(个人贷款发生额信息 a AND 个人客户基础信息 b)
                if (checknumList.contains("JS2169")){
                    errorinfo = "个人客户的借款人证件代码应该在个人客户基础信息.客户证件代码中存在";
                    sqlexist="select a.id,a.loancontractcode,a.loanbrowcode  from xgrdkfsxx a" +whereStra+ "and (NOT EXISTS (SELECT 1 FROM xgrkhxx b WHERE a.sjrq=b.sjrq AND a.browidcode =b.customercode  AND b.operationname != '申请删除') and NOT EXISTS (SELECT 1 FROM xgrkhxxh b WHERE a.sjrq=b.sjrq AND a.browidcode =b.customercode))";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
                }
                errorId.clear();
            }else {
                if (checknumList.contains("JS2391")){
                    if (sqlServer){
                        errorinfo  = "数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号+发放/收回标识不唯一";
                        sqlexist ="select id,loancontractcode,loanbrowcode from xgrdkfsxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid from xgrdkfsxx where operationname != '申请删除' group by sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.finorgincode = t2.finorgincode and t1.loancontractcode = t2.loancontractcode and t1.loanbrowcode = t2.loanbrowcode and t1.transactionnum = t2.transactionnum and t1.givetakeid = t2.givetakeid)";
                        executeCheckSqlLimit("数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号+发放/收回标识不唯一",limitMap, checklimitNum, errorId0, sqlexist);

                        errorinfo = "数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号+发放/收回标识不唯一（历史表）";
                        sqlexist ="select id,loancontractcode,loanbrowcode from xgrdkfsxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid from xgrdkfsxxh group by sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.finorgincode = t2.finorgincode and t1.loancontractcode = t2.loancontractcode and t1.loanbrowcode = t2.loanbrowcode and t1.transactionnum = t2.transactionnum and t1.givetakeid = t2.givetakeid)";
                        executeCheckSqlLimit("数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号+发放/收回标识不唯一（历史表）", limitMap, checklimitNum, errorId0, sqlexist);
                    }else {
                        errorinfo  = "数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号+发放/收回标识不唯一";
                        sqlexist ="select id,loancontractcode,loanbrowcode from xgrdkfsxx "+whereStr+" and (sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid) in (select sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid from xgrdkfsxx where operationname != '申请删除' group by sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid having  count(1) > 1)";
                        executeCheckSqlLimit("数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号+发放/收回标识不唯一",  limitMap, checklimitNum, errorId0, sqlexist);

                        errorinfo = "数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号+发放/收回标识不唯一（历史表）";
                        sqlexist = "select id,loancontractcode,loanbrowcode from xgrdkfsxx "+whereStr+" and (sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid) in (select sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid from xgrdkfsxxh group by sjrq,finorgincode,loancontractcode,loanbrowcode,transactionnum,givetakeid having  count(1) > 0)";
                        executeCheckSqlLimit("数据日期+金融机构代码+贷款借据编码+贷款合同编码+交易流水号+发放/收回标识不唯一（历史表）", limitMap, checklimitNum, errorId0, sqlexist);
                    }
                }



                // 表间校验
                //TODO
                // 1.贷款产品类别与存量个人贷款信息的贷款产品类别应该一致(个人贷款发生额信息 a AND 存量个人贷款信息 b)
                if (checknumList.contains("JS1832")){
                    errorinfo = "贷款产品类别与存量个人贷款信息的贷款产品类别应该一致";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xgrdkfsxx a INNER JOIN  xclgrdkxx b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.loanprocode != b.productcetegory  and b.operationname != '申请删除'";
                    executeCheckSqlLimit("贷款产品类别与存量个人贷款信息的贷款产品类别应该一致",limitMap, checklimitNum, errorId0, sqlexist);

                    errorinfo = "贷款产品类别与存量个人贷款信息的贷款产品类别应该一致（历史表）";
                    sqlexist ="select distinct a.id,a.loancontractcode,a.loanbrowcode from xgrdkfsxx a INNER JOIN  xclgrdkxxh b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.loanprocode != b.productcetegory";
                    executeCheckSqlLimit("贷款产品类别与存量个人贷款信息的贷款产品类别应该一致（历史表）",limitMap, checklimitNum, errorId0, sqlexist);
                }

                //TODO
                // 9.借款人证件代码与存量个人贷款信息的借款人证件代码应该一致(个人贷款发生额信息 a AND 存量个人贷款信息 b)
                if (checknumList.contains("JS1830")){
                    errorinfo = "借款人证件代码与存量个人贷款信息的借款人证件代码应该一致";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xgrdkfsxx a INNER JOIN xclgrdkxx b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.browidcode != b.brroweridnum and b.operationname != '申请删除'";
                    executeCheckSqlLimit("借款人证件代码与存量个人贷款信息的借款人证件代码应该一致", limitMap, checklimitNum, errorId0, sqlexist);

                    errorinfo = "借款人证件代码与存量个人贷款信息的借款人证件代码应该一致（历史表）";
                    sqlexist ="select distinct a.id,a.loancontractcode,a.loanbrowcode from xgrdkfsxx a INNER JOIN xclgrdkxxh b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.browidcode != b.brroweridnum";
                    executeCheckSqlLimit("借款人证件代码与存量个人贷款信息的借款人证件代码应该一致（历史表）",limitMap, checklimitNum, errorId0, sqlexist);
                }

                //TODO
                // 11.借款人证件类型与存量个人贷款信息的借款人证件类型应该一致(个人贷款发生额信息 a AND 存量个人贷款信息 b)
                if (checknumList.contains("JS1829")){
                    errorinfo = "借款人证件类型与存量个人贷款信息的借款人证件类型应该一致";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xgrdkfsxx a INNER JOIN xclgrdkxx  b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.isfarmerloan != b.isfarmerloan and b.operationname != '申请删除'";
                    executeCheckSqlLimit("借款人证件类型与存量个人贷款信息的借款人证件类型应该一致",limitMap, checklimitNum, errorId0, sqlexist);

                    errorinfo = "借款人证件类型与存量个人贷款信息的借款人证件类型应该一致（历史表）";
                    sqlexist = "select distinct a.id,a.loancontractcode,a.loanbrowcode from xgrdkfsxx a INNER JOIN xclgrdkxxh b on a.finorgincode = b.financeorginnum and a.loancontractcode = b.contractcode and a.loanbrowcode = b.receiptcode and a.sjrq = b.sjrq "+whereStra+" and a.isfarmerloan != b.isfarmerloan";
                    executeCheckSqlLimit("借款人证件类型与存量个人贷款信息的借款人证件类型应该一致（历史表）",limitMap, checklimitNum, errorId0, sqlexist);
                }



                // 10.个人客户的借款人证件代码应该在个人客户基础信息.客户证件代码中存在(个人贷款发生额信息 a AND 个人客户基础信息 b)
                if (checknumList.contains("JS2169")){
                    errorinfo = "个人客户的借款人证件代码应该在个人客户基础信息.客户证件代码中存在";
                    sqlexist="select a.id,a.loancontractcode,a.loanbrowcode  from xgrdkfsxx a" +whereStra+ "and (NOT EXISTS (SELECT 1 FROM xgrkhxx b WHERE a.sjrq=b.sjrq AND a.browidcode =b.customercode  AND b.operationname != '申请删除') and NOT EXISTS (SELECT 1 FROM xgrkhxxh b WHERE a.sjrq=b.sjrq AND a.browidcode =b.customercode))";
                    executeCheckSqlLimit("个人客户的借款人证件代码应该在个人客户基础信息.客户证件代码中存在", limitMap, checklimitNum, errorId0, sqlexist);
                }

            }

            //TODO
                // 12.金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致(单位贷款发生额信息 a AND 金融机构（分支机构）基础信息 b)
//                errorinfo = "金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致";
//                sqlexist = "select a.id,a.loancontractcode from xdkfsxx a INNER JOIN xjrjgfz b on a.finorgincode = b.inorgnum and a.sjrq = b.sjrq "+whereStra+" and a.finorgcode != b.finorgcode";
//                inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);
//
//                errorinfo = "金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致（历史表）";
//                sqlexist = "select a.id,a.loancontractcode from xdkfsxx a INNER JOIN xjrjgfzh b on a.finorgincode = b.inorgnum and a.sjrq = b.sjrq "+whereStra+" and a.finorgcode != b.finorgcode";
//                inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo);

            long bbb = System.currentTimeMillis();
            System.out.println("表间校验用时："+(bbb-aaa)/1000+"秒");
            outer: for(int i = 0; i < page; i++) {
                if(i != 0) {
                    isFirst = false;
                }

                resultSet = JDBCUtils.Query(sql, 0, size);

                Xgrdkfsxx xgrdkfsxx = null;
                if(checklimitNum == 0) {
                    while (true) {
                        if (!resultSet.next()) break;
                        xgrdkfsxx = new Xgrdkfsxx();
                        Stringutil.getEntity(resultSet, xgrdkfsxx);
                        CheckAllData2.checkXgrdkfsxx(customSqlUtil,checknumList, xgrdkfsxx, errorMsg,errorMsg2, errorId, rightId);
                    }
                }else {
                    while (true) {
                        if (!resultSet.next()) break;
                        xgrdkfsxx = new Xgrdkfsxx();
                        Stringutil.getEntity(resultSet, xgrdkfsxx);
                        CheckAllDataGr.checkXgrdkfsxxLimit(customSqlUtil, xgrdkfsxx, errorId0, errorId, rightId, checknumList, limitMap, checklimitNum);
                    }
                }
                resultSet.close();

                //修改状态 和 下载
                if (errorId!=null && errorId.size()>0) {
                    b = true;
                    if (checklimitNum == 0) {

                    //-下载到本地
                    StringBuffer errorMsgAll = new StringBuffer("");
                    for (Map.Entry<String, String> entry : errorMsg2.entrySet()) {
                        errorMsgAll.append(entry.getValue() + "\r\n");
                    }
                    DownloadUtil.downLoad2(errorMsgAll.toString(), target + File.separator + "checkout", fileName + ".txt", isFirst);
                    errorMsg2.clear();
                    errorMsgAll.setLength(0);
                    errorMsgAll = null;
                }
                    errorMsg2.clear();

                    setMap.put("checkStatus","2");
                    setMap.put("operationname"," ");
                    customSqlUtil.updateByWhereBatchGR(tableName, setMap, errorId);
                    errorId.clear();

                }
                if (rightId!=null && rightId.size()>0){
                    setMap.put("checkStatus","1");
                    setMap.put("operationname"," ");
                    customSqlUtil.updateByWhereBatchGR(tableName, setMap, rightId);
                    rightId.clear();
                }

            }



        } catch (Exception throwables) {
            throwables.printStackTrace();
        }finally {
            if(indexMap != null && indexMap.size() != 0) {
                dropIndex(connection, indexMap);
            }
            if(rsCount != null){
                try {
                    rsCount.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (resultSet!=null){
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(CheckAllData2.fzList != null) {
                CheckAllData2.fzList.clear();
                CheckAllData2.fzList = null;
            }
            if(CheckAllData2.frList != null) {
                CheckAllData2.frList.clear();
                CheckAllData2.frList = null;
            }
        }

        //校验结束
            if (b){
                json.put("msg","0");
            }else {
                json.put("msg","1");
            }

        if(checklimitNum != 0) {
            if(CheckAllDataGr.limitFlag) {
                //limitBuffer.append("阈值量:"+checklimitNum+"\r\n");
                limitBuffer.append("提示错误种类数量:"+limitMap.size()+"\r\n");
                limitBuffer.append("因错误数据较多,目前只显示错误示例数据；\r\n");
                limitBuffer.append("\r\n");
            }
            for(Map.Entry<String, String> entry: limitMap.entrySet()) {
                limitBuffer.append(entry.getKey()+"："+errorcode[0]+"+"+errorcode[1]+"->\r\n");
                limitBuffer.append(entry.getValue().substring(entry.getValue().lastIndexOf("#")+1));
            }
        }

        if(limitBuffer.length() != 0) {
            DownloadUtil.downLoad2(limitBuffer.toString(),target+File.separator+"checkout",fileName+".txt",isFirst);

        }
        limitBuffer.setLength(0);
//        if (rightId!=null && rightId.size()>0){
//            setMap.put("checkStatus","1");
//            setMap.put("operationname"," ");
//            customSqlUtil.updateByWhereBatchGR(tableName, setMap, rightId);
//            rightId.clear();
//        }
        customSqlUtil.saveLog("个人贷款发生额校验成功","校验");
        long b2 = System.currentTimeMillis();
        System.out.println("校验用时： "+(b2-a)/1000+"秒");
        try {
            PrintWriter out = response.getWriter();
            out.write(json.toString());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            CheckAllDataGr.limitFlag = false;
        }
    }

    @GetMapping("XDownLoadCheckSeven")
    public void XDownLoadCheckTwo(HttpServletResponse response) {
        if (StringUtils.isBlank(target)){
            target=suisService.findAll().get(0).getMessagepath();
        }
        DownloadUtil.downLoadCheckFile(response,target + File.separator + "checkout",fileName+".txt");
    }
    
    // 表间校验
    public void inTableCalibration(Map<String,String> setMap,List<String> errorId,Map<String,String> whereMap,LinkedHashMap<String, String> errorMsg,String sql,String[] errorcode,String errorinfo) {
        List<Object[]> exist = customSqlUtil.executeQuery(sql);
	    if (exist!=null && exist.size()>0){
	        setMap.put("checkStatus","2");
	        setMap.put("operationname"," ");
            exist.forEach(errId->{
	            errorId.add(String.valueOf(errId[0]));
	            whereMap.put("id", String.valueOf(errId[0]));
                if(!errorMsg.containsKey(String.valueOf(errId[0]))) {
                    errorMsg.put(String.valueOf(errId[0]), errorcode[0]+":"+String.valueOf(errId[1])+"，"+errorcode[1]+":"+String.valueOf(errId[2])+"]->\r\n");

                }
                String str = errorMsg.get(String.valueOf(errId[0]));
                str = str + errorinfo+"|";
                errorMsg.put(String.valueOf(errId[0]), str);
	        });
            whereMap.clear();
            setMap.clear();
            exist.clear();
	    }
    }

    //lsit数据字段重复 返回true
    private Boolean knowledgeIsRepeat(List<Xftykhxbl> orderList) {
        Set<Xftykhxbl> set = new TreeSet<Xftykhxbl>(new Comparator<Xftykhxbl>() {
            public int compare(Xftykhxbl a, Xftykhxbl b) {
                if (StringUtils.isNotBlank(a.getCustomernum())&&StringUtils.isNotBlank(b.getCustomernum())){
                    return a.getCustomernum().compareTo(b.getCustomernum());
                }

                return 100000;
            }
        });
        set.addAll(orderList);
        if (set.size() < orderList.size()) {
            return true;
        }
        return false;
    }

    public void executeCheckSqlLimit(String rule,Map<String,String> map,int checknumLimit,List<Integer> errorId,String sql) {
        List<Object[]> exist = customSqlUtil.executeQuery(sql);
        if (exist!=null && exist.size()>0){
            exist.forEach(errId->{
                errorId.add((Integer) errId[0]);
                CheckAllDataGr.putMap(rule, String.valueOf(errId[1]),String.valueOf(errId[2]), map, checknumLimit);
            });
            exist.clear();
        }
    }



    //创建索引
    public Map<String,String> addIndex(Connection conn,Map<String,String> fieldMap) {
        Statement ps = null;
        Map<String,String> indexMap = new HashMap<String,String>();
        try {
            ps = conn.createStatement();
            for(Map.Entry<String, String> entry : fieldMap.entrySet()) {
                String index = "index_"+Stringutil.getUUid();
                String sql = "CREATE INDEX "+index+" ON "+entry.getValue()+" ("+entry.getKey().replaceAll("#", "")+")";
                ps.addBatch(sql);
                indexMap.put(index, entry.getValue());
            }
            ps.executeBatch();
        }catch(Exception e) {
            e.printStackTrace();
            System.out.println("创建索引失败...");
        }finally {
            if(ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return indexMap;
    }

    //删除索引
    public void dropIndex(Connection conn,Map<String,String> indexMap) {
        Statement ps = null;
        try {
            ps = conn.createStatement();
            for(Map.Entry<String, String> entry : indexMap.entrySet()) {
                ps.addBatch("drop index "+entry.getKey()+" on "+entry.getValue()+"");
            }
            ps.executeBatch();
        }catch(Exception e) {
            e.printStackTrace();
            System.out.println("删除索引失败...");
        }finally {
            if(ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
