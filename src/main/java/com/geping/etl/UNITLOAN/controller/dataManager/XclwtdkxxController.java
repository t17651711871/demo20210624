package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.entity.report.Xcldkxx;
import com.geping.etl.UNITLOAN.entity.report.Xclwtdkxx;
import com.geping.etl.UNITLOAN.entity.report.Xftykhxbl;
import com.geping.etl.UNITLOAN.service.report.*;
import com.geping.etl.UNITLOAN.util.*;
import com.geping.etl.UNITLOAN.util.check.CheckAllData;
import com.geping.etl.UNITLOAN.util.check.CheckAllDataWt;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.utils.jdbc.JDBCUtils;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @Author: chenggen
 * @Date: 17:03 2020/6/9
 */
@RestController
public class XclwtdkxxController {

    @Autowired
    private XclwtdkxxService xclwtdkxxService;


    @Autowired
    private XCheckRuleService checkRuleService;

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private ExcelUploadUtil excelUploadUtil;

    @Autowired
    private XcommonService commonService;

    @Autowired
    private XftykhxblService ftykhxblService;

    @Autowired
    private CustomSqlUtil customSqlUtil;

    @Autowired
    private CalcCountUtil countUtil;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private XDataBaseTypeUtil dataBaseTypeUtil;
    
    @Autowired
    private DepartUtil departUtil;


    private Sys_UserAndOrgDepartment sys_user;

    private final String fileName="存量委托贷款基础数据信息";

    private String target="";

    private final String tableName="xclwtdkxx";
    //查询数据
    @PostMapping("XGetXclwtdkxxData")
    public ResponseResult XGetXclwtdkxxData(int page, int rows,String financeorgcodeParam,String contractcodeParam,String receiptcodeParam,
        String brroweridnumParam,String isfarmerloanParam,String checkstatusParam,String operationnameParam,String datastatus){
        page = page - 1;
        sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");

        String departId = departUtil.getDepart(sys_user);
        Specification specification=new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("datastatus"), datastatus));

                predicates.add(criteriaBuilder.like(root.get("departid"), departId));
                if (StringUtils.isNotBlank(financeorgcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("financeorgcode"), "%"+financeorgcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(contractcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("contractcode"), "%"+contractcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(receiptcodeParam)){
                    predicates.add(criteriaBuilder.like(root.get("receiptcode"), "%"+receiptcodeParam+"%"));
                }
                if (StringUtils.isNotBlank(brroweridnumParam)){
                    predicates.add(criteriaBuilder.like(root.get("brroweridnum"), "%"+brroweridnumParam+"%"));
                }
                if (StringUtils.isNotBlank(isfarmerloanParam)){
                    predicates.add(criteriaBuilder.like(root.get("isfarmerloan"), "%"+isfarmerloanParam+"%"));
                }
                if (StringUtils.isNotBlank(checkstatusParam)){
                    predicates.add(criteriaBuilder.equal(root.get("checkstatus"), checkstatusParam));
                }
                if (StringUtils.isNotBlank(operationnameParam)){
                    predicates.add(criteriaBuilder.equal(root.get("operationname"), operationnameParam));
                }
                predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
        PageRequest pageRequest = new PageRequest(page, rows, sort);
        Page<Xclwtdkxx> all = xclwtdkxxService.findAll(specification, pageRequest);
        List<Xclwtdkxx> content = all.getContent();
        long totalCount = all.getTotalElements();
        return ResponseResult.success(totalCount,content);
    }

    //导入
    @PostMapping(value = "XimmportExcelXclwt",produces = "text/plain;charset=UTF-8")
    public void XimmportExcel(HttpServletRequest request,HttpServletResponse response){
        sys_user=((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        File uploadFile = null;
        OPCPackage opcPackage=null;
        PrintWriter out = null;
        Boolean aBoolean=false;
        try {
            out = response.getWriter();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multiRequest.getFile("excelfile");
            JSONObject json = new JSONObject();
            if (file == null) {
                json.put("msg", "导入文件为空");
            } else {
                uploadFile = excelUploadUtil.uploadFile(file);
                opcPackage = OPCPackage.open(uploadFile);
                String[] headers = {"金融机构代码", "内部机构号", "金融机构地区代码", "借款人证件类型",
                        "借款人证件代码", "借款人国民经济部门", "借款人行业", "借款人地区代码", "借款人经济成分",
                        "借款人企业规模", "委托贷款借据编码", "委托贷款合同编码", "贷款实际投向", "委托贷款发放日期",
                        "委托贷款到期日期", "委托贷款展期到期日期", "币种", "委托贷款余额", "委托贷款余额折人民币",
                        "利率是否固定", "利率水平", "手续费金额折人民币", "贷款担保方式", "贷款质量", "贷款状态",
                        "委托人国民经济部门", "委托人证件类型", "委托人证件代码", "委托人行业", "委托人地区代码", "委托人经济成分", "委托人企业规模","贷款用途","数据日期"
                };
                String[] values = {"financeorgcode", "financeorginnum", "financeorgareacode", "isfarmerloan", "brroweridnum",
                        "isgreenloan", "brrowerindustry", "brrowerareacode", "inverstoreconomy", "enterprisescale", "receiptcode",
                        "contractcode", "loanactualdirection", "loanstartdate", "loanenddate", "extensiondate", "currency", "receiptbalance",
                        "receiptcnybalance", "interestisfixed", "interestislevel", "procedurebalance", "guaranteemethod", "loanquality",
                        "loanstatus", "bailorgreenloan", "bailorgfarmerloan", "bailorgidnum", "bailorgindustry", "bailorgareacode", "bailorgeconomy", "bailorgenterprise","issupportliveloan","sjrq"
                };
                String[] amountFields = {"receiptbalance", "receiptcnybalance","procedurebalance"};
                String[] dateFields = {"loanstartdate", "loanenddate", "extensiondate", "loaninterestrepricedate","sjrq"};

                String[] rateFields={"baseinterest","interestislevel"};
//                List<Xftykhxbl> xftykhxblList = ftykhxblService.findAll();
//                if (xftykhxblList.size()>0){
//                    aBoolean = knowledgeIsRepeat(xftykhxblList);
//                }
                XCommonExcel<Xclwtdkxx> commonExcel = new XCommonExcel<>(sys_user, opcPackage, customSqlUtil, new Xclwtdkxx(), headers, values,amountFields,dateFields,null,rateFields);
                commonExcel.process(0);
                String departId = departUtil.getDepart(sys_user);
                if (commonExcel.msg.toString().length() == 0) {
                	commonService.importDelete(tableName,departId);
                    commonExcel.process(1);
                    json.put("msg","导入成功");
                } else {
                    json.put("msg",commonExcel.msg.toString());
                }
//                if (aBoolean && json.getString("msg").equals("导入成功")){
//                    json.put("msg","补录表中有相同客户号码的数据");
//                }
            }
            String logContext="导入成功".equals(json.getString("msg")) || "补录表中有相同客户号码的数据".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
            customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
            out.write(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                opcPackage.close();
                //将上传的文件删除
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    //校验
    @PostMapping("XCheckDataWtdk")
    public void XCheckDataOne(HttpServletRequest request, HttpServletResponse response,String financeorgcodeParam,String contractcodeParam,String checkstatusParam){
        //记录是否有错
        boolean b=false;
        boolean sqlServer = dataBaseTypeUtil.equalsSqlServer();
        sys_user = ((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));

        List<SUpOrgInfoSet> sUpOrgInfoSets = suisService.findAll();
        target=sUpOrgInfoSets.get(0).getMessagepath();
        String departId = departUtil.getDepart(sys_user);

        JSONObject json = new JSONObject();
        String id = request.getParameter("id");
        System.out.println(id);

        LinkedHashMap<String, String> errorMsg = new LinkedHashMap<String, String>();
        List<String> errorId = new ArrayList<>();
        List<String> rightId = new ArrayList<>();
        Map<String,String> whereMap=new HashMap<>();
        Map<String,String> setMap=new HashMap<>();
        setMap.put("operator",sys_user.getLoginid());
        setMap.put("operationtime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

//        List<String> splitList = Arrays.asList(id.split(","));//返回固定长度的ArrayList
        String whereStr=" where datastatus='0' and checkstatus='0'";
        String whereStra=" where a.datastatus='0' and a.checkstatus='0' ";

        if (StringUtils.isNotBlank(id)){
            id = id.substring(0,id.length()-1);
            id = "'" + id.replace(",","','")  + "'";
            System.out.println(id);
            whereStr = whereStr + " and id in(" + id + ")";
            whereStra =whereStra+" and a.id in(" + id + ")";
        }else{
            if (StringUtils.isNotBlank(financeorgcodeParam)){
                whereStr = whereStr + " and financeorgcode like '%"+ financeorgcodeParam +"%'";
                whereStra= whereStra+" and a.financeorgcode like '%"+ financeorgcodeParam +"%'";
            }
            if (StringUtils.isNotBlank(contractcodeParam)){
                whereStr = whereStr + " and contractcode like '%"+ contractcodeParam +"%'";
                whereStra=whereStra +" and a.contractcode like '%"+ contractcodeParam +"%'";
            }
            whereStr = whereStr + " and departid like '%"+ departId +"%'";
            whereStra = whereStra + " and a.departid like '%"+ departId +"%'";
            /*
            if (StringUtils.isNotBlank(checkstatusParam)){
                sql = sql + " and checkstatus ='"+ checkstatusParam +"'";
            }*/
        }

        String sql="select * from xclwtdkxx "+whereStr;
        Connection connection = null;
        ResultSet resultSet=null;
        String errorcode = "委托贷款合同编码";
        String errorcode2 = "委托贷款借据编号";
        try {
            connection = JDBCUtils.getConnection();
            resultSet = JDBCUtils.Query(connection, sql);

            if (!resultSet.next()) {	//没有可校验的数据
                json.put("msg","-1");
                try {
                    PrintWriter out = response.getWriter();
                    out.write(json.toString());
                    out.flush();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }else {
                //校验规则list
                List<String> xcldkxx1List = checkRuleService.getChecknum("xclwtdkxx");

                if (xcldkxx1List.contains("JS2305")){
                    if (sqlServer){
                        String errorinfo = "数据日期+金融机构代码+委托贷款借据编码不唯一";
                        String sqlexist = "select id,contractcode,receiptcode from xclwtdkxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,financeorgcode,receiptcode from xclwtdkxx where operationname != '申请删除' group by sjrq,financeorgcode,receiptcode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.receiptcode = t2.receiptcode)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                        sqlexist = "select id,contractcode,receiptcode from xclwtdkxx t1 "+whereStra.replace("a.", "t1.")+" and exists (select 1 from (select sjrq,financeorgcode,receiptcode from xclwtdkxxh group by sjrq,financeorgcode,receiptcode having count(1) > 1) t2 where t1.sjrq = t2.sjrq and t1.financeorgcode = t2.financeorgcode and t1.receiptcode = t2.receiptcode)";
                        errorinfo = "数据日期+金融机构代码+委托贷款借据编码不唯一（历史表）";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }else {
                        String errorinfo = "数据日期+金融机构代码+贷款借据编码不唯一";
                        String sqlexist = "select id,contractcode,receiptcode from xclwtdkxx "+whereStr+"  and (sjrq,financeorgcode,receiptcode) in (select sjrq,financeorgcode,receiptcode from xclwtdkxx where operationname != '申请删除' group by sjrq,financeorgcode,receiptcode HAVING COUNT(1)>1)";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                        sqlexist = "select id,contractcode,receiptcode from xclwtdkxx "+whereStr+"  and (sjrq,financeorgcode,receiptcode) in (select sjrq,financeorgcode,receiptcode from xclwtdkxxh group by sjrq,financeorgcode,receiptcode HAVING COUNT(1)>0)";
                        errorinfo = "数据日期+金融机构代码+贷款借据编码不唯一（历史表）";
                        inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                    }
                }

//                errorinfo = "当担保合同信息中担保人证件代码不为空时，贷款担保方式应该为保证或组合";
//                sqlexist = "select a.id id , a.contractcode,a.receiptcode  from xcldkxx a inner join (SELECT * FROM xdkdbht GROUP BY financeorginnum,sjrq,loancontractcode)b on  b.loancontractcode=a.contractcode and a.sjrq=b.sjrq AND a.financeorginnum=b.financeorginnum" + whereStra + "AND a.guaranteemethod not like 'C%' and  a.guaranteemethod not like 'E%' And b.gteeidnum is not null AND LTRIM(b.gteeidnum)!='' AND b.operationname != '申请删除'";
//                inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
//
//                errorinfo = "当担保合同信息中担保人证件代码不为空时，贷款担保方式应该为保证或组合(历史表)";
//                sqlexist = "select a.id id , a.contractcode,a.receiptcode   from xcldkxx a inner join (SELECT * FROM xdkdbhth GROUP BY financeorginnum,sjrq,loancontractcode)b on  b.loancontractcode=a.contractcode and a.sjrq=b.sjrq AND a.financeorginnum=b.financeorginnum" + whereStra + "AND a.guaranteemethod not like 'C%' and  a.guaranteemethod not like 'E%' And b.gteeidnum is not null AND LTRIM(b.gteeidnum)!=''";
//                inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
//
//                if (xcldkxx1List.contains("JS1679")){
//                    String errorinfo = "借款人行业与非同业单位客户基础信息的所属行业应该一致";
//                    String sqlexist = "select distinct a.id id, a.contractcode,a.receiptcode   from xcldkxx a inner join  xftykhx b on a.brroweridnum=b.customercode and a.sjrq=b.sjrq " + whereStra + "AND a.brrowerindustry != b.industry AND b.operationname != '申请删除'";
//                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
//
//                    errorinfo = "借款人行业与非同业单位客户基础信息的所属行业应该一致(历史表)";
//                    sqlexist = "select distinct a.id id, a.contractcode,a.receiptcode   from xcldkxx a inner join  xftykhxh b on a.brroweridnum=b.customercode and a.sjrq=b.sjrq " + whereStra + "AND a.brrowerindustry != b.industry";
//                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
//                }

//                if (xcldkxx1List.contains("JS1680")){
//                    String errorinfo = "借款人地区代码与非同业单位客户基础信息的地区代码应该一致";
//                    String sqlexist = "select distinct a.id id ,a.contractcode,a.receiptcode  from xcldkxx a inner join xftykhx b on a.brroweridnum=b.customercode and a.sjrq=b.sjrq" + whereStra + "AND a.brrowerareacode != b.regareacode AND b.operationname != '申请删除'";
//                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
//
//                    errorinfo = "借款人地区代码与非同业单位客户基础信息的地区代码应该一致(历史表)";
//                    sqlexist = "select distinct a.id id ,a.contractcode,a.receiptcode  from xcldkxx a inner join xftykhxh b on a.brroweridnum=b.customercode and a.sjrq=b.sjrq" + whereStra + "AND a.brrowerareacode != b.regareacode";
//                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
//
//                }


//                if (xcldkxx1List.contains("JS1781")){
//                    String errorinfo = "当借款人经济成分不为空时，借款人经济成分与非同业单位客户基础信息的客户经济成分应该一致";
//                    String sqlexist = "select distinct a.id id,a.contractcode,a.receiptcode   from xcldkxx a inner join  xftykhx b on a.brroweridnum=b.customercode and a.sjrq=b.sjrq" + whereStra + "AND a.inverstoreconomy != b.entpczjjcf and a.inverstoreconomy !='' AND b.entpczjjcf!=''AND b.operationname != '申请删除'";
//                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
//
//                    errorinfo = "当借款人经济成分不为空时，借款人经济成分与非同业单位客户基础信息的客户经济成分应该一致(历史表)";
//                    sqlexist = "select distinct a.id id,a.contractcode,a.receiptcode   from xcldkxx a inner join  xftykhxh b on a.brroweridnum=b.customercode and a.sjrq=b.sjrq" + whereStra + "AND a.inverstoreconomy != b.entpczjjcf and a.inverstoreconomy !='' AND b.entpczjjcf!=''";
//                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
//
//                }
//                if (xcldkxx1List.contains("JS1824")){
//                    String  errorinfo = "借款人国民经济部门与非同业单位客户基础信息的客户国民经济部门应该一致";
//                    String sqlexist = "select distinct  a.id id ,a.contractcode,a.receiptcode   from xcldkxx a inner join xftykhx b on a.brroweridnum=b.customercode and a.sjrq=b.sjrq" + whereStra + "AND a.isgreenloan != b.actamtcreny AND b.operationname != '申请删除'";
//                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
//
//                    errorinfo = "借款人国民经济部门与非同业单位客户基础信息的客户国民经济部门应该一致(历史表)";
//                    sqlexist = "select distinct  a.id id ,a.contractcode,a.receiptcode   from xcldkxx a inner join  xftykhxh b on a.brroweridnum=b.customercode and a.sjrq=b.sjrq" + whereStra + "AND a.isgreenloan != b.actamtcreny";
//                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
//                }

                if (xcldkxx1List.contains("JS1906")){
                    String  errorinfo = "借款人为个人客户的证件代码应该在个人客户基础信息.客户证件代码中存在";
                    String sqlexist="select a.id,a.contractcode,a.receiptcode from xclwtdkxx a " +whereStra+ " and a.isfarmerloan like 'B%' and (NOT EXISTS (SELECT 1 FROM xgrkhxx b WHERE a.sjrq=b.sjrq AND a.brroweridnum =b.customercode and a.isfarmerloan = b.regamtcreny  AND b.operationname != '申请删除') and NOT EXISTS (SELECT 1 FROM xgrkhxxh b WHERE a.sjrq=b.sjrq AND a.brroweridnum =b.customercode and a.isfarmerloan = b.regamtcreny))";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }


                if (xcldkxx1List.contains("JS1902")){
                    String  errorinfo = "委托人为个人客户的证件代码应该在个人客户基础信息.客户证件代码中存在";
                    String sqlexist="select a.id,a.contractcode,a.receiptcode from xclwtdkxx a " +whereStra+ " and a.bailorgfarmerloan like 'B%' and (NOT EXISTS (SELECT 1 FROM xgrkhxx b WHERE a.sjrq=b.sjrq AND a.bailorgidnum =b.customercode and a.bailorgfarmerloan = b.regamtcreny  AND b.operationname != '申请删除') and NOT EXISTS (SELECT 1 FROM xgrkhxxh b WHERE a.sjrq=b.sjrq AND a.bailorgidnum =b.customercode and a.bailorgfarmerloan = b.regamtcreny))";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                }



                if (xcldkxx1List.contains("JS1904")){
                    String  errorinfo = "借款人证件代码应该在非同业单位客户基础信息.客户证件代码中存在";
//                    String sqlexist="select  a.id,a.contractcode,a.receiptcode  from xcldkxx a" +whereStra+ "and (NOT EXISTS (SELECT 1 FROM xftykhx b WHERE a.sjrq=b.sjrq AND a.brroweridnum =b.customercode  AND b.operationname != '申请删除') and NOT EXISTS (SELECT 1 FROM xftykhxh b WHERE a.sjrq=b.sjrq AND a.brroweridnum =b.customercode)) and a.isgreenloan not like 'B%' and a.isgreenloan != 'E03'";
                    String sqlexist="select  a.id,a.contractcode,a.receiptcode  from xclwtdkxx a " +whereStra+ " and (NOT EXISTS (SELECT 1 FROM xftykhx b WHERE a.sjrq=b.sjrq AND a.brroweridnum =b.customercode  AND b.operationname != '申请删除') and NOT EXISTS (SELECT 1 FROM xftykhxh b WHERE a.sjrq=b.sjrq AND a.brroweridnum =b.customercode)) and a.isfarmerloan like 'A%' and a.isgreenloan not like 'B%' and a.isgreenloan not in ('D01','E03')";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                }


                if (xcldkxx1List.contains("JS1900")){
                    String  errorinfo = "委托人证件代码应该在非同业单位客户基础信息.客户证件代码中存在";
                    String sqlexist="select  a.id,a.contractcode,a.receiptcode  from xclwtdkxx a  " +whereStra+ " and a.checkstatus='0' and (NOT EXISTS (SELECT 1 FROM xftykhx b WHERE a.sjrq=b.sjrq AND a.bailorgidnum =b.customercode  AND b.operationname != '申请删除') and NOT EXISTS (SELECT 1 FROM xftykhxh b WHERE a.sjrq=b.sjrq AND a.bailorgidnum =b.customercode)) and a.bailorgfarmerloan like 'A%' and a.bailorgreenloan not like 'B%' and a.bailorgreenloan not in ('D01','E03')";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);

                }





                if (xcldkxx1List.contains("JS1895")){
                    String errorinfo = "存量委托贷款信息的贷款担保方式不是信用/免担保贷款的，委托贷款合同编码应该在担保合同信息.被担保合同编码中存在";
                    String  sqlexist="select a.id,a.contractcode,a.receiptcode  from xclwtdkxx a" + whereStra + "and a.guaranteemethod !='D' AND (a.contractcode not in(SELECT b.loancontractcode FROM xdkdbht b INNER JOIN xcldkxx c on c.sjrq = b.sjrq  where b.operationname != '申请删除')AND a.contractcode not in(SELECT e.loancontractcode FROM xdkdbhth e INNER JOIN xcldkxx f on f.sjrq = e.sjrq))";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }

                if (xcldkxx1List.contains("JS1896")){
                    String errorinfo = "存量委托贷款信息的贷款担保方式包含抵押、质押等情况的，委托贷款合同编码应该在担保物信息.被担保合同编码中存在";
                    String  sqlexist="SELECT a.id,a.contractcode,a.receiptcode  FROM xclwtdkxx a" +whereStra+ "and (a.guaranteemethod LIKE 'B%' OR a.guaranteemethod = 'A') and(a.contractcode not in (SELECT b.loancontractcode FROM xdkdbwx b INNER JOIN xcldkxx c on c.sjrq = b.sjrq  where  b.operationname != '申请删除') AND a.contractcode not in (SELECT e.loancontractcode FROM xdkdbwxh e INNER JOIN xcldkxx f on f.sjrq = e.sjrq))";
                    inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);
                }

//                errorinfo="金融机构代码与金融机构（分支机构）基础信息的金融机构代码应该一致";
//                sqlexist ="select a.id,a.contractcode,a.receiptcode from xcldkxx a  "+whereStra+" and a.operationname!='申请删除' and (a.financeorgcode <> (select b.finorgcode from xjrjgfz b INNER JOIN xdkdbwx c on b.sjrq = c.sjrq and b.inorgnum = c.financeorginnum where b.operationname != '申请删除') or a.financeorgcode <>(select e.finorgcode from xjrjgfzh e INNER JOIN xdkdbwx f on e.sjrq = f.sjrq and e.inorgnum = f.financeorginnum))";
//                inTableCalibration(setMap, errorId, whereMap, errorMsg, sqlexist, errorcode, errorinfo,errorcode2);




                resultSet = JDBCUtils.Query(connection, sql);

                while (true) {
                    if (!resultSet.next()) break;
                    Xclwtdkxx xclwtdkxx = new Xclwtdkxx();
                    Stringutil.getEntity(resultSet, xclwtdkxx);
                    CheckAllDataWt.checkXclwtdkxx(customSqlUtil,xcldkxx1List,xclwtdkxx, errorMsg, errorId, rightId);
                }
            }
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }finally {
            if (connection!=null){
                JDBCUtils.close();
            }
            if(CheckAllData.fzList != null) {
                CheckAllData.fzList.clear();
                CheckAllData.fzList = null;
            }
            if(CheckAllData.frList != null) {
                CheckAllData.frList.clear();
                CheckAllData.frList = null;
            }
        }

        //校验结束
        //修改状态 和 下载
        if (errorId!=null && errorId.size()>0){

          /*  //-下载到本地
            DownloadUtil.downLoad(errorMsg.toString(),target+File.separator+"checkout",fileName+".txt");
            errorMsg.delete(0,errorMsg.length());*/

            //-下载到本地
            StringBuffer errorMsgAll = new StringBuffer("");
            for(Map.Entry<String, String> entry : errorMsg.entrySet()) {
                errorMsgAll.append(entry.getValue()+"\r\n");
            }
            DownloadUtil.downLoad(errorMsgAll.toString(),target+File.separator+"checkout",fileName+".txt");
            errorMsg.clear();

            setMap.put("checkStatus","2");
            setMap.put("operationname"," ");
            customSqlUtil.updateByWhereBatch("xclwtdkxx",setMap,errorId);
            /*errorId.forEach(errId->{
                whereMap.put("id",errId);
                customSqlUtil.updateByWhere("xcldkxx",setMap,whereMap);
            });*/
            errorId.clear();
            json.put("msg","0");
        }else {
            if (b){
                json.put("msg","0");
            }else {
                json.put("msg","1");
            }

        }

        if (rightId!=null && rightId.size()>0){
            setMap.put("checkStatus","1");
            setMap.put("operationname"," ");
            customSqlUtil.updateByWhereBatch("xclwtdkxx",setMap,rightId);
           /* rightId.forEach(rigId->{
                whereMap.put("id",rigId);
                customSqlUtil.updateByWhere("xcldkxx",setMap,whereMap);
            });*/
            rightId.clear();
        }

        customSqlUtil.saveLog("存量委托贷款基础数据校验成功","校验");
        try {
            PrintWriter out = response.getWriter();
            out.write(json.toString());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("XDownLoadCheckwt")
    public void XDownLoadCheckwt(HttpServletResponse response) {
        if (StringUtils.isBlank(target)){
            target=suisService.findAll().get(0).getMessagepath();
        }
        DownloadUtil.downLoadCheckFile(response,target + File.separator + "checkout",fileName+".txt");
    }

    // 表间校验
    public void inTableCalibration(Map<String,String> setMap,List<String> errorId,Map<String,String> whereMap,LinkedHashMap<String, String> errorMsg,String sql,String errorcode,String errorinfo,String errorcode2) {
        List<Object[]> exist = customSqlUtil.executeQuery(sql);
        if (exist!=null && exist.size()>0){
            setMap.put("checkStatus","2");
            setMap.put("operationname"," ");
            exist.forEach(errId->{
                errorId.add(String.valueOf(errId[0]));
                whereMap.put("id", String.valueOf(errId[0]));
                if(!errorMsg.containsKey(String.valueOf(errId[0]))) {
                    errorMsg.put(String.valueOf(errId[0]), errorcode+":"+String.valueOf(errId[1])+"，"+errorcode2+":"+String.valueOf(errId[2])+"]->\r\n");
                }
                String str = errorMsg.get(String.valueOf(errId[0]));
                str = str + errorinfo+"|";
                errorMsg.put(String.valueOf(errId[0]), str);
            });
            whereMap.clear();
            setMap.clear();
            exist.clear();
        }
    }
    //lsit数据字段重复 返回true
    private Boolean knowledgeIsRepeat(List<Xftykhxbl> orderList) {
        Set<Xftykhxbl> set = new TreeSet<Xftykhxbl>(new Comparator<Xftykhxbl>() {
            public int compare(Xftykhxbl a, Xftykhxbl b) {
                // 字符串则按照asicc码升序排列
                if (StringUtils.isNotBlank(a.getCustomernum())&&StringUtils.isNotBlank(b.getCustomernum())){
                    return a.getCustomernum().compareTo(b.getCustomernum());
                }

                return 100000;
            }
        });
        set.addAll(orderList);
        if (set.size() < orderList.size()) {
            return true;
        }
        return false;
    }

}
