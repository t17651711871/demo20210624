package com.geping.etl.UNITLOAN.controller.createReport;

import com.geping.etl.UNITLOAN.common.report.ReportManagerService;
import com.geping.etl.UNITLOAN.service.statistics.ReportStatisticsService;
import com.geping.etl.UNITLOAN.util.ResponseResult;
import com.geping.etl.common.entity.Report_Info;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.common.service.Report_InfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.List;

/***
 *   报表生成管理
 * @author liang.xu
 * @date 2021.4.12
 */
@RestController
public class ReportManagerController {


    @Autowired
    private Report_InfoService riService;

    private final String subjectId = "25";

    @Autowired
    private ReportStatisticsService reportStatisticsService;

    @Autowired
    private ReportManagerService reportManagerService;

    //跳转到报文管理页面
    @RequestMapping(value = "/XCreateReportManager", method = RequestMethod.GET)
    public ModelAndView PCMDZYWUi(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        ModelAndView model = new ModelAndView();
        model.setViewName("unitloan/createreport/report_info_cr");
        return model;
    }


    //报文管理显示数据
    @PostMapping("/XCreateReportManagerList")
    public ResponseResult XCreateReportManager(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String rows = request.getParameter("rows");
        String pages = request.getParameter("page");
        int page = Integer.parseInt(pages) - 1;
        Sys_UserAndOrgDepartment sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
        String departId = "%%";
        String orgid = sys_user.getOrgid();
        List<Report_Info> reports = riService.getReportBySubjectId(subjectId);
        reportStatisticsService.yshReportStatistics(orgid, departId, reports);
        long totalCount = reports.size();
        return ResponseResult.success(totalCount, reports);
    }

    /**
     * 生成报文
     *
     * @param response
     * @param session
     * @param request
     */
    @PostMapping("createReport*")
    public void createReport(HttpServletResponse response, HttpSession session, HttpServletRequest request) {
        reportManagerService.createReport(response, session, request);
    }


    /**
     * 生成报文
     *
     * @param response
     * @param session
     * @param request
     */
    @PostMapping("createReportByCode*")
    public void createReportByCode(HttpServletResponse response, HttpSession session, HttpServletRequest request) {
        reportManagerService.createReportByCode(response, session, request);
    }
}
