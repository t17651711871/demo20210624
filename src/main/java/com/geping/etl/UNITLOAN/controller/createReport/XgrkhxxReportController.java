package com.geping.etl.UNITLOAN.controller.createReport;

import java.io.File;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.entity.report.Xgrkhxx;
import com.geping.etl.UNITLOAN.entity.report.XreportInfo;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.service.report.XgrkhxxService;
import com.geping.etl.UNITLOAN.util.CalcCountUtil;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.DownloadUtil;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.UNITLOAN.util.XDataBaseTypeUtil;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.utils.jdbc.JDBCUtils;

/**    
*  
* @author liuweixin  
* @date 2020年11月12日 下午2:49:08  
*/
@RestController
public class XgrkhxxReportController {
	
	@Autowired
    private SUpOrgInfoSetService suisService;
	
	@Autowired
    private XgrkhxxService xgrkhxxService;

    @Autowired
    private CustomSqlUtil customSqlUtil;
    @Autowired
    private CalcCountUtil countUtil;
    @Autowired
    private XDataBaseTypeUtil xDataBaseTypeUtil;
    @Autowired
    private DepartUtil departUtil;

    @Autowired
    private XcommonService commonService;

    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@PostMapping("XCreateReportXgrkhxx")
    public void XCreateReportXgrkhxx(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		
//		String sql="select * from xgrkhxx where datastatus='3'";
//        Connection connection = null;
//        ResultSet resultSet=null;
        try {
//            connection = JDBCUtils.getConnection();
//            resultSet = JDBCUtils.Query(connection, sql);
		
		//List<Object> list = Stringutil.getReportList(new Xgrkhxx(), "xgrkhxx");
        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        
        String date = request.getParameter("date");
                String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
                String target=messagePath+ File.separator+"report"+File.separator+localDate;
                String fileName=bankcodewangdian+"_GRKHXX_"+localDate+".dat";
                long a = System.currentTimeMillis();
                fileName = DownloadUtil.writeZIPgrkhxx(xDataBaseTypeUtil,target,fileName);
                long b = System.currentTimeMillis();
                System.out.println("生成报文用时："+(b-a)/1000+"秒");

                Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
                String departId = "%%";
                String orgid = sys_user.getOrgid();
                commonService.moveToHistoryTable("xgrkhxx",orgid,departId);
                XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
                commonService.save(reportInfo);
                Map<Integer,String> countMap=new HashMap<>();
                countMap.put(0,"reduce");
                countMap.put(3,"add");
                countUtil.handleCount("xgrkhxx",3,"reduce",0);
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(1));
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
        	JDBCUtils.close();
        }
    }
}
