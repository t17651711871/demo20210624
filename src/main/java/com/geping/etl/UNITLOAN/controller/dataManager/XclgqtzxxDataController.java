package com.geping.etl.UNITLOAN.controller.dataManager;

import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.bean.dto.selectDTO.SelectXclgqtzxxDTO;
import com.geping.etl.UNITLOAN.entity.report.Xclgqtzxx;
import com.geping.etl.UNITLOAN.enums.AuditStatusEnum;
import com.geping.etl.UNITLOAN.service.report.XclgqtzxxService;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.ResponseResult;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.controller.dataManager
 * @USER: tangshuai
 * @DATE: 2021/3/30
 * @TIME: 15:32
 * @描述:
 */
@RestController
public class XclgqtzxxDataController extends  BaseDataController{
    @Autowired
    private XclgqtzxxService xclgqtzxxService;
    @Autowired
    private DepartUtil departUtil;


    @GetMapping("/xclgqtzxxMainPage")
    public ModelAndView xclzqfxxxMainPage(HttpServletRequest request, String status) {
        ModelAndView modelAndView = new ModelAndView();
        request.setAttribute(SysConstants.dk,"9,10,11,12,13");
        beforeRender(request,modelAndView);
        if (AuditStatusEnum.DTJ.getText().equals(status)) {
            modelAndView.addObject("datastatus", AuditStatusEnum.DTJ.getStatus());
            modelAndView.setViewName("unitloan/datamanage/xgqcltzxx/xclgqtzxx_dtj");
        } else if (AuditStatusEnum.DSH.getText().equals(status)) {
            modelAndView.addObject("datastatus", AuditStatusEnum.DSH.getStatus());
            modelAndView.setViewName("unitloan/datamanage/xgqcltzxx/xclgqtzxx_dsh");
        } else {
            modelAndView.addObject("datastatus", AuditStatusEnum.YSH.getStatus());
            modelAndView.addObject("datamanege", status);
            modelAndView.setViewName("unitloan/createreport/xclgqtzxx_bw");
        }
        return modelAndView;
    }
    //查询数据
    @GetMapping("getXclgqtzxxData")
    public ResponseResult getXclgqtzxxData(HttpServletRequest request, SelectXclgqtzxxDTO selectDTO){
        {
            Sys_UserAndOrgDepartment sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
            String departId = departUtil.getDepart(sys_user);
            Specification specification = new Specification() {
                @Override
                public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                    List<Predicate> predicates = new ArrayList<>();
                    predicates.add(criteriaBuilder.equal(root.get("datastatus"), selectDTO.getDatastatus()));
                    predicates.add(criteriaBuilder.like(root.get("departid"), departId));
                    if (StringUtils.isNotBlank(selectDTO.getFinanceorgcodeParam())){
                        predicates.add(criteriaBuilder.like(root.get("financeorgcode"), "%"+selectDTO.getFinanceorgcodeParam()+"%"));
                    }
                    if (StringUtils.isNotBlank(selectDTO.getCheckstatusParam())){
                        predicates.add(criteriaBuilder.equal(root.get("checkstatus"), selectDTO.getCheckstatusParam()));
                    }
                    if (StringUtils.isNotBlank(selectDTO.getOperationtimeParam())){
                        predicates.add(criteriaBuilder.equal(root.get("sjrq"), selectDTO.getOperationtimeParam()));
                    }
//                    if (StringUtils.isNotBlank(contractcodeParam)){
//                        predicates.add(criteriaBuilder.like(root.get("contractcode"), "%"+contractcodeParam+"%"));
//                    }
//                    if (StringUtils.isNotBlank(receiptcodeParam)){
//                        predicates.add(criteriaBuilder.like(root.get("receiptcode"), "%"+receiptcodeParam+"%"));
//                    }
//                    if (StringUtils.isNotBlank(brroweridnumParam)){
//                        predicates.add(criteriaBuilder.like(root.get("brroweridnum"), "%"+brroweridnumParam+"%"));
//                    }
//                    if (StringUtils.isNotBlank(isfarmerloanParam)){
//                        predicates.add(criteriaBuilder.like(root.get("isfarmerloan"), "%"+isfarmerloanParam+"%"));
//                    }
                    if (StringUtils.isNotBlank(selectDTO.getOperationnameParam())){
                        predicates.add(criteriaBuilder.equal(root.get("operationname"), selectDTO.getOperationnameParam()));
                    }
                    predicates.add(criteriaBuilder.equal(root.get("orgid"), sys_user.getOrgid()));
                    return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
                }
            };
            Sort sort = new Sort(Sort.Direction.DESC, "operationtime");
            PageRequest pageRequest = new PageRequest(selectDTO.getPage() - 1, selectDTO.getRows(), sort);
            Page<Xclgqtzxx> all = xclgqtzxxService.findAll(specification, pageRequest);
            List<Xclgqtzxx> content = all.getContent();
            long totalCount = all.getTotalElements();
            return ResponseResult.success(totalCount, content);
        }
    }

}
