package com.geping.etl.UNITLOAN.controller.createReport;

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.entity.report.Xcltyckxx;
import com.geping.etl.UNITLOAN.entity.report.Xcltyjdxx;
import com.geping.etl.UNITLOAN.entity.report.XreportInfo;
import com.geping.etl.UNITLOAN.entity.report.Xtyckfsexx;
import com.geping.etl.UNITLOAN.entity.report.Xtyjdfsexx;
import com.geping.etl.UNITLOAN.entity.report.Xtykhjcxx;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.util.CalcCountUtil;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.DownloadUtil;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;

/**    
*  
* @author liuweixin  
* @date 2021年2月22日 下午4:22:00  
*/
@RestController
public class XCreateReporttyModule {

	@Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private CustomSqlUtil customSqlUtil;
    @Autowired
    private CalcCountUtil countUtil;
    @Autowired
    private DepartUtil departUtil;

    @Autowired
    private XcommonService commonService;
    
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    @PostMapping("XCreateReportXcltyjdxx")
    public void XCreateReportcltyjdxx(HttpServletRequest request, HttpServletResponse response,HttpSession session){
        List<Object> list = Stringutil.getReportList(new Xcltyjdxx(), "xcltyjdxx");
        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        
        String date = request.getParameter("date");
        try {
                String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
                String target=messagePath+ File.separator+"report"+File.separator+localDate;
                String fileName=bankcodewangdian+"_CLTYJD_"+localDate+".dat";
                fileName = DownloadUtil.writeZIP(list,target,fileName);

                Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
                String departId = "%%";
                String orgid = sys_user.getOrgid();
                commonService.moveToHistoryTable("xcltyjdxx",orgid,departId);
                XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
                commonService.save(reportInfo);
                Map<Integer,String> countMap=new HashMap<>();
                countMap.put(0,"reduce");
                countMap.put(3,"add");
                countUtil.handleCount("xcltyjdxx",3,"reduce",list.size());
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @PostMapping("XCreateReportXtyjdfsexx")
    public void XCreateReporttyjdfsexx(HttpServletRequest request, HttpServletResponse response,HttpSession session){
        List<Object> list = Stringutil.getReportList(new Xtyjdfsexx(), "xtyjdfsexx");
        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        
        String date = request.getParameter("date");
        try {
                String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
                String target=messagePath+ File.separator+"report"+File.separator+localDate;
                String fileName=bankcodewangdian+"_TYJDFS_"+localDate+".dat";
                fileName = DownloadUtil.writeZIP(list,target,fileName);

                Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
                String departId = "%%";
                String orgid = sys_user.getOrgid();
                commonService.moveToHistoryTable("xtyjdfsexx",orgid,departId);
                XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
                commonService.save(reportInfo);
                Map<Integer,String> countMap=new HashMap<>();
                countMap.put(0,"reduce");
                countMap.put(3,"add");
                countUtil.handleCount("xtyjdfsexx",3,"reduce",list.size());
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @PostMapping("XCreateReportXtykhjcxx")
    public void XCreateReporttykhjcxx(HttpServletRequest request, HttpServletResponse response,HttpSession session){
        List<Object> list = Stringutil.getReportList(new Xtykhjcxx(), "xtykhjcxx");
        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        
        String date = request.getParameter("date");
        try {
                String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
                String target=messagePath+ File.separator+"report"+File.separator+localDate;
                String fileName=bankcodewangdian+"_TYKHXX_"+localDate+".dat";
                fileName = DownloadUtil.writeZIP(list,target,fileName);

                Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
                String departId = "%%";
                String orgid = sys_user.getOrgid();
                commonService.moveToHistoryTable("xtykhjcxx",orgid,departId);
                XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
                commonService.save(reportInfo);
                Map<Integer,String> countMap=new HashMap<>();
                countMap.put(0,"reduce");
                countMap.put(3,"add");
                countUtil.handleCount("xtykhjcxx",3,"reduce",list.size());
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @PostMapping("XCreateReportXcltyckxx")
    public void XCreateReportcltyckxx(HttpServletRequest request, HttpServletResponse response,HttpSession session){
        List<Object> list = Stringutil.getReportList(new Xcltyckxx(), "xcltyckxx");
        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        
        String date = request.getParameter("date");
        try {
                String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
                String target=messagePath+ File.separator+"report"+File.separator+localDate;
                String fileName=bankcodewangdian+"_CLTYCK_"+localDate+".dat";
                fileName = DownloadUtil.writeZIP(list,target,fileName);

                Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
                boolean isDepart="yes".equals(sUpOrgInfoSet.getIsusedepart())?true:false;
                String departId = "%%";
                String orgid = sys_user.getOrgid();
                commonService.moveToHistoryTable("xcltyckxx",orgid,departId);
                XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
                commonService.save(reportInfo);
                Map<Integer,String> countMap=new HashMap<>();
                countMap.put(0,"reduce");
                countMap.put(3,"add");
                countUtil.handleCount("xcltyckxx",3,"reduce",list.size());
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @PostMapping("XCreateReportXtyckfsexx")
    public void XCreateReporttyckfsexx(HttpServletRequest request, HttpServletResponse response,HttpSession session){
        List<Object> list = Stringutil.getReportList(new Xtyckfsexx(), "xtyckfsexx");
        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath=sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        
        String date = request.getParameter("date");
        try {
                String localDate=new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
                String target=messagePath+ File.separator+"report"+File.separator+localDate;
                String fileName=bankcodewangdian+"_TYCKFS_"+localDate+".dat";
                fileName = DownloadUtil.writeZIP(list,target,fileName);

                Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment)session.getAttribute("sys_User"));
                String departId = "%%";
                String orgid = sys_user.getOrgid();
                commonService.moveToHistoryTable("xtyckfsexx",orgid,departId);
                XreportInfo reportInfo = new XreportInfo(Stringutil.getUUid(),fileName,target+ File.separator + fileName,format.format(new Date()),sys_user.getLoginid(),orgid,sys_user.getDepartid());
                commonService.save(reportInfo);
                Map<Integer,String> countMap=new HashMap<>();
                countMap.put(0,"reduce");
                countMap.put(3,"add");
                countUtil.handleCount("xtyckfsexx",3,"reduce",list.size());
            PrintWriter out=response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
