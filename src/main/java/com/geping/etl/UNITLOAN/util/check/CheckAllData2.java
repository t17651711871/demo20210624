package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.entity.baseInfo.*;
import com.geping.etl.UNITLOAN.entity.report.Xdkfsxx;
import com.geping.etl.UNITLOAN.entity.report.Xgrdkfsxx;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.TuoMinUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.geping.etl.UNITLOAN.util.check.CheckUtil.checkStr2;

/**
 * 单位贷款发生额信息校验
 */
public class CheckAllData2 {
	
    //币种代码
    private static List<String> currencyList;
    //行政区划代码
    private static List<String> areaList;
    //国家代码
    private static List<String> countryList;
    //大行业代码
    private static List<String> aindustryList;

    //C0017-行业中类代码
    private static List<String> bindustryList;

    //金融机构（分支机构）基础信息表.金融机构代码
    public static List<String> fzList;

    //金融机构（法人）基础信息表.金融机构代码
    public static List<String> frList;
	
	//单位贷款发生额信息
    public static void checkXdkfsxx(CustomSqlUtil customSqlUtil, List<String> checknumList,Xdkfsxx xdkfsxx, LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId) {
        boolean isError = false;
        StringBuffer tempMsg = new StringBuffer("");

        String nullError = "不能为空";
        String lengthError = "超过限制长度";
        String spaceStr = "|";
        //#贷款币种
        if (currencyList==null) currencyList = customSqlUtil.getBaseCode(BaseCurrency.class);

        if (StringUtils.isNotBlank(xdkfsxx.getLoancurrency())){
            if (checknumList.contains("JS0113"))
            {
                isError = CheckUtil.checkCurrency(xdkfsxx.getLoancurrency(), currencyList, customSqlUtil, tempMsg, isError, "贷款币种");
            }

        }else {
            if (checknumList.contains("JS1044")){ tempMsg.append("贷款币种不能为空" + spaceStr);isError = true; }
        }


        //#贷款财政扶持方式
        if (checknumList.contains("JS0116")){
            isError = CheckUtil.checkLoanczfcfs(xdkfsxx.getLoanfinancesupport(), tempMsg, isError, "贷款财政扶持方式");
        }

        //#贷款产品类别
        if (StringUtils.isNotBlank(xdkfsxx.getLoanprocode())) {
        	 if(!ArrayUtils.contains(CheckUtil.dkcplb, xdkfsxx.getLoanprocode())) {
                 if (checknumList.contains("JS0111"))
                 {
                     tempMsg.append("贷款产品类别不在符合要求的值域范围内" + spaceStr);
                     isError = true;
                 }
        	 }
        }else {
            if (checknumList.contains("JS1039"))
            {
                tempMsg.append("贷款产品类别" + nullError + spaceStr);
                isError = true;
            }
        }
        //#贷款担保方式
        if (StringUtils.isNotBlank(xdkfsxx.getLoanprocode())&&ArrayUtils.contains(CheckUtil.dkcplb, xdkfsxx.getLoanprocode())) {
        	String temp = xdkfsxx.getLoanprocode().substring(0, 2);
            if(!"F01".equals(temp)&&!"F03".equals(temp)&&!"F06".equals(temp)&&!"F07".equals(temp)) {
             	if (StringUtils.isNotBlank(xdkfsxx.getGteemethod())){
                    if(!ArrayUtils.contains(CheckUtil.dkdbfs, xdkfsxx.getGteemethod())) {
                        if (checknumList.contains("JS0117"))
                        {
                            tempMsg.append("贷款担保方式不在符合要求的值域范围内" + spaceStr);
                            isError = true;
                        }
                    }
                }else {
                    if (checknumList.contains("JS1021"))
                    {
                        tempMsg.append("当贷款产品类别为资产类贷款时，贷款担保方式"+nullError+spaceStr);
                        isError=true;
                    }
                }
            }
        }
        //#贷款到期日期
        if (StringUtils.isNotBlank(xdkfsxx.getLoanenddate())) {
            if (xdkfsxx.getLoanenddate().length() == 10) {
                boolean b = CheckUtil.checkDate(xdkfsxx.getLoanenddate(), "yyyy-MM-dd");
                if (b) {
                    if (xdkfsxx.getLoanenddate().compareTo("1800-01-01") < 0 || xdkfsxx.getLoanenddate().compareTo("2100-12-31") > 0) {
                        if (checknumList.contains("JS0336"))
                        {
                            tempMsg.append("贷款到期日期需晚于1800-01-01早于2100-12-31" + spaceStr);
                            isError = true;
                        }
                    }
                } else {
                    if (checknumList.contains("JS0336")) {tempMsg.append("贷款到期日期不符合yyyy-MM-dd格式" + spaceStr);isError = true;}
                }
            } else {
                if (checknumList.contains("JS0336")) {  tempMsg.append("贷款到期日期长度不等于10" + spaceStr);isError = true;}
            }
        } else {
            if (checknumList.contains("JS1042")) { tempMsg.append("贷款到期日期" + nullError + spaceStr);isError = true;}
        }

        //#贷款定价基准类型
        if (StringUtils.isNotBlank(xdkfsxx.getLoanfixamttype())) {
	        if (!ArrayUtils.contains(CheckUtil.loanjzlx, xdkfsxx.getLoanfixamttype())) {
                if (checknumList.contains("JS0115"))
                {
                    tempMsg.append("贷款定价基准类型不在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
	        }
        } else {
            if (checknumList.contains("JS1049"))
            {
                tempMsg.append("贷款定价基准类型" + nullError + spaceStr);
                isError = true;
            }
        }

        //#贷款发放日期
        if (StringUtils.isNotBlank(xdkfsxx.getLoanstartdate())) {
            if (xdkfsxx.getLoanstartdate().length() == 10) {
                boolean b1 = CheckUtil.checkDate(xdkfsxx.getLoanstartdate(), "yyyy-MM-dd");
                if (b1) {
                    if (xdkfsxx.getLoanstartdate().compareTo("1800-01-01") >= 0 && xdkfsxx.getLoanstartdate().compareTo("2100-12-31") <= 0) {
                        if (StringUtils.isNotBlank(xdkfsxx.getLoanenddate()) &&
                                xdkfsxx.getLoanstartdate().compareTo(xdkfsxx.getLoanenddate()) > 0) {
                            if (checknumList.contains("JS2436"))
                            {
                                tempMsg.append("贷款发放日期应小于等于贷款到期日期" + spaceStr);
                                isError = true;
                            }
                        }
                        if(StringUtils.isNotBlank(xdkfsxx.getSjrq()) &&
                        		xdkfsxx.getLoanstartdate().compareTo(xdkfsxx.getSjrq()) > 0) {
                            if (checknumList.contains("JS2435"))
                            {
                                tempMsg.append("贷款发放日期应小于等于数据日期" + spaceStr);
                                isError = true;
                            }
        	            }
                        if("1".equals(xdkfsxx.getGivetakeid())) {
                        	if(StringUtils.isNotBlank(xdkfsxx.getSjrq()) && CheckUtil.checkDate(xdkfsxx.getSjrq(), "yyyy-MM-dd")) {
                        		if(!xdkfsxx.getLoanstartdate().substring(0,7).equals(xdkfsxx.getSjrq().substring(0,7))&&!xdkfsxx.getLoanstatus().equals("LF04")&&!xdkfsxx.getLoanprocode().startsWith("F04")) {
                                    if (checknumList.contains("JS2562"))
                                    {
                                        tempMsg.append("当月发放的贷款且不是转入贷款的，贷款发放日期应该在当月范围内（透支贷款除外）" + spaceStr);
                                        isError = true;
                                    }
                            	}
                        	}  	
                        }
                    } else {
                        if (checknumList.contains("JS0335")) { tempMsg.append("贷款发放日期需晚于1800-01-01早于2100-12-31" + spaceStr);isError = true; }
                    }
                } else {
                    if (checknumList.contains("JS0335")) {tempMsg.append("贷款发放日期不符合yyyy-MM-dd格式" + spaceStr);isError = true;}
                }
            } else {
                if (checknumList.contains("JS0335")) { tempMsg.append("贷款发放日期长度不等于10" + spaceStr);isError = true;}
            }
        } else {
            if (checknumList.contains("JS1041")) {  tempMsg.append("贷款发放日期" + nullError + spaceStr);isError = true;}
        }

        //#贷款发生金额
        if (StringUtils.isNotBlank(xdkfsxx.getLoanamt())) {
            if (xdkfsxx.getLoanamt().length() > 20 || !CheckUtil.checkDecimal(xdkfsxx.getLoanamt(), 2)) {
                if (checknumList.contains("JS0330"))
                {
                    tempMsg.append("贷款发生金额总长度不能超过20位,小数位应保留2位" + spaceStr);
                    isError = true;
                }
            }
            if(new BigDecimal(xdkfsxx.getLoanamt()).compareTo(BigDecimal.ZERO)<=0) {
                if (checknumList.contains("JS2438"))
                {
                    tempMsg.append("贷款发生金额应大于0" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1045"))
            {
                tempMsg.append("贷款发生金额" + nullError + spaceStr);
                isError = true;
            }
        }
        //#贷款发生金额折人民币
        if (StringUtils.isNotBlank(xdkfsxx.getLoancnyamt())) {
            if (xdkfsxx.getLoancnyamt().length() > 20 || !CheckUtil.checkDecimal(xdkfsxx.getLoancnyamt(), 2)) {
                if (checknumList.contains("JS0331"))
                {
                    tempMsg.append("贷款发生金额折人民币总长度不能超过20位,小数位应保留2位" + spaceStr);
                    isError = true;
                }
            }
            if(new BigDecimal(xdkfsxx.getLoancnyamt()).compareTo(BigDecimal.ZERO)<=0) {
                if (checknumList.contains("JS2439"))
                {
                    tempMsg.append("贷款发生金额折人民币应大于0" + spaceStr);
                    isError = true;
                }
            }

            if (Double.valueOf(xdkfsxx.getLoancnyamt())>10000000000L){
                if (checknumList.contains("JS2883"))
                {
                    tempMsg.append("单位贷款的发生金额折人民币应不超过100亿" + spaceStr);
                    isError = true;
                }
            }



            if("CNY".equals(xdkfsxx.getLoancurrency())){
            	if (StringUtils.isNotBlank(xdkfsxx.getLoanamt())) {
            		if(!xdkfsxx.getLoanamt().equals(xdkfsxx.getLoancnyamt())) {
                        if (checknumList.contains("JS2691"))
                        {
                            tempMsg.append("币种为人民币的，贷款发生金额折人民币应该与贷款发生金额的值相等" + spaceStr);
                            isError = true;
                        }
            		}
            	}else {
                    if (checknumList.contains("JS2691"))
                    {
                        tempMsg.append("币种为人民币的，贷款发生金额折人民币应该与贷款发生金额的值相等" + spaceStr);
                        isError = true;
                    }
            	}
            }
        } else {
            if (checknumList.contains("JS1046"))
            {
                tempMsg.append("贷款发生金额折人民币" + nullError + spaceStr);
                isError = true;
            }
        }
        //#贷款合同编码
//        isError = CheckUtil.grantNullAndLengthAndStr2(xdkfsxx.getLoancontractcode(), 100, tempMsg, isError, "贷款合同编码");
        if (StringUtils.isNotBlank(xdkfsxx.getLoancontractcode())){
            if (xdkfsxx.getLoancontractcode().length()<=100){
                    if (checkStr2(xdkfsxx.getLoancontractcode())){
                        if (checknumList.contains("JS0318"))
                        {
                            tempMsg.append("贷款合同编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                            isError=true;
                        }
                    }
                }else {
                if (checknumList.contains("JS0327")) {  tempMsg.append("贷款合同编码字符长度不能超过100"+spaceStr);isError=true;}

                }
        }else {
            if (checknumList.contains("JS1038")) {   tempMsg.append("贷款合同编码不能为空" + nullError + spaceStr);isError = true;}
        }

        //#贷款借据编码
//        isError = CheckUtil.grantNullAndLengthAndStr2(xdkfsxx.getLoanbrowcode(), 100, tempMsg, isError, "贷款借据编码");

        if (StringUtils.isNotBlank(xdkfsxx.getLoanbrowcode())){
            if (xdkfsxx.getLoanbrowcode().length()<=100){
                if (checkStr2(xdkfsxx.getLoanbrowcode())){
                    if (checknumList.contains("JS0317"))
                    {
                        tempMsg.append("贷款借据编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS0326")) {  tempMsg.append("贷款借据编码字符长度不能超过100"+spaceStr);isError=true;}

            }
        }else {
            if (checknumList.contains("JS1037")) {   tempMsg.append("贷款借据编码不能为空" + nullError + spaceStr);isError = true;}
        }


        //#贷款利率重新定价日
        if (StringUtils.isNotBlank(xdkfsxx.getLoanraterepricedate())) {
	        boolean b = CheckUtil.checkDate(xdkfsxx.getLoanraterepricedate(), "yyyy-MM-dd");
	        if (b) {
	            if (xdkfsxx.getLoanraterepricedate().compareTo("1800-01-01") < 0 || xdkfsxx.getLoanraterepricedate().compareTo("2100-12-31") > 0) {
                    if (checknumList.contains("JS0338"))
                    {
                        tempMsg.append("贷款利率重新定价日需晚于1800-01-01早于2100-12-31" + spaceStr);
                        isError = true;
                    }
	            }
	            if(StringUtils.isNotBlank(xdkfsxx.getLoanstartdate())) {
	            	if(xdkfsxx.getLoanraterepricedate().compareTo(xdkfsxx.getLoanstartdate()) < 0) {
                        if (checknumList.contains("JS2367"))
                        {
                            tempMsg.append("贷款利率重新定价日应大于等于贷款发放日期" + spaceStr);
                            isError = true;
                        }
		            }
	            }
	        } else {
                if (checknumList.contains("JS0338")) { tempMsg.append("贷款利率重新定价日不符合yyyy-MM-dd格式" + spaceStr);isError = true; }

	        }
        } else {
            if (checknumList.contains("JS1051")) {  tempMsg.append("贷款利率重新定价日" + nullError + spaceStr);isError = true;  }

        }
        //#贷款实际投向
        if (StringUtils.isNotBlank(xdkfsxx.getLoanprocode())&&ArrayUtils.contains(CheckUtil.dkcplb, xdkfsxx.getLoanprocode())) {
        	String temp = xdkfsxx.getLoanprocode().substring(0, 2);
            if(!"F01".equals(temp)&&!"F03".equals(temp)&&!"F06".equals(temp)&&!"F07".equals(temp)) {
            	if (StringUtils.isNotBlank(xdkfsxx.getLoanactdect())){
            		if (bindustryList==null) bindustryList = customSqlUtil.getBaseCode(BaseBindustry.class);
                    if (!bindustryList.contains(xdkfsxx.getLoanactdect())||"1000".equals(xdkfsxx.getLoanactdect())){
                        if (checknumList.contains("JS0112"))
                        {
                            tempMsg.append("贷款实际投向不在符合要求的值域范围内" + spaceStr);
                            isError = true;
                        }
                    }
            	}else {
                    if (checknumList.contains("JS1647")) { tempMsg.append("当贷款产品类别为资产类贷款时，贷款实际投向不能为空"+nullError+spaceStr);isError=true;}
            	}
            }
        }
        //#贷款实际终止日期
        if ("0".equals(xdkfsxx.getGivetakeid())&&((StringUtils.isNotBlank(xdkfsxx.getLoanstatus()) && ArrayUtils.contains(CheckUtil.loanstatus4, xdkfsxx.getLoanstatus()))||("LF05".equals(xdkfsxx.getLoanstatus())&&("01".equals(xdkfsxx.getLoanrestructuring())||"02".equals(xdkfsxx.getLoanrestructuring()))))) {
            if (StringUtils.isNotBlank(xdkfsxx.getLoanactenddate())) {
                if (xdkfsxx.getLoanactenddate().length() == 10) {
                    boolean b1 = CheckUtil.checkDate(xdkfsxx.getLoanactenddate(), "yyyy-MM-dd");
                    if (b1) {
                        if (xdkfsxx.getLoanactenddate().compareTo("1800-01-01") >= 0 && xdkfsxx.getLoanactenddate().compareTo("2100-12-31") <= 0) {
                            if (StringUtils.isNotBlank(xdkfsxx.getLoanstartdate()) &&
                                    xdkfsxx.getLoanstartdate().compareTo(xdkfsxx.getLoanenddate()) > 0) {
                                if (checknumList.contains("JS2437"))
                                {
                                    tempMsg.append("当贷款实际终止日期不为空时，贷款实际终止日期应大于等于贷款发放日期" + spaceStr);
                                    isError = true;
                                }
                            }
                        } else {
                            if (checknumList.contains("JS0337")) { tempMsg.append("贷款实际终止日期需晚于1800-01-01早于2100-12-31" + spaceStr);isError = true;}
                        }
                    } else {
                        if (checknumList.contains("JS0337")) {tempMsg.append("贷款实际终止日期不符合yyyy-MM-dd格式" + spaceStr);isError = true;}
                    }
                } else {
                    if (checknumList.contains("JS0337")) {  tempMsg.append("贷款实际终止日期长度不等于10" + spaceStr);isError = true;}
                }
            } else {
                if (checknumList.contains("JS1012")) { tempMsg.append("当贷款状态为核销、剥离、转让、重组、债转股、以物抵债且发放/收回标识为收回时，贷款实际终止日期" + nullError + spaceStr);isError = true;}
            }
        }


        if (StringUtils.isNotBlank(xdkfsxx.getLoanactenddate())){
//            Calendar cal = Calendar.getInstance();
//            int year = cal.get(Calendar.YEAR);
//            int month = cal.get(Calendar.MONTH) + 1;
//            String monthStr;
//            if(month<10) {
//                monthStr = year+"-0"+month;
//            }else {
//                monthStr = year+"-"+month;
//            }
            if (StringUtils.isNotBlank(xdkfsxx.getSjrq()) &&
                    (xdkfsxx.getLoanactenddate().compareTo(xdkfsxx.getSjrq()) > 0) ||!xdkfsxx.getLoanactenddate().substring(0,7).equals(xdkfsxx.getSjrq().substring(0,7))) {
                if (checknumList.contains("JS2369"))
                {
                    tempMsg.append("当贷款实际终止日期不为空时，贷款实际终止日期应小于等于数据日期且应该在报送期内" + spaceStr);
                    isError = true;
                }
            }
        }

        //#贷款用途
        if (StringUtils.isNotBlank(xdkfsxx.getLoanprocode())&&ArrayUtils.contains(CheckUtil.dkcplb, xdkfsxx.getLoanprocode())) {
        	String temp = xdkfsxx.getLoanprocode().substring(0, 2);
            if(!"F01".equals(temp)&&!"F03".equals(temp)&&!"F06".equals(temp)&&!"F07".equals(temp)) {
            	if (StringUtils.isNotBlank(xdkfsxx.getIssupportliveloan())){
                    if (xdkfsxx.getIssupportliveloan().length()<=3000){
                        if (CheckUtil.checkStr5(xdkfsxx.getIssupportliveloan())){
                            if (checknumList.contains("JS0322")) {
                                tempMsg.append("贷款用途不能包含空格和特殊字符"+spaceStr);
                                isError=true;
                            }
                        }
                    }else {
                        if (checknumList.contains("JS0329")) {   tempMsg.append("贷款用途"+lengthError+3000+spaceStr);isError=true;   }
                    }
                }else {
                    if (checknumList.contains("JS1521")) { 	tempMsg.append("当贷款产品类别为资产类贷款时，贷款用途"+nullError+spaceStr);isError=true;   }
                }
            }
        }
        
        //#贷款重组方式
        if("LF05".equals(xdkfsxx.getLoanstatus())) {
        	if (StringUtils.isNotBlank(xdkfsxx.getLoanrestructuring())){
                if (!ArrayUtils.contains(CheckUtil.dkczfs, xdkfsxx.getLoanrestructuring())){
                    if (checknumList.contains("JS0120"))
                    {
                        tempMsg.append("贷款重组方式不在符合要求的值域范围内"+spaceStr);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS1055"))
                {
                    tempMsg.append("当贷款状态为重组时，贷款重组方式"+nullError+spaceStr);
                    isError=true;
                }
            }
        }else {
        	if (StringUtils.isNotBlank(xdkfsxx.getLoanrestructuring())){
                if (checknumList.contains("JS2651"))
                {
                    tempMsg.append("贷款状态不为重组时，贷款重组方式必须为空"+spaceStr);
                    isError=true;
                }
            }
        }
        
        //#贷款状态
        if (StringUtils.isNotBlank(xdkfsxx.getLoanstatus())) {
        	if (!ArrayUtils.contains(CheckUtil.loanstatus5, xdkfsxx.getLoanstatus())) {
                if (checknumList.contains("JS0119"))
                {
                    tempMsg.append("贷款状态不符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1054")) {  tempMsg.append("贷款状态" + nullError + spaceStr);isError = true; }
        }
        
        //#发放/收回标识
        if (StringUtils.isNotBlank(xdkfsxx.getGivetakeid())) {
        	if (!ArrayUtils.contains(CheckUtil.sfcommon, xdkfsxx.getGivetakeid())) {
                if (checknumList.contains("JS0121"))
                {
                    tempMsg.append("发放/收回标识不符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
        	if(StringUtils.isNotBlank(xdkfsxx.getLoanactenddate())) {
        		if(!"0".equals(xdkfsxx.getGivetakeid())) {
        		    if (checknumList.contains("JS2104"))
                    {
                        tempMsg.append("贷款实际终止日期不为空的则发放/收回标识应该为0-收回" + spaceStr);
                        isError = true;
                    }
        		}
        	}
        } else {
            if (checknumList.contains("JS2104 ")) {   tempMsg.append("发放/收回标识" + nullError + spaceStr);isError = true;  }
        }
        
        //#基准利率
        if ("RF02".equals(xdkfsxx.getRateisfix())) {
            if (StringUtils.isNotBlank(xdkfsxx.getRate())) {
                if(xdkfsxx.getRate().length()>10||!CheckUtil.checkDecimal(xdkfsxx.getRate(), 5)) {
                    if (checknumList.contains("JS0333"))
                    {
                        tempMsg.append("当基准利率不为空时，基准利率总长度不能超过10位，小数位必须为5位" + spaceStr);
                        isError = true;
                    }
                }else {
                	 if (CheckUtil.checkPerSign(xdkfsxx.getRate())) {
                         if (checknumList.contains("JS0320"))
                         {
                             tempMsg.append("利率水平不能包含‰或%" + spaceStr);
                             isError = true;
                         }
                     }
                	 if(new BigDecimal(xdkfsxx.getRate()).compareTo(BigDecimal.ZERO) < 0||new BigDecimal(xdkfsxx.getRate()).compareTo(new BigDecimal("30")) >0) {
                         if (checknumList.contains("JS2441"))
                         {
                             tempMsg.append("当为浮动利率贷款时，基准利率应大于等于0且小于等于30" + spaceStr);
                             isError = true;
                         }
                     }
				}
            } else {
                if (checknumList.contains("JS1050")) { tempMsg.append("当为浮动利率贷款时，基准利率不能为空" + spaceStr);isError = true; }
            }
        }else if("RF01".equals(xdkfsxx.getRateisfix())){
        	if (StringUtils.isNotBlank(xdkfsxx.getRate())) {
                if (checknumList.contains("JS2158"))
                {
                    tempMsg.append("利率类型为固定利率的，基准利率必须为空" + spaceStr);
                    isError = true;
                }
            }
		}
        
        //#交易流水号
//        isError = CheckUtil.grantNullAndLengthAndStr2(xdkfsxx.getTransactionnum(), 60, tempMsg, isError, "交易流水号");
        if (StringUtils.isNotBlank(xdkfsxx.getTransactionnum())){
            if (xdkfsxx.getTransactionnum().length()<=60){
                if (checkStr2(xdkfsxx.getTransactionnum())){
                    if (checknumList.contains("JS0898"))
                    {
                        tempMsg.append("交易流水号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                        isError = true;
                    }
                }
            }else {
                if (checknumList.contains("JS0899"))
                {
                    tempMsg.append("交易流水号字段长度不能超过60" + spaceStr);
                    isError = true;
                }
            }

        }else {
            if (checknumList.contains("JS1648"))
            {
                tempMsg.append("交易流水号不能为空" + spaceStr);
                isError = true;
            }
        }



        //#借款人地区代码
        if (StringUtils.isNotBlank(xdkfsxx.getBrowareacode())) {
            if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
            if (countryList == null) countryList = customSqlUtil.getBaseCode(BaseCountry.class);
            if (!areaList.contains(xdkfsxx.getBrowareacode())&& !countryList.contains(xdkfsxx.getBrowareacode())) {
                if (checknumList.contains("JS0107"))
                {
                    tempMsg.append("借款人地区代码不在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
            if (StringUtils.isNotBlank(xdkfsxx.getIsgreenloan())&&xdkfsxx.getIsgreenloan().startsWith("E")) {
                if (!xdkfsxx.getBrowareacode().startsWith("000")) {
                    if (checknumList.contains("JS1940"))
                    {
                        tempMsg.append("借款人国民经济部门为E开头的非居民部门，则借款人地区代码应为000+对应的3位数字国别代码" + spaceStr);
                        isError = true;
                    }
                } 
            }else {
                if (xdkfsxx.getBrowareacode().startsWith("000")) {
                    if (checknumList.contains("JS1941"))
                    {
                        tempMsg.append("借款人国民经济部门不是E开头的非居民部门，则借款人地区代码不能为000开头" + spaceStr);
                        isError = true;
                    }
                }
            }
            if ("200".equals(xdkfsxx.getBrowinds())) {
                if (!xdkfsxx.getBrowareacode().startsWith("000")) {
                    if (checknumList.contains("JS2138"))
                    {
                        tempMsg.append("借款人行业为境外的，借款人地区代码应该以000开头" + spaceStr);
                        isError = true;
                    }
                }
            } else {
                if (xdkfsxx.getBrowareacode().startsWith("000")) {
                    if (checknumList.contains("JS2139"))
                    {
                        tempMsg.append("借款人行业为境内的，借款人地区代码不能以000开头" + spaceStr);
                        isError = true;
                    }
                }
            }
        } else {
            if (checknumList.contains("JS1034")) {  tempMsg.append("借款人地区代码" + nullError + spaceStr);isError = true; }
        }
        
        //#借款人国民经济部门
        if (checknumList.contains("JS0105")) {
            isError = CheckUtil.checkEconamicSector(xdkfsxx.getIsgreenloan(), Arrays.asList(CheckUtil.dwdkjkrgmjjbm), tempMsg, isError, "借款人国民经济部门");
        }


        if(StringUtils.isNotBlank(xdkfsxx.getIsgreenloan())){
        	if(ArrayUtils.contains(CheckUtil.qygm,xdkfsxx.getEntpmode())){
        		if(!xdkfsxx.getIsgreenloan().startsWith("C") && !xdkfsxx.getIsgreenloan().startsWith("B")){
                    if (checknumList.contains("JS2627"))
                    {
                        tempMsg.append("借款人企业规模为CS01至CS04的，借款人国民经济部门应该为C开头的非金融企业部门或者B开头的金融机构" + spaceStr);
                        isError = true;
                    }
        		}
        	}
        	if(xdkfsxx.getBrowareacode().startsWith("000")){
        		if(!xdkfsxx.getIsgreenloan().startsWith("E")){
                    if (checknumList.contains("JS1930"))
                    {
                        tempMsg.append("借款人地区代码为000开头的，国民经济部门应为E开头的非居民部门" + spaceStr);
                        isError = true;
                    }
        		}
        	}else {
        		if(xdkfsxx.getIsgreenloan().startsWith("E")){
                    if (checknumList.contains("JS1931"))
                    {
                        tempMsg.append("借款人地区代码不是000开头的，国民经济部门不应为E开头的非居民部门" + spaceStr);
                        isError = true;
                    }
        		}
        	}
        }
        
        //#借款人行业
        if (StringUtils.isNotBlank(xdkfsxx.getBrowinds())) {
        	if("100".equals(xdkfsxx.getBrowinds())) {
                if (checknumList.contains("JS0106"))
                {
                    tempMsg.append("借款人行业不在行业标准大类范围内" + spaceStr);
                    isError = true;
                }
        	}else {
        		if (aindustryList == null) aindustryList = customSqlUtil.getBaseCode(BaseAindustry.class);
                if (!aindustryList.contains(xdkfsxx.getBrowinds())) {
                    if (checknumList.contains("JS0106"))
                    {
                    tempMsg.append("借款人行业不在行业标准大类范围内" + spaceStr);
                    isError = true;
                    }
                }
                if (StringUtils.isNotBlank(xdkfsxx.getIsgreenloan())&&xdkfsxx.getIsgreenloan().startsWith("E")) {
                    if (!"200".equals(xdkfsxx.getBrowinds())) {
                        if (checknumList.contains("JS1938"))
                        {
                            tempMsg.append("借款人国民经济部门为E开头的非居民部门，则借款人行业必须为200-境外" + spaceStr);
                            isError = true;
                        }
                    }
                } else {
                    if ("200".equals(xdkfsxx.getBrowinds())) {
                        if (checknumList.contains("JS1939"))
                        {
                            tempMsg.append("借款人国民经济部门不为E开头的，则借款人行业不能为200-境外" + spaceStr);
                            isError = true;
                        }
                    }
                }
			}
        } else {
            if (checknumList.contains("JS1033")) { tempMsg.append("借款人行业" + nullError + spaceStr);isError = true; }
        }
        
        //#借款人经济成分
        if (StringUtils.isNotBlank(xdkfsxx.getBrowinds()) &&  StringUtils.isNotBlank(xdkfsxx.getEntpmode())){
            if((!"200".equals(xdkfsxx.getBrowinds()))&&(!"CS05".equals(xdkfsxx.getEntpmode()))){
                if (StringUtils.isBlank(xdkfsxx.getEntpczjjcf())){
                    if (checknumList.contains("JS1035"))
                    {
                        tempMsg.append("当客户不为境外非居民以及境内非企业时，借款人经济成分不能为空" + spaceStr);
                        isError = true;
                    }
                }else {
                    if (!ArrayUtils.contains(CheckUtil.qyczrjjcf, xdkfsxx.getEntpczjjcf())) {
                        if (checknumList.contains("JS0109"))
                        {
                            tempMsg.append("借款人经济成分不在符合要求的值域范围" + spaceStr);
                            isError = true;
                        }
                    }
                }
            }
            if (xdkfsxx.getBrowinds().equals("200") || xdkfsxx.getEntpmode().equals("CS05")){
                if (StringUtils.isNotBlank(xdkfsxx.getEntpczjjcf())){
                    if (checknumList.contains("JS2814"))
                    {
                        tempMsg.append("当客户为境外非居民以及境内非企业时，借款人经济成分必须为空" + spaceStr);
                        isError = true;
                    }
                }
            }
        }
        
        //#借款人企业规模
        if (!"200".equals(xdkfsxx.getBrowinds())) {
            if(StringUtils.isBlank(xdkfsxx.getEntpmode())){
                if (checknumList.contains("JS1036"))
                {
                    tempMsg.append("当客户不为境外非居民时，借款人企业规模不能为空" + spaceStr);
                    isError = true;
                }
            }else {
            	if (!ArrayUtils.contains(CheckUtil.qygm2, xdkfsxx.getEntpmode())) {
                    if (checknumList.contains("JS0110"))
                    {
                        tempMsg.append("借款人企业规模不在符合要求的值域范围" + spaceStr);
                        isError = true;
                    }
                }
            	if(StringUtils.isNotBlank(xdkfsxx.getIsgreenloan())&&xdkfsxx.getIsgreenloan().startsWith("C")&&!xdkfsxx.getIsgreenloan().equals("C99")) {
            		if (!ArrayUtils.contains(CheckUtil.qygm, xdkfsxx.getEntpmode())) {
                        if (checknumList.contains("JS2628"))
                        {
                            tempMsg.append("借款人国民经济部门为C开头且不是C99的非金融企业部门，则借款人企业规模应该在CS01至CS04范围内");
                            isError = true;
                        }
                    }
            	}
			}
        }
        
        //#借款人证件代码
        if (StringUtils.isNotBlank(xdkfsxx.getBrowidcode())){
	        if (checkStr2(xdkfsxx.getBrowidcode())){
                if (checknumList.contains("JS0316"))
                {
                    tempMsg.append("借款人证件代码不能包含空格和特殊字符"+spaceStr);
                    isError=true;
                }
	        }
	        if (StringUtils.isNotBlank(xdkfsxx.getBrowareacode())) {
	            if (xdkfsxx.getBrowidcode().length() > 60) {
                    if (checknumList.contains("JS0325"))
                    {
                        tempMsg.append("借款人证件代码字符长度不能超过60" + spaceStr);
                        isError = true;
                    }
	            }
	        }
            if (StringUtils.isNotBlank(xdkfsxx.getBrowareacode())&&!xdkfsxx.getBrowareacode().startsWith("000")&&xdkfsxx.getIsfarmerloan().equals("A02")) {
                if (xdkfsxx.getBrowidcode().length() != 9){
                    if (checknumList.contains("JS2787"))
                    {
                        tempMsg.append("境内客户且借款人证件类型为A02的，借款人证件代码字符长度应该为9位" + spaceStr);
                        isError = true;
                    }
                }
            }
            if (!xdkfsxx.getBrowareacode().startsWith("000")&&!xdkfsxx.getIsfarmerloan().equals("A01")){
                if (checknumList.contains("JS2796"))
                {
                    tempMsg.append("境内客户的借款人证件类型应为A01-统一社会信用代码" + spaceStr);
                    isError = true;
                }
            }

            if (StringUtils.isNotBlank(xdkfsxx.getBrowareacode())&&!xdkfsxx.getBrowareacode().startsWith("000")&&xdkfsxx.getIsfarmerloan().equals("A01")) {
                if (xdkfsxx.getBrowidcode().length() != 18) {
                    if (checknumList.contains("JS0891"))
                    {
                        tempMsg.append("境内客户的借款人证件代码字符长度应该为18位" + spaceStr);
                        isError = true;
                    }
                }
            }
        }else {
            if (checknumList.contains("JS1031"))
            {
                tempMsg.append("借款人证件代码不能为空"+spaceStr);
                isError=true;
            }
        }
        
        //#借款人证件类型
        if (StringUtils.isNotBlank(xdkfsxx.getIsfarmerloan())) {
            if (!ArrayUtils.contains(CheckUtil.khzjlx, xdkfsxx.getIsfarmerloan())) {
                if (checknumList.contains("JS0104"))
                {
                    tempMsg.append("借款人证件类型不在符合要求的最底层值域范围内" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1030")) { tempMsg.append("借款人证件类型不能为空" + spaceStr);isError = true;  }
        }
        
        //#金融机构代码
        if (StringUtils.isNotBlank(xdkfsxx.getFinorgcode())){
            if (xdkfsxx.getFinorgcode().length()==18){
                if (CheckUtil.checkStr(xdkfsxx.getFinorgcode())){
                    if (checknumList.contains("JS0314")) {
                        tempMsg.append("金融机构代码不能包含空格和特殊字符"+spaceStr);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS0323")) { tempMsg.append("金融机构代码字符长度应该为18位"+spaceStr);isError=true;  }
            }

            if (fzList == null) {
                String sql = "select distinct finorgcode from xjrjgfz";
                fzList = customSqlUtil.executeQuery(sql);
            }
            if (frList == null) {
                String sql = "select distinct finorgcode from xjrjgfrbaseinfo";
                frList = customSqlUtil.executeQuery(sql);
            }
            if(checknumList.contains("JS1887")) {
                if(!fzList.contains(xdkfsxx.getFinorgcode()) && !frList.contains(xdkfsxx.getFinorgcode())) {
                    tempMsg.append("金融机构代码必须在金融机构（法人）基础信息表.金融机构代码或者金融机构（分支机构）基础信息表.金融机构代码中存在" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1027")) { tempMsg.append("金融机构代码不能为空" + spaceStr);isError = true;}
        }
        
        //#金融机构地区代码
        if (StringUtils.isNotBlank(xdkfsxx.getFinorgareacode())) {
	        if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
	        if (!areaList.contains(xdkfsxx.getFinorgareacode())) {
	            if (checknumList.contains("JS0108")) {
                    tempMsg.append("金融机构地区代码不在规定的代码范围内" + spaceStr);
                    isError = true;
                }
	        }
        } else {
            if (checknumList.contains("JS1029")){  tempMsg.append("金融机构地区代码不能为空" + spaceStr);isError = true; }
        }
        
        //#利率是否固定
        if (StringUtils.isNotBlank(xdkfsxx.getRateisfix())) {
            if (!ArrayUtils.contains(CheckUtil.llsfgd, xdkfsxx.getRateisfix())) {
                if (checknumList.contains("JS0114")){
                    tempMsg.append("利率是否固定不在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1047")){  tempMsg.append("利率是否固定不能为空" + spaceStr);isError = true;  }
        }
        
        //#利率水平
        if (StringUtils.isNotBlank(xdkfsxx.getRatelevel())) {
            if (xdkfsxx.getRatelevel().length() <= 10 && CheckUtil.checkDecimal(xdkfsxx.getRatelevel(), 5)) {
                if (CheckUtil.checkPerSign(xdkfsxx.getRatelevel())) {
                    if (checknumList.contains("JS0319")){
                        tempMsg.append("利率水平不能包含‰或%" + spaceStr);
                        isError = true;
                    }
                }
                if (!CheckUtil.checkPerSign(xdkfsxx.getRatelevel())){
                    if(new BigDecimal(xdkfsxx.getRatelevel()).compareTo(BigDecimal.ZERO) < 0||new BigDecimal(xdkfsxx.getRatelevel()).compareTo(new BigDecimal("30")) >0) {
                        if (checknumList.contains("JS2440")){
                            tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
                            isError = true;
                        }
                    }
                }
            } else {
                if (checknumList.contains("JS0332")){
                    tempMsg.append("利率水平总长度不能超过10位,小数位应保留5位" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1048")){  tempMsg.append("利率水平" + nullError + spaceStr);isError = true; }
        }
        
        //#内部机构号
//        isError = CheckUtil.grantNullAndLengthAndStr2(xdkfsxx.getFinorgincode(), 30, tempMsg, isError, "内部机构号");

        if (StringUtils.isNotBlank(xdkfsxx.getFinorgincode())){
            if (xdkfsxx.getFinorgincode().length()<=30){
                if (checkStr2(xdkfsxx.getFinorgincode())){
                    if (checknumList.contains("JS0315")){     tempMsg.append("内部机构号不能包含空格和特殊字符"+spaceStr);isError=true;   }
                }
            }else {
                if (checknumList.contains("JS0324")){   tempMsg.append("内部机构号字符长度不能超过30"+spaceStr);isError=true; }
            }
        }else {
            if (checknumList.contains("JS1028")){    tempMsg.append("内部机构号不能为空"+spaceStr);isError=true;}
        }



        //#是否首次贷款
        if (StringUtils.isNotBlank(xdkfsxx.getIsplatformloan())) {
            if (!ArrayUtils.contains(CheckUtil.sfcommon, xdkfsxx.getIsplatformloan())) {
                if (checknumList.contains("JS0118"))
                {
                    tempMsg.append("是否首次贷款不在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1053")) {   tempMsg.append("是否首次贷款不能为空" + spaceStr);isError = true;  }
        }
        
        //#数据日期
//        if (StringUtils.isNotBlank(xdkfsxx.getSjrq())) {
//        	boolean b = CheckUtil.checkDate(xdkfsxx.getSjrq(), "yyyy-MM-dd");
//            if (b) {
//            	if (xdkfsxx.getSjrq().compareTo("1900-01-01") < 0 || xdkfsxx.getSjrq().compareTo("2100-12-31") > 0) {
//            		tempMsg.append("数据日期需晚于1900-01-01早于2100-12-31" + spaceStr);
//            		isError = true;
//                }
//            } else {
//            	tempMsg.append("数据日期不符合yyyy-MM-dd格式" + spaceStr);
//            	isError = true;
//            }
//        } else {
//            tempMsg.append("数据日期" + nullError + spaceStr);
//            isError = true;
//        }
//
        //#资产证券化产品代码
        //数据日期
        isError = CheckUtil.nullAndDate(xdkfsxx.getSjrq(), tempMsg, isError, "数据日期",false);




        if("LF07".equals(xdkfsxx.getLoanstatus())) {
        	if (StringUtils.isNotBlank(xdkfsxx.getAssetproductcode())){
                if (xdkfsxx.getAssetproductcode().length()<=400){
                    if (checkStr2(xdkfsxx.getAssetproductcode())){
                        if (checknumList.contains("JS0321")) {
                            tempMsg.append("资产证券化产品代码不能包含空格和特殊字符"+spaceStr);
                            isError=true;
                        }
                    }
                }else {
                    if (checknumList.contains("JS0328")) {
                        tempMsg.append("当资产证券化产品代码不为空时，资产证券化产品代码长度不超过400"+spaceStr);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS1541")) {
                    tempMsg.append("当贷款状态为资产证券化转让时，资产证券化产品代码不能为空" + spaceStr);
                    isError = true;
                }
            }
        }else {
        	if (StringUtils.isNotBlank(xdkfsxx.getAssetproductcode())){
                if (checknumList.contains("JS2653")) {
                    tempMsg.append("当贷款状态不是资产证券化转让时，资产证券化产品代码必须为空" + spaceStr);
                    isError = true;
                }
        	}
		}

        //#整表
        if (isError) {
            if(!errorMsg.containsKey(xdkfsxx.getId())) {
                errorMsg.put(xdkfsxx.getId(), "贷款合同编码:"+xdkfsxx.getLoancontractcode()+"，"+"贷款借据编号:"+xdkfsxx.getLoanbrowcode()+"]->\r\n");
            }
            String str = errorMsg.get(xdkfsxx.getId());
            str = str + tempMsg;
            errorMsg.put(xdkfsxx.getId(), str);
            errorId.add(xdkfsxx.getId());
        } else {
            if(!errorId.contains(xdkfsxx.getId())) {
                rightId.add(xdkfsxx.getId());
            }
        }
    }




    //个人贷款发生额信息
    public static int checkXgrdkfsxx(CustomSqlUtil customSqlUtil, List<String> checknumList, Xgrdkfsxx xgrdkfsxx,LinkedHashMap<String, String> errorMsg, LinkedHashMap<String, String> errorMsg2,List<String> errorId, List<String> rightId) {
        boolean isError = false;
        StringBuffer tempMsg = new StringBuffer("");

        String nullError = "不能为空";
        String lengthError = "超过限制长度";
        String spaceStr = "|";
        //#贷款币种
        if (currencyList==null) currencyList = customSqlUtil.getBaseCode(BaseCurrency.class);

        if (StringUtils.isNotBlank(xgrdkfsxx.getLoancurrency())){
            if (checknumList.contains("JS0231"))
            {
                isError = CheckUtil.checkCurrency(xgrdkfsxx.getLoancurrency(), currencyList, customSqlUtil, tempMsg, isError, "贷款币种");
            }

        }else {
            if (checknumList.contains("JS1599")){ tempMsg.append("贷款币种不能为空" + spaceStr);isError = true; }
        }


        //#贷款财政扶持方式
        if (checknumList.contains("JS0234")){
            isError = CheckUtil.checkLoanczfcfs(xgrdkfsxx.getLoanfinancesupport(), tempMsg, isError, "贷款财政扶持方式");
        }

        //#贷款产品类别
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())) {
            if(!ArrayUtils.contains(CheckUtil.dkfsecplb, xgrdkfsxx.getLoanprocode())) {
                if (checknumList.contains("JS0230"))
                {
                    tempMsg.append("贷款产品类别不在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
            if (xgrdkfsxx.getLoanprocode().equals("F03")){
                if (checknumList.contains("JS2197"))
                {
                    tempMsg.append("贷款产品不能出现F03拆借" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1595"))
            {
                tempMsg.append("贷款产品类别" + nullError + spaceStr);
                isError = true;
            }
        }
        //#贷款担保方式
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&ArrayUtils.contains(CheckUtil.dkfsecplb, xgrdkfsxx.getLoanprocode())) {
            String temp = xgrdkfsxx.getLoanprocode().substring(0, 3);
            if(!"F01".equals(temp)&&!"F03".equals(temp)&&!"F06".equals(temp)&&!"F07".equals(temp)) {
                if (StringUtils.isNotBlank(xgrdkfsxx.getGteemethod())){
                    if(!ArrayUtils.contains(CheckUtil.dkdbfs, xgrdkfsxx.getGteemethod())) {
                        if (checknumList.contains("JS0235"))
                        {
                            tempMsg.append("贷款担保方式不在符合要求的值域范围内" + spaceStr);
                            isError = true;
                        }
                    }
                }else {
                    if (checknumList.contains("JS1536"))
                    {
                        tempMsg.append("当贷款产品类别为资产类贷款时，贷款担保方式"+nullError+spaceStr);
                        isError=true;
                    }
                }
            }
        }
        //#贷款到期日期
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanenddate())&&StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&!xgrdkfsxx.getLoanprocode().startsWith("F04")) {
            if (xgrdkfsxx.getLoanenddate().length() == 10) {
                boolean b = CheckUtil.checkDate(xgrdkfsxx.getLoanenddate(), "yyyy-MM-dd");
                if (b) {
                    if (xgrdkfsxx.getLoanenddate().compareTo("1800-01-01") < 0 || xgrdkfsxx.getLoanenddate().compareTo("2100-12-31") > 0) {
                        if (checknumList.contains("JS0948")) {
                                tempMsg.append("除透支类业务以外的贷款到期日期需晚于1800-01-01早于2100-12-31" + spaceStr);
                                isError = true;
                        }
                    }
                } else {
                        if (checknumList.contains("JS0948")) {
                            tempMsg.append("除透支类业务以外的贷款到期日期不符合yyyy-MM-dd格式" + spaceStr);
                            isError = true;
                        }

                }
            }
        } else {
            if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&!xgrdkfsxx.getLoanprocode().startsWith("F04") && StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan())&& !xgrdkfsxx.getIssupportliveloan().contains("线上联合消费贷款")){
                if (checknumList.contains("JS1597")) { tempMsg.append("除透支类业务以外的贷款到期日期" + nullError + spaceStr);isError = true;}
            }
        }

        //#贷款定价基准类型
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanfixamttype())) {
            if (!ArrayUtils.contains(CheckUtil.loanjzlx, xgrdkfsxx.getLoanfixamttype())) {
                if (checknumList.contains("JS0233"))
                {
                    tempMsg.append("贷款定价基准类型不在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1604"))
            {
                tempMsg.append("贷款定价基准类型" + nullError + spaceStr);
                isError = true;
            }
        }

        //#贷款发放日期
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanstartdate())&&StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&!xgrdkfsxx.getLoanprocode().startsWith("F04")) {
            if (xgrdkfsxx.getLoanstartdate().length() == 10) {
                boolean b1 = CheckUtil.checkDate(xgrdkfsxx.getLoanstartdate(), "yyyy-MM-dd");
                if (b1) {
                    if (xgrdkfsxx.getLoanstartdate().compareTo("1800-01-01") >= 0 && xgrdkfsxx.getLoanstartdate().compareTo("2100-12-31") <= 0) {
                        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanenddate()) &&
                                xgrdkfsxx.getLoanstartdate().compareTo(xgrdkfsxx.getLoanenddate()) > 0) {
                            if (checknumList.contains("JS2383"))
                            {
                                tempMsg.append("贷款发放日期不为空时，应小于等于贷款到期日期" + spaceStr);
                                isError = true;
                            }
                        }
                        if(StringUtils.isNotBlank(xgrdkfsxx.getSjrq()) &&
                                xgrdkfsxx.getLoanstartdate().compareTo(xgrdkfsxx.getSjrq()) > 0) {
                            if (checknumList.contains("JS2382"))
                            {
                                tempMsg.append("贷款发放日期不为空时，应小于等于数据日期" + spaceStr);
                                isError = true;
                            }
                        }
                        if("1".equals(xgrdkfsxx.getGivetakeid())) {
                            if(StringUtils.isNotBlank(xgrdkfsxx.getSjrq()) && CheckUtil.checkDate(xgrdkfsxx.getSjrq(), "yyyy-MM-dd")) {
                                if(!xgrdkfsxx.getLoanstartdate().substring(0,7).equals(xgrdkfsxx.getSjrq().substring(0,7))&&!xgrdkfsxx.getLoanstatus().equals("LF04")&&!xgrdkfsxx.getLoanprocode().startsWith("F04") && StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan())&& !xgrdkfsxx.getIssupportliveloan().contains("线上联合消费贷款")) {
                                    if (checknumList.contains("JS2561"))
                                    {
                                        tempMsg.append("当月发放的贷款且不是转入贷款的，贷款发放日期应该在当月范围内（透支类和、线上联合消费贷款类贷款除外）" + spaceStr);
                                        isError = true;
                                    }
                                }
                            }
                        }
                    } else {
                        if (checknumList.contains("JS0947")) {
                                tempMsg.append("除透支类业务以外的贷款发放日期需晚于1800-01-01早于2100-12-31" + spaceStr);
                                isError = true;
                            }
                    }
                } else {
                    if (checknumList.contains("JS0947")) {
                            tempMsg.append("除透支类业务以外的贷款发放日期不符合yyyy-MM-dd格式" + spaceStr);
                            isError = true;
                        }

                }
            } else {
//                if (checknumList.contains("JS0335")) { tempMsg.append("贷款发放日期长度不等于10" + spaceStr);isError = true;}
            }
        } else {
            if (!ArrayUtils.contains(CheckUtil.tzyw,xgrdkfsxx.getLoanprocode())&& StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan()) && !xgrdkfsxx.getIssupportliveloan().contains("线上联合消费贷款")){
                if (checknumList.contains("JS1596")) {  tempMsg.append("除透支类业务以外的贷款发放日期" + nullError + spaceStr);isError = true;}
            }
        }

        //#贷款发生金额
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanamt())) {
            if (xgrdkfsxx.getLoanamt().length() > 20 || !CheckUtil.checkDecimal(xgrdkfsxx.getLoanamt(), 2)) {
                if (checknumList.contains("JS0942"))
                {
                    tempMsg.append("贷款发生金额总长度不能超过20位,小数位应保留2位" + spaceStr);
                    isError = true;
                }
            }
            if(new BigDecimal(xgrdkfsxx.getLoanamt()).compareTo(BigDecimal.ZERO)<=0) {
                if (checknumList.contains("JS2385"))
                {
                    tempMsg.append("贷款发生金额应大于0" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1600"))
            {
                tempMsg.append("贷款发生金额" + nullError + spaceStr);
                isError = true;
            }
        }
        //#贷款发生金额折人民币
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoancnyamt())) {
            if (xgrdkfsxx.getLoancnyamt().length() > 20 || !CheckUtil.checkDecimal(xgrdkfsxx.getLoancnyamt(), 2)) {
                if (checknumList.contains("JS0943"))
                {
                    tempMsg.append("贷款发生金额折人民币总长度不能超过20位,小数位应保留2位" + spaceStr);
                    isError = true;
                }
            }
            if (Double.valueOf(xgrdkfsxx.getLoancnyamt())>100000000){
                if (checknumList.contains("JS2882"))
                {
                    tempMsg.append("个人贷款的发生金额折人民币应不超过1亿" + spaceStr);
                    isError = true;
                }
            }

            if(new BigDecimal(xgrdkfsxx.getLoancnyamt()).compareTo(BigDecimal.ZERO)<=0) {
                if (checknumList.contains("JS2386"))
                {
                    tempMsg.append("贷款发生金额折人民币应大于0" + spaceStr);
                    isError = true;
                }
            }
            if("CNY".equals(xgrdkfsxx.getLoancurrency())){
                if (StringUtils.isNotBlank(xgrdkfsxx.getLoanamt())) {
                    if(!xgrdkfsxx.getLoanamt().equals(xgrdkfsxx.getLoancnyamt())) {
                        if (checknumList.contains("JS2712"))
                        {
                            tempMsg.append("币种为人民币的，贷款发生金额折人民币应该与贷款发生金额的值相等" + spaceStr);
                            isError = true;
                        }
                    }
                }else {
                    if (checknumList.contains("JS2712"))
                    {
                        tempMsg.append("币种为人民币的，贷款发生金额折人民币应该与贷款发生金额的值相等" + spaceStr);
                        isError = true;
                    }
                }
            }
        } else {
            if (checknumList.contains("JS1601"))
            {
                tempMsg.append("贷款发生金额折人民币" + nullError + spaceStr);
                isError = true;
            }
        }
        //#贷款合同编码
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoancontractcode())){
            if (xgrdkfsxx.getLoancontractcode().length()<=100){
                if (checkStr2(xgrdkfsxx.getLoancontractcode())){
                    if (checknumList.contains("JS0930"))
                    {
                        tempMsg.append("贷款合同编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS0939")) {  tempMsg.append("贷款合同编码字符长度不能超过100"+spaceStr);isError=true;}

            }
        }else {
            if (checknumList.contains("JS1594")) {   tempMsg.append("贷款合同编码不能为空" + nullError + spaceStr);isError = true;}
        }

        //#贷款借据编码
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanbrowcode())){
            if (xgrdkfsxx.getLoanbrowcode().length()<=100){
                if (checkStr2(xgrdkfsxx.getLoanbrowcode())){
                    if (checknumList.contains("JS0929"))
                    {
                        tempMsg.append("贷款借据编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS0938")) {  tempMsg.append("贷款借据编码字符长度不能超过100"+spaceStr);isError=true;}

            }
        }else {
            if (checknumList.contains("JS1593")) {   tempMsg.append("贷款借据编码不能为空" + nullError + spaceStr);isError = true;}
        }


        //#贷款利率重新定价日
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanraterepricedate())) {
            boolean b = CheckUtil.checkDate(xgrdkfsxx.getLoanraterepricedate(), "yyyy-MM-dd");
            if (b) {
                if (xgrdkfsxx.getLoanraterepricedate().compareTo("1800-01-01") < 0 || xgrdkfsxx.getLoanraterepricedate().compareTo("2100-12-31") > 0) {
                    //排除透支业务
                    if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode()) && !xgrdkfsxx.getLoanprocode().startsWith("F04")) {
                        if (checknumList.contains("JS0950")) {
                            tempMsg.append("除透支类业务以外的贷款利率重新定价日需晚于1800-01-01早于2100-12-31" + spaceStr);
                            isError = true;
                        }
                    }
                }
                if(StringUtils.isNotBlank(xgrdkfsxx.getLoanstartdate())) {
                    if(xgrdkfsxx.getLoanraterepricedate().compareTo(xgrdkfsxx.getLoanstartdate()) < 0) {
                        if (checknumList.contains("JS2389"))
                        {
                            tempMsg.append("贷款利率重新定价日不为空时，应大于等于贷款发放日期" + spaceStr);
                            isError = true;
                        }
                    }
                }
            } else {
                //排除透支业务
                if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode()) &&!xgrdkfsxx.getLoanprocode().startsWith("F04")) {
                    if (checknumList.contains("JS0950")) {
                        tempMsg.append("除透支类业务以外的贷款利率重新定价日不符合yyyy-MM-dd格式" + spaceStr);
                        isError = true;
                    }

                }
            }
        } else {
            if (!ArrayUtils.contains(CheckUtil.tzyw,xgrdkfsxx.getLoanprocode()) && StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan())&& !xgrdkfsxx.getIssupportliveloan().contains("线上联合消费贷款")){
                if (checknumList.contains("JS1606")) {  tempMsg.append("除透支类业务以外的贷款利率重新定价日" + nullError + spaceStr);isError = true;  }
            }
        }

        //#贷款实际终止日期  //排除透支业务
        if (StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan()) && !xgrdkfsxx.getIssupportliveloan().contains("线上联合消费贷款")&&!ArrayUtils.contains(CheckUtil.tzyw,xgrdkfsxx.getLoanprocode())&&"0".equals(xgrdkfsxx.getGivetakeid())&&((StringUtils.isNotBlank(xgrdkfsxx.getLoanstatus()) && ArrayUtils.contains(CheckUtil.loanstatus4, xgrdkfsxx.getLoanstatus()))||("LF05".equals(xgrdkfsxx.getLoanstatus())&&("01".equals(xgrdkfsxx.getLoanrestructuring())||"02".equals(xgrdkfsxx.getLoanrestructuring()))))) {
            if (StringUtils.isNotBlank(xgrdkfsxx.getLoanactenddate())) {
                if (xgrdkfsxx.getLoanactenddate().length() == 10) {
                    boolean b1 = CheckUtil.checkDate(xgrdkfsxx.getLoanactenddate(), "yyyy-MM-dd");
                    if (b1) {
                        if (xgrdkfsxx.getLoanactenddate().compareTo("1800-01-01") >= 0 && xgrdkfsxx.getLoanactenddate().compareTo("2100-12-31") <= 0) {
                            if (StringUtils.isNotBlank(xgrdkfsxx.getLoanstartdate()) &&
                                    xgrdkfsxx.getLoanstartdate().compareTo(xgrdkfsxx.getLoanenddate()) > 0) {
                                if (checknumList.contains("JS2384"))
                                {
                                    tempMsg.append("当贷款实际终止日期不为空时，贷款实际终止日期应大于等于贷款发放日期" + spaceStr);
                                    isError = true;
                                }
                            }
                        } else {
                            if (checknumList.contains("JS0949")) { tempMsg.append("贷款实际终止日期需晚于1800-01-01早于2100-12-31" + spaceStr);isError = true;}
                        }
                    } else {
                        if (checknumList.contains("JS0949")) {tempMsg.append("贷款实际终止日期不符合yyyy-MM-dd格式" + spaceStr);isError = true;}
                    }
                } else {
//                    if (checknumList.contains("JS0337")) {  tempMsg.append("贷款实际终止日期长度不等于10" + spaceStr);isError = true;}
                }
            } else {
                if (checknumList.contains("JS1535")) { tempMsg.append("除透支类业务以外的贷款，当贷款状态为核销、剥离、转让、重组、以物抵债、债转股且发放/收回标识为收回时，贷款实际终止日期" + nullError + spaceStr);isError = true;}

            }
        }
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanactenddate())){
//            Calendar cal = Calendar.getInstance();
//            int year = cal.get(Calendar.YEAR);
//            int month = cal.get(Calendar.MONTH) + 1;
//            String monthStr;
//            if(month<10) {
//                monthStr = year+"-0"+month;
//            }else {
//                monthStr = year+"-"+month;
//            }
            if (StringUtils.isNotBlank(xgrdkfsxx.getSjrq()) &&
                    (xgrdkfsxx.getLoanactenddate().compareTo(xgrdkfsxx.getSjrq()) > 0) || !xgrdkfsxx.getLoanactenddate().substring(0,7).equals(xgrdkfsxx.getSjrq().substring(0,7))) {
                if (checknumList.contains("JS2390"))
                {
                    tempMsg.append("当贷款实际终止日期不为空时，贷款实际终止日期应小于等于数据日期且应该在报送期内" + spaceStr);
                    isError = true;
                }
            }

        }


        //#贷款用途
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&ArrayUtils.contains(CheckUtil.dkfsecplb, xgrdkfsxx.getLoanprocode())) {
            String temp = xgrdkfsxx.getLoanprocode().substring(0,3);
            if(!"F01".equals(temp)&&!"F03".equals(temp)&&!"F06".equals(temp)&&!"F07".equals(temp)) {
                if (StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan())){
                    if (xgrdkfsxx.getIssupportliveloan().length()<=3000){
                        if (CheckUtil.checkStr5(xgrdkfsxx.getIssupportliveloan())){
                            if (checknumList.contains("JS0934")) {
                                tempMsg.append("贷款用途不能包含空格和特殊字符"+spaceStr);
                                isError=true;
                            }
                        }
                    }else {
                        if (checknumList.contains("JS0941")) {   tempMsg.append("贷款用途"+lengthError+3000+spaceStr);isError=true;   }
                    }
                }else {
                    if (checknumList.contains("JS1612")) { 	tempMsg.append("当贷款产品类别为资产类贷款时，贷款用途"+nullError+spaceStr);isError=true;   }
                }
            }
        }

        //#贷款重组方式
        if("LF05".equals(xgrdkfsxx.getLoanstatus())) {
            if (StringUtils.isNotBlank(xgrdkfsxx.getLoanrestructuring())){
                if (!ArrayUtils.contains(CheckUtil.dkczfs, xgrdkfsxx.getLoanrestructuring())){
                    if (checknumList.contains("JS0238"))
                    {
                        tempMsg.append("贷款重组方式不在符合要求的值域范围内"+spaceStr);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS1610"))
                {
                    tempMsg.append("当贷款状态为重组时，贷款重组方式"+nullError+spaceStr);
                    isError=true;
                }
            }
        }else {
            if (StringUtils.isNotBlank(xgrdkfsxx.getLoanrestructuring())){
                if (checknumList.contains("JS2652"))
                {
                    tempMsg.append("贷款状态不为重组时，贷款重组方式必须为空"+spaceStr);
                    isError=true;
                }
            }
        }

        //#贷款状态
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanstatus())) {
            if (!ArrayUtils.contains(CheckUtil.loanstatus5, xgrdkfsxx.getLoanstatus())) {
                if (checknumList.contains("JS0237"))
                {
                    tempMsg.append("贷款状态不符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1609")) {  tempMsg.append("贷款状态" + nullError + spaceStr);isError = true; }
        }

        //#发放/收回标识
        if (StringUtils.isNotBlank(xgrdkfsxx.getGivetakeid())) {
            if (!ArrayUtils.contains(CheckUtil.sfcommon, xgrdkfsxx.getGivetakeid())) {
                if (checknumList.contains("JS0239"))
                {
                    tempMsg.append("发放/收回标识不符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
            if(StringUtils.isNotBlank(xgrdkfsxx.getLoanactenddate())) {
                if(!"0".equals(xgrdkfsxx.getGivetakeid())) {
                    if (checknumList.contains("JS2198"))
                    {
                        tempMsg.append("贷款实际终止日期不为空的则发放/收回标识应该为0-收回" + spaceStr);
                        isError = true;
                    }
                }
            }
            if (StringUtils.isNotBlank(xgrdkfsxx.getLoanstatus())&&ArrayUtils.contains(CheckUtil.loanstatus6, xgrdkfsxx.getLoanstatus())){
                if (xgrdkfsxx.getGivetakeid().equals("1")){
                    if (checknumList.contains("JS2191")) {
                        tempMsg.append("发生核销、剥离、以物抵债、资产证券化转让、债转股等交易的贷款发放/收回标识不能为1-发放" + spaceStr);
                        isError = true;}
                }
            }
        } else {
            if (checknumList.contains("JS1611 ")) {   tempMsg.append("发放/收回标识" + nullError + spaceStr);isError = true;  }
        }

        //#基准利率
        if ("RF02".equals(xgrdkfsxx.getRateisfix())) {
            if (StringUtils.isNotBlank(xgrdkfsxx.getRate())) {
                if(xgrdkfsxx.getRate().length()>10||!CheckUtil.checkDecimal(xgrdkfsxx.getRate(), 5)) {
                    if (checknumList.contains("JS0945"))
                    {
                        tempMsg.append("当基准利率不为空时，基准利率总长度不能超过10位，小数位必须为5位" + spaceStr);
                        isError = true;
                    }
                }else {
                    if (CheckUtil.checkPerSign(xgrdkfsxx.getRate())) {
                        if (checknumList.contains("JS0932"))
                        {
                            tempMsg.append("利率水平不能包含‰或%" + spaceStr);
                            isError = true;
                        }
                    }
                    if(new BigDecimal(xgrdkfsxx.getRate()).compareTo(BigDecimal.ZERO) < 0||new BigDecimal(xgrdkfsxx.getRate()).compareTo(new BigDecimal("30")) >0) {
                        if (checknumList.contains("JS2388"))
                        {
                            tempMsg.append("当为浮动利率贷款时，基准利率应大于等于0且小于等于30" + spaceStr);
                            isError = true;
                        }
                    }
                }
            } else {
                if (checknumList.contains("JS1605")) { tempMsg.append("当为浮动利率贷款时，基准利率不能为空" + spaceStr);isError = true; }
            }
        }else if("RF01".equals(xgrdkfsxx.getRateisfix())){
            if (StringUtils.isNotBlank(xgrdkfsxx.getRate())) {
                if (checknumList.contains("JS2160"))
                {
                    tempMsg.append("利率类型为固定利率的，基准利率必须为空" + spaceStr);
                    isError = true;
                }
            }
        }

        //#交易流水号
//        isError = CheckUtil.grantNullAndLengthAndStr2(xdkfsxx.getTransactionnum(), 60, tempMsg, isError, "交易流水号");
        if (StringUtils.isNotBlank(xgrdkfsxx.getTransactionnum())&&StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&!xgrdkfsxx.getLoanprocode().startsWith("F04")){
            if (xgrdkfsxx.getTransactionnum().length()<=60){
                if (checkStr2(xgrdkfsxx.getTransactionnum())){
                    if (checknumList.contains("JS0952"))
                    {
                        tempMsg.append("交易流水号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                        isError = true;
                    }
                }
            }else {
                if (checknumList.contains("JS0953"))
                {
                    tempMsg.append("交易流水号字段长度不能超过60" + spaceStr);
                    isError = true;
                }
            }

        }else {
            if (!ArrayUtils.contains(CheckUtil.tzyw,xgrdkfsxx.getLoanprocode()) && StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan()) && !xgrdkfsxx.getIssupportliveloan().contains("线上联合消费贷款")){
                if (checknumList.contains("JS1616"))
                {
                    tempMsg.append("除透支类业务以外的交易流水号不能为空" + spaceStr);
                    isError = true;
                }
            }
        }



        //#借款人地区代码
        if (StringUtils.isNotBlank(xgrdkfsxx.getBrowareacode())) {
            if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
            if (countryList == null) countryList = customSqlUtil.getBaseCode(BaseCountry.class);
            if (!areaList.contains(xgrdkfsxx.getBrowareacode())&& !countryList.contains(xgrdkfsxx.getBrowareacode())) {
                if (checknumList.contains("JS0228"))
                {
                    tempMsg.append("借款人地区代码不在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1592")) {  tempMsg.append("借款人地区代码" + nullError + spaceStr);isError = true; }
        }


        //#借款人证件代码
        if (StringUtils.isNotBlank(xgrdkfsxx.getBrowidcode())){
            if (StringUtils.isNotBlank(xgrdkfsxx.getIsfarmerloan())&& xgrdkfsxx.getIsfarmerloan().equals("A01")){
                if(xgrdkfsxx.getBrowidcode().length() != 18){
                    if (checknumList.contains("JS2178"))
                    {
                        tempMsg.append("借款人证件类型为统一社会信用代码的，证件代码长度必须为18位"+spaceStr);
                        isError=true;
                    }
                }
            }

            if (checkStr2(xgrdkfsxx.getBrowidcode())){
                if (checknumList.contains("JS0928"))
                {
                    tempMsg.append("借款人证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                    isError=true;
                }
            }
            if (StringUtils.isNotBlank(xgrdkfsxx.getBrowidcode())){
                if (xgrdkfsxx.getBrowidcode().length() > 60) {
                    if (checknumList.contains("JS0937"))
                    {
                        tempMsg.append("借款人证件代码字符长度不能超过60" + spaceStr);
                        isError = true;
                    }
                }
            }
            if (StringUtils.isNotBlank(xgrdkfsxx.getIsfarmerloan())&&xgrdkfsxx.getIsfarmerloan().startsWith("B")){
                if (xgrdkfsxx.getIsfarmerloan().equals("B01") || xgrdkfsxx.getIsfarmerloan().equals("B08")){
                    if (TuoMinUtils.getB01(xgrdkfsxx.getBrowidcode()).length() != 46){
                        if (checknumList.contains("JS2656")){  tempMsg.append("借款人证件类型为B01-身份证或者B08-临时身份证的，借款人证件代码脱敏后的长度应该为46" + spaceStr);isError = true;}
                    }
                }else {
                    if (TuoMinUtils.getB01otr(xgrdkfsxx.getBrowidcode()).length() != 32){
                        if (checknumList.contains("JS2666")){ tempMsg.append("借款人证件类型为B开头的个人证件且不是B01-身份证和B08-临时身份证的，借款人证件代码脱敏后的长度应该为32"+spaceStr);isError=true;}
                    }
                }

                if (xgrdkfsxx.getIsfarmerloan().equals("B01")){
                    if (StringUtils.isNotBlank(xgrdkfsxx.getSjrq())&&xgrdkfsxx.getBrowidcode().substring(6,14).compareTo(xgrdkfsxx.getSjrq().replace("-","")) >=0){
                        if (checknumList.contains("JS2676")){
                            tempMsg.append("证件类型为B01-身份证的，第7-14位的截取结果应该<数据日期"+spaceStr);isError=true;
                        }

                    }
                }
            }

        }else {
            if (checknumList.contains("JS1591"))
            {
                tempMsg.append("借款人证件代码不能为空"+spaceStr);
                isError=true;
            }
        }

        //#借款人证件类型
        if (StringUtils.isNotBlank(xgrdkfsxx.getIsfarmerloan())) {
            if (!ArrayUtils.contains(CheckUtil.grkhzjlx, xgrdkfsxx.getIsfarmerloan())) {
                if (checknumList.contains("JS0227"))
                {
                    tempMsg.append("借款人证件类型不在符合要求的最底层值域范围内" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1590")) { tempMsg.append("借款人证件类型不能为空" + spaceStr);isError = true;  }
        }

        //#金融机构代码
        if (StringUtils.isNotBlank(xgrdkfsxx.getFinorgcode())){
            if (xgrdkfsxx.getFinorgcode().length()==18){
                if (CheckUtil.checkStr(xgrdkfsxx.getFinorgcode())){
                    if (checknumList.contains("JS0926")) {
                        tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS0935")) { tempMsg.append("金融机构代码字符长度应该为18位"+spaceStr);isError=true;  }
            }

            if (fzList == null) {
                String sql = "select distinct finorgcode from xjrjgfz";
                fzList = customSqlUtil.executeQuery(sql);
            }
            if (frList == null) {
                String sql = "select distinct finorgcode from xjrjgfrbaseinfo";
                frList = customSqlUtil.executeQuery(sql);
            }
            if(checknumList.contains("JS1887")) {
                if(!fzList.contains(xgrdkfsxx.getFinorgcode()) && !frList.contains(xgrdkfsxx.getFinorgcode())) {
                    tempMsg.append("金融机构代码必须在金融机构（法人）基础信息表.金融机构代码或者金融机构（分支机构）基础信息表.金融机构代码中存在" + spaceStr);
                    isError = true;
                }
            }

        }else {
            if (checknumList.contains("JS1587")) { tempMsg.append("金融机构代码不能为空" + spaceStr);isError = true;}
        }

        //#金融机构地区代码
        if (StringUtils.isNotBlank(xgrdkfsxx.getFinorgareacode())) {
            if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
            if (!areaList.contains(xgrdkfsxx.getFinorgareacode())) {
                if (checknumList.contains("JS0229")) {
                    tempMsg.append("金融机构地区代码不在规定的代码范围内" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1589")){  tempMsg.append("金融机构地区代码不能为空" + spaceStr);isError = true; }
        }

        //#利率是否固定
        if (StringUtils.isNotBlank(xgrdkfsxx.getRateisfix())) {
            if (!ArrayUtils.contains(CheckUtil.llsfgd, xgrdkfsxx.getRateisfix())) {
                if (checknumList.contains("JS0232")){
                    tempMsg.append("个人贷款发生额信息中的利率是否固定需在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1602")){  tempMsg.append("利率是否固定不能为空" + spaceStr);isError = true;  }
        }

        //#利率水平
        if (StringUtils.isNotBlank(xgrdkfsxx.getRatelevel())) {
            if (xgrdkfsxx.getRatelevel().length() <= 10 && CheckUtil.checkDecimal(xgrdkfsxx.getRatelevel(), 5)) {
                if (CheckUtil.checkPerSign(xgrdkfsxx.getRatelevel())) {
                    if (checknumList.contains("JS0931")){
                        tempMsg.append("利率水平不能包含‰或%" + spaceStr);
                        isError = true;
                    }
                }
                if (!CheckUtil.checkPerSign(xgrdkfsxx.getRatelevel())){
                    if(new BigDecimal(xgrdkfsxx.getRatelevel()).compareTo(BigDecimal.ZERO) < 0||new BigDecimal(xgrdkfsxx.getRatelevel()).compareTo(new BigDecimal("30")) >0) {
                        if (checknumList.contains("JS2387")){
                            tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
                            isError = true;
                        }
                    }
                }
            } else {
                if (checknumList.contains("JS0944")){
                    tempMsg.append("利率水平总长度不能超过10位,小数位应保留5位" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1603")){  tempMsg.append("利率水平" + nullError + spaceStr);isError = true; }
        }

        //#内部机构号
//        isError = CheckUtil.grantNullAndLengthAndStr2(xdkfsxx.getFinorgincode(), 30, tempMsg, isError, "内部机构号");

        if (StringUtils.isNotBlank(xgrdkfsxx.getFinorgincode())){
            if (xgrdkfsxx.getFinorgincode().length()<=30){
                if (checkStr2(xgrdkfsxx.getFinorgincode())){
                    if (checknumList.contains("JS0927")){     tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
                }
            }else {
                if (checknumList.contains("JS0936")){   tempMsg.append("内部机构号字符长度不能超过30"+spaceStr);isError=true; }
            }
        }else {
            if (checknumList.contains("JS1588")){    tempMsg.append("内部机构号不能为空"+spaceStr);isError=true;}
        }



        //#是否首次贷款
        if (StringUtils.isNotBlank(xgrdkfsxx.getIsplatformloan())) {
            if (!ArrayUtils.contains(CheckUtil.sfcommon, xgrdkfsxx.getIsplatformloan())) {
                if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&!xgrdkfsxx.getLoanprocode().startsWith("F04")){
                    if (checknumList.contains("JS0236"))
                    {
                        tempMsg.append("是否首次贷款需在符合要求的值域范围内（透支类业务除外）" + spaceStr);
                        isError = true;
                    }
                }
            }
        }else {
            if (checknumList.contains("JS1608")) {   tempMsg.append("是否首次贷款不能为空" + spaceStr);isError = true;  }
        }

        //#数据日期
//        if (StringUtils.isNotBlank(xdkfsxx.getSjrq())) {
//        	boolean b = CheckUtil.checkDate(xdkfsxx.getSjrq(), "yyyy-MM-dd");
//            if (b) {
//            	if (xdkfsxx.getSjrq().compareTo("1900-01-01") < 0 || xdkfsxx.getSjrq().compareTo("2100-12-31") > 0) {
//            		tempMsg.append("数据日期需晚于1900-01-01早于2100-12-31" + spaceStr);
//            		isError = true;
//                }
//            } else {
//            	tempMsg.append("数据日期不符合yyyy-MM-dd格式" + spaceStr);
//            	isError = true;
//            }
//        } else {
//            tempMsg.append("数据日期" + nullError + spaceStr);
//            isError = true;
//        }
//
        //#资产证券化产品代码
        //数据日期
//        isError = CheckUtil.nullAndDate(xgrdkfsxx.getSjrq(), tempMsg, isError, "数据日期",false);




        if("LF07".equals(xgrdkfsxx.getLoanstatus())) {
            if (StringUtils.isNotBlank(xgrdkfsxx.getAssetproductcode())){
                if (xgrdkfsxx.getAssetproductcode().length()<=400){
                    if (checkStr2(xgrdkfsxx.getAssetproductcode())){
                        if (checknumList.contains("JS0933")) {
                            tempMsg.append("资产证券化产品代码不能包含空格和特殊字符"+spaceStr);
                            isError=true;
                        }
                    }
                }else {
                    if (checknumList.contains("JS0940")) {
                        tempMsg.append("当资产证券化产品代码不为空时，资产证券化产品代码长度不超过400"+spaceStr);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS1614")) {
                    tempMsg.append("当贷款状态为资产证券化转让时，资产证券化产品代码不能为空" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (StringUtils.isNotBlank(xgrdkfsxx.getAssetproductcode())){
                if (checknumList.contains("JS2654")) {
                    tempMsg.append("当贷款状态不是资产证券化转让时，资产证券化产品代码必须为空" + spaceStr);
                    isError = true;
                }
            }
        }
        //透支业务
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&xgrdkfsxx.getLoanprocode().startsWith("F04")){
            if (!StringUtils.isNotBlank(xgrdkfsxx.getLoanactenddate())){
                if (checknumList.contains("JS2801")) {
                    tempMsg.append("透支类业务的贷款实际终止日期应为空" + spaceStr);
                    isError = true;
                }
            }
            if (!StringUtils.isNotBlank(xgrdkfsxx.getLoanstartdate())){
                if (checknumList.contains("JS2802")) {
                    tempMsg.append("透支类业务的贷款发放日期应为空" + spaceStr);
                    isError = true;
                }
            }
            if (!StringUtils.isNotBlank(xgrdkfsxx.getLoanenddate())){
                if (checknumList.contains("JS2803")) {
                    tempMsg.append("透支类业务的贷款到期日期应为空" + spaceStr);
                    isError = true;
                }
            }
            if (!StringUtils.isNotBlank(xgrdkfsxx.getLoanraterepricedate())){
                if (checknumList.contains("JS2804")) {
                    tempMsg.append("透支类业务的贷款利率重新定价日应为空" + spaceStr);
                    isError = true;
                }
            }
            if (!StringUtils.isNotBlank(xgrdkfsxx.getTransactionnum())){
                if (checknumList.contains("JS2807")) {
                    tempMsg.append("透支类业务的交易流水号应为空" + spaceStr);
                    isError = true;
                }
            }
        }
        //#整表
        if (isError || errorMsg.containsKey(xgrdkfsxx.getId().toString())) {
            errorMsg2.put(xgrdkfsxx.getId().toString(), "贷款合同编码:"+xgrdkfsxx.getLoancontractcode()+"，"+"贷款借据编号:"+xgrdkfsxx.getLoanbrowcode()+"]->\r\n");
            String str = errorMsg.get(xgrdkfsxx.getId().toString());
            str = str == null ? "贷款合同编码:" + xgrdkfsxx.getLoancontractcode() + "，"+"贷款借据编号:"+xgrdkfsxx.getLoanbrowcode()+"]->\r\n" : str;
            str = str + tempMsg;
            errorMsg2.put(xgrdkfsxx.getId().toString(), str);
            errorId.add(xgrdkfsxx.getId().toString());
            return 1;
        } else {
            if(!errorId.contains(xgrdkfsxx.getId().toString())) {
                rightId.add(xgrdkfsxx.getId().toString());
            }
            return 0 ;
        }
    }


    public static void main(String[] args) {
        LinkedHashMap<String, String> errorMsg=new LinkedHashMap<>();
        errorMsg.put("03696", "贷款合同编码:"+3333+"，"+"贷款借据编号:"+545454+"]->\r\n 当为浮动利率贷款时，基准利率不能为空|666");
        errorMsg.put("03697", "贷款合同编码:"+5555+"，"+"贷款借据编号:"+88888+"]->\r\n 当为浮动利率贷款时，基准利率不能为空|666");
        for(Map.Entry<String, String> entry : errorMsg.entrySet()) {
            System.out.println("key:" + entry.getKey() + "   value:" + entry.getValue());
        }
/*
        StringBuffer tempMsg=new StringBuffer();
        tempMsg.append("当为浮动利率贷款时，基准利率不能为空" + "|");
        tempMsg.append("错误提示" + "|");
        String str = errorMsg.get("03696");
        str = str + tempMsg;
        errorMsg.put("03696", str);*/

        //StringBuffer errorMsgAll = new StringBuffer("");
        for(Map.Entry<String, String> entry : errorMsg.entrySet()) {
           // System.out.println("数据ID:"+entry.getKey());
           // System.out.println("列ID:"+entry.getKey());
        }
        String  s1="]->\n";
        String s="交易对手代码:888，存款账户编码:25]->\nA|b|";
        //String[] error=s.split(s1);

        //System.out.println(error[1]);
        for(String i:s.split("，")){
            for(String j:i.split(":")){//四个
               // System.out.println(j.replace(s1,""));
            }
        }
        //System.out.println();
        //System.out.println(errorMsgAll.toString());
    }
}
