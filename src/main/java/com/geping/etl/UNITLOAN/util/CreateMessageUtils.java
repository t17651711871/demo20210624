package com.geping.etl.UNITLOAN.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.UNITLOAN.entity.report.Xdkdbwx;
import com.geping.etl.UNITLOAN.entity.report.Xjrjgfz;

/**
 * 生成报文工具
 * @author WuZengWen
 * @date 2020年6月11日 下午5:28:59
 */
public class CreateMessageUtils {

	/**
	 * 生成指定数据的报文
	 * @param messagepath 生成报文存放路径
	 * @param fileName 生成的报文全名称
	 * @param dataList 报文数据
	 * @param code 报文类型
	 * @return 返回1为正确否则错误
	 */
	public static String shengChengBaoWen(String messagepath,String fileName,List<?> dataList,TableCodeEnum code) {
		StringBuffer result = new StringBuffer();
		if(dataList!=null&&dataList.size()>0) {
			if(TableCodeEnum.DWDKDBWXX.equals(code)) {
				shengChengBaoWenForDWDKDBWXX(messagepath,fileName,(List<Xdkdbwx>)dataList,result);
			}else if(TableCodeEnum.JRJGFZJCXX.equals(code)) {
				shengChengBaoWenForJRJGFZJCXX(messagepath,fileName,(List<Xjrjgfz>)dataList,result);
			}
		}else {
			result.append("要生成报文的数据集合为空");
		}
		
		if(result.length() == 0) {
			result.append("1");
		}
		return result.toString();
	}
	
	/**
	 * 生成指定类型的报文
	 * @param messagepath 生成报文存放路径
	 * @param fileName 生成的报文全名称
	 * @param code 报文类型
	 * @return 返回1为正确否则错误
	 */
	public static String shengChengBaoWen(String messagepath,String fileName,TableCodeEnum code) {
		StringBuffer result = new StringBuffer();
		StringBuffer exportString = new StringBuffer();  //文件
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
    	FileOutputStream fos=null;
		BufferedWriter bw = null;
		if(TableCodeEnum.DWDKDBWXX.equals(code)) {
			String query = "select id,gteecontractcode,loancontractcode,gteegoodscode,gteegoodscategory,warrantcode,isfirst,assessmode,assessmethod,assessvalue,assessdate,gteegoodsamt,firstrightamt,gteegoodsstataus,mortgagepgerate from xdkdbwx where datastatus='3'";
			try {
				File infofile = new File(messagepath+"\\report\\"+fileName);
				if(!infofile.exists()) {
					infofile.getParentFile().mkdirs();
				}
				infofile.createNewFile();
				fos = new FileOutputStream(infofile,true);
				bw = new BufferedWriter(new OutputStreamWriter(fos,"GB18030"));
			    con = JDBCUtil.getConnection("/intg/jdbc.properties", "mysql.driverClassName", "mysql.url", "mysql.username", "mysql.password");
		    	ps = con.prepareStatement(query);
				rs = ps.executeQuery();
				while (rs.next()) {
					/*Xdkdbwx xx = new Xdkdbwx();
					xx.setId(rs.getString(1));
					xx.setGteecontractcode(rs.getString(2));
					xx.setLoancontractcode(rs.getString(3));
					xx.setGteegoodscode(rs.getString(4));
					xx.setGteegoodscategory(rs.getString(5));
					xx.setWarrantcode(rs.getString(6));
					xx.setIsfirst(rs.getString(7));
					xx.setAssessmode(rs.getString(8));
					xx.setAssessmethod(rs.getString(9));
					xx.setAssessvalue(rs.getString(10));
					xx.setAssessdate(rs.getString(11));
					xx.setGteegoodsamt(rs.getString(12));
					xx.setFirstrightamt(rs.getString(13));
					xx.setGteegoodsstataus(rs.getString(14));
					xx.setMortgagepgerate(rs.getString(15));*/
					exportString.append(rs.getString(2)+"|");
					exportString.append(rs.getString(3)+"|");
					exportString.append(rs.getString(4)+"|");
					exportString.append(rs.getString(5)+"|");
					exportString.append(rs.getString(6)+"|");
					exportString.append(rs.getString(7)+"|");
					exportString.append(rs.getString(8)+"|");
					exportString.append(rs.getString(9)+"|");
					exportString.append(rs.getString(10)+"|");
					exportString.append(rs.getString(11)+"|");
					exportString.append(rs.getString(12)+"|");
					exportString.append(rs.getString(13)+"|");
					exportString.append(rs.getString(14)+"|");
					exportString.append(rs.getString(15)+"|");
					exportString.append("\r\n");
				}
				bw.write(exportString.toString());
	            if(bw != null) {
					bw.flush();
					bw.close();
				}
				
				if(fos != null) {
					fos.flush();
					fos.close();
				}
				result.append("1");
			} catch (Exception e) {
				result.append("0");
				e.printStackTrace();
			}finally {
				if(rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if(ps != null) {
					try {
						ps.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if(con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}else if(TableCodeEnum.JRJGFZJCXX.equals(code)) {
			String query = "select id,finorgname,finorgcode,finorgnum,inorgnum,orglevel,highlevelorgname,highlevelfinorgcode,highlevelinorgnum,regarea,regareacode,workarea,workareacode,setupdate,mngmestus from xjrjgfz where datastatus='3'";
			try {
				File infofile = new File(messagepath+"\\report\\"+fileName);
				if(!infofile.exists()) {
					infofile.getParentFile().mkdirs();
				}
				infofile.createNewFile();
				fos = new FileOutputStream(infofile,true);
				bw = new BufferedWriter(new OutputStreamWriter(fos,"GB18030"));
			    con = JDBCUtil.getConnection("/intg/jdbc.properties", "mysql.driverClassName", "mysql.url", "mysql.username", "mysql.password");
		    	ps = con.prepareStatement(query);
				rs = ps.executeQuery();
				while (rs.next()) {
					/*Xjrjgfz xx = new Xjrjgfz();
					xx.setId(rs.getString(1));
					xx.setFinorgname(rs.getString(2));
					xx.setFinorgcode(rs.getString(3));
					xx.setFinorgnum(rs.getString(4));
					xx.setInorgnum(rs.getString(5));
					xx.setOrglevel(rs.getString(6));
					xx.setHighlevelorgname(rs.getString(7));
					xx.setHighlevelfinorgcode(rs.getString(8));
					xx.setHighlevelinorgnum(rs.getString(9));
					xx.setRegarea(rs.getString(10));
					xx.setRegareacode(rs.getString(11));
					xx.setWorkarea(rs.getString(12));
					xx.setWorkareacode(rs.getString(13));
					xx.setSetupdate(rs.getString(14));
					xx.setMngmestus(rs.getString(15));*/
					exportString.append(rs.getString(2)+"|");
					exportString.append(rs.getString(3)+"|");
					exportString.append(rs.getString(4)+"|");
					exportString.append(rs.getString(5)+"|");
					exportString.append(rs.getString(6)+"|");
					exportString.append(rs.getString(7)+"|");
					exportString.append(rs.getString(8)+"|");
					exportString.append(rs.getString(9)+"|");
					exportString.append(rs.getString(10)+"|");
					exportString.append(rs.getString(11)+"|");
					exportString.append(rs.getString(12)+"|");
					exportString.append(rs.getString(13)+"|");
					exportString.append(rs.getString(14)+"|");
					exportString.append(rs.getString(15)+"|");
					exportString.append("\r\n");
				}
				bw.write(exportString.toString());
	            if(bw != null) {
					bw.flush();
					bw.close();
				}
				
				if(fos != null) {
					fos.flush();
					fos.close();
				}
				result.append("1");
			} catch (Exception e) {
				result.append("0");
				e.printStackTrace();
			}finally {
				if(rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if(ps != null) {
					try {
						ps.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if(con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}else if(TableCodeEnum.JRJGFRJCXX.equals(code)) {
			String queryzc = "select id,gxck,gxdk,zczj,fzzj,syzqyhj,sxzc,fxzc,ldxzc,ldxfz,zcldk,gzldk,cjldk,jyldk,ssldk,yqdk,yqninetytysdk,dkjzzb from xjrjgfr_assets where datastatus='3'";
			String[] stringA = {"id","gxck","gxdk","zczj","fzzj","syzqyhj","sxzc","fxzc","ldxzc","ldxfz","zcldk","gzldk","cjldk","jyldk","ssldk","yqdk","yqninetytysdk","dkjzzb"};
			String queryjc = "select id,finorgname,finorgcode,finorgnum,regaddress,regarea,regamt,setupdate,phone,orgmanagestatus,orgstoreconomy,industrycetegory,invermodel,"
					+ "actctrlidtype,actctrlname,actctrlcode,personnum,onestockcode,twostockcode,threestockcode,fourstockcode,fivestockcode,sixstockcode,sevenstockcode,"
					+ "eightstockcode,ninestockcode,tenstockcode,onestockprop,twostockprop,threestockprop,fourstockprop,fivestockprop,sixstockprop,sevenstockprop,eightstockprop,"
					+ "ninestockprop,tenstockprop from xjrjgfr_baseinfo where datastatus='3'";
			String[] stringB = {"id","finorgname","finorgcode","finorgnum","regaddress","regarea","regamt","setupdate","phone","orgmanagestatus","orgstoreconomy","industrycetegory","invermodel,"
					+ "actctrlidtype","actctrlname","actctrlcode","personnum","onestockcode","twostockcode","threestockcode","fourstockcode","fivestockcode","sixstockcode","sevenstockcode,"
					+ "eightstockcode","ninestockcode","tenstockcode","onestockprop","twostockprop","threestockprop","fourstockprop","fivestockprop","sixstockprop","sevenstockprop","eightstockprop,"
					+ "ninestockprop","tenstockprop"};
			String querylr = "select id,yysr,lxjsr,lxsr,jrjgwllxsr,xtnwllxsr,gxdklxsr,zqlxsr,qtlxsr,lxzc,jrjgwllxzc,xtnwllxzc,gxcklxzc,zqlxzc,qtlxzc,sxfjyjjsr,sxfjyjsr,"
					+ "jxfjyjzc,zlsy,tzsy,zqtzsy,gqtzsy,qttzsy,gyjzbdsy,hdjsy,qtywsr,yyzc,ywjglf,zggz,flf,zfgjjhzfbt,sjjfj,zcjzss,qtywzc,yylr,yywsr,yywzc,lrze,"
					+ "sds,jlr,ndsytz,lclr,wfplr,ynzzs,hxyjzbje,yjzbje,zbje,ygzbjfxjqzchj from xjrjgfr_profit where datastatus='3'";
			String[] stringC = {"id","yysr","lxjsr","lxsr","jrjgwllxsr","xtnwllxsr","gxdklxsr","zqlxsr","qtlxsr","lxzc","jrjgwllxzc","xtnwllxzc","gxcklxzc","zqlxzc","qtlxzc","sxfjyjjsr","sxfjyjsr,"
					+ "jxfjyjzc","zlsy","tzsy","zqtzsy","gqtzsy","qttzsy","gyjzbdsy","hdjsy","qtywsr","yyzc","ywjglf","zggz","flf","zfgjjhzfbt","sjjfj","zcjzss","qtywzc","yylr","yywsr","yywzc","lrze,"
					+ "sds","jlr","ndsytz","lclr","wfplr","ynzzs","hxyjzbje","yjzbje","zbje","ygzbjfxjqzchj"};
			
			try {
				File infofile = new File(messagepath+"\\report\\"+fileName);
				if(!infofile.exists()) {
					infofile.getParentFile().mkdirs();
					infofile.createNewFile();
				}
				//infofile.createNewFile();
				//fos = new FileOutputStream(infofile,true);
				//bw = new BufferedWriter(new OutputStreamWriter(fos,"GB18030"));
				
			    Integer totalA = JDBCUtil.findTotalElements(queryzc);
			    List<Object[]> listA = JDBCUtil.selectQuery(queryzc, "id", 0, totalA, stringA);
			    List<List<Object>> objectA = new LinkedList<>();
			    for(Object[] obj : listA) {
			    	List<Object> dataA = new LinkedList<>();
			    	dataA.add(obj[1]);
					dataA.add(obj[2]);
					dataA.add(obj[3]);
					dataA.add(obj[4]);
					dataA.add(obj[5]);
					dataA.add(obj[6]);
					dataA.add(obj[7]);
					dataA.add(obj[8]);
					dataA.add(obj[9]);
					dataA.add(obj[10]);
					dataA.add(obj[11]);
					dataA.add(obj[12]);
					dataA.add(obj[13]);
					dataA.add(obj[14]);
					dataA.add(obj[15]);
					dataA.add(obj[16]);
					dataA.add(obj[17]);
					dataA.add(obj[18]);
					objectA.add(dataA);
			    }
			    List<String> columnNameA = new LinkedList<>();
			    columnNameA.add("各项存款");
			    columnNameA.add("各项贷款");
			    columnNameA.add("资产总计");
			    columnNameA.add("负债总计");
			    columnNameA.add("所有者权益合计");
			    columnNameA.add("生息资产");
			    columnNameA.add("付息负债");
			    columnNameA.add("流动性资产");
			    columnNameA.add("流动性负债");
			    columnNameA.add("正常类贷款");
			    columnNameA.add("关注类贷款");
			    columnNameA.add("次级类贷款");
			    columnNameA.add("可疑类贷款");
			    columnNameA.add("损失类贷款");
			    columnNameA.add("逾期贷款");
			    columnNameA.add("逾期90天以上贷款");
			    columnNameA.add("贷款减值准备");
			    
				//Excel2007Utils.writeExcelToFile(infofile, columnNameA, objectA, "资产负债及风险统计表");
				
				Integer totalB = JDBCUtil.findTotalElements(queryjc);
			    List<Object[]> listB = JDBCUtil.selectQuery(queryjc, "id", 0, totalB, stringB);
			    List<List<Object>> objectB = new LinkedList<>();
			    for(Object[] obj : listB) {
			    	List<Object> dataB = new LinkedList<>();
			    	dataB.add(obj[1]);
			    	dataB.add(obj[2]);
			    	dataB.add(obj[3]);
			    	dataB.add(obj[4]);
			    	dataB.add(obj[5]);
			    	dataB.add(obj[6]);
			    	dataB.add(obj[7]);
			    	dataB.add(obj[8]);
			    	dataB.add(obj[9]);
			    	dataB.add(obj[10]);
			    	dataB.add(obj[11]);
			    	dataB.add(obj[12]);
			    	dataB.add(obj[13]);
			    	dataB.add(obj[14]);
			    	dataB.add(obj[15]);
			    	dataB.add(obj[16]);
			    	dataB.add(obj[17]);
			    	dataB.add(obj[18]);
			    	dataB.add(obj[19]);
			    	dataB.add(obj[20]);
			    	dataB.add(obj[21]);
			    	dataB.add(obj[22]);
			    	dataB.add(obj[23]);
			    	dataB.add(obj[24]);
			    	dataB.add(obj[25]);
			    	dataB.add(obj[26]);
			    	dataB.add(obj[27]);
			    	dataB.add(obj[28]);
			    	dataB.add(obj[29]);
			    	dataB.add(obj[30]);
			    	dataB.add(obj[31]);
			    	dataB.add(obj[32]);
			    	dataB.add(obj[33]);
			    	dataB.add(obj[34]);
			    	dataB.add(obj[35]);
			    	dataB.add(obj[36]);
					objectB.add(dataB);
			    }
			    List<String> columnNameB = new LinkedList<>();
			    columnNameB.add("金融机构名称");
			    columnNameB.add("金融机构代码");
			    columnNameB.add("金融机构编码");
			    columnNameB.add("注册地址");
			    columnNameB.add("注册地行政区划代码");
			    columnNameB.add("注册资本");
			    columnNameB.add("成立日期");
			    columnNameB.add("联系电话");
			    columnNameB.add("机构经营状态");
			    columnNameB.add("机构出资人经济成分");
			    columnNameB.add("行业分类");
			    columnNameB.add("企业规模");
			    columnNameB.add("实际控制人身份类别");
			    columnNameB.add("实际控制人名称");
			    columnNameB.add("实际控制人代码");
			    columnNameB.add("员工数");
			    columnNameB.add("第一大股东代码");
			    columnNameB.add("第二大股东代码");
			    columnNameB.add("第三大股东代码");
			    columnNameB.add("第四大股东代码");
			    columnNameB.add("第五大股东代码");
			    columnNameB.add("第六大股东代码");
			    columnNameB.add("第七大股东代码");
			    columnNameB.add("第八大股东代码");
			    columnNameB.add("第九大股东代码");
			    columnNameB.add("第十大股东代码");
			    columnNameB.add("第一大股东持股比例");
			    columnNameB.add("第二大股东持股比例");
			    columnNameB.add("第三大股东持股比例");
			    columnNameB.add("第四大股东持股比例");
			    columnNameB.add("第五大股东持股比例");
			    columnNameB.add("第六大股东持股比例");
			    columnNameB.add("第七大股东持股比例");
			    columnNameB.add("第八大股东持股比例");
			    columnNameB.add("第九大股东持股比例");
			    columnNameB.add("第十大股东持股比例");
				Excel2007Utils.writeExcelToFile(infofile, columnNameB, objectB, "基本情况统计表");
				
				Integer totalC = JDBCUtil.findTotalElements(querylr);
			    List<Object[]> listC = JDBCUtil.selectQuery(querylr, "id", 0, totalC, stringC);
			    List<List<Object>> objectC = new LinkedList<>();
			    for(Object[] obj : listC) {
			    	List<Object> dataC = new LinkedList<>();
			    	dataC.add(obj[1]);
			    	dataC.add(obj[2]);
			    	dataC.add(obj[3]);
			    	dataC.add(obj[4]);
			    	dataC.add(obj[5]);
			    	dataC.add(obj[6]);
			    	dataC.add(obj[7]);
			    	dataC.add(obj[8]);
			    	dataC.add(obj[9]);
			    	dataC.add(obj[10]);
			    	dataC.add(obj[11]);
			    	dataC.add(obj[12]);
			    	dataC.add(obj[13]);
			    	dataC.add(obj[14]);
			    	dataC.add(obj[15]);
			    	dataC.add(obj[16]);
			    	dataC.add(obj[17]);
			    	dataC.add(obj[18]);
			    	dataC.add(obj[19]);
			    	dataC.add(obj[20]);
			    	dataC.add(obj[21]);
			    	dataC.add(obj[22]);
			    	dataC.add(obj[23]);
			    	dataC.add(obj[24]);
			    	dataC.add(obj[25]);
			    	dataC.add(obj[26]);
			    	dataC.add(obj[27]);
			    	dataC.add(obj[28]);
			    	dataC.add(obj[29]);
			    	dataC.add(obj[30]);
			    	dataC.add(obj[31]);
			    	dataC.add(obj[32]);
			    	dataC.add(obj[33]);
			    	dataC.add(obj[34]);
			    	dataC.add(obj[35]);
			    	dataC.add(obj[36]);
			    	dataC.add(obj[37]);
			    	dataC.add(obj[38]);
			    	dataC.add(obj[39]);
			    	dataC.add(obj[40]);
			    	dataC.add(obj[41]);
			    	dataC.add(obj[42]);
			    	dataC.add(obj[43]);
			    	dataC.add(obj[44]);
			    	dataC.add(obj[45]);
			    	dataC.add(obj[46]);
			    	dataC.add(obj[47]);
					objectC.add(dataC);
			    }
			    List<String> columnNameC = new LinkedList<>();
			    columnNameC.add("营业收入");
			    columnNameC.add("利息净收入");
			    columnNameC.add("利息收入");
			    columnNameC.add("金融机构往来利息收入");
			    columnNameC.add("其中：系统内往来利息收入");
			    columnNameC.add("各项贷款利息收入");
			    columnNameC.add("债券利息收入");
			    columnNameC.add("其他利息收入");
			    columnNameC.add("利息支出");
			    columnNameC.add("金融机构往来利息支出");
			    columnNameC.add("其中：系统内往来利息支出");
			    columnNameC.add("各项存款利息支出");
			    columnNameC.add("债券利息支出");
			    columnNameC.add("其他利息支出");
			    columnNameC.add("手续费及佣金净收入");
			    columnNameC.add("手续费及佣金收入");
			    columnNameC.add("手续费及佣金支出");
			    columnNameC.add("租赁收益");
			    columnNameC.add("投资收益");
			    columnNameC.add("债券投资收益");
			    columnNameC.add("股权投资收益");
			    columnNameC.add("其他投资收益");
			    columnNameC.add("公允价值变动收益");
			    columnNameC.add("汇兑净收益");
			    columnNameC.add("其他业务收入");
			    columnNameC.add("营业支出");
			    columnNameC.add("业务及管理费");
			    columnNameC.add("其中:职工工资");
			    columnNameC.add("福利费");
			    columnNameC.add("住房公积金和住房补贴");
			    columnNameC.add("税金及附加");
			    columnNameC.add("资产减值损失");
			    columnNameC.add("其他业务支出");
			    columnNameC.add("营业利润");
			    columnNameC.add("营业外收入（加）");
			    columnNameC.add("营业外支出（减）");
			    columnNameC.add("利润总额");
			    columnNameC.add("所得税（减）");
			    columnNameC.add("净利润");
			    columnNameC.add("年度损益调整（加）");
			    columnNameC.add("留存利润");
			    columnNameC.add("未分配利润");
			    columnNameC.add("应纳增值税");
			    columnNameC.add("核心一级资本净额");
			    columnNameC.add("一级资本净额");
			    columnNameC.add("资本净额");
			    columnNameC.add("应用资本底线及校准后的风险加权资产合计");
				Excel2007Utils.writeExcelToFile(infofile, columnNameC, objectC, "利润及资本统计表");
				result.append("1");
			} catch (Exception e) {
				result.append("0");
				e.printStackTrace();
			}finally {
				if(rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if(ps != null) {
					try {
						ps.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if(con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		if(result.length() == 0) {
			result.append("1");
		}
		return result.toString();
	}
	
	
	/**
	 * 生成单位贷款担保物信息的报文
	 * @param messagepath 生成报文存放路径
	 * @param fileName 生成的报文全名称
	 * @param dataList 报文数据
	 * @param result 生成结果
	 */
	public static void shengChengBaoWenForDWDKDBWXX(String messagepath,String fileName,List<Xdkdbwx> dataList,StringBuffer result) {
		StringBuffer exportString = new StringBuffer();  //文件
    	FileOutputStream fos=null;
		BufferedWriter bw = null;
		try {
			File infofile = new File(messagepath+"\\report\\"+fileName);
			if(!infofile.exists()) {
				infofile.getParentFile().mkdirs();
			}
			infofile.createNewFile();
			fos = new FileOutputStream(infofile,true);
			bw = new BufferedWriter(new OutputStreamWriter(fos,"GB18030"));
			for(int i=0;i<dataList.size();i++) {
				Xdkdbwx data = (Xdkdbwx)dataList.get(i);
				exportString.append(data.getGteecontractcode()+"|");
				exportString.append(data.getLoancontractcode()+"|");
				exportString.append(data.getGteegoodscode()+"|");
				exportString.append(data.getGteegoodscategory()+"|");
				exportString.append(data.getWarrantcode()+"|");
				exportString.append(data.getIsfirst()+"|");
				exportString.append(data.getAssessmode()+"|");
				exportString.append(data.getAssessmethod()+"|");
				exportString.append(data.getAssessvalue()+"|");
				exportString.append(data.getAssessdate()+"|");
				exportString.append(data.getGteegoodsamt()+"|");
				exportString.append(data.getFirstrightamt()+"|");
				exportString.append(data.getGteegoodsstataus()+"|");
				exportString.append(data.getMortgagepgerate()+"|");
				exportString.append("\r\n");
			}
			bw.write(exportString.toString());
            if(bw != null) {
				bw.flush();
				bw.close();
			}
			
			if(fos != null) {
				fos.flush();
				fos.close();
			}
			result.append("1");
		} catch (IOException e) {
			result.append("0");
			e.printStackTrace();
		}finally {
			 if(bw != null) {
					try {
						bw.flush();
						bw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				if(fos != null) {
					try {
						fos.flush();
						fos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
		}
	}
	
	/**
	 * 生成金融机构（机构分支）基础信息的报文
	 * @param messagepath 生成报文存放路径
	 * @param fileName 生成的报文全名称
	 * @param dataList 报文数据
	 * @param result 生成结果
	 */
	public static void shengChengBaoWenForJRJGFZJCXX(String messagepath,String fileName,List<Xjrjgfz> dataList,StringBuffer result) {
		StringBuffer exportString = new StringBuffer();  //文件
    	FileOutputStream fos=null;
		BufferedWriter bw = null;
		try {
			File infofile = new File(messagepath+"\\report\\"+fileName);
			if(!infofile.exists()) {
				infofile.getParentFile().mkdirs();
			}
			infofile.createNewFile();
			fos = new FileOutputStream(infofile,true);
			bw = new BufferedWriter(new OutputStreamWriter(fos,"GB18030"));
			for(int i=0;i<dataList.size();i++) {
				Xjrjgfz data = (Xjrjgfz)dataList.get(i);
				exportString.append(data.getFinorgname()+"|");
				exportString.append(data.getFinorgcode()+"|");
				exportString.append(data.getFinorgnum()+"|");
				exportString.append(data.getInorgnum()+"|");
				exportString.append(data.getOrglevel()+"|");
				exportString.append(data.getHighlevelorgname()+"|");
				exportString.append(data.getHighlevelfinorgcode()+"|");
				exportString.append(data.getHighlevelinorgnum()+"|");
				exportString.append(data.getRegarea()+"|");
				exportString.append(data.getRegareacode()+"|");
				exportString.append(data.getWorkarea()+"|");
				exportString.append(data.getWorkareacode()+"|");
				exportString.append(data.getSetupdate()+"|");
				exportString.append(data.getMngmestus()+"|");
				exportString.append("\r\n");
			}
			bw.write(exportString.toString());
            if(bw != null) {
				bw.flush();
				bw.close();
			}
			
			if(fos != null) {
				fos.flush();
				fos.close();
			}
			result.append("1");
		} catch (IOException e) {
			result.append("0");
			e.printStackTrace();
		}finally {
			 if(bw != null) {
					try {
						bw.flush();
						bw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				if(fos != null) {
					try {
						fos.flush();
						fos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
		}
	}
	

	public static String shengChengBaoWenNone(String messagepath,String fileName,List<?> dataList,TableCodeEnum code) {
		StringBuffer result = new StringBuffer();
		StringBuffer exportString = new StringBuffer();  //文件
    	FileOutputStream fos=null;
		BufferedWriter bw = null;
		if(dataList!=null&&dataList.size()>0) {
			if(TableCodeEnum.DWDKDBWXX.equals(code)) {
				try {
					File infofile = new File(messagepath+"\\report\\"+fileName);
					if(!infofile.exists()) {
						infofile.getParentFile().mkdirs();
					}
					infofile.createNewFile();
					fos = new FileOutputStream(infofile,true);
					bw = new BufferedWriter(new OutputStreamWriter(fos,"GB18030"));
					for(int i=0;i<dataList.size();i++) {
						Xdkdbwx data = (Xdkdbwx)dataList.get(i);
						exportString.append(data.getGteecontractcode()+"|");
						exportString.append(data.getLoancontractcode()+"|");
						exportString.append(data.getGteegoodscode()+"|");
						exportString.append(data.getGteegoodscategory()+"|");
						exportString.append(data.getWarrantcode()+"|");
						exportString.append(data.getIsfirst()+"|");
						exportString.append(data.getAssessmode()+"|");
						exportString.append(data.getAssessmethod()+"|");
						exportString.append(data.getAssessvalue()+"|");
						exportString.append(data.getAssessdate()+"|");
						exportString.append(data.getGteegoodsamt()+"|");
						exportString.append(data.getFirstrightamt()+"|");
						exportString.append(data.getGteegoodsstataus()+"|");
						exportString.append(data.getMortgagepgerate()+"|");
						exportString.append("\r\n");
					}
					bw.write(exportString.toString());
		            if(bw != null) {
						bw.flush();
						bw.close();
					}
					
					if(fos != null) {
						fos.flush();
						fos.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					 if(bw != null) {
							try {
								bw.flush();
								bw.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						
						if(fos != null) {
							try {
								fos.flush();
								fos.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
				}
			}else if(TableCodeEnum.JRJGFZJCXX.equals(code)){
				try {
					File infofile = new File(messagepath+"\\report\\"+fileName);
					if(!infofile.exists()) {
						infofile.getParentFile().mkdirs();
					}
					infofile.createNewFile();
					fos = new FileOutputStream(infofile,true);
					bw = new BufferedWriter(new OutputStreamWriter(fos,"GB18030"));
					for(int i=0;i<dataList.size();i++) {
						Xjrjgfz data = (Xjrjgfz)dataList.get(i);
						exportString.append(data.getFinorgname()+"|");
						exportString.append(data.getFinorgcode()+"|");
						exportString.append(data.getFinorgnum()+"|");
						exportString.append(data.getInorgnum()+"|");
						exportString.append(data.getOrglevel()+"|");
						exportString.append(data.getHighlevelorgname()+"|");
						exportString.append(data.getHighlevelfinorgcode()+"|");
						exportString.append(data.getHighlevelinorgnum()+"|");
						exportString.append(data.getRegarea()+"|");
						exportString.append(data.getRegareacode()+"|");
						exportString.append(data.getWorkarea()+"|");
						exportString.append(data.getWorkareacode()+"|");
						exportString.append(data.getSetupdate()+"|");
						exportString.append(data.getMngmestus()+"|");
						exportString.append("\r\n");
					}
					bw.write(exportString.toString());
		            if(bw != null) {
						bw.flush();
						bw.close();
					}
					
					if(fos != null) {
						fos.flush();
						fos.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					 if(bw != null) {
							try {
								bw.flush();
								bw.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						
						if(fos != null) {
							try {
								fos.flush();
								fos.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
				}
			}else if(TableCodeEnum.JRJGFZJCXX.equals(code)){
				//guiZeJiaoYan(dataList,result);
			}
		}else {
			result.append("要生成报文的数据集合为空");
		}
		
		if(result.length() == 0) {
			result.append("1");
		}
		return result.toString();
	}
}
