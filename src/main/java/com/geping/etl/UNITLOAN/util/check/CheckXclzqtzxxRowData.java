package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.dataDictionary.ZqFieldUtils;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.report.Xclzqtzxx;
import com.geping.etl.UNITLOAN.util.CheckDataUtils;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;


/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.util.check
 * @USER: tangshuai
 * @DATE: 2021/4/20
 * @TIME: 16:50
 * @描述:
 */
@Service
public class CheckXclzqtzxxRowData implements CheckRowData {

    @Autowired
    private CustomSqlUtil customSqlUtil;

    public void checkRow(String header, CheckParamContext checkParamContext, Object object) {
        //存量债券投资信息
        Xclzqtzxx xclzqtzxx = (Xclzqtzxx) object;
        StringBuffer tempMsg = new StringBuffer("");
        Boolean isError = false;
        List<String> checknumList = checkParamContext.getCheckNums();
        //发行人经济成分不为空
        if (checkXclzqtzxxRowFxrjjcf(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //债券余额折人民币
        if (checkXclzqtzxxRowZqyezrmb(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //发行人国民经济部门为C开头且不是C99的非金融企业部门，则发行人企业规模应该在CS01至CS04范围内
        if (checkXclzqtzxxRowGmjjbm(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //境内发行人证件代码字符长度应该为18位
        if (checkXclzqtzxxRowFxrzjdm(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }

        //发行人国民经济部门
        if (checkXclzqtzxxRowFxrdqdm(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //当票面利率不为空时，应大于等于0且小于等于30
        if (checkXclzqtzxxRowPmll(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //债券债务登记日期校验
        if (checkXclzqtzxxRowZqzwdjr(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //起息日校验
        if (checkXclzqtzxxRowQxr(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //内部机构号
        if (checkXclzqtzxxRowNbjgh(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //非空判断
        if (checkXclzqtzxxRowFkpd(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //长度判断
        if (checkXclzqtzxxRowCdpd(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //特殊字符判断
        if (checkXclzqtzxxRowTszf(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //发行人企业规模
        if (checkXclzqtzxxRowFxrqygm(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //债券总托管机构
        if (checkXclzqtzxxRowZqztgjg(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //债券品种
        if (checkXclzqtzxxRowZqpz(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //债券信用级别
        if (checkXclzqtzxxRowZqxyjb(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //币种
        if (checkXclzqtzxxRowBz(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //发行人行业代码
        if (checkXclzqtzxxRowFxrhydm(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }
        //数据日期校验
        if (checkXclzqtzxxRowSjrq(checknumList, xclzqtzxx, tempMsg)) {
            isError = true;
        }


        CheckHelper checkHelper = (CheckHelper) SpringContextUtil.getBean(CheckHelper.class);
        //单条实体交验完,处理结果
        checkHelper.handleRowCheckResult(isError, xclzqtzxx.getId(), String.format(header, xclzqtzxx.getFinanceorgcode(), xclzqtzxx.getZqdm()), tempMsg.toString(), checkParamContext);

    }

    private boolean checkXclzqtzxxRowSjrq(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String sjrq = xclzqtzxx.getSjrq();//数据日期
        if (StringUtils.isNotBlank(sjrq)) {
            if (sjrq.length() == 10) {
                boolean b1 = CheckUtil.checkDate(sjrq, "yyyy-MM-dd");
                if (b1) {
                    if (sjrq.compareTo("1800-01-01") < 0 || sjrq.compareTo("2100-12-31") > 0) {
                        tempMsg.append("数据日期需晚于1800-01-01早于2100-12-31" + SysConstants.spaceStr);
                        result = true;
                    }
                } else {
                    tempMsg.append("数据日期不符合yyyy-MM-dd格式" + SysConstants.spaceStr);
                    result = true;
                }
            } else {
                tempMsg.append("数据日期长度不等于10" + SysConstants.spaceStr);
                result = true;
            }
        } else {
            tempMsg.append("数据日期不能为空" + SysConstants.spaceStr);
            result = true;
        }
        return result;
    }

    private boolean checkXclzqtzxxRowFxrhydm(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String fxrhy = xclzqtzxx.getFxrhy();
        //List baseCode = customSqlUtil.getBaseCode(BaseAindustry.class);
        List baseCode = ZqFieldUtils.fxrhyList;
        if (checknumList.contains("JS0028") && StringUtils.isNotBlank(fxrhy)) {
            if (!baseCode.contains(fxrhy) && !"2".equals(fxrhy)) {
                result = true;
                tempMsg.append("存量债券投资信息中的发行人行业应该在《国民经济行业分类》（GB/T 4754）最新标准门类范围或为'2'" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowBz(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String bz = xclzqtzxx.getBz();
        List baseCode = customSqlUtil.getBaseCode(BaseCurrency.class);
        if (checknumList.contains("JS0026") && StringUtils.isNotBlank(bz)) {
            if (!baseCode.contains(bz)) {
                result = true;
                tempMsg.append("存量债券投资信息中的币种需在《表示货币和资金的代码》（GB/T 12406）中的三位字母范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowZqxyjb(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String zqxyjg = xclzqtzxx.getZqxyjg();//债券信用级别
        List<String> list = ZqFieldUtils.zqxyjbList();
        if (checknumList.contains("JS0025") && StringUtils.isNotBlank(zqxyjg)) {
            if (!list.contains(zqxyjg)) {
                result = true;
                tempMsg.append("存量债券投资信息表中的债券信用级别需在符合要求的值域范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowZqpz(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String zqpz = xclzqtzxx.getZqpz();//债券总托管机构
        if (checknumList.contains("JS0024") && StringUtils.isNotBlank(zqpz)) {
            try {
                if (!ZqFieldUtils.zqpzList.contains(zqpz) && !zqpz.substring(0, 5).equals("TB99-")) {
                    result = true;
                    tempMsg.append("存量债券投资信息表中的债券品种需在符合要求的值域范围内 或者为'TB99-’开头加债券中文名称" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowZqztgjg(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String zqztgjg = xclzqtzxx.getZqztgjg();//债券总托管机构
        if (checknumList.contains("JS0023") && StringUtils.isNotBlank(zqztgjg)) {
            if (!(zqztgjg.equals("300") || zqztgjg.equals("400") || zqztgjg.equals("900"))) {
                result = true;
                tempMsg.append("存量债券投资信息表中的债券总托管机构需在符合要求的值域范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowFxrqygm(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String fxrqygm = xclzqtzxx.getFxrqygm();//发行人企业规模
        String fxrhy = xclzqtzxx.getFxrhy();//发行人行业

        if (checknumList.contains("JS0029") && StringUtils.isNotBlank(fxrqygm) && !CheckDataUtils.comma(fxrqygm)) {
            if (!ZqFieldUtils.fxrqygmList.contains(fxrqygm)) {
                result = true;
                tempMsg.append("若发行人企业规模不为空，需在符合要求的值域范围内" + SysConstants.spaceStr);
            }
        }

        if (checknumList.contains("JS1356") && StringUtils.isNotBlank(fxrhy)) {
            if (!(fxrhy.equals("100") || fxrhy.equals("200")) && StringUtils.isBlank(fxrqygm)) {
                result = true;
                tempMsg.append("当发行人不为个人、境外非居民时，发行人企业规模不能为空" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowTszf(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xclzqtzxx.getFinanceorgcode();//金融机构到代码
        String zqdm = xclzqtzxx.getZqdm();//债券代码
        String fxrzjdm = xclzqtzxx.getFxrzjdm();//发行人证件代码
        String pmll = xclzqtzxx.getPmll();//票面利率
        String financeorginnum = xclzqtzxx.getFinanceorginnum();//内部机构号
        if (checknumList.contains("JS0701") && StringUtils.isNotBlank(financeorgcode)) {
            if (CheckUtil.checkStr(financeorgcode)) {
                result = true;
                tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0702") && StringUtils.isNotBlank(zqdm)) {
            if (CheckUtil.checkStr(zqdm)) {
                result = true;
                tempMsg.append("债券代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。 " + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0703") && StringUtils.isNotBlank(fxrzjdm)) {
            if (CheckUtil.checkStr(fxrzjdm)) {
                result = true;
                tempMsg.append("发行人证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0704") && StringUtils.isNotBlank(pmll)) {
            if (CheckUtil.checkPerSign(pmll)) {
                result = true;
                tempMsg.append("当票面利率不为空时，票面利率不能包含‰或%" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0705") && StringUtils.isNotBlank(financeorginnum)) {
            if (CheckUtil.checkStr(financeorginnum)) {
                result = true;
                tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowCdpd(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xclzqtzxx.getFinanceorgcode();//金融机构到代码
        String financeorginnum = xclzqtzxx.getFinanceorginnum();//内部机构号
        String zqdm = xclzqtzxx.getZqdm();//债券代码
        String fxrzjdm = xclzqtzxx.getFxrzjdm();//发行人证件代码
        String zqpz = xclzqtzxx.getZqpz();//债券品种
        String fxrdqdm = xclzqtzxx.getFxrdqdm();//发行人地区代码
        String fxrhy = xclzqtzxx.getFxrhy();//发行人行业
        String fxrqygm = xclzqtzxx.getFxrqygm();//发行人企业规模
        String fxrjjcf = xclzqtzxx.getFxrjjcf();//发行人经济成分
        String fxrgmjjbm = xclzqtzxx.getFxrgmjjbm();//发行人国民经济部门

        if (checknumList.contains("JS2931") && StringUtils.isNotBlank(fxrgmjjbm) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((zqdm.equals("SMECN1") || zqdm.equals("SMECN2") || zqdm.substring(0, 4).equals("TB99")) && fxrgmjjbm.length() > 100) {
                    result = true;
                    tempMsg.append("债券品种为'SMECN1'，'SMECN2'或者'TB99'开头时，发行人国民经济部门长度不能超过100" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2932") && StringUtils.isNotBlank(fxrgmjjbm) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((!zqdm.equals("SMECN1") && !zqdm.equals("SMECN2") && !zqdm.substring(0, 4).equals("TB99")) && fxrgmjjbm.length() > 3) {
                    result = true;
                    tempMsg.append("债券品种不为'SMECN1'，'SMECN2'且不是'TB99'开头时，发行人国民经济部门长度不能超过3位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2929") && StringUtils.isNotBlank(fxrjjcf) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((zqdm.equals("SMECN1") || zqdm.equals("SMECN2") || zqdm.substring(0, 4).equals("TB99")) && fxrjjcf.length() > 100) {
                    result = true;
                    tempMsg.append("债券品种为'SMECN1'，'SMECN2'或者'TB99'开头时，发行人经济成分长度不能超过100" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2930") && StringUtils.isNotBlank(fxrjjcf) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((!zqdm.equals("SMECN1") && !zqdm.equals("SMECN2") && !zqdm.substring(0, 4).equals("TB99")) && fxrjjcf.length() > 3) {
                    result = true;
                    tempMsg.append("债券品种不为'SMECN1'，'SMECN2'且不是'TB99'开头时，发行人经济成分长度不能超过3位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2927") && StringUtils.isNotBlank(fxrqygm) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((zqdm.equals("SMECN1") || zqdm.equals("SMECN2") || zqdm.substring(0, 4).equals("TB99")) && fxrqygm.length() > 100) {
                    result = true;
                    tempMsg.append("债券品种为'SMECN1'，'SMECN2'或者'TB99'开头时，发行人企业规模长度不能超过100" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2928") && StringUtils.isNotBlank(fxrqygm) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((!zqdm.equals("SMECN1") && !zqdm.equals("SMECN2") && !zqdm.substring(0, 4).equals("TB99")) && fxrqygm.length() > 4) {
                    result = true;
                    tempMsg.append("债券品种不为'SMECN1'，'SMECN2'且不是'TB99'开头时，发行人企业规模长度不能超过4位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2925") && StringUtils.isNotBlank(fxrhy) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((zqdm.equals("SMECN1") || zqdm.equals("SMECN2") || zqdm.substring(0, 4).equals("TB99")) && fxrhy.length() > 100) {
                    result = true;
                    tempMsg.append("债券品种为'SMECN1'，'SMECN2'或者'TB99'开头时，发行人行业长度不能超过100" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2926") && StringUtils.isNotBlank(fxrhy) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((!zqdm.equals("SMECN1") && !zqdm.equals("SMECN2") && !zqdm.substring(0, 4).equals("TB99")) && fxrhy.length() > 1) {
                    result = true;
                    tempMsg.append("债券品种不为'SMECN1'，'SMECN2'且不是'TB99'开头时，发行人行业长度不能超过1位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2923") && StringUtils.isNotBlank(fxrdqdm) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((zqdm.equals("SMECN1") || zqdm.equals("SMECN2") || zqdm.substring(0, 4).equals("TB99")) && fxrdqdm.length() > 100) {
                    result = true;
                    tempMsg.append("债券品种为'SMECN1'，'SMECN2'或者'TB99'开头时，发行人地区代码长度不能超过100" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2924") && StringUtils.isNotBlank(fxrdqdm) && StringUtils.isNotBlank(zqpz)) {
            try {
                if ((!zqdm.equals("SMECN1") && !zqdm.equals("SMECN2") && !zqdm.substring(0, 4).equals("TB99")) && fxrdqdm.length() > 6) {
                    result = true;
                    tempMsg.append("债券品种不为'SMECN1'，'SMECN2'且不是'TB99'开头时，发行人地区代码长度不能超过6位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2915") && StringUtils.isNotBlank(zqpz)) {
            try {
                if (zqpz.substring(0, 4).equals("TB99") && zqpz.length() > 200) {
                    result = true;
                    tempMsg.append("债券品种为TB99开头时，长度不能超过200" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2916") && StringUtils.isNotBlank(zqpz)) {
            try {
                if (!zqpz.substring(0, 4).equals("TB99") && zqpz.length() > 6) {
                    result = true;
                    tempMsg.append("债券品种不为TB99开头时，长度不能超过6" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS0706") && StringUtils.isNotBlank(financeorgcode)) {
            if (financeorgcode.length() != 18) {
                result = true;
                tempMsg.append("金融机构代码字符长度应该为18位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0707") && StringUtils.isNotBlank(financeorginnum)) {
            if (financeorginnum.length() > 30) {
                result = true;
                tempMsg.append("内部机构号字符长度不能超过30" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0708") && StringUtils.isNotBlank(zqdm)) {
            if (zqdm.length() > 60) {
                result = true;
                tempMsg.append("债券代码字符长度不能超过60" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0709") && StringUtils.isNotBlank(fxrzjdm)) {
            try {
                if (fxrzjdm.length() > 1000 && (zqpz.equals("SMECN1") || zqdm.equals("SMECN2") || zqdm.substring(0, 4).equals("TB99"))) {
                    result = true;
                    tempMsg.append("债券品种为'SMECN1'，'SMECN2'或者 TB99开头时，发行人证件代码字符长度不能超过1000" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2933") && StringUtils.isNotBlank(fxrzjdm)) {
            try {
                if ((!zqpz.equals("SMECN1") && !zqdm.equals("SMECN2") && !zqdm.substring(0, 4).equals("TB99"))&&fxrzjdm.length() > 60) {
                    result = true;
                    tempMsg.append("债券品种不为'SMECN1'，'SMECN2' 且不是'TB99'开头时，发行人证件代码字符长度不能超过60" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowFkpd(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        //金融机构代码不能为空        JS1344
        String financeorgcode = xclzqtzxx.getFinanceorgcode();
        // 债券代码不能为空     JS1345
        String zqdm = xclzqtzxx.getZqdm();
        //债券总托管机构不能为空       JS1346
        String zqztgjg = xclzqtzxx.getZqztgjg();
        //债券品种不能为空      JS1347
        String zqpz = xclzqtzxx.getZqpz();
        //债券信用级别不能为空        JS1348
        String zqxyjg = xclzqtzxx.getZqxyjg();
        //币种不能为空        JS1349
        String bz = xclzqtzxx.getBz();
        //债权债务登记日不能为空       JS1350
        String zqzwdj = xclzqtzxx.getZqzwdj();
        //起息日不能为空       JS1351
        String qxr = xclzqtzxx.getQxr();
        //兑付日期不能为空      JS1352
        String dfrq = xclzqtzxx.getDfrq();
        //发行人证件代码不能为空       JS1353
        String fxrzjdm = xclzqtzxx.getFxrzjdm();
        //发行人地区代码不能为空       JS1354
        String fxrdqdm = xclzqtzxx.getFxrdqdm();
        //发行人行业不能为空     JS1355
        String fxrhy = xclzqtzxx.getFxrhy();
        if (checknumList.contains("JS1344")) {
            if (StringUtils.isBlank(financeorgcode)) {
                result = true;
                tempMsg.append("金融机构代码不能为空" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS1345")) {
            if (StringUtils.isBlank(zqdm)) {
                result = true;
                tempMsg.append("债券代码不能为空" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS1346")) {
            if (StringUtils.isBlank(zqztgjg)) {
                result = true;
                tempMsg.append("债券总托管机构不能为空" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS1347")) {
            if (StringUtils.isBlank(zqpz)) {
                result = true;
                tempMsg.append("债券品种不能为空" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS1348")) {
            if (StringUtils.isBlank(zqxyjg)) {
                result = true;
                tempMsg.append("债券信用级别不能为空" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS1349")) {
            if (StringUtils.isBlank(bz)) {
                result = true;
                tempMsg.append("币种不能为空" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS1350")) {
            if (StringUtils.isBlank(zqzwdj)) {
                result = true;
                tempMsg.append("债权债务登记日不能为空" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0714") && StringUtils.isNotBlank(zqzwdj)) {
            if (!CheckUtil.checkDate(zqzwdj, "yyyy-MM-dd") || zqzwdj.compareTo("1800-01-01") < 0 || zqzwdj.compareTo("2100-12-31") > 0) {
                result = true;
                tempMsg.append("债权债务登记日必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS1351")) {
            if (StringUtils.isBlank(qxr)) {
                result = true;
                tempMsg.append("起息日不能为空" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0715") && StringUtils.isNotBlank(qxr)) {
            if (!CheckUtil.checkDate(qxr, "yyyy-MM-dd") || qxr.compareTo("1800-01-01") < 0 || qxr.compareTo("2100-12-31") > 0) {
                result = true;
                tempMsg.append("起息日必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS1352")) {
            if (StringUtils.isBlank(dfrq)) {
                result = true;
                tempMsg.append("兑付日期不能为空" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0716") && StringUtils.isNotBlank(dfrq)) {
            if (!CheckUtil.checkDate(dfrq, "yyyy-MM-dd") || dfrq.compareTo("1800-01-01") < 0 || dfrq.compareTo("2100-12-31") > 0) {
                result = true;
                tempMsg.append("兑付日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS1353")) {
            if (StringUtils.isBlank(fxrzjdm)) {
                result = true;
                tempMsg.append("发行人证件代码不能为空" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS1354")) {
            if (StringUtils.isBlank(fxrdqdm)) {
                result = true;
                tempMsg.append("发行人地区代码不能为空" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS1355")) {
            if (StringUtils.isBlank(fxrhy)) {
                result = true;
                tempMsg.append("发行人行业不能为空" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowNbjgh(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorginnum = xclzqtzxx.getFinanceorginnum();
        if (checknumList.contains("JS1531")) {
            if (StringUtils.isBlank(financeorginnum)) {
                result = true;
                tempMsg.append("内部机构号不能为空" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowQxr(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String qxr = xclzqtzxx.getQxr();//起息日
        String dfrq = xclzqtzxx.getDfrq();//兑付日期
        String sjrq = xclzqtzxx.getSjrq();//数据日期
        if (checknumList.contains("JS2248")) {
            if (StringUtils.isNotBlank(sjrq) && StringUtils.isNotBlank(qxr)) {
                try {
                    if (qxr.compareTo(sjrq) > 0) {
                        result = true;
                        tempMsg.append("起息日应小于等于数据日期" + SysConstants.spaceStr);
                    }
                } catch (Exception e) {
                    result = true;
                    tempMsg.append("起息日应小于等于数据日期" + SysConstants.spaceStr);
                }
            }
        }
        if (checknumList.contains("JS2249")) {
            if (StringUtils.isNotBlank(qxr) && StringUtils.isNotBlank(dfrq)) {
                try {
                    if (qxr.compareTo(dfrq) > 0) {
                        result = true;
                        tempMsg.append("起息日应小于等于兑付日期" + SysConstants.spaceStr);
                    }
                } catch (Exception e) {
                    result = true;
                    tempMsg.append("起息日应小于等于兑付日期" + SysConstants.spaceStr);
                }
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowZqzwdjr(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String zqzwdj = xclzqtzxx.getZqzwdj();//债权债务登记日
        String sjrq = xclzqtzxx.getSjrq();//数据日期
        String qxr = xclzqtzxx.getQxr();//起息日
//        if (checknumList.contains("JS2251")) {
//            if (StringUtils.isNotBlank(zqzwdj) && StringUtils.isNotBlank(qxr)) {
//                try {
//                    if (zqzwdj.compareTo(qxr) > 0) {
//                        result = true;
//                        tempMsg.append("债权债务登记日应小于等于起息日" + SysConstants.spaceStr);
//                    }
//                } catch (Exception e) {
//                    result = true;
//                    tempMsg.append("债权债务登记日应小于等于起息日" + SysConstants.spaceStr);
//                }
//            }
//        }
        if (checknumList.contains("JS2250")) {
            if (StringUtils.isNotBlank(zqzwdj) && StringUtils.isNotBlank(sjrq)) {
                try {
                    if (zqzwdj.compareTo(sjrq) > 0) {
                        result = true;
                        tempMsg.append("债权债务登记日应小于等于数据日期" + SysConstants.spaceStr);
                    }
                } catch (Exception e) {
                    result = true;
                    tempMsg.append("债权债务登记日应小于等于数据日期" + SysConstants.spaceStr);
                }
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowPmll(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        BigDecimal bigDecimal1 = new BigDecimal(0);
        BigDecimal bigDecimal2 = new BigDecimal(30);
        String pmll = xclzqtzxx.getPmll();
        if (checknumList.contains("JS0710") && StringUtils.isNotBlank(pmll)) {
            if (pmll.length() > 10 || !CheckUtil.checkDecimal(pmll, 5)) {
                tempMsg.append("当票面利率不为空时，票面利率总长度不能超过10位，小数位必须为5位" + SysConstants.spaceStr);
                result = true;
            }
        }

        if (checknumList.contains("JS2252")) {
            if (StringUtils.isNotBlank(pmll)) {
                try {
                    if (new BigDecimal(pmll).compareTo(bigDecimal1) < 0 || new BigDecimal(pmll).compareTo(bigDecimal2) > 0) {
                        result = true;
                        tempMsg.append("当票面利率不为空时，应大于等于0且小于等于30" + SysConstants.spaceStr);
                    }
                } catch (Exception e) {
                    result = true;
                    tempMsg.append("当票面利率不为空时，应大于等于0且小于等于30" + SysConstants.spaceStr);
                }
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowFxrdqdm(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String fxrgmjjbm = xclzqtzxx.getFxrgmjjbm();//发行人国民经济部门
        String fxrdqdm = xclzqtzxx.getFxrdqdm();//发行人地区代码
        String zqpz = xclzqtzxx.getZqpz();
        String fxrhy = xclzqtzxx.getFxrhy();

        if (checknumList.contains("JS2575") && StringUtils.isNotBlank(fxrgmjjbm) && !CheckDataUtils.comma(fxrgmjjbm)) {
            if (!ZqFieldUtils.fxrgmjjbmList.contains(fxrgmjjbm) || fxrgmjjbm.equals("D01") || fxrgmjjbm.equals("E05")) {
                result = true;
                tempMsg.append("存量债券投资信息的发行人国民经济部门需在符合要求的最底层值域范围内且不能为个人" + SysConstants.spaceStr);
            }
        }

        List baseCodeGj = customSqlUtil.getBaseCode(BaseCountry.class);//国家地区代码C0013
        List baseCodeXz = customSqlUtil.getBaseCode(BaseArea.class);//行政区划代码C0009
        if (checknumList.contains("JS0027") && StringUtils.isNotBlank(fxrdqdm) && !CheckDataUtils.comma(fxrdqdm)) {
            if ((!baseCodeGj.contains(fxrdqdm) && !baseCodeXz.contains(fxrdqdm)) && !"000000".equals(fxrdqdm)) {
                result = true;
                tempMsg.append("存量债券投资信息中的发行人地区代码需在符合要求的值域范围内，境内机构为《中华人民共和国行政区划代码》（GB/T 2260）最新标准的县（区）级数字码值域范围内（不包括港澳台），境外机构为000+《世界各国和地区名称代码》（GB/T 2659）的三位国别数字代码，发行人为财政部和中国人民银行的填写000000" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2065") && StringUtils.isNotBlank(fxrhy) && StringUtils.isNotBlank(zqpz)) {
            if (zqpz.equals("MDBB") && !fxrhy.substring(0, 1).equals("T")) {
                result = true;
                tempMsg.append("国际开发机构的债权发行人行业应该为T-国际组织" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2064")) {
            try {
                if (StringUtils.isNotBlank(fxrdqdm) && StringUtils.isNotBlank(fxrhy)) {
                    if (!fxrhy.equals("200") && (fxrdqdm.substring(0, 3).equals("000") && !fxrdqdm.equals("000000"))) {
                        result = true;
                        tempMsg.append("债券发行人行业不是200-境外时，发行人地区代码不是000开头或者应该为000000" + SysConstants.spaceStr);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2063")) {
            try {
                if (StringUtils.isNotBlank(fxrdqdm) && StringUtils.isNotBlank(fxrhy)) {
                    if (fxrhy.equals("200") && (!fxrdqdm.substring(0, 3).equals("000") || fxrdqdm.equals("000000"))) {
                        result = true;
                        tempMsg.append("债券发行人行业为200-境外时，发行人地区代码为000开头且不是000000" + SysConstants.spaceStr);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2062")) {
            if (StringUtils.isNotBlank(fxrdqdm) && StringUtils.isNotBlank(zqpz)) {
                if ((!zqpz.equals("GB01") && !zqpz.equals("GB02") && !zqpz.equals("GB03") && !zqpz.equals("CBN")) && fxrdqdm.equals("000000")) {
                    result = true;
                    tempMsg.append("债券品种不是国债、央票时，发行人地区代码不应该为000000" + SysConstants.spaceStr);

                }
            }
        }
        if (checknumList.contains("JS2061")) {
            if (StringUtils.isNotBlank(fxrdqdm) && StringUtils.isNotBlank(zqpz)) {
                if ((zqpz.equals("GB01") || zqpz.equals("GB02") || zqpz.equals("GB03") || zqpz.equals("CBN")) && !fxrdqdm.equals("000000")) {
                    result = true;
                    tempMsg.append("债券品种为国债、央票时，发行人地区代码应该为000000" + SysConstants.spaceStr);
                }
            }
        }
        if (checknumList.contains("JS2576")) {
            if (StringUtils.isBlank(fxrgmjjbm)) {
                result = true;
                tempMsg.append("发行人国民经济部门不能为空" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2579")) {
            if (StringUtils.isNotBlank(fxrgmjjbm) && StringUtils.isNotBlank(fxrdqdm)) {
                try {
                    if (fxrdqdm.substring(0, 3).equals("000") && !fxrdqdm.equals("000000")) {
                        if (!fxrgmjjbm.substring(0, 1).equals("E")) {
                            result = true;
                            tempMsg.append("发行人地区代码为000开头的且不是000000时，国民经济部门应为E开头的非居民部门" + SysConstants.spaceStr);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (checknumList.contains("JS2580")) {
            try {
                if (StringUtils.isNotBlank(fxrgmjjbm) && StringUtils.isNotBlank(fxrdqdm)) {
                    if (!fxrdqdm.substring(0, 3).equals("000") || fxrdqdm.equals("000000")) {
                        if (fxrgmjjbm.substring(0, 1).equals("E")) {
                            result = true;
                            tempMsg.append("发行人地区代码不是000开头的或为000000时，国民经济部门不应为E开头的非居民部门" + SysConstants.spaceStr);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowFxrzjdm(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String fxrzjdm = xclzqtzxx.getFxrzjdm();
        String fxrdqdm = xclzqtzxx.getFxrdqdm();//发行人地区代码
//        if (checknumList.contains("JS2621")) {
//            if (StringUtils.isNotBlank(fxrzjdm) && StringUtils.isNotBlank(fxrdqdm)) {
//                try {
//                    if (!"18".equals(String.valueOf(fxrzjdm.length()))) {
//                        if (!fxrdqdm.substring(1, 3).equals("000") || fxrdqdm.equals("000000")) {
//                            result = true;
//                            tempMsg.append("境内发行人证件代码字符长度应该为18位" + SysConstants.spaceStr);
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
        return result;
    }

    private boolean checkXclzqtzxxRowGmjjbm(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String fxrgmjjbm = xclzqtzxx.getFxrgmjjbm();//发行国民经济部门
        String fxrqygm = xclzqtzxx.getFxrqygm();//发行人企业规模
        if (checknumList.contains("JS2640")) {
            try {
                if (StringUtils.isNotBlank(fxrgmjjbm) && StringUtils.isNotBlank(fxrqygm) && !CheckDataUtils.comma(fxrqygm)) {
                    String substring1 = fxrgmjjbm.substring(0, 1);
                    String substring = fxrqygm.substring(0, 4);
                    if (substring1.equals("C") && !fxrgmjjbm.equals("C99")) {
                        if (!(substring.equals("CS01") || substring.equals("CS02") || substring.equals("CS03") || substring.equals("CS04"))) {
                            tempMsg.append("发行人国民经济部门为C开头且不是C99的非金融企业部门，则发行人企业规模应该在CS01至CS04范围内" + SysConstants.spaceStr);
                            result = true;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2639")) {
            try {
                if (StringUtils.isNotBlank(fxrgmjjbm) && StringUtils.isNotBlank(fxrqygm) && !CheckDataUtils.comma(fxrgmjjbm)) {
                    String substring1 = fxrgmjjbm.substring(0, 1);
                    String substring = fxrqygm.substring(0, 4);
                    if (substring.equals("CS01") || substring.equals("CS02") || substring.equals("CS03") || substring.equals("CS04")) {
                        if (!(substring1.equals("C") || substring1.equals("B"))) {
                            tempMsg.append("发行人企业规模为CS01至CS04的，发行人国民经济部门应该为C开头的非金融企业部门或者B开头的金融机构" + SysConstants.spaceStr);
                            result = true;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowZqyezrmb(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String bz = xclzqtzxx.getBz();//币种
        String zqyezrmb = xclzqtzxx.getZqyezrmb();//债券余额折人民币
        String zqye = xclzqtzxx.getZqye();//债券余额
        BigDecimal zero = new BigDecimal(0);

        if (checknumList.contains("JS0711") && StringUtils.isNotBlank(zqye)) {
            if (zqye.length() > 20 || !CheckUtil.checkDecimal(zqye, 2)) {
                tempMsg.append("债券余额总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
                result = true;
            }
        }
        if (checknumList.contains("JS0712") && StringUtils.isNotBlank(zqyezrmb)) {
            if (zqyezrmb.length() > 20 || !CheckUtil.checkDecimal(zqyezrmb, 2)) {
                tempMsg.append("债券余额折人民币总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
                result = true;
            }
        }

        if (checknumList.contains("JS1532")) {
            if (StringUtils.isBlank(zqye)) {
                tempMsg.append("债券余额不能为空" + SysConstants.spaceStr);
                result = true;
            }
        }
        if (checknumList.contains("JS1533")) {
            if (StringUtils.isBlank(zqyezrmb)) {
                tempMsg.append("债券余额折人民币不能为空" + SysConstants.spaceStr);
                result = true;
            }
        }
        if (checknumList.contains("JS2698")) {
            if (StringUtils.isNotBlank(bz) && StringUtils.isNotBlank(bz) && StringUtils.isNotBlank(bz)) {
                if (bz.equals("CNY")) {
                    if (!zqyezrmb.equals(zqye)) {
                        tempMsg.append("币种为人民币的，债券余额折人民币应该与债券余额的值相等" + SysConstants.spaceStr);
                        result = true;
                    }
                }
            }
        }
        if (checknumList.contains("JS2350")) {
            if (StringUtils.isNotBlank(zqye)) {
                try {
                    if (new BigDecimal(zqye).compareTo(zero) <= 0) {
                        tempMsg.append("债券余额应大于0" + SysConstants.spaceStr);
                        result = true;
                    }
                } catch (Exception e) {
                    tempMsg.append("债券余额应大于0" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        if (checknumList.contains("JS2351")) {
            try {
                if (StringUtils.isNotBlank(zqyezrmb)) {
                    if (new BigDecimal(zqyezrmb).compareTo(zero) <= 0) {
                        tempMsg.append("债券余额折人名币应大于0" + SysConstants.spaceStr);
                        result = true;
                    }
                }
            } catch (Exception e) {
                tempMsg.append("债券余额折人名币应大于0" + SysConstants.spaceStr);
                result = true;
            }
        }
        return result;
    }

    private boolean checkXclzqtzxxRowFxrjjcf(List<String> checknumList, Xclzqtzxx xclzqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String fxrjjcf = xclzqtzxx.getFxrjjcf(); //发行人经济成分
        String fxrqygm = xclzqtzxx.getFxrqygm();//发行人企业规模
        String fxrhy = xclzqtzxx.getFxrhy();//发行人行业

        if (checknumList.contains("JS0030") && StringUtils.isNotBlank(fxrjjcf) && !CheckDataUtils.comma(fxrjjcf)) {
            if (!ZqFieldUtils.fxrjjcfList.contains(fxrjjcf)) {
                result = true;
                tempMsg.append("若发行人经济成分不为空，需在符合要求的值域范围内" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS1357")) {
            if (StringUtils.isNotBlank(fxrhy) && StringUtils.isNotBlank(fxrqygm)) {
                if (!fxrhy.equals("100") && !fxrhy.equals("200") && !fxrqygm.equals("CS05") && StringUtils.isBlank(fxrjjcf)) {
                    tempMsg.append("当发行人不为个人、境外非居民以及境内非企业时，发行人经济成分不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        if (checknumList.contains("JS2867")) {
            if (StringUtils.isNotBlank(fxrqygm)) {
                if (StringUtils.isNotBlank(fxrjjcf) && fxrqygm.equals("CS05")) {
                    tempMsg.append("当发行人为境外非居民或境内非企业时，发行人经济成分应为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

}
