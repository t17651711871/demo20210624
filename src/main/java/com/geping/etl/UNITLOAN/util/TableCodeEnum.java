package com.geping.etl.UNITLOAN.util;
/**
 * 数据代码枚举
 * @author WuZengWen
 * @date 2020年6月10日 下午3:40:43
 */
public enum TableCodeEnum {

	/**
	 * 二级行业代码EJHYDM
	 */
	EJHYDM("二级行业代码"),
	/**
	 * 行政区划代码XZQHDM
	 */
	XZQHDM("行政区划代码"),
	/**
	 * 一级行业代码YJHYDM
	 */
	YJHYDM("一级行业代码"),
	/**
	 * 国家代码GJDM
	 */
	GJDM("国家代码 "),
	/**
	 * 币种代码BZDM
	 */
	BZDM("币种代码 "),
	/**
	 * 单位贷款担保物信息DWDKDBWXX
	 */
	DWDKDBWXX("单位贷款担保物信息"),
	/**
	 * 金融机构（分支机构）基础信息JRJGFZJCXX
	 */
	JRJGFZJCXX("金融机构（分支机构）基础信息"),
	/**
	 * 金融机构（法人）基础信息JRJGFZJCXX
	 */
	JRJGFRJCXX("金融机构（法人）基础信息"),
	/**
	 * 金融机构（法人）基础信息-基础情况统计表JRJGFRJCXX
	 */
	JCQKTJ("金融机构（法人）基础信息-基础情况统计表"),
	/**
	 * 金融机构（法人）基础信息-资产负债及风险统计表JRJGFRJCXX
	 */
	ZCFZJFXTJ("金融机构（法人）基础信息-资产负债及风险统计表"),
	/**
	 * 金融机构（法人）基础信息-利润及资本统计表JRJGFRJCXX
	 */
	LRJZBTJ("金融机构（法人）基础信息-利润及资本统计表");
	
	private String chinese;
	
	TableCodeEnum (String chinese){
        this.chinese= chinese;
    }
 
    public String getChinese(){
        return chinese;
    }
}
