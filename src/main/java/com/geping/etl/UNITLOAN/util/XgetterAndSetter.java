package com.geping.etl.UNITLOAN.util;

import java.lang.reflect.Method;

public class XgetterAndSetter {
    public static String getter(Object o, String att){
        String value = "";
        try {
            Method method = o.getClass().getMethod("get" + att.substring(0,1).toUpperCase() + att.substring(1));
            if (method.invoke(o) != null){
                value = method.invoke(o).toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static void setter(Object o,String att,Object v,Class<?> type){
        try {
            Method method = o.getClass().getMethod("set" + att.substring(0,1).toUpperCase() + att.substring(1),type);
            method.invoke(o,v);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
