package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.entity.report.Xtdmdzttzfsexx;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DateUtils;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.util.check
 * @USER: tangshuai
 * @DATE: 2021/4/20
 * @TIME: 16:50
 * @描述:
 */
@Service
public class CheckXtdmdzttzfsexxRowData implements CheckRowData {


    public void checkRow(String header, CheckParamContext checkParamContext, Object object) {

        //特定目的载体投资发生额信息
        Xtdmdzttzfsexx xtdmdzttzfsexx = (Xtdmdzttzfsexx) object;
        StringBuffer tempMsg = new StringBuffer("");
        Boolean isError = false;
        List<String> checknumList = checkParamContext.getCheckNums();
        //数据日期
        if (checkXtdmdzttzfsexxRowSjrq(checknumList, xtdmdzttzfsexx, tempMsg)) {
            isError = true;
        }
        //特定目的载体类型
        if (checkXtdmdzttzfsexxRowTdmdztlx(checkParamContext, checknumList, xtdmdzttzfsexx, tempMsg)) {
            isError = true;
        }
        //发行人地区代码
        if (checkXtdmdzttzfsexxRowDqdm(checkParamContext, checknumList, xtdmdzttzfsexx, tempMsg)) {
            isError = true;
        }
        //运行方式
        if (checkXtdmdzttzfsexxRowYxfs(checkParamContext, checknumList, xtdmdzttzfsexx, tempMsg)) {
            isError = true;
        }
        //币种
        if (checkXtdmdzttzfsexxRowBz(checkParamContext, checknumList, xtdmdzttzfsexx, tempMsg)) {
            isError = true;
        }
        //交易方向
        if (checkXtdmdzttzfsexxRowJyfx(checkParamContext, checknumList, xtdmdzttzfsexx, tempMsg)) {
            isError = true;
        }
        //特殊字符
        if (checkXtdmdzttzfsexxRowTszf(checkParamContext, checknumList, xtdmdzttzfsexx, tempMsg)) {
            isError = true;
        }
        //长度校验
        if (checkXtdmdzttzfsexxRowCdjy(checkParamContext, checknumList, xtdmdzttzfsexx, tempMsg)) {
            isError = true;
        }
        //非空检验
        if (checkXtdmdzttzfsexxRowFkjy(checkParamContext, checknumList, xtdmdzttzfsexx, tempMsg)) {
            isError = true;
        }
        //数据比较
        if (checkXtdmdzttzfsexxRowSjbj(checkParamContext, checknumList, xtdmdzttzfsexx, tempMsg)) {
            isError = true;
        }
        //日期校验
        if (checkXtdmdzttzfsexxRowRqjy(checkParamContext, checknumList, xtdmdzttzfsexx, tempMsg)) {
            isError = true;
        }



        CheckHelper checkHelper = (CheckHelper) SpringContextUtil.getBean(CheckHelper.class);
        checkHelper.handleRowCheckResult(isError, xtdmdzttzfsexx.getId(), String.format(header, xtdmdzttzfsexx.getFinanceorgcode(), xtdmdzttzfsexx.getTdmdztdm()), tempMsg.toString(), checkParamContext);
    }

    private boolean checkXtdmdzttzfsexxRowRqjy(CheckParamContext checkParamContext, List<String> checknumList, Xtdmdzttzfsexx xtdmdzttzfsexx, StringBuffer tempMsg) {
        boolean result = false ;
        String jyrq = xtdmdzttzfsexx.getJyrq();//交易日期
        String rgrq = xtdmdzttzfsexx.getRgrq();//认购日期
        String jyfx = xtdmdzttzfsexx.getJyfx();//交易方向
        if (checknumList.contains("JS2571")&&StringUtils.isNotBlank(jyrq)){
            if (!DateUtils.inCurrentMM(jyrq)) {
                tempMsg.append("交易日期应该在当月范围内" + SysConstants.spaceStr);
                result = true;
            }
        }
        if (checknumList.contains("JS2570")&&StringUtils.isNotBlank(rgrq)){
            if (!DateUtils.inCurrentMM(rgrq)&&jyfx.equals("1")) {
                tempMsg.append("当月认购的特定目的载体，认购日期应该在当月范围内" + SysConstants.spaceStr);
                result = true;
            }
        }
        return result;
    }

    private boolean checkXtdmdzttzfsexxRowSjbj(CheckParamContext checkParamContext, List<String> checknumList, Xtdmdzttzfsexx xtdmdzttzfsexx, StringBuffer tempMsg) {
        boolean result = false ;
        String rgrq = xtdmdzttzfsexx.getRgrq();//认购日期
        String sjrq = xtdmdzttzfsexx.getSjrq();//数据日期
        String dqrq = xtdmdzttzfsexx.getDqrq();//到期日期
        String jyrq = xtdmdzttzfsexx.getJyrq();//交易日期
        String bz = xtdmdzttzfsexx.getBz();//币种
        String jyje = xtdmdzttzfsexx.getJyje();//交易金额
        String jyjezrmb = xtdmdzttzfsexx.getJyjezrmb();//交易金额折人民币
        if (checknumList.contains("JS2293")&&StringUtils.isNotBlank(rgrq)&&StringUtils.isNotBlank(sjrq)){
            if (rgrq.compareTo(sjrq)>0) {
                result = true;
                tempMsg.append("认购日期应小于等于数据日期" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2294")&&StringUtils.isNotBlank(rgrq)&&StringUtils.isNotBlank(dqrq)){
            if (rgrq.compareTo(dqrq)>0) {
                result = true;
                tempMsg.append("认购日期应小于等于到期日期" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2295")&&StringUtils.isNotBlank(jyrq)&&StringUtils.isNotBlank(rgrq)){
            if (jyrq.compareTo(rgrq)<0) {
                result = true;
                tempMsg.append("交易日期应大于等于认购日期" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2296")&&StringUtils.isNotBlank(jyrq)&&StringUtils.isNotBlank(dqrq)){
            if (jyrq.compareTo(dqrq)>0) {
                result = true;
                tempMsg.append("交易日期应小于等于到期日期" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2297")&&StringUtils.isNotBlank(jyje)){
            try {
                if (Double.parseDouble(jyje)<=0) {
                    result = true;
                    tempMsg.append("交易金额应大于0" + SysConstants.spaceStr);
                }
            } catch (NumberFormatException e) {
                result = true;
                tempMsg.append("交易金额应大于0" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2298")&&StringUtils.isNotBlank(jyjezrmb)){
            try {
                if (Double.parseDouble(jyjezrmb)<=0) {
                    result = true;
                    tempMsg.append("交易金额折人民币应大于0" + SysConstants.spaceStr);
                }
            } catch (NumberFormatException e) {
                result = true;
                tempMsg.append("交易金额折人民币应大于0" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2707")&&StringUtils.isNotBlank(jyjezrmb)&&StringUtils.isNotBlank(jyje)&&StringUtils.isNotBlank(bz)){
            try {
                if (bz.equals("CNY")&&Double.parseDouble(jyje)!=Double.parseDouble(jyjezrmb)) {
                    result = true;
                    tempMsg.append("币种为人民币的，交易金额折人民币应该与交易金额的值相等" + SysConstants.spaceStr);
                }
            } catch (NumberFormatException e) {
                result = true;
                tempMsg.append("币种为人民币的，交易金额折人民币应该与交易金额的值相等" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        return result ;
    }

    private boolean checkXtdmdzttzfsexxRowFkjy(CheckParamContext checkParamContext, List<String> checknumList, Xtdmdzttzfsexx xtdmdzttzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xtdmdzttzfsexx.getFinanceorgcode();//金融机构代码
        String tdmdztlx = xtdmdzttzfsexx.getTdmdztlx();//特定目的载体类型
        String zgcptjbm = xtdmdzttzfsexx.getZgcptjbm();//资管产品统计编码
        String fxrdm = xtdmdzttzfsexx.getFxrdm();//发行人代码
        String fxrdqdm = xtdmdzttzfsexx.getFxrdqdm();//发行人地区代码
        String yxfs = xtdmdzttzfsexx.getYxfs();//运行方式
        String rgrq = xtdmdzttzfsexx.getRgrq();//认购日期
        String dqrq = xtdmdzttzfsexx.getDqrq();//到期日期
        String jyrq = xtdmdzttzfsexx.getJyrq();//交易日期
        String bz = xtdmdzttzfsexx.getBz();//币种
        String jyje = xtdmdzttzfsexx.getJyje();//交易金额
        String jyjezrmb = xtdmdzttzfsexx.getJyjezrmb();//交易金额折人民币
        String jyfx = xtdmdzttzfsexx.getJyfx();//交易方向
        String financeorginnum = xtdmdzttzfsexx.getFinanceorginnum();//内部机构号
        String tdmdztdm = xtdmdzttzfsexx.getTdmdztdm();//特定目的载体代码
        if (checknumList.contains("JS1449") && StringUtils.isBlank(financeorgcode)) {
            result = true;
            tempMsg.append("金融机构代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1450") && StringUtils.isBlank(tdmdztlx)) {
            result = true;
            tempMsg.append("特定目的载体类型不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1451") && StringUtils.isBlank(zgcptjbm)) {
            result = true;
            tempMsg.append("资管产品统计编码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1452") && StringUtils.isBlank(fxrdm)) {
            result = true;
            tempMsg.append("发行人代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1453") && StringUtils.isBlank(fxrdqdm)) {
            result = true;
            tempMsg.append("发行人地区代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1454") && StringUtils.isBlank(yxfs)) {
            result = true;
            tempMsg.append("运行方式不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1455") && StringUtils.isBlank(rgrq)) {
            result = true;
            tempMsg.append("认购日期不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1456") && StringUtils.isBlank(dqrq)) {
            result = true;
            tempMsg.append("到期日期不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1457") && StringUtils.isBlank(jyrq)) {
            result = true;
            tempMsg.append("交易日期不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1458") && StringUtils.isBlank(bz)) {
            result = true;
            tempMsg.append("币种不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1459") && StringUtils.isBlank(jyje)) {
            result = true;
            tempMsg.append("交易金额不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1460") && StringUtils.isBlank(jyjezrmb)) {
            result = true;
            tempMsg.append("交易金额折人民币不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1461") && StringUtils.isBlank(jyfx)) {
            result = true;
            tempMsg.append("交易方向不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1565") && StringUtils.isBlank(financeorginnum)) {
            result = true;
            tempMsg.append("内部机构号不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS2617") && StringUtils.isBlank(tdmdztdm)) {
            result = true;
            tempMsg.append("特定目的载体代码不能为空" + SysConstants.spaceStr);
        }
        return result;
    }


    private boolean checkXtdmdzttzfsexxRowCdjy(CheckParamContext checkParamContext, List<String> checknumList, Xtdmdzttzfsexx xtdmdzttzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xtdmdzttzfsexx.getFinanceorgcode();//金融机构到代码
        String financeorginnum = xtdmdzttzfsexx.getFinanceorginnum();//内部机构号
        String zgcptjbm = xtdmdzttzfsexx.getZgcptjbm();//资管产品统计编码
        String fxrdqdm = xtdmdzttzfsexx.getFxrdqdm();//发行人地区代码
        String tdmdztdm = xtdmdzttzfsexx.getTdmdztdm();//特定目的载体代码
        String fxrdm = xtdmdzttzfsexx.getFxrdm();//发行人代码
        String jyje = xtdmdzttzfsexx.getJyje();//交易金额
        String jyjezrmb = xtdmdzttzfsexx.getJyjezrmb();//交易金额折人民币
        String rgrq = xtdmdzttzfsexx.getRgrq();//认购日期
        String dqrq = xtdmdzttzfsexx.getDqrq();//到期日期
        String jyrq = xtdmdzttzfsexx.getJyrq();//交易日期

        if (checknumList.contains("JS0812") && StringUtils.isNotBlank(financeorgcode)) {
            if (financeorgcode.length() != 18) {
                result = true;
                tempMsg.append("金融机构代码字符长度应该为18位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0813") && StringUtils.isNotBlank(financeorginnum)) {
            if (financeorginnum.length() > 30) {
                result = true;
                tempMsg.append("内部机构号字符长度不能超过30" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0814") && StringUtils.isNotBlank(zgcptjbm)) {
            if (zgcptjbm.length() > 30) {
                result = true;
                tempMsg.append("资管产品统计代码字符长度不能超过30" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0815") && StringUtils.isNotBlank(tdmdztdm)) {
            if (tdmdztdm.length() > 30) {
                result = true;
                tempMsg.append("当特定目的载体代码不为空时，特定目的载体代码字符长度不能超过30" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0817") && StringUtils.isNotBlank(jyje)) {
            if (jyje.length() > 20 || !CheckUtil.checkDecimal(jyje, 2)) {
                result = true;
                tempMsg.append("交易金额总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0818") && StringUtils.isNotBlank(jyjezrmb)) {
            if (jyjezrmb.length() > 20 || !CheckUtil.checkDecimal(jyjezrmb, 2)) {
                result = true;
                tempMsg.append("交易金额折人民币总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0816") && StringUtils.isNotBlank(fxrdm) && StringUtils.isNotBlank(fxrdqdm)) {
            try {
                if (fxrdm.length() != 18 && !fxrdqdm.substring(0, 3).equals("000")) {
                    result = true;
                    tempMsg.append("境内发行人代码字符长度应该为18位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                result = true;
                tempMsg.append("境内发行人代码字符长度应该为18位" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2620") && StringUtils.isNotBlank(fxrdm)) {
            try {
                if (fxrdm.length() > 60) {
                    result = true;
                    tempMsg.append("发行人代码字符长度不能超过60位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                result = true;
                tempMsg.append("发行人代码字符长度不能超过60位" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }

        if (checknumList.contains("JS0820") && StringUtils.isNotBlank(rgrq)) {
            if (!CheckUtil.checkDate(rgrq, "yyyy-MM-dd") || rgrq.compareTo("1800-01-01") < 0 || rgrq.compareTo("2100-12-31") > 0) {
                result = true;
                tempMsg.append("认购日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0821") && StringUtils.isNotBlank(dqrq)) {
            if (!CheckUtil.checkDate(dqrq, "yyyy-MM-dd") || dqrq.compareTo("1800-01-01") < 0 || dqrq.compareTo("2100-12-31") > 0) {
                result = true;
                tempMsg.append("到期日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0822") && StringUtils.isNotBlank(jyrq)) {
            if (!CheckUtil.checkDate(jyrq, "yyyy-MM-dd") || jyrq.compareTo("1800-01-01") < 0 || jyrq.compareTo("2100-12-31") > 0) {
                result = true;
                tempMsg.append("交易日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXtdmdzttzfsexxRowTszf(CheckParamContext checkParamContext, List<String> checknumList, Xtdmdzttzfsexx xtdmdzttzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xtdmdzttzfsexx.getFinanceorgcode();//金融机构到代码
        String financeorginnum = xtdmdzttzfsexx.getFinanceorginnum();//内部机构号
        String zgcptjbm = xtdmdzttzfsexx.getZgcptjbm();//资管产统计品编码
        String fxrdm = xtdmdzttzfsexx.getFxrdm();//发行人代码
        String tdmdztdm = xtdmdzttzfsexx.getTdmdztdm();//特定目的载体代码
        if (checknumList.contains("JS0806") && StringUtils.isNotBlank(financeorgcode)) {
            if (CheckUtil.checkStr(financeorgcode)) {
                result = true;
                tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0807") && StringUtils.isNotBlank(financeorginnum)) {
            if (CheckUtil.checkStr(financeorginnum)) {
                result = true;
                tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0808") && StringUtils.isNotBlank(zgcptjbm)) {
            if (CheckUtil.checkStr(zgcptjbm)) {
                result = true;
                tempMsg.append("资管产品统计编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0810") && StringUtils.isNotBlank(fxrdm)) {
            if (CheckUtil.checkStr(fxrdm)) {
                result = true;
                tempMsg.append("发行人代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0811") && StringUtils.isNotBlank(tdmdztdm)) {
            if (CheckUtil.checkStr(tdmdztdm)) {
                result = true;
                tempMsg.append("特定目的载体代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXtdmdzttzfsexxRowJyfx(CheckParamContext checkParamContext, List<String> checknumList, Xtdmdzttzfsexx xtdmdzttzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String jyfx = xtdmdzttzfsexx.getJyfx();//交易方向
        List jyfxList = checkParamContext.getDataDictionarymap().get("jyfxList");//交易方向List
        if (checknumList.contains("JS0065") && StringUtils.isNotBlank(jyfx)) {
            if (!jyfxList.contains(jyfx)) {
                result = true;
                tempMsg.append("特定目的载体投资发生额信息表中的交易方向需在符合要求的值域范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXtdmdzttzfsexxRowBz(CheckParamContext checkParamContext, List<String> checknumList, Xtdmdzttzfsexx xtdmdzttzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String bz = xtdmdzttzfsexx.getBz();//币种
        List baseCode = checkParamContext.getDataDictionarymap().get("baseCode");//币种代码
        if (checknumList.contains("JS0064") && StringUtils.isNotBlank(bz)) {
            if (!baseCode.contains(bz)) {
                result = true;
                tempMsg.append("特定目的载体投资发生额信息表中的币种需在《表示货币和资金的代码》（GB/T 12406）中的三位字母范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXtdmdzttzfsexxRowYxfs(CheckParamContext checkParamContext, List<String> checknumList, Xtdmdzttzfsexx xtdmdzttzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String yxfs = xtdmdzttzfsexx.getYxfs();
        if (checknumList.contains("JS0063") && StringUtils.isNotBlank(yxfs)) {
            if (!checkParamContext.getDataDictionarymap().get("yxfsList").contains(yxfs)) {
                tempMsg.append("特定目的载体投资发生额信息表中的运行方式需在符合要求的值域范围内" + SysConstants.spaceStr);
                result = true;
            }
        }
        return result;
    }

    private boolean checkXtdmdzttzfsexxRowDqdm(CheckParamContext checkParamContext, List<String> checknumList, Xtdmdzttzfsexx xtdmdzttzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String dqdm = xtdmdzttzfsexx.getFxrdqdm();//地区代码
        List baseCodeGj = checkParamContext.getDataDictionarymap().get("baseCodeGj");//国家地区代码C0013
        List baseCodeXz = checkParamContext.getDataDictionarymap().get("baseCodeXz");//国家地区代码C0013
        if (checknumList.contains("JS0062") && StringUtils.isNotBlank(dqdm)) {
            if ((!baseCodeGj.contains(dqdm) && !baseCodeXz.contains(dqdm))) {
                result = true;
                tempMsg.append("特定目的载体投资发生额信息表中的发行人地区代码需在符合要求的值域范围内，境内机构为《中华人民共和国行政区划代码》（GB/T 2260）最新标准的县（区）级数字码值域范围内（不包括港澳台），境外机构为000+《世界各国和地区名称代码》（GB/T 2659）的三位国别数字代码" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXtdmdzttzfsexxRowTdmdztlx(CheckParamContext checkParamContext, List<String> checknumList, Xtdmdzttzfsexx xtdmdzttzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String tdmdztlx = xtdmdzttzfsexx.getTdmdztlx();
        if (checknumList.contains("JS0061") && StringUtils.isNotBlank(tdmdztlx)) {
            if (!checkParamContext.getDataDictionarymap().get("ztlxList").contains(tdmdztlx)) {
                tempMsg.append("特定母的载体投资发生额信息表中的特定目的载体类型需在符合要求的值域范围内" + SysConstants.spaceStr);
                result = true;
            }
        }
        return result;
    }


    private boolean checkXtdmdzttzfsexxRowSjrq(List<String> checknumList, Xtdmdzttzfsexx xtdmdzttzfsexx, StringBuffer tempMsg) {
        boolean result = false;
        String sjrq = xtdmdzttzfsexx.getSjrq();//数据日期
        if (StringUtils.isNotBlank(sjrq)) {
            if (sjrq.length() == 10) {
                boolean b1 = CheckUtil.checkDate(sjrq, "yyyy-MM-dd");
                if (b1) {
                    if (sjrq.compareTo("1800-01-01") < 0 || sjrq.compareTo("2100-12-31") > 0) {
                        tempMsg.append("数据日期需晚于1800-01-01早于2100-12-31" + SysConstants.spaceStr);
                        result = true;
                    }
                } else {
                    tempMsg.append("数据日期不符合yyyy-MM-dd格式" + SysConstants.spaceStr);
                    result = true;
                }
            } else {
                tempMsg.append("数据日期长度不等于10" + SysConstants.spaceStr);
                result = true;
            }
        } else {
            tempMsg.append("数据日期不能为空" + SysConstants.spaceStr);
            result = true;
        }
        return result;
    }


}