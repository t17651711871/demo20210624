package com.geping.etl.UNITLOAN.util.IOUtil;

import com.github.junrar.Archive;
import com.github.junrar.Junrar;
import com.github.junrar.exception.RarException;
import com.github.junrar.rarfile.FileHeader;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Expand;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * @Author: wangzd
 * @Date: 14:28 2020/6/22
 */
public class ReadMoreFileUtil {

    /**
     * 读取txt
     * @param filePath 文件路径
     * @return List<String>
     */
    public static List<String> readTxt(String filePath){
        List<String> resultList = new ArrayList<>();
        BufferedReader bufferedReader = null;
        InputStream inputStream = null;
        String line;//读取每一行
        try {
            File file = new File(filePath);
            if (file.exists()) {
                inputStream = new FileInputStream(file);
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"));
                while ((line = bufferedReader.readLine()) != null) {
                    if ("".equals(line)) {
                        continue;
                    } else {
                        resultList.add(line);
                    }
                }
            }else {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultList;
    }

    /**
     * 解压rar文件 不支持rar5
     * @param rarDir rar文件路径
     * @param outDir 解压文件输出路径
     * @throws Exception
     */
    public static void readRar(String rarDir, String outDir) throws IOException, RarException {
        File file = new File(rarDir);
        if (file.exists()){
            File outFileDir = new File(outDir);
            if (!outFileDir.exists()) {
                outFileDir.mkdirs();
            }
            List<File> extract = Junrar.extract(rarDir, outDir);
            final Archive archive = new Archive(new FileInputStream(rarDir));
            while (true) {
                FileHeader fileHeader = archive.nextFileHeader();
                if (fileHeader == null) {
                    break;
                }
                File out = new File(outDir + fileHeader.getFileNameString());
                if (!out.exists()) {
                    if (!out.getParentFile().exists()) {
                        out.getParentFile().mkdirs();
                    }
                    out.createNewFile();
                }
                FileOutputStream os = new FileOutputStream(out);
                archive.extractFile(fileHeader, os);
                os.close();
            }
            archive.close();
        }
    }

    /*public static boolean readRar5(String winRar,String rarDir, String outDir){
        boolean bool = false;
        File f=new File(rarDir);
        if(!f.exists()){
            return false;
        }
        String cmd="cmd.exe /c ";
        String winrarCmd = winRar + " WinRaR X " + rarDir + " "+outDir;
        try {
            Runtime runtime = Runtime.getRuntime();
            Runtime.getRuntime().exec(cmd+winRar.substring(0,winRar.indexOf(":")+1));
            Runtime.getRuntime().exec(cmd+winRar);
            Process proc = Runtime.getRuntime().exec(cmd+winrarCmd);
            if (proc.waitFor() == 0) {
                bool = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bool;
    }*/

    /**
     * 解压文件
     * @param zipFile（压缩文件，必须是后缀为zip的文件，不能是文件夹）
     * @param unzipFilePath（解压后文件的相对路径）
     * @return
     */
    public static void unzipFile(File zipFile,String unzipFilePath) {
        //if(zipFile.exists() && zipFile.getName().endsWith(".zip")){
            Project proj = new Project();
            Expand expand = new Expand();
            expand.setProject(proj);
            expand.setTaskType("unzip");
            expand.setTaskName("unzip");
            expand.setEncoding("UTF-8");
            expand.setSrc(zipFile);
            expand.setDest(new File(unzipFilePath));
            expand.execute();
        //}
    }

    public static void JieYaRAR(File rarFile, String outDir) throws Exception {
    	File outFileDir = new File(outDir);
        if (!outFileDir.exists()) {
            boolean isMakDir = outFileDir.mkdirs();
            if (isMakDir) {
                System.out.println("创建压缩目录成功");
            }
        }
        Archive archive = new Archive(new FileInputStream(rarFile));
        FileHeader fileHeader = archive.nextFileHeader();
        while (fileHeader != null) {
            if (fileHeader.isDirectory()) {
                fileHeader = archive.nextFileHeader();
                continue;
            }
            File out = new File(outDir + fileHeader.getFileNameString());
            if (!out.exists()) {
                if (!out.getParentFile().exists()) {
                    out.getParentFile().mkdirs();
                }
                out.createNewFile();
            }
            FileOutputStream os = new FileOutputStream(out);
            archive.extractFile(fileHeader, os);
            os.close();
            fileHeader = archive.nextFileHeader();
        }
        archive.close();            
    }
    
    public static void JieYaZIP(String srcFile, String destDirPath) {
    	long start = System.currentTimeMillis();
    	ZipFile zipFile = null;
    	 try {
             zipFile = new ZipFile(srcFile);
             Enumeration<?> entries = zipFile.entries();
             while (entries.hasMoreElements()) {
                 ZipEntry entry = (ZipEntry) entries.nextElement();
                 System.out.println("解压" + entry.getName());
                 // 如果是文件夹，就创建个文件夹
                 if (entry.isDirectory()) {
                     String dirPath = destDirPath + "/" + entry.getName();
                     File dir = new File(dirPath);
                     dir.mkdirs();
                 } else {
                     // 如果是文件，就先创建一个文件，然后用io流把内容copy过去
                     File targetFile = new File(destDirPath + "/" + entry.getName());
                     // 保证这个文件的父文件夹必须要存在
                     if(!targetFile.getParentFile().exists()){
                         targetFile.getParentFile().mkdirs();
                     }
                     targetFile.createNewFile();
                     // 将压缩文件内容写入到这个文件中
                     InputStream is = zipFile.getInputStream(entry);
                     FileOutputStream fos = new FileOutputStream(targetFile);
                     int len;
                     byte[] buf = new byte[1024];
                     while ((len = is.read(buf)) != -1) {
                         fos.write(buf, 0, len);
                     }
                     // 关流顺序，先打开的后关闭
                     fos.close();
                     is.close();
                 }
             }
             long end = System.currentTimeMillis();
             System.out.println("解压完成，耗时：" + (end - start) +" ms");
         } catch (Exception e) {
             throw new RuntimeException("unzip error from ZipUtils", e);
         } finally {
             if(zipFile != null){
                 try {
                     zipFile.close();
                 } catch (IOException e) {
                     e.printStackTrace();
                 }
             }
         }
    }
    
    public static void main(String[] args) throws IOException, RarException {
        readRar("E:\\log\\Desktop.rar","E:\\log");
    }

}
