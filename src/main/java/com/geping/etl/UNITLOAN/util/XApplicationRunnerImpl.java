package com.geping.etl.UNITLOAN.util;

import com.geping.etl.UNITLOAN.entity.baseInfo.*;
import com.geping.etl.UNITLOAN.service.baseInfo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class XApplicationRunnerImpl implements ApplicationRunner {
    @Autowired
    private BaseAindustryService ads;
    @Autowired
    private BaseAreaService bas;
    @Autowired
    private BaseBindustryService bbs;
    @Autowired
    private BaseCountryService cts;
    @Autowired
    private BaseCurrencyService ces;
    @Autowired
    private BaseCountryTwoService bcts;
    @Autowired
    private BaseEducationService bes;
    @Autowired
    private BaseNationService bns;
    /**
     * 一级行业代码
     */
    public static List<BaseAindustry>  baseAindustryList;
    /**
     * 行政区划代码
     */
    public static List<BaseArea>  baseAreaList;
    /**
     * 二级行业代码
     */
    public static List<BaseBindustry>  baseBindustryList;
    /**
     * 国家代码
     */
    public static List<BaseCountry>  baseCountryList;
    /**
     * 币种代码
     */
    public static List<BaseCurrency>  baseCurrencyList;
    /**
     * 国籍代码
     */
    public static List<BaseCountryTwo> baseCountryTwoList;
    /**
     * 学历代码
     */
    public static List<BaseEducation> baseEducationList;
    /**
     * 民族代码
     */
    public static List<BaseNation> baseNationList;

    
    @Override
    public void run(ApplicationArguments args) throws Exception {
        baseAindustryList = ads.findAll();
        baseAreaList = bas.findAll();
        baseBindustryList = bbs.findAll();
        baseCountryList = cts.findAll();
        baseCurrencyList = ces.findAll();
        baseCountryTwoList = bcts.findAll();
        baseEducationList = bes.findAll();
        baseNationList = bns.findAll();
    }
}