package com.geping.etl.UNITLOAN.util;

import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author: wangzd
 * @Date: 11:17 2020/6/5
 * @description:计算数量工具类
 */

@Component
@Transactional
public class CalcCountUtil {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private HttpServletRequest request;

    //0:待提交;1:待审核;2:审核不通过;3:审核通过
    private final List<Integer> statusCodes= Arrays.asList(new Integer[]{0,1,2,3});

    //datastatus->0:待提交;1:待审核;2:审核不通过;3:审核通过
    private final String[] whereType=new String[]{" and datastatus='0'"," and datastatus='1'"," and datastatus='2'"," and datastatus='3'"};

    private Map<Integer,String> statusMap=new HashMap<>();
    {
        statusMap.put(0,"pending");
        statusMap.put(1,"pending_audit");
        statusMap.put(2,"check_fail");
        statusMap.put(3,"check_success");
    }

    /**
     * 根据表名和操作类型查询所有数量
     * @param tableName 表名
     * @param isDept 是否分部门
     * @param statusCode 状态代码->0:待提交;1:待审核;2:审核不通过;3:审核通过
     * @return Map<状态代码, 查出数量>
     */
    @Transactional(readOnly = true)
    public Map<Integer, Long> getCountByTable(@NotNull String tableName, boolean isDept, Integer... statusCode){
        Map<Integer,Long> map=new HashMap<>();
        Sys_UserAndOrgDepartment sys_user = (Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User");
        StringBuffer sql=new StringBuffer("select count(*) from "+tableName+" t where t.orgid=?1 "+(isDept?" and t.departid=?2 ":""));
        int sqlSize=sql.length();
        Query nativeQuery;
        if (statusCode==null){
            //查询表中所有操作类型的数据数量
            for (int i=0;i<whereType.length;i++){
                sql.append(whereType[i]);
                nativeQuery = em.createNativeQuery(String.valueOf(sql));
                nativeQuery.setParameter(1,sys_user.getOrgid());
                if (isDept){
                    nativeQuery.setParameter(2,sys_user.getDepartid());
                }
                Object singleResult = nativeQuery.getSingleResult();
                Long aLong = Long.valueOf(String.valueOf(singleResult));
                map.put(i,aLong);
                sql.delete(sqlSize,sql.length());
            }
        }else {
            if (!statusCodes.containsAll(Arrays.asList(statusCode))){
                throw new RuntimeException("statusCode is a error param");
            }
            for (Integer i : statusCode){
                sql.append(whereType[i]);
                nativeQuery = em.createNativeQuery(String.valueOf(sql));
                nativeQuery.setParameter(1,sys_user.getOrgid());
                if (isDept){
                    nativeQuery.setParameter(2,sys_user.getDepartid());
                }
                Object singleResult = nativeQuery.getSingleResult();
                Long aLong = Long.valueOf(String.valueOf(singleResult));
                map.put(i,aLong);
                sql.delete(sqlSize,sql.length());
            }
        }
        return map;
    }

    /**
     * 获取数据管理下所有表数据数量
     * @param subjectId 项目id
     * @param isDept 是否分部门
     * @return Map<表名,Map<状态代码,查出数量>>
     */
    @Transactional(readOnly = true)
    public Map<String,Map<Integer,Long>> getAllTableCount(String subjectId, boolean isDept){
        Map<String,Map<Integer,Long>> tableMap=new HashMap<>();
        //instruction字段在这里用作表名
        String sql="select t.instruction from sys_report_info t where t.subject_id=?1";
        Query nativeQuery = em.createNativeQuery(sql);
        nativeQuery.setParameter(1,subjectId);
        List<String> resultList = nativeQuery.getResultList();
        resultList.forEach(tableName->{
            Map<Integer, Long> countByTable = getCountByTable(tableName, isDept, null);
            tableMap.put(tableName,countByTable);
        });
        return tableMap;
    }

    /**
     * 根据表名更新sys_report_info表
     * @param tableName 表名
     * @param map Map<状态代码,修改数量>
     * @return int
     */
    public int updateCountByTable(@NotNull String tableName,Map<Integer,Long> map){
        StringBuffer setString=new StringBuffer(" set ");
        map.forEach((i,count)->{
            setString.append(statusMap.get(i)+"="+count+",");
        });
        setString.deleteCharAt(setString.length()-1);
        String sql="update sys_report_info "+setString+" where instruction=?1";
        Query nativeQuery = em.createNativeQuery(sql);
        nativeQuery.setParameter(1,tableName);
        int i = nativeQuery.executeUpdate();
        return i;
    }

    /**
     * 根据subjectId 更新sys_report_info中
     * @param map Map<表名,Map<状态代码,查出数量>>
     * @return int
     */
    public int updateAllTableCount(@NotNull Map<String,Map<Integer,Long>> map){
        AtomicInteger i= new AtomicInteger();
        map.forEach((tableName,type)->{
            i.addAndGet(updateCountByTable(tableName, type));
        });
         i.set(i.get() / map.size());
        return i.get();
    }

    /**
     * 处理sys_report_info表中单个状态数量
     * @param tableName 表名
     * @param statusCode 状态代码->0:待提交;1:待审核;2:审核不通过;3:审核通过
     * @param operation 操作->'add':增加;'reduce':减少
     * @param count 添加数量
     * @return int
     */
    public int handleCount(@NotNull String tableName,@NotNull Integer statusCode,@NotNull String operation,int count){
    	//目前程序用不到，oracle会有问题，暂时注释掉
//        String statusField = statusMap.get(statusCode);
//        if ("add".equals(operation)){
//            operation=statusField+"="+statusField+"+";
//        }else if ("reduce".equals(operation)){
//            operation=statusField+"="+statusField+"-";
//        }else {
//            throw new RuntimeException("operation is a error param");
//        }
//        String sql="update sys_report_info set "+operation+count+" where instruction=?1";
//        Query nativeQuery = em.createNativeQuery(sql);
//        nativeQuery.setParameter(1,tableName);
//        return nativeQuery.executeUpdate();
    	return 1;
    }

    /**
     * 处理sys_report_info表中多个状态数量
     * @param tableName 表名
     * @param map Map<状态代码,操作>
     *            状态代码->0:待提交;1:待审核;2:审核不通过;3:审核通过
     *            操作->'add':增加;'reduce':减少
     * @param count 修改数量
     * @return int
     */
    public int handleMoreCount(@NotNull String tableName,@NotNull Map<Integer,String> map,int count){
        AtomicInteger i= new AtomicInteger();
        map.forEach((statusCode,operation)->{
            i.addAndGet(handleCount(tableName, statusCode, operation, count));
        });
        i.set(i.get()/map.size());
        return i.get();
    }
}
