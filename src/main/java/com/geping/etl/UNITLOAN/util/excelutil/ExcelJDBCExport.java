package com.geping.etl.UNITLOAN.util.excelutil;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @Author: david
 * @Date: 2019/4/11
 * @Description: excel jdbc 导出工具类
 */
public class ExcelJDBCExport {

    /**
     * 每个sheet最大行数
     */
    private static final int SHEETMAXROW = 1000000;

    /**
     * @param titleList 表头
     * @param workbook
     * @param sheetName sheet 名称
     * @param offset 偏移量
     * @param length 长度
     * @param resultSet 结果集
     * @throws SQLException
     */
    public static void createSheet(Map<String,String> titleMap, Workbook workbook, String sheetName,  ResultSet resultSet) throws SQLException {
        //创建sheet
        Sheet sheet = workbook.createSheet(sheetName);
        //创建表头
        createExcelHead(sheet, titleMap);
        //当前行数
        int rowNum = 1;
        int sheetNum = 1;
        Row row = null;
        Cell cell = null;
        //循环构建数据
        while (resultSet.next()) {
            //如果超过最大行数创建第二个sheet
            if (rowNum > SHEETMAXROW) {
                sheet = workbook.createSheet(sheetName + sheetNum);
                createExcelHead(sheet, titleMap);
                rowNum = 1;
                sheetNum++;
            }
            row = sheet.createRow(rowNum);
            int i = 0;
            for(Map.Entry<String,String> entry : titleMap.entrySet()) {
            	cell = row.createCell(i);
                String value = resultSet.getString(entry.getKey());
                cell.setCellValue(value);
                i++;
            }
            rowNum++;
        }
    }

    private static void createExcelHead(Sheet sheet, Map<String,String> titleMap) {
        Row row = sheet.createRow(0);
        int i = 0;
        for(String s : titleMap.values()) {
        	Cell cell = row.createCell(i);
            cell.setCellValue(s);
            sheet.setColumnWidth(i, s.length() * 650);
            i++;
        }
    }
}
