package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.dataDictionary.ZqFieldUtils;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.report.Xclgqtzxx;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.util.check
 * @USER: tangshuai
 * @DATE: 2021/4/20
 * @TIME: 16:50
 * @描述:
 */
@Service
public class CheckXclgqtzxxRowData implements CheckRowData {



    public void checkRow(String header, CheckParamContext checkParamContext, Object object) {

        //存量股权投资信息
        Xclgqtzxx xclgqtzxx = (Xclgqtzxx) object;
        StringBuffer tempMsg = new StringBuffer("");
        Boolean isError = false;
        List<String>checknumList=checkParamContext.getCheckNums();
        //数据日期
        if (checkXclgqtzxxRowSjrq(checknumList, xclgqtzxx, tempMsg)) {
            isError = true;
        }
        //股权类型
        if (checkXclgqtzxxRowGqlx(checknumList, xclgqtzxx, tempMsg)) {
            isError = true;
        }
        //地区代码
        if (checkXclgqtzxxRowDqdm(checkParamContext,checknumList, xclgqtzxx, tempMsg)) {
            isError = true;
        }
        //币种
        if (checkXclgqtzxxRowBz(checkParamContext,checknumList, xclgqtzxx, tempMsg)) {
            isError = true;
        }
        //机构类型
        if (checkXclgqtzxxRowJglx(checkParamContext,checknumList, xclgqtzxx, tempMsg)) {
            isError = true;
        }
        //特殊符号
        if (checkXclgqtzxxRowTszf(checkParamContext,checknumList, xclgqtzxx, tempMsg)) {
            isError = true;
        }
        //长度校验
        if (checkXclgqtzxxRowCdjy(checkParamContext,checknumList, xclgqtzxx, tempMsg)) {
            isError = true;
        }
        //非空校验
        if (checkXclgqtzxxRowFkjy(checkParamContext,checknumList, xclgqtzxx, tempMsg)) {
            isError = true;
        }
        //投资余额
        if (checkXclgqtzxxRowTzye(checkParamContext,checknumList, xclgqtzxx, tempMsg)) {
            isError = true;
        }
        //投资余额折人民币
        if (checkXclgqtzxxRowTzyezrmb(checkParamContext,checknumList, xclgqtzxx, tempMsg)) {
            isError = true;
        }



        CheckHelper checkHelper = (CheckHelper) SpringContextUtil.getBean(CheckHelper.class);

        checkHelper.handleRowCheckResult(isError,  xclgqtzxx.getId(), String.format(header, xclgqtzxx.getFinanceorgcode(), xclgqtzxx.getPzbm()), tempMsg.toString(), checkParamContext);

    }

    private boolean checkXclgqtzxxRowTzyezrmb(CheckParamContext checkParamContext, List<String> checknumList, Xclgqtzxx xclgqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String tzyezrmb = xclgqtzxx.getTzyezrmb();//投资余额折人民币

        String tzye = xclgqtzxx.getTzye();//投资余额
        String bz = xclgqtzxx.getBz();//币种
        if (checknumList.contains("JS2285")&&StringUtils.isNotBlank(tzyezrmb)){
            try {
                if (Double.parseDouble(tzyezrmb)<=0){
                    result = true;
                    tempMsg.append("投资余额折人民币应大于0" + SysConstants.spaceStr);
                }
            } catch (NumberFormatException e) {
                result = true;
                tempMsg.append("投资余额折人民币应大于0" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2704")&&StringUtils.isNotBlank(tzyezrmb)&&StringUtils.isNotBlank(bz)){
            try {
                if (bz.equals("CNY")&&Double.parseDouble(tzyezrmb)!=Double.parseDouble(tzye)){
                    result = true;
                    tempMsg.append("币种为人民币的，投资余额折人民币应该与投资余额的值相等" + SysConstants.spaceStr);
                }
            } catch (NumberFormatException e) {
                result = true;
                tempMsg.append("币种为人民币的，投资余额折人民币应该与投资余额的值相等" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkXclgqtzxxRowTzye(CheckParamContext checkParamContext, List<String> checknumList, Xclgqtzxx xclgqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String tzye = xclgqtzxx.getTzye();//投资余额
        if (checknumList.contains("JS2284")&&StringUtils.isNotBlank(tzye)){
            try {
                if (Double.parseDouble(tzye)<=0){
                    result = true;
                    tempMsg.append("投资余额应大于0" + SysConstants.spaceStr);
                }
            } catch (NumberFormatException e) {
                result = true;
                tempMsg.append("投资余额应大于0" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean checkXclgqtzxxRowFkjy(CheckParamContext checkParamContext, List<String> checknumList, Xclgqtzxx xclgqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xclgqtzxx.getFinanceorgcode();//金融机构代码
        String financeorginnum = xclgqtzxx.getFinanceorginnum();//内部机构号
        String pzbm = xclgqtzxx.getPzbm();//凭证编码
        String gqlx = xclgqtzxx.getGqlx();//股权类型
        String jglx = xclgqtzxx.getJglx();//机构类型
        String jgzjdm = xclgqtzxx.getJgzjdm();//机构证件代码
        String dqdm = xclgqtzxx.getDqdm();//地区代码
        String bz = xclgqtzxx.getBz();//币种
        String tzye = xclgqtzxx.getTzye();//投资余额
        String tzyezrmb = xclgqtzxx.getTzyezrmb();//投资余额折人民币
        if (checknumList.contains("JS1414")&&StringUtils.isBlank(financeorgcode)){
            result = true;
            tempMsg.append("金融机构代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1415")&&StringUtils.isBlank(financeorginnum)){
            result = true;
            tempMsg.append("内部机构号不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1416")&&StringUtils.isBlank(pzbm)){
            result = true;
            tempMsg.append("凭证编码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1417")&&StringUtils.isBlank(gqlx)){
            result = true;
            tempMsg.append("股权类型不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1418")&&StringUtils.isBlank(jglx)){
            result = true;
            tempMsg.append("机构类型不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1419")&&StringUtils.isBlank(jgzjdm)){
            result = true;
            tempMsg.append("机构证件代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1420")&&StringUtils.isBlank(dqdm)){
            result = true;
            tempMsg.append("地区代码不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1421")&&StringUtils.isBlank(bz)){
            result = true;
            tempMsg.append("币种不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1422")&&StringUtils.isBlank(tzye)){
            result = true;
            tempMsg.append("投资余额不能为空" + SysConstants.spaceStr);
        }
        if (checknumList.contains("JS1423")&&StringUtils.isBlank(tzyezrmb)){
            result = true;
            tempMsg.append("投资余额折人民币不能为空" + SysConstants.spaceStr);
        }
        return result;
    }

    private boolean checkXclgqtzxxRowCdjy(CheckParamContext checkParamContext, List<String> checknumList, Xclgqtzxx xclgqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xclgqtzxx.getFinanceorgcode();//金融机构到代码
        String pzbm = xclgqtzxx.getPzbm();//凭证编码
        String dqdm = xclgqtzxx.getDqdm();//地区代码
        String jgzjdm = xclgqtzxx.getJgzjdm();//机构证件代码
        String financeorginnum = xclgqtzxx.getFinanceorginnum();//内部机构号
        String tzye = xclgqtzxx.getTzye();//投资余额
        String tzyezrmb = xclgqtzxx.getTzyezrmb();//投资余额折人民币
        if (checknumList.contains("JS0771")&&StringUtils.isNotBlank(financeorgcode)){
            if (financeorgcode.length()!=18){
                result = true;
                tempMsg.append("金融机构代码字符长度应该为18位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0772")&&StringUtils.isNotBlank(pzbm)){
            if (pzbm.length()>100){
                result = true;
                tempMsg.append("凭证编码字符长度不能超过100" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0773")&&StringUtils.isNotBlank(jgzjdm)){
                if (jgzjdm.length()>60){
                    result = true;
                    tempMsg.append("机构证件代码字符长度不能超过60" + SysConstants.spaceStr);
                }
        }
        if (checknumList.contains("JS0896")&&StringUtils.isNotBlank(jgzjdm)&&StringUtils.isNotBlank(dqdm)){
            try {
                if (jgzjdm.length()!=18&&!dqdm.substring(0,3).equals("000")){
                    result = true;
                    tempMsg.append("境内客户的机构证件代码字符长度应该为18位" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                result = true;
                tempMsg.append("境内客户的机构证件代码字符长度应该为18位" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS0774")&&StringUtils.isNotBlank(financeorginnum)){
            if (financeorginnum.length()>30){
                result = true;
                tempMsg.append("内部机构号字符长度不能超过30" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0775")&&StringUtils.isNotBlank(tzye)){
            if (tzye.length() > 20 || !CheckUtil.checkDecimal(tzye, 2)) {
                result = true;
                tempMsg.append("投资余额总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0776")&&StringUtils.isNotBlank(tzyezrmb)){
            if (tzyezrmb.length() > 20 || !CheckUtil.checkDecimal(tzyezrmb, 2)) {
                result = true;
                tempMsg.append("投资余额折人民币总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXclgqtzxxRowTszf(CheckParamContext checkParamContext, List<String> checknumList, Xclgqtzxx xclgqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String financeorgcode = xclgqtzxx.getFinanceorgcode();//金融机构到代码
        String pzbm = xclgqtzxx.getPzbm();//凭证编码
        String jgzjdm = xclgqtzxx.getJgzjdm();//机构证件代码
        String financeorginnum = xclgqtzxx.getFinanceorginnum();//内部机构号

        if (checknumList.contains("JS0767") && StringUtils.isNotBlank(financeorgcode)) {
            if (CheckUtil.checkStr(financeorgcode)) {
                result = true;
                tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0768") && StringUtils.isNotBlank(pzbm)) {
            if (CheckUtil.checkStr(pzbm)) {
                result = true;
                tempMsg.append("凭证编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0769") && StringUtils.isNotBlank(jgzjdm)) {
            if (CheckUtil.checkStr(jgzjdm)) {
                result = true;
                tempMsg.append("机构证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS0770") && StringUtils.isNotBlank(financeorginnum)) {
            if (CheckUtil.checkStr(financeorginnum)) {
                result = true;
                tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXclgqtzxxRowJglx(CheckParamContext checkParamContext,List<String> checknumList, Xclgqtzxx xclgqtzxx, StringBuffer tempMsg) {
        boolean result =false;
        String jglx = xclgqtzxx.getJglx();
        List jglxList = checkParamContext.getDataDictionarymap().get("jglxList");//机构类型
        if (checknumList.contains("JS0202")&&StringUtils.isNotBlank(jglx)){
            if (!jglxList.contains(jglx)){
                result = true;
                tempMsg.append("存量股权投资信息表中的机构类型需在符合要求的最底层值域范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXclgqtzxxRowBz(CheckParamContext checkParamContext,List<String> checknumList, Xclgqtzxx xclgqtzxx, StringBuffer tempMsg) {
        boolean result =false;
        String bz = xclgqtzxx.getBz();//币种
        List baseCode = checkParamContext.getDataDictionarymap().get("baseCode");//币种
        if (checknumList.contains("JS0052")&&StringUtils.isNotBlank(bz)){
            if (!baseCode.contains(bz)){
                result = true;
                tempMsg.append("存量股权投资信息表中的币种需在《表示货币和资金的代码》（GB/T 12406）中的三位字母范围内" + SysConstants.spaceStr);
            }
        }
        return result;
    }

    private boolean checkXclgqtzxxRowDqdm(CheckParamContext checkParamContext,List<String> checknumList, Xclgqtzxx xclgqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String dqdm = xclgqtzxx.getDqdm();//地区代码
        String jglx = xclgqtzxx.getJglx();//机构类型
        List baseCodeGj = checkParamContext.getDataDictionarymap().get("baseCodeGj");//国家地区代码C0013
        List baseCodeXz = checkParamContext.getDataDictionarymap().get("baseCodeXz");//国家地区代码C0013
        if (checknumList.contains("JS0051")&&StringUtils.isNotBlank(dqdm)){
            if ((!baseCodeGj.contains(dqdm)&&!baseCodeXz.contains(dqdm))){
                result = true;
                tempMsg.append("存量股权投资信息表中的地区代码需在符合要求的值域范围内，境内客户为《中华人民共和国行政区划代码》（GB/T 2260）规定的最新行政区划代码（不包括港澳台），境外客户为000+《世界各国和地区名称代码》（GB/T 2659）的三位国别数字代码" + SysConstants.spaceStr);
            }
        }
        if (checknumList.contains("JS2073")&&StringUtils.isNotBlank(dqdm)&&StringUtils.isNotBlank(jglx)){
            try {
                if (jglx.substring(0,1).equals("E")&&!dqdm.substring(0,3).equals("000")){
                    result = true;
                    tempMsg.append("境外被投资机构的地区代码应该为000开头" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                result = true;
                tempMsg.append("境外被投资机构的地区代码应该为000开头" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2074")&&StringUtils.isNotBlank(dqdm)&&StringUtils.isNotBlank(jglx)){
            try {
                if (!jglx.substring(0,1).equals("E")&&dqdm.substring(0,3).equals("000")){
                    result = true;
                    tempMsg.append("境内被投资机构的地区代码不应该为000开头" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                result = true;
                tempMsg.append("境内被投资机构的地区代码不应该为000开头" + SysConstants.spaceStr);
                e.printStackTrace();
            }
        }

        return result;
    }

    private boolean checkXclgqtzxxRowGqlx(List<String> checknumList, Xclgqtzxx xclgqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String gqlx = xclgqtzxx.getGqlx();//股权类型
        if (checknumList.contains("JS0050")&& StringUtils.isNotBlank(gqlx)){
            if (!ZqFieldUtils.gqlxList.contains(gqlx)){
                tempMsg.append("存量股权投资信息表中的股权类型需在符合要求的值域范围内" + SysConstants.spaceStr);
                result = true;
            }
        }
        return result;
    }


    private boolean checkXclgqtzxxRowSjrq(List<String> checknumList, Xclgqtzxx xclgqtzxx, StringBuffer tempMsg) {
        boolean result = false;
        String sjrq = xclgqtzxx.getSjrq();//数据日期
        if (StringUtils.isNotBlank(sjrq)){
            if (sjrq.length()==10){
                boolean b1 = CheckUtil.checkDate(sjrq, "yyyy-MM-dd");
                if (b1){
                    if (sjrq.compareTo("1800-01-01")<0 || sjrq.compareTo("2100-12-31")>0){
                        tempMsg.append("数据日期需晚于1800-01-01早于2100-12-31" + SysConstants.spaceStr);
                        result = true;
                    }
                }else {
                    tempMsg.append("数据日期不符合yyyy-MM-dd格式" + SysConstants.spaceStr);
                    result = true;
                }
            }else {
                tempMsg.append("数据日期长度不等于10" + SysConstants.spaceStr);
                result = true;
            }
        }else {
            tempMsg.append("数据日期不能为空" + SysConstants.spaceStr);
            result = true;
        }
        return result;
    }


}