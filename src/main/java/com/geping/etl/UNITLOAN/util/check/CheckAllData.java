package com.geping.etl.UNITLOAN.util.check;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import com.geping.etl.UNITLOAN.entity.baseInfo.*;
import com.geping.etl.UNITLOAN.entity.report.Xclgrdkxx;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.util.*;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.geping.etl.UNITLOAN.entity.report.Xcldkxx;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @Author: wangzd
 * @Date: 15:21 2020/6/10
 */
public class CheckAllData {

    //币种代码
    private static List<String> currencyList;
    //行政区划代码
    private static List<String> areaList;
    //国家代码
    private static List<String> countryList;
    //大行业代码
    private static List<String> aindustryList;

    //C0017-行业中类代码
    private static List<String> bindustryList;
    @Autowired
    private CustomSqlUtil customSqlUtil;

    @Autowired
    private XCheckRuleService xCheckRuleService;

    //金融机构（分支机构）基础信息表.金融机构代码
    public static List<String> fzList;

    //金融机构（法人）基础信息表.金融机构代码
    public static List<String> frList;
    //存量单位贷款信息
    public static void checkXcldkxx(CustomSqlUtil customSqlUtil,List<String> xcldkxx1List,Xcldkxx xcldkxx, LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId) {
        boolean isError = false;
        StringBuffer tempMsg = new StringBuffer("");
        String nullError = "不能为空";
        String lengthError = "超过限制长度";
        String spaceStr = "|";
        //#金融机构代码

//      isError = CheckUtil.grantNullAndLengthAndStr(xcldkxx.getFinanceorgcode(), 18, tempMsg, isError, "金融机构代码");
        if(StringUtils.isNotBlank(xcldkxx.getFinanceorgcode())){
            if (xcldkxx1List.contains("JS0299")){
                if(xcldkxx.getFinanceorgcode().length()!=18){
                    tempMsg.append("金融机构代码长度应为18" + spaceStr);
                    isError = true;
                }
            }
            if (xcldkxx1List.contains("JS0291")){
                if(CheckUtil.checkStr(xcldkxx.getFinanceorgcode())){
                    tempMsg.append("金融机构代码字段内容中不得出现“？”、“！” 、“^” 、“*”其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }

            if (fzList == null) {
                String sql = "select distinct finorgcode from xjrjgfz";
                fzList = customSqlUtil.executeQuery(sql);
            }
            if (frList == null) {
                String sql = "select distinct finorgcode from xjrjgfrbaseinfo";
                frList = customSqlUtil.executeQuery(sql);
            }
            if(xcldkxx1List.contains("JS1887")) {
                if(!fzList.contains(xcldkxx.getFinanceorgcode()) && !frList.contains(xcldkxx.getFinanceorgcode())) {
                    tempMsg.append("金融机构代码必须在金融机构（法人）基础信息表.金融机构代码或者金融机构（分支机构）基础信息表.金融机构代码中存在" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (xcldkxx1List.contains("JS0996")){
                tempMsg.append("金融机构代码不能为空" + spaceStr);
                isError = true;
            }
        }

        //#金融机构地区代码
        if (StringUtils.isNotBlank(xcldkxx.getFinanceorgareacode())) {
            if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
            if (xcldkxx1List.contains("JS0089")) {
                if (!areaList.contains(xcldkxx.getFinanceorgareacode())) {
                    tempMsg.append("金融机构地区代码不在规定的代码范围内" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (xcldkxx1List.contains("JS0998")){  tempMsg.append("金融机构地区代码不能为空" + spaceStr);isError = true; }
        }

        //#贷款合同编码
        if (StringUtils.isNotBlank(xcldkxx.getContractcode())){
            if (xcldkxx.getContractcode().length()>100){
                if (xcldkxx1List.contains("JS0303")){   tempMsg.append("贷款合同编码字符长度不能超过100" + spaceStr);isError = true;  }
            }
            if (xcldkxx1List.contains("JS0295")){
                if(CheckUtil.checkStr(xcldkxx.getContractcode())){
                    tempMsg.append("贷款合同编码字段内容中不得出现“？”、“！” 、“^” 、“*”其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (xcldkxx1List.contains("JS1007")){ tempMsg.append("贷款合同编码不能为空" + spaceStr);isError = true; }
        }
        //查重

        //#币种
        if (currencyList==null) currencyList = customSqlUtil.getBaseCode(BaseCurrency.class);

        if (xcldkxx1List.contains("JS1013")){
            if (StringUtils.isBlank(xcldkxx.getCurrency())){
                tempMsg.append("币种不能为空" + spaceStr);
                isError = true;
            }
        }
        if (StringUtils.isNotBlank(xcldkxx.getCurrency())){
            if (xcldkxx1List.contains("JS0095")){
                isError = CheckUtil.checkCurrency(xcldkxx.getCurrency(), currencyList, customSqlUtil, tempMsg, isError, "币种");
            }
        }
//        isError = CheckUtil.checkCurrency(xcldkxx.getCurrency(), currencyList, customSqlUtil, tempMsg, isError, "币种");

        //#贷款财政扶持方式
        if (xcldkxx1List.contains("JS0098")){
            isError = CheckUtil.checkLoanczfcfs(xcldkxx.getLoanfinancesupport(), tempMsg, isError, "贷款财政扶持方式");
        }

        //#贷款产品类别
        if (StringUtils.isNotBlank(xcldkxx.getProductcetegory())) {
            if (!ArrayUtils.contains(CheckUtil.dkcplb, xcldkxx.getProductcetegory())) {
                if (xcldkxx1List.contains("JS0093")){
                    tempMsg.append("贷款产品类别需在贷款品种码值中是否采集为是对应的最底层值域范围内，且不能为F021开头的个人消费贷款" + spaceStr);
                    isError = true;
                }
            }
            if ("F03".equals(xcldkxx.getProductcetegory())) {
                if (xcldkxx1List.contains("JS2054")){
                    tempMsg.append("贷款产品不能出现F03拆借" + spaceStr);
                    isError = true;
                }
            }
        }
        else{
            if (xcldkxx1List.contains("JS1008")){
                tempMsg.append("贷款产品类别不能为空" + spaceStr);
                isError = true;
            }
        }

        if (StringUtils.isNotBlank(xcldkxx.getIsgreenloan())){
            if (xcldkxx1List.contains("JS0084")){
                //#借款人国民经济部门
                isError = CheckUtil.checkEconamicSector(xcldkxx.getIsgreenloan(), Arrays.asList(CheckUtil.gmjjbm3), tempMsg, isError, "借款人国民经济部门");
            }

            if (xcldkxx.getIsgreenloan().startsWith("E")) {
                if (!xcldkxx.getBrrowerindustry().equals("200")) {
                    if (xcldkxx1List.contains("JS1934"))
                    {
                        tempMsg.append("借款人国民经济部门为E开头的非居民部门，则借款人行业必须为200-境外" + spaceStr);
                        isError = true;
                    }
                }
            }

            if (!xcldkxx.getIsgreenloan().equals("D01")&&StringUtils.isNotBlank(xcldkxx.getIsgreenloan())&&!xcldkxx.getIsgreenloan().startsWith("E")) {
                if (StringUtils.isNotBlank(xcldkxx.getBrrowerindustry())&&xcldkxx.getBrrowerindustry().equals("200")) {
                    if (xcldkxx1List.contains("JS1935")) {
                        tempMsg.append("借款人国民经济部门不为E开头的，则借款人行业不能为200-境外" + spaceStr);
                        isError = true;
                    }
                }
            }
        }else {
            if (xcldkxx1List.contains("JS1001")){
                tempMsg.append("借款人国民经济部门不能为空" + spaceStr);
                isError = true;
            }
        }




        if(StringUtils.isBlank(xcldkxx.getGuaranteemethod())){
            if (xcldkxx1List.contains("JS1645")){
                tempMsg.append("贷款担保方式不能为空" + spaceStr);
                isError = true;
            }

        }else {
            if (xcldkxx1List.contains("JS0099")){
                //#贷款担保方式
                isError = CheckUtil.checkdkdbfs(xcldkxx.getGuaranteemethod(), tempMsg, isError, "贷款担保方式");
            }
        }

        if (xcldkxx1List.contains("JS1645")){
       if(StringUtils.isNotBlank(xcldkxx.getProductcetegory())&&xcldkxx.getProductcetegory().length()>2){
           String abc = xcldkxx.getProductcetegory().substring(0, 2);
           String abd[] = abc.split(",");
           if (!ArrayUtils.contains(abd, CheckUtil.dklb)) {
               if (!StringUtils.isNotBlank(xcldkxx.getGuaranteemethod())&&StringUtils.isNotBlank(xcldkxx.getProductcetegory())) {
                       tempMsg.append("当贷款产品类别为资产类贷款时，贷款担保方式不能为空" + spaceStr);
                       isError = true;
               }
           }
         }
        }


            //数据日期
//        if(StringUtils.isNotBlank(xcldkxx.getSjrq())){
//            boolean b=CheckUtil.checkDate(xcldkxx.getSjrq(),"yyyy-MM-dd");
//            if(b){
//                if(xcldkxx.getSjrq().compareTo("1900-01-01")<0||xcldkxx.getSjrq().compareTo("2100-12-31")>0){
//                    tempMsg.append("数据日期必须满足日期格式为：YYYY-MM-DD且日期范围在1900-01-01到2100-12-31之间" + spaceStr);
//                    isError = true;
//                }
//
//            }else {
//                tempMsg.append("数据日期不符合yyyy-MM-dd格式" + spaceStr);
//                isError = true;
//            }
//
//        }else {
//            tempMsg.append("数据日期" + nullError + spaceStr);
//            isError = true;
//        }


        //#贷款到期日期
        //数据日期
//        isError = CheckUtil.nullAndDate(xcldkxx.getSjrq(), tempMsg, isError, "数据日期",false);



        if (StringUtils.isNotBlank(xcldkxx.getLoanenddate())) {
            boolean b = CheckUtil.checkDate(xcldkxx.getLoanenddate(), "yyyy-MM-dd");
            if (b) {
                if (xcldkxx.getLoanenddate().compareTo("1800-01-01") >= 0 && xcldkxx.getLoanenddate().compareTo("2100-12-31") <= 0) {

                    if (xcldkxx.getLoanstatus().equals("LS02") && StringUtils.isNotBlank(xcldkxx.getExtensiondate())) {
                        if (xcldkxx.getLoanenddate().compareTo(xcldkxx.getExtensiondate()) >= 0) {
                            if (xcldkxx1List.contains("JS2426")){
                                tempMsg.append("当贷款展期到期日期不为空且贷款状态为LS02-展期时，贷款到期日期应小于贷款展期到期日期" + spaceStr);
                                isError = true;
                            }
                        }
                    }

                    if (xcldkxx.getLoanstatus().equals("LS04") && StringUtils.isNotBlank(xcldkxx.getExtensiondate())) {
//                        if (xcldkxx.getLoanstatus().equals("LS02") && StringUtils.isNotBlank(xcldkxx.getExtensiondate())) {
                            if (xcldkxx.getLoanstatus().compareTo(xcldkxx.getExtensiondate()) <= 0) {
                                if (xcldkxx1List.contains("JS2614")){
                                    tempMsg.append("当贷款展期到期日期不为空且贷款状态为LS04-缩期时，贷款到期日期应大于贷款展期到期日期" + spaceStr);
                                    isError = true;
                                }
                            }
                        }
//                    }
                } else {
                    if (xcldkxx1List.contains("JS0311")){
                        tempMsg.append("贷款到期日期需早于1800-01-01晚于2100-12-31" + spaceStr);
                        isError = true;
                    }
                }

            } else {
                if (xcldkxx1List.contains("JS0311")){
                    tempMsg.append("贷款到期日期不符合yyyy-MM-dd格式" + spaceStr);
                    isError = true;
                }
            }
        }
         else {
             if (xcldkxx1List.contains("JS1011")){
                 tempMsg.append("贷款到期日期" + nullError + spaceStr);
                 isError = true;
             }
        }

        //#贷款定价基准类型
        if (StringUtils.isNotBlank(xcldkxx.getFixpricetype())) {
            if (xcldkxx.getFixpricetype().length() == 4) {
                if (!ArrayUtils.contains(CheckUtil.loanjzlx, xcldkxx.getFixpricetype())) {
                    if (xcldkxx1List.contains("JS0097")){
                        tempMsg.append("贷款定价基准类型不在符合要求的值域范围内" + spaceStr);
                        isError = true;
                    }
                }
            } else {
                tempMsg.append("贷款定价基准类型长度不等于4" + spaceStr);
                isError = true;
            }
        } else {
            if (xcldkxx1List.contains("JS1018")){
                tempMsg.append("贷款定价基准类型" + nullError + spaceStr);
                isError = true;
            }
        }
        //#贷款发放日期

            if (StringUtils.isNotBlank(xcldkxx.getLoanstartdate())) {
                    boolean b1 = CheckUtil.checkDate(xcldkxx.getLoanstartdate(), "yyyy-MM-dd");
                    if (b1) {
                        if (xcldkxx.getLoanstartdate().compareTo("1800-01-01") >0 && xcldkxx.getLoanstartdate().compareTo("2100-12-31") < 0){

                            if(StringUtils.isNotBlank(xcldkxx.getExtensiondate())){
                                if (StringUtils.isNotBlank(xcldkxx.getLoanenddate()) && xcldkxx.getLoanstartdate().compareTo(xcldkxx.getLoanenddate()) <= 0) {

                                    if (StringUtils.isNotBlank(xcldkxx.getExtensiondate()) &&
                                            xcldkxx.getLoanstartdate().compareTo(xcldkxx.getExtensiondate()) > 0) {
                                        if (xcldkxx1List.contains("JS2425"))
                                        {
                                            tempMsg.append("贷款发放日期应小于等于贷款展期到期日期" + spaceStr);
                                            isError = true;
                                        }
                                    }
                                }

                            }
                            if(StringUtils.isNotBlank(xcldkxx.getLoanenddate()))
                            {
                                if(xcldkxx.getLoanstartdate().compareTo(xcldkxx.getLoanenddate())>0) {
                                    if (xcldkxx1List.contains("JS2424"))
                                    {
                                        tempMsg.append("贷款发放日期应小于等于贷款到期日期" + spaceStr);
                                        isError = true;
                                    }
                                }
                            }
                            if(StringUtils.isNotBlank(xcldkxx.getSjrq())){
                                if(xcldkxx.getLoanstartdate().compareTo(xcldkxx.getSjrq())>0){
                                    if (xcldkxx1List.contains("JS2423"))
                                    {
                                        tempMsg.append("贷款发放日期应小于等于数据日期" + spaceStr);
                                        isError = true;
                                    }
                                }
                            }

                        }else {
                            if (xcldkxx1List.contains("JS0310")) {
                                tempMsg.append("贷款发放日期需早于1800-01-01晚于2100-12-31" + spaceStr);
                                isError = true;
                            }

                        }

                    } else {
                        if (xcldkxx1List.contains("JS0310")) {
                            tempMsg.append("贷款发放日期不符合yyyy-MM-dd格式" + spaceStr);
                            isError = true;
                        }
                    }
            }else {
                if (xcldkxx1List.contains("JS1010")){
                    tempMsg.append("贷款发放日期" + nullError + spaceStr);
                    isError = true;
                }
            }



        //#贷款借据编码
//        isError = CheckUtil.grantNullAndLengthAndStr2(xcldkxx.getReceiptcode(), 100, tempMsg, isError, "贷款借据编码");
        if (StringUtils.isNotBlank(xcldkxx.getReceiptcode())){
            if (CheckUtil.checkStr(xcldkxx.getReceiptcode())){
                if (xcldkxx1List.contains("JS0294"))
                {
                    tempMsg.append("贷款借据编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+ spaceStr); isError = true;
                }
            }
            if (xcldkxx.getReceiptcode().length()>100){
                if (xcldkxx1List.contains("JS0302"))
                {
                    tempMsg.append("贷款借据编码字符长度不能超过100" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (xcldkxx1List.contains("JS1006"))
            {
                tempMsg.append("贷款借据编码" +nullError+ spaceStr);
                isError = true;
            }
        }



        //#贷款余额
        if (StringUtils.isNotBlank(xcldkxx.getReceiptbalance())) {
            if (xcldkxx.getReceiptbalance().length() > 20 || !CheckUtil.checkDecimal(xcldkxx.getReceiptbalance(), 2)) {
                if (xcldkxx1List.contains("JS0305"))
                {
                    tempMsg.append("贷款余额总长度不能超过20位,小数位应保留2位" + spaceStr);
                    isError = true;
                }
            }
            if (Double.valueOf(xcldkxx.getReceiptbalance()) <= 0) {
                if (xcldkxx1List.contains("JS2431"))
                {
                    tempMsg.append("贷款余额应大于0" + spaceStr);
                    isError = true;
                }
            }

        } else {
            if (xcldkxx1List.contains("JS1014")){  tempMsg.append("贷款余额" + nullError + spaceStr);isError = true;}
        }


        //#贷款余额折人民币
        if (StringUtils.isNotBlank(xcldkxx.getReceiptcnybalance())) {
//            贷款余额折人民币一般应在1000元至100亿元之间
            if (Double.valueOf(xcldkxx.getReceiptcnybalance())<1000 || Double.valueOf(xcldkxx.getReceiptcnybalance())> 10000000000L){
                if (xcldkxx1List.contains("JS2771"))
                {
                    tempMsg.append("贷款余额折人民币一般应在1000元至100亿元之间" + spaceStr);
                    isError = true;
                }
            }


            if (xcldkxx.getReceiptcnybalance().length() > 20 || !CheckUtil.checkDecimal(xcldkxx.getReceiptcnybalance(), 2)) {
                if (xcldkxx1List.contains("JS0306"))
                {
                    tempMsg.append("贷款余额折人民币总长度不能超过20位,小数位应保留2位" + spaceStr);
                    isError = true;
                }
            }
            if (Double.valueOf(xcldkxx.getReceiptcnybalance()) <= 0) {
                if (xcldkxx1List.contains("JS2432"))
                {
                    tempMsg.append("贷款余额折人民币>0" + spaceStr);
                    isError = true;
                }
            }

            if (xcldkxx.getCurrency().equals("CNY")) {
                if (!xcldkxx.getReceiptbalance().equals(xcldkxx.getReceiptcnybalance())) {
                    if (xcldkxx1List.contains("JS2690"))
                    {
                        tempMsg.append("币种为人民币的，贷款余额折人民币应该与贷款余额的值相等" + spaceStr);
                        isError = true;
                    }
                }
            }
        } else {
            if (xcldkxx1List.contains("JS1015")) { tempMsg.append("贷款余额折人民币" + nullError + spaceStr);isError = true;}

        }
        //#贷款利率重新定价日

        isError = nullAndDateCldkxx(xcldkxx.getLoaninterestrepricedate(),xcldkxx1List,tempMsg, isError, "贷款利率重新定价日", false);


        if (StringUtils.isNotBlank(xcldkxx.getLoaninterestrepricedate())) {

            if(StringUtils.isNotBlank(xcldkxx.getLoanstartdate())) {
                if (xcldkxx.getLoaninterestrepricedate().compareTo(xcldkxx.getLoanstartdate()) < 0) {
                    if (xcldkxx1List.contains("JS2366"))
                    {
                        tempMsg.append("贷款利率重新定价日应大于等于贷款发放日期" + spaceStr);isError = true;
                    }
                }
                if (StringUtils.isNotBlank(xcldkxx.getExtensiondate()) && "RF02".equals(xcldkxx.getInterestisfixed())) {
                    if (xcldkxx.getLoaninterestrepricedate().compareTo(xcldkxx.getExtensiondate()) > 0) {
                        if (xcldkxx1List.contains("JS2427"))
                        {
                            tempMsg.append("当贷款展期到期日期不为空且为浮动利率贷款时，贷款利率重新定价日应小于等于贷款展期到期日期" + spaceStr);
                            isError = true;
                        }
                    }
                }
                if ((!StringUtils.isNotBlank(xcldkxx.getExtensiondate()))&& "RF02".equals(xcldkxx.getInterestisfixed())) {
                    if (xcldkxx.getLoaninterestrepricedate().compareTo(xcldkxx.getLoanenddate()) > 0) {
                        if (xcldkxx1List.contains("JS2429"))
                        {
                            tempMsg.append("当贷款展期到期日期为空且为浮动利率贷款时，贷款利率重新定价日应小于等于贷款到期日期" + spaceStr);
                            isError = true;
                        }
                    }
                }
                if (StringUtils.isNotBlank(xcldkxx.getExtensiondate()) && "RF01".equals(xcldkxx.getInterestisfixed())) {
                    if (!xcldkxx.getLoaninterestrepricedate().equals(xcldkxx.getExtensiondate())) {
                        if (xcldkxx1List.contains("JS2428"))
                        {
                            tempMsg.append("当贷款展期到期日期不为空且为固定利率贷款时，贷款利率重新定价日应等于贷款展期到期日期" + spaceStr);
                            isError = true;
                        }
                    }

                }
                if (!StringUtils.isNotBlank(xcldkxx.getExtensiondate()) && "RF01".equals(xcldkxx.getInterestisfixed())) {
                    if (!xcldkxx.getLoaninterestrepricedate().equals(xcldkxx.getLoanenddate())) {
                        if (xcldkxx1List.contains("JS2430"))
                        {
                            tempMsg.append("当贷款展期到期日期为空且为固定利率贷款时，贷款利率重新定价日应等于贷款到期日期" + spaceStr);
                            isError = true;
                        }
                    }
                }

            }


        }




        //#贷款展期到期日期 ========================待定  因为找不到资产负债类型字段
        if (StringUtils.isNotBlank(xcldkxx.getExtensiondate())) {
                boolean b = CheckUtil.checkDate(xcldkxx.getExtensiondate(), "yyyy-MM-dd");
                if (b) {
                    if (xcldkxx.getExtensiondate().compareTo("1800-01-01") >= 0 && xcldkxx.getExtensiondate().compareTo("2100-12-31") <= 0) {

                    } else {
                        if (xcldkxx1List.contains("JS0312")){tempMsg.append("贷款展期到期日期需晚于1800-01-01早于2100-12-31" + spaceStr);isError = true;}
                    }
                } else {
                    if (xcldkxx1List.contains("JS0312")){ tempMsg.append("贷款展期到期日期不符合yyyy-MM-dd格式" + spaceStr);isError = true;}
                }

        } else {
            if ("LS02".equals(xcldkxx.getLoanstatus())){
                if(StringUtils.isNotBlank(xcldkxx.getProductcetegory())&&xcldkxx.getProductcetegory().length()>2){
                    String ccd = xcldkxx.getProductcetegory().substring(0, 2);
                    String[] cce = ccd.split(",");
                    if(!ArrayUtils.contains(cce, CheckUtil.dklb)){
                        if(!StringUtils.isNotBlank(xcldkxx.getExtensiondate())){
                            if (xcldkxx1List.contains("JS1644"))
                            {
                                tempMsg.append("当贷款状态为展期时，贷款展期到期日期不能为空" + spaceStr);
                                isError = true;
                            }
                        }
                    }
                }
            }
        }


        //#贷款质量



        if (StringUtils.isNotBlank(xcldkxx.getLoanquality())) {

            if (!ArrayUtils.contains(CheckUtil.dkzl, xcldkxx.getLoanquality())) {
                if (xcldkxx1List.contains("JS0101")){  tempMsg.append("贷款质量不符合要求的值域范围内" + spaceStr);isError = true; }
            }


        } else if(StringUtils.isNotBlank(xcldkxx.getProductcetegory())&&xcldkxx.getProductcetegory().length()>2){
            String sbr = xcldkxx.getProductcetegory().substring(0, 2);
            String[] sbs = sbr.split(",");
            if (!ArrayUtils.contains(sbs, CheckUtil.dklb)) {
                if (!StringUtils.isNotBlank(xcldkxx.getLoanquality())) {
                    if (xcldkxx1List.contains("JS1646"))
                    {
                        tempMsg.append("当贷款产品类别为资产类贷款时，贷款质量不能为空" + spaceStr);
                        isError = true;
                    }
                }
            }

        } else {
//            tempMsg.append("贷款质量" + nullError + spaceStr);
//            isError = true;
        }




        //#贷款状态
        if (StringUtils.isNotBlank(xcldkxx.getLoanstatus())) {
            if (xcldkxx.getLoanstatus().length() == 4) {
                if (!ArrayUtils.contains(CheckUtil.loanstatus, xcldkxx.getLoanstatus())) {
                    if (xcldkxx1List.contains("JS0102"))
                    {
                        tempMsg.append("贷款状态不符合要求的值域范围内" + spaceStr);
                        isError = true;
                    }
                }
            } else {
                tempMsg.append("贷款状态长度不等于4" + spaceStr);
                isError = true;
            }


            if(StringUtils.isNotBlank(xcldkxx.getSjrq())){
                if(StringUtils.isNotBlank(xcldkxx.getExtensiondate())){
                    if(xcldkxx.getSjrq().compareTo(xcldkxx.getExtensiondate())>=1){
                        if(!xcldkxx.getLoanstatus().equals("LS03")){
                            if (xcldkxx1List.contains("JS1950"))
                            {
                                tempMsg.append("逾期贷款的贷款状态应该为LS03-逾期" + spaceStr);
                                isError = true;
                            }
                        }
                    }
                }
                else if(StringUtils.isNotBlank(xcldkxx.getLoanenddate())){
                    if(xcldkxx.getSjrq().compareTo(xcldkxx.getLoanenddate())>=1){
                        if(!xcldkxx.getLoanstatus().equals("LS03")){
                            if (xcldkxx1List.contains("JS1950")) {
                                tempMsg.append("逾期贷款的贷款状态应该为LS03-逾期" + spaceStr);
                                isError = true;
                            }
                        }
                    }
                }
            }

            if(StringUtils.isNotBlank(xcldkxx.getExtensiondate())){
//                if(xcldkxx.getExtensiondate().compareTo(xcldkxx.getSjrq())>=0){
                        if(!xcldkxx.getLoanstatus().equals("LS02") && !xcldkxx.getLoanstatus().equals("LS04") && !xcldkxx.getLoanstatus().equals("LS03")){
                            if (xcldkxx1List.contains("JS1951"))
                            {
                                tempMsg.append("展期贷款的贷款状态应该为LS02-展期 或者 LS04-缩期 或者LS03-逾期" + spaceStr);
                                isError = true;
                            }
                        }
//                }
            }
            if(!StringUtils.isNotBlank(xcldkxx.getExtensiondate())){
                if(xcldkxx.getLoanstatus().equals("LS02")){
                    if (xcldkxx1List.contains("JS2140"))
                    {
                        tempMsg.append("如果贷款展期日期为空，则贷款状态不能为LS02-展期" + spaceStr);
                        isError = true;
                    }
                }
            }


        }else {
            if (xcldkxx1List.contains("JS1024"))
            {
                tempMsg.append("贷款状态" + nullError + spaceStr);
                isError = true;
            }
        }



            //基准利率
             if (StringUtils.isNotBlank(xcldkxx.getBaseinterest())) {
                if (xcldkxx.getBaseinterest().length() > 10 || !CheckUtil.checkDecimal(xcldkxx.getBaseinterest(), 5)) {
                    if (xcldkxx1List.contains("JS0308"))
                    {
                        tempMsg.append("当基准利率不为空时，基准利率总长度不能超过10位，小数位必须为5位" + spaceStr);
                        isError = true;
                    }
                }
                if (CheckUtil.checkPerSign(xcldkxx.getInterestisfixed())) {
                    if (xcldkxx1List.contains("JS0297")) {    tempMsg.append("利率水平不能包含‰或%" + spaceStr);isError = true;}
                }

            }

            if ("RF02".equals(xcldkxx.getInterestisfixed()))
            {
                if (StringUtils.isNotBlank(xcldkxx.getBaseinterest())) {
                    if ((new BigDecimal(xcldkxx.getBaseinterest()).compareTo(BigDecimal.ZERO) < 0 || new BigDecimal(xcldkxx.getBaseinterest()).compareTo(new BigDecimal("30")) > 0)) {
                        if (xcldkxx1List.contains("JS2434"))
                        {
                            tempMsg.append("当为浮动利率贷款时，基准利率应大于等于0且小于等于30" + spaceStr);
                            isError = true;
                        }
                    }
                } else {
                    if (xcldkxx1List.contains("JS1019")) { tempMsg.append("当为浮动利率贷款时，基准利率不能为空" + spaceStr);isError = true;}

                }

            } else if ("RF01".equals(xcldkxx.getInterestisfixed())) {
                if (StringUtils.isNotBlank(xcldkxx.getBaseinterest())) {
                    if (xcldkxx1List.contains("JS2157")) { tempMsg.append("利率类型为固定利率的，基准利率必须为空" + spaceStr);isError = true; }

                }

            }

        //#借款人地区代码
        if (StringUtils.isNotBlank(xcldkxx.getBrrowerareacode()))
        {

//            if (xcldkxx.getBrrowerareacode().length() == 6) {
                if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
                if (countryList == null) countryList = customSqlUtil.getBaseCode(BaseCountry.class);
                if (!areaList.contains(xcldkxx.getBrrowerareacode()) &&
                        !countryList.contains(xcldkxx.getBrrowerareacode())) {
                    if (xcldkxx1List.contains("JS0090")) {  tempMsg.append("借款人地区代码不在符合要求的值域范围内" + spaceStr);isError = true;}
                }
//            } else {
//                tempMsg.append("借款人地区代码长度不等于6" + spaceStr);
//                isError = true;
//            }

            if(StringUtils.isNotBlank(xcldkxx.getIsgreenloan())){
                if (xcldkxx.getIsgreenloan().startsWith("E")) {
                    if (!xcldkxx.getBrrowerareacode().startsWith("000")) {
                        if (xcldkxx1List.contains("JS1936"))
                        {
                            tempMsg.append("借款人国民经济部门为E开头的非居民部门，则借款人地区代码应为000+对应的3位数字国别代码" + spaceStr);
                            isError = true;
                        }
                    }
                } else {
                    if (xcldkxx.getBrrowerareacode().startsWith("000")) {
                        if (xcldkxx1List.contains("JS1937"))
                        {
                            tempMsg.append("借款人国民经济部门不是E开头的非居民部门，则借款人地区代码不能为000开头" + spaceStr);
                            isError = true;
                        }
                    }
                }
            }


            if(StringUtils.isNotBlank(xcldkxx.getBrrowerindustry())){

                if (xcldkxx.getBrrowerindustry().startsWith("200")) {
                    if (!xcldkxx.getBrrowerareacode().startsWith("000")) {
                        if (xcldkxx1List.contains("JS2136"))
                        {
                            tempMsg.append("借款人行业为境外的，借款人地区代码应该以000开头" + spaceStr);
                            isError = true;
                        }
                    }
                } else {
                    if (xcldkxx.getBrrowerareacode().startsWith("000")) {
                        if (xcldkxx1List.contains("JS2137"))
                        {
                            tempMsg.append("借款人行业为境内的，借款人地区代码不能以000开头" + spaceStr);
                            isError = true;
                        }
                    }
                }
            }
            /*JDBCUtils.getConnection();
            String query4 ="select * from xcldkxx a inner join xftykhx b on  a.brroweridnum=b.customercode and a.sjrq=b.sjrq where a.brrowerareacode != b.regareacode ='"+xcldkxx.getFinanceorgareacode()+"'and a.checkstatus='0'" ;
            ResultSet result4 =JDBCUtils.Query(query4);
            if(result4!=null){
                tempMsg.append("借款人地区代码与非同业单位客户基础信息的地区代码应该一致" + spaceStr);
                isError = true;

            }
            JDBCUtils.close();*/

        }
        else {
            if (xcldkxx1List.contains("JS1003")) { tempMsg.append("借款人地区代码" + nullError + spaceStr);isError = true; }
        }

        //#贷款实际投向


        if (StringUtils.isNotBlank(xcldkxx.getLoanactualdirection())) {
            if (xcldkxx.getLoanactualdirection().length() == 4) {
                if (bindustryList == null) bindustryList = customSqlUtil.getBaseCode(BaseBindustry.class);
                if (!bindustryList.contains(xcldkxx.getLoanactualdirection())||"1000".equals(xcldkxx.getLoanactualdirection())) {
                    if (xcldkxx1List.contains("JS0094"))
                    {
                        tempMsg.append("贷款实际投向代码不在符合要求的值域范围内" + spaceStr);
                        isError = true;
                    }
                }

            } else {
                if (xcldkxx1List.contains("JS0094")) { tempMsg.append("贷款实际投向代码长度不等于4" + spaceStr);isError = true;}
            }
        } else if(StringUtils.isNotBlank(xcldkxx.getProductcetegory())&&xcldkxx.getProductcetegory().length()>2)
        {
            String wer = xcldkxx.getProductcetegory().substring(0, 3);
            if (!ArrayUtils.contains(CheckUtil.dklb,wer)) {
                if(StringUtils.isBlank(xcldkxx.getLoanactualdirection())) {
                    if (xcldkxx1List.contains("JS1009"))
                    {
                        tempMsg.append("当贷款产品类别为资产类贷款时，贷款实际投向不能为空" + spaceStr);
                        isError = true;
                    }
                }
            }

        }
//        else {
//            tempMsg.append("贷款实际投向代码" + nullError + spaceStr);
//            isError = true;
//        }


        //#借款人行业
        if (StringUtils.isNotBlank(xcldkxx.getBrrowerindustry())) {
            if (xcldkxx.getBrrowerindustry().length() == 3){
                if (aindustryList == null) aindustryList = customSqlUtil.getBaseCode(BaseAindustry.class);
                if (!aindustryList.contains(xcldkxx.getBrrowerindustry())) {
                    if (xcldkxx1List.contains("JS0088"))
                    {
                        tempMsg.append("借款人行业不在行业标准大类范围内" + spaceStr);
                        isError = true;
                    }
                }
            } else {
                tempMsg.append("借款人行业长度不为3" + spaceStr);
                isError = true;
            }
        } else {
            if (xcldkxx1List.contains("JS1002")) {  tempMsg.append("借款人行业" + nullError + spaceStr);isError = true;}

        }

        //#借款人证件代码
//        isError = CheckUtil.grantNullAndLengthAndStr2(xcldkxx.getBrroweridnum(), 60, tempMsg, isError, "借款人证件代码");
        if (StringUtils.isNotBlank(xcldkxx.getBrroweridnum())){
            if (xcldkxx.getBrroweridnum().length()<=60){
                if (CheckUtil.checkStr(xcldkxx.getBrroweridnum())){
                    if (xcldkxx1List.contains("JS0293"))
                    {
                        tempMsg.append("借款人证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;
                    }
                }
            }else {
                if (StringUtils.isNotBlank(xcldkxx.getBrrowerareacode())) {
                    if (xcldkxx.getBrroweridnum().length() > 60) {
                        if (xcldkxx1List.contains("JS0301"))
                        {
                            tempMsg.append("借款人证件代码字符长度不能超过60" + spaceStr);
                            isError = true;
                        }
                    }
                }
//                tempMsg.append("借款人证件代码字段超过限制长度60|");isError=true;
            }

        }else {
            if (xcldkxx1List.contains("JS1000")) {  tempMsg.append("借款人证件代码不能为空" + spaceStr);isError = true;}
        }
        //#查重在外层写#end
        //#内部机构号
//        isError = CheckUtil.grantNullAndLengthAndStr2(xcldkxx.getFinanceorginnum(), 30, tempMsg, isError, "内部机构号");
        if (StringUtils.isNotBlank(xcldkxx.getFinanceorginnum())){
            if (CheckUtil.checkStr(xcldkxx.getFinanceorginnum())){
                if (xcldkxx1List.contains("JS0292"))
                {
                    tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }
            if (xcldkxx.getFinanceorginnum().length()>30){
                if (xcldkxx1List.contains("JS0300"))
                {
                    tempMsg.append("内部机构号字符长度不能超过30" + spaceStr);
                    isError = true;
                }
            }

        }else {
            if (xcldkxx1List.contains("JS0997")){tempMsg.append("内部机构号不能为空" + spaceStr);isError = true;}
        }

        //#利率是否固定
        if (StringUtils.isNotBlank(xcldkxx.getInterestisfixed())){
            if (xcldkxx1List.contains("JS0096"))
            {
                isError = CheckUtil.equalNullAndCode(xcldkxx.getInterestisfixed(), 4, Arrays.asList(CheckUtil.llsfgd), tempMsg, isError, "利率是否固定");
            }
        }else {
            if (xcldkxx1List.contains("JS1016"))
            {
                tempMsg.append("利率是否固定不能为空" + spaceStr);
                isError = true;
            }
        }


        //#利率水平
        if (StringUtils.isNotBlank(xcldkxx.getInterestislevel())) {
            if (CheckUtil.checkPerSign(xcldkxx.getInterestislevel())) {
                if (xcldkxx1List.contains("JS0296"))
                {
                    tempMsg.append("利率水平不能包含‰或%" + spaceStr);
                    isError = true;
                }
            }
             if (!CheckUtil.checkPerSign(xcldkxx.getInterestislevel())){
                 if (new BigDecimal(xcldkxx.getInterestislevel()).compareTo(BigDecimal.ZERO) < 0||new BigDecimal(xcldkxx.getInterestislevel()).compareTo(new BigDecimal("30")) >0) {
                     if (xcldkxx1List.contains("JS2433"))
                     {
                         tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
                         isError = true;
                     }
                 }

             }

            if (xcldkxx.getInterestislevel().length() <= 10 && CheckUtil.checkDecimal(xcldkxx.getInterestislevel(), 5)) {

            } else {
                if (xcldkxx1List.contains("JS0307"))
                {
                    tempMsg.append("利率水平总长度不能超过10位，小数位必须为5位" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (xcldkxx1List.contains("JS1017"))
            {
                tempMsg.append("利率水平" + nullError + spaceStr);
                isError = true;
            }
        }

        //#借款人经济成分
        if (StringUtils.isNotBlank(xcldkxx.getInverstoreconomy())) {
            if (xcldkxx.getInverstoreconomy().length() <= 5) {
                if (StringUtils.isNotBlank(xcldkxx.getBrrowerindustry()) && !ArrayUtils.contains(CheckUtil.jkrhy, xcldkxx.getBrrowerindustry()) &&
                        StringUtils.isNotBlank(xcldkxx.getEnterprisescale()) && ArrayUtils.contains(CheckUtil.qygm, xcldkxx.getEnterprisescale())) {
                    if (!ArrayUtils.contains(CheckUtil.qyczrjjcf, xcldkxx.getInverstoreconomy())) {
                        if (xcldkxx1List.contains("JS0091")){
                            tempMsg.append("借款人经济成分不在符合要求的值域范围" + spaceStr);
                            isError = true;
                        }
                    }
                }
            }
        }
        if (StringUtils.isNotBlank(xcldkxx.getBrrowerindustry())&&StringUtils.isNotBlank(xcldkxx.getEnterprisescale())){
            if (xcldkxx.getBrrowerindustry().equals("200") || xcldkxx.getEnterprisescale().equals("CS05")){
                if (StringUtils.isNotBlank(xcldkxx.getInverstoreconomy())){
                    if (xcldkxx1List.contains("JS2813"))
                    {
                        tempMsg.append("当客户为境外非居民以及境内非企业时，借款人经济成分必须为空" + spaceStr);
                        isError = true;
                    }
                }
            }else {
                if (!StringUtils.isNotBlank(xcldkxx.getInverstoreconomy())) {
                    if (xcldkxx1List.contains("JS1004"))
                    {
                        tempMsg.append("当客户不为境外非居民以及境内非企业时，借款人经济成分不能为空" + spaceStr);
                        isError = true;
                    }
                }
            }
        }


        //#借款人企业规模
        if (StringUtils.isNotBlank(xcldkxx.getEnterprisescale())) {
            if (xcldkxx.getEnterprisescale().length() == 4) {
                if (StringUtils.isNotBlank(xcldkxx.getBrrowerindustry()) && !ArrayUtils.contains(CheckUtil.jkrhy, xcldkxx.getBrrowerindustry()) &&
                        !ArrayUtils.contains(CheckUtil.qygm2, xcldkxx.getEnterprisescale())) {
                    if (xcldkxx1List.contains("JS0092"))
                    {
                        tempMsg.append("借款人企业规模不在符合要求的值域范围" + spaceStr);
                        isError = true;
                    }
                }
            } else {
                tempMsg.append("借款人企业规模长度不等于4" + spaceStr);
                isError = true;
            }
        }

        if (StringUtils.isNotBlank(xcldkxx.getBrrowerindustry())){
            if (!xcldkxx.getBrrowerindustry().equals("200")) {
                if (StringUtils.isBlank(xcldkxx.getEnterprisescale())) {
                    if (xcldkxx1List.contains("JS1005"))
                    {
                        tempMsg.append("当客户不为境外非居民时，借款人企业规模不能为空" + spaceStr);
                        isError = true;
                    }
                }

            }

        }



        if(StringUtils.isNotBlank(xcldkxx.getIsgreenloan())){
            if (xcldkxx.getIsgreenloan().startsWith("C")&&!xcldkxx.getIsgreenloan().equals("C99")) {
                if (!ArrayUtils.contains(CheckUtil.qygm, xcldkxx.getEnterprisescale())) {
                    if (xcldkxx1List.contains("JS2626"))
                    {
                        tempMsg.append("借款人国民经济部门为C开头且不是C99的非金融企业部门，则借款人企业规模应该在CS01至CS04范围内");
                        isError = true;
                    }
                }
            }
        }

        //#贷款用途
        if (StringUtils.isNotBlank(xcldkxx.getIssupportliveloan())) {
            if (CheckUtil.checkStr6(xcldkxx.getIssupportliveloan())) {
                if (xcldkxx1List.contains("JS0298"))
                {
                    tempMsg.append("当贷款用途不为空时，贷款用途字段内容中不得出现“？”、“！” 、“^”  。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }
            if (xcldkxx.getIssupportliveloan().length() > 3000) {
                if (xcldkxx1List.contains("JS0304"))
                {
                    tempMsg.append("当贷款用途不为空时，贷款用途字符长度不能超过3000" + spaceStr);
                    isError = true;
                }
            }
            if (StringUtils.isNotBlank(xcldkxx.getProductcetegory())&&xcldkxx.getProductcetegory().length()>2) {
                String qwe = xcldkxx.getProductcetegory().substring(0, 2);
                String Qwe[] = qwe.split(",");
                if (ArrayUtils.contains(Qwe, CheckUtil.dklb)) {
                    if(!StringUtils.isNotBlank(xcldkxx.getIssupportliveloan())){
                        if (xcldkxx1List.contains("JS1550"))
                        {
                            tempMsg.append("当贷款产品类别为资产类贷款时，贷款用途不能为空" + spaceStr);
                            isError = true;
                        }
                    }

                }
             }

        }




        //#借款人国民经济部门
        isError = checkEconamicSector(xcldkxx.getIsgreenloan(),xcldkxx1List, Arrays.asList(CheckUtil.dwdkjkrgmjjbm), tempMsg, isError, "借款人国民经济部门");

        if (StringUtils.isNotBlank(xcldkxx.getIsgreenloan())) {
            if (ArrayUtils.contains(CheckUtil.qygm, xcldkxx.getEnterprisescale())) {
                if (!xcldkxx.getIsgreenloan().startsWith("C") && !xcldkxx.getIsgreenloan().startsWith("B")) {
                    if (xcldkxx1List.contains("JS2625"))
                    {
                        tempMsg.append("借款人企业规模为CS01至CS04的，借款人国民经济部门应该为C开头的非金融企业部门或者B开头的金融机构" + spaceStr);
                        isError = true;
                    }
                }

            }
            if (xcldkxx.getBrrowerareacode().startsWith("000")) {
                if (!xcldkxx.getIsgreenloan().startsWith("E")) {
                    if (xcldkxx1List.contains("JS1928"))
                    {
                        tempMsg.append("借款人地区代码为000开头的，国民经济部门应为E开头的非居民部门" + spaceStr);
                        isError = true;
                    }
                }

            } else {
                if (xcldkxx.getIsgreenloan().startsWith("E")) {
                    if (xcldkxx1List.contains("JS1929"))
                    {
                        tempMsg.append("借款人地区代码不是000开头的，国民经济部门不应为E开头的非居民部门" + spaceStr);
                        isError = true;
                    }
                }
            }

        }

        //#是否首次贷款
        isError = equalNullAndCode(xcldkxx.getIsplatformloan(), xcldkxx1List,1, Arrays.asList(CheckUtil.sfcommon), tempMsg, isError, "是否首次贷款");

        //#借款人证件类型
        if (StringUtils.isNotBlank(xcldkxx.getIsfarmerloan())) {
            if (!ArrayUtils.contains(CheckUtil.khzjlx, xcldkxx.getIsfarmerloan())) {
                if (xcldkxx1List.contains("JS0083")){
                    tempMsg.append("借款人证件类型不在符合要求的最底层值域范围内" + spaceStr);
                    isError = true;
                }
            }

              if (StringUtils.isNotBlank(xcldkxx.getBrrowerareacode())){
                  if (!xcldkxx.getBrrowerareacode().startsWith("000") && xcldkxx.getIsfarmerloan().equals("A02")) {
                      if (StringUtils.isNotBlank(xcldkxx.getBrroweridnum())&&xcldkxx.getBrroweridnum().length() != 9) {
                          if (xcldkxx1List.contains("JS2786")) {
                              tempMsg.append("境内客户且借款人证件类型为A02的，借款人证件代码字符长度应该为9位" + spaceStr);
                              isError = true;
                          }
                      }
                  }

                  if (!xcldkxx.getBrrowerareacode().startsWith("000") && xcldkxx.getIsfarmerloan().equals("A01")) {
                      if (StringUtils.isNotBlank(xcldkxx.getBrroweridnum())&&xcldkxx.getBrroweridnum().length() != 18) {
                          if (xcldkxx1List.contains("JS0890")) {
                              tempMsg.append("境内客户且借款人证件类型为A01的，借款人证件代码字符长度应该为18位" + spaceStr);
                              isError = true;
                          }
                      }

                  }
                  if (!xcldkxx.getBrrowerareacode().startsWith("000") && !xcldkxx.getIsfarmerloan().equals("A01")) {
                      if (xcldkxx1List.contains("JS2795")) {
                          tempMsg.append("境内客户的借款人证件类型应为A01-统一社会信用代码" + spaceStr);
                          isError = true;
                      }
                  }
              }
        } else {
            if (xcldkxx1List.contains("JS0999")){ tempMsg.append("借款人证件类型不能为空" + spaceStr);isError = true;}
        }
        //#逾期类型  ========================待定  条件冲突
        if (StringUtils.isNotBlank(xcldkxx.getLoanstatus()) && "LS03".equals(xcldkxx.getLoanstatus())) {
            if (StringUtils.isNotBlank(xcldkxx.getOverduetype())) {
                CheckUtil.equalNullAndCode(xcldkxx.getOverduetype(), 2, Arrays.asList(CheckUtil.jylx), tempMsg, isError, "借款人证件类型");
            } else {
                if (xcldkxx1List.contains("JS1025"))
                {
                    tempMsg.append("当贷款状态为逾期时，逾期类型不能为空" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (StringUtils.isNotBlank(xcldkxx.getOverduetype())) {
                if (xcldkxx1List.contains("JS0103")){
                    CheckUtil.equalNullAndCode(xcldkxx.getOverduetype(), 2, Arrays.asList(CheckUtil.jylx), tempMsg, isError, "逾期类型");
                }
            }
        }

        if (StringUtils.isNotBlank(xcldkxx.getOverduetype())&&!xcldkxx.getLoanstatus().equals("LS03")){
            if (xcldkxx1List.contains("JS2783"))
            {
                tempMsg.append("当贷款状态不为逾期时，逾期类型必须为空" + spaceStr);
                isError = true;
            }
        }

        if (isError) {
            if(!errorMsg.containsKey(xcldkxx.getId())) {
                errorMsg.put(xcldkxx.getId(), "贷款合同编码:"+xcldkxx.getContractcode()+"，"+"贷款借据编号:"+xcldkxx.getReceiptcode()+"]->\n");
            }
            String str = errorMsg.get(xcldkxx.getId());
            str = str + tempMsg;
            errorMsg.put(xcldkxx.getId(), str);
            errorId.add(xcldkxx.getId());
        } else {
            if(!errorId.contains(xcldkxx.getId())) {
                rightId.add(xcldkxx.getId());
            }
        }


    }


    //存量个人贷款信息
    public static int checkXclgrdkxx(CustomSqlUtil customSqlUtil, List<String> xclgrdkxx1List, Xclgrdkxx xclgrdkxx,LinkedHashMap<String, String> errorMsg,LinkedHashMap<String, String> errorMsg2,List<String> errorId, List<String> rightId) {
        boolean isError = false;
        StringBuffer tempMsg = new StringBuffer("");
        String nullError = "不能为空";
        String lengthError = "超过限制长度";
        String spaceStr = "|";
        //#金融机构代码

//      isError = CheckUtil.grantNullAndLengthAndStr(xcldkxx.getFinanceorgcode(), 18, tempMsg, isError, "金融机构代码");
        if(StringUtils.isNotBlank(xclgrdkxx.getFinanceorgcode())){
            if (xclgrdkxx1List.contains("JS0910")){
                if(xclgrdkxx.getFinanceorgcode().length()!=18){
                    tempMsg.append("金融机构代码长度应为18" + spaceStr);
                    isError = true;
                }
            }
            if (xclgrdkxx1List.contains("JS0902")){
                if(CheckUtil.checkStr(xclgrdkxx.getFinanceorgcode())){
                    tempMsg.append("金融机构代码字段内容中不得出现“？”、“！” 、“^” 、“*”其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }

            if (fzList == null) {
                String sql = "select distinct finorgcode from xjrjgfz";
                fzList = customSqlUtil.executeQuery(sql);
            }
            if (frList == null) {
                String sql = "select distinct finorgcode from xjrjgfrbaseinfo";
                frList = customSqlUtil.executeQuery(sql);
            }
            if(xclgrdkxx1List.contains("JS1887")) {
                if(!fzList.contains(xclgrdkxx.getFinanceorgcode()) && !frList.contains(xclgrdkxx.getFinanceorgcode())) {
                    tempMsg.append("金融机构代码必须在金融机构（法人）基础信息表.金融机构代码或者金融机构（分支机构）基础信息表.金融机构代码中存在" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (xclgrdkxx1List.contains("JS1619")){
                tempMsg.append("金融机构代码不能为空" + spaceStr);
                isError = true;
            }
        }

        //#金融机构地区代码
        if (StringUtils.isNotBlank(xclgrdkxx.getFinanceorgareacode())) {
            if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
            if (xclgrdkxx1List.contains("JS0215")) {
                if (!areaList.contains(xclgrdkxx.getFinanceorgareacode())) {
                    tempMsg.append("金融机构地区代码不在规定的代码范围内" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (xclgrdkxx1List.contains("JS1621")){  tempMsg.append("金融机构地区代码不能为空" + spaceStr);isError = true; }
        }

        //#贷款合同编码
        if (StringUtils.isNotBlank(xclgrdkxx.getContractcode())){
            if (xclgrdkxx.getContractcode().length()>100){
                if (xclgrdkxx1List.contains("JS0914")){   tempMsg.append("贷款合同编码字符长度不能超过100" + spaceStr);isError = true;  }
            }
            if (xclgrdkxx1List.contains("JS0906")){
                if(CheckUtil.checkStr(xclgrdkxx.getContractcode())){
                    tempMsg.append("贷款合同编码字段内容中不得出现“？”、“！” 、“^” 、“*”其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (xclgrdkxx1List.contains("JS1626")){ tempMsg.append("贷款合同编码不能为空" + spaceStr);isError = true; }
        }
        //查重

        //#币种
        if (currencyList==null) currencyList = customSqlUtil.getBaseCode(BaseCurrency.class);

        if (xclgrdkxx1List.contains("JS1631")){
            if (StringUtils.isBlank(xclgrdkxx.getCurrency())){
                tempMsg.append("币种不能为空" + spaceStr);
                isError = true;
            }
        }
        if (StringUtils.isNotBlank(xclgrdkxx.getCurrency())){
            if (xclgrdkxx1List.contains("JS0218")){
                isError = CheckUtil.checkCurrency(xclgrdkxx.getCurrency(), currencyList, customSqlUtil, tempMsg, isError, "币种");
            }
        }
//        isError = CheckUtil.checkCurrency(xcldkxx.getCurrency(), currencyList, customSqlUtil, tempMsg, isError, "币种");

        //#贷款财政扶持方式
        if (xclgrdkxx1List.contains("JS0221")){
            isError = CheckUtil.checkLoanczfcfs(xclgrdkxx.getLoanfinancesupport(), tempMsg, isError, "贷款财政扶持方式");
        }

        //#贷款产品类别
        if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())) {
            if (!ArrayUtils.contains(CheckUtil.dkfsecplb, xclgrdkxx.getProductcetegory())) {
                if (xclgrdkxx1List.contains("JS0217")){
                    tempMsg.append("贷款产品类别需在贷款品种码值中是否采集为是对应的最底层值域范围内" + spaceStr);
                    isError = true;
                }
            }
            if ("F03".equals(xclgrdkxx.getProductcetegory())) {
                if (xclgrdkxx1List.contains("JS2196")){
                    tempMsg.append("贷款产品不能出现F03拆借" + spaceStr);
                    isError = true;
                }
            }
        }
        else{
            if (xclgrdkxx1List.contains("JS1627")){
                tempMsg.append("贷款产品类别不能为空" + spaceStr);
                isError = true;
            }
        }


        if(StringUtils.isNotBlank(xclgrdkxx.getGuaranteemethod())){
            if (xclgrdkxx1List.contains("JS0222")){
                //#贷款担保方式
                isError = CheckUtil.checkdkdbfs(xclgrdkxx.getGuaranteemethod(), tempMsg, isError, "贷款担保方式");
            }
        }

        if (xclgrdkxx1List.contains("JS1617")){
            if(StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&xclgrdkxx.getProductcetegory().length()>2){
                String abc = xclgrdkxx.getProductcetegory().substring(0, 3);
//                String abd[] = abc.split(",");
                if (!ArrayUtils.contains( CheckUtil.dklb,abc)) {
                    if (!StringUtils.isNotBlank(xclgrdkxx.getGuaranteemethod())&&StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())) {
                        tempMsg.append("当贷款产品类别为资产类贷款时，贷款担保方式不能为空" + spaceStr);
                        isError = true;
                    }
                }
            }
        }


        //数据日期
//        if(StringUtils.isNotBlank(xcldkxx.getSjrq())){
//            boolean b=CheckUtil.checkDate(xcldkxx.getSjrq(),"yyyy-MM-dd");
//            if(b){
//                if(xcldkxx.getSjrq().compareTo("1900-01-01")<0||xcldkxx.getSjrq().compareTo("2100-12-31")>0){
//                    tempMsg.append("数据日期必须满足日期格式为：YYYY-MM-DD且日期范围在1900-01-01到2100-12-31之间" + spaceStr);
//                    isError = true;
//                }
//
//            }else {
//                tempMsg.append("数据日期不符合yyyy-MM-dd格式" + spaceStr);
//                isError = true;
//            }
//
//        }else {
//            tempMsg.append("数据日期" + nullError + spaceStr);
//            isError = true;
//        }


        //#贷款到期日期
        //数据日期
//        isError = CheckUtil.nullAndDate(xclgrdkxx.getSjrq(), tempMsg, isError, "数据日期",false);



        if (StringUtils.isNotBlank(xclgrdkxx.getLoanenddate())) {

            boolean b = CheckUtil.checkDate(xclgrdkxx.getLoanenddate(), "yyyy-MM-dd");
            if (b) {
                if (xclgrdkxx.getLoanenddate().compareTo("1800-01-01") >= 0 && xclgrdkxx.getLoanenddate().compareTo("2100-12-31") <= 0) {

                    if (xclgrdkxx.getLoanstatus().equals("LS02") && StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())) {
                        if (xclgrdkxx.getLoanenddate().compareTo(xclgrdkxx.getExtensiondate()) >= 0) {
                            if (xclgrdkxx1List.contains("JS2395")){
                                tempMsg.append("当贷款展期到期日期和贷款到期日期不为空，且贷款状态为LS02-展期时，贷款到期日期应小于贷款展期到期日期" + spaceStr);
                                isError = true;
                            }
                        }
                    }

                    if (StringUtils.isNotBlank(xclgrdkxx.getExtensiondate()) && xclgrdkxx.getLoanstatus().equals("LS04")) {
//                        if (xclgrdkxx.getLoanstatus().equals("LS02") && StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())) {
                            if (xclgrdkxx.getLoanstatus().compareTo(xclgrdkxx.getExtensiondate()) <= 0) {
                                if (xclgrdkxx1List.contains("JS2613")){
                                    tempMsg.append("当贷款展期到期日期和贷款到期日期不为空，且贷款状态为LS04-缩期时，贷款到期日期应大于贷款展期到期日期" + spaceStr);
                                    isError = true;
                                }
                            }
                        }
//                    }
                } else {
                    if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory()) && !ArrayUtils.contains(CheckUtil.tzyw, xclgrdkxx.getProductcetegory())) {
                        if (xclgrdkxx1List.contains("JS0922")) {
                            tempMsg.append("除透支类业务以外的贷款到期日期需早于1800-01-01晚于2100-12-31" + spaceStr);
                            isError = true;
                        }
                    }
                }
            } else {
                if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory()) && !ArrayUtils.contains(CheckUtil.tzyw, xclgrdkxx.getProductcetegory())) {
                    if (xclgrdkxx1List.contains("JS0922")) {
                        tempMsg.append("除透支类业务以外的贷款到期日期不符合yyyy-MM-dd格式" + spaceStr);
                        isError = true;
                    }
                }
            }
        }
        else {
            if (!ArrayUtils.contains(CheckUtil.tzyw,xclgrdkxx.getProductcetegory())  && StringUtils.isNotBlank(xclgrdkxx.getIssupportliveloan()) &&!xclgrdkxx.getIssupportliveloan().contains("线上联合消费贷款")){
                if (xclgrdkxx1List.contains("JS1629")){
                    tempMsg.append("除透支类业务以外的贷款到期日期" + nullError + spaceStr);
                    isError = true;
                }
            }
        }

        //#贷款定价基准类型
        if (StringUtils.isNotBlank(xclgrdkxx.getFixpricetype())) {
            if (xclgrdkxx.getFixpricetype().length() == 4) {
                if (!ArrayUtils.contains(CheckUtil.loanjzlx, xclgrdkxx.getFixpricetype())) {
                    if (xclgrdkxx1List.contains("JS0220")){
                        tempMsg.append("贷款定价基准类型不在符合要求的值域范围内" + spaceStr);
                        isError = true;
                    }
                }
            } else {
                tempMsg.append("贷款定价基准类型长度不等于4" + spaceStr);
                isError = true;
            }
        } else {
            if (xclgrdkxx1List.contains("JS1636")){
                tempMsg.append("贷款定价基准类型" + nullError + spaceStr);
                isError = true;
            }
        }
        //#贷款发放日期

        if (StringUtils.isNotBlank(xclgrdkxx.getLoanstartdate())) {
            boolean b1 = CheckUtil.checkDate(xclgrdkxx.getLoanstartdate(), "yyyy-MM-dd");
            if (b1) {
                if (xclgrdkxx.getLoanstartdate().compareTo("1800-01-01") >0 && xclgrdkxx.getLoanstartdate().compareTo("2100-12-31") < 0){

                    if(StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())){
                      if (StringUtils.isNotBlank(xclgrdkxx.getLoanenddate()) && xclgrdkxx.getLoanstartdate().compareTo(xclgrdkxx.getLoanenddate()) <= 0) {

                            if (StringUtils.isNotBlank(xclgrdkxx.getExtensiondate()) &&
                                    xclgrdkxx.getLoanstartdate().compareTo(xclgrdkxx.getExtensiondate()) > 0) {
                                if (xclgrdkxx1List.contains("JS2394"))
                                {
                                    tempMsg.append("当贷款展期到期日期不为空时，贷款发放日期应小于等于贷款展期到期日期" + spaceStr);
                                    isError = true;
                                }
                            }
                        }

                    }
                    if(StringUtils.isNotBlank(xclgrdkxx.getLoanenddate())) {
                        if(xclgrdkxx.getLoanstartdate().compareTo(xclgrdkxx.getLoanenddate())>0) {
                            if (xclgrdkxx1List.contains("JS2393"))
                            {
                                tempMsg.append("贷款发放日期不为空时，应小于等于贷款到期日期" + spaceStr);
                                isError = true;
                            }
                        }
                    }
                    if(StringUtils.isNotBlank(xclgrdkxx.getSjrq())){
                        if(xclgrdkxx.getLoanstartdate().compareTo(xclgrdkxx.getSjrq())>0){
                            if (xclgrdkxx1List.contains("JS2392"))
                            {
                                tempMsg.append("贷款发放日期不为空时，应小于等于数据日期" + spaceStr);
                                isError = true;
                            }
                        }
                    }

                }else {
                    //透支类除外
                    if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory()) && !xclgrdkxx.getProductcetegory().startsWith("F04")){
                        if (xclgrdkxx1List.contains("JS0921")) {
                            tempMsg.append("除透支类业务以外的贷款发放日期需早于1800-01-01晚于2100-12-31" + spaceStr);
                            isError = true;
                        }
                    }
                }

            } else {
                //透支类除外
                if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory()) && xclgrdkxx.getProductcetegory().length()>2 && !xclgrdkxx.getProductcetegory().startsWith("F04")) {
                    if (xclgrdkxx1List.contains("JS0921")) {
                        tempMsg.append("除透支类业务以外的贷款发放日期不符合yyyy-MM-dd格式" + spaceStr);
                        isError = true;
                    }
                }
            }
        }else {
            if (!ArrayUtils.contains(CheckUtil.tzyw,xclgrdkxx.getProductcetegory())&& StringUtils.isNotBlank(xclgrdkxx.getIssupportliveloan()) && !xclgrdkxx.getIssupportliveloan().contains("线上联合消费贷款")){
                if (xclgrdkxx1List.contains("JS1628")){
                    tempMsg.append("除透支类业务以外的贷款发放日期" + nullError + spaceStr);
                    isError = true;
                }
            }
        }



        //#贷款借据编码
//        isError = CheckUtil.grantNullAndLengthAndStr2(xcldkxx.getReceiptcode(), 100, tempMsg, isError, "贷款借据编码");
        if (StringUtils.isNotBlank(xclgrdkxx.getReceiptcode())){
            if (CheckUtil.checkStr(xclgrdkxx.getReceiptcode())){
                if (xclgrdkxx1List.contains("JS0905"))
                {
                    tempMsg.append("贷款借据编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+ spaceStr); isError = true;
                }
            }
            if (xclgrdkxx.getReceiptcode().length()>100){
                if (xclgrdkxx1List.contains("JS0913"))
                {
                    tempMsg.append("贷款借据编码字符长度不能超过100" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (xclgrdkxx1List.contains("JS1625"))
            {
                tempMsg.append("贷款借据编码" +nullError+ spaceStr);
                isError = true;
            }
        }



        //#贷款余额
        if (StringUtils.isNotBlank(xclgrdkxx.getReceiptbalance())) {
            if (xclgrdkxx.getReceiptbalance().length() > 20 || !CheckUtil.checkDecimal(xclgrdkxx.getReceiptbalance(), 2)) {
                if (xclgrdkxx1List.contains("JS0916"))
                {
                    tempMsg.append("贷款余额总长度不能超过20位,小数位应保留2位" + spaceStr);
                    isError = true;
                }
            }
            if (Double.valueOf(xclgrdkxx.getReceiptbalance()) <= 0) {
                if (xclgrdkxx1List.contains("JS2400"))
                {
                    tempMsg.append("贷款余额应大于0" + spaceStr);
                    isError = true;
                }
            }

        } else {
            if (xclgrdkxx1List.contains("JS1632")){  tempMsg.append("贷款余额" + nullError + spaceStr);isError = true;}
        }


        //#贷款余额折人民币
        if (StringUtils.isNotBlank(xclgrdkxx.getReceiptcnybalance())) {
//            贷款余额折人民币一般应在1000元至100亿元之间
//            if (Double.valueOf(xclgrdkxx.getReceiptcnybalance())<1000 || Double.valueOf(xclgrdkxx.getReceiptcnybalance())> 10000000000L){
//                if (xclgrdkxx1List.contains("JS2771"))
//                {
//                    tempMsg.append("贷款余额折人民币一般应在1000元至100亿元之间" + spaceStr);
//                    isError = true;
//                }
//            }


            if (Double.valueOf(xclgrdkxx.getReceiptcnybalance())>100000000){
                if (xclgrdkxx1List.contains("JS2881")){  tempMsg.append("个人贷款的余额折人民币应不超过1亿" + spaceStr);isError = true;}
            }


            if (xclgrdkxx.getReceiptcnybalance().length() > 20 || !CheckUtil.checkDecimal(xclgrdkxx.getReceiptcnybalance(), 2)) {
                if (xclgrdkxx1List.contains("JS0917"))
                {
                    tempMsg.append("贷款余额折人民币总长度不能超过20位,小数位应保留2位" + spaceStr);
                    isError = true;
                }
            }
            if (Double.valueOf(xclgrdkxx.getReceiptcnybalance()) <= 0) {
                if (xclgrdkxx1List.contains("JS2401"))
                {
                    tempMsg.append("贷款余额折人民币应大于0" + spaceStr);
                    isError = true;
                }
            }

            if (xclgrdkxx.getCurrency().equals("CNY")) {
                if (!xclgrdkxx.getReceiptbalance().equals(xclgrdkxx.getReceiptcnybalance())) {
                    if (xclgrdkxx1List.contains("JS2711"))
                    {
                        tempMsg.append("币种为人民币的，贷款余额折人民币应该与贷款余额的值相等" + spaceStr);
                        isError = true;
                    }
                }
            }
        } else {
            if (xclgrdkxx1List.contains("JS1633")) { tempMsg.append("贷款余额折人民币" + nullError + spaceStr);isError = true;}

        }
        //#贷款利率重新定价日
        isError = nullAndDateCldkxx2(xclgrdkxx,xclgrdkxx1List,tempMsg, isError, "贷款利率重新定价日", false);


        if (StringUtils.isNotBlank(xclgrdkxx.getLoaninterestrepricedate())) {
                if(StringUtils.isNotBlank(xclgrdkxx.getLoanstartdate())) {
//                if (xclgrdkxx.getLoaninterestrepricedate().compareTo(xclgrdkxx.getLoanstartdate()) < 0) {
//                    if (xclgrdkxx1List.contains("JS2366"))
//                    {
//                        tempMsg.append("贷款利率重新定价日应大于等于贷款发放日期" + spaceStr);isError = true;
//                    }
//                }
                    if (StringUtils.isNotBlank(xclgrdkxx.getExtensiondate()) && "RF02".equals(xclgrdkxx.getInterestisfixed())) {
                        if (xclgrdkxx.getLoaninterestrepricedate().compareTo(xclgrdkxx.getExtensiondate()) > 0) {
                            if (xclgrdkxx1List.contains("JS2396"))
                            {
                                tempMsg.append("当贷款展期到期日期不为空和贷款利率重新定价日不为空且为浮动利率贷款时，贷款利率重新定价日应小于等于贷款展期到期日期" + spaceStr);
                                isError = true;
                            }
                        }
                    }
                    if ((!StringUtils.isNotBlank(xclgrdkxx.getExtensiondate()))&& "RF02".equals(xclgrdkxx.getInterestisfixed())) {
                        if (xclgrdkxx.getLoaninterestrepricedate().compareTo(xclgrdkxx.getLoanenddate()) > 0) {
                            if (xclgrdkxx1List.contains("JS2398"))
                            {
                                tempMsg.append("当贷款展期到期日期为空，贷款利率重新定价日不为空且为浮动利率贷款时，贷款利率重新定价日应小于等于贷款到期日期" + spaceStr);
                                isError = true;
                            }
                        }
                    }
                    if (StringUtils.isNotBlank(xclgrdkxx.getExtensiondate()) && "RF01".equals(xclgrdkxx.getInterestisfixed())) {
                        if (!xclgrdkxx.getLoaninterestrepricedate().equals(xclgrdkxx.getExtensiondate())) {
                            if (xclgrdkxx1List.contains("JS2397"))
                            {
                                tempMsg.append("当贷款展期到期日期不为空和贷款利率重新定价日不为空且为固定利率贷款时，贷款利率重新定价日应等于贷款展期到期日期" + spaceStr);
                                isError = true;
                            }
                        }

                    }
                    if (!StringUtils.isNotBlank(xclgrdkxx.getExtensiondate()) && "RF01".equals(xclgrdkxx.getInterestisfixed())) {
                        if (!xclgrdkxx.getLoaninterestrepricedate().equals(xclgrdkxx.getLoanenddate())) {
                            if (xclgrdkxx1List.contains("JS2399"))
                            {
                                tempMsg.append("当贷款展期到期日期为空，贷款利率重新定价日不为空且为固定利率贷款时，贷款利率重新定价日应等于贷款到期日期" + spaceStr);
                                isError = true;
                            }
                        }
                    }

                }
        }




        //#贷款展期到期日期 ========================待定  因为找不到资产负债类型字段
        if (StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())) {
            boolean b = CheckUtil.checkDate(xclgrdkxx.getExtensiondate(), "yyyy-MM-dd");
            if (b) {
                if (xclgrdkxx.getExtensiondate().compareTo("1800-01-01") >= 0 && xclgrdkxx.getExtensiondate().compareTo("2100-12-31") <= 0) {

                } else {
                    if (xclgrdkxx1List.contains("JS0923")){tempMsg.append("贷款展期到期日期需晚于1800-01-01早于2100-12-31" + spaceStr);isError = true;}
                }
            } else {
                if (xclgrdkxx1List.contains("JS0923")){ tempMsg.append("贷款展期到期日期不符合yyyy-MM-dd格式" + spaceStr);isError = true;}
            }

        } else {
            if ("LS02".equals(xclgrdkxx.getLoanstatus())){
                if(StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&xclgrdkxx.getProductcetegory().length()>2){
                    String ccd = xclgrdkxx.getProductcetegory().substring(0, 3);
//                    String[] cce = ccd.split(",");
                    if(!ArrayUtils.contains(CheckUtil.dklb,ccd)){
                        if(!StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())){
                            if (xclgrdkxx1List.contains("JS1615"))
                            {
                                tempMsg.append("当贷款状态为展期时，贷款展期到期日期不能为空" + spaceStr);
                                isError = true;
                            }
                        }
                    }
                }
            }
        }


        //#贷款质量

        if (StringUtils.isNotBlank(xclgrdkxx.getLoanquality())) {

            if (!ArrayUtils.contains(CheckUtil.dkzl, xclgrdkxx.getLoanquality())) {
                if (xclgrdkxx1List.contains("JS0224")){  tempMsg.append("贷款质量不符合要求的值域范围内" + spaceStr);isError = true; }
            }


        } else if(StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&xclgrdkxx.getProductcetegory().length()>2){
            String sbr = xclgrdkxx.getProductcetegory().substring(0, 3);
//            String[] sbs = sbr.split(",");
            if (!ArrayUtils.contains(CheckUtil.dklb,sbr)) {
                if (!StringUtils.isNotBlank(xclgrdkxx.getLoanquality())) {
                    if (xclgrdkxx1List.contains("JS1630"))
                    {
                        tempMsg.append("当贷款产品类别为资产类贷款时，贷款质量不能为空" + spaceStr);
                        isError = true;
                    }
                }
            }

        } else {
//            tempMsg.append("贷款质量" + nullError + spaceStr);
//            isError = true;
        }




        //#贷款状态
        if (StringUtils.isNotBlank(xclgrdkxx.getLoanstatus())) {
            if (xclgrdkxx.getLoanstatus().length() == 4) {
                if (!ArrayUtils.contains(CheckUtil.loanstatus, xclgrdkxx.getLoanstatus())) {
                    if (xclgrdkxx1List.contains("JS0225"))
                    {
                        tempMsg.append("贷款状态不符合要求的值域范围内" + spaceStr);
                        isError = true;
                    }
                }
            } else {
                tempMsg.append("贷款状态长度不等于4" + spaceStr);
                isError = true;
            }


            if(StringUtils.isNotBlank(xclgrdkxx.getSjrq())){
                if(StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())){
                    if(xclgrdkxx.getSjrq().compareTo(xclgrdkxx.getExtensiondate())>=1){
                        if(!xclgrdkxx.getLoanstatus().equals("LS03")){
                            if (xclgrdkxx1List.contains("JS2188"))
                            {
                                tempMsg.append("逾期贷款的贷款状态应该为LS03-逾期" + spaceStr);
                                isError = true;
                            }
                        }
                    }
                }
                else if(StringUtils.isNotBlank(xclgrdkxx.getLoanenddate())){
                    if(xclgrdkxx.getSjrq().compareTo(xclgrdkxx.getLoanenddate())>=1){
                        if(!xclgrdkxx.getLoanstatus().equals("LS03")){
                            if (xclgrdkxx1List.contains("JS2188")) {
                                tempMsg.append("逾期贷款的贷款状态应该为LS03-逾期" + spaceStr);
                                isError = true;
                            }
                        }
                    }
                }
            }

            if(StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())){
//                if(xcldkxx.getExtensiondate().compareTo(xcldkxx.getSjrq())>=0){
                if(!xclgrdkxx.getLoanstatus().equals("LS02") && !xclgrdkxx.getLoanstatus().equals("LS04") && !xclgrdkxx.getLoanstatus().equals("LS03")){
                    if (xclgrdkxx1List.contains("JS2189"))
                    {
                        tempMsg.append("展期贷款的贷款状态应该为LS02-展期 或者 LS04-缩期 或者LS03-逾期" + spaceStr);
                        isError = true;
                    }
                }
//                }
            }
            if(!StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())){
                if(xclgrdkxx.getLoanstatus().equals("LS02")){
                    if (xclgrdkxx1List.contains("JS2200"))
                    {
                        tempMsg.append("如果贷款展期日期为空，则贷款状态不能为LS02-展期" + spaceStr);
                        isError = true;
                    }
                }
            }
             if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&xclgrdkxx.getIsfarmerloan().startsWith("F04")){
                if (!xclgrdkxx.getLoanstatus().equals("LS01") || !xclgrdkxx.getLoanstatus().equals("LS03")){
                    if (xclgrdkxx1List.contains("JS2800"))
                    {
                        tempMsg.append("信用卡透支的贷款状态只能为LS01-正常或LS03-逾期" + spaceStr);
                        isError = true;
                    }
                }
            }
        }else {
            if (xclgrdkxx1List.contains("JS1642"))
            {
                tempMsg.append("贷款状态" + nullError + spaceStr);
                isError = true;
            }
        }



        //基准利率
        if (StringUtils.isNotBlank(xclgrdkxx.getBaseinterest())) {
            if (xclgrdkxx.getBaseinterest().length() > 10 || !CheckUtil.checkDecimal(xclgrdkxx.getBaseinterest(), 5)) {
                if (xclgrdkxx1List.contains("JS0919"))
                {
                    tempMsg.append("当基准利率不为空时，基准利率总长度不能超过10位，小数位必须为5位" + spaceStr);
                    isError = true;
                }
            }
            if (CheckUtil.checkPerSign(xclgrdkxx.getInterestisfixed())) {
                if (xclgrdkxx1List.contains("JS0908")) {    tempMsg.append("当基准利率不为空时，基准利率不能包含‰或%" + spaceStr);isError = true;}
            }

        }

        if ("RF02".equals(xclgrdkxx.getInterestisfixed()))
        {
            if (StringUtils.isNotBlank(xclgrdkxx.getBaseinterest())) {
                if ((new BigDecimal(xclgrdkxx.getBaseinterest()).compareTo(BigDecimal.ZERO) < 0 || new BigDecimal(xclgrdkxx.getBaseinterest()).compareTo(new BigDecimal("30")) > 0)) {
                    if (xclgrdkxx1List.contains("JS2403"))
                    {
                        tempMsg.append("当为浮动利率贷款时，基准利率应大于等于0且小于等于30" + spaceStr);
                        isError = true;
                    }
                }
            } else {
                if (xclgrdkxx1List.contains("JS1637")) { tempMsg.append("当为浮动利率贷款时，基准利率不能为空" + spaceStr);isError = true;}

            }

        } else if ("RF01".equals(xclgrdkxx.getInterestisfixed())) {
            if (StringUtils.isNotBlank(xclgrdkxx.getBaseinterest())) {
                if (xclgrdkxx1List.contains("JS2159")) { tempMsg.append("利率类型为固定利率的，基准利率必须为空" + spaceStr);isError = true; }

            }

        }

        //#借款人地区代码
        if (StringUtils.isNotBlank(xclgrdkxx.getBrrowerareacode()))
        {
//            if (xclgrdkxx.getBrrowerareacode().length() == 6) {
                if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
                if (countryList == null) countryList = customSqlUtil.getBaseCode(BaseCountry.class);
                if (!areaList.contains(xclgrdkxx.getBrrowerareacode()) &&
                        !countryList.contains(xclgrdkxx.getBrrowerareacode())) {
                    if (xclgrdkxx1List.contains("JS0216")) {  tempMsg.append("借款人地区代码不在符合要求的值域范围内" + spaceStr);isError = true;}
                }
//            } else {
//                tempMsg.append("借款人地区代码长度不等于6" + spaceStr);
//                isError = true;
//            }
        }
        else {
            if (xclgrdkxx1List.contains("JS1624")) { tempMsg.append("借款人地区代码" + nullError + spaceStr);isError = true; }
        }


        //#借款人证件代码
//        isError = CheckUtil.grantNullAndLengthAndStr2(xcldkxx.getBrroweridnum(), 60, tempMsg, isError, "借款人证件代码");
        if (StringUtils.isNotBlank(xclgrdkxx.getBrroweridnum())){
            if (xclgrdkxx.getBrroweridnum().length()<=60){
                if (CheckUtil.checkStr(xclgrdkxx.getBrroweridnum())){
                    if (xclgrdkxx1List.contains("JS0904"))
                    {
                        tempMsg.append("借款人证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;
                    }
                }
            }else {
                tempMsg.append("借款人证件代码字段超过限制长度60|");isError=true;
            }
        }else {
            if (xclgrdkxx1List.contains("JS1623")) {  tempMsg.append("借款人证件代码不能为空" + spaceStr);isError = true;}
        }

        if (StringUtils.isNotBlank(xclgrdkxx.getBrroweridnum())) {
            if (xclgrdkxx.getBrroweridnum().length() > 60) {
                if (xclgrdkxx1List.contains("JS0912"))
                {
                    tempMsg.append("借款人证件代码字符长度不能超过60" + spaceStr);
                    isError = true;
                }
            }
        }

        //#查重在外层写#end
        //#内部机构号
//        isError = CheckUtil.grantNullAndLengthAndStr2(xcldkxx.getFinanceorginnum(), 30, tempMsg, isError, "内部机构号");
        if (StringUtils.isNotBlank(xclgrdkxx.getFinanceorginnum())){
            if (CheckUtil.checkStr(xclgrdkxx.getFinanceorginnum())){
                if (xclgrdkxx1List.contains("JS0903"))
                {
                    tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }
            if (xclgrdkxx.getFinanceorginnum().length()>30){
                if (xclgrdkxx1List.contains("JS0911"))
                {
                    tempMsg.append("内部机构号字符长度不能超过30" + spaceStr);
                    isError = true;
                }
            }

        }else {
            if (xclgrdkxx1List.contains("JS1620")){tempMsg.append("内部机构号不能为空" + spaceStr);isError = true;}
        }

        //#利率是否固定
        if (StringUtils.isNotBlank(xclgrdkxx.getInterestisfixed())){
            if (xclgrdkxx1List.contains("JS0219"))
            {
                isError = CheckUtil.equalNullAndCode(xclgrdkxx.getInterestisfixed(), 4, Arrays.asList(CheckUtil.llsfgd), tempMsg, isError, "利率是否固定");
            }
        }else {
            if (xclgrdkxx1List.contains("JS1634"))
            {
                tempMsg.append("利率是否固定不能为空" + spaceStr);
                isError = true;
            }
        }


        //#利率水平
        if (StringUtils.isNotBlank(xclgrdkxx.getInterestislevel())) {
            if (CheckUtil.checkPerSign(xclgrdkxx.getInterestislevel())) {
                if (xclgrdkxx1List.contains("JS0907"))
                {
                    tempMsg.append("利率水平不能包含‰或%" + spaceStr);
                    isError = true;
                }
            }
            if (!CheckUtil.checkPerSign(xclgrdkxx.getInterestislevel())){}
            if (new BigDecimal(xclgrdkxx.getInterestislevel()).compareTo(BigDecimal.ZERO) < 0||new BigDecimal(xclgrdkxx .getInterestislevel()).compareTo(new BigDecimal("30")) >0) {
                if (xclgrdkxx1List.contains("JS2402"))
                {
                    tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
                    isError = true;
                }
            }
            if (xclgrdkxx.getInterestislevel().length() <= 10 && CheckUtil.checkDecimal(xclgrdkxx.getInterestislevel(), 5)) {


            } else {
                if (xclgrdkxx1List.contains("JS0918"))
                {
                    tempMsg.append("利率水平总长度不能超过10位，小数位必须为5位" + spaceStr);
                    isError = true;
                }
            }

        } else {
            if (xclgrdkxx1List.contains("JS1635"))
            {
                tempMsg.append("利率水平" + nullError + spaceStr);
                isError = true;
            }
        }

        //#贷款用途
        if (StringUtils.isNotBlank(xclgrdkxx.getIssupportliveloan())) {
            if (CheckUtil.checkStr6(xclgrdkxx.getIssupportliveloan())) {
                if (xclgrdkxx1List.contains("JS0909"))
                {
                    tempMsg.append("当贷款用途不为空时，贷款用途字段内容中不得出现“？”、“！” 、“^”  。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }
            if (xclgrdkxx.getIssupportliveloan().length() > 3000) {
                if (xclgrdkxx1List.contains("JS0915"))
                {
                    tempMsg.append("当贷款用途不为空时，贷款用途字符长度不能超过3000" + spaceStr);
                    isError = true;
                }
            }
            if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&xclgrdkxx.getProductcetegory().length()>2) {
                String qwe = xclgrdkxx.getProductcetegory().substring(0, 3);
//                String Qwe[] = qwe.split(",");
                if (ArrayUtils.contains(CheckUtil.dklb,qwe)) {
                    if(!StringUtils.isNotBlank(xclgrdkxx.getIssupportliveloan())){
                        if (xclgrdkxx1List.contains("JS1639"))
                        {
                            tempMsg.append("当贷款产品类别为资产类贷款时，贷款用途不能为空" + spaceStr);
                            isError = true;
                        }
                    }

                }
            }

        }




        //#是否首次贷款
        isError = equalNullAndCode(xclgrdkxx.getIsplatformloan(), xclgrdkxx1List,1, Arrays.asList(CheckUtil.sfcommon), tempMsg, isError, "是否首次贷款");

        //#借款人证件类型
        if (StringUtils.isNotBlank(xclgrdkxx.getIsfarmerloan())) {
            if (xclgrdkxx.getIsfarmerloan().equals("A01")&&StringUtils.isNotBlank(xclgrdkxx.getBrroweridnum())&&xclgrdkxx.getBrroweridnum().length() != 18){
                if (xclgrdkxx1List.contains("JS2174")){ tempMsg.append("借款人证件类型为统一社会信用代码的，证件代码长度必须为18位" + spaceStr); isError = true; }
            }


            if (xclgrdkxx.getIsfarmerloan().startsWith("B")){
                if (xclgrdkxx.getIsfarmerloan().equals("B01") || xclgrdkxx.getIsfarmerloan().equals("B08")){
                    if (StringUtils.isNotBlank(xclgrdkxx.getBrroweridnum())&&TuoMinUtils.getB01(xclgrdkxx.getBrroweridnum()).length() != 46){
                        if (xclgrdkxx1List.contains("JS2655")){  tempMsg.append("借款人证件类型为B01-身份证或者B08-临时身份证的，借款人证件代码脱敏后的长度应该为46" + spaceStr);isError = true;}
                    }
                }else {
                    if (StringUtils.isNotBlank(xclgrdkxx.getBrroweridnum())&&TuoMinUtils.getB01otr(xclgrdkxx.getBrroweridnum()).length() != 32){
                        if (xclgrdkxx1List.contains("JS2665")){ tempMsg.append("借款人证件类型为B开头的个人证件且不是B01-身份证和B08-临时身份证的，借款人证件代码脱敏后的长度应该为32"+spaceStr);isError=true;}
                    }
                }
            }
//            441202197603061000
            if (xclgrdkxx.getIsfarmerloan().equals("B01")&&StringUtils.isNotBlank(xclgrdkxx.getBrroweridnum())){
                if (StringUtils.isNotBlank(xclgrdkxx.getSjrq())&&xclgrdkxx.getBrroweridnum().substring(6,14).compareTo(xclgrdkxx.getSjrq().replace("-","")) >=0){
                    if (xclgrdkxx1List.contains("JS2675")){
                        tempMsg.append("证件类型为B01-身份证的，第7-14位的截取结果应该<数据日期"+spaceStr);isError=true;
                    }

                }
            }


            //TODO
            if (!ArrayUtils.contains(CheckUtil.grzjlx, xclgrdkxx.getIsfarmerloan())) {
                if (xclgrdkxx1List.contains("JS0214")){
                    tempMsg.append("借款人证件类型不在符合要求的最底层值域范围内" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (xclgrdkxx1List.contains("JS1622")){ tempMsg.append("借款人证件类型不能为空" + spaceStr);isError = true;}
        }
        //#逾期类型  ========================待定  条件冲突
        if (StringUtils.isNotBlank(xclgrdkxx.getLoanstatus()) && "LS03".equals(xclgrdkxx.getLoanstatus())) {
            if (StringUtils.isNotBlank(xclgrdkxx.getOverduetype())) {
                CheckUtil.equalNullAndCode(xclgrdkxx.getOverduetype(), 2, Arrays.asList(CheckUtil.jylx), tempMsg, isError, "借款人证件类型");
            } else {
                if (xclgrdkxx1List.contains("JS1643"))
                {
                    tempMsg.append("当贷款状态为逾期时，逾期类型不能为空" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (StringUtils.isNotBlank(xclgrdkxx.getOverduetype())) {
                if (xclgrdkxx1List.contains("JS0226")){
                    CheckUtil.equalNullAndCode(xclgrdkxx.getOverduetype(), 2, Arrays.asList(CheckUtil.jylx), tempMsg, isError, "逾期类型");
                }

            }
        }
        //透支业务
        if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&xclgrdkxx.getProductcetegory().startsWith("F04")){
            if (!StringUtils.isNotBlank(xclgrdkxx.getLoanstartdate())){
                if (xclgrdkxx1List.contains("JS2808"))
                {
                    tempMsg.append("透支类业务的贷款发放日期应为空" + spaceStr);
                    isError = true;
                }
            }
            if (!StringUtils.isNotBlank(xclgrdkxx.getLoanenddate())){
                if (xclgrdkxx1List.contains("JS2809"))
                {
                    tempMsg.append("透支类业务的贷款到期日期应为空" + spaceStr);
                    isError = true;
                }
            }
            if (!StringUtils.isNotBlank(xclgrdkxx.getLoaninterestrepricedate())){
                if (xclgrdkxx1List.contains("JS2810"))
                {
                    tempMsg.append("透支类业务的贷款利率重新定价日应为空" + spaceStr);
                    isError = true;
                }
            }
        }


        if (isError || errorMsg.containsKey(xclgrdkxx.getId().toString())) {
            errorMsg2.put(xclgrdkxx.getId().toString(), "贷款合同编码:"+xclgrdkxx.getContractcode()+"，"+"贷款借据编号:"+xclgrdkxx.getReceiptcode()+"]->\n");
            String str = errorMsg.get(xclgrdkxx.getId().toString());
            str = str == null ? "贷款合同编码:" + xclgrdkxx.getContractcode() +"，"+"贷款借据编号:"+xclgrdkxx.getReceiptcode()+ "]->\r\n" : str;
            str = str + tempMsg;
            errorMsg2.put(xclgrdkxx.getId().toString(), str);
            errorId.add(xclgrdkxx.getId().toString());
            return 1;
        } else {
            if(!errorId.contains(xclgrdkxx.getId().toString())) {
                rightId.add(xclgrdkxx.getId().toString());
            }
            return 0;
        }


    }


    public static Boolean nullAndDateCldkxx(String fieldValue,List<String>xcldkxx1List,StringBuffer errorMsg,boolean isError,String fieldName,boolean isHasNull){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()==10){
                boolean b1 = checkDate(fieldValue, "yyyy-MM-dd");
                if (b1){
                    if (fieldValue.compareTo("1800-01-01")<0 && fieldValue.compareTo("2100-12-31")>0)
                    {
                        if (xcldkxx1List.contains("JS0313")){ errorMsg.append(fieldName+"需晚于1800-01-01早于2100-12-31|");isError=true; }
                    }
                }else {
                    if (xcldkxx1List.contains("JS0313")){ errorMsg.append(fieldName+"不符合yyyy-MM-dd格式|");isError=true;}
                }
            }else {
                errorMsg.append(fieldName+"长度不等于10|");isError=true;
            }
        }else {
            if (!isHasNull){
                if (xcldkxx1List.contains("JS1020")){ errorMsg.append(fieldName+"不能为空|");isError=true;}
            }

        }
        return isError;
    }
    public static Boolean nullAndDateCldkxx2(Xclgrdkxx xclgrdkxx,List<String>xcldkxx1List,StringBuffer errorMsg,boolean isError,String fieldName,boolean isHasNull){
        if (StringUtils.isNotBlank(xclgrdkxx.getLoaninterestrepricedate())&&StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&!xclgrdkxx.getProductcetegory().startsWith("F04")){
            if (xclgrdkxx.getLoaninterestrepricedate().length()==10){
                boolean b1 = checkDate(xclgrdkxx.getLoaninterestrepricedate(), "yyyy-MM-dd");
                if (b1){
                    if (xclgrdkxx.getLoaninterestrepricedate().compareTo("1800-01-01")<0 && xclgrdkxx.getLoaninterestrepricedate().compareTo("2100-12-31")>0)
                    {
                            if (xcldkxx1List.contains("JS0924")) {
                                errorMsg.append(fieldName + "除透支类业务以外的需晚于1800-01-01早于2100-12-31|");
                                isError = true;
                            }
                    }
                }else {
                        if (xcldkxx1List.contains("JS0924")) {
                            errorMsg.append(fieldName + "除透支类业务以外的不符合yyyy-MM-dd格式|");
                            isError = true;
                        }

                }
            }else {
                errorMsg.append(fieldName+"长度不等于10|");isError=true;
            }
        }else {
            if (!isHasNull){
                if (!ArrayUtils.contains(CheckUtil.tzyw, xclgrdkxx.getProductcetegory())&& StringUtils.isNotBlank(xclgrdkxx.getIssupportliveloan())  && !xclgrdkxx.getIssupportliveloan().contains("线上联合消费贷款")) {
                    if (xcldkxx1List.contains("JS1638")){ errorMsg.append(fieldName+"除透支类业务以外不能为空|");isError=true;}
                }
            }

        }
        return isError;
    }
    /**
     * 校验日期
     * @param date
     * @return
     */
    public static boolean checkDate(String date,String pattern){
        boolean convertSuccess=true;
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            format.parse(date);
        } catch (Exception e) {
            convertSuccess=false;
        }
        return convertSuccess;
    }

    public static boolean  checkEconamicSector(String fieldValue,List<String>xcldkxx1List, List<String> list, StringBuffer errorMsg, boolean isError, String fieldName) {
        if (StringUtils.isNotBlank(fieldValue)){
            if (!list.contains(fieldValue)){
                if (xcldkxx1List.contains("JS0084")){
                    errorMsg.append(fieldName+"不在规定的代码范围内|");isError=true;
                }
            }
        }else {
            if (xcldkxx1List.contains("JS1001")){
                errorMsg.append(fieldName+"不能为空|");isError=true;
            }
        }
        return isError;
    }

    public static Boolean   equalNullAndCode(String fieldValue,List<String>xcldkxx1List, int length, List<String> list,StringBuffer errorMsg, boolean isError, String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()==length){
                if (!list.contains(fieldValue)){
                    if (xcldkxx1List.contains("JS0100") || xcldkxx1List.contains("JS0223")){
                        errorMsg.append(fieldName+"不在规定的代码范围内|");isError=true;
                    }
                }
            }else {
                errorMsg.append(fieldName+"长度不等于"+length+"|");isError=true;
            }
        }else {
            if (xcldkxx1List.contains("JS1022") || xcldkxx1List.contains("JS1640")){
                errorMsg.append(fieldName+"不能为空|");isError=true;
            }
        }
        return isError;
    }
}

