package com.geping.etl.UNITLOAN.util;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class JDBCUtils {
	private static String driver = null;
	private static String url = null;
	private static String user = null;
	private static String pwd = null;// oracle数据库的用户密码
	private static PreparedStatement sta = null;
	private static ResultSet rs = null;
	private static Connection conn = null;

	public static Connection getConnection(){ // 1.加载驱动程序
		Properties props = new Properties();
		try {	
		
			props.load(com.geping.etl.UNITLOAN.util.JDBCUtils.class.getClassLoader().getResourceAsStream("intg/jdbc.properties"));
	    	
			driver = props.getProperty("mysql.driverClassName");//数据库驱动
			url = props.getProperty("mysql.url");//数据库
			user = props.getProperty("mysql.username");//用户名
			pwd = props.getProperty("mysql.password");//密码
			
			Class.forName(driver);
			// 2.建立与数据库的连接
			if (conn == null || conn.isClosed()) {
				conn = DriverManager.getConnection(url, user, pwd);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	/**
	 * * @param sql * sql语句 修改 *  @return
	 */
	public static int update(Connection conn,String sql) {
		int count = 0;
		try {
			sta = conn.prepareStatement(sql);
			count = sta.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
		}
		return count;
	}
	
	/**
	 * * @param sql * sql语句 添加 *  @return
	 */
	public static Integer add(Connection conn,String sql) {
		int count = 0;
		try {
			sta = conn.prepareStatement(sql);
			sta.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
		}
		return count;
	}

	/**
	 * * @param sql * sql语句 删除 *  @return
	 */
	public static int delete(Connection conn,String sql) {
		int count = 0;
		try {
			sta = conn.prepareStatement(sql);
			count = sta.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
		}
		return count;
	}

	
	/**
	 *
	 * * @param sql sql语句 * 
	 */
	
	public static ResultSet Query(String sql) {
		try {
			sta = conn.prepareStatement(sql);
			rs = sta.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
		}
		return rs;
	}
	
	/**
	 *
	 * * @param sql sql语句 * 
	 */
	public static ResultSet Query(Connection conn,PreparedStatement sta,String sql) {
		ResultSet rs= null;
		try {
			sta = conn.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rs = sta.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
		}
		return rs;
	}

	/**
	 *分页查询
	 */
	public static ResultSet Query(String sql,int page,int size) {
		try {
			sta = conn.prepareStatement(sql,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			sta.setMaxRows((page+1)*size);
			rs = sta.executeQuery();
			if(page == 0) {
				rs.first();
				rs.previous();
			}else {
				rs.absolute(page*size);
			}			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	
	/** * 关闭资源 */
	public static void close() {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (sta != null) {
					sta.close();
				}
			} catch (SQLException e2) {
				e2.printStackTrace();
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

}
