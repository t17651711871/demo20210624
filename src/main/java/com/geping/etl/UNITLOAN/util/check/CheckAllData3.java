package com.geping.etl.UNITLOAN.util.check;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountryTwo;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseEducation;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseNation;
import com.geping.etl.UNITLOAN.entity.report.Xdkdbht;
import com.geping.etl.UNITLOAN.entity.report.Xftykhx;
import com.geping.etl.UNITLOAN.entity.report.Xgrkhxx;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.UNITLOAN.util.TuoMinUtils;

/**
 * 
 * @author liuweixin
 * @date 2020年8月26日 下午2:14:04
 */
public class CheckAllData3 {

	//判断是否达到校验阈值
	public static boolean limitFlag = false;
	// 币种代码
	private static List<String> currencyList;
	// 行政区划代码
	private static List<String> areaList;
	// 国家代码
	private static List<String> countryList;
	// 大行业代码
	private static List<String> aindustryList;
	// 国籍代码
	private static List<String> countryTwoList;
	// 学历代码
	private static List<String> educationList;
	// 民族代码
	private static List<String> nationList;

	// C0017-行业中类代码
	private static List<String> bindustryList;
	
	//非同业单位客户基础信息表证件代码
	public static List<String> ftyList;
	
	//金融机构（分支机构）基础信息表.金融机构代码
	public static List<String> fzList;
	
	//金融机构（法人）基础信息表.金融机构代码
	public static List<String> frList;

	public static void checkXftykhx(CustomSqlUtil customSqlUtil, Xftykhx xftykhx,
			LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId,
			List<String> checknumList) {
		boolean isError = false;
		StringBuffer tempMsg = new StringBuffer("");

		String nullError = "不能为空";
		String lengthError = "超过限制长度";
		String spaceStr = "|";
		// #获取字段值
		String workareacode = xftykhx.getWorkareacode();
		String workarea = xftykhx.getWorkarea();
		String setupdate = xftykhx.getSetupdate();
		String jobpeoplenum = xftykhx.getJobpeoplenum();
		String depositnum = xftykhx.getDepositnum();
		String countopenbankname = xftykhx.getCountopenbankname();
		String finorgcode = xftykhx.getFinorgcode();
		String mngmestus = xftykhx.getMngmestus();
		String netamt = xftykhx.getNetamt();
		String customercode = xftykhx.getCustomercode();
		String customername = xftykhx.getCustomername();
		String entpczjjcf = xftykhx.getEntpczjjcf();
		String industry = xftykhx.getIndustry();
		String entpmode = xftykhx.getEntpmode();
		String actctrltype = xftykhx.getActctrltype();
		String actctrlidcode = xftykhx.getActctrlidcode();
		String actctrlidtype = xftykhx.getActctrlidtype();
		String actamt = xftykhx.getActamt();
		String actamtcreny = xftykhx.getActamtcreny();
		String isrelation = xftykhx.getIsrelation();
		String islistcmpy = xftykhx.getIslistcmpy();
		String fistdate = xftykhx.getFistdate();
		String creditamt = xftykhx.getCreditamt();
		String usedamt = xftykhx.getUsedamt();
		String regarea = xftykhx.getRegarea();
		String regareacode = xftykhx.getRegareacode();
		String regamt = xftykhx.getRegamt();
		String regamtcreny = xftykhx.getRegamtcreny();
		String totalamt = xftykhx.getTotalamt();
		String sjrq = xftykhx.getSjrq();

		// #客户信用级别总等级数
		if (StringUtils.isNotBlank(workareacode)) {
			if (checknumList.contains("JS0608")) {
				if(!CheckUtil.checkZhenshu(workareacode)) {
					tempMsg.append("客户信用级别总等级数字符长度不能超过3位的整数" + spaceStr);
					isError = true;	
				}
				
			}
			
			if (checknumList.contains("JS2355")) {
				if (new BigDecimal(workareacode).compareTo(BigDecimal.ZERO) != 1) {
					tempMsg.append("客户信用级别总等级数应大于0" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1549")) {
				tempMsg.append("客户信用级别总等级数" + nullError + spaceStr);
				isError = true;
			}
		}
		// #营业收入
		if (StringUtils.isNotBlank(workarea)) {
			if (checknumList.contains("JS0610")) {
				if (workarea.length() > 20 || !CheckUtil.checkDecimal(workarea, 2)) {
					tempMsg.append("营业收入总长度不能超过20位,小数位应保留2位" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS2873")) {
				if(StringUtils.isNotBlank(entpmode) && (entpmode.equals("CS03") || entpmode.equals("CS04"))) {
					if(new BigDecimal(workarea).compareTo(new BigDecimal("10000000000")) == 1) {
						tempMsg.append("营业收入不为空时，小型微型企业的营业收入应不超过100亿元" + spaceStr);
						isError = true;
					}
				}
			}
			if(checknumList.contains("JS2877")) {
				if(StringUtils.isNotBlank(entpmode)){
					if(!entpmode.equals("CS03") && !entpmode.equals("CS04")) {
						if(new BigDecimal(workarea).compareTo(new BigDecimal("50000000000")) == 1) {
							tempMsg.append("营业收入不为空时，大型中型及其他类型企业的营业收入应不超过500亿元" + spaceStr);
							isError = true;
						}
					}
				}else {
					if(new BigDecimal(workarea).compareTo(new BigDecimal("50000000000")) == 1) {
						tempMsg.append("营业收入不为空时，大型中型及其他类型企业的营业收入应不超过500亿元" + spaceStr);
						isError = true;
					}
				}
			}
		} else {
			if (checknumList.contains("JS1547")) {
				tempMsg.append("营业收入" + nullError + spaceStr);
				isError = true;
			}
		}
		// isError = CheckUtil.grantNullAndLengthAndStr(workarea, 400, tempMsg, isError,
		// "营业收入");
		// #成立日期
		if (StringUtils.isNotBlank(setupdate)) {
			if (checknumList.contains("JS0619")) {
				if (setupdate.length() == 10) {
					boolean b1 = CheckUtil.checkDate(setupdate, "yyyy-MM-dd");
					if (b1) {
						if (setupdate.compareTo("1800-01-01") < 0 || setupdate.compareTo("2100-12-31") > 0) {
							tempMsg.append("成立日期需晚于1800-01-01早于2100-12-31" + spaceStr);
							isError = true;
						}
					} else {
						tempMsg.append("成立日期不符合yyyy-MM-dd格式" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("成立日期长度不等于10" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2226")) {
				if (StringUtils.isNotBlank(sjrq)) {
					if (setupdate.compareTo(sjrq) > 0) {
						tempMsg.append("成立日期应小于等于数据日期");
						isError = true;
					}
				}
			}
		} else {
			if (checknumList.contains("JS1275")) {
				tempMsg.append("成立日期" + nullError + spaceStr);
				isError = true;
			}

		}
		// isError = CheckUtil.nullAndDate(setupdate, tempMsg, isError, "成立日期", false);
		// #从业人员数
		if (StringUtils.isNotBlank(jobpeoplenum)) {
			if (checknumList.contains("JS0616")) {
				if (!CheckUtil.checkeCyryPattern(jobpeoplenum)) {
					tempMsg.append("从业人员数应为整数" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS0616")) {
				if (jobpeoplenum.length() > 10) {
					tempMsg.append("从业人员数字符长度不能超过10位" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2222")) {
				if (new BigDecimal(jobpeoplenum).compareTo(BigDecimal.ZERO) != 1 || new BigDecimal(jobpeoplenum).compareTo(new BigDecimal("1000000")) == 1) {
					tempMsg.append("从业人员数应该大于0小于等于100万" + spaceStr);
					isError = true;
				}
			}
			
		}else {
			if (checknumList.contains("JS2222")) {
				tempMsg.append("从业人员数不能为空" + spaceStr);
				isError = true;
			}
		}
		// else {
		// tempMsg.append("从业人员数" + nullError + spaceStr);isError = true;
		// }
		// #基本存款账号
		if (StringUtils.isNotBlank(depositnum)) {
			if (checknumList.contains("JS0599")) {
				if (depositnum.length() > 60) {
					tempMsg.append("基本存款账号字符长度不能超过60" + spaceStr);
					isError = true;

				}
			}
			if (checknumList.contains("JS0592")) {
				if (CheckUtil.checkStr2(depositnum)) {
					tempMsg.append("基本存款账号不能包含特殊字符" + spaceStr);
					isError = true;
				}
			}

		}
		// isError = CheckUtil.grantLengthAndStr2(depositnum, 60, tempMsg, isError,
		// "基本存款账号");
		// #基本账户开户行名称
		if (StringUtils.isNotBlank(countopenbankname)) {
			if (checknumList.contains("JS0602")) {
				if (depositnum.length() > 200) {
					tempMsg.append("基本账户开户行名称字符长度不能超过200" + spaceStr);
					isError = true;

				}
			}
			if (checknumList.contains("JS0601")) {
				if (CheckUtil.checkStr4(countopenbankname)) {
					tempMsg.append("基本账户开户行名称不能包含特殊字符" + spaceStr);
					isError = true;
				}
			}
		}
		// isError = CheckUtil.grantLengthAndStr4(countopenbankname, 200, tempMsg,
		// isError, "基本账户开户行名称");
		// #金融机构代码
		if (StringUtils.isNotBlank(finorgcode)) {			
			if (finorgcode.length() == 18) {

			} else {
				if (checknumList.contains("JS0595")) {
					tempMsg.append("金融机构代码字符长度应该为18位" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS0590")) {
				if (CheckUtil.checkStr(finorgcode)) {
					tempMsg.append("金融机构代码不能包含空格和特殊字符" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1258")) {
				tempMsg.append("金融机构代码" + nullError + spaceStr);
				isError = true;
			}
		}
		// #金融机构代码+客户证件代码唯一 查重
		// #经营状态
		if (StringUtils.isNotBlank(mngmestus)) {
			if (checknumList.contains("JS0182")) {
				if (!Arrays.asList(CheckUtil.jyzt).contains(mngmestus)) {
					tempMsg.append("经营状态不在规定的代码范围内" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1274")) {
				tempMsg.append("经营状态" + nullError + spaceStr);
				isError = true;
			}
		}
		// isError = CheckUtil.equalNullAndCode(mngmestus, 2,
		// Arrays.asList(CheckUtil.jyzt), tempMsg, isError, "经营状态");
		// #经营范围
		if (StringUtils.isNotBlank(netamt)) {
			if (checknumList.contains("JS0607")) {
				if (netamt.length() > 3000) {
					tempMsg.append("经营范围" + lengthError + 3000 + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS0594")) {
				if (CheckUtil.checkStr3(netamt)) {
					tempMsg.append("经营范围不能包含特殊字符" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1546")) {
				tempMsg.append("经营范围" + nullError + spaceStr);
				isError = true;
			}
		}
		// isError = CheckUtil.grantNullAndLengthAndStr3(netamt, 250, tempMsg, isError,
		// "经营范围");
		/*
		 * if (StringUtils.isNotBlank(netamt)) { if (netamt.length() > 20 ||
		 * !CheckUtil.checkDecimal(netamt, 2)) {
		 * tempMsg.append("经营范围总长度不能超过20位,小数位应保留2位" + spaceStr);isError = true; } } else
		 * {
		 * 
		 * tempMsg.append("经营范围" + nullError + spaceStr);isError = true; }
		 */
		// #客户证件代码
		if (StringUtils.isNotBlank(customercode)) {
			if (checknumList.contains("JS0591")) {
				if (CheckUtil.checkStr2(customercode)) {
					tempMsg.append("客户证件代码不能包含空格和特殊字符" + spaceStr);
					isError = true;
				}
			}
			if (StringUtils.isNotBlank(regareacode)) {
				if (checknumList.contains("JS0598")) {
					if (customercode.length() > 60) {
						tempMsg.append("客户证件代码字符长度不能超过60" + spaceStr);
						isError = true;
					}
				}
				if (regareacode.startsWith("000")) {

				} else {
					if(StringUtils.isNotBlank(regamtcreny) && "A01".equals(regamtcreny)) {
						if (checknumList.contains("JS0887")) {
							if (customercode.length() != 18) {
								tempMsg.append("境内客户且客户证件类型为A01的，客户证件代码字符长度应该为18位" + spaceStr);
								isError = true;
							}
						}
					}
					if(StringUtils.isNotBlank(regamtcreny) && "A02".equals(regamtcreny)) {
						if (checknumList.contains("JS2788")) {
							if (customercode.length() != 9) {
								tempMsg.append("境内客户且客户证件类型为A02的，客户证件代码字符长度应该为9位" + spaceStr);
								isError = true;
							}
						}
					}
				}
			}
		} else {
			if (checknumList.contains("JS1260")) {
				tempMsg.append("客户证件代码" + nullError + spaceStr);
				isError = true;
			}
		}

		// #客户名称
		if (StringUtils.isNotBlank(customername)) {
			if (checknumList.contains("JS0597")) {
				if (customername.length() > 200) {
					tempMsg.append("客户名称" + lengthError + 200 + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS0600")) {
				if (CheckUtil.checkStr4(customername)) {
					tempMsg.append("客户名称不能包含特殊字符" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1259")) {
				tempMsg.append("客户名称" + nullError + spaceStr);
				isError = true;
			}
		}
		// isError = CheckUtil.grantNullAndLengthAndStr4(customername, 200, tempMsg,
		// isError, "客户名称");
		// #客户经济成分
		if (StringUtils.isNotBlank(entpczjjcf)) {
			if (checknumList.contains("JS0187")) {
				if (!ArrayUtils.contains(CheckUtil.qyczrjjcf, entpczjjcf)) {
					tempMsg.append("客户经济成分不在符合要求的值域范围" + spaceStr);
					isError = true;
				}
			}
		}
		if (checknumList.contains("JS1548")) {
			if (StringUtils.isNotBlank(industry) && !ArrayUtils.contains(CheckUtil.jkrhy, industry)
					&& StringUtils.isNotBlank(entpmode) && ArrayUtils.contains(CheckUtil.qygm, entpmode)) {
				if (StringUtils.isBlank(entpczjjcf)) {
					tempMsg.append("当客户不为个人、境外非居民以及境内非企业时，客户经济成分" + nullError + spaceStr);
					isError = true;
				}
			}
		}
		if (checknumList.contains("JS2869")) {
			if ((StringUtils.isNotBlank(industry) && ArrayUtils.contains(CheckUtil.jkrhy, industry)) ||
					(StringUtils.isNotBlank(entpmode) && entpmode.equals("CS05"))) {
				if (StringUtils.isNotBlank(entpczjjcf)) {
					tempMsg.append("当客户为境外非居民或境内非企业时，客户经济成分应为空" + spaceStr);
					isError = true;
				}
			}
		}
		// isError = CheckUtil.checkEntpczrjjcf(industry, entpmode, entpczjjcf, tempMsg,
		// isError);
		// #企业规模
		if (StringUtils.isNotBlank(entpmode)) {
			if (checknumList.contains("JS0184")) {
				if (entpmode.length() == 4) {
					if (StringUtils.isNotBlank(industry) && !ArrayUtils.contains(CheckUtil.jkrhy, industry)
							&& !ArrayUtils.contains(CheckUtil.qygm2, entpmode)) {
						tempMsg.append("企业规模不在符合要求的值域范围" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("企业规模长度不等于4" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2644")) {
				if (StringUtils.isNotBlank(actamtcreny)) {
					if (actamtcreny.startsWith("C") && !"C99".equals(actamtcreny)) {
						if (!ArrayUtils.contains(CheckUtil.qygm, entpmode)) {
							tempMsg.append("客户国民经济部门为C开头且不是C99的非金融企业部门，则企业规模应该在CS01至CS04范围内" + spaceStr);
							isError = true;
						}
					}
				}
			}
		} else {
			if (checknumList.contains("JS1277")) {
				if (StringUtils.isNotBlank(industry)) {
					if (!"200".equals(industry)) {
						tempMsg.append("当客户不为境外客户时，企业规模不能为空" + spaceStr);
						isError = true;
					}
				}
			}
		}
		// #客户信用评级 客户信用评级 not in ('01','02')看不懂！！！备注：如果客户信用评级有中文或者英文逗号分隔符，需要根据分隔符拆分后再校验
		if (StringUtils.isNotBlank(actctrltype)) {
			if (actctrltype.length() <= 3 && CheckUtil.checkeCyryPattern(actctrltype)) {
				if (checknumList.contains("JS2354")) {
					if (new BigDecimal(actctrltype).compareTo(new BigDecimal(workareacode)) > 0) {
						tempMsg.append("当客户信用评级不为空时，客户信用评级应小于等于客户信用级别总等级数" + spaceStr);
						isError = true;
					}
				}
			} else {
				if (checknumList.contains("JS0609")) {
					tempMsg.append("客户信用评级字符长度不能超过3位的整数" + spaceStr);
					isError = true;
				}
			}
		}
		/*
		 * if (StringUtils.isNotBlank(actctrltype)){ if (actctrltype.length()<=60){
		 * }else { tempMsg.append("客户信用评级" + lengthError + 60 + spaceStr);isError =
		 * true; } }
		 */
		// #实际控制人证件代码
		if (StringUtils.isNotBlank(actctrlidcode)) {
			if (checknumList.contains("JS0593")) {
				if (CheckUtil.checkStr2(actctrlidcode)) {
					tempMsg.append("实际控制人证件代码不能包含空格和特殊字符" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS0606")) {
				if (actctrlidcode.length() > 400) {
					tempMsg.append("当实际控制人证件代码不为空时，实际控制人证件代码字符长度不能超过400" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS2828")) {
				if(StringUtils.isNotBlank(actctrlidtype) && actctrlidtype.equals("A01")) {
					if(!actctrlidcode.contains(",") && !actctrlidcode.contains("，") && actctrlidcode.length() != 18) {
						tempMsg.append("如果实际控制人证件类型为A01，实际控制人证件代码长度必须为18|");
						isError = true;
					}
				}
			}
			if(checknumList.contains("JS2829")) {
				if(StringUtils.isNotBlank(actctrlidtype) && actctrlidtype.equals("A02")) {
					if(!actctrlidcode.contains(",") && !actctrlidcode.contains("，") && actctrlidcode.length() != 9) {
						tempMsg.append("如果实际控制人证件类型为A02，实际控制人证件代码长度必须为9|");
						isError = true;
					}
				}
			}
			if(checknumList.contains("JS2830")) {
				if(StringUtils.isNotBlank(actctrlidtype) && (actctrlidtype.equals("B01") || actctrlidtype.equals("B08"))) {
					if(!actctrlidcode.contains(",") && !actctrlidcode.contains("，") && TuoMinUtils.getB01(actctrlidcode).length() != 46) {
						tempMsg.append("如果实际控制人证件类型为B01-身份证或B08-临时身份证，实际控制人证件代码长度必须为46|");
						isError = true;
					}
				}
			}
			if(checknumList.contains("JS2831")) {
				if(StringUtils.isNotBlank(actctrlidtype) && actctrlidtype.startsWith("B")) {
					if(!"B01".equals(actctrlidtype) && !"B08".equals(actctrlidtype)) {
						if(!actctrlidcode.contains(",") && !actctrlidcode.contains("，") && TuoMinUtils.getB01otr(actctrlidcode).length() != 32) {
							tempMsg.append("如果实际控制人证件类型为B开头且不是B01-身份证和B08-临时身份证的，实际控制人证件代码长度必须为32|");
							isError = true;
						}
					}
				}
			}
			// B01是身份证涉及脱敏，额外加的校验
			if (StringUtils.isNotBlank(actctrlidtype) && "B01".equals(actctrlidtype)) {
				if (actctrlidcode.length() != 18) {
					tempMsg.append("当实际控制人证件类型为B01-身份证时，实际控制人证件代码字符长度应为18" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1553")) {
				if (StringUtils.isNotBlank(actctrlidtype)) {
					tempMsg.append("实际控制人证件代码" + nullError + spaceStr);
					isError = true;
				}
			}
		}
		// #实际控制人证件类型
		if (StringUtils.isNotBlank(actctrlidtype)) {
			if (actctrlidtype.length() <= 60) {
				if (checknumList.contains("JS0190")) {
					String[] actctrlidtypes = actctrlidtype.split(",");
					for (int i = 0; i < actctrlidtypes.length; i++) {
						if (!ArrayUtils.contains(CheckUtil.bzrzjlx, actctrlidtypes[i])) {
							tempMsg.append("实际控制人证件类型不在符合要求的最底层值域范围内" + spaceStr);
							isError = true;
							break;
						}
					}
				}
			} else {
				if (checknumList.contains("JS0605")) {
					tempMsg.append("实际控制人证件类型" + lengthError + 60 + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1552")) {
				if (StringUtils.isNotBlank(actctrlidcode)) {
					tempMsg.append("实际控制人证件类型" + nullError + spaceStr);
					isError = true;
				}
			}
		}
		// #实收资本
		if (StringUtils.isNotBlank(actamt)) {
			if (checknumList.contains("JS0612")) {
				if (actamt.length() > 20 || !CheckUtil.checkDecimal(actamt, 2)) {
					tempMsg.append("实收资本总长度不能超过20位,小数位应保留2位" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2220")) {
				if (new BigDecimal(actamt).compareTo(BigDecimal.ZERO) < 0) {
					tempMsg.append("当实收资本不为空时，实收资本应大于等于0" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS2875")) {
				if(StringUtils.isNotBlank(entpmode) && (entpmode.equals("CS03") || entpmode.equals("CS04"))) {
					if(new BigDecimal(actamt).compareTo(new BigDecimal("10000000000")) == 1) {
						tempMsg.append("当实收资本不为空时，小型微型企业的实收资本应不超过100亿元" + spaceStr);
						isError = true;
					}
				}
			}
			if(checknumList.contains("JS2879")) {
				if(StringUtils.isNotBlank(entpmode)){
					if(!entpmode.equals("CS03") && !entpmode.equals("CS04")) {
						if(new BigDecimal(actamt).compareTo(new BigDecimal("50000000000")) == 1) {
							tempMsg.append("当实收资本不为空时，大型中型及其他类型企业的实收资本应不超过500亿元" + spaceStr);
							isError = true;
						}
					}
				}else {
					if(new BigDecimal(actamt).compareTo(new BigDecimal("50000000000")) == 1) {
						tempMsg.append("当实收资本不为空时，大型中型及其他类型企业的实收资本应不超过500亿元" + spaceStr);
						isError = true;
					}
				}
			}
		}
		// else {
		// tempMsg.append("实收资本" + nullError + spaceStr);isError = true;
		// }
		// #客户国民经济部门
		if (StringUtils.isNotBlank(actamtcreny)) {
			if (checknumList.contains("JS0188")) {
				if (!Arrays.asList(CheckUtil.gmjjbm2).contains(actamtcreny)) {
					tempMsg.append("客户国民经济部门需在符合要求的值域范围内且不能为个人和金融机构" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1649")) {
				tempMsg.append("客户国民经济部门" + nullError + spaceStr);
				isError = true;
			}
		}
		// isError = CheckUtil.checkEconamicSector(actamtcreny,
		// Arrays.asList(CheckUtil.gmjjbm2), tempMsg, isError,
		// "客户国民经济部门");
		if (StringUtils.isNotBlank(actamtcreny)) {
			if (StringUtils.isNotBlank(regareacode)) {
				if (regareacode.startsWith("000")) {
					if (checknumList.contains("JS2587")) {
						if (!actamtcreny.startsWith("E")) {
							tempMsg.append("客户地区代码为000开头的，国民经济部门应为E开头的非居民部门" + spaceStr);
							isError = true;
						}
					}
				} else {
					if (checknumList.contains("JS2588")) {
						if (actamtcreny.startsWith("E")) {
							tempMsg.append("客户地区代码不是000开头的，国民经济部门不应为E开头的非居民部门" + spaceStr);
							isError = true;
						}
					}
				}
			}
			if (StringUtils.isNotBlank(entpmode)) {
				if (checknumList.contains("JS2643")) {
					if (ArrayUtils.contains(CheckUtil.qygm, entpmode)) {
						if (!actamtcreny.startsWith("C") && !actamtcreny.startsWith("B")) {
							tempMsg.append("企业规模为CS01至CS04的，客户国民经济部门应该为C开头的非金融企业部门或者B开头的金融机构" + spaceStr);
							isError = true;
						}
					}
				}
			}

		}

		// #是否关联方
		if (StringUtils.isNotBlank(isrelation)) {
			if (checknumList.contains("JS0185")) {
				if (!Arrays.asList(CheckUtil.sfcommon).contains(isrelation)) {
					tempMsg.append("是否关联方不在规定的代码范围内" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1280")) {
				tempMsg.append("是否关联方" + nullError + spaceStr);
				isError = true;
			}
		}
		// isError = CheckUtil.equalNullAndCode(isrelation, 1,
		// Arrays.asList(CheckUtil.sfcommon), tempMsg, isError,
		// "是否关联方");
		// #是否上市公司
		if (StringUtils.isNotBlank(islistcmpy)) {
			if (checknumList.contains("JS0180")) {
				if (!Arrays.asList(CheckUtil.sfcommon).contains(islistcmpy)) {
					tempMsg.append("是否上市公司不在规定的代码范围内" + spaceStr);
					isError = true;
				}
			}
		}
		// isError = CheckUtil.equalNullAndCode(islistcmpy, 1,
		// Arrays.asList(CheckUtil.sfcommon), tempMsg, isError,
		// "是否上市公司");
		// #首次建立信贷关系日期
		if (StringUtils.isNotBlank(fistdate)) {
			if (checknumList.contains("JS0618")) {
				if (fistdate.length() == 10) {
					boolean b1 = CheckUtil.checkDate(fistdate, "yyyy-MM-dd");
					if (b1) {
						if (fistdate.compareTo("1800-01-01") < 0 || fistdate.compareTo("2100-12-31") > 0) {
							tempMsg.append("首次建立信贷关系日期需晚于1800-01-01早于2100-12-31" + spaceStr);
							isError = true;
						}
					} else {
						tempMsg.append("首次建立信贷关系日期不符合yyyy-MM-dd格式" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("首次建立信贷关系日期长度不等于10" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2368")) {
				if (StringUtils.isNotBlank(sjrq)) {
					if (fistdate.compareTo(sjrq) > 0) {
						tempMsg.append("首次建立信贷关系日期应小于等于数据日期");
						isError = true;
					}
				}
			}
		}
		// isError = CheckUtil.nullAndDate(fistdate, tempMsg, isError, "首次建立信贷关系日期",
		// true);w
		// #授信额度
		if (StringUtils.isNotBlank(creditamt)) {
			if (checknumList.contains("JS0614")) {
				if (creditamt.length() > 20 || !CheckUtil.checkDecimal(creditamt, 2)) {
					tempMsg.append("授信额度总长度不能超过20位,小数位应保留2位" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2223")) {
				if (new BigDecimal(creditamt).compareTo(BigDecimal.ZERO) == -1) {
					tempMsg.append("当授信额度不为空时，授信额度应大于等于0" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2772")) {
				if (new BigDecimal(creditamt).compareTo(new BigDecimal("100000000000")) == 1) {
					tempMsg.append("授信额度一般不能大于1000亿" + spaceStr);
					isError = true;
				}
			}
		}
		// else {
		// tempMsg.append("授信额度" + nullError + spaceStr);isError = true;
		// }
		// #所属行业
		if (StringUtils.isNotBlank(industry)) {
			if (checknumList.contains("JS0183")) {
				if (industry.length() == 3) {
					if (aindustryList == null)
						aindustryList = customSqlUtil.getBaseCode(BaseAindustry.class);
					if (!aindustryList.contains(industry) && "100".equals(industry)) {
						tempMsg.append("所属行业不在行业标准大类范围内" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("所属行业长度不等于3" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1276")) {
				tempMsg.append("所属行业" + nullError + spaceStr);
				isError = true;
			}
		}
		// #已用额度
		if (StringUtils.isNotBlank(usedamt)) {
			if (checknumList.contains("JS0615")) {
				if (usedamt.length() > 20 || !CheckUtil.checkDecimal(usedamt, 2)) {
					tempMsg.append("已用额度总长度不能超过20位,小数位应保留2位" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2224")) {
				if (new BigDecimal(usedamt).compareTo(BigDecimal.ZERO) == -1) {
					tempMsg.append("当已用额度不为空时，已用额度应大于等于0" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2773")) {
				if (new BigDecimal(usedamt).compareTo(new BigDecimal("100000000000")) == 1) {
					tempMsg.append("已用额度一般不能大于1000亿" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS2225")) {
				if(StringUtils.isNotBlank(creditamt)) {
					 if(new BigDecimal(usedamt).compareTo(new BigDecimal(creditamt)) == 1) {
						 tempMsg.append("当授信额度与已用额度不为空时，已用额度应小于等于授信额度" + spaceStr);isError = true;
					 }
				}
			}
		}
		// #注册地址
		if (StringUtils.isNotBlank(regarea)) {
			if (checknumList.contains("JS0603")) {
				if (regarea.length() > 400) {
					tempMsg.append("注册地址" + lengthError + 400 + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1272")) {
				tempMsg.append("注册地址" + nullError + spaceStr);
				isError = true;
			}
		}
		// #地区代码
		if (StringUtils.isNotBlank(regareacode)) {
				if (checknumList.contains("JS0181")) {
					if (areaList == null)
						areaList = customSqlUtil.getBaseCode(BaseArea.class);
					if (countryList == null)
						countryList = customSqlUtil.getBaseCode(BaseCountry.class);
					if (!areaList.contains(regareacode) && !countryList.contains(regareacode)) {
						tempMsg.append("地区代码不在规定的代码范围内" + spaceStr);
						isError = true;
					}
				}
				if (StringUtils.isNotBlank(industry)) {
					if ("200".equals(industry)) {
						if (checknumList.contains("JS2056")) {
							if (!regareacode.startsWith("000")) {
								tempMsg.append("所属行业为200-境外，则地区代码应该为000开头" + spaceStr);
								isError = true;
							}
						}
					} else {
						if (checknumList.contains("JS2057")) {
							if (regareacode.startsWith("000")) {
								tempMsg.append("所属行业不是200-境外，则地区代码不应该为000开头" + spaceStr);
								isError = true;
							}
						}
					}
				}
		} else {
			if (checknumList.contains("JS1273")) {
				tempMsg.append("地区代码" + nullError + spaceStr);
				isError = true;
			}
		}
		// #注册资本
		if (StringUtils.isNotBlank(regamt)) {
			if (checknumList.contains("JS0611")) {
				if (regamt.length() > 20 || !CheckUtil.checkDecimal(regamt, 2)) {
					tempMsg.append("注册资本总长度不能超过20位,小数位应保留2位" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2219")) {
				if (new BigDecimal(regamt).compareTo(BigDecimal.ZERO) != 1) {
					tempMsg.append("注册资本应大于0" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS2874")) {
				if(StringUtils.isNotBlank(entpmode) && (entpmode.equals("CS03") || entpmode.equals("CS04"))) {
					if(new BigDecimal(regamt).compareTo(new BigDecimal("10000000000")) == 1) {
						tempMsg.append("注册资本不为空时，小型微型企业的注册资本资本应不超过100亿元" + spaceStr);
						isError = true;
					}
				}
			}
			if(checknumList.contains("JS2878")) {
				if(StringUtils.isNotBlank(entpmode)){
					if(!entpmode.equals("CS03") && !entpmode.equals("CS04")) {
						if(new BigDecimal(regamt).compareTo(new BigDecimal("50000000000")) == 1) {
							tempMsg.append("注册资本不为空时，大型中型及其他类型企业的注册资本应不超过500亿元" + spaceStr);
							isError = true;
						}
					}
				}else {
					if(new BigDecimal(regamt).compareTo(new BigDecimal("50000000000")) == 1) {
						tempMsg.append("注册资本不为空时，大型中型及其他类型企业的注册资本应不超过500亿元" + spaceStr);
						isError = true;
					}
				}
			}
		} else {
			if (checknumList.contains("JS1263")) {
				tempMsg.append("注册资本" + nullError + spaceStr);
				isError = true;
			}
		}
		// #客户证件类型
		if (StringUtils.isNotBlank(regamtcreny)) {
			if (checknumList.contains("JS0189")) {
				if (!ArrayUtils.contains(CheckUtil.khzjlx, regamtcreny)) {
					tempMsg.append("客户证件类型不在符合要求的最底层值域范围内" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS2797")) {
				if(StringUtils.isNotBlank(regareacode) && !regareacode.startsWith("000") && !"A01".equals(regamtcreny)) {
					tempMsg.append("境内客户的客户证件类型应该为A01-统一社会信用代码" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1545")) {
				tempMsg.append("客户证件类型" + nullError + spaceStr);
				isError = true;
			}
		}
		// isError = CheckUtil.checkCurrency(regamtcreny, currencyList, customSqlUtil,
		// tempMsg, isError, "客户证件类型");
		// #总资产
		if (StringUtils.isNotBlank(totalamt)) {
			if (checknumList.contains("JS0613")) {
				if (totalamt.length() > 20 || !CheckUtil.checkDecimal(totalamt, 2)) {
					tempMsg.append("总资产总长度不能超过20位,小数位应保留2位" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2221")) {
				if (new BigDecimal(totalamt).compareTo(BigDecimal.ZERO) != 1) {
					tempMsg.append("当总资产不为空时，总资产应大于0" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS2876")) {
				if(StringUtils.isNotBlank(entpmode) && (entpmode.equals("CS03") || entpmode.equals("CS04"))) {
					if(new BigDecimal(totalamt).compareTo(new BigDecimal("10000000000")) == 1) {
						tempMsg.append("当总资产不为空时，小型微型企业的总资产应不超过100亿元" + spaceStr);
						isError = true;
					}
				}
			}
			if(checknumList.contains("JS2880")) {
				if(StringUtils.isNotBlank(entpmode)){
					if(!entpmode.equals("CS03") && !entpmode.equals("CS04")) {
						if(new BigDecimal(totalamt).compareTo(new BigDecimal("50000000000")) == 1) {
							tempMsg.append("当总资产不为空时，大型中型及其他类型企业的总资产应不超过500亿元" + spaceStr);
							isError = true;
						}
					}
				}else {
					if(new BigDecimal(totalamt).compareTo(new BigDecimal("50000000000")) == 1) {
						tempMsg.append("当总资产不为空时，大型中型及其他类型企业的总资产应不超过500亿元" + spaceStr);
						isError = true;
					}
				}
			}
		}
		// #数据日期
		isError = CheckUtil.nullAndDate(sjrq, tempMsg, isError, "数据日期", false);
		// #整表

		if (isError) {
			if (!errorMsg.containsKey(xftykhx.getId())) {
				errorMsg.put(xftykhx.getId(),
						"客户名称:" + xftykhx.getCustomername() + "，" + "客户证件代码:" + xftykhx.getCustomercode() + "]->\r\n");
			}
			String str = errorMsg.get(xftykhx.getId());
			str = str + tempMsg;
			errorMsg.put(xftykhx.getId(), str);
			errorId.add(xftykhx.getId());
		} else {
			if (!errorId.contains(xftykhx.getId())) {
				rightId.add(xftykhx.getId());
			}
		}

	}

	// 单位贷款担保合同信息
	public static void checkXdkdbht(CustomSqlUtil customSqlUtil, Xdkdbht xdkdbht,
			LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId,
			List<String> checknumList) {
		boolean isError = false;
		StringBuffer tempMsg = new StringBuffer("");

		String nullError = "不能为空";
		String lengthError = "超过限制长度";
		String spaceStr = "|";
		// #字段命名
		String gteeidnum = xdkdbht.getGteeidnum();
		String gteeidtype = xdkdbht.getGteeidtype();
		String loancontractcode = xdkdbht.getLoancontractcode();
		String gteecurrency = xdkdbht.getGteecurrency();
		String gteecontractcode = xdkdbht.getGteecontractcode();
		String gteeenddate = xdkdbht.getGteeenddate();
		String gteeamount = xdkdbht.getGteeamount();
		String gteecnyamount = xdkdbht.getGteecnyamount();
		String gteecontracttype = xdkdbht.getGteecontracttype();
		String gteestartdate = xdkdbht.getGteestartdate();
		String financeorgcode = xdkdbht.getFinanceorgcode();
		String financeorginnum = xdkdbht.getFinanceorginnum();
		String transactiontype = xdkdbht.getTransactiontype();
		String pledgerate = xdkdbht.getPledgerate();
		String isgreenloan = xdkdbht.getIsgreenloan();
		String brrowerindustry = xdkdbht.getBrrowerindustry();
		String brrowerareacode = xdkdbht.getBrrowerareacode();
		String enterprisescale = xdkdbht.getEnterprisescale();
		String sjrq = xdkdbht.getSjrq();

		// #担保人企业规模
		if (StringUtils.isNotBlank(enterprisescale)) {
			if (checknumList.contains("JS0087")) {
				if (enterprisescale.length() == 4) {
					if (StringUtils.isNotBlank(enterprisescale)
							&& !ArrayUtils.contains(CheckUtil.qygm2, enterprisescale)) {
						tempMsg.append("担保人企业规模不在符合要求的值域范围" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("担保人企业规模长度不等于4" + spaceStr);
					isError = true;
				}
			}
		}
		if (checknumList.contains("JS1520")) {
			if (StringUtils.isNotBlank(gteeidnum) && !"100".equals(brrowerindustry) && !"200".equals(brrowerindustry)) {
				if (StringUtils.isBlank(enterprisescale)) {
					tempMsg.append("当担保人不为个人和境外非居民时，担保人企业规模不能为空" + spaceStr);
					isError = true;
				}
			}
		}
		if (checknumList.contains("JS2133")) {
			if ("100".equals(brrowerindustry)) {
				if (StringUtils.isNotBlank(enterprisescale)) {
					tempMsg.append("担保人为个人的企业规模应为空" + spaceStr);
					isError = true;
				}
			}
		}
		if (checknumList.contains("JS2630")) {
			if (StringUtils.isNotBlank(isgreenloan)) {
				if (isgreenloan.startsWith("C") && !("C99").equals(isgreenloan)) {
					if (StringUtils.isBlank(enterprisescale) || !ArrayUtils.contains(CheckUtil.qygm, enterprisescale)) {
						tempMsg.append("担保人国民经济部门为C开头且不是C99的非金融企业部门，则担保人企业规模应该在CS01至CS04范围内" + spaceStr);
						isError = true;
					}
				}
			}
		}
		// #担保人地区代码
		if (StringUtils.isNotBlank(brrowerareacode)) {
				if (checknumList.contains("JS0086")) {
					if (areaList == null)
						areaList = customSqlUtil.getBaseCode(BaseArea.class);
					if (countryList == null)
						countryList = customSqlUtil.getBaseCode(BaseCountry.class);
					if (!areaList.contains(brrowerareacode) && !countryList.contains(brrowerareacode)) {
						tempMsg.append("担保人地区代码不在符合要求的值域范围内" + spaceStr);
						isError = true;
					}
				}
				if (StringUtils.isNotBlank(gteeidtype)) {
//					if (checknumList.contains("JS2112")) {
//						if ("B06".equals(gteeidtype) || "B12".equals(gteeidtype) || "B07".equals(gteeidtype)) {
//							if (!"000344".equals(brrowerareacode) && !"000446".equals(brrowerareacode)
//									&& !"000158".equals(brrowerareacode)) {
//								tempMsg.append("担保人证件类型为港澳台证件的，担保人地区代码应为000+对应的港澳台地区代码" + spaceStr);
//								isError = true;
//							}
//						}
//					}
//					if (checknumList.contains("JS2113")) {
//						if ("B06".equals(gteeidtype) || "B12".equals(gteeidtype) || "B07".equals(gteeidtype)
//								|| "B09".equals(gteeidtype) || "B11".equals(gteeidtype)) {
//							if (!brrowerareacode.startsWith("000")) {
//								tempMsg.append("担保人证件类型为境外证件的，担保人地区代码应为000+对应的3位数字国别代码" + spaceStr);
//								isError = true;
//							}
//						}
//					}
//					if (checknumList.contains("JS2116")) {
//						if ("A01".equals(gteeidtype) || "A02".equals(gteeidtype) || "A03".equals(gteeidtype)
//								|| "A04".equals(gteeidtype) || "B01".equals(gteeidtype) || "B02".equals(gteeidtype)
//								|| "B04".equals(gteeidtype) || "B05".equals(gteeidtype) || "B08".equals(gteeidtype)
//								|| "B10".equals(gteeidtype)) {
//							if (brrowerareacode.startsWith("000")) {
//								tempMsg.append("担保人证件类型为境内证件的，担保人地区代码不能为000开头" + spaceStr);
//								isError = true;
//							}
//						}
//					}
				}
				if (StringUtils.isNotBlank(isgreenloan)) {
//					if (isgreenloan.startsWith("E")) {
//						if (checknumList.contains("JS2124")) {
//							if (!brrowerareacode.startsWith("000")) {
//								tempMsg.append("担保人国民经济部门为E开头的非居民部门，则担保人地区代码应为000+对应的3位数字国别代码" + spaceStr);
//								isError = true;
//							}
//						}
//					} else {
//						if (checknumList.contains("JS2125")) {
//							if (brrowerareacode.startsWith("000")) {
//								tempMsg.append("担保人国民经济部门不是E开头的非居民部门，则担保人地区代码不能为000开头" + spaceStr);
//								isError = true;
//							}
//						}
//					}
				}
				if (StringUtils.isNotBlank(brrowerindustry)) {
//					if ("200".equals(brrowerindustry)) {
//						if (checknumList.contains("JS2148")) {
//							if (!brrowerareacode.startsWith("000")) {
//								tempMsg.append("担保人行业为境外的，担保人地区代码应该以000开头" + spaceStr);
//								isError = true;
//							}
//						}
//					} else {
//						if (checknumList.contains("JS2149")) {
//							if (brrowerareacode.startsWith("000")) {
//								tempMsg.append("担保人行业为境内的，担保人地区代码不能以000开头" + spaceStr);
//								isError = true;
//							}
//						}
//					}
				}
		} else {
			if (checknumList.contains("JS1519")) {
				if (StringUtils.isNotBlank(gteeidnum)) {
					tempMsg.append("担保人地区代码" + nullError + spaceStr);
					isError = true;
				}
			}
		}

		// #担保人行业
		if (StringUtils.isNotBlank(brrowerindustry)) {
			if (checknumList.contains("JS0085")) {
				if (brrowerindustry.length() == 3) {
					if (aindustryList == null)
						aindustryList = customSqlUtil.getBaseCode(BaseAindustry.class);
					if (!aindustryList.contains(brrowerindustry) && !"100".equals(brrowerindustry)
							&& !"200".equals(brrowerindustry)) {
						tempMsg.append("担保人行业不在行业标准大类范围内" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("担保人行业不等于3" + spaceStr);
					isError = true;
				}
			}

			if (StringUtils.isNotBlank(isgreenloan)) {
				if (isgreenloan.startsWith("E")) {
					if (checknumList.contains("JS2121")) {
						if (!"200".equals(brrowerindustry)) {
							tempMsg.append("担保人国民经济部门为E开头的非居民部门，则担保人行业必须为200-境外" + spaceStr);
							isError = true;
						}
					}
				} else if ("D01".equals(isgreenloan)) {
					if (checknumList.contains("JS2122")) {
						if (!"100".equals(brrowerindustry)) {
							tempMsg.append("担保人国民经济部门为D01-住户，则担保人行业必须为100-个人" + spaceStr);
							isError = true;
						}
					}
				} else {
					if (checknumList.contains("JS2123")) {
						if ("200".equals(brrowerindustry) || "100".equals(brrowerindustry)) {
							tempMsg.append("担保人国民经济部门不为E开头且不是D01-住户的，则担保人行业不能为100-个人和200-境外" + spaceStr);
							isError = true;
						}
					}
				}
			}
		} else {
			if (checknumList.contains("JS1607")) {
				if (StringUtils.isNotBlank(gteeidnum)) {
					tempMsg.append("担保人行业" + nullError + spaceStr);
					isError = true;
				}
			}

		}

		// #交易类型
		if (StringUtils.isNotBlank(transactiontype)) {
			if (checknumList.contains("JS0123")) {
				String[] strings = transactiontype.split(",");
				for (String s : strings) {
					if (!ArrayUtils.contains(CheckUtil.jylx, s)) {
						tempMsg.append("交易类型不在符合要求的最底层值域范围内" + spaceStr);
						isError = true;
						break;
					}
				}
			}
		} else {
			if (checknumList.contains("JS1062")) {
				tempMsg.append("交易类型" + nullError + spaceStr);
				isError = true;
			}
		}

		// #担保人国民经济部门
		if (StringUtils.isNotBlank(isgreenloan)) {
			if (checknumList.contains("JS0211")) {
				if (!Arrays.asList(CheckUtil.gmjjbm).contains(isgreenloan)) {
					tempMsg.append("若担保人国民经济部门不为空，需在符合要求的最底层值域范围内" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1598")) {
				if (StringUtils.isNotBlank(gteeidnum)) {
					tempMsg.append("担保人国民经济部门不能为空" + spaceStr);
					isError = true;
				}
			}
		}
		// isError = CheckUtil.checkEconamicSector(isgreenloan,
		// Arrays.asList(CheckUtil.gmjjbm), tempMsg, isError, "担保人国民经济部门");
		if (StringUtils.isNotBlank(gteeidtype) && StringUtils.isNotBlank(isgreenloan)) {
			if ("B06".equals(gteeidtype) || "B12".equals(gteeidtype) || "B07".equals(gteeidtype)
					|| "B09".equals(gteeidtype) || "B11".equals(gteeidtype)) {
				if (checknumList.contains("JS2117")) {
					if (!isgreenloan.startsWith("E")) {
						tempMsg.append("担保人证件类型为境外证件的，国民经济部门应为E开头的非居民部门" + spaceStr);
						isError = true;
					}
				}
			} else if ("A01".equals(gteeidtype) || "A02".equals(gteeidtype) || "A03".equals(gteeidtype)
					|| "A04".equals(gteeidtype) || "B01".equals(gteeidtype) || "B02".equals(gteeidtype)
					|| "B04".equals(gteeidtype) || "B05".equals(gteeidtype) || "B08".equals(gteeidtype)
					|| "B10".equals(gteeidtype)) {
				if (checknumList.contains("JS2118")) {
					if (isgreenloan.startsWith("E")) {
						tempMsg.append("担保人证件类型为境内证件的，国民经济部门不应为E开头的非居民部门" + spaceStr);
						isError = true;
					}
				}
			}
			if (checknumList.contains("JS2119")) {
				if (gteeidtype.startsWith("B")) {
					if (!"D01".equals(isgreenloan) && !"E05".equals(isgreenloan)) {
						tempMsg.append("担保人证件类型为B开头的个人证件，担保人国民经济部门应该为D01-住户或者E05-外国居民" + spaceStr);
						isError = true;
					}
				}
			}

		}
		if (checknumList.contains("JS2629")) {
			if (StringUtils.isNotBlank(enterprisescale)) {
				if (ArrayUtils.contains(CheckUtil.qygm, enterprisescale)) {
					if (!isgreenloan.startsWith("C") && !isgreenloan.startsWith("B")) {
						tempMsg.append("担保人企业规模为CS01至CS04的，担保人国民经济部门应该为C开头的非金融企业部门或者B开头的金融机构" + spaceStr);
						isError = true;
					}
				}
			}
		}

		// #金融机构代码
		if (StringUtils.isNotBlank(financeorgcode)) {
			if (checknumList.contains("JS0345")) {
				if (financeorgcode.length() == 18) {

				} else {
					if (checknumList.contains("JS0345")) {
						tempMsg.append("金融机构代码字符长度应该为18位" + spaceStr);
						isError = true;
					}
				}
			}
			if (checknumList.contains("JS0339")) {
				if (CheckUtil.checkStr(financeorgcode)) {
					tempMsg.append("金融机构代码不能包含空格和特殊字符" + spaceStr);
					isError = true;
				}
			}
			if (fzList == null) {
				String sql = "select distinct finorgcode from xjrjgfz";
				fzList = customSqlUtil.executeQuery(sql);
			}
			if (frList == null) {
				String sql = "select distinct finorgcode from xjrjgfrbaseinfo";
				frList = customSqlUtil.executeQuery(sql);
			}
			if(checknumList.contains("JS1887")) {
				if(!fzList.contains(financeorgcode) && !frList.contains(financeorgcode)) {
					tempMsg.append("金融机构代码必须在金融机构（法人）基础信息表.金融机构代码或者金融机构（分支机构）基础信息表.金融机构代码中存在" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1058")) {
				tempMsg.append("金融机构代码" + nullError + spaceStr);
				isError = true;
			}
		}

		// #内部机构号
		if (StringUtils.isNotBlank(financeorginnum)) {
			if (financeorginnum.length() <= 30) {

			} else {
				if (checknumList.contains("JS0346")) {
					tempMsg.append("内部机构号字符长度不能超过30" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS0340")) {
				if (CheckUtil.checkStr2(financeorginnum)) {
					tempMsg.append("内部机构号不能包含空格和特殊字符" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1534")) {
				tempMsg.append("内部机构号" + nullError + spaceStr);
				isError = true;
			}
		}

		// #担保人证件代码
		if (StringUtils.isNotBlank(gteeidnum)) {
			if (gteeidnum.length() <= 60) {

			} else {
				if (checknumList.contains("JS0352")) {
					tempMsg.append("担保人证件代码" + lengthError + 60 + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS0343")) {
				if (CheckUtil.checkStr2(gteeidnum)) {
					tempMsg.append("担保人证件代码不能包含空格和特殊字符" + spaceStr);
					isError = true;
				}
			}
			if (StringUtils.isNotBlank(gteeidtype)) {
				if ("A01".equals(gteeidtype)) {
					if (checknumList.contains("JS2114")) {
						if (gteeidnum.length() != 18) {
							tempMsg.append("担保人证件类型为统一社会信用代码的，证件代码长度必须为18位" + spaceStr);
							isError = true;
						}
					}
				} else if ("A02".equals(gteeidtype)) {
					if (checknumList.contains("JS2115")) {
						if (gteeidnum.length() != 9) {
							tempMsg.append("担保人证件类型为组织机构代码的，证件代码长度必须为9位" + spaceStr);
							isError = true;
						}
					}
				} else if (gteeidtype.startsWith("B")) {
					if ("B01".equals(gteeidtype) || "B08".equals(gteeidtype)) {
						if (checknumList.contains("JS2664")) {
							try {
								if (TuoMinUtils.getB01(gteeidnum).length() != 46) {
									tempMsg.append("担保人证件类型为B01-身份证或者B08-临时身份证的，担保人证件代码脱敏后的长度应该为46" + spaceStr);
									isError = true;
								}
							} catch (Exception e) {
								tempMsg.append("担保人证件类型为B01-身份证或者B08-临时身份证的，担保人证件代码脱敏后的长度应该为46" + spaceStr);
								isError = true;
							}
						}
					} else {
						if (checknumList.contains("JS2674")) {
							if (TuoMinUtils.getB01otr(gteeidnum).length() != 32) {
								tempMsg.append("担保人证件类型为B开头的个人证件且不是B01-身份证和B08-临时身份证的，担保人证件代码脱敏后的长度应该为32" + spaceStr);
								isError = true;
							}
						}
					}
				}
			}
			if("B01".equals(gteeidtype) || "B08".equals(gteeidtype)) {
				if (checknumList.contains("JS2684")) {
					try {
						if (gteeidnum.substring(6,14).compareTo(sjrq.replace("-", "")) >= 0) {
							tempMsg.append("证件类型为B01-身份证或者B08-临时身份证的，第7-14位的截取结果应该<数据日期" + spaceStr);
							isError = true;
						}
					}catch(Exception e) {
						tempMsg.append("证件类型为B01-身份证或者B08-临时身份证的，第7-14位的截取结果应该<数据日期" + spaceStr);
						isError = true;
					}
					
				}
			}
		}
		// #担保人证件类型
		if (StringUtils.isNotBlank(gteeidtype)) {
			// if (gteeidtype.length() <= 400) {
			if (checknumList.contains("JS0125")) {
				if (!ArrayUtils.contains(CheckUtil.bzrzjlx2, gteeidtype)) {
					tempMsg.append("担保人证件类型不在符合要求的最底层值域范围内" + spaceStr);
					isError = true;
				}
			}
			// } else {
			// tempMsg.append("担保人证件类型" + lengthError + 400 + spaceStr);
			// isError = true;
			// }
			// if(StringUtils.isNotBlank(isgreenloan)) {
			// if("D01".equals(isgreenloan)) {
			// if(!gteeidtype.startsWith("B")) {
			// tempMsg.append("担保人国民经济部门为D01-住户，则担保人证件类型应该为B开头的个人证件" + spaceStr);
			// isError = true;
			// }
			// }
			// }
		}
		// #被担保合同编码
		if (StringUtils.isNotBlank(loancontractcode)) {
			if (checknumList.contains("JS0348")) {
				if (loancontractcode.length() > 100) {
					tempMsg.append("被担保合同编码" + lengthError + 100 + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS0342")) {
				if (CheckUtil.checkStr2(loancontractcode)) {
					tempMsg.append("被担保合同编码不能包含特殊字符" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1060")) {
				tempMsg.append("被担保合同编码" + nullError + spaceStr);
				isError = true;
			}
		}
		// isError = CheckUtil.grantNullAndLengthAndStr2(loancontractcode, 100, tempMsg,
		// isError, "被担保合同编码");
		// #币种
		if (currencyList == null)
			currencyList = customSqlUtil.getBaseCode(BaseCurrency.class);
		if (StringUtils.isNotBlank(gteecurrency)) {
			if (checknumList.contains("JS0124")) {
				String[] strings = gteecurrency.split(",");
				for (String s : strings) {
					if (s.length() == 3) {
						if (!currencyList.contains(s)) {
							tempMsg.append("币种不在货币与资金代码表中" + spaceStr);
							isError = true;
							break;
						}
					} else {
						tempMsg.append("币种长度不等于3" + spaceStr);
						isError = true;
						break;
					}
				}
			}
		} else {
			if (checknumList.contains("JS1065")) {
				tempMsg.append("币种" + nullError + spaceStr);
				isError = true;
			}
		}
		// isError = CheckUtil.checkCurrency(gteecurrency, currencyList, customSqlUtil,
		// tempMsg, isError, "币种");
		// #担保合同编码
		if (StringUtils.isNotBlank(gteecontractcode)) {
			if (checknumList.contains("JS0347")) {
				if (gteecontractcode.length() > 100) {
					tempMsg.append("担保合同编码" + lengthError + 100 + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS0341")) {
				if (CheckUtil.checkStr2(gteecontractcode)) {
					tempMsg.append("担保合同编码不能包含特殊字符" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1059")) {
				tempMsg.append("担保合同编码" + nullError + spaceStr);
				isError = true;
			}
		}
		// isError = CheckUtil.grantNullAndLengthAndStr2(gteecontractcode, 100, tempMsg,
		// isError, "担保合同编码");
		// #担保合同编码+被担保合同编码唯一 =====前面查重

		// #担保合同到期日期
		if (StringUtils.isNotBlank(gteeenddate)) {
			if (checknumList.contains("JS0358")) {
				if (gteeenddate.length() == 10) {
					boolean b1 = CheckUtil.checkDate(gteeenddate, "yyyy-MM-dd");
					if (b1) {
						if (gteeenddate.compareTo("1800-01-01") < 0 || gteeenddate.compareTo("2100-12-31") > 0) {
							tempMsg.append("担保合同到期日期需晚于1800-01-01早于2100-12-31" + spaceStr);
							isError = true;
						}
					} else {
						tempMsg.append("担保合同到期日期不符合yyyy-MM-dd格式" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("担保合同到期日期长度不等于10" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1064")) {
				tempMsg.append("担保合同到期日期" + nullError + spaceStr);
				isError = true;
			}

		}
		// isError = CheckUtil.nullAndDate(gteeenddate, tempMsg, isError, "担保合同到期日期",
		// false);
		// #抵质押率
			if (StringUtils.isNotBlank(pledgerate)) {
				if (pledgerate.contains("‰") || pledgerate.contains("%")) {
					if (checknumList.contains("JS0344")) {
						tempMsg.append("抵质押合同的抵质押率不能包含‰或%" + spaceStr);
						isError = true;
					}
				}
				if (checknumList.contains("JS0355")) {
					if (pledgerate.length() > 10 || !CheckUtil.checkDecimal(pledgerate, 2)) {

						tempMsg.append("抵质押合同的抵质押率不能超过10位,小数位应保留2位" + spaceStr);
						isError = true;
					}
				}

				if (checknumList.contains("JS2352")) {
					if (new BigDecimal(pledgerate).compareTo(BigDecimal.ZERO) == -1) {
						tempMsg.append("抵质押合同的抵质押率应大于等于0" + spaceStr);
						isError = true;
					}
				}
				if (checknumList.contains("JS2353")) {
				 if(new BigDecimal(pledgerate).compareTo(new BigDecimal("100")) == 1) {
					 tempMsg.append("抵质押合同的抵质押率应小于等于100" + spaceStr);
					 isError = true;
				 }
				}
				if (checknumList.contains("JS2777")) {
					if (new BigDecimal(pledgerate).compareTo(BigDecimal.ONE) == -1) {
						tempMsg.append("抵质押合同的抵质押率应大于等于1" + spaceStr);
						isError = true;
					}
				}
			} else {
				if(StringUtils.isBlank(gteeidnum)) {
					if (checknumList.contains("JS1551")) {
						tempMsg.append("抵质押率" + nullError + spaceStr);
						isError = true;
					}
				}
			}

		// #担保合同金额
		if (StringUtils.isNotBlank(gteeamount)) {
			if (checknumList.contains("JS0353")) {
				if (gteeamount.length() > 20 || !CheckUtil.checkDecimal(gteeamount, 2)) {
					tempMsg.append("担保合同金额总长度不能超过20位,小数位应保留2位" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2445")) {
				if (new BigDecimal(gteeamount).compareTo(BigDecimal.ZERO) == -1) {
					tempMsg.append("担保合同金额应大于等于0" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1066")) {
				tempMsg.append("担保合同金额" + nullError + spaceStr);
				isError = true;
			}
		}
		// #担保合同金额折人民币

		if (StringUtils.isNotBlank(gteecnyamount)) {
			if (checknumList.contains("JS0354")) {
				if (gteecnyamount.length() > 20 || !CheckUtil.checkDecimal(gteecnyamount, 2)) {
					tempMsg.append("担保合同金额折人民币总长度不能超过20位,小数位应保留2位" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2446")) {
				if (new BigDecimal(gteecnyamount).compareTo(BigDecimal.ZERO) == -1) {
					tempMsg.append("担保合同金额折人民币应大于等于0" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2776")) {
				if (new BigDecimal(gteecnyamount).compareTo(new BigDecimal("1000")) == -1 || new BigDecimal(gteecnyamount).compareTo(new BigDecimal("100000000000")) == 1) {
					tempMsg.append("担保合同金额折人民币一般应在1000元至1000亿元范围内" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2692")) {
				if (StringUtils.isNotBlank(gteecurrency)) {
					if (StringUtils.isNotBlank(gteeamount) && "CNY".equals(gteecurrency)) {
						if (!gteecnyamount.equals(gteeamount)) {
							tempMsg.append("币种为人民币的，担保合同金额折人民币应该与担保合同金额的值相等" + spaceStr);
							isError = true;
						}
					}
				}
			}
		} else {
			if (checknumList.contains("JS1067")) {
				tempMsg.append("担保合同金额折人民币" + nullError + spaceStr);
				isError = true;
			}
		}
		// #担保合同类型
		if (StringUtils.isNotBlank(gteecontracttype)) {
			if (checknumList.contains("JS0122")) {
				if (!Arrays.asList(CheckUtil.dbhtlx).contains(gteecontracttype)) {
					tempMsg.append("担保合同类型不在规定的代码范围内" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1061")) {
				tempMsg.append("担保合同类型" + nullError + spaceStr);
				isError = true;
			}
		}
		// isError = CheckUtil.equalNullAndCode(gteecontracttype, 2,
		// Arrays.asList(CheckUtil.dbhtlx), tempMsg, isError,
		// "担保合同类型");
		// #担保合同起始日期
		if (StringUtils.isNotBlank(gteestartdate)) {
				if (checknumList.contains("JS0357")) {
					boolean b1 = CheckUtil.checkDate(gteestartdate, "yyyy-MM-dd");
					if (b1) {
						if (gteestartdate.compareTo("1800-01-01") >= 0 && gteestartdate.compareTo("2100-12-31") <= 0) {

						} else {
							tempMsg.append("担保合同起始日期需晚于1800-01-01早于2100-12-31" + spaceStr);
							isError = true;
						}
					} else {
						tempMsg.append("担保合同起始日期不符合yyyy-MM-dd格式" + spaceStr);
						isError = true;
					}
				}
				if (checknumList.contains("JS2443")) {
					if (StringUtils.isNotBlank(gteeenddate) && gteestartdate.compareTo(gteeenddate) > 0) {
						tempMsg.append("担保合同起始日期应小于等于担保合同到期日期" + spaceStr);
						isError = true;
					}
				}
				if (checknumList.contains("JS2442")) {
					if (StringUtils.isNotBlank(sjrq) && gteestartdate.compareTo(sjrq) > 0) {
						tempMsg.append("担保合同起始日期应小于等于数据日期" + spaceStr);
						isError = true;
					}
				}
		} else {
			if (checknumList.contains("JS1063")) {
				tempMsg.append("担保合同起始日期" + nullError + spaceStr);
				isError = true;
			}
		}
		// #数据日期
		isError = CheckUtil.nullAndDate(sjrq, tempMsg, isError, "数据日期", false);
		// #金融机构代码+内部机构号+贷款借据编码+被担保合同编码+发放/收回标识唯一

		if (isError) {
			if (!errorMsg.containsKey(xdkdbht.getId())) {
				errorMsg.put(xdkdbht.getId(), "担保合同编码:" + xdkdbht.getGteecontractcode() + "]->\r\n");
			}
			String str = errorMsg.get(xdkdbht.getId());
			str = str + tempMsg;
			errorMsg.put(xdkdbht.getId(), str);
			errorId.add(xdkdbht.getId());
		} else {
			if (!errorId.contains(xdkdbht.getId())) {
				rightId.add(xdkdbht.getId());
			}
		}
	}
	
	//个人客户基础信息
	public static int checkXgrkhxx(CustomSqlUtil customSqlUtil, Xgrkhxx xgrkhxx,
			LinkedHashMap<String, String> errorMsg,LinkedHashMap<String, String> errorMsg2, List<String> errorId, List<String> rightId,
			List<String> checknumList) {
		boolean isError = false;
		StringBuffer tempMsg = new StringBuffer("");

		String nullError = "不能为空";
		String lengthError = "超过限制长度";
		String spaceStr = "|";
		// #获取字段值
		String finorgcode = xgrkhxx.getFinorgcode();
		String regamtcreny = xgrkhxx.getRegamtcreny();
		String customercode = xgrkhxx.getCustomercode();
		String country = xgrkhxx.getCountry();
		String nation = xgrkhxx.getNation();
		String sex = xgrkhxx.getSex();
		String education = xgrkhxx.getEducation();
		String birthday = xgrkhxx.getBirthday();
		String regareacode = xgrkhxx.getRegareacode();
		String grincome = xgrkhxx.getGrincome();
		String familyincome = xgrkhxx.getFamilyincome();
		String marriage = xgrkhxx.getMarriage();
		String isrelation = xgrkhxx.getIsrelation();
		String creditamt = xgrkhxx.getCreditamt();
		String usedamt = xgrkhxx.getUsedamt();
		String grkhsfbs = xgrkhxx.getGrkhsfbs();
		String gtgshyyzzdm = xgrkhxx.getGtgshyyzzdm();
		String xwqyshtyxydm = xgrkhxx.getXwqyshtyxydm();
		String workareacode = xgrkhxx.getWorkareacode();
		String actctrltype = xgrkhxx.getActctrltype();
		String sjrq = xgrkhxx.getSjrq();

		//金融机构代码
		if (StringUtils.isNotBlank(finorgcode)) {
			if (finorgcode.length() == 18) {

			} else {
				if (checknumList.contains("JS0625")) {
					tempMsg.append("金融机构代码字符长度应该为18位" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS0620")) {
				if (CheckUtil.checkStr(finorgcode)) {
					tempMsg.append("金融机构代码不能包含空格和特殊字符" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1282")) {
				tempMsg.append("金融机构代码" + nullError + spaceStr);
				isError = true;
			}
		}
		
		//客户证件类型
		if (StringUtils.isNotBlank(regamtcreny)) {
			if (checknumList.contains("JS0193")) {
				if (!ArrayUtils.contains(CheckUtil.bzrzjlx, regamtcreny)) {
					tempMsg.append("客户证件类型不在符合要求的最底层值域范围内" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1284")) {
				tempMsg.append("客户证件类型" + nullError + spaceStr);
				isError = true;
			}
		}
		
		//客户证件代码
		if (StringUtils.isNotBlank(customercode)) {
			if (checknumList.contains("JS0621")) {
				if (CheckUtil.checkStr2(customercode)) {
					tempMsg.append("客户证件代码不能包含空格和特殊字符" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS0627")) {
				if (customercode.length() > 60) {
					tempMsg.append("境外客户的客户证件代码字符长度不能超过60" + spaceStr);
					isError = true;
				}
			}
			if ("A01".equals(regamtcreny)) {
				if (checknumList.contains("JS2612")) {
					if (customercode.length() != 18) {
						tempMsg.append("境内客户的客户证件代码字符长度应该为18位" + spaceStr);
						isError = true;
					}
				}
			} 
			if("B01".equals(regamtcreny) || "B08".equals(regamtcreny)) {
				if (checknumList.contains("JS2661")) {
					if (TuoMinUtils.getB01(customercode).length() != 46) {
						tempMsg.append("客户证件类型为B01-身份证或者B08-临时身份证的，客户证件代码脱敏后的长度应该为46" + spaceStr);
						isError = true;
					}
				}
			}else if(regamtcreny.startsWith("B")) {
				if (checknumList.contains("JS2671")) {
					if (TuoMinUtils.getB01otr(customercode).length() != 32) {
						tempMsg.append("客户证件类型为B开头的个人证件且不是B01-身份证和B08-临时身份证的，客户证件代码脱敏后的长度应该为32" + spaceStr);
						isError = true;
					}
				}
			}
			if("B01".equals(regamtcreny)) {
				if (checknumList.contains("JS2681")) {
					try {
						if (customercode.substring(6,14).compareTo(sjrq.replace("-", "")) >= 0) {
							tempMsg.append("证件类型为B01-身份证的，第7-14位的截取结果应该<数据日期" + spaceStr);
							isError = true;
						}
					}catch(Exception e) {
						tempMsg.append("证件类型为B01-身份证的，第7-14位的截取结果应该<数据日期" + spaceStr);
						isError = true;
					}
					
				}
			}
			
		} else {
			if (checknumList.contains("JS1285")) {
				tempMsg.append("客户证件代码" + nullError + spaceStr);
				isError = true;
			}
		}
		
		//国籍
		if(countryTwoList == null) {
			countryTwoList = customSqlUtil.getBaseCode(BaseCountryTwo.class);
		}
		if(StringUtils.isNotBlank(country)) {
			if(checknumList.contains("JS0194")) {
				if(!countryTwoList.contains(country)) {
					tempMsg.append("国籍不在符合要求的最底层值域范围内" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS1965")) {
				if(Arrays.asList(new String[]{"B06","B07","B12"}).contains(regamtcreny)) {
					if(!Arrays.asList(new String[]{"HK","MO","TW"}).contains(country)) {
						tempMsg.append("客户证件类型为港澳台证件的，国籍应为港澳台地区代码" + spaceStr);
						isError = true;
					}
				}
			}
			
			
		}else {
			if(checknumList.contains("JS1286")) {
				tempMsg.append("国籍不能为空" + spaceStr);
				isError = true;
			}
		}
		
		//民族
		if(nationList == null) {
			nationList = customSqlUtil.getBaseCode(BaseNation.class);
		}
		if(StringUtils.isNotBlank(nation)) {
			if(checknumList.contains("JS0195")) {
				if(!nationList.contains(nation)) {
					tempMsg.append("民族不在符合要求的最底层值域范围内" + spaceStr);
					isError = true;
				}
			}
			
			
		}else {
			if(checknumList.contains("JS1287")) {
				tempMsg.append("民族不能为空" + spaceStr);
				isError = true;
			}
		}
		
		//性别
		if(StringUtils.isNotBlank(sex)) {
			if(checknumList.contains("JS0196")) {
				if(!ArrayUtils.contains(CheckUtil.sex, sex)) {
					tempMsg.append("性别不在符合要求的最底层值域范围内" + spaceStr);
					isError = true;
				}
			}
			
			
		}else {
			if(checknumList.contains("JS1288")) {
				tempMsg.append("性别不能为空" + spaceStr);
				isError = true;
			}
		}
		
		//最高学历
		if(educationList == null) {
			educationList = customSqlUtil.getBaseCode(BaseEducation.class);
		}
		if(StringUtils.isNotBlank(education)) {
			if(checknumList.contains("JS0197")) {
				if(!educationList.contains(education)) {
					tempMsg.append("最高学历不在符合要求的最底层值域范围内" + spaceStr);
					isError = true;
				}
			}
		}else{
			if(checknumList.contains("JS1289")) {
				tempMsg.append("最高学历不能为空" + spaceStr);
				isError = true;
			}
		}
		
		//出生日期
		if(StringUtils.isNotBlank(birthday)) {
			if (checknumList.contains("JS0638")) {
				if (birthday.length() == 10) {
					boolean b1 = CheckUtil.checkDate(birthday, "yyyy-MM-dd");
					if (b1) {
						if (birthday.compareTo("1800-01-01") < 0 || birthday.compareTo("2100-12-31") > 0) {
							tempMsg.append("出生日期需晚于1800-01-01早于2100-12-31" + spaceStr);
							isError = true;
						}
					} else {
						tempMsg.append("出生日期不符合yyyy-MM-dd格式" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("出生日期长度不等于10" + spaceStr);
					isError = true;
				}
			}
			
			if(checknumList.contains("JS2227")) {
				if(StringUtils.isNotBlank(sjrq) && birthday.compareTo(sjrq) > 0) {
					tempMsg.append("出生日期应小于等于数据日期" + spaceStr);
					isError = true;
				}
			}
		}else {
			if(checknumList.contains("JS1290")) {
				tempMsg.append("出生日期不能为空" + spaceStr);
				isError = true;
			}
		}
		
		//地区代码
		if (StringUtils.isNotBlank(regareacode)) {
				if (checknumList.contains("JS0201")) {
					if (areaList == null)
						areaList = customSqlUtil.getBaseCode(BaseArea.class);
					if (countryList == null)
						countryList = customSqlUtil.getBaseCode(BaseCountry.class);
					if (!areaList.contains(regareacode) && !countryList.contains(regareacode)) {
						tempMsg.append("地区代码不在规定的代码范围内" + spaceStr);
						isError = true;
					}
				}
			
		} else {
			if (checknumList.contains("JS1554")) {
				tempMsg.append("地区代码" + nullError + spaceStr);
				isError = true;
			}
		}
		
		//个人年收入
		if (StringUtils.isNotBlank(grincome)) {
			if (checknumList.contains("JS0633")) {
				if (grincome.length() > 20 || !CheckUtil.checkDecimal(grincome, 2)) {
					tempMsg.append("个人年收入总长度不能超过20位,小数位必须为2位" + spaceStr);
					isError = true;
				}
			}
			if(StringUtils.isNotBlank(familyincome)) {
				if(checknumList.contains("JS2228")) {
					if(new BigDecimal(grincome).compareTo(new BigDecimal(familyincome)) > 0) {
						tempMsg.append("个人年收入应小于等于家庭年收入" + spaceStr);
						isError = true;
					}
				}
			}
			if(checknumList.contains("JS2229")) {
				if(new BigDecimal(grincome).compareTo(BigDecimal.ZERO) < 0) {
					tempMsg.append("个人年收入应大于等于0" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS2871")) {
				if(new BigDecimal(grincome).compareTo(new BigDecimal("10000000")) == 1) {
					tempMsg.append("当个人年收入不为空时，应小于等于1000万" + spaceStr);
					isError = true;
				}
			}
		}
		
		//家庭年收入
		if (StringUtils.isNotBlank(familyincome)) {
			if (checknumList.contains("JS0634")) {
				if (familyincome.length() > 20 || !CheckUtil.checkDecimal(familyincome, 2)) {
					tempMsg.append("家庭年收入总长度不能超过20位,小数位必须为2位" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS2230")) {
				if(new BigDecimal(familyincome).compareTo(BigDecimal.ZERO) < 0) {
					tempMsg.append("家庭年收入应大于等于0" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS2872")) {
				if(new BigDecimal(familyincome).compareTo(new BigDecimal("10000000")) == 1) {
					tempMsg.append("当家庭年收入不为空时，应小于等于1000万" + spaceStr);
					isError = true;
				}
			}
		}
		
		//婚姻情况
		if(StringUtils.isNotBlank(marriage)) {
			if(checknumList.contains("JS0198")) {
				if(!ArrayUtils.contains(CheckUtil.marriage, marriage)) {
					tempMsg.append("婚姻情况不在符合要求的最底层值域范围内" + spaceStr);
					isError = true;
				}
			}			
			
		}else {
			if(checknumList.contains("JS1293")) {
				tempMsg.append("婚姻情况不能为空" + spaceStr);
				isError = true;
			}
		}
		
		//是否关联方
		if (StringUtils.isNotBlank(isrelation)) {
			if (checknumList.contains("JS0199")) {
				if (!Arrays.asList(CheckUtil.sfcommon).contains(isrelation)) {
					tempMsg.append("是否关联方不在规定的代码范围内" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1294")) {
				tempMsg.append("是否关联方" + nullError + spaceStr);
				isError = true;
			}
		}
		
		//授信额度
		if (StringUtils.isNotBlank(creditamt)) {
			if (checknumList.contains("JS0635")) {
				if (creditamt.length() > 20 || !CheckUtil.checkDecimal(creditamt, 2)) {
					tempMsg.append("授信额度总长度不能超过20位,小数位应保留2位" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2356")) {
				if (new BigDecimal(creditamt).compareTo(BigDecimal.ZERO) == -1) {
					tempMsg.append("当授信额度不为空时，授信额度应大于等于0" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2774")) {
				if (new BigDecimal(creditamt).compareTo(new BigDecimal("100000000")) == 1) {
					tempMsg.append("授信额度一般不能大于1亿" + spaceStr);
					isError = true;
				}
			}
		}
		
		//已用额度
		if (StringUtils.isNotBlank(usedamt)) {
			if (checknumList.contains("JS0636")) {
				if (usedamt.length() > 20 || !CheckUtil.checkDecimal(usedamt, 2)) {
					tempMsg.append("已用额度总长度不能超过20位,小数位应保留2位" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2357")) {
				if (new BigDecimal(usedamt).compareTo(BigDecimal.ZERO) == -1) {
					tempMsg.append("当已用额度不为空时，已用额度应大于等于0" + spaceStr);
					isError = true;
				}
			}
			if (checknumList.contains("JS2775")) {
				if (new BigDecimal(usedamt).compareTo(new BigDecimal("100000000")) == 1) {
					tempMsg.append("已用额度一般不能大于1亿" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS2358")) {
				if(StringUtils.isNotBlank(creditamt)) {
					 if(new BigDecimal(usedamt).compareTo(new BigDecimal(creditamt)) == 1) {
						 tempMsg.append("当授信额度与已用额度不为空时，已用额度应小于等于授信额度" + spaceStr);isError = true;
					 }
				}
			}
		}
		
		//个人客户身份标识
		if(StringUtils.isNotBlank(grkhsfbs)) {
			if(checknumList.contains("JS0200")) {
				String[] grkhsfbsArr = grkhsfbs.split(",");
				for(String str : grkhsfbsArr) {
					if(!ArrayUtils.contains(CheckUtil.grkhsfbs, str)) {
						tempMsg.append("个人客户身份标识不在符合要求的最底层值域范围内" + spaceStr);
						isError = true;
						break;
					}
				}
			}
			if(checknumList.contains("JS0628")) {
				if(grkhsfbs.length() > 10) {
					tempMsg.append("个人客户身份标识字段长度不能超过10" + spaceStr);
					isError = true;
				}
			}
			
		}else {
			if (checknumList.contains("JS1557")) {
				tempMsg.append("个人客户身份标识不能为空" + spaceStr);
				isError = true;
			}
		}
		
		//个体工商户营业执照代码
		if(StringUtils.isNotBlank(gtgshyyzzdm)) {
			if(checknumList.contains("JS0623")) {
				if(CheckUtil.checkStr(gtgshyyzzdm)) {
					tempMsg.append("个体工商户营业执照代码不能包含特殊字符" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS0629")) {
				if(gtgshyyzzdm.length() > 400) {
					tempMsg.append("个体工商户营业执照代码字段长度不能超过400" + spaceStr);
					isError = true;
				}
			}
		}else {
			if(checknumList.contains("JS1558")) {
				if(StringUtils.isNotBlank(grkhsfbs) && grkhsfbs.indexOf("2") != -1) {
					tempMsg.append("当个人客户身份标识中包含2-个体工商户时，个体工商户营业执照代码不能为空" + spaceStr);
					isError = true;
				}
			}
		}
		
		//小微企业社会统一信用代码
		if(StringUtils.isNotBlank(xwqyshtyxydm)) {
			if(checknumList.contains("JS0624")) {
				if(CheckUtil.checkStr(xwqyshtyxydm)) {
					tempMsg.append("小微企业社会统一信用代码不能包含特殊字符" + spaceStr);
					isError = true;
				}
			}
			if(checknumList.contains("JS0630")) {
				if(xwqyshtyxydm.length() > 400) {
					tempMsg.append("小微企业社会统一信用代码字段长度不能超过400" + spaceStr);
					isError = true;
				}
			}
			//个人客户若为小微企业主，则小微企业社会统一信用代码应该能关联上非同业单位客户基础信息表
	    	if(checknumList.contains("JS1851")) {
	    		if(ftyList == null) {
	    			String sql = "select customercode from xftykhx t1 where exists (select 1 from xgrkhxx t2 where t1.sjrq = t2.sjrq and t2.regamtcreny in('A01','A02'))";
	    			ftyList = customSqlUtil.executeQuery(sql);
	    		}
	    		String[] codeStr = xwqyshtyxydm.split(",");
	    		for(String s : codeStr) {
	    			if(!ftyList.contains(s)) {
	    				tempMsg.append("个人客户若为小微企业主，则小微企业社会统一信用代码应该能关联上非同业单位客户基础信息表" + spaceStr);
						isError = true;
						break;
	    			}
	    		}
	    	}
		}else {
			if(checknumList.contains("JS1559")) {
				if(StringUtils.isNotBlank(grkhsfbs) && grkhsfbs.indexOf("3") != -1) {
					tempMsg.append("当个人客户身份标识中包含3-小微企业主时，小微企业社会统一信用代码不能为空" + spaceStr);
					isError = true;
				}
			}
		}
		
		//客户信用级别总等级数
		if (StringUtils.isNotBlank(workareacode)) {
			if (checknumList.contains("JS0631")) {
				if(!CheckUtil.checkZhenshu(workareacode)) {
					tempMsg.append("客户信用级别总等级数字符长度不能超过3位的整数" + spaceStr);
					isError = true;
				}
			}
		} else {
			if (checknumList.contains("JS1560")) {
				tempMsg.append("客户信用级别总等级数" + nullError + spaceStr);
				isError = true;
			}
		}
		
		//客户信用评级
		if (StringUtils.isNotBlank(actctrltype)) {
			if (actctrltype.length() <= 3 && CheckUtil.checkeCyryPattern(actctrltype)) {

			} else {
				if (checknumList.contains("JS0632")) {
					tempMsg.append("客户信用评级字符长度不能超过3位的整数" + spaceStr);
					isError = true;
				}
			}
		}
		
		//数据日期
		isError = CheckUtil.nullAndDate(sjrq, tempMsg, isError, "数据日期", false);
		
		if (isError || errorMsg.containsKey(xgrkhxx.getId().toString())) {
			errorMsg2.put(xgrkhxx.getId().toString(), "客户证件代码:" + xgrkhxx.getCustomercode() + "]->\r\n");
			String str = errorMsg.get(xgrkhxx.getId().toString());
			str = str == null ? "客户证件代码:" + xgrkhxx.getCustomercode() + "]->\r\n" : str;
			str = str + tempMsg;
			errorMsg2.put(xgrkhxx.getId().toString(), str);
			errorId.add(xgrkhxx.getId().toString());
			return 1;
		} else {
			if (!errorId.contains(xgrkhxx.getId().toString())) {
				rightId.add(xgrkhxx.getId().toString());
			}
			return 0;
		}
	}
	
	//个人客户基础信息(有校验阈值)
		public static int checkXgrkhxxLimit(CustomSqlUtil customSqlUtil, Xgrkhxx xgrkhxx,
				List<Integer> errorId0, List<String> errorId, List<String> rightId,
				List<String> checknumList,Map<String,String> limitMap,int checknumLimit) {
			boolean isError = false;

			String nullError = "不能为空";
			String lengthError = "超过限制长度";
			String spaceStr = "|";
			// #获取字段值
			String finorgcode = xgrkhxx.getFinorgcode();
			String regamtcreny = xgrkhxx.getRegamtcreny();
			String customercode = xgrkhxx.getCustomercode();
			String country = xgrkhxx.getCountry();
			String nation = xgrkhxx.getNation();
			String sex = xgrkhxx.getSex();
			String education = xgrkhxx.getEducation();
			String birthday = xgrkhxx.getBirthday();
			String regareacode = xgrkhxx.getRegareacode();
			String grincome = xgrkhxx.getGrincome();
			String familyincome = xgrkhxx.getFamilyincome();
			String marriage = xgrkhxx.getMarriage();
			String isrelation = xgrkhxx.getIsrelation();
			String creditamt = xgrkhxx.getCreditamt();
			String usedamt = xgrkhxx.getUsedamt();
			String grkhsfbs = xgrkhxx.getGrkhsfbs();
			String gtgshyyzzdm = xgrkhxx.getGtgshyyzzdm();
			String xwqyshtyxydm = xgrkhxx.getXwqyshtyxydm();
			String workareacode = xgrkhxx.getWorkareacode();
			String actctrltype = xgrkhxx.getActctrltype();
			String sjrq = xgrkhxx.getSjrq();

			//金融机构代码
			if (StringUtils.isNotBlank(finorgcode)) {
				if (finorgcode.length() == 18) {

				} else {
					if (checknumList.contains("JS0625")) {
						putMap("金融机构代码字符长度应该为18位", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if (checknumList.contains("JS0620")) {
					if (CheckUtil.checkStr(finorgcode)) {
						putMap("金融机构代码不能包含空格和特殊字符", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
			} else {
				if (checknumList.contains("JS1282")) {
					putMap("金融机构代码" + nullError, customercode, limitMap, checknumLimit);
					isError = true;
				}
			}
			
			//客户证件类型
			if (StringUtils.isNotBlank(regamtcreny)) {
				if (checknumList.contains("JS0193")) {
					if (!ArrayUtils.contains(CheckUtil.bzrzjlx, regamtcreny)) {
						putMap("客户证件类型不在符合要求的最底层值域范围内", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
			} else {
				if (checknumList.contains("JS1284")) {
					putMap("客户证件类型" + nullError, customercode, limitMap, checknumLimit);
					isError = true;
				}
			}
			
			//客户证件代码
			if (StringUtils.isNotBlank(customercode)) {
				if (checknumList.contains("JS0621")) {
					if (CheckUtil.checkStr2(customercode)) {
						putMap("客户证件代码不能包含空格和特殊字符", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if (checknumList.contains("JS0627")) {
					if (customercode.length() > 60) {
						putMap("境外客户的客户证件代码字符长度不能超过60", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if ("A01".equals(regamtcreny)) {
					if (checknumList.contains("JS2612")) {
						if (customercode.length() != 18) {
							putMap("境内客户的客户证件代码字符长度应该为18位", customercode, limitMap, checknumLimit);
							isError = true;
						}
					}
				} 
				if("B01".equals(regamtcreny) || "B08".equals(regamtcreny)) {
					if (checknumList.contains("JS2661")) {
						if (TuoMinUtils.getB01(customercode).length() != 46) {
							putMap("客户证件类型为B01-身份证或者B08-临时身份证的，客户证件代码脱敏后的长度应该为46", customercode, limitMap, checknumLimit);
							isError = true;
						}
					}
				}else if(regamtcreny.startsWith("B")) {
					if (checknumList.contains("JS2671")) {
						if (TuoMinUtils.getB01otr(customercode).length() != 32) {
							putMap("客户证件类型为B开头的个人证件且不是B01-身份证和B08-临时身份证的，客户证件代码脱敏后的长度应该为32", customercode, limitMap, checknumLimit);
							isError = true;
						}
					}
				}
				if("B01".equals(regamtcreny)) {
					if (checknumList.contains("JS2681")) {
						try {
							if (customercode.substring(6,14).compareTo(sjrq.replace("-", "")) >= 0) {
								putMap("证件类型为B01-身份证的，第7-14位的截取结果应该<数据日期", customercode, limitMap, checknumLimit);
								isError = true;
							}
						}catch(Exception e) {
							putMap("证件类型为B01-身份证的，第7-14位的截取结果应该<数据日期", customercode, limitMap, checknumLimit);
							isError = true;
						}
						
					}
				}
				
			} else {
				if (checknumList.contains("JS1285")) {
					putMap("客户证件代码" + nullError, customercode, limitMap, checknumLimit);
					isError = true;
				}
			}
			
			//国籍
			if(countryTwoList == null) {
				countryTwoList = customSqlUtil.getBaseCode(BaseCountryTwo.class);
			}
			if(StringUtils.isNotBlank(country)) {
				if(checknumList.contains("JS0194")) {
					if(!countryTwoList.contains(country)) {
						putMap("国籍不在符合要求的最底层值域范围内", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if(checknumList.contains("JS1965")) {
					if(Arrays.asList(new String[]{"B06","B07","B12"}).contains(regamtcreny)) {
						if(!Arrays.asList(new String[]{"HK","MO","TW"}).contains(country)) {
							putMap("客户证件类型为港澳台证件的，国籍应为港澳台地区代码", customercode, limitMap, checknumLimit);
							isError = true;
						}
					}
				}
				
				
			}else {
				if(checknumList.contains("JS1286")) {
					putMap("国籍不能为空", customercode, limitMap, checknumLimit);
					isError = true;
				}
			}
			
			//民族
			if(nationList == null) {
				nationList = customSqlUtil.getBaseCode(BaseNation.class);
			}
			if(StringUtils.isNotBlank(nation)) {
				if(checknumList.contains("JS0195")) {
					if(!nationList.contains(nation)) {
						putMap("民族不在符合要求的最底层值域范围内", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				
				
			}else {
				if(checknumList.contains("JS1287")) {
					putMap("民族不能为空", customercode, limitMap, checknumLimit);
					isError = true;
				}
			}
			
			//性别
			if(StringUtils.isNotBlank(sex)) {
				if(checknumList.contains("JS0196")) {
					if(!ArrayUtils.contains(CheckUtil.sex, sex)) {
						putMap("性别不在符合要求的最底层值域范围内", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				
				
			}else {
				if(checknumList.contains("JS1288")) {
					putMap("性别不能为空", customercode, limitMap, checknumLimit);
					isError = true;
				}
			}
			
			//最高学历
			if(educationList == null) {
				educationList = customSqlUtil.getBaseCode(BaseEducation.class);
			}
			if(StringUtils.isNotBlank(education)) {
				if(checknumList.contains("JS0197")) {
					if(!educationList.contains(education)) {
						putMap("最高学历不在符合要求的最底层值域范围内", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
			}else{
				if(checknumList.contains("JS1289")) {
					putMap("最高学历不能为空", customercode, limitMap, checknumLimit);
					isError = true;
				}
			}
			
			//出生日期
			if(StringUtils.isNotBlank(birthday)) {
				if (checknumList.contains("JS0638")) {
					if (birthday.length() == 10) {
						boolean b1 = CheckUtil.checkDate(birthday, "yyyy-MM-dd");
						if (b1) {
							if (birthday.compareTo("1800-01-01") < 0 || birthday.compareTo("2100-12-31") > 0) {
								putMap("出生日期需晚于1800-01-01早于2100-12-31", customercode, limitMap, checknumLimit);
								isError = true;
							}
						} else {
							putMap("出生日期不符合yyyy-MM-dd格式", customercode, limitMap, checknumLimit);
							isError = true;
						}
					} else {
						putMap("出生日期长度不等于10", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				
				if(checknumList.contains("JS2227")) {
					if(StringUtils.isNotBlank(sjrq) && birthday.compareTo(sjrq) > 0) {
						putMap("出生日期应小于等于数据日期", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
			}else {
				if(checknumList.contains("JS1290")) {
					putMap("出生日期不能为空", customercode, limitMap, checknumLimit);
					isError = true;
				}
			}
			
			//地区代码
			if (StringUtils.isNotBlank(regareacode)) {
					if (checknumList.contains("JS0201")) {
						if (areaList == null)
							areaList = customSqlUtil.getBaseCode(BaseArea.class);
						if (countryList == null)
							countryList = customSqlUtil.getBaseCode(BaseCountry.class);
						if (!areaList.contains(regareacode) && !countryList.contains(regareacode)) {
							putMap("地区代码不在规定的代码范围内", customercode, limitMap, checknumLimit);
							isError = true;
						}
					}
				
			} else {
				if (checknumList.contains("JS1554")) {
					putMap("地区代码" + nullError, customercode, limitMap, checknumLimit);
					isError = true;
				}
			}
			
			//个人年收入
			if (StringUtils.isNotBlank(grincome)) {
				if (checknumList.contains("JS0633")) {
					if (grincome.length() > 20 || !CheckUtil.checkDecimal(grincome, 2)) {
						putMap("个人年收入总长度不能超过20位,小数位必须为2位", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if(StringUtils.isNotBlank(familyincome)) {
					if(checknumList.contains("JS2228")) {
						if(new BigDecimal(grincome).compareTo(new BigDecimal(familyincome)) > 0) {
							putMap("个人年收入应小于等于家庭年收入", customercode, limitMap, checknumLimit);
							isError = true;
						}
					}
				}
				if(checknumList.contains("JS2229")) {
					if(new BigDecimal(grincome).compareTo(BigDecimal.ZERO) < 0) {
						putMap("个人年收入应大于等于0", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if(checknumList.contains("JS2871")) {
					if(new BigDecimal(grincome).compareTo(new BigDecimal("10000000")) == 1) {
						putMap("当个人年收入不为空时，应小于等于1000万", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
			}
			
			//家庭年收入
			if (StringUtils.isNotBlank(familyincome)) {
				if (checknumList.contains("JS0634")) {
					if (familyincome.length() > 20 || !CheckUtil.checkDecimal(familyincome, 2)) {
						putMap("家庭年收入总长度不能超过20位,小数位必须为2位", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if(checknumList.contains("JS2230")) {
					if(new BigDecimal(familyincome).compareTo(BigDecimal.ZERO) < 0) {
						putMap("家庭年收入应大于等于0", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if(checknumList.contains("JS2872")) {
					if(new BigDecimal(familyincome).compareTo(new BigDecimal("10000000")) == 1) {
						putMap("当家庭年收入不为空时，应小于等于1000万", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
			}
			
			//婚姻情况
			if(StringUtils.isNotBlank(marriage)) {
				if(checknumList.contains("JS0198")) {
					if(!ArrayUtils.contains(CheckUtil.marriage, marriage)) {
						putMap("婚姻情况不在符合要求的最底层值域范围内", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}			
				
			}else {
				if(checknumList.contains("JS1293")) {
					putMap("婚姻情况不能为空", customercode, limitMap, checknumLimit);
					isError = true;
				}
			}
			
			//是否关联方
			if (StringUtils.isNotBlank(isrelation)) {
				if (checknumList.contains("JS0199")) {
					if (!Arrays.asList(CheckUtil.sfcommon).contains(isrelation)) {
						putMap("是否关联方不在规定的代码范围内", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
			} else {
				if (checknumList.contains("JS1294")) {
					putMap("是否关联方" + nullError, customercode, limitMap, checknumLimit);
					isError = true;
				}
			}
			
			//授信额度
			if (StringUtils.isNotBlank(creditamt)) {
				if (checknumList.contains("JS0635")) {
					if (creditamt.length() > 20 || !CheckUtil.checkDecimal(creditamt, 2)) {
						putMap("授信额度总长度不能超过20位,小数位应保留2位", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if (checknumList.contains("JS2356")) {
					if (new BigDecimal(creditamt).compareTo(BigDecimal.ZERO) == -1) {
						putMap("当授信额度不为空时，授信额度应大于等于0", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if (checknumList.contains("JS2774")) {
					if (new BigDecimal(creditamt).compareTo(new BigDecimal("100000000")) == 1) {
						putMap("授信额度一般不能大于1亿", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
			}
			
			//已用额度
			if (StringUtils.isNotBlank(usedamt)) {
				if (checknumList.contains("JS0636")) {
					if (usedamt.length() > 20 || !CheckUtil.checkDecimal(usedamt, 2)) {
						putMap("已用额度总长度不能超过20位,小数位应保留2位", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if (checknumList.contains("JS2357")) {
					if (new BigDecimal(usedamt).compareTo(BigDecimal.ZERO) == -1) {
						putMap("当已用额度不为空时，已用额度应大于等于0", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if (checknumList.contains("JS2775")) {
					if (new BigDecimal(usedamt).compareTo(new BigDecimal("100000000")) == 1) {
						putMap("已用额度一般不能大于1亿", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if(checknumList.contains("JS2358")) {
					if(StringUtils.isNotBlank(creditamt)) {
						 if(new BigDecimal(usedamt).compareTo(new BigDecimal(creditamt)) == 1) {
							 putMap("当授信额度与已用额度不为空时，已用额度应小于等于授信额度", customercode, limitMap, checknumLimit);
						 }
					}
				}
			}
			
			//个人客户身份标识
			if(StringUtils.isNotBlank(grkhsfbs)) {
				if(checknumList.contains("JS0200")) {
					String[] grkhsfbsArr = grkhsfbs.split(",");
					for(String str : grkhsfbsArr) {
						if(!ArrayUtils.contains(CheckUtil.grkhsfbs, str)) {
							putMap("个人客户身份标识不在符合要求的最底层值域范围内", customercode, limitMap, checknumLimit);
							isError = true;
							break;
						}
					}
				}
				if(checknumList.contains("JS0628")) {
					if(grkhsfbs.length() > 10) {
						putMap("个人客户身份标识字段长度不能超过10", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				
			}else {
				if (checknumList.contains("JS1557")) {
					putMap("个人客户身份标识不能为空", customercode, limitMap, checknumLimit);
					isError = true;
				}
			}
			
			//个体工商户营业执照代码
			if(StringUtils.isNotBlank(gtgshyyzzdm)) {
				if(checknumList.contains("JS0623")) {
					if(CheckUtil.checkStr(gtgshyyzzdm)) {
						putMap("个体工商户营业执照代码不能包含特殊字符", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if(checknumList.contains("JS0629")) {
					if(gtgshyyzzdm.length() > 400) {
						putMap("个体工商户营业执照代码字段长度不能超过400", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
			}else {
				if(checknumList.contains("JS1558")) {
					if(StringUtils.isNotBlank(grkhsfbs) && grkhsfbs.indexOf("2") != -1) {
						putMap("当个人客户身份标识中包含2-个体工商户时，个体工商户营业执照代码不能为空", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
			}
			
			//小微企业社会统一信用代码
			if(StringUtils.isNotBlank(xwqyshtyxydm)) {
				if(checknumList.contains("JS0624")) {
					if(CheckUtil.checkStr(xwqyshtyxydm)) {
						putMap("小微企业社会统一信用代码不能包含特殊字符", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				if(checknumList.contains("JS0630")) {
					if(xwqyshtyxydm.length() > 400) {
						putMap("小微企业社会统一信用代码字段长度不能超过400", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
				//个人客户若为小微企业主，则小微企业社会统一信用代码应该能关联上非同业单位客户基础信息表
		    	if(checknumList.contains("JS1851")) {
		    		if(ftyList == null) {
		    			String sql = "select customercode from xftykhx t1 where exists (select 1 from xgrkhxx t2 where t1.sjrq = t2.sjrq and t2.regamtcreny in('A01','A02'))";
		    			ftyList = customSqlUtil.executeQuery(sql);
		    		}
		    		String[] codeStr = xwqyshtyxydm.split(",");
		    		for(String s : codeStr) {
		    			if(!ftyList.contains(s)) {
		    				putMap("个人客户若为小微企业主，则小微企业社会统一信用代码应该能关联上非同业单位客户基础信息表", customercode, limitMap, checknumLimit);
							isError = true;
							break;
		    			}
		    		}
		    	}
			}else {
				if(checknumList.contains("JS1559")) {
					if(StringUtils.isNotBlank(grkhsfbs) && grkhsfbs.indexOf("3") != -1) {
						putMap("当个人客户身份标识中包含3-小微企业主时，小微企业社会统一信用代码不能为空", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
			}
			
			//客户信用级别总等级数
			if (StringUtils.isNotBlank(workareacode)) {
				if (checknumList.contains("JS0631")) {
					if(!CheckUtil.checkZhenshu(workareacode)) {
						putMap("客户信用级别总等级数字符长度不能超过3位的整数", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
			} else {
				if (checknumList.contains("JS1560")) {
					putMap("客户信用级别总等级数" + nullError, customercode, limitMap, checknumLimit);
					isError = true;
				}
			}
			
			//客户信用评级
			if (StringUtils.isNotBlank(actctrltype)) {
				if (actctrltype.length() <= 3 && CheckUtil.checkeCyryPattern(actctrltype)) {

				} else {
					if (checknumList.contains("JS0632")) {
						putMap("客户信用评级字符长度不能超过3位的整数", customercode, limitMap, checknumLimit);
						isError = true;
					}
				}
			}
			
			//数据日期
			if (StringUtils.isNotBlank(sjrq)){
	            if (sjrq.length()==10){
	                boolean b1 = CheckUtil.checkDate(sjrq, "yyyy-MM-dd");
	                if (b1){
	                    if (sjrq.compareTo("1800-01-01")<0 || sjrq.compareTo("2100-12-31")>0){
	                    	putMap("数据日期需晚于1800-01-01早于2100-12-31", customercode, limitMap, checknumLimit);
	        				isError = true;
	                    }
	                }else {
	                	putMap("数据日期不符合yyyy-MM-dd格式1", customercode, limitMap, checknumLimit);
        				isError = true;
	                }
	            }else {
	            	putMap("数据日期长度不等于10", customercode, limitMap, checknumLimit);
    				isError = true;
	            }
	        }else {
	        	putMap("数据日期不能为空", customercode, limitMap, checknumLimit);
				isError = true;

	        }
			
			if (isError || errorId0.contains(xgrkhxx.getId())) {
				errorId.add(xgrkhxx.getId().toString());
				return 1;
			} else {
				if (!errorId.contains(xgrkhxx.getId().toString())) {
					rightId.add(xgrkhxx.getId().toString());
				}
				return 0;
			}
		}
	
		public static void putMap(String rule,String code,Map<String,String> map,int checknumLimit) {
			if(map.containsKey(rule)) {
				String value = map.get(rule);
				int num = Integer.valueOf(value.substring(0,value.indexOf("#")));
				if(num >= checknumLimit) {
					limitFlag = true;
					return;
				}
				num++;
				map.put(rule, num+"#"+value+code+"\r\n");
			}else {
				map.put(rule, "1#"+code+"\r\n");
			}
		}
		
	public static void main(String[] args) throws NoSuchFieldException, SecurityException {
		Xgrkhxx xgrkhxx = new Xgrkhxx();
		Class<?> clazz = xgrkhxx.getClass();
		Field[] fields = clazz.getDeclaredFields();
//		for(Field f : fields) {
//			System.out.println("String "+f.getName()+" = xgrkhxx.get"+Stringutil.getMethodName(f.getName())+"();");
//			
//		}
		for(Field f : fields) {
		System.out.println(""+f.getName()+" = null;");
		
	}
	}
}
