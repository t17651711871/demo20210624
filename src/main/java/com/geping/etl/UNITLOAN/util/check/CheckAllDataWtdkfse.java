package com.geping.etl.UNITLOAN.util.check;

import static com.geping.etl.UNITLOAN.util.check.CheckUtil.checkStr2;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.report.Xdkfsxx;
import com.geping.etl.UNITLOAN.entity.report.Xwtdkfse;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.TuoMinUtils;

/**    
*  
* @author liuweixin  
* @date 2021年1月7日 下午3:32:49  
*/
public class CheckAllDataWtdkfse {
	//币种代码
    private static List<String> currencyList;
    //行政区划代码
    private static List<String> areaList;
    //国家代码
    private static List<String> countryList;
    //大行业代码
    private static List<String> aindustryList;

    //C0017-行业中类代码
    private static List<String> bindustryList;

    //金融机构（分支机构）基础信息表.金融机构代码
    public static List<String> fzList;

    //金融机构（法人）基础信息表.金融机构代码
    public static List<String> frList;

	//委托贷款发生额信息
    public static void checkXwtdkfse(CustomSqlUtil customSqlUtil, List<String> checknumList,Xwtdkfse xwtdkfse, LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId) {
        boolean isError = false;
        StringBuffer tempMsg = new StringBuffer("");

        String nullError = "不能为空";
        String lengthError = "超过限制长度";
        String spaceStr = "|";
        
        String finorgcode = xwtdkfse.getFinorgcode();
        String finorgincode = xwtdkfse.getFinorgincode();
        String finorgareacode = xwtdkfse.getFinorgareacode();
        String isfarmerloan = xwtdkfse.getIsfarmerloan();
        String browidcode = xwtdkfse.getBrowidcode();
        String isgreenloan = xwtdkfse.getIsgreenloan();
        String browinds = xwtdkfse.getBrowinds();
        String browareacode = xwtdkfse.getBrowareacode();
        String entpczjjcf = xwtdkfse.getEntpczjjcf();
        String entpmode = xwtdkfse.getEntpmode();
        String loanbrowcode = xwtdkfse.getLoanbrowcode();
        String loancontractcode = xwtdkfse.getLoancontractcode();
        String loanactdect = xwtdkfse.getLoanactdect();
        String loanstartdate = xwtdkfse.getLoanstartdate();
        String loanenddate = xwtdkfse.getLoanenddate();
        String transactionnum = xwtdkfse.getTransactionnum();
        String loancurrency = xwtdkfse.getLoancurrency();
        String loanamt = xwtdkfse.getLoanamt();
        String loancnyamt = xwtdkfse.getLoancnyamt();
        String rateisfix = xwtdkfse.getRateisfix();
        String ratelevel = xwtdkfse.getRatelevel();
        String sxfcny = xwtdkfse.getSxfcny();
        String gteemethod = xwtdkfse.getGteemethod();
        String wtrgmjjbm = xwtdkfse.getWtrgmjjbm();
        String wtrtype = xwtdkfse.getWtrtype();
        String wtrcode = xwtdkfse.getWtrcode();
        String wtrhy = xwtdkfse.getWtrhy();
        String wtrdqdm = xwtdkfse.getWtrdqdm();
        String wtrjjcf = xwtdkfse.getWtrjjcf();
        String wtrqygm = xwtdkfse.getWtrqygm();
        String givetakeid = xwtdkfse.getGivetakeid();
        String issupportliveloan = xwtdkfse.getIssupportliveloan();
        String sjrq = xwtdkfse.getSjrq();
        
        //金融机构代码
        if (StringUtils.isNotBlank(finorgcode)){
            if (CheckUtil.checkStr(finorgcode)){
                if (checknumList.contains("JS0399")) {
                    tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                    isError=true;
                }
            }
            
            if (finorgcode.length() !=18 ){
                if (checknumList.contains("JS0407")) { tempMsg.append("金融机构代码字符长度应该为18位"+spaceStr);isError=true;  }
            }

        }else {
            if (checknumList.contains("JS1117")) { tempMsg.append("金融机构代码不能为空" + spaceStr);isError = true;}
        }
        
        //内部机构号
        if (StringUtils.isNotBlank(finorgincode)){
            
            if (checkStr2(finorgincode)){
                if (checknumList.contains("JS0400")){     tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }
            
            if (finorgincode.length() > 30){
                if (checknumList.contains("JS0408")){   tempMsg.append("内部机构号字符长度不能超过30"+spaceStr);isError=true; }
            }
        }else {
            if (checknumList.contains("JS1118")){    tempMsg.append("内部机构号不能为空"+spaceStr);isError=true;}
        }
        
        //金融机构地区代码
        if (StringUtils.isNotBlank(finorgareacode)) {
	        if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
	        if (checknumList.contains("JS0150")) {
	        	if (!areaList.contains(finorgareacode)) {
                    tempMsg.append("金融机构地区代码不在规定的代码范围内" + spaceStr);
                    isError = true;
	        	}
	        }
        } else {
            if (checknumList.contains("JS1119")){  tempMsg.append("金融机构地区代码不能为空" + spaceStr);isError = true; }
        }
        
        //借款人证件类型
        if (StringUtils.isNotBlank(isfarmerloan)) {
            if (!ArrayUtils.contains(CheckUtil.grkhzjlx, isfarmerloan)) {
                if (checknumList.contains("JS0152"))
                {
                    tempMsg.append("委托贷款发生额信息的借款人证件类型需在符合要求的最底层值域范围内" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1121")) { tempMsg.append("借款人证件类型不能为空" + spaceStr);isError = true;  }
        }
        
        //借款人证件代码
        if(StringUtils.isNotBlank(browidcode)) {
        	if(checknumList.contains("JS0401")) {
        		if(CheckUtil.checkStr(browidcode)) {
        			tempMsg.append("借款人证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
        		}
        	}
        	if(checknumList.contains("JS0409")) {
        		if(StringUtils.isNotBlank(browidcode)) {
        			if(browidcode.length() > 60) {
        				tempMsg.append("借款人证件代码字符长度不能超过60" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS0894")) {
        		if(StringUtils.isNotBlank(browareacode) && StringUtils.isNotBlank(isfarmerloan) && !browareacode.startsWith("000") && "A01".equals(isfarmerloan)) {
        			if(browidcode.length() != 18) {
        				tempMsg.append("境内对公客户且借款人证件类型为A01的，借款人证件代码字符长度应该为18位" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS2659")) {
        		if(StringUtils.isNotBlank(isfarmerloan) && ("B01".equals(isfarmerloan) || "B08".equals(isfarmerloan))) {
        			if(TuoMinUtils.getB01(browidcode).length() != 46) {
        				tempMsg.append("借款人证件类型为B01-身份证或者B08-临时身份证的，借款人证件代码脱敏后的长度应该为46" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS2669")) {
        		if(StringUtils.isNotBlank(isfarmerloan) && isfarmerloan.startsWith("B") && !"B01".equals(isfarmerloan) && !"B08".equals(isfarmerloan)) {
        			if(TuoMinUtils.getB01otr(browidcode).length() != 32) {
        				tempMsg.append("借款人证件类型为B开头的个人证件且不是B01-身份证和B08-临时身份证的，借款人证件代码脱敏后的长度应该为32" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS2679")) {
        		if(StringUtils.isNotBlank(isfarmerloan) && ("B01".equals(isfarmerloan) || "B08".equals(isfarmerloan))) {
        			try {
						if (browidcode.substring(6,14).compareTo(sjrq.replace("-", "")) >= 0) {
							tempMsg.append("证件类型为B01-身份证或者B08-临时身份证的，第7-14位的截取结果应该<数据日期" + spaceStr);
							isError = true;
						}
					}catch(Exception e) {
						tempMsg.append("证件类型为B01-身份证或者B08-临时身份证的，第7-14位的截取结果应该<数据日期" + spaceStr);
						isError = true;
					}
        		}
        	}
        	if(checknumList.contains("JS2793")) {
        		if(StringUtils.isNotBlank(browareacode) && StringUtils.isNotBlank(isfarmerloan) && !browareacode.startsWith("000") && "A02".equals(isfarmerloan)) {
        			if(browidcode.length() != 9) {
        				tempMsg.append("境内对公客户且借款人证件类型为A02的，借款人证件代码字符长度应该为9位" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	
        }else {
        	if (checknumList.contains("JS1122")) { tempMsg.append("借款人证件代码不能为空" + spaceStr);isError = true;  }
        }
        
        //借款人国民经济部门
        if(StringUtils.isNotBlank(isgreenloan)) {
        	if(checknumList.contains("JS0151")) {
        		if(!ArrayUtils.contains(CheckUtil.wtjkrgmjjbm, isgreenloan)) {
        			tempMsg.append("委托贷款发生额信息的借款人国民经济部门需在符合要求的最底层值域范围内" + spaceStr);
                    isError = true;
        		}
        	}
        	if(checknumList.contains("JS1981")) {
        		if(StringUtils.isNotBlank(isfarmerloan) && isfarmerloan.startsWith("B")) {
        			if(!"D01".equals(isgreenloan) && !"E05".equals(isgreenloan)) {
        				tempMsg.append("借款人证件类型为B开头的个人证件，则借款人国民经济部门为D01-住户或E05-非居民" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS1983")) {
        		if(StringUtils.isNotBlank(isfarmerloan) && ArrayUtils.contains(new String[] {"B06","B12","B07","B09","B11"},isfarmerloan)) {
        			if(!isgreenloan.startsWith("E")) {
        				tempMsg.append("借款人证件类型为境外证件的，借款人国民经济部门应为E开头的非居民部门" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS2635")) {
        		if(StringUtils.isNotBlank(entpmode) && ArrayUtils.contains(CheckUtil.qygm, entpmode)) {
        			if(!isgreenloan.startsWith("C") && !isgreenloan.startsWith("B")) {
        				tempMsg.append("借款人企业规模为CS01至CS04的，借款人国民经济部门应该为C开头的非金融企业部门或者B开头的金融机构" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	
        }else {
        	if (checknumList.contains("JS1120")) { tempMsg.append("借款人国民经济部门不能为空" + spaceStr);isError = true;  }
        }
        
        //借款人行业
        if(StringUtils.isNotBlank(browinds)) {
        	if(aindustryList == null) aindustryList = customSqlUtil.getBaseCode(BaseAindustry.class);
        	if(checknumList.contains("JS0153")) {
        		if(!aindustryList.contains(browinds)) {
        			tempMsg.append("委托贷款发生额信息的借款人行业需在值域范围内" + spaceStr);
                    isError = true;
        		}
        	}
        	if(checknumList.contains("JS2009")) {
        		if(StringUtils.isNotBlank(isgreenloan) && isgreenloan.startsWith("E")) {
        			if(!"200".equals(browinds)) {
        				tempMsg.append("借款人国民经济部门为E开头的非居民部门，则借款人行业必须为200-境外" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS2010")) {
        		if(StringUtils.isNotBlank(isgreenloan) && "D01".equals(isgreenloan)) {
        			if(!"100".equals(browinds)) {
        				tempMsg.append("借款人国民经济部门为D01-住户，则借款人行业必须为100-个人" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS2011")) {
        		if(StringUtils.isNotBlank(isgreenloan) && !isgreenloan.startsWith("E") && !"D01".equals(isgreenloan)) {
        			if(ArrayUtils.contains(CheckUtil.jkrhy,browinds)) {
        				tempMsg.append("借款人国民经济部门不为E开头且不是D01-住户的，则借款人行业不能为100-个人和200-境外" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        }else {
        	if (checknumList.contains("JS1123")) { tempMsg.append("借款人行业不能为空" + spaceStr);isError = true;  }
        }
        
        //借款人地区代码
        if(StringUtils.isNotBlank(browareacode)) {
        	if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
        	if (countryList == null) countryList = customSqlUtil.getBaseCode(BaseCountry.class);
	        if (checknumList.contains("JS0154")) {
	        	if (!areaList.contains(browareacode) && !countryList.contains(browareacode)) {
                    tempMsg.append("委托贷款发生额信息的借款人地区代码需在符合要求的值域范围内" + spaceStr);
                    isError = true;
	        	}
	        }
        }else {
        	if (checknumList.contains("JS1124")) { tempMsg.append("借款人地区代码不能为空" + spaceStr);isError = true;  }
        }
        
        //借款人经济成分
        if(StringUtils.isNotBlank(entpczjjcf)) {
	        if (checknumList.contains("JS0155")) {
	        	if (!ArrayUtils.contains(CheckUtil.qyczrjjcf, entpczjjcf)) {
                    tempMsg.append("若借款人经济成分不为空，需在符合要求的值域范围最底层类别或最底层类别的父类中" + spaceStr);
                    isError = true;
	        	}
	        }
	        if (checknumList.contains("JS2016")) {
        		if(StringUtils.isNotBlank(browinds) && StringUtils.isNotBlank(entpmode) && (ArrayUtils.contains(CheckUtil.jkrhy, browinds) || "CS05".equals(entpmode))) {
        			tempMsg.append("当借款人为个人、境外非居民或境内非企业时，借款人经济成分必须为空" + spaceStr);
                    isError = true;
        		}
        	}
        }else {
        	if (checknumList.contains("JS1125")) {
        		if(StringUtils.isNotBlank(browinds) && StringUtils.isNotBlank(entpmode) && !ArrayUtils.contains(CheckUtil.jkrhy, browinds) && !"CS05".equals(entpmode)) {
        			tempMsg.append("当借款人不为个人、境外非居民以及境内非企业时，借款人经济成分不能为空" + spaceStr);
                    isError = true;
        		}
        	}
        }
        
        //借款人企业规模
        if(StringUtils.isNotBlank(entpmode)) {
        	if(checknumList.contains("JS0156")) {
        		if(!ArrayUtils.contains(CheckUtil.qygm2, entpmode)) {
        			tempMsg.append("若借款人企业规模不为空，需在符合要求的值域范围内" + spaceStr);
                    isError = true;
        		}
        	}
        	if(checknumList.contains("JS2017")) {
        		if(StringUtils.isNotBlank(browinds) && "100".equals(browinds)) {
        			tempMsg.append("个人客户的企业规模应为空" + spaceStr);
                    isError = true;
        		}
        	}
        	if(checknumList.contains("JS2636")) {
        		if(StringUtils.isNotBlank(isgreenloan) && isgreenloan.startsWith("C") && !"C99".equals(isgreenloan)) {
        			if(!ArrayUtils.contains(CheckUtil.qygm, entpmode)) {
        				tempMsg.append("借款人国民经济部门为C开头且不是C99的非金融企业部门，则借款人企业规模应该在CS01至CS04范围内" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        }else {
        	if(checknumList.contains("JS1126")) {
        		if(StringUtils.isNotBlank(browinds) && !ArrayUtils.contains(CheckUtil.jkrhy, browinds)) {
        			tempMsg.append("当借款人行业不为个人和境外时，借款人企业规模不能为空" + spaceStr);
                    isError = true;
        		}
        	}
        	
        }
        
        //委托贷款借据编码
        if(StringUtils.isNotBlank(loanbrowcode)) {
        	if(checknumList.contains("JS0402")) {
        		if(CheckUtil.checkStr(loanbrowcode)) {
            		tempMsg.append("委托贷款借据编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
            	}
        	}
        	if(checknumList.contains("JS0410")) {
        		if(loanbrowcode.length() > 100) {
            		tempMsg.append("委托贷款借据编码字符长度不能超过100" + spaceStr);
                    isError = true;
            	}
        	}
        }else {
        	if (checknumList.contains("JS1127")){ tempMsg.append("委托贷款借据编码不能为空" + spaceStr);isError = true; }
        }
        
        //委托贷款合同编码
        if(StringUtils.isNotBlank(loancontractcode)) {
        	if(checknumList.contains("JS0403")) {
        		if(CheckUtil.checkStr(loancontractcode)) {
            		tempMsg.append("委托贷款合同编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
            	}
        	}
        	if(checknumList.contains("JS0411")) {
        		if(loancontractcode.length() > 100) {
            		tempMsg.append("委托贷款合同编码字符长度不能超过100" + spaceStr);
                    isError = true;
            	}
        	}
        }else {
        	if (checknumList.contains("JS1128")){ tempMsg.append("委托贷款合同编码不能为空" + spaceStr);isError = true; }
        }
        
        //贷款实际投向
        if(StringUtils.isNotBlank(loanactdect)) {
        	if (bindustryList==null) bindustryList = customSqlUtil.getBaseCode(BaseBindustry.class);
        	if(checknumList.contains("JS0157")) {
        		if(!bindustryList.contains(loanactdect)) {
            		tempMsg.append("委托贷款发生额信息的贷款实际投向需在值域范围内" + spaceStr);
                    isError = true;
            	}
        	}
        }else {
        	if (checknumList.contains("JS1131")){ tempMsg.append("贷款实际投向不能为空" + spaceStr);isError = true; }
        }
        
        //委托贷款发放日期
        if(StringUtils.isNotBlank(loanstartdate)) {
        	if(checknumList.contains("JS0419")) {
        		boolean b1 = CheckUtil.checkDate(loanstartdate, "yyyy-MM-dd");
				if (b1) {
					if (loanstartdate.compareTo("1800-01-01") < 0 || loanstartdate.compareTo("2100-12-31") > 0) {
						tempMsg.append("委托贷款发放日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("委托贷款发放日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
					isError = true;
				}
        	}
        	if(checknumList.contains("JS2457")) {
        		if(StringUtils.isNotBlank(sjrq) && loanstartdate.compareTo(sjrq) > 0) {
        			tempMsg.append("委托贷款发放日期应小于等于数据日期" + spaceStr);
					isError = true;
            	}
        	}
        	if(checknumList.contains("JS2458")) {
        		if(StringUtils.isNotBlank(loanenddate) && loanstartdate.compareTo(loanenddate) > 0) {
        			tempMsg.append("委托贷款发放日期应小于等于委托贷款到期日期" + spaceStr);
					isError = true;
            	}
        	}
        	if(checknumList.contains("JS2563")) {
        		if(StringUtils.isNotBlank(givetakeid) && "1".equals(givetakeid)) {
        			try {
        				if(!loanstartdate.substring(0,7).equals(sjrq.substring(0,7))) {
            				tempMsg.append("当月发放的贷款，贷款发放日期应该在当月范围内" + spaceStr);
        					isError = true;
            			}
        			}catch(Exception e){
        				tempMsg.append("当月发放的贷款，贷款发放日期应该在当月范围内" + spaceStr);
    					isError = true;
        			}
        			
            	}
        	}
        }else {
        	if (checknumList.contains("JS1129")){ tempMsg.append("委托贷款发放日期不能为空" + spaceStr);isError = true; }
        }
        
        //委托贷款到期日期
        if(StringUtils.isNotBlank(loanenddate)) {
        	if(checknumList.contains("JS0420")) {
        		boolean b1 = CheckUtil.checkDate(loanenddate, "yyyy-MM-dd");
				if (b1) {
					if (loanenddate.compareTo("1800-01-01") < 0 || loanenddate.compareTo("2100-12-31") > 0) {
						tempMsg.append("委托贷款到期日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
						isError = true;
					}
				} else {
					tempMsg.append("委托贷款到期日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
					isError = true;
				}
        	}
        }else {
        	if (checknumList.contains("JS1130")){ tempMsg.append("委托贷款到期日期不能为空" + spaceStr);isError = true; }
        }
        
        //交易流水号
        if(StringUtils.isNotBlank(transactionnum)) {
        	if(checknumList.contains("JS0900")) {
        		if(CheckUtil.checkStr(transactionnum)) {
        			tempMsg.append("交易流水号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
					isError = true;
        		}
        	}
        	if(checknumList.contains("JS0901")) {
        		if(transactionnum.length() > 60) {
        			tempMsg.append("交易流水号字段长度不能超过60" + spaceStr);
					isError = true;
        		}
        	}
        }else {
        	if (checknumList.contains("JS1584")){ tempMsg.append("交易流水号不能为空" + spaceStr);isError = true; }
        }
        
        //币种
        if(StringUtils.isNotBlank(loancurrency)) {
        	if(currencyList == null) currencyList = customSqlUtil.getBaseCode(BaseCurrency.class);
        	if(checknumList.contains("JS0158")) {
        		if(!currencyList.contains(loancurrency)) {
            		tempMsg.append("委托贷款发生额信息的币种需在值域范围内" + spaceStr);
    				isError = true;
            	}
        	}
        	
        }else {
        	if (checknumList.contains("JS1132")){ tempMsg.append("币种不能为空" + spaceStr);isError = true; }
        }
        
        //委托贷款发生金额
        if(StringUtils.isNotBlank(loanamt)) {
        	if(checknumList.contains("JS0414")) {
        		if (loanamt.length() > 20 || !CheckUtil.checkDecimal(loanamt, 2)) {
					tempMsg.append("委托贷款发生金额总长度不能超过20位，小数位必须为2位" + spaceStr);
					isError = true;
				}
        	}
        	if(checknumList.contains("JS2459")) {
        		try {
        			if(new BigDecimal(loanamt).compareTo(BigDecimal.ZERO) <= 0) {
            			tempMsg.append("委托贷款发生金额应大于0" + spaceStr);
    					isError = true;
            		}
        		}catch(Exception e) {
        			tempMsg.append("委托贷款发生金额应大于0" + spaceStr);
					isError = true;
        		}
        		
        	}
        }else {
        	if (checknumList.contains("JS1133")){ tempMsg.append("委托贷款发生金额不能为空" + spaceStr);isError = true; }
        }
        
        //委托贷款发生金额折人民币
        if(StringUtils.isNotBlank(loancnyamt)) {
        	if(checknumList.contains("JS0415")) {
        		if (loancnyamt.length() > 20 || !CheckUtil.checkDecimal(loancnyamt, 2)) {
					tempMsg.append("委托贷款发生金额折人民币总长度不能超过20位，小数位必须为2位" + spaceStr);
					isError = true;
				}
        	}
        	if(checknumList.contains("JS2460")) {
        		try {
        			if(new BigDecimal(loancnyamt).compareTo(BigDecimal.ZERO) <= 0) {
            			tempMsg.append("委托贷款发生金额折人民币应大于0" + spaceStr);
    					isError = true;
            		}
        		}catch(Exception e) {
        			tempMsg.append("委托贷款发生金额折人民币应大于0" + spaceStr);
					isError = true;
        		}
        	}
        	if(checknumList.contains("JS2694")) {
        		if(StringUtils.isNotBlank(loancurrency) && "CNY".equals(loancurrency)) {
        			if(StringUtils.isNotBlank(loanamt) && loancnyamt.equals(loanamt)) {
            			
            		}else {
            			tempMsg.append("币种为人民币的，委托贷款发生金额折人民币应该与委托贷款发生金额的值相等" + spaceStr);
    					isError = true;
            		}
        		}
        	}
        }else {
        	if (checknumList.contains("JS1134")){ tempMsg.append("委托贷款发生金额折人民币不能为空" + spaceStr);isError = true; }
        }
        
        //利率是否固定
        if(StringUtils.isNotBlank(rateisfix)) {
        	if(checknumList.contains("JS0159")) {
        		if(!ArrayUtils.contains(CheckUtil.llsfgd, rateisfix)) {
        			tempMsg.append("委托贷款发生额信息的利率是否固定需在符合要求的值域范围内" + spaceStr);
					isError = true;
        		}
        	}
        }else {
        	if (checknumList.contains("JS1135")){ tempMsg.append("利率是否固定不能为空" + spaceStr);isError = true; }
        }
        
        //利率水平
        if(StringUtils.isNotBlank(ratelevel)) {
        	if(checknumList.contains("JS0405")) {
        		if(ratelevel.contains("‰") || ratelevel.contains("%")) {
        			tempMsg.append("利率水平不能包含‰或%" + spaceStr);
					isError = true;
        		}
        	}
        	if(checknumList.contains("JS0416")) {
        		if (ratelevel.length() > 10 || !CheckUtil.checkDecimal(ratelevel, 5)) {
					tempMsg.append("利率水平总长度不能超过10位，小数位必须为5位" + spaceStr);
					isError = true;
				}
        	}
        	if(checknumList.contains("JS2461")) {
        		try {
        			if(new BigDecimal(ratelevel).compareTo(BigDecimal.ZERO) == -1 || new BigDecimal(ratelevel).compareTo(new BigDecimal("30")) == 1) {
        				tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
    					isError = true;
        			}
        		}catch(Exception e) {
        			tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
					isError = true;
        		}
        	}
        }else {
        	if (checknumList.contains("JS1136")){ tempMsg.append("利率水平不能为空" + spaceStr);isError = true; }
        }
        
        //手续费金额折人民币
        if(StringUtils.isNotBlank(sxfcny)) {
        	if(checknumList.contains("JS0417")) {
        		if (sxfcny.length() > 20 || !CheckUtil.checkDecimal(sxfcny, 2)) {
					tempMsg.append("手续费金额折人民币总长度不能超过20位，小数位必须为2位" + spaceStr);
					isError = true;
				}
        	}
        	if(checknumList.contains("JS2462")) {
        		try {
        			if(new BigDecimal(sxfcny).compareTo(BigDecimal.ZERO) <= 0) {
            			tempMsg.append("手续费金额折人民币应大于0" + spaceStr);
    					isError = true;
            		}
        		}catch(Exception e) {
        			tempMsg.append("手续费金额折人民币应大于0" + spaceStr);
					isError = true;
        		}
        		
        	}
        }else {
        	if (checknumList.contains("JS1137")){ tempMsg.append("手续费金额折人民币不能为空" + spaceStr);isError = true; }
        }
        
        //贷款担保方式
        if(StringUtils.isNotBlank(gteemethod)) {
        	if(checknumList.contains("JS0160")) {
        		if(!ArrayUtils.contains(CheckUtil.dkdbfs, gteemethod)) {
        			tempMsg.append("委托贷款发生额信息的贷款担保方式需在符合要求的最底层值域范围内" + spaceStr);
					isError = true;
        		}
        	}
        }else {
        	if (checknumList.contains("JS1138")){ tempMsg.append("贷款担保方式不能为空" + spaceStr);isError = true; }
        }
        
        //委托人国民经济部门
        if(StringUtils.isNotBlank(wtrgmjjbm)) {
        	if(checknumList.contains("JS0161")) {
        		if(!ArrayUtils.contains(CheckUtil.wtjkrgmjjbm, wtrgmjjbm)) {
        			tempMsg.append("委托贷款发生额信息的委托人国民经济部门需在符合要求的最底层值域范围内" + spaceStr);
                    isError = true;
        		}
        	}
        	if(checknumList.contains("JS1988")) {
        		if(StringUtils.isNotBlank(wtrtype) && ArrayUtils.contains(new String[] {"B06","B12","B07","B09","B11"},wtrtype)) {
        			if(!wtrgmjjbm.startsWith("E")) {
        				tempMsg.append("委托人证件类型为境外证件的，委托人国民经济部门应为E开头的非居民部门" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS2637")) {
        		if(StringUtils.isNotBlank(wtrqygm) && ArrayUtils.contains(CheckUtil.qygm, wtrqygm)) {
        			if(!wtrgmjjbm.startsWith("C") && !wtrgmjjbm.startsWith("B")) {
        				tempMsg.append("委托人企业规模为CS01至CS04的，委托人国民经济部门应该为C开头的非金融企业部门或者B开头的金融机构" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	
        }else {
        	if (checknumList.contains("JS1139")) { tempMsg.append("委托人国民经济部门不能为空" + spaceStr);isError = true;  }
        }
        
        //委托人证件类型
        if (StringUtils.isNotBlank(wtrtype)) {
        	if (checknumList.contains("JS0162")){
        		if (ArrayUtils.contains(CheckUtil.grkhzjlx, wtrtype) && !"A03".equals(wtrtype) && !"A04".equals(wtrtype)) {
                    
                }else {
                	tempMsg.append("委托贷款发生额信息的委托人证件类型需在符合要求的最底层值域范围内且不能为资管产品统计编码、资管产品登记备案编码" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1140")) { tempMsg.append("委托人证件类型不能为空" + spaceStr);isError = true;  }
        }
        
        //委托人证件代码
        if(StringUtils.isNotBlank(wtrcode)) {
        	if(checknumList.contains("JS0404")) {
        		if(CheckUtil.checkStr(wtrcode)) {
        			tempMsg.append("委托人证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
        		}
        	}
        	if(checknumList.contains("JS0412")) {
        		if(StringUtils.isNotBlank(wtrcode)) {
        			if(wtrcode.length() > 60) {
        				tempMsg.append("委托人证件代码字符长度不能超过60" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS0895")) {
        		if(StringUtils.isNotBlank(wtrdqdm) && StringUtils.isNotBlank(wtrtype) && !wtrdqdm.startsWith("000") && "A01".equals(wtrtype)) {
        			if(wtrcode.length() != 18) {
        				tempMsg.append("境内对公客户且委托人证件类型为A01的，委托人证件代码字符长度应该为18位" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS2660")) {
        		if(StringUtils.isNotBlank(wtrtype) && ("B01".equals(wtrtype) || "B08".equals(wtrtype))) {
        			if(TuoMinUtils.getB01(wtrcode).length() != 46) {
        				tempMsg.append("委托人证件类型为B01-身份证或者B08-临时身份证的，委托人证件代码脱敏后的长度应该为46" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS2670")) {
        		if(StringUtils.isNotBlank(wtrtype) && wtrtype.startsWith("B") && !"B01".equals(wtrtype) && !"B08".equals(wtrtype)) {
        			if(TuoMinUtils.getB01otr(wtrcode).length() != 32) {
        				tempMsg.append("委托人证件类型为B开头的个人证件且不是B01-身份证和B08-临时身份证的，委托人证件代码脱敏后的长度应该为32" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS2680")) {
        		if(StringUtils.isNotBlank(wtrtype) && ("B01".equals(wtrtype) || "B08".equals(wtrtype))) {
        			try {
						if (wtrcode.substring(6,14).compareTo(sjrq.replace("-", "")) >= 0) {
							tempMsg.append("证件类型为B01-身份证或者B08-临时身份证的，第7-14位的截取结果应该<数据日期" + spaceStr);
							isError = true;
						}
					}catch(Exception e) {
						tempMsg.append("证件类型为B01-身份证或者B08-临时身份证的，第7-14位的截取结果应该<数据日期" + spaceStr);
						isError = true;
					}
        		}
        	}
        	if(checknumList.contains("JS2794")) {
        		if(StringUtils.isNotBlank(wtrdqdm) && StringUtils.isNotBlank(wtrtype) && !wtrdqdm.startsWith("000") && "A02".equals(wtrtype)) {
        			if(wtrcode.length() != 9) {
        				tempMsg.append("境内对公客户且委托人证件类型为A02的，委托人证件代码字符长度应该为9位" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	
        }else {
        	if (checknumList.contains("JS1141")) { tempMsg.append("委托人证件代码不能为空" + spaceStr);isError = true;  }
        }
        
        //委托人行业
        if(StringUtils.isNotBlank(wtrhy)) {
        	if(aindustryList == null) aindustryList = customSqlUtil.getBaseCode(BaseAindustry.class);
        	if(checknumList.contains("JS0163")) {
        		if(!aindustryList.contains(wtrhy)) {
        			tempMsg.append("委托贷款发生额信息的委托人行业需在值域范围内" + spaceStr);
                    isError = true;
        		}
        	}
        	if(checknumList.contains("JS2033")) {
        		if(StringUtils.isNotBlank(wtrgmjjbm) && wtrgmjjbm.startsWith("E")) {
        			if(!"200".equals(wtrhy)) {
        				tempMsg.append("委托人国民经济部门为E开头的非居民部门，则委托人行业必须为200-境外" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS2034")) {
        		if(StringUtils.isNotBlank(wtrgmjjbm) && "D01".equals(wtrgmjjbm)) {
        			if(!"100".equals(wtrhy)) {
        				tempMsg.append("委托人国民经济部门为D01-住户，则委托人行业必须为100-个人" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        	if(checknumList.contains("JS2035")) {
        		if(StringUtils.isNotBlank(wtrgmjjbm) && !wtrgmjjbm.startsWith("E") && !"D01".equals(wtrgmjjbm)) {
        			if(ArrayUtils.contains(CheckUtil.jkrhy,wtrhy)) {
        				tempMsg.append("委托人国民经济部门不为E开头且不是D01-住户的，则委托人行业不能为100-个人和200-境外" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        }else {
        	if (checknumList.contains("JS1142")) { tempMsg.append("委托人行业不能为空" + spaceStr);isError = true;  }
        }
        
        //委托人地区代码
        if(StringUtils.isNotBlank(wtrdqdm)) {
        	if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
        	if (countryList == null) countryList = customSqlUtil.getBaseCode(BaseCountry.class);
	        if (checknumList.contains("JS0164")) {
	        	if (!areaList.contains(wtrdqdm) && !countryList.contains(wtrdqdm)) {
                    tempMsg.append("委托贷款发生额信息的委托人地区代码需在符合要求的值域范围内" + spaceStr);
                    isError = true;
	        	}
	        }
        }else {
        	if (checknumList.contains("JS1143")) { 
        			if(StringUtils.isNotBlank(wtrgmjjbm) && !"B09".equals(wtrgmjjbm)) {
        				tempMsg.append("当委托人国民经济部门不为特定目的载体时，委托人地区代码不能为空" + spaceStr);
        				isError = true;
        			}
        		}
        }
        
        //委托人经济成分
        if(StringUtils.isNotBlank(wtrjjcf)) {
	        if (checknumList.contains("JS0165")) {
	        	if (!ArrayUtils.contains(CheckUtil.qyczrjjcf, wtrjjcf)) {
                    tempMsg.append("若委托人经济成分不为空，需在符合要求的值域范围最底层类别或最底层类别的父类中" + spaceStr);
                    isError = true;
	        	}
	        }
	        if (checknumList.contains("JS2040")) {
        		if(StringUtils.isNotBlank(wtrhy) && StringUtils.isNotBlank(wtrqygm) && StringUtils.isNotBlank(wtrgmjjbm) && (ArrayUtils.contains(CheckUtil.jkrhy, wtrhy) || "CS05".equals(wtrqygm) || "B09".equals(wtrgmjjbm))) {
        			tempMsg.append("当委托人为个人、境外非居民、境内非企业或特定目的载体时，委托人经济成分必须为空" + spaceStr);
                    isError = true;
        		}
        	}
        }else {
        	if (checknumList.contains("JS1144")) {
        		if(StringUtils.isNotBlank(wtrhy) && StringUtils.isNotBlank(wtrqygm) && StringUtils.isNotBlank(wtrgmjjbm) && !ArrayUtils.contains(CheckUtil.jkrhy, wtrhy) && !"CS05".equals(wtrqygm) && !"B09".equals(wtrgmjjbm)) {
        			tempMsg.append("当委托人不为个人、境外以及境内非企业时，委托人经济成分不能为空" + spaceStr);
                    isError = true;
        		}
        	}
        }
        
        //委托人企业规模
        if(StringUtils.isNotBlank(wtrqygm)) {
        	if(checknumList.contains("JS0166")) {
        		if(!ArrayUtils.contains(CheckUtil.qygm2, wtrqygm)) {
        			tempMsg.append("若委托人企业规模不为空，需在符合要求的值域范围内" + spaceStr);
                    isError = true;
        		}
        	}
        	if(checknumList.contains("JS2041")) {
        		if(StringUtils.isNotBlank(wtrhy) && "100".equals(wtrhy)) {
        			tempMsg.append("个人客户的企业规模应为空" + spaceStr);
                    isError = true;
        		}
        	}
        	if(checknumList.contains("JS2638")) {
        		if(StringUtils.isNotBlank(wtrgmjjbm) && wtrgmjjbm.startsWith("C") && !"C99".equals(wtrgmjjbm)) {
        			if(!ArrayUtils.contains(CheckUtil.qygm, wtrqygm)) {
        				tempMsg.append("借款人国民经济部门为C开头且不是C99的非金融企业部门，则借款人企业规模应该在CS01至CS04范围内" + spaceStr);
                        isError = true;
        			}
        		}
        	}
        }else {
        	if(checknumList.contains("JS1145")) {
        		if(StringUtils.isNotBlank(wtrhy) && !ArrayUtils.contains(CheckUtil.jkrhy, wtrhy)) {
        			tempMsg.append("当委托人行业不为个人和境外时，委托人企业规模不能为空" + spaceStr);
                    isError = true;
        		}
        	}
        	
        }
        
        //发放/收回标识
        if(StringUtils.isNotBlank(givetakeid)) {
        	if(checknumList.contains("JS0213")) {
        		if(!ArrayUtils.contains(CheckUtil.sfcommon, givetakeid)) {
        			tempMsg.append("委托贷款发生额信息的发放/收回标识需在符合要求的值域范围内" + spaceStr);
                    isError = true;
        		}
        	}
        }else {
        	if (checknumList.contains("JS1585")) { tempMsg.append("发放/收回标识不能为空" + spaceStr);isError = true;  }
        }
        
        //贷款用途
        if(StringUtils.isNotBlank(issupportliveloan)) {
        	if (checknumList.contains("JS0406")) {
        		if(CheckUtil.checkStr(issupportliveloan)) {
        			tempMsg.append("贷款用途字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
        		}
        	}
        	if (checknumList.contains("JS0406")) {
        		if(issupportliveloan.length() > 3000) {
        			tempMsg.append("贷款用途字符长度不能超过3000" + spaceStr);
                    isError = true;
        		}
        	}
        }
        
        
        //数据日期
        isError = CheckUtil.nullAndDate(sjrq, tempMsg, isError, "数据日期",false);

        //#整表
        if (isError) {
            if(!errorMsg.containsKey(xwtdkfse.getId())) {
                errorMsg.put(xwtdkfse.getId(), "委托贷款合同编码:"+xwtdkfse.getLoancontractcode()+"，"+"委托贷款借据编码:"+xwtdkfse.getLoanbrowcode()+"]->\r\n");
            }
            String str = errorMsg.get(xwtdkfse.getId());
            str = str + tempMsg;
            errorMsg.put(xwtdkfse.getId(), str);
            errorId.add(xwtdkfse.getId());
        } else {
            if(!errorId.contains(xwtdkfse.getId())) {
                rightId.add(xwtdkfse.getId());
            }
        }
    }
    
	public static void main(String[] args) throws NoSuchFieldException, SecurityException {
		Xwtdkfse xwtdkfse = new Xwtdkfse();
		Class<?> clazz = xwtdkfse.getClass();
		Field[] fields = clazz.getDeclaredFields();
		for(Field f : fields) {
		System.out.println("String "+f.getName()+" = xwtdkfse.get"+f.getName().substring(0, 1).toUpperCase()+f.getName().substring(1)+"();");
		
		}
	}
}
