package com.geping.etl.UNITLOAN.util;

import com.geping.etl.utils.EncryptUtils;
import org.apache.commons.lang.StringUtils;

import java.io.UnsupportedEncodingException;

/**
 * 脱敏工具包
 * @author Administrator
 *
 */
public class Desensitization {
	
	
	/**
	 * 身份证号码脱敏方法
	 * @return
	 */
	public static String idCardDesen(String idcard) throws UnsupportedEncodingException {
		if (StringUtils.isBlank(idcard)) return "";
		StringBuffer newCard=new StringBuffer();
		byte[] bytes = idcard.getBytes();
		for (int i = 0; i < bytes.length; i++) {
			//小写字母:97-122;大写字母:65-90
			if ((char)bytes[i]>=97 && (char)bytes[i]<=122){
				bytes[i]=(byte)((char)bytes[i] - 'a' + 'A');
			}
			if (i<6){
				newCard.append((char)bytes[i]);
			}
		}
		String tempEncode = EncryptUtils.encodeMD5String(new String(bytes));
		byte[] bytes2 = tempEncode.getBytes();
		for (int i = 0; i < bytes2.length; i++) {
			if ((char)bytes2[i]>=65 && (char)bytes2[i]<=90){
				bytes2[i]=(byte) ((char)bytes2[i] - 'A' + 'a');
			}
		}
		newCard.append(new String(bytes2));
		return String.valueOf(newCard);
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			String s = idCardDesen("419981020723a");
			System.out.println(s);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

}
