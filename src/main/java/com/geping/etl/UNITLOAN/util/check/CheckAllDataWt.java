package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.entity.baseInfo.*;
import com.geping.etl.UNITLOAN.entity.report.Xclgrdkxx;
import com.geping.etl.UNITLOAN.entity.report.Xclwtdkxx;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.TuoMinUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * @Author: wangzd
 * @Date: 15:21 2020/6/10
 */
public class CheckAllDataWt {

    //币种代码
    private static List<String> currencyList;
    //行政区划代码
    private static List<String> areaList;
    //国家代码
    private static List<String> countryList;
    //大行业代码
    private static List<String> aindustryList;

    //C0017-行业中类代码
    private static List<String> bindustryList;
    @Autowired
    private CustomSqlUtil customSqlUtil;

    @Autowired
    private XCheckRuleService xCheckRuleService;

    //金融机构（分支机构）基础信息表.金融机构代码
    public static List<String> fzList;

    //金融机构（法人）基础信息表.金融机构代码
    public static List<String> frList;

    //存量委托贷款信息
    public static void checkXclwtdkxx(CustomSqlUtil customSqlUtil, List<String> xclwtdkxx1List, Xclwtdkxx xclwtdkxx, LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId) {
        boolean isError = false;
        StringBuffer tempMsg = new StringBuffer("");
        String nullError = "不能为空";
        String lengthError = "超过限制长度";
        String spaceStr = "|";
        //#金融机构代码

//      isError = CheckUtil.grantNullAndLengthAndStr(xclwtdkxx.getFinanceorgcode(), 18, tempMsg, isError, "金融机构代码");
        if (StringUtils.isNotBlank(xclwtdkxx.getFinanceorgcode())) {
            if (xclwtdkxx1List.contains("JS0384")) {
                if (xclwtdkxx.getFinanceorgcode().length() != 18) {
                    tempMsg.append("金融机构代码长度应为18" + spaceStr);
                    isError = true;
                }
            }
            if (xclwtdkxx1List.contains("JS0376")) {
                if (CheckUtil.checkStr(xclwtdkxx.getFinanceorgcode())) {
                    tempMsg.append("金融机构代码字段内容中不得出现“？”、“！” 、“^” 、“*”其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }

//            if (fzList == null) {
//                String sql = "select distinct finorgcode from xjrjgfz";
//                fzList = customSqlUtil.executeQuery(sql);
//            }
//            if (frList == null) {
//                String sql = "select distinct finorgcode from xjrjgfrbaseinfo";
//                frList = customSqlUtil.executeQuery(sql);
//            }
//            if(xclwtdkxx1List.contains("JS1887")) {
//                if(!fzList.contains(xclwtdkxx.getFinanceorgcode()) && !frList.contains(xclwtdkxx.getFinanceorgcode())) {
//                    tempMsg.append("金融机构代码必须在金融机构（法人）基础信息表.金融机构代码或者金融机构（分支机构）基础信息表.金融机构代码中存在" + spaceStr);
//                    isError = true;
//                }
//            }
        } else {
            if (xclwtdkxx1List.contains("JS1086")) {
                tempMsg.append("金融机构代码不能为空" + spaceStr);
                isError = true;
            }
        }

        //#金融机构地区代码
        if (StringUtils.isNotBlank(xclwtdkxx.getFinanceorgareacode())) {
            if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
            if (xclwtdkxx1List.contains("JS0131")) {
                if (!areaList.contains(xclwtdkxx.getFinanceorgareacode())) {
                    tempMsg.append("金融机构地区代码不在规定的代码范围内" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (xclwtdkxx1List.contains("JS1087")) {
                tempMsg.append("金融机构地区代码不能为空" + spaceStr);
                isError = true;
            }
        }


        //查重

        //#币种
        if (currencyList == null) currencyList = customSqlUtil.getBaseCode(BaseCurrency.class);
        if (xclwtdkxx1List.contains("JS1101")) {
            if (StringUtils.isBlank(xclwtdkxx.getCurrency())) {
                tempMsg.append("币种不能为空" + spaceStr);
                isError = true;
            }
        }
        if (StringUtils.isNotBlank(xclwtdkxx.getCurrency())) {
            if (xclwtdkxx1List.contains("JS0139")) {
                isError = CheckUtil.checkCurrency(xclwtdkxx.getCurrency(), currencyList, customSqlUtil, tempMsg, isError, "币种");
            }
        }

        //借款人国民经济部门
        if (StringUtils.isNotBlank(xclwtdkxx.getIsgreenloan())) {
            if (xclwtdkxx.getIsgreenloan().startsWith("E")) {
                if (!xclwtdkxx.getBrrowerindustry().equals("200")) {
                    if (xclwtdkxx1List.contains("JS2004")) {
                        tempMsg.append("借款人国民经济部门为E开头的非居民部门，则借款人行业必须为200-境外" + spaceStr);
                        isError = true;
                    }
                }
            }

            if (xclwtdkxx.getIsgreenloan().equals("D01")) {
                if (!xclwtdkxx.getBrrowerindustry().equals("100")) {
                    if (xclwtdkxx1List.contains("JS2005")) {
                        tempMsg.append("借款人国民经济部门为D01-住户，则借款人行业必须为100-个人" + spaceStr);
                        isError = true;
                    }
                }
            }


            if (!xclwtdkxx.getIsgreenloan().equals("D01") && StringUtils.isNotBlank(xclwtdkxx.getIsgreenloan()) && !xclwtdkxx.getIsgreenloan().startsWith("E")) {
                if (StringUtils.isNotBlank(xclwtdkxx.getBrrowerindustry()) && ArrayUtils.contains(CheckUtil.jkrhy,xclwtdkxx.getBrrowerindustry()) ) {
                    if (xclwtdkxx1List.contains("JS2006")) {
                        tempMsg.append("借款人国民经济部门不为E开头且不是D01-住户的，则借款人行业不能为100-个人和200-境外" + spaceStr);
                        isError = true;
                    }
                }
            }
        }


        if (StringUtils.isBlank(xclwtdkxx.getGuaranteemethod())) {
            if (xclwtdkxx1List.contains("JS1107")) {
                tempMsg.append("贷款担保方式不能为空" + spaceStr);
                isError = true;
            }

        } else {
            if (xclwtdkxx1List.contains("JS0141")) {
                //#贷款担保方式
                isError = CheckUtil.checkdkdbfs(xclwtdkxx.getGuaranteemethod(), tempMsg, isError, "贷款担保方式");
            }
        }


        //数据日期
//        if(StringUtils.isNotBlank(xclwtdkxx.getSjrq())){
//            boolean b=CheckUtil.checkDate(xclwtdkxx.getSjrq(),"yyyy-MM-dd");
//            if(b){
//                if(xclwtdkxx.getSjrq().compareTo("1900-01-01")<0||xclwtdkxx.getSjrq().compareTo("2100-12-31")>0){
//                    tempMsg.append("数据日期必须满足日期格式为：YYYY-MM-DD且日期范围在1900-01-01到2100-12-31之间" + spaceStr);
//                    isError = true;
//                }
//
//            }else {
//                tempMsg.append("数据日期不符合yyyy-MM-dd格式" + spaceStr);
//                isError = true;
//            }
//
//        }else {
//            tempMsg.append("数据日期" + nullError + spaceStr);
//            isError = true;
//        }


        //#委托贷款到期日期
        //数据日期
//        isError = CheckUtil.nullAndDate(xclwtdkxx.getSjrq(), tempMsg, isError, "数据日期",false);

        if (StringUtils.isNotBlank(xclwtdkxx.getLoanenddate())) {
            boolean b = CheckUtil.checkDate(xclwtdkxx.getLoanenddate(), "yyyy-MM-dd");
            if (b) {
                if (xclwtdkxx.getLoanenddate().compareTo("1800-01-01") >= 0 && xclwtdkxx.getLoanenddate().compareTo("2100-12-31") <= 0) {

                    if (xclwtdkxx.getLoanstatus().equals("LS02") && StringUtils.isNotBlank(xclwtdkxx.getExtensiondate())) {
                        if (xclwtdkxx.getLoanenddate().compareTo(xclwtdkxx.getExtensiondate()) >= 0) {
                            if (xclwtdkxx1List.contains("JS2452")) {
                                tempMsg.append("当贷款展期到期日期不为空且贷款状态为LS02-展期时，委托贷款到期日期应小于委托贷款展期到期日期" + spaceStr);
                                isError = true;
                            }
                        }
                    }

                    if (xclwtdkxx.getLoanstatus().equals("LS04") && StringUtils.isNotBlank(xclwtdkxx.getExtensiondate())) {
//                        if (xclwtdkxx.getLoanstatus().equals("LS02") && StringUtils.isNotBlank(xclwtdkxx.getExtensiondate())) {
                        if (xclwtdkxx.getLoanstatus().compareTo(xclwtdkxx.getExtensiondate()) <= 0) {
                            if (xclwtdkxx1List.contains("JS2615")) {
                                tempMsg.append("当贷款展期到期日期不为空且贷款状态为LS04-缩期时，委托贷款到期日期应大于委托贷款展期到期日期" + spaceStr);
                                isError = true;
                            }
                        }
                    }
//                    }
                } else {
                    if (xclwtdkxx1List.contains("JS0397")) {
                        tempMsg.append("委托贷款到期日期需早于1800-01-01晚于2100-12-31" + spaceStr);
                        isError = true;
                    }
                }

            } else {
                if (xclwtdkxx1List.contains("JS0397")) {
                    tempMsg.append("委托贷款到期日期不符合yyyy-MM-dd格式" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (xclwtdkxx1List.contains("JS1099")) {
                tempMsg.append("委托贷款到期日期" + nullError + spaceStr);
                isError = true;
            }
        }


        //#委托贷款发放日期
        if (StringUtils.isNotBlank(xclwtdkxx.getLoanstartdate())) {
            boolean b1 = CheckUtil.checkDate(xclwtdkxx.getLoanstartdate(), "yyyy-MM-dd");
            if (b1) {
                if (xclwtdkxx.getLoanstartdate().compareTo("1800-01-01") > 0 && xclwtdkxx.getLoanstartdate().compareTo("2100-12-31") < 0) {

//                    if (StringUtils.isNotBlank(xclwtdkxx.getExtensiondate())) {
//                        if (StringUtils.isNotBlank(xclwtdkxx.getLoanenddate()) && xclwtdkxx.getLoanstartdate().compareTo(xclwtdkxx.getLoanenddate()) <= 0) {
//
//                            if (StringUtils.isNotBlank(xclwtdkxx.getExtensiondate()) &&
//                                    xclwtdkxx.getLoanstartdate().compareTo(xclwtdkxx.getExtensiondate()) > 0) {
//                                if (xclwtdkxx1List.contains("JS2425")) {
//                                    tempMsg.append("贷款发放日期应小于等于贷款展期到期日期" + spaceStr);
//                                    isError = true;
//                                }
//                            }
//                        }
//
//                    }
                    if (StringUtils.isNotBlank(xclwtdkxx.getLoanenddate())) {
                        if (xclwtdkxx.getLoanstartdate().compareTo(xclwtdkxx.getLoanenddate()) > 0) {
                            if (xclwtdkxx1List.contains("JS2451")) {
                                tempMsg.append("委托贷款发放日期应小于等于委托贷款到期日期" + spaceStr);
                                isError = true;
                            }
                        }
                    }
                    if (StringUtils.isNotBlank(xclwtdkxx.getSjrq())) {
                        if (xclwtdkxx.getLoanstartdate().compareTo(xclwtdkxx.getSjrq()) > 0) {
                            if (xclwtdkxx1List.contains("JS2450")) {
                                tempMsg.append("委托贷款发放日期应小于等于数据日期" + spaceStr);
                                isError = true;
                            }
                        }
                    }

                } else {
                    if (xclwtdkxx1List.contains("JS0396")) {
                        tempMsg.append("委托贷款发放日期需早于1800-01-01晚于2100-12-31" + spaceStr);
                        isError = true;
                    }

                }

            } else {
                if (xclwtdkxx1List.contains("JS0396")) {
                    tempMsg.append("委托贷款发放日期不符合yyyy-MM-dd格式" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (xclwtdkxx1List.contains("JS1098")) {
                tempMsg.append("委托贷款发放日期" + nullError + spaceStr);
                isError = true;
            }
        }



        //#委托贷款余额
        if (StringUtils.isNotBlank(xclwtdkxx.getReceiptbalance())) {
            if (xclwtdkxx.getReceiptbalance().length() > 20 || !CheckUtil.checkDecimal(xclwtdkxx.getReceiptbalance(), 2)) {
                if (xclwtdkxx1List.contains("JS0391")) {
                    tempMsg.append("委托贷款余额总长度不能超过20位,小数位应保留2位" + spaceStr);
                    isError = true;
                }
            }
            if (Double.valueOf(xclwtdkxx.getReceiptbalance()) <= 0) {
                if (xclwtdkxx1List.contains("JS2453")) {
                    tempMsg.append("委托贷款余额应大于0" + spaceStr);
                    isError = true;
                }
            }

        } else {
            if (xclwtdkxx1List.contains("JS1102")) {
                tempMsg.append("委托贷款余额" + nullError + spaceStr);
                isError = true;
            }
        }

        //手续费金额折人民币
        if (StringUtils.isNotBlank(xclwtdkxx.getProcedurebalance())) {
            if (xclwtdkxx.getProcedurebalance().length() > 20 || !CheckUtil.checkDecimal(xclwtdkxx.getProcedurebalance(), 2)) {
                if (xclwtdkxx1List.contains("JS0394")) {
                    tempMsg.append("手续费金额折人民币总长度不能超过20位，小数位必须为2位" + spaceStr);
                    isError = true;
                }
            }
            if (Double.valueOf(xclwtdkxx.getProcedurebalance()) <= 0) {
                if (xclwtdkxx1List.contains("JS2455")) {
                    tempMsg.append("手续费金额折人民币应大于0" + spaceStr);
                    isError = true;
                }
            }

        } else {
            if (xclwtdkxx1List.contains("JS1106")) {
                tempMsg.append("手续费金额折人民币" + nullError + spaceStr);
                isError = true;
            }
        }



        //#委托贷款余额折人民币
        if (StringUtils.isNotBlank(xclwtdkxx.getReceiptcnybalance())) {
            if (xclwtdkxx.getReceiptcnybalance().length() > 20 || !CheckUtil.checkDecimal(xclwtdkxx.getReceiptcnybalance(), 2)) {
                if (xclwtdkxx1List.contains("JS0392")) {
                    tempMsg.append("委托贷款余额折人民币总长度不能超过20位，小数位必须为2位" + spaceStr);
                    isError = true;
                }
            }
            if (Double.valueOf(xclwtdkxx.getReceiptcnybalance()) <= 0) {
                if (xclwtdkxx1List.contains("JS2454")) {
                    tempMsg.append("委托贷款余额折人民币应大于0" + spaceStr);
                    isError = true;
                }
            }

            if (xclwtdkxx.getCurrency().equals("CNY")) {
                if (!xclwtdkxx.getReceiptbalance().equals(xclwtdkxx.getReceiptcnybalance())) {
                    if (xclwtdkxx1List.contains("JS2693")) {
                        tempMsg.append("币种为人民币的，委托贷款余额折人民币应该与委托贷款余额的值相等" + spaceStr);
                        isError = true;
                    }
                }
            }
        } else {
            if (xclwtdkxx1List.contains("JS1103")) {
                tempMsg.append("委托贷款余额折人民币" + nullError + spaceStr);
                isError = true;
            }

        }
        //#贷款利率重新定价日


        //#贷款展期到期日期 ========================待定  因为找不到资产负债类型字段
        if (StringUtils.isNotBlank(xclwtdkxx.getExtensiondate())) {
            boolean b = CheckUtil.checkDate(xclwtdkxx.getExtensiondate(), "yyyy-MM-dd");
            if (b) {
                if (xclwtdkxx.getExtensiondate().compareTo("1800-01-01") >= 0 && xclwtdkxx.getExtensiondate().compareTo("2100-12-31") <= 0) {

                } else {
                    if (xclwtdkxx1List.contains("JS0398")) {
                        tempMsg.append("委托贷款展期到期日期需晚于1800-01-01早于2100-12-31" + spaceStr);
                        isError = true;
                    }
                }
            } else {
                if (xclwtdkxx1List.contains("JS0398")) {
                    tempMsg.append("委托贷款展期到期日期不符合yyyy-MM-dd格式" + spaceStr);
                    isError = true;
                }
            }

        } else {
            if (StringUtils.isNotBlank(xclwtdkxx.getLoanstatus()) && "LS02".equals(xclwtdkxx.getLoanstatus())) {
                if(!StringUtils.isNotBlank(xclwtdkxx.getExtensiondate())){
                            if (xclwtdkxx1List.contains("JS1530"))
                            {
                                tempMsg.append("当贷款状态为展期时，委托贷款展期到期日期不能为空" + spaceStr);
                                isError = true;
                            }
                        }
            }
        }


        //#贷款质量
        if (StringUtils.isNotBlank(xclwtdkxx.getLoanquality())) {
            if (!ArrayUtils.contains(CheckUtil.dkzl, xclwtdkxx.getLoanquality())) {
                if (xclwtdkxx1List.contains("JS0142")) {
                    tempMsg.append("贷款质量不符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }

        }
//        } else if(StringUtils.isNotBlank(xclwtdkxx.getProductcetegory())&&xclwtdkxx.getProductcetegory().length()>2){
//            String sbr = xclwtdkxx.getProductcetegory().substring(0, 2);
//            String[] sbs = sbr.split(",");
//            if (!ArrayUtils.contains(sbs, CheckUtil.dklb)) {
//                if (!StringUtils.isNotBlank(xclwtdkxx.getLoanquality())) {
//                    if (xclwtdkxx1List.contains("JS1646"))
//                    {
//                        tempMsg.append("当贷款产品类别为资产类贷款时，贷款质量不能为空" + spaceStr);
//                        isError = true;
//                    }
//                }
//            }
//
//        } else {
////            tempMsg.append("贷款质量" + nullError + spaceStr);
////            isError = true;
//        }

        //#贷款状态
        if (StringUtils.isNotBlank(xclwtdkxx.getLoanstatus())) {
            if (!ArrayUtils.contains(CheckUtil.loanstatus, xclwtdkxx.getLoanstatus())) {
                if (xclwtdkxx1List.contains("JS0143")) {
                    tempMsg.append("贷款状态不符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }

            if (StringUtils.isNotBlank(xclwtdkxx.getSjrq())) {
                if (StringUtils.isNotBlank(xclwtdkxx.getExtensiondate())) {
                    if (xclwtdkxx.getSjrq().compareTo(xclwtdkxx.getExtensiondate()) >= 1) {
                        if (!xclwtdkxx.getLoanstatus().equals("LS03")) {
                            if (xclwtdkxx1List.contains("JS2046")) {
                                tempMsg.append("逾期委托贷款的贷款状态应该为LS03-逾期" + spaceStr);
                                isError = true;
                            }
                        }
                    }
                } else if (StringUtils.isNotBlank(xclwtdkxx.getLoanenddate())) {
                    if (xclwtdkxx.getSjrq().compareTo(xclwtdkxx.getLoanenddate()) >= 1) {
                        if (!xclwtdkxx.getLoanstatus().equals("LS03")) {
                            if (xclwtdkxx1List.contains("JS2046")) {
                                tempMsg.append("逾期委托贷款的贷款状态应该为LS03-逾期" + spaceStr);
                                isError = true;
                            }
                        }
                    }
                }
            }

            if (StringUtils.isNotBlank(xclwtdkxx.getExtensiondate())) {
                if (xclwtdkxx.getExtensiondate().compareTo(xclwtdkxx.getSjrq()) >= 0) {
                    if (!xclwtdkxx.getLoanstatus().equals("LS02") && !xclwtdkxx.getLoanstatus().equals("LS04") && !xclwtdkxx.getLoanstatus().equals("LS03")) {
                        if (xclwtdkxx1List.contains("JS2047")) {
                            tempMsg.append("展期委托贷款的贷款状态应该为LS02-展期 或者 LS04-缩期 或者LS03-逾期" + spaceStr);
                            isError = true;
                        }
                    }
                }
            }
            if (!StringUtils.isNotBlank(xclwtdkxx.getExtensiondate())) {
                if (xclwtdkxx.getLoanstatus().equals("LS02")) {
                    if (xclwtdkxx1List.contains("JS2142")) {
                        tempMsg.append("如果贷款展期日期为空，则贷款状态不能为LS02-展期" + spaceStr);
                        isError = true;
                    }
                }
            }


        } else {
            if (xclwtdkxx1List.contains("JS1108")) {
                tempMsg.append("贷款状态" + nullError + spaceStr);
                isError = true;
            }
        }

        //#借款人地区代码
        if (StringUtils.isNotBlank(xclwtdkxx.getBrrowerareacode())) {

//            if (xclwtdkxx.getBrrowerareacode().length() == 6) {
            if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
            if (countryList == null) countryList = customSqlUtil.getBaseCode(BaseCountry.class);
            if (!areaList.contains(xclwtdkxx.getBrrowerareacode()) &&
                    !countryList.contains(xclwtdkxx.getBrrowerareacode())) {
                if (xclwtdkxx1List.contains("JS0135")) {
                    tempMsg.append("借款人地区代码不在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
//            } else {
//                tempMsg.append("借款人地区代码长度不等于6" + spaceStr);
//                isError = true;
//            }

//            if (StringUtils.isNotBlank(xclwtdkxx.getIsgreenloan())) {
//                if (xclwtdkxx.getIsgreenloan().startsWith("E")) {
//                    if (!xclwtdkxx.getBrrowerareacode().startsWith("000")) {
//                        if (xclwtdkxx1List.contains("JS1936")) {
//                            tempMsg.append("借款人国民经济部门为E开头的非居民部门，则借款人地区代码应为000+对应的3位数字国别代码" + spaceStr);
//                            isError = true;
//                        }
//                    }
//                } else {
//                    if (xclwtdkxx.getBrrowerareacode().startsWith("000")) {
//                        if (xclwtdkxx1List.contains("JS1937")) {
//                            tempMsg.append("借款人国民经济部门不是E开头的非居民部门，则借款人地区代码不能为000开头" + spaceStr);
//                            isError = true;
//                        }
//                    }
//                }
//            }


//            if (StringUtils.isNotBlank(xclwtdkxx.getBrrowerindustry())) {
//                if (xclwtdkxx.getBrrowerindustry().startsWith("200")) {
//                    if (!xclwtdkxx.getBrrowerareacode().startsWith("000")) {
//                        if (xclwtdkxx1List.contains("JS2136")) {
//                            tempMsg.append("借款人行业为境外的，借款人地区代码应该以000开头" + spaceStr);
//                            isError = true;
//                        }
//                    }
//                } else {
//                    if (xclwtdkxx.getBrrowerareacode().startsWith("000")) {
//                        if (xclwtdkxx1List.contains("JS2137")) {
//                            tempMsg.append("借款人行业为境内的，借款人地区代码不能以000开头" + spaceStr);
//                            isError = true;
//                        }
//                    }
//                }
//            }
            /*JDBCUtils.getConnection();
            String query4 ="select * from xclwtdkxx a inner join xftykhx b on  a.brroweridnum=b.customercode and a.sjrq=b.sjrq where a.brrowerareacode != b.regareacode ='"+xclwtdkxx.getFinanceorgareacode()+"'and a.checkstatus='0'" ;
            ResultSet result4 =JDBCUtils.Query(query4);
            if(result4!=null){
                tempMsg.append("借款人地区代码与非同业单位客户基础信息的地区代码应该一致" + spaceStr);
                isError = true;

            }
            JDBCUtils.close();*/

        } else {
            if (xclwtdkxx1List.contains("JS1093")) {
                tempMsg.append("借款人地区代码" + nullError + spaceStr);
                isError = true;
            }
        }

        //#贷款实际投向
        if (StringUtils.isNotBlank(xclwtdkxx.getLoanactualdirection())) {
            if (bindustryList == null) bindustryList = customSqlUtil.getBaseCode(BaseBindustry.class);
            if (!bindustryList.contains(xclwtdkxx.getLoanactualdirection())) {
                if (xclwtdkxx1List.contains("JS0138")) {
                    tempMsg.append("贷款实际投向代码不在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }


        } else {
            if (xclwtdkxx1List.contains("JS1100")) {
                tempMsg.append("贷款实际投向不能为空" + spaceStr);
                isError = true;
            }
        }
//        } else if(StringUtils.isNotBlank(xclwtdkxx.getProductcetegory())&&xclwtdkxx.getProductcetegory().length()>2)
//        {
//            String wer = xclwtdkxx.getProductcetegory().substring(0, 3);
//            if (!ArrayUtils.contains(CheckUtil.dklb,wer)) {
//                if(StringUtils.isBlank(xclwtdkxx.getLoanactualdirection())) {
//                    if (xclwtdkxx1List.contains("JS1009"))
//                    {
//                        tempMsg.append("当贷款产品类别为资产类贷款时，贷款实际投向不能为空" + spaceStr);
//                        isError = true;
//                    }
//                }
//            }
//
//        }
//        else {
//            tempMsg.append("贷款实际投向代码" + nullError + spaceStr);
//            isError = true;
//        }


        //#借款人行业
        if (StringUtils.isNotBlank(xclwtdkxx.getBrrowerindustry())) {
            if (xclwtdkxx.getBrrowerindustry().length() == 3) {
                if (aindustryList == null) aindustryList = customSqlUtil.getBaseCode(BaseAindustry.class);
                if (!aindustryList.contains(xclwtdkxx.getBrrowerindustry())) {
                    if (xclwtdkxx1List.contains("JS0134")) {
                        tempMsg.append("借款人行业不在行业标准大类范围内" + spaceStr);
                        isError = true;
                    }
                }
            }
        } else {
            if (xclwtdkxx1List.contains("JS1092")) {
                tempMsg.append("借款人行业" + nullError + spaceStr);
                isError = true;
            }

        }

        //#借款人证件代码
        if (StringUtils.isNotBlank(xclwtdkxx.getBrroweridnum())) {
            if (CheckUtil.checkStr(xclwtdkxx.getBrroweridnum())) {
                    if (xclwtdkxx1List.contains("JS0378")) {
                        tempMsg.append("借款人证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                        isError = true;
                    }
                }

                if (StringUtils.isNotBlank(xclwtdkxx.getIsfarmerloan())){
                    if (ArrayUtils.contains(CheckUtil.wtrzjlx2,xclwtdkxx.getIsfarmerloan())){
                        try {
                            if (StringUtils.isNotBlank(xclwtdkxx.getSjrq())&&xclwtdkxx.getBrroweridnum().substring(6,14).compareTo(xclwtdkxx.getSjrq().replace("-","")) >=0){
                                if (xclwtdkxx1List.contains("JS2677")){
                                    tempMsg.append("证件类型为B01-身份证或者B08-临时身份证的，第7-14位的截取结果应该<数据日期"+spaceStr);
                                    isError=true;
                                }

                            }
                        }catch (Exception e){
                            if (xclwtdkxx1List.contains("JS2677")){
                                tempMsg.append("证件类型为B01-身份证或者B08-临时身份证的，第7-14位的截取结果应该<数据日期"+spaceStr);
                                isError=true;
                            }
                        }
                    }

                    if (ArrayUtils.contains(CheckUtil.wtrzjlx2,xclwtdkxx.getIsfarmerloan())){
                        if (TuoMinUtils.getB01(xclwtdkxx.getBrroweridnum()).length()!=46){
                            if (xclwtdkxx1List.contains("JS2657")) {
                                tempMsg.append("借款人证件类型为B01-身份证或者B08-临时身份证的，借款人证件代码脱敏后的长度应该为46" + spaceStr);
                                isError = true;
                            }
                        }
                    }

                    if (xclwtdkxx.getIsfarmerloan().startsWith("B") && !ArrayUtils.contains(CheckUtil.wtrzjlx2,xclwtdkxx.getIsfarmerloan())){
                        if (TuoMinUtils.getB01otr(xclwtdkxx.getBrroweridnum()).length() != 32){
                            if (xclwtdkxx1List.contains("JS2667")) {
                                tempMsg.append("借款人证件类型为B开头的个人证件且不是B01-身份证和B08-临时身份证的，借款人证件代码脱敏后的长度应该为32" + spaceStr);
                                isError = true;
                            }
                        }
                    }



                }

                if (StringUtils.isNotBlank(xclwtdkxx.getBrrowerareacode()) && StringUtils.isNotBlank(xclwtdkxx.getIsfarmerloan())) {
                        if (xclwtdkxx.getBrroweridnum().length() > 60) {
                            if (xclwtdkxx1List.contains("JS0386")) {
                                tempMsg.append("借款人证件代码字符长度不能超过60" + spaceStr);
                                isError = true;
                            }
                        }

                    if (!xclwtdkxx.getBrrowerareacode().startsWith("000") && xclwtdkxx.getIsfarmerloan().equals("A01")) {
                        if (xclwtdkxx.getBrroweridnum().length() != 18) {
                            if (xclwtdkxx1List.contains("JS0892")) {
                                tempMsg.append("境内对公客户且借款人证件类型为A01的，借款人证件代码字符长度应该为18位" + spaceStr);
                                isError = true;
                            }
                        }
                    }

                    if (!xclwtdkxx.getBrrowerareacode().startsWith("000") && xclwtdkxx.getIsfarmerloan().equals("A02")) {
                        if (xclwtdkxx.getBrroweridnum().length() != 9) {
                            if (xclwtdkxx1List.contains("JS2791")) {
                                tempMsg.append("境内对公客户且借款人证件类型为A02的，借款人证件代码字符长度应该为9位" + spaceStr);
                                isError = true;
                            }
                        }
                    }


                }

        } else {
            if (xclwtdkxx1List.contains("JS1091")) {
                tempMsg.append("借款人证件代码不能为空" + spaceStr);
                isError = true;
            }
        }
        //#查重在外层写#end
        //#内部机构号
        if (StringUtils.isNotBlank(xclwtdkxx.getFinanceorginnum())) {
            if (CheckUtil.checkStr(xclwtdkxx.getFinanceorginnum())) {
                if (xclwtdkxx1List.contains("JS0377")) {
                    tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }
            if (xclwtdkxx.getFinanceorginnum().length() > 30) {
                if (xclwtdkxx1List.contains("JS0385")) {
                    tempMsg.append("内部机构号字符长度不能超过30" + spaceStr);
                    isError = true;
                }
            }

        } else {
            if (xclwtdkxx1List.contains("JS1088")) {
                tempMsg.append("内部机构号不能为空" + spaceStr);
                isError = true;
            }
        }

        //#利率是否固定
        if (StringUtils.isNotBlank(xclwtdkxx.getInterestisfixed())) {
            if (xclwtdkxx1List.contains("JS0140")) {
                isError = CheckUtil.equalNullAndCode(xclwtdkxx.getInterestisfixed(), 4, Arrays.asList(CheckUtil.llsfgd), tempMsg, isError, "利率是否固定");
            }
        } else {
            if (xclwtdkxx1List.contains("JS1104")) {
                tempMsg.append("利率是否固定不能为空" + spaceStr);
                isError = true;
            }
        }


        //#利率水平
        if (StringUtils.isNotBlank(xclwtdkxx.getInterestislevel())) {
            if (CheckUtil.checkPerSign(xclwtdkxx.getInterestislevel())) {
                if (xclwtdkxx1List.contains("JS0382")) {
                    tempMsg.append("利率水平不能包含‰或%" + spaceStr);
                    isError = true;
                }
            }
            if (!CheckUtil.checkPerSign(xclwtdkxx.getInterestislevel())) {
                if (new BigDecimal(xclwtdkxx.getInterestislevel()).compareTo(BigDecimal.ZERO) < 0 || new BigDecimal(xclwtdkxx.getInterestislevel()).compareTo(new BigDecimal("30")) > 0) {
                    if (xclwtdkxx1List.contains("JS2456")) {
                        tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
                        isError = true;
                    }
                }

            }

            if (xclwtdkxx.getInterestislevel().length() <= 10 && CheckUtil.checkDecimal(xclwtdkxx.getInterestislevel(), 5)) {

            } else {
                if (xclwtdkxx1List.contains("JS0393")) {
                    tempMsg.append("利率水平总长度不能超过10位，小数位必须为5位" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (xclwtdkxx1List.contains("JS1105")) {
                tempMsg.append("利率水平" + nullError + spaceStr);
                isError = true;
            }
        }

        //#借款人经济成分
        if (StringUtils.isNotBlank(xclwtdkxx.getInverstoreconomy())) {
            if (!ArrayUtils.contains(CheckUtil.qyczrjjcf, xclwtdkxx.getInverstoreconomy())) {
                if (xclwtdkxx1List.contains("JS0136")) {
                    tempMsg.append("借款人经济成分不在符合要求的值域范围" + spaceStr);
                    isError = true;
                }
            }
        }
        if (StringUtils.isNotBlank(xclwtdkxx.getBrrowerindustry())) {
            if (!ArrayUtils.contains(CheckUtil.jkrhy, xclwtdkxx.getBrrowerindustry()) && StringUtils.isNotBlank(xclwtdkxx.getEnterprisescale()) && !xclwtdkxx.getEnterprisescale().equals("CS05")) {
                if (StringUtils.isBlank(xclwtdkxx.getInverstoreconomy())) {
                    if (xclwtdkxx1List.contains("JS1094")) {
                        tempMsg.append("当借款人不为个人、境外非居民以及境内非企业时，借款人经济成分不能为空" + spaceStr);
                        isError = true;
                    }
                }
            }

            if (ArrayUtils.contains(CheckUtil.jkrhy, xclwtdkxx.getBrrowerindustry()) ||  StringUtils.isNotBlank(xclwtdkxx.getEnterprisescale()) && xclwtdkxx.getEnterprisescale().equals("CS05")) {
                if (StringUtils.isNotBlank(xclwtdkxx.getInverstoreconomy())) {
                    if (xclwtdkxx1List.contains("JS2014")) {
                        tempMsg.append("当借款人为个人、境外非居民或境内非企业时，借款人经济成分必须为空" + spaceStr);
                        isError = true;
                    }
                }
            }
        }


        //#借款人企业规模
        if (StringUtils.isNotBlank(xclwtdkxx.getEnterprisescale())) {
            if (!ArrayUtils.contains(CheckUtil.qygm2, xclwtdkxx.getEnterprisescale())) {
                if (xclwtdkxx1List.contains("JS0137")) {
                    tempMsg.append("若借款人企业规模不为空，需在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }

            if (StringUtils.isNotBlank(xclwtdkxx.getIsgreenloan())) {
                if (xclwtdkxx.getIsgreenloan().startsWith("C") && !xclwtdkxx.getIsgreenloan().equals("C99")) {
                    if (!ArrayUtils.contains(CheckUtil.qygm, xclwtdkxx.getEnterprisescale())) {
                        if (xclwtdkxx1List.contains("JS2632")) {
                            tempMsg.append("借款人国民经济部门为C开头且不是C99的非金融企业部门，则借款人企业规模应该在CS01至CS04范围内");
                            isError = true;
                        }
                    }
                }
            }
        }

        if (StringUtils.isNotBlank(xclwtdkxx.getBrrowerindustry())) {
            if (!ArrayUtils.contains(CheckUtil.jkrhy, xclwtdkxx.getBrrowerindustry())) {
                if (StringUtils.isBlank(xclwtdkxx.getEnterprisescale())) {
                    if (xclwtdkxx1List.contains("JS1095")) {
                        tempMsg.append("当借款人不为个人和境外时，借款人企业规模不能为空" + spaceStr);
                        isError = true;
                    }
                }

            }

            if (xclwtdkxx.getBrrowerindustry().equals("100")) {
                if (StringUtils.isNotBlank(xclwtdkxx.getEnterprisescale())) {
                    if (xclwtdkxx1List.contains("JS2015")) {
                        tempMsg.append("个人客户的企业规模应为空" + spaceStr);
                        isError = true;
                    }
                }
            }
        }


        //#贷款用途
        if (StringUtils.isNotBlank(xclwtdkxx.getIssupportliveloan())) {
            if (CheckUtil.checkStr6(xclwtdkxx.getIssupportliveloan())) {
                if (xclwtdkxx1List.contains("JS0383")) {
                    tempMsg.append("当贷款用途不为空时，贷款用途字段内容中不得出现“？”、“！” 、“^”  。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }
            if (xclwtdkxx.getIssupportliveloan().length() > 3000) {
                if (xclwtdkxx1List.contains("JS0390")) {
                    tempMsg.append("当贷款用途不为空时，贷款用途字符长度不能超过3000" + spaceStr);
                    isError = true;
                }
            }
//            if (StringUtils.isNotBlank(xclwtdkxx.getProductcetegory())&&xclwtdkxx.getProductcetegory().length()>2) {
//                String qwe = xclwtdkxx.getProductcetegory().substring(0, 2);
//                String Qwe[] = qwe.split(",");
//                if (ArrayUtils.contains(Qwe, CheckUtil.dklb)) {
//                    if(!StringUtils.isNotBlank(xclwtdkxx.getIssupportliveloan())){
//                        if (xclwtdkxx1List.contains("JS1550"))
//                        {
//                            tempMsg.append("当贷款产品类别为资产类贷款时，贷款用途不能为空" + spaceStr);
//                            isError = true;
//                        }
//                    }
//
//                }
//             }

        }


        //#借款人国民经济部门
        if (xclwtdkxx1List.contains("JS0132")) {
            isError = checkEconamicSector(xclwtdkxx.getIsgreenloan(), xclwtdkxx1List, Arrays.asList(CheckUtil.wtjkrgmjjbm), tempMsg, isError, "借款人国民经济部门");
        }
        if (StringUtils.isNotBlank(xclwtdkxx.getIsgreenloan())) {
            if (ArrayUtils.contains(CheckUtil.qygm, xclwtdkxx.getEnterprisescale())) {
                if (!xclwtdkxx.getIsgreenloan().startsWith("C") && !xclwtdkxx.getIsgreenloan().startsWith("B")) {
                    if (xclwtdkxx1List.contains("JS2631")) {
                        tempMsg.append("借款人企业规模为CS01至CS04的，借款人国民经济部门应该为C开头的非金融企业部门或者B开头的金融机构" + spaceStr);
                        isError = true;
                    }
                }

            }

            if (StringUtils.isNotBlank(xclwtdkxx.getIsfarmerloan()) && xclwtdkxx.getIsfarmerloan().startsWith("B")) {
                if (!ArrayUtils.contains(CheckUtil.gmjjbmwt, xclwtdkxx.getIsgreenloan())) {
                    if (xclwtdkxx1List.contains("JS1980")) {
                        tempMsg.append("借款人证件类型为B开头的个人证件，则借款人国民经济部门为D01-住户或E05-非居民" + spaceStr);
                        isError = true;
                    }
                }

            }

        } else {
            if (xclwtdkxx1List.contains("JS1089")) {
                tempMsg.append("借款人国民经济部门不能为空" + spaceStr);
                isError = true;
            }
        }


        //#借款人证件类型
        if (StringUtils.isNotBlank(xclwtdkxx.getIsfarmerloan())) {
            if (!ArrayUtils.contains(CheckUtil.wtdkzjlx, xclwtdkxx.getIsfarmerloan())) {
                if (xclwtdkxx1List.contains("JS0133")) {
                    tempMsg.append("借款人证件类型不在符合要求的最底层值域范围内" + spaceStr);
                    isError = true;
                }
            }

//            if (StringUtils.isNotBlank(xclwtdkxx.getBrrowerareacode())) {
//                if (!xclwtdkxx.getBrrowerareacode().startsWith("000") && xclwtdkxx.getIsfarmerloan().equals("A02")) {
//                    if (StringUtils.isNotBlank(xclwtdkxx.getBrroweridnum()) && xclwtdkxx.getBrroweridnum().length() != 9) {
//                        if (xclwtdkxx1List.contains("JS2786")) {
//                            tempMsg.append("境内客户且借款人证件类型为A02的，借款人证件代码字符长度应该为9位" + spaceStr);
//                            isError = true;
//                        }
//                    }
//                }
//
//                if (!xclwtdkxx.getBrrowerareacode().startsWith("000") && xclwtdkxx.getIsfarmerloan().equals("A01")) {
//                    if (StringUtils.isNotBlank(xclwtdkxx.getBrroweridnum()) && xclwtdkxx.getBrroweridnum().length() != 18) {
//                        if (xclwtdkxx1List.contains("JS0890")) {
//                            tempMsg.append("境内客户且借款人证件类型为A01的，借款人证件代码字符长度应该为18位" + spaceStr);
//                            isError = true;
//                        }
//                    }
//
//                }
//                if (!xclwtdkxx.getBrrowerareacode().startsWith("000") && !xclwtdkxx.getIsfarmerloan().equals("A01")) {
//                    if (xclwtdkxx1List.contains("JS2795")) {
//                        tempMsg.append("境内客户的借款人证件类型应为A01-统一社会信用代码" + spaceStr);
//                        isError = true;
//                    }
//                }
//            }
        } else {
            if (xclwtdkxx1List.contains("JS1090")) {
                tempMsg.append("借款人证件类型不能为空" + spaceStr);
                isError = true;
            }
        }
        //#逾期类型  ========================待定  条件冲突
//        if (StringUtils.isNotBlank(xclwtdkxx.getLoanstatus()) && "LS03".equals(xclwtdkxx.getLoanstatus())) {
//            if (StringUtils.isNotBlank(xclwtdkxx.getOverduetype())) {
//                CheckUtil.equalNullAndCode(xclwtdkxx.getOverduetype(), 2, Arrays.asList(CheckUtil.jylx), tempMsg, isError, "借款人证件类型");
//            } else {
//                if (xclwtdkxx1List.contains("JS1025"))
//                {
//                    tempMsg.append("当贷款状态为逾期时，逾期类型不能为空" + spaceStr);
//                    isError = true;
//                }
//            }
//        } else {
//            if (StringUtils.isNotBlank(xclwtdkxx.getOverduetype())) {
//                if (xclwtdkxx1List.contains("JS0103")){
//                    CheckUtil.equalNullAndCode(xclwtdkxx.getOverduetype(), 2, Arrays.asList(CheckUtil.jylx), tempMsg, isError, "逾期类型");
//                }
//            }
//        }

//        if (StringUtils.isNotBlank(xclwtdkxx.getOverduetype())&&!xclwtdkxx.getLoanstatus().equals("LS03")){
//            if (xclwtdkxx1List.contains("JS2783"))
//            {
//                tempMsg.append("当贷款状态不为逾期时，逾期类型必须为空" + spaceStr);
//                isError = true;
//            }
//        }

        //委托人国民经济部门
        if (xclwtdkxx1List.contains("JS0144")) {
            isError = checkEconamicSector(xclwtdkxx.getBailorgreenloan(), xclwtdkxx1List, Arrays.asList(CheckUtil.wtjkrgmjjbm), tempMsg, isError, "委托人国民经济部门");
        }
        if (StringUtils.isNotBlank(xclwtdkxx.getBailorgreenloan())) {
            if (ArrayUtils.contains(CheckUtil.qygm, xclwtdkxx.getBailorgenterprise())) {
                if (!xclwtdkxx.getBailorgreenloan().startsWith("C") && !xclwtdkxx.getBailorgreenloan().startsWith("B")) {
                    if (xclwtdkxx1List.contains("JS2633")) {
                        tempMsg.append("委托人企业规模为CS01至CS04的，委托人国民经济部门应该为C开头的非金融企业部门或者B开头的金融机构" + spaceStr);
                        isError = true;
                    }
                }

            }

            if (StringUtils.isNotBlank(xclwtdkxx.getBailorgfarmerloan()) && ArrayUtils.contains(CheckUtil.wtrzjlx, xclwtdkxx.getBailorgfarmerloan())) {
                if (!xclwtdkxx.getBailorgreenloan().startsWith("E")) {
                    if (xclwtdkxx1List.contains("JS1986")) {
                        tempMsg.append("委托人证件类型为境外证件的，委托人国民经济部门应为E开头的非居民部门" + spaceStr);
                        isError = true;
                    }
                }
            }

//            if (xclwtdkxx.getBrrowerareacode().startsWith("000")) {
//                if (!xclwtdkxx.getBailorgreenloan().startsWith("E")) {
//                    if (xclwtdkxx1List.contains("JS1928")) {
//                        tempMsg.append("借款人地区代码为000开头的，国民经济部门应为E开头的非居民部门" + spaceStr);
//                        isError = true;
//                    }
//                }
//
//            } else {
//                if (xclwtdkxx.getBailorgreenloan().startsWith("E")) {
//                    if (xclwtdkxx1List.contains("JS1929")) {
//                        tempMsg.append("借款人地区代码不是000开头的，国民经济部门不应为E开头的非居民部门" + spaceStr);
//                        isError = true;
//                    }
//                }
//            }

        } else {
            if (xclwtdkxx1List.contains("JS1109")) {
                tempMsg.append("委托人国民经济部门不能为空" + spaceStr);
                isError = true;
            }
        }

        //委托人证件类型
        if (StringUtils.isNotBlank(xclwtdkxx.getBailorgfarmerloan())) {
            if (!ArrayUtils.contains(CheckUtil.wtdkzjlx2, xclwtdkxx.getBailorgfarmerloan())) {
                if (xclwtdkxx1List.contains("JS0145")) {
                    tempMsg.append("委托人证件类型不在符合要求的最底层值域范围内" + spaceStr);
                    isError = true;
                }
            }

//            if (StringUtils.isNotBlank(xclwtdkxx.getBrrowerareacode())) {
//                if (!xclwtdkxx.getBrrowerareacode().startsWith("000") && xclwtdkxx.getBailorgfarmerloan().equals("A02")) {
//                    if (StringUtils.isNotBlank(xclwtdkxx.getBrroweridnum()) && xclwtdkxx.getBrroweridnum().length() != 9) {
//                        if (xclwtdkxx1List.contains("JS2786")) {
//                            tempMsg.append("境内客户且借款人证件类型为A02的，借款人证件代码字符长度应该为9位" + spaceStr);
//                            isError = true;
//                        }
//                    }
//                }
//
//                if (!xclwtdkxx.getBrrowerareacode().startsWith("000") && xclwtdkxx.getBailorgfarmerloan().equals("A01")) {
//                    if (StringUtils.isNotBlank(xclwtdkxx.getBrroweridnum()) && xclwtdkxx.getBrroweridnum().length() != 18) {
//                        if (xclwtdkxx1List.contains("JS0890")) {
//                            tempMsg.append("境内客户且借款人证件类型为A01的，借款人证件代码字符长度应该为18位" + spaceStr);
//                            isError = true;
//                        }
//                    }
//
//                }
//                if (!xclwtdkxx.getBrrowerareacode().startsWith("000") && !xclwtdkxx.getBailorgfarmerloan().equals("A01")) {
//                    if (xclwtdkxx1List.contains("JS2795")) {
//                        tempMsg.append("境内客户的借款人证件类型应为A01-统一社会信用代码" + spaceStr);
//                        isError = true;
//                    }
//                }
//            }
        } else {
            if (xclwtdkxx1List.contains("JS1110")) {
                tempMsg.append("委托人证件类型不能为空" + spaceStr);
                isError = true;
            }
        }

        //委托人行业
        if (StringUtils.isNotBlank(xclwtdkxx.getBailorgindustry())) {
            if (aindustryList == null) aindustryList = customSqlUtil.getBaseCode(BaseAindustry.class);
            if (!aindustryList.contains(xclwtdkxx.getBailorgindustry())) {
                if (xclwtdkxx1List.contains("JS0146")) {
                    tempMsg.append("委托人行业不在行业标准大类范围内" + spaceStr);
                    isError = true;
                }
            }

            if (StringUtils.isNotBlank(xclwtdkxx.getBailorgreenloan())) {
                if (xclwtdkxx.getBailorgreenloan().startsWith("E")) {
                    if (!xclwtdkxx.getBailorgindustry().equals("200")) {
                        if (xclwtdkxx1List.contains("JS2028")) {
                            tempMsg.append("委托人国民经济部门为E开头的非居民部门，则委托人行业必须为200-境外" + spaceStr);
                            isError = true;
                        }
                    }
                }

                if (!xclwtdkxx.getBailorgreenloan().startsWith("E") && !xclwtdkxx.getBailorgreenloan().equals("D01")) {
                    if (ArrayUtils.contains(CheckUtil.jkrhy, xclwtdkxx.getBailorgindustry())) {
                        if (xclwtdkxx1List.contains("JS2030")) {
                            tempMsg.append("委托人国民经济部门不为E开头且不是D01-住户的，则委托人行业不能为100-个人和200-境外" + spaceStr);
                            isError = true;
                        }
                    }
                }

                if (xclwtdkxx.getBailorgreenloan().equals("D01")) {
                    if (!xclwtdkxx.getBailorgindustry().equals("100")) {
                        if (xclwtdkxx1List.contains("JS2029")) {
                            tempMsg.append("委托人国民经济部门为D01-住户，则委托人行业必须为100-个人" + spaceStr);
                            isError = true;
                        }
                    }
                }
            }
        } else {
            if (xclwtdkxx1List.contains("JS1112")) {
                tempMsg.append("委托人行业" + nullError + spaceStr);
                isError = true;
            }

        }


        //#委托人地区代码
        if (StringUtils.isNotBlank(xclwtdkxx.getBailorgareacode())) {
            if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
            if (countryList == null) countryList = customSqlUtil.getBaseCode(BaseCountry.class);
            if (!areaList.contains(xclwtdkxx.getBailorgareacode()) &&
                    !countryList.contains(xclwtdkxx.getBailorgareacode())) {
                if (xclwtdkxx1List.contains("JS0147")) {
                    tempMsg.append("委托人地区代码不在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }


//            if(StringUtils.isNotBlank(xclwtdkxx.getIsgreenloan())){
//                if (xclwtdkxx.getIsgreenloan().startsWith("E")) {
//                    if (!xclwtdkxx.getBailorgareacode().startsWith("000")) {
//                        if (xclwtdkxx1List.contains("JS1936"))
//                        {
//                            tempMsg.append("借款人国民经济部门为E开头的非居民部门，则借款人地区代码应为000+对应的3位数字国别代码" + spaceStr);
//                            isError = true;
//                        }
//                    }
//                } else {
//                    if (xclwtdkxx.getBailorgareacode().startsWith("000")) {
//                        if (xclwtdkxx1List.contains("JS1937"))
//                        {
//                            tempMsg.append("借款人国民经济部门不是E开头的非居民部门，则借款人地区代码不能为000开头" + spaceStr);
//                            isError = true;
//                        }
//                    }
//                }
//            }


//            if(StringUtils.isNotBlank(xclwtdkxx.getBrrowerindustry())){
//                if (xclwtdkxx.getBrrowerindustry().startsWith("200")) {
//                    if (!xclwtdkxx.getBailorgareacode().startsWith("000")) {
//                        if (xclwtdkxx1List.contains("JS2136"))
//                        {
//                            tempMsg.append("借款人行业为境外的，借款人地区代码应该以000开头" + spaceStr);
//                            isError = true;
//                        }
//                    }
//                } else {
//                    if (xclwtdkxx.getBailorgareacode().startsWith("000")) {
//                        if (xclwtdkxx1List.contains("JS2137"))
//                        {
//                            tempMsg.append("借款人行业为境内的，借款人地区代码不能以000开头" + spaceStr);
//                            isError = true;
//                        }
//                    }
//                }
//            }
        } else {
            if (xclwtdkxx1List.contains("JS1113")) {
            	if(StringUtils.isNotBlank(xclwtdkxx.getBailorgreenloan()) && !"B09".equals(xclwtdkxx.getBailorgreenloan())) {
            		tempMsg.append("当委托人国民经济部门不为特定目的载体时，委托人地区代码" + nullError + spaceStr);
                    isError = true;
            	}
            }
        }


        //#委托人经济成分
        if (StringUtils.isNotBlank(xclwtdkxx.getBailorgeconomy())) {
            if (!ArrayUtils.contains(CheckUtil.qyczrjjcf, xclwtdkxx.getBailorgeconomy())) {
                if (xclwtdkxx1List.contains("JS0148")) {
                    tempMsg.append("委托人经济成分不在符合要求的值域范围" + spaceStr);
                    isError = true;
                }
            }
        }

        if (StringUtils.isNotBlank(xclwtdkxx.getBailorgindustry()) && StringUtils.isNotBlank(xclwtdkxx.getBailorgenterprise()) && StringUtils.isNotBlank(xclwtdkxx.getBailorgreenloan())) {
            if (!ArrayUtils.contains(CheckUtil.jkrhy, xclwtdkxx.getBailorgindustry()) && !xclwtdkxx.getBailorgenterprise().equals("CS05") && !"B09".equals(xclwtdkxx.getBailorgreenloan())) {
                if (StringUtils.isBlank(xclwtdkxx.getBailorgeconomy())) {
                    if (xclwtdkxx1List.contains("JS1114")) {
                        tempMsg.append("当委托人不为个人、境外非居民、境内非企业以及特定目的载体时，委托人经济成分不能为空" + spaceStr);
                        isError = true;
                    }
                }
            }

            if (ArrayUtils.contains(CheckUtil.jkrhy, xclwtdkxx.getBailorgindustry()) || xclwtdkxx.getBailorgenterprise().equals("CS05") || "B09".equals(xclwtdkxx.getBailorgreenloan())) {
                if (StringUtils.isNotBlank(xclwtdkxx.getBailorgeconomy())) {
                    if (xclwtdkxx1List.contains("JS2038")) {
                        tempMsg.append("当委托人为个人、境外非居民、境内非企业或特定目的载体时，委托人经济成分必须为空" + spaceStr);
                        isError = true;
                    }
                }
            }
        }


        //#委托人企业规模
        if (StringUtils.isNotBlank(xclwtdkxx.getBailorgenterprise())) {
            if (!ArrayUtils.contains(CheckUtil.qygm2, xclwtdkxx.getBailorgenterprise())) {
                if (xclwtdkxx1List.contains("JS0149")) {
                    tempMsg.append("若委托人企业规模不为空，需在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }

            if (StringUtils.isNotBlank(xclwtdkxx.getBailorgreenloan())) {
                if (xclwtdkxx.getBailorgreenloan().startsWith("C") && !xclwtdkxx.getBailorgreenloan().equals("C99")) {
                    if (!ArrayUtils.contains(CheckUtil.qygm, xclwtdkxx.getBailorgenterprise())) {
                        if (xclwtdkxx1List.contains("JS2634")) {
                            tempMsg.append("委托人国民经济部门为C开头且不是C99的非金融企业部门，则委托人企业规模应该在CS01至CS04范围内");
                            isError = true;
                        }
                    }
                }
            }
        }

        if (StringUtils.isNotBlank(xclwtdkxx.getBailorgindustry())) {
            if (!ArrayUtils.contains(CheckUtil.jkrhy, xclwtdkxx.getBailorgindustry())) {
                if (StringUtils.isBlank(xclwtdkxx.getBailorgenterprise())) {
                    if (xclwtdkxx1List.contains("JS1115")) {
                        tempMsg.append("当委托人不为个人和境外非居民时，委托人企业规模不能为空" + spaceStr);
                        isError = true;
                    }
                }

            }

            if (xclwtdkxx.getBailorgindustry().equals("100")) {
                if (StringUtils.isNotBlank(xclwtdkxx.getBailorgenterprise())) {
                    if (xclwtdkxx1List.contains("JS2039")) {
                        tempMsg.append("个人委托人的企业规模应为空" + spaceStr);
                        isError = true;
                    }
                }
            }
        }



        //委托贷款借据编码
        if (StringUtils.isNotBlank(xclwtdkxx.getReceiptcode())){
            if (xclwtdkxx1List.contains("JS0379")) {
                if (CheckUtil.checkStr(xclwtdkxx.getReceiptcode())) {
                    tempMsg.append("委托贷款借据编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }

            if (xclwtdkxx.getReceiptcode().length()>100){
                if (xclwtdkxx1List.contains("JS0387")) {
                    tempMsg.append("委托贷款借据编码字符长度不能超过100");
                    isError = true;
                }
            }

        }else {
            if (xclwtdkxx1List.contains("JS1096")) {
                tempMsg.append("委托贷款借据编码不能为空");
                isError = true;
            }
        }

        //委托贷款合同编码
        if (StringUtils.isNotBlank(xclwtdkxx.getContractcode())){
                if (xclwtdkxx.getContractcode().length()<100){
                    if (CheckUtil.checkStr(xclwtdkxx.getContractcode())){
                        if (xclwtdkxx1List.contains("JS0380")) {
                            tempMsg.append("委托贷款合同编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。");
                            isError = true;
                        }
                    }
                }else {
                    if (xclwtdkxx1List.contains("JS0388")) {
                        tempMsg.append("委托贷款合同编码字符长度不能超过100");
                        isError = true;
                    }
                }

        }else {
            if (xclwtdkxx1List.contains("JS1097")) {
                tempMsg.append("委托贷款合同编码不能为空");
                isError = true;
            }
        }

        //委托人证件代码
        if (StringUtils.isNotBlank(xclwtdkxx.getBailorgidnum())){
            if (CheckUtil.checkStr(xclwtdkxx.getBailorgidnum())){
                if (xclwtdkxx1List.contains("JS0381")) {
                    tempMsg.append("委托人证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。");
                    isError = true;
                }
            }

            if (StringUtils.isNotBlank(xclwtdkxx.getBailorgareacode()) && StringUtils.isNotBlank(xclwtdkxx.getBailorgfarmerloan())){
                            if (xclwtdkxx.getBailorgidnum().length()>60){
                                if (xclwtdkxx1List.contains("JS0389")) {
                                    tempMsg.append("委托人证件代码字符长度不能超过60");
                                    isError = true;
                                }
                            }

                    if (!xclwtdkxx.getBailorgareacode().substring(0,3).equals("000") && xclwtdkxx.getBailorgfarmerloan().equals("A01")){
                        if (xclwtdkxx.getBailorgidnum().length() != 18){
                            if (xclwtdkxx1List.contains("JS0893")) {
                                tempMsg.append("境内对公客户且委托人证件类型为A01的，委托人证件代码字符长度应该为18位");
                                isError = true;
                            }
                        }
                    }

                    if (xclwtdkxx.getBailorgfarmerloan().equals("B01") || xclwtdkxx.getBailorgfarmerloan().equals("B01") ){
                        if (TuoMinUtils.getB01(xclwtdkxx.getBailorgidnum()).length() != 46){
                            if (xclwtdkxx1List.contains("JS2658")) {
                                tempMsg.append("委托人证件类型为B01-身份证或者B08-临时身份证的，委托人证件代码脱敏后的长度应该为46");
                                isError = true;
                            }
                        }
                    }

            }
            if (StringUtils.isNotBlank(xclwtdkxx.getBailorgfarmerloan())){
                if (xclwtdkxx.getBailorgfarmerloan().startsWith("B") && !ArrayUtils.contains(CheckUtil.wtrzjlx2,xclwtdkxx.getBailorgfarmerloan())){
                    if (TuoMinUtils.getB01otr(xclwtdkxx.getBailorgidnum()).length() != 32){
                        if (xclwtdkxx1List.contains("JS2668")) {
                            tempMsg.append("委托人证件类型为B开头的个人证件且不是B01-身份证和B08-临时身份证的，委托人证件代码脱敏后的长度应该为32" + spaceStr);
                            isError = true;
                        }
                    }
                }

                if (ArrayUtils.contains(CheckUtil.wtrzjlx2,xclwtdkxx.getBailorgfarmerloan())){
                    if (StringUtils.isNotBlank(xclwtdkxx.getSjrq())&&xclwtdkxx.getBailorgidnum().substring(6,14).compareTo(xclwtdkxx.getSjrq().replace("-","")) >=0){
                        if (xclwtdkxx1List.contains("JS2678")){
                            tempMsg.append("证件类型为B01-身份证或者B08-临时身份证的，第7-14位的截取结果应该<数据日期"+spaceStr);isError=true;
                        }

                    }
                }

                if (!xclwtdkxx.getBailorgareacode().startsWith("000") && xclwtdkxx.getBailorgfarmerloan().equals("A02")) {
                    if (xclwtdkxx.getBailorgidnum().length() != 9) {
                        if (xclwtdkxx1List.contains("JS2792")) {
                            tempMsg.append("境内对公客户且委托人证件类型为A02的，委托人证件代码字符长度应该为9位" + spaceStr);
                            isError = true;
                        }
                    }
                }
            }


        }else {
            if (xclwtdkxx1List.contains("JS1111")) {
                tempMsg.append("委托人证件代码不能为空");
                isError = true;
            }
        }




        if (isError) {
            if (!errorMsg.containsKey(xclwtdkxx.getId())) {
                errorMsg.put(xclwtdkxx.getId(), "委托贷款合同编码:" + xclwtdkxx.getContractcode() + "，" + "委托贷款借据编号:" + xclwtdkxx.getReceiptcode() + "]->\n");
            }
            String str = errorMsg.get(xclwtdkxx.getId());
            str = str + tempMsg;
            errorMsg.put(xclwtdkxx.getId(), str);
            errorId.add(xclwtdkxx.getId());
        } else {
            if (!errorId.contains(xclwtdkxx.getId())) {
                rightId.add(xclwtdkxx.getId());
            }
        }


    }


    public static Boolean nullAndDateCldkxx(String fieldValue, List<String> xclwtdkxx1List, StringBuffer errorMsg, boolean isError, String fieldName, boolean isHasNull) {
        if (StringUtils.isNotBlank(fieldValue)) {
            if (fieldValue.length() == 10) {
                boolean b1 = checkDate(fieldValue, "yyyy-MM-dd");
                if (b1) {
                    if (fieldValue.compareTo("1800-01-01") < 0 && fieldValue.compareTo("2100-12-31") > 0) {
                        if (xclwtdkxx1List.contains("JS0313")) {
                            errorMsg.append(fieldName + "需晚于1800-01-01早于2100-12-31|");
                            isError = true;
                        }
                    }
                } else {
                    if (xclwtdkxx1List.contains("JS0313")) {
                        errorMsg.append(fieldName + "不符合yyyy-MM-dd格式|");
                        isError = true;
                    }
                }
            } else {
                errorMsg.append(fieldName + "长度不等于10|");
                isError = true;
            }
        } else {
            if (!isHasNull) {
                if (xclwtdkxx1List.contains("JS1020")) {
                    errorMsg.append(fieldName + "不能为空|");
                    isError = true;
                }
            }

        }
        return isError;
    }

    public static Boolean nullAndDateCldkxx2(Xclgrdkxx xclgrdkxx, List<String> xclwtdkxx1List, StringBuffer errorMsg, boolean isError, String fieldName, boolean isHasNull) {
        if (StringUtils.isNotBlank(xclgrdkxx.getLoaninterestrepricedate()) && StringUtils.isNotBlank(xclgrdkxx.getProductcetegory()) && !xclgrdkxx.getProductcetegory().startsWith("F04")) {
            if (xclgrdkxx.getLoaninterestrepricedate().length() == 10) {
                boolean b1 = checkDate(xclgrdkxx.getLoaninterestrepricedate(), "yyyy-MM-dd");
                if (b1) {
                    if (xclgrdkxx.getLoaninterestrepricedate().compareTo("1800-01-01") < 0 && xclgrdkxx.getLoaninterestrepricedate().compareTo("2100-12-31") > 0) {
                        if (xclwtdkxx1List.contains("JS0924")) {
                            errorMsg.append(fieldName + "除透支类业务以外的需晚于1800-01-01早于2100-12-31|");
                            isError = true;
                        }
                    }
                } else {
                    if (xclwtdkxx1List.contains("JS0924")) {
                        errorMsg.append(fieldName + "除透支类业务以外的不符合yyyy-MM-dd格式|");
                        isError = true;
                    }

                }
            } else {
                errorMsg.append(fieldName + "长度不等于10|");
                isError = true;
            }
        } else {
            if (!isHasNull) {
                if (!ArrayUtils.contains(CheckUtil.tzyw, xclgrdkxx.getProductcetegory()) && StringUtils.isNotBlank(xclgrdkxx.getIssupportliveloan()) && !xclgrdkxx.getIssupportliveloan().contains("线上联合消费贷款")) {
                    if (xclwtdkxx1List.contains("JS1638")) {
                        errorMsg.append(fieldName + "除透支类业务以外不能为空|");
                        isError = true;
                    }
                }
            }

        }
        return isError;
    }

    /**
     * 校验日期
     *
     * @param date
     * @return
     */
    public static boolean checkDate(String date, String pattern) {
        boolean convertSuccess = true;
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            format.parse(date);
        } catch (Exception e) {
            convertSuccess = false;
        }
        return convertSuccess;
    }

    public static boolean checkEconamicSector(String fieldValue, List<String> xclwtdkxx1List, List<String> list, StringBuffer errorMsg, boolean isError, String fieldName) {
        if (StringUtils.isNotBlank(fieldValue)) {
            if (!list.contains(fieldValue)) {
                errorMsg.append(fieldName + "不在规定的代码范围内|");
                isError = true;
            }
        }
        return isError;
    }

    public static Boolean equalNullAndCode(String fieldValue, List<String> xclwtdkxx1List, int length, List<String> list, StringBuffer errorMsg, boolean isError, String fieldName) {
        if (StringUtils.isNotBlank(fieldValue)) {
            if (fieldValue.length() == length) {
                if (!list.contains(fieldValue)) {
                    if (xclwtdkxx1List.contains("JS0100") || xclwtdkxx1List.contains("JS0223")) {
                        errorMsg.append(fieldName + "不在规定的代码范围内|");
                        isError = true;
                    }
                }
            } else {
                errorMsg.append(fieldName + "长度不等于" + length + "|");
                isError = true;
            }
        } else {
            if (xclwtdkxx1List.contains("JS1022") || xclwtdkxx1List.contains("JS1640")) {
                errorMsg.append(fieldName + "不能为空|");
                isError = true;
            }
        }
        return isError;
    }
}

