package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.dataDictionary.ZqFieldUtils;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.report.Xclzqfxxx;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/***
 *   存量债券发行信息 row校验处理器
 * @author liang.xu
 * @date 2021.4.10
 */
@Service
public class CheckXclzqfxxxRowData implements CheckRowData {
    @Autowired
    private CustomSqlUtil customSqlUtil;

    @Override
    public void checkRow(String header, CheckParamContext checkParamContext, Object object) {
        Xclzqfxxx xclzqfxxx = (Xclzqfxxx) object;
        StringBuffer tempMsg = new StringBuffer();
        List<String> checknumList = checkParamContext.getCheckNums();
        Boolean isError = false;
        //1.金融机构属性校验
        if (checkXclzqfxxxRowFinanceorgcode(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //2.债券代码属性校验
        if (checkXclzqfxxxRowZqdm(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //3.债券总托管机构属性校验
        if (checkXclzqfxxxRowZqztgjg(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //4.债券品种属性校验
        if (checkXclzqfxxxRowZqpz(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //5.债券信用级别属性校验
        if (checkXclzqfxxxRowZqxyjg(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //6.续发次数属性校验
        if (checkXclzqfxxxRowXfcs(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //7.币种属性校验
        if (checkXclzqfxxxRowBz(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //8.期末债券面值属性校验
        if (checkXclzqfxxxRowQmzqmz(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //9.期末债券面值折人民币属性校验
        if (checkXclzqfxxxRowQmzqmzzrmb(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //10.期末债券余额属性校验
        if (checkXclzqfxxxRowQmzqye(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //11.期末债券余额折人民币属性校验
        if (checkXclzqfxxxRowQmzqyezrmb(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //12.债权债务登记日属性校验
        if (checkXclzqfxxxRowZqzwdj(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //13.起息日属性校验
        if (checkXclzqfxxxRowQxr(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //14.兑付日期属性校验
        if (checkXclzqfxxxRowDfrq(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //15.付息方式属性校验
        if (checkXclzqfxxxRowFxfs(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //16.票面利率属性校验
        if (checkXclzqfxxxRowPmll(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //17.数据日期
        if (checkXclzqfxxxRowSjrq(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        CheckHelper checkHelper = (CheckHelper) SpringContextUtil.getBean(CheckHelper.class);
        //单条实体交验完,处理结果
        checkHelper.handleRowCheckResult(isError, xclzqfxxx.getId(), String.format(header, xclzqfxxx.getFinanceorgcode(), xclzqfxxx.getZqdm()), tempMsg.toString(), checkParamContext);
    }

    private boolean checkXclzqfxxxRowSjrq(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        String sjrq = xclzqfxxx.getSjrq();//数据日期
        if (StringUtils.isNotBlank(sjrq)){
            if (sjrq.length()==10){
                boolean b1 = CheckUtil.checkDate(sjrq, "yyyy-MM-dd");
                if (b1){
                    if (sjrq.compareTo("1800-01-01")<0 || sjrq.compareTo("2100-12-31")>0){
                        tempMsg.append("数据日期需晚于1800-01-01早于2100-12-31" + SysConstants.spaceStr);
                        result = true;
                    }
                }else {
                    tempMsg.append("数据日期不符合yyyy-MM-dd格式" + SysConstants.spaceStr);
                    result = true;
                }
            }else {
                tempMsg.append("数据日期长度不等于10" + SysConstants.spaceStr);
                result = true;
            }
        }else {
            tempMsg.append("数据日期不能为空" + SysConstants.spaceStr);
            result = true;
        }
        return result;
    }

    //16.票面利率属性校验
    private boolean checkXclzqfxxxRowPmll(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getPmll())) {
            if (checknumList.contains("JS0738")) {
                if (xclzqfxxx.getPmll().contains("‰") || xclzqfxxx.getPmll().contains("%")) {
                    tempMsg.append("当票面利率不为空时，票面利率不能包含‰或%" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS0746")) {
                if (xclzqfxxx.getPmll().length() > 10 || !CheckUtil.checkDecimal(xclzqfxxx.getPmll(), 5)) {
                    tempMsg.append("当票面利率不为空时，票面利率总长度不能超过10位，小数位必须为5位" + SysConstants.spaceStr);
                    result = true;
                }
            }
            String pmll = xclzqfxxx.getPmll();
            BigDecimal bigDecimal1 = new BigDecimal(0);
            BigDecimal bigDecimal2 = new BigDecimal(30);
            if (checknumList.contains("JS2271")) {
                if (StringUtils.isNotBlank(pmll)) {
                    try {
                        if (new BigDecimal(pmll).compareTo(bigDecimal1) < 0 || new BigDecimal(pmll).compareTo(bigDecimal2) > 0) {
                            result = true;
                            tempMsg.append("当票面利率不为空时，应大于等于0且小于等于30" + SysConstants.spaceStr);
                        }
                    } catch (Exception e) {
                        result = true;
                        tempMsg.append("当票面利率不为空时，应大于等于0且小于等于30" + SysConstants.spaceStr);
                    }
                }
            }
        } else {
            if (checknumList.contains("JS1394") && StringUtils.isNotBlank(xclzqfxxx.getFxfs())) {
                //付息方式 in ('03','04','05')
                List<String> limit = Arrays.asList("03", "04", "05");
                if (limit.contains(xclzqfxxx.getFxfs())) {
                    tempMsg.append("当付息方式为利随本清、附息式固定利率、附息式浮动利率时，票面利率不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    //15.付息方式属性校验
    private boolean checkXclzqfxxxRowFxfs(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getFxfs())) {
            if (checknumList.contains("JS0044")) {
                String fxfs = xclzqfxxx.getFxfs();
                if (!ZqFieldUtils.fxfsList.contains(fxfs)) {
                    result = true;
                    tempMsg.append("存量债券发行信息中的付息方式需在符合要求的值域范围内" + SysConstants.spaceStr);
                }
            }
        } else {
            if (checknumList.contains("JS1393")) {
                if (StringUtils.isBlank(xclzqfxxx.getFxfs())) {
                    tempMsg.append("付息方式不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 兑付日期属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowDfrq(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getDfrq())) {
            if (checknumList.contains("JS0750")) {
                boolean ckDfrqDate = CheckUtil.checkDate(xclzqfxxx.getDfrq(), SysConstants.yyyyMMdd);
                if (!ckDfrqDate || !(xclzqfxxx.getDfrq().compareTo("1800-01-01") > 0 && xclzqfxxx.getDfrq().compareTo("2100-12-31") < 0)) {
                    tempMsg.append("兑付日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1391")) {
                if (StringUtils.isBlank(xclzqfxxx.getDfrq())) {
                    tempMsg.append("兑付日期不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 债权债务登记日属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowZqzwdj(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getZqzwdj())) {
            if (checknumList.contains("JS0748")) {
                boolean ckZqzwdjDate = CheckUtil.checkDate(xclzqfxxx.getZqzwdj(), SysConstants.yyyyMMdd);
                if (!ckZqzwdjDate || !(xclzqfxxx.getZqzwdj().compareTo("1800-01-01") > 0 && xclzqfxxx.getZqzwdj().compareTo("2100-12-31") < 0)) {
                    tempMsg.append("债权债务登记日必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
                    result = true;
                }
            }
//            if (checknumList.contains("JS2267")) {
//                if (StringUtils.isNotBlank(xclzqfxxx.getQxr()) && CheckUtil.checkDate(xclzqfxxx.getQxr(), SysConstants.yyyyMMdd) && xclzqfxxx.getZqzwdj().compareTo(xclzqfxxx.getQxr()) > 0) {
//                    tempMsg.append("债权债务登记日应小于等于起息日" + SysConstants.spaceStr);
//                    result = true;
//                }
//            }
            if (checknumList.contains("JS2266")) {
                if (StringUtils.isNotBlank(xclzqfxxx.getSjrq()) && CheckUtil.checkDate(xclzqfxxx.getSjrq(), SysConstants.yyyyMMdd) && xclzqfxxx.getZqzwdj().compareTo(xclzqfxxx.getSjrq()) > 0) {
                    tempMsg.append("债权债务登记日应小于等于数据日期" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1389")) {
                if (StringUtils.isBlank(xclzqfxxx.getZqzwdj())) {
                    tempMsg.append("债权债务登记日不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 期末债券余额折人民币属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowQmzqyezrmb(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getQmzqyezrmb())) {
            if (checknumList.contains("JS0744")) {
                if (xclzqfxxx.getQmzqyezrmb().length() > 20 || !CheckUtil.checkDecimal(xclzqfxxx.getQmzqyezrmb(), 2)) {
                    tempMsg.append("期末债券余额折人民币总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS2265")&&StringUtils.isNotBlank(xclzqfxxx.getQmzqye())) {
                if (new BigDecimal(xclzqfxxx.getQmzqye()).compareTo(BigDecimal.ZERO) <= 0) {
                    tempMsg.append("期末债券余额折人民币应大于0" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS2701")) {
                String bz = xclzqfxxx.getBz();
                String qmzqye = xclzqfxxx.getQmzqye();
                String qmzqyezrmb = xclzqfxxx.getQmzqyezrmb();
                if(StringUtils.isNotBlank(bz)&&StringUtils.isBlank(qmzqyezrmb)&&StringUtils.isBlank(qmzqye)) {
                    if (bz.equals("CNY") && !qmzqye.equals(qmzqyezrmb)) {
                        tempMsg.append("币种为人民币的，期末债券余额折人民币应该与期末债券余额相等" + SysConstants.spaceStr);
                        result = true;
                    }
                }
            }
        } else {
            if (checknumList.contains("JS1388")) {
                if (StringUtils.isBlank(xclzqfxxx.getQmzqyezrmb())) {
                    tempMsg.append("期末债券余额折人民币不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 期末债券余额属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowQmzqye(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getQmzqye())) {
            if (checknumList.contains("JS0743")) {
                if (xclzqfxxx.getQmzqye().length() > 20 || !CheckUtil.checkDecimal(xclzqfxxx.getQmzqye(), 2)) {
                    tempMsg.append("期末债券余额总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS2264")) {
                if (new BigDecimal(xclzqfxxx.getQmzqye()).compareTo(BigDecimal.ZERO) <= 0) {
                    tempMsg.append("期末债券余额应大于0" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1387")) {
                if (StringUtils.isBlank(xclzqfxxx.getQmzqye())) {
                    tempMsg.append("期末债券余额不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 期末债券面值折人民币属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowQmzqmzzrmb(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getQmzqmzzrmb())) {
            if (checknumList.contains("JS0742")) {
                if (xclzqfxxx.getQmzqmzzrmb().length() > 20 || !CheckUtil.checkDecimal(xclzqfxxx.getQmzqmzzrmb(), 2)) {
                    tempMsg.append("期末债券面值折人民币总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS2263")) {
                if (new BigDecimal(xclzqfxxx.getQmzqmzzrmb()).compareTo(BigDecimal.ZERO) <= 0) {
                    tempMsg.append("期末债券面值折人民币应大于0" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS2700")) {
                if (StringUtils.isNotBlank(xclzqfxxx.getQmzqmz()) && !(xclzqfxxx.getQmzqmzzrmb().equals(xclzqfxxx.getQmzqmz()))) {
                    tempMsg.append("币种为人民币的，期末债券面值折人民币应该与期末债券面值相等" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1386")) {
                if (StringUtils.isBlank(xclzqfxxx.getQmzqmzzrmb())) {
                    tempMsg.append("期末债券面值折人民币不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 期末债券面值属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowQmzqmz(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getQmzqmz())) {
            if (checknumList.contains("JS0741")) {
                if (xclzqfxxx.getQmzqmz().length() > 20 || !CheckUtil.checkDecimal(xclzqfxxx.getQmzqmz(), 2)) {
                    tempMsg.append("期末债券面值总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS2262")) {
                if (new BigDecimal(xclzqfxxx.getQmzqmz()).compareTo(BigDecimal.ZERO) <= 0) {
                    tempMsg.append("期末债券面值应大于0" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1385")) {
                if (StringUtils.isBlank(xclzqfxxx.getQmzqmz())) {
                    tempMsg.append("期末债券面值不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 币种属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowBz(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        String bz = xclzqfxxx.getBz();
        if (StringUtils.isNotBlank(xclzqfxxx.getBz())) {
            if (checknumList.contains("JS0043") && StringUtils.isNotBlank(bz)) {
                List baseCode = customSqlUtil.getBaseCode(BaseCurrency.class);
                if (!baseCode.contains(bz)) {
                    result = true;
                    tempMsg.append("债存量债券发行信息中的币种需在《表示货币和资金的代码》（GB/T 12406）中的三位字母范围内" + SysConstants.spaceStr);
                }
            }
        } else {
            if (checknumList.contains("JS1384")) {
                if (StringUtils.isBlank(xclzqfxxx.getBz())) {
                    tempMsg.append("币种不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 续发次数属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowXfcs(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getXfcs())) {
            if (checknumList.contains("JS2270")) {
                if (xclzqfxxx.getXfcs().compareTo("0")<0) {
                    tempMsg.append("续发次数应大于等于0" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS0745")) {
                if (xclzqfxxx.getXfcs().length() > 3||!CheckUtil.checkeCyryPattern(xclzqfxxx.getXfcs())) {
                    tempMsg.append("续发次数字符长度不能超过3位的整数" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1392")) {
                if (StringUtils.isBlank(xclzqfxxx.getXfcs())) {
                    tempMsg.append("续发次数不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 债券信用级别属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowZqxyjg(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getZqxyjg())) {
            String zqxyjg = xclzqfxxx.getZqxyjg();
            if (checknumList.contains("JS0042") && StringUtils.isNotBlank(zqxyjg)) {
                if (!ZqFieldUtils.zqxyjbList().contains(zqxyjg)) {
                    result = true;
                    tempMsg.append("存量债券发行信息中的债券信用级别需在符合要求的值域范围内" + SysConstants.spaceStr);
                }
            }
        } else {
            if (checknumList.contains("JS1383")) {
                if (StringUtils.isBlank(xclzqfxxx.getZqxyjg())) {
                    tempMsg.append("债券信用级别不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 债券总托管属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowZqztgjg(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getZqztgjg())) {
            String zqztgjg = xclzqfxxx.getZqztgjg();
            if (checknumList.contains("JS0040") && StringUtils.isNotBlank(zqztgjg)) {
                if (!(zqztgjg.equals("300") || zqztgjg.equals("400") || zqztgjg.equals("900"))) {
                    result = true;
                    tempMsg.append("存量债券发行信息中的债券总托管机构需在符合要求的值域范围内" + SysConstants.spaceStr);
                }
            }
        } else {
            if (checknumList.contains("JS1381")) {
                if (StringUtils.isBlank(xclzqfxxx.getZqztgjg())) {
                    tempMsg.append("债券总托管机构不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 债券属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowZqdm(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getZqdm())) {
            if (checknumList.contains("JS0740")) {
                if (xclzqfxxx.getZqdm().length() > 60) {
                    tempMsg.append("债券代码字符长度不能超过60" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS0737")) {
                if (CheckUtil.checkStr(xclzqfxxx.getZqdm())) {
                    tempMsg.append("债券代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1380")) {
                if (StringUtils.isBlank(xclzqfxxx.getZqdm())) {
                    tempMsg.append("债券代码不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 金融机构属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowFinanceorgcode(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getFinanceorgcode())) {
            if (checknumList.contains("JS0739")) {
                if (xclzqfxxx.getFinanceorgcode().length() != 18) {
                    tempMsg.append("金融机构代码字符长度应该为18位" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS0736")) {
                if (CheckUtil.checkStr(xclzqfxxx.getFinanceorgcode())) {
                    tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1379")) {
                if (StringUtils.isBlank(xclzqfxxx.getFinanceorgcode())) {
                    tempMsg.append("金融机构代码不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 起息日属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     */
    private Boolean checkXclzqfxxxRowQxr(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        String qxr = xclzqfxxx.getQxr();
        String dfrq = xclzqfxxx.getDfrq();
        String sjrq = xclzqfxxx.getSjrq();
        if (StringUtils.isNotBlank(qxr)) {
            if (checknumList.contains("JS0749")) {
                boolean ckQxrDate = CheckUtil.checkDate(qxr, SysConstants.yyyyMMdd);
                if (!ckQxrDate || !(qxr.compareTo("1800-01-01") > 0 && qxr.compareTo("2100-12-31") < 0)) {
                    tempMsg.append("起息日必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS2269")) {
                if (StringUtils.isNotBlank(dfrq)) {
                    if (qxr.compareTo(dfrq) > 0) {
                        tempMsg.append("起息日应小于等于兑付日期" + SysConstants.spaceStr);
                        result = true;
                    }
                }
            }
            if (checknumList.contains("JS2268")) {
                if (StringUtils.isNotBlank(sjrq)) {
                    if (qxr.compareTo(sjrq) > 0) {
                        tempMsg.append("起息日应小于等于数据日期" + SysConstants.spaceStr);
                        result = true;
                    }
                }
            }
        } else {
            if (checknumList.contains("JS1390")) {
                if (StringUtils.isBlank(qxr)) {
                    tempMsg.append("起息日不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }

        return result;
    }

    /**
     * 债券品种属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private Boolean checkXclzqfxxxRowZqpz(List<String> checknumList, Xclzqfxxx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        String zqpz = xclzqfxxx.getZqpz();
        if (checknumList.contains("JS2919") && StringUtils.isNotBlank(zqpz)){
            try {
                if (zqpz.substring(0,4).equals("TB99") && zqpz.length() > 200){
                    result = true;
                    tempMsg.append("债券品种为TB99开头时，长度不能超过200" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2920") && StringUtils.isNotBlank(zqpz)){
            try {
                if (!zqpz.substring(0,4).equals("TB99") && zqpz.length() > 6){
                    result = true;
                    tempMsg.append("债券品种不为TB99开头时，长度不能超过6" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (StringUtils.isNotBlank(xclzqfxxx.getZqpz())) {
            if (checknumList.contains("JS0041")&&!zqpz.substring(0,5).equals("TB99-")) {
                try {
                    if (!ZqFieldUtils.zqpzList.contains(zqpz)) {
                        result = true;
                        tempMsg.append("存量债券发行信息中的债券品种需在符合要求的值域范围内 或者为'TB99-’开头加债券中文名称" + SysConstants.spaceStr);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (checknumList.contains("JS1382")) {
                if (StringUtils.isBlank(xclzqfxxx.getZqpz())) {
                    tempMsg.append("债券品种不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }
}