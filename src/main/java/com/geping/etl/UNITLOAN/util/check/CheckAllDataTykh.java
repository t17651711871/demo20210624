package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.report.Xtykhjcxx;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;

import static com.geping.etl.UNITLOAN.util.check.CheckUtil.checkStr2;

/**
 * @ClassName: CheckAllDataTykh
 * @Description: TOOD
 * @Author: 陈根
 * @Date: 2021/2/26 9:29
 * @Version
 **/
public class CheckAllDataTykh {
    //币种代码
    private static List<String> currencyList;
    //行政区划代码
    private static List<String> areaList;
    //国家代码
    private static List<String> countryList;
    //大行业代码
    private static List<String> aindustryList;

    //C0017-行业中类代码
    private static List<String> bindustryList;

    //金融机构（分支机构）基础信息表.金融机构代码
    public static List<String> fzList;

    //金融机构（法人）基础信息表.金融机构代码
    public static List<String> frList;

    //委托贷款发生额信息
    public static void checkXtykhjcxx(CustomSqlUtil customSqlUtil, List<String> checknumList, Xtykhjcxx xtykhjcxx, LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId) {
        boolean isError = false;
        StringBuffer tempMsg = new StringBuffer("");

        String nullError = "不能为空";
        String lengthError = "超过限制长度";
        String spaceStr = "|";
//        金融机构代码
        String financeorgcode = xtykhjcxx.getFinanceorgcode();
        //客户名称
        String khname = xtykhjcxx.getKhname();
        //客户代码
        String khcode = xtykhjcxx.getKhcode();
        //客户金融机构编码
        String khjrjgbm = xtykhjcxx.getKhjrjgbm();
        //客户内部编码
        String khnbbm = xtykhjcxx.getKhnbbm();
        //基本存款账号
        String jbckzh = xtykhjcxx.getJbckzh();
        //基本账户开户行名称
        String jbzhkhhmc = xtykhjcxx.getJbzhkhhmc();
        //注册地址
        String zcdz = xtykhjcxx.getZcdz();
        //地区代码
        String dqdm = xtykhjcxx.getDqdm();
        //客户类别
        String khtype = xtykhjcxx.getKhtype();
        //成立日期
        String setupdate = xtykhjcxx.getSetupdate();
        //是否关联方
        String sfglf = xtykhjcxx.getSfglf();
        //客户经济成分
        String khjjcf = xtykhjcxx.getKhjjcf();
        //客户国民经济部门
        String khgmjjbm = xtykhjcxx.getKhgmjjbm();
        //客户信用级别总等级数
        String khxyjbzdjs = xtykhjcxx.getKhxyjbzdjs();
        //客户信用评级
        String khxypj = xtykhjcxx.getKhxypj();

        String sjrq = xtykhjcxx.getSjrq();

        //金融机构代码
        if (StringUtils.isNotBlank(financeorgcode)){
            if (CheckUtil.checkStr(financeorgcode)){
                if (checknumList.contains("JS0574")) {
                    tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                    isError=true;
                }
            }

            if (financeorgcode.length() !=18 ){
                if (checknumList.contains("JS0583")) { tempMsg.append("金融机构代码字符长度应该为18位"+spaceStr);isError=true;  }
            }

        }else {
            if (checknumList.contains("JS1247")) { tempMsg.append("金融机构代码不能为空" + spaceStr);isError = true;}
        }

        //客户名称
        if (StringUtils.isNotBlank(khname)){
            if (checkStr2(khname)){
                if (checknumList.contains("JS0581")){     tempMsg.append("客户名称字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }

            if (khname.length() > 200){
                if (checknumList.contains("JS0577")){   tempMsg.append("客户名称字符长度不能超过200"+spaceStr);isError=true; }
            }
        }else {
            if (checknumList.contains("JS1248")){    tempMsg.append("客户名称不能为空"+spaceStr);isError=true;}
        }


        //客户代码
        if (StringUtils.isNotBlank(khcode)){
            if (checkStr2(khcode)){
                if (checknumList.contains("JS0571")){     tempMsg.append("客户代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }
            if (StringUtils.isNotBlank(xtykhjcxx.getDqdm())){
                if (khcode.length() > 60){
                    if (checknumList.contains("JS0578")){   tempMsg.append("客户代码字符长度不能超过60"+spaceStr);isError=true; }
                }
                if (xtykhjcxx.getDqdm().substring(0,3).equals("000")){

                }else if (!xtykhjcxx.getDqdm().substring(0,3).equals("000")){
                    if (khcode.length() != 18){
                        if (checknumList.contains("JS0886")){   tempMsg.append("境内客户的客户代码字符长度应该为18位"+spaceStr);isError=true; }
                    }
                }
            }
        }else {
            if (checknumList.contains("JS1249")){    tempMsg.append("客户代码不能为空"+spaceStr);isError=true;}
        }


        //客户金融机构编码
        if (StringUtils.isNotBlank(khjrjgbm)){
            if (checkStr2(khjrjgbm)){
                if (checknumList.contains("JS0572")){     tempMsg.append("客户金融机构编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }

            if (khjrjgbm.length() > 60){
                if (checknumList.contains("JS0579")){   tempMsg.append("客户金融机构编码字符长度不能超过60"+spaceStr);isError=true; }
            }
        }else {
            if (checknumList.contains("JS1250")){    tempMsg.append("客户金融机构编码不能为空"+spaceStr);isError=true;}
        }

      //客户内部编码
        if (StringUtils.isNotBlank(khnbbm)){
            if (checkStr2(khnbbm)){
                if (checknumList.contains("JS0575")){     tempMsg.append("客户内部编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }

            if (khnbbm.length() > 60){
                if (checknumList.contains("JS0584")){   tempMsg.append("客户内部编码字符长度不能超过60"+spaceStr);isError=true; }
            }
        }else {
            if (checknumList.contains("JS1571")){    tempMsg.append("客户内部编码不能为空"+spaceStr);isError=true;}
        }

        //基本存款账号
        if (StringUtils.isNotBlank(jbckzh)){
            if (checkStr2(jbckzh)){
                if (checknumList.contains("JS0573")){     tempMsg.append("基本存款账号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }

            if (jbckzh.length() > 60){
                if (checknumList.contains("JS0580")){   tempMsg.append("基本存款账号字符长度不能超过60"+spaceStr);isError=true; }
            }
        }

        //基本账户开户行名称
        if (StringUtils.isNotBlank(jbzhkhhmc)){
            if (checkStr2(jbzhkhhmc)){
                if (checknumList.contains("JS0596")){     tempMsg.append("基本账户开户行名称字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }

            if (jbzhkhhmc.length() > 200){
                if (checknumList.contains("JS0582")){   tempMsg.append("基本账户开户行名称字符长度不能超过200"+spaceStr);isError=true; }
            }
        }

        //注册地址
        if (StringUtils.isNotBlank(zcdz)){
            if (checkStr2(zcdz)){
                if (checknumList.contains("JS0576")){     tempMsg.append("注册地址字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }

            if (zcdz.length() > 400){
                if (checknumList.contains("JS0585")){   tempMsg.append("注册地址字符长度不能超过400"+spaceStr);isError=true; }
            }
        }else {
            if (checknumList.contains("JS1572")){    tempMsg.append("注册地址不能为空"+spaceStr);isError=true;}
        }

        //地区代码
        if (StringUtils.isNotBlank(dqdm)){
            if (checknumList.contains("JS0176")) {
                if (areaList == null)
                    areaList = customSqlUtil.getBaseCode(BaseArea.class);
                if (countryList == null)
                    countryList = customSqlUtil.getBaseCode(BaseCountry.class);
                if (!areaList.contains(dqdm) && !countryList.contains(dqdm)) {
                    tempMsg.append("同业客户基础信息的地区代码需在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1253")){    tempMsg.append("地区代码不能为空"+spaceStr);isError=true;}
        }


        //客户类别
        if(StringUtils.isNotBlank(khtype)) {
            if (checknumList.contains("JS0178")) {
                if (!ArrayUtils.contains(CheckUtil.tykhlb, khtype)) {
                    tempMsg.append("同业客户基础信息的客户类别需在符合要求的最底层值域范围内" + spaceStr);
                    isError = true;
                }
            }
            if (StringUtils.isNotBlank(dqdm)){
                    if (dqdm.startsWith("000")){
                        if (!khtype.equals("E012")){
                            if (checknumList.contains("JS2052")){    tempMsg.append("境外注册客户的客户类别应该为E012-境外金融机构"+spaceStr);isError=true;}
                        }
                    }else{
                        if (khtype.equals("E012")){
                            if (checknumList.contains("JS2053")){    tempMsg.append("境内注册客户的客户类别不能为E012-境外金融机构"+spaceStr);isError=true;}
                        }
                    }
            }
        }else {
            if (checknumList.contains("JS1254")) {
                    tempMsg.append("客户类别不能为空" + spaceStr);
                    isError = true;
            }
        }



//        成立日期
        if(StringUtils.isNotBlank(setupdate)) {
            if(checknumList.contains("JS0589")) {
                boolean b1 = CheckUtil.checkDate(setupdate, "yyyy-MM-dd");
                if (b1) {
                    if (setupdate.compareTo("1800-01-01") < 0 || setupdate.compareTo("2100-12-31") > 0) {
                        tempMsg.append("成立日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
                        isError = true;
                    }
                } else {
                    tempMsg.append("成立日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
                    isError = true;
                }
            }

            if(checknumList.contains("JS2218")) {
                if(StringUtils.isNotBlank(sjrq) && setupdate.compareTo(sjrq) > 0) {
                    tempMsg.append("成立日期应小于等于数据日期" + spaceStr);
                    isError = true;
                }
            }

        }else {
            if (checknumList.contains("JS1255")){ tempMsg.append("成立日期不能为空" + spaceStr);isError = true; }
        }


        //是否关联方
        if(StringUtils.isNotBlank(sfglf)) {
            if (!ArrayUtils.contains(CheckUtil.sfcommon,sfglf)){
                if (checknumList.contains("JS0179")){ tempMsg.append("同业客户基础信息的是否关联方需在符合要求的值域范围内" + spaceStr);isError = true; }
            }

        }else {
            if (checknumList.contains("JS1256")){ tempMsg.append("是否关联方不能为空" + spaceStr);isError = true; }
        }

        //客户经济成分
        if(StringUtils.isNotBlank(khjjcf)) {
            if (!ArrayUtils.contains(CheckUtil.qyczrjjcf,khjjcf)){
                if (checknumList.contains("JS0177")){ tempMsg.append("若客户经济成分不为空，需在符合要求的值域范围内" + spaceStr);isError = true; }
            }

            if (StringUtils.isNotBlank(khtype) && khtype.equals("E012")){
                if (checknumList.contains("JS2870")){ tempMsg.append("境外客户的客户经济成分应为空" + spaceStr);isError = true; }
            }
        }else {
            if (StringUtils.isNotBlank(khtype) && !khtype.equals("E012")){
                if (checknumList.contains("JS1573")){ tempMsg.append("境内客户的客户经济成分不能为空" + spaceStr);isError = true; }
            }
        }

        //客户国民经济部门
        if(StringUtils.isNotBlank(khgmjjbm)) {
            if(checknumList.contains("JS2590")) {
                if(!ArrayUtils.contains(CheckUtil.gmjjbmkh, khgmjjbm)) {
                    tempMsg.append("客户国民经济部门需在符合要求的值域范围内且必须为金融机构" + spaceStr);
                    isError = true;
                }
            }
            if (StringUtils.isNotBlank(dqdm)){
                if (dqdm.startsWith("000")){
                    if (!khgmjjbm.startsWith("E")){
                        if (checknumList.contains("JS2592")){ tempMsg.append("客户地区代码为000开头的，国民经济部门应为E开头的非居民部门" + spaceStr);isError = true; }
                    }
                }else {
                    if (khgmjjbm.startsWith("E")){
                        if (checknumList.contains("JS2593")){ tempMsg.append("客户地区代码不是000开头的，国民经济部门不应为E开头的非居民部门" + spaceStr);isError = true; }
                    }
                }
            }
        }else {
            if (checknumList.contains("JS2591")) { tempMsg.append("客户国民经济部门不能为空" + spaceStr);isError = true;  }
        }

        //客户信用级别总等级数
        if (StringUtils.isNotBlank(khxyjbzdjs)){
            if (checknumList.contains("JS0586")) {
                if(!CheckUtil.checkZhenshu(khxyjbzdjs)) {
                    tempMsg.append("客户信用级别总等级数字符长度不能超过3位的整数" + spaceStr);
                    isError = true;
                }
            }

            if(new BigDecimal(khxyjbzdjs).compareTo(BigDecimal.ZERO) <= 0) {
                if (checknumList.contains("JS2364")){
                    tempMsg.append("客户信用级别总等级数应大于0" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1574")){    tempMsg.append("客户信用级别总等级数不能为空"+spaceStr);isError=true;}
        }

         //客户信用评级
        if (StringUtils.isNotBlank(khxypj)){
            if (checknumList.contains("JS0587")) {
                if(StringUtils.isNotBlank(khxyjbzdjs)&&!CheckUtil.checkZhenshu(khxyjbzdjs)) {
                    tempMsg.append("当客户信用评级不为空时，客户信用评级字符长度不能超过3位的整数" + spaceStr);
                    isError = true;
                }
            }

            if (StringUtils.isNotBlank(khxyjbzdjs)){
                if(checknumList.contains("JS2363")) {
                    try {
                        if(new BigDecimal(khxypj).compareTo(new BigDecimal(khxyjbzdjs)) == 1) {
                            tempMsg.append("当客户信用评级不为空时，客户信用评级应小于等于客户信用级别总等级数" + spaceStr);
                            isError = true;
                        }
                    }catch(Exception e) {
                        tempMsg.append("当客户信用评级不为空时，客户信用评级应小于等于客户信用级别总等级数" + spaceStr);
                        isError = true;
                    }
                }
            }

        }

        //数据日期
        isError = CheckUtil.nullAndDate(sjrq, tempMsg, isError, "数据日期",false);

        //#整表
        if (isError) {
            if(!errorMsg.containsKey(xtykhjcxx.getId())) {
                errorMsg.put(xtykhjcxx.getId(), "客户金融机构编码:"+xtykhjcxx.getKhjrjgbm()+"，"+"客户内部编码:"+xtykhjcxx.getKhnbbm()+"]->\r\n");
            }
            String str = errorMsg.get(xtykhjcxx.getId());
            str = str + tempMsg;
            errorMsg.put(xtykhjcxx.getId(), str);
            errorId.add(xtykhjcxx.getId());
        } else {
            if(!errorId.contains(xtykhjcxx.getId())) {
                rightId.add(xtykhjcxx.getId());
            }
        }
    }

}
