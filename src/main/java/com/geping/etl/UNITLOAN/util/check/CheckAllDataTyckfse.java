package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.report.Xtyckfsexx;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DateUtils;
import com.geping.etl.UNITLOAN.util.tyckfse.TyckfseHqUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import static com.geping.etl.UNITLOAN.util.check.CheckUtil.checkStr2;

/**
 * @ClassName: CheckAllDataTyckfse
 * @Description: TOOD
 * @Author: 陈根
 * @Date: 2021/2/24 13:53
 * @Version
 **/
public class CheckAllDataTyckfse {
    //币种代码
    private static List<String> currencyList;
    //行政区划代码
    private static List<String> areaList;
    //国家代码
    private static List<String> countryList;
    //大行业代码
    private static List<String> aindustryList;

    //C0017-行业中类代码
    private static List<String> bindustryList;

    //金融机构（分支机构）基础信息表.金融机构代码
    public static List<String> fzList;

    //金融机构（法人）基础信息表.金融机构代码
    public static List<String> frList;

    //委托贷款发生额信息
    public static void checkXtyckfsexx(CustomSqlUtil customSqlUtil, List<String> checknumList, Xtyckfsexx xtyckfsexx, LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId) {
        boolean isError = false;
        StringBuffer tempMsg = new StringBuffer("");

        String nullError = "不能为空";
        String lengthError = "超过限制长度";
        String spaceStr = "|";
        //金融机构代码
        String financeorgcode = xtyckfsexx.getFinanceorgcode();
        //内部机构号
        String financeorginnum = xtyckfsexx.getFinanceorginnum();
        //业务类型
        String ywlx = xtyckfsexx.getYwlx();
        //交易对手证件类型
        String jydszjlx = xtyckfsexx.getJydszjlx();
        //交易对手代码
        String jydsdm = xtyckfsexx.getJydsdm();
        //存款账户编码
        String ckzhbm = xtyckfsexx.getCkzhbm();
        //存款协议代码
        String ckxydm = xtyckfsexx.getCkxydm();
        //协议起始日期
        String startdate = xtyckfsexx.getStartdate();
        //协议到期日期
        String enddate = xtyckfsexx.getEnddate();
        //币种
        String currency = xtyckfsexx.getCurrency();
        //交易金额
        String receiptbalance = xtyckfsexx.getReceiptbalance();
        //交易金额折人民币
        String receiptcnybalance = xtyckfsexx.getReceiptcnybalance();
        //交易日期
        String jyrq = xtyckfsexx.getJyrq();
        //交易流水号
        String jylsh = xtyckfsexx.getJylsh();
        //利率水平
        String llsp = xtyckfsexx.getLlsp();
        //交易账户号
        String jyzhh = xtyckfsexx.getJyzhh();
//        交易账户开户行号
        String jyzhkhhh = xtyckfsexx.getJyzhkhhh();
        //交易对手账户号
        String jydszhh = xtyckfsexx.getJydszhh();
        //交易方向
        String jyfx = xtyckfsexx.getJyfx();
        //数据日期
        String sjrq = xtyckfsexx.getSjrq();


        //金融机构代码
        if (StringUtils.isNotBlank(financeorgcode)) {
            if (CheckUtil.checkStr(financeorgcode)) {
                if (checknumList.contains("JS0656")) {
                    tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }

            if (financeorgcode.length() != 18) {
                if (checknumList.contains("JS0666")) {
                    tempMsg.append("金融机构代码字符长度应该为18位" + spaceStr);
                    isError = true;
                }
            }

        } else {
            if (checknumList.contains("JS1310")) {
                tempMsg.append("金融机构代码不能为空" + spaceStr);
                isError = true;
            }
        }

        //内部机构号
        if (StringUtils.isNotBlank(financeorginnum)) {

            if (checkStr2(financeorginnum)) {
                if (checknumList.contains("JS0657")) {
                    tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }

            if (financeorginnum.length() > 30) {
                if (checknumList.contains("JS0667")) {
                    tempMsg.append("内部机构号字符长度不能超过30" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1311")) {
                tempMsg.append("内部机构号不能为空" + spaceStr);
                isError = true;
            }
        }

        //业务类型
        if (StringUtils.isNotBlank(ywlx)) {
            if (!ArrayUtils.contains(CheckUtil.ywlx, ywlx)) {
                if (checknumList.contains("JS0004")) {
                    tempMsg.append("同业存款发生额信息表中的业务类型需在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1312")) {
                tempMsg.append("业务类型不能为空" + spaceStr);
                isError = true;
            }
        }

        //交易对手证件类型
        if (StringUtils.isNotBlank(jydszjlx)) {
            if (!ArrayUtils.contains(CheckUtil.jydszjlx, jydszjlx)) {
                if (checknumList.contains("JS0073")) {
                    tempMsg.append("交易对手证件类型应该在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1566")) {
                tempMsg.append("交易对手证件类型不能为空" + spaceStr);
                isError = true;
            }
        }

        //交易对手代码
        if (StringUtils.isNotBlank(jydsdm)) {
            if (checkStr2(jydsdm)) {
                if (checknumList.contains("JS0658")) {
                    tempMsg.append("交易对手代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }

            if (jydsdm.length() > 60) {
                if (checknumList.contains("JS0668")) {
                    tempMsg.append("交易对手代码字符长度不能超过60" + spaceStr);
                    isError = true;
                }
            }

            if (StringUtils.isNotBlank(jydszjlx) && (jydszjlx.equals("A01") || jydszjlx.equals("A02"))) {
                if (jydsdm.length() != 9 && jydsdm.length() != 18) {
                    if (checknumList.contains("JS2408")) {
                        tempMsg.append("境内非SPV客户的交易对手代码字符长度应该为9位或18位" + spaceStr);
                        isError = true;
                    }
                }
            }


        } else {
            if (checknumList.contains("JS1313")) {
                tempMsg.append("交易对手代码不能为空" + spaceStr);
                isError = true;
            }
        }


        //存款账户编码
        if (StringUtils.isNotBlank(ckzhbm)) {
            if (checkStr2(ckzhbm)) {
                if (checknumList.contains("JS0659")) {
                    tempMsg.append("存款账户编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }

            if (ckzhbm.length() > 60) {
                if (checknumList.contains("JS0669")) {
                    tempMsg.append("存款账户编码字符长度不能超过60" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1314")) {
                tempMsg.append("存款账户编码不能为空" + spaceStr);
                isError = true;
            }
        }


        //存款协议代码
        if (StringUtils.isNotBlank(ckxydm)) {
            if (checkStr2(ckxydm)) {
                if (checknumList.contains("JS0665")) {
                    tempMsg.append("存款协议代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }

            if (ckxydm.length() > 60) {
                if (checknumList.contains("JS0674")) {
                    tempMsg.append("存款协议代码字符长度不能超过60" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1567")) {
                tempMsg.append("存款协议代码不能为空" + spaceStr);
                isError = true;
            }
        }

        //协议起始日期
        if (StringUtils.isNotBlank(startdate)) {
            if (checknumList.contains("JS0680")) {
                boolean b1 = CheckUtil.checkDate(startdate, "yyyy-MM-dd");
                if (b1) {
                    if (startdate.compareTo("1800-01-01") < 0 || startdate.compareTo("2100-12-31") > 0) {
                        tempMsg.append("协议起始日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
                        isError = true;
                    }
                } else {
                    tempMsg.append("协议起始日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
                    isError = true;
                }
            }
            if (checknumList.contains("JS2238")) {
                if (StringUtils.isNotBlank(sjrq) && startdate.compareTo(sjrq) > 0) {
                    tempMsg.append("协议起始日期应小于等于数据日期" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1315")) {
                tempMsg.append("协议起始日期不能为空" + spaceStr);
                isError = true;

            }
        }


        //协议到期日期
        if (StringUtils.isNotBlank(enddate)) {
            if (checknumList.contains("JS0681")) {
                boolean b1 = CheckUtil.checkDate(enddate, "yyyy-MM-dd");
                if (b1) {
                    if ((enddate.compareTo("1800-01-01") < 0 || enddate.compareTo("2100-12-31") > 0) && !enddate.equals("9999-12-31")) {
                        tempMsg.append("协议到期日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间,或者是9999-12-31" + spaceStr);
                        isError = true;
                    }
                } else {
                    tempMsg.append("协议到期日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
                    isError = true;
                }
            }
            if (checknumList.contains("JS2239") && StringUtils.isNotBlank(startdate) && StringUtils.isNotBlank(enddate)) {
                if (enddate.compareTo(startdate) < 0 && enddate.compareTo("1999-01-01") != 0 && enddate.compareTo("1999-01-07") != 0) {
                    tempMsg.append("协议到期日期不等于1999-01-01、1999-01-07时，应大于等于协议起始日期" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1316")) {
                tempMsg.append("协议到期日期不能为空" + spaceStr);
                isError = true;

            }
        }


        //币种
        if (StringUtils.isNotBlank(currency)) {
            if (currencyList == null) currencyList = customSqlUtil.getBaseCode(BaseCurrency.class);
            if (checknumList.contains("JS0005")) {
                if (!currencyList.contains(currency)) {
                    tempMsg.append("同业存款发生额信息表中的币种需在值域范围内" + spaceStr);
                    isError = true;
                }
            }

        } else {
            if (checknumList.contains("JS1318")) {
                tempMsg.append("币种不能为空" + spaceStr);
                isError = true;
            }
        }


        //交易金额
        if (StringUtils.isNotBlank(receiptbalance)) {
            if (checknumList.contains("JS0675")) {
                if (receiptbalance.length() > 20 || !CheckUtil.checkDecimal(receiptbalance, 2)) {
                    tempMsg.append("交易金额总长度不能超过20位，小数位必须为2位" + spaceStr);
                    isError = true;
                }
            }


            if (checknumList.contains("JS2236")) {
                try {
                    if (new BigDecimal(receiptbalance).compareTo(BigDecimal.ZERO) <= 0) {
                        tempMsg.append("交易金额应大于0" + spaceStr);
                        isError = true;
                    }
                } catch (Exception e) {
                    tempMsg.append("交易金额应大于0" + spaceStr);
                    isError = true;
                }
            }

        } else {
            if (checknumList.contains("JS1319")) {
                tempMsg.append("交易金额不能为空" + spaceStr);
                isError = true;
            }
        }

        // 交易金额折人民币
        if (StringUtils.isNotBlank(receiptcnybalance)) {
            if (checknumList.contains("JS0676")) {
                if (receiptcnybalance.length() > 20 || !CheckUtil.checkDecimal(receiptcnybalance, 2)) {
                    tempMsg.append("交易金额折人民币总长度不能超过20位，小数位必须为2位" + spaceStr);
                    isError = true;
                }
            }


            if (checknumList.contains("JS2237")) {
                try {
                    if (new BigDecimal(receiptcnybalance).compareTo(BigDecimal.ZERO) <= 0) {
                        tempMsg.append("交易金额折人民币应大于0" + spaceStr);
                        isError = true;
                    }
                } catch (Exception e) {
                    tempMsg.append("交易金额折人民币应大于0" + spaceStr);
                    isError = true;
                }
            }


            if (StringUtils.isNotBlank(currency) && currency.equals("CNY")) {
                if (StringUtils.isNotBlank(receiptbalance) && !receiptbalance.equals(receiptcnybalance)) {
                    if (checknumList.contains("JS2696")) {
                        tempMsg.append("币种为人民币的，交易金额折人民币应该与交易金额的值相等" + spaceStr);
                        isError = true;
                    }
                }
            }
        } else {
            if (checknumList.contains("JS1320")) {
                tempMsg.append("交易金额折人民币不能为空" + spaceStr);
                isError = true;
            }
        }

        //交易日期
        if (StringUtils.isNotBlank(jyrq)) {
            if (checknumList.contains("JS0679")) {
                boolean b1 = CheckUtil.checkDate(jyrq, "yyyy-MM-dd");
                if (b1) {
                    if (jyrq.compareTo("1800-01-01") < 0 || jyrq.compareTo("2100-12-31") > 0) {
                        tempMsg.append("交易日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
                        isError = true;
                    }
                } else {
                    tempMsg.append("交易日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
                    isError = true;
                }
            }
            if (checknumList.contains("JS2240") && StringUtils.isNotBlank(jyrq) && StringUtils.isNotBlank(startdate) && jyrq.compareTo(startdate) < 0) {
                if (StringUtils.isNotBlank(ywlx) && !ywlx.equals("T011") && !ywlx.equals("T021")) {
                    tempMsg.append("除活期同业存款以外的其他同业存款业务，交易日期应大于等于协议起始日期" + spaceStr);
                    isError = true;
                }
                if (StringUtils.isBlank(ywlx)) {
                    tempMsg.append("除活期同业存款以外的其他同业存款业务，交易日期应大于等于协议起始日期" + spaceStr);
                    isError = true;
                }
            }
            if (checknumList.contains("JS2241") && StringUtils.isNotBlank(jyrq) && StringUtils.isNotBlank(enddate)) {
                if (jyrq.compareTo(enddate) > 0) {
                    if (StringUtils.isNotBlank(ywlx)&&!ywlx.equals("T011") && !ywlx.equals("T021") && enddate.compareTo("1999-01-01") != 0 && enddate.compareTo("1999-01-01") != 0) {
                        tempMsg.append("除活期同业存款以外的其他同业存款业务，协议到期日期不等于1999-01-01、1999-01-07时，交易日期应小于等于协议到期日期" + spaceStr);
                        isError = true;
                    }
                    if (StringUtils.isBlank(ywlx)) {
                        tempMsg.append("除活期同业存款以外的其他同业存款业务，协议到期日期不等于1999-01-01、1999-01-07时，交易日期应小于等于协议到期日期" + spaceStr);
                        isError = true;
                    }
                }
            }
            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH) + 1;
            //int month = cal.get(Calendar.MONTH);
            String monthStr;
            if (month < 10) {
                monthStr = year + "-0" + month;
            } else {
                monthStr = year + "-" + month + 1;
            }
            if (checknumList.contains("JS2564")) {
                try {
                    if (StringUtils.isNotBlank(jyrq)&&StringUtils.isNotBlank(sjrq)&&!DateUtils.isInMonth(jyrq, sjrq)) {
                        if (StringUtils.isNotBlank(ywlx)&&!ywlx.equals("T011") && !ywlx.equals("T021")) {
                            tempMsg.append("除活期同业存款以外的其他同业存款业务，交易日期应该在当月范围内" + spaceStr);
                            isError = true;
                        }
                        if (StringUtils.isBlank(ywlx)) {
                            tempMsg.append("除活期同业存款以外的其他同业存款业务，交易日期应该在当月范围内" + spaceStr);
                            isError = true;
                        }
                    }
                } catch (Exception e) {
                    tempMsg.append("除活期同业存款以外的其他同业存款业务，交易日期应该在当月范围内" + spaceStr);
                    isError = true;
                }
            }

        } else {
            if (checknumList.contains("JS1317")) {
                if (StringUtils.isNotBlank(ywlx) && !(ywlx.equals("T011") || ywlx.equals("T021"))) {//活期不做空校验
                    tempMsg.append("交易日期不能为空" + spaceStr);
                    isError = true;
                }
                if (StringUtils.isBlank(ywlx)) {//活期不做空校验
                    tempMsg.append("交易日期不能为空" + spaceStr);
                    isError = true;
                }
            }
        }

        //交易流水号
        if (StringUtils.isNotBlank(jylsh)) {
            if (checkStr2(jylsh)) {
                if (checknumList.contains("JS0660")) {
                    tempMsg.append("交易流水号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }

            if (jylsh.length() > 60) {
                if (checknumList.contains("JS0670")) {
                    tempMsg.append("交易流水号字符长度不能超过60" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1309")) {
                if (StringUtils.isNotBlank(ywlx) && !(ywlx.equals("T011") || ywlx.equals("T021"))) {//活期不做空校验
                    tempMsg.append("除活期同业存款以外的其他同业存款业务，交易流水号不能为空" + spaceStr);
                    isError = true;
                }
                if (StringUtils.isBlank(ywlx)) {
                    tempMsg.append("除活期同业存款以外的其他同业存款业务，交易流水号不能为空" + spaceStr);
                    isError = true;
                }
            }
        }

        //利率水平
        if (StringUtils.isNotBlank(llsp)) {
            if (CheckUtil.checkPerSign(llsp)) {
                if (checknumList.contains("JS0661")) {
                    tempMsg.append("利率水平不能包含‰或%" + spaceStr);
                    isError = true;
                }
            }

            if (checknumList.contains("JS0677")) {
                if (llsp.length() > 10 || !CheckUtil.checkDecimal(llsp, 5)) {
                    tempMsg.append("利率水平总长度不能超过10位，小数位必须为5位" + spaceStr);
                    isError = true;
                }
            }

            if (checknumList.contains("JS2242")) {
                try {
                    if (new BigDecimal(llsp).compareTo(BigDecimal.ZERO) == -1 || new BigDecimal(llsp).compareTo(new BigDecimal("30")) == 1) {
                        tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
                        isError = true;
                    }
                } catch (Exception e) {
                    tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
                    isError = true;
                }
            }

        } else {
            if (checknumList.contains("JS1321")) {
                tempMsg.append("利率水平不能为空" + spaceStr);
                isError = true;

            }
        }

        //交易账户号
        if (StringUtils.isNotBlank(jyzhh)) {
            if (CheckUtil.checkStr(jyzhh)) {
                if (checknumList.contains("JS0662")) {
                    tempMsg.append("交易账户号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }

            if (jyzhh.length() > 60) {
                if (checknumList.contains("JS0671")) {
                    tempMsg.append("交易账户号字符长度不能超过60" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1322")) {
                if (StringUtils.isNotBlank(ywlx) && !(ywlx.equals("T011") || ywlx.equals("T021"))) {//活期不做空校验
                    tempMsg.append("交易账户号不能为空" + spaceStr);
                    isError = true;
                }
                if (StringUtils.isBlank(ywlx)) {//活期不做空校验
                    tempMsg.append("交易账户号不能为空" + spaceStr);
                    isError = true;
                }
            }
        }


        //交易账户开户行号
        if (StringUtils.isNotBlank(jyzhkhhh)) {
            if (CheckUtil.checkStr(jyzhkhhh)) {
                if (checknumList.contains("JS0663")) {
                    tempMsg.append("交易账户开户行号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }

            if (jyzhkhhh.length() > 60) {
                if (checknumList.contains("JS0672")) {
                    tempMsg.append("交易账户开户行号字符长度不能超过60" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1323")) {
                if (StringUtils.isNotBlank(ywlx) && !(ywlx.equals("T011") || ywlx.equals("T021"))) {//活期不做空校验
                    tempMsg.append("交易账户开户行号不能为空" + spaceStr);
                    isError = true;
                }
                if (StringUtils.isBlank(ywlx)) {//活期不做空校验
                    tempMsg.append("交易账户开户行号不能为空" + spaceStr);
                    isError = true;
                }
            }
        }


        //交易对手账户号
        if (StringUtils.isNotBlank(jydszhh)) {
            if (CheckUtil.checkStr(jydszhh)) {
                if (checknumList.contains("JS0664")) {
                    tempMsg.append("交易对手账户号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + spaceStr);
                    isError = true;
                }
            }

            if (jydszhh.length() > 60) {
                if (checknumList.contains("JS0673")) {
                    tempMsg.append("交易对手账户号字符长度不能超过60" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1324")) {
                if (StringUtils.isNotBlank(ywlx) && !(ywlx.equals("T011") || ywlx.equals("T021"))) {//活期不做空校验
                    tempMsg.append("交易对手账户号不能为空" + spaceStr);
                    isError = true;
                }
                if (StringUtils.isBlank(ywlx)) {//活期不做空校验
                    tempMsg.append("交易对手账户号不能为空" + spaceStr);
                    isError = true;
                }
            }
        }

        //交易方向
        if (StringUtils.isNotBlank(jyfx)) {
            if (!jyfx.equals("1") && !jyfx.equals("0")) {
                if (checknumList.contains("JS0006")) {
                    tempMsg.append("同业存款发生额信息表中的交易方向需在符合要求的值域范围内" + spaceStr);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1325")) {
                tempMsg.append("交易方向不能为空" + spaceStr);
                isError = true;
            }
        }


        //交易对手账户号
        /*if (StringUtils.isNotBlank(jydszhh)){
            if (CheckUtil.checkStr(jydszhh)){
                if (checknumList.contains("JS0664")) {
                    tempMsg.append("交易对手账户号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                    isError=true;
                }
            }

            if (jydszhh.length() > 60 ){
                if (checknumList.contains("JS0673")) { tempMsg.append("交易对手账户号字符长度不能超过60"+spaceStr);isError=true;  }
            }
        }else {
            if(StringUtils.isNotBlank(ywlx)&&!(TyckfseHqUtils.isHqYwlx(ywlx))) {//活期不做空校验
                if (checknumList.contains("JS1324")) { tempMsg.append("交易对手账户号不能为空" + spaceStr);isError = true;}
            }
        }
*/


        //数据日期
        isError = CheckUtil.nullAndDate(sjrq, tempMsg, isError, "数据日期", false);

        //#整表
        if (isError) {
            if (!errorMsg.containsKey(xtyckfsexx.getId())) {
                errorMsg.put(xtyckfsexx.getId(), "交易对手代码:" + xtyckfsexx.getJydsdm() + "，" + "存款账户编码:" + xtyckfsexx.getCkzhbm() + "]->\r\n");
            }
            String str = errorMsg.get(xtyckfsexx.getId());
            str = str + tempMsg;
            errorMsg.put(xtyckfsexx.getId(), str);
            errorId.add(xtyckfsexx.getId());
        } else {
            if (!errorId.contains(xtyckfsexx.getId())) {
                rightId.add(xtyckfsexx.getId());
            }
        }
    }
}
