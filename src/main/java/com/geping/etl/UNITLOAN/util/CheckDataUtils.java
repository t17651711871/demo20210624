package com.geping.etl.UNITLOAN.util;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.geping.etl.UNITLOAN.util.check.CheckUtil;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.report.XjrjgfrAssets;
import com.geping.etl.UNITLOAN.entity.report.XjrjgfrBaseinfo;
import com.geping.etl.UNITLOAN.entity.report.XjrjgfrProfit;
import com.geping.etl.UNITLOAN.service.report.XjrjgfrAssetsService;
import com.geping.etl.UNITLOAN.service.report.XjrjgfrBaseinfoService;
import com.geping.etl.UNITLOAN.service.report.XjrjgfrProfitService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 校验数据工具
 * 
 * @author WuZengWen
 * @date 2020年6月10日 下午1:42:21
 */
public class CheckDataUtils {

	@PersistenceContext
	private EntityManager entityManager;

	// 币种代码
	private static List<String> currencyList;
	// 行政区划代码
	private static List<String> areaList;
	// 国家代码
	private static List<String> countryList;
	// 大行业代码
	private static List<String> aindustryList;

	private static Map<String, String> ziDuanJiHe;

	private static class SingletonInstance {
		private static final Map<String, String> ziDuanJiHe = new HashMap<String, String>();
	}

	public static Map<String, String> getInstance() {
		return SingletonInstance.ziDuanJiHe;
	}

	private static final Pattern strPattern = Pattern.compile("[？?！!$%^*|-]|[\\s\"'\\n\\r]");
	private static final Pattern strPattern01 = Pattern.compile("[？?！!$%^*|]|[\\s\"'\\n\\r]");

	public static Map<String, String> getDWDKDBWXX() {
		ziDuanJiHe = getInstance();
		ziDuanJiHe.put("gteecontractcode", "担保合同编码");
		ziDuanJiHe.put("loancontractcode", "贷款合同编码");
		ziDuanJiHe.put("gteegoodscode", "担保物编码");
		ziDuanJiHe.put("gteegoodscategory", "担保物类别");
		ziDuanJiHe.put("warrantcode", "权证编号");
		ziDuanJiHe.put("isfirst", "是否第一顺位");
		ziDuanJiHe.put("assessmode", "评估方式");
		ziDuanJiHe.put("assessmethod", "评估方法");
		ziDuanJiHe.put("assessvalue", "评估价值");
		ziDuanJiHe.put("assessdate", "评估基准日");
		ziDuanJiHe.put("gteegoodsamt", "担保物账面价值");
		ziDuanJiHe.put("firstrightamt", "优先受偿权数额");
		ziDuanJiHe.put("gteegoodsstataus", "担保物状态");
		ziDuanJiHe.put("mortgagepgerate", "抵质押率");
		return ziDuanJiHe;
	}

	// private static String regEx = "[
	// _`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]|\n|\r|\t";
	private static String regEx = "[!$%^*?！？ ]|\n|\r|\t";
	private static Pattern pat = Pattern.compile(regEx);
	// private static Pattern pattern =
	// Pattern.compile("^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){2})?$"); //
	// 判断小数点后2位的数字的正则表达式
	private static Pattern pattern = Pattern
			.compile("^([\\+\\-]?[1-9]{1}[0-9]{0,16}\\.[0-9]{2})|([\\+\\-]?[0]{1}\\.[0-9]{2})?$");// 判断20位的数值且包含2位小数的数字的正则表达式
	private static Pattern comma = Pattern.compile("[,，]");
	/**
	 * 校验指定的数据
	 * 
	 * @param dataList
	 *            需要校验数据的集合
	 * @param code
	 *            需要校验数据的泛型
	 * @return 返回1表示校验成功否则返回校验结果
	 */
	public static String jiaoYanShuJu(List<?> dataList, TableCodeEnum code, Object obj, CustomSqlUtil customSqlUtil, List<String> checknumList) {
		StringBuffer result = new StringBuffer();
		List<String> zhengqueid = new ArrayList<String>();
		List<String> cuowuid = new ArrayList<String>();
		if (dataList != null && dataList.size() > 0) {
			if (TableCodeEnum.ZCFZJFXTJ.equals(code)) {
				for (int i = 0; i < dataList.size(); i++) {
					XjrjgfrAssets data = (XjrjgfrAssets) dataList.get(i);
					guiZeJiaoYanForZCFZJFXTJ(data, result, zhengqueid, cuowuid, customSqlUtil, checknumList);
				}
				XjrjgfrAssetsService xas = (XjrjgfrAssetsService) obj;
				if (zhengqueid.size() > 0) {
					xas.updateXjrjgfrAssetsOnCheckstatus("1", zhengqueid);
				}
				if (cuowuid.size() > 0) {
					xas.updateXjrjgfrAssetsOnCheckstatus("2", cuowuid);
				}
			} else if (TableCodeEnum.JCQKTJ.equals(code)) {
				for (int i = 0; i < dataList.size(); i++) {
					XjrjgfrBaseinfo data = (XjrjgfrBaseinfo) dataList.get(i);
					guiZeJiaoYanForJCQKTJ(data, result, zhengqueid, cuowuid, customSqlUtil, checknumList);
				}
				XjrjgfrBaseinfoService xbs = (XjrjgfrBaseinfoService) obj;
				if (zhengqueid.size() > 0) {
					xbs.updateXjrjgfrBaseinfoOnCheckstatus("1", zhengqueid);
				}
				if (cuowuid.size() > 0) {
					xbs.updateXjrjgfrBaseinfoOnCheckstatus("2", cuowuid);
				}
			} else if (TableCodeEnum.LRJZBTJ.equals(code)) {
				for (int i = 0; i < dataList.size(); i++) {
					XjrjgfrProfit data = (XjrjgfrProfit) dataList.get(i);
					guiZeJiaoYanForLRJZBTJ(data, result, zhengqueid, cuowuid, customSqlUtil, checknumList);
				}
				XjrjgfrProfitService xps = (XjrjgfrProfitService) obj;
				if (zhengqueid.size() > 0) {
					xps.updateXjrjgfrProfitOnCheckstatus("1", zhengqueid);
				}
				if (cuowuid.size() > 0) {
					xps.updateXjrjgfrProfitOnCheckstatus("2", cuowuid);
				}
			}
		} else {
			result.append("要校验的数据集合为空");
		}
		if (result.length() == 0) {
			result.append("1");
		}
		return result.toString();
	}

	/**
	 * 校验指定类型的数据
	 * 
	 * @param code
	 *            需要校验数据的泛型
	 * @return 返回1表示校验成功否则返回校验结果
	 */
	public static String jiaoYanShuJu(TableCodeEnum code, Object obj, CustomSqlUtil customSqlUtil, List<String> checknumList,String departId) {
		StringBuffer result = new StringBuffer();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<String> zhengqueid = new ArrayList<String>();
		List<String> cuowuid = new ArrayList<String>();
		Integer integerDBWXXzq = null;
		Integer integerDBWXXcw = null;
		Integer integerJRFZzq = null;
		Integer integerJRFZcw = null;
		// StringBuffer buffer=new StringBuffer();
		if (TableCodeEnum.ZCFZJFXTJ.equals(code)) {
			String query = "select id,gxck,gxdk,zczj,fzzj,syzqyhj,sxzc,fxzc,ldxzc,ldxfz,zcldk,gzldk,cjldk,jyldk,ssldk,yqdk,yqninetytysdk,dkjzzb,sjrq from xjrjgfrassets where checkstatus='0' and (datastatus='0' or datastatus='2') and departid like '"+departId+"'";
			try {
				con = JDBCUtil.getConnection("/intg/jdbc.properties", "mysql.driverClassName", "mysql.url",
						"mysql.username", "mysql.password");
				ps = con.prepareStatement(query);
				rs = ps.executeQuery();
				while (rs.next()) {
					XjrjgfrAssets xx = new XjrjgfrAssets();
					xx.setId(rs.getString(1));
					xx.setGxck(rs.getString(2));
					xx.setGxdk(rs.getString(3));
					xx.setZczj(rs.getString(4));
					xx.setFzzj(rs.getString(5));
					xx.setSyzqyhj(rs.getString(6));
					xx.setSxzc(rs.getString(7));
					xx.setFxzc(rs.getString(8));
					xx.setLdxzc(rs.getString(9));
					xx.setLdxfz(rs.getString(10));
					xx.setZcldk(rs.getString(11));
					xx.setGzldk(rs.getString(12));
					xx.setCjldk(rs.getString(13));
					xx.setJyldk(rs.getString(14));
					xx.setSsldk(rs.getString(15));
					xx.setYqdk(rs.getString(16));
					xx.setYqninetytysdk(rs.getString(17));
					xx.setDkjzzb(rs.getString(18));
					xx.setSjrq(rs.getString(19));
					guiZeJiaoYanForZCFZJFXTJ(xx, result, zhengqueid, cuowuid, customSqlUtil, checknumList);
				}
				XjrjgfrAssetsService xas = (XjrjgfrAssetsService) obj;
				if (zhengqueid.size() > 0) {
					xas.updateXjrjgfrAssetsOnCheckstatus("1", zhengqueid);
				}
				if (cuowuid.size() > 0) {
					xas.updateXjrjgfrAssetsOnCheckstatus("2", cuowuid);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (ps != null) {
					try {
						ps.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} else if (TableCodeEnum.JCQKTJ.equals(code)) {
			String query = "select id,finorgname,finorgcode,finorgnum,jglb,regaddress,regarea,regamt,setupdate,lxr,phone,orgmanagestatus,orgstoreconomy,invermodel,"
					+ "actctrlidtype,actctrlname,actctrlcode,personnum,onestockcode,twostockcode,threestockcode,fourstockcode,fivestockcode,sixstockcode,sevenstockcode,"
					+ "eightstockcode,ninestockcode,tenstockcode,onestockprop,twostockprop,threestockprop,fourstockprop,fivestockprop,sixstockprop,sevenstockprop,eightstockprop,"
					+ "ninestockprop,tenstockprop,sjrq from xjrjgfrbaseinfo where checkstatus='0' and (datastatus='0' or datastatus='2') and departid like '"+departId+"'";
			try {
				con = JDBCUtil.getConnection("/intg/jdbc.properties", "mysql.driverClassName", "mysql.url",
						"mysql.username", "mysql.password");
				ps = con.prepareStatement(query);
				rs = ps.executeQuery();
				while (rs.next()) {
					XjrjgfrBaseinfo xx = new XjrjgfrBaseinfo();
					xx.setId(rs.getString(1));
					xx.setFinorgname(rs.getString(2));
					xx.setFinorgcode(rs.getString(3));
					xx.setFinorgnum(rs.getString(4));
					xx.setJglb(rs.getString(5));
					xx.setRegaddress(rs.getString(6));
					xx.setRegarea(rs.getString(7));
					xx.setRegamt(rs.getString(8));
					xx.setSetupdate(rs.getString(9));
					xx.setLxr(rs.getString(10));
					xx.setPhone(rs.getString(11));
					xx.setOrgmanagestatus(rs.getString(12));
					xx.setOrgstoreconomy(rs.getString(13));
					// xx.setIndustrycetegory(rs.getString(12));
					xx.setInvermodel(rs.getString(14));
					xx.setActctrlidtype(rs.getString(15));
					xx.setActctrlname(rs.getString(16));
					xx.setActctrlcode(rs.getString(17));
					xx.setPersonnum(rs.getString(18));
					xx.setOnestockcode(rs.getString(19));
					xx.setTwostockcode(rs.getString(20));
					xx.setThreestockcode(rs.getString(21));
					xx.setFourstockcode(rs.getString(22));
					xx.setFivestockcode(rs.getString(23));
					xx.setSixstockcode(rs.getString(24));
					xx.setSevenstockcode(rs.getString(25));
					xx.setEightstockcode(rs.getString(26));
					xx.setNinestockcode(rs.getString(27));
					xx.setTenstockcode(rs.getString(28));
					xx.setOnestockprop(rs.getString(29));
					xx.setTwostockprop(rs.getString(30));
					xx.setThreestockprop(rs.getString(31));
					xx.setFourstockprop(rs.getString(32));
					xx.setFivestockprop(rs.getString(33));
					xx.setSixstockprop(rs.getString(34));
					xx.setSevenstockprop(rs.getString(35));
					xx.setEightstockprop(rs.getString(36));
					xx.setNinestockprop(rs.getString(37));
					xx.setTenstockprop(rs.getString(38));
					xx.setSjrq(rs.getString(39));
					guiZeJiaoYanForJCQKTJ(xx, result, zhengqueid, cuowuid, customSqlUtil, checknumList);
				}
				XjrjgfrBaseinfoService xbs = (XjrjgfrBaseinfoService) obj;
				if (zhengqueid.size() > 0) {
					xbs.updateXjrjgfrBaseinfoOnCheckstatus("1", zhengqueid);
				}
				if (cuowuid.size() > 0) {
					xbs.updateXjrjgfrBaseinfoOnCheckstatus("2", cuowuid);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (ps != null) {
					try {
						ps.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		} else if (TableCodeEnum.LRJZBTJ.equals(code)) {
			String query = "select id,yysr,lxjsr,lxsr,jrjgwllxsr,xtnwllxsr,gxdklxsr,zqlxsr,qtlxsr,lxzc,jrjgwllxzc,xtnwllxzc,gxcklxzc,zqlxzc,qtlxzc,sxfjyjjsr,sxfjyjsr,"
					+ "jxfjyjzc,zlsy,tzsy,zqtzsy,gqtzsy,qttzsy,gyjzbdsy,hdjsy,qtywsr,yyzc,ywjglf,zggz,flf,zfgjjhzfbt,sjjfj,zcjzss,qtywzc,yylr,yywsr,yywzc,lrze,"
					+ "sds,jlr,ndsytz,lclr,wfplr,ynzzs,hxyjzbje,yjzbje,zbje,ygzbjfxjqzchj,zcczsy,sjrq from xjrjgfrprofit where checkstatus='0' and (datastatus='0' or datastatus='2') and departid like '"+departId+"'";
			try {
				con = JDBCUtil.getConnection("/intg/jdbc.properties", "mysql.driverClassName", "mysql.url",
						"mysql.username", "mysql.password");
				ps = con.prepareStatement(query);
				rs = ps.executeQuery();
				while (rs.next()) {
					XjrjgfrProfit xx = new XjrjgfrProfit();
					xx.setId(rs.getString(1));
					xx.setYysr(rs.getString(2));
					xx.setLxjsr(rs.getString(3));
					xx.setLxsr(rs.getString(4));
					xx.setJrjgwllxsr(rs.getString(5));
					xx.setXtnwllxsr(rs.getString(6));
					xx.setGxdklxsr(rs.getString(7));
					xx.setZqlxsr(rs.getString(8));
					xx.setQtlxsr(rs.getString(9));
					xx.setLxzc(rs.getString(10));
					xx.setJrjgwllxzc(rs.getString(11));
					xx.setXtnwllxzc(rs.getString(12));
					xx.setGxcklxzc(rs.getString(13));
					xx.setZqlxzc(rs.getString(14));
					xx.setQtlxzc(rs.getString(15));
					xx.setSxfjyjjsr(rs.getString(16));
					xx.setSxfjyjsr(rs.getString(17));
					xx.setJxfjyjzc(rs.getString(18));
					xx.setZlsy(rs.getString(19));
					xx.setTzsy(rs.getString(20));
					xx.setZqtzsy(rs.getString(21));
					xx.setGqtzsy(rs.getString(22));
					xx.setQttzsy(rs.getString(23));
					xx.setGyjzbdsy(rs.getString(24));
					xx.setHdjsy(rs.getString(25));
					xx.setQtywsr(rs.getString(26));
					xx.setYyzc(rs.getString(27));
					xx.setYwjglf(rs.getString(28));
					xx.setZggz(rs.getString(29));
					xx.setFlf(rs.getString(30));
					xx.setZfgjjhzfbt(rs.getString(31));
					xx.setSjjfj(rs.getString(32));
					xx.setZcjzss(rs.getString(33));
					xx.setQtywzc(rs.getString(34));
					xx.setYylr(rs.getString(35));
					xx.setYywsr(rs.getString(36));
					xx.setYywzc(rs.getString(37));
					xx.setLrze(rs.getString(38));
					xx.setSds(rs.getString(39));
					xx.setJlr(rs.getString(40));
					xx.setNdsytz(rs.getString(41));
					xx.setLclr(rs.getString(42));
					xx.setWfplr(rs.getString(43));
					xx.setYnzzs(rs.getString(44));
					xx.setHxyjzbje(rs.getString(45));
					xx.setYjzbje(rs.getString(46));
					xx.setZbje(rs.getString(47));
					xx.setYgzbjfxjqzchj(rs.getString(48));
					xx.setZcczsy(rs.getString(49));
					xx.setSjrq(rs.getString(50));
					guiZeJiaoYanForLRJZBTJ(xx, result, zhengqueid, cuowuid, customSqlUtil, checknumList);
					// shuJuBiDuiForLRJZBTJ(xx, result,zhengqueid,cuowuid);
				}
				XjrjgfrProfitService xps = (XjrjgfrProfitService) obj;
				if (zhengqueid.size() > 0) {
					xps.updateXjrjgfrProfitOnCheckstatus("1", zhengqueid);
				}
				if (cuowuid.size() > 0) {
					xps.updateXjrjgfrProfitOnCheckstatus("2", cuowuid);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (ps != null) {
					try {
						ps.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}

		if (result.length() == 0) {
			result.append("1");
		}

		// if (buffer.length() ==0){
		// result.append("3");
		//
		// }
		return result.toString();
	}

	/**
	 * 对金融机构（法人）基础信息-资产负债及风险统计表进行规则校验
	 * 
	 * @param data
	 *            校验的数据
	 * @param result
	 *            校验的结果
	 */
	public static void guiZeJiaoYanForZCFZJFXTJ(XjrjgfrAssets data, StringBuffer result, List<String> zhengqueid,
			List<String> cuowuid, CustomSqlUtil customSqlUtil, List<String> checknumList) {
		boolean haveerror = false;
		StringBuffer errorsb = new StringBuffer();

		// 各项存款
		if (StringUtils.isNotBlank(data.getGxck())) {
			Matcher match = pattern.matcher(data.getGxck());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0482")) {
					errorsb.append("各项存款值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2476")) {
				if (new BigDecimal(data.getGxck()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("各项存款值应大于等于0|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1167")) {
				errorsb.append("各项存款值不能为空|");
				haveerror = true;
			}
		}

		// 各项贷款
		if (StringUtils.isNotBlank(data.getGxdk())) {
			Matcher match = pattern.matcher(data.getGxdk());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0483")) {
					errorsb.append("各项贷款值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2355")) {
				if (new BigDecimal(data.getGxdk()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("各项贷款值应大于等于0|");
					haveerror = true;
				}
			}
//			if (checknumList.contains("JS2504")) {
//				if (StringUtils.isNotBlank(data.getZcldk()) && StringUtils.isNotBlank(data.getGzldk())
//						&& StringUtils.isNotBlank(data.getJyldk()) && StringUtils.isNotBlank(data.getCjldk())
//						&& StringUtils.isNotBlank(data.getSsldk())) {
//					if (new BigDecimal(data.getGxdk()).compareTo(new BigDecimal(data.getZcldk())
//							.add(new BigDecimal(data.getGzldk())).add(new BigDecimal(data.getJyldk()))
//							.add(new BigDecimal(data.getCjldk())).add(new BigDecimal(data.getSsldk()))) < 0) {
//						errorsb.append("正常类贷款、关注类贷款、可疑类贷款、次级类贷款与损失类贷款之和应小于等于各项贷款|");
//						haveerror = true;
//					}
//				}
//			}
		} else {
			if (checknumList.contains("JS1168")) {
				errorsb.append("各项贷款值不能为空|");
				haveerror = true;
			}
		}

		if (StringUtils.isNotBlank(data.getZczj())) {
			Matcher match = pattern.matcher(data.getZczj());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0484")) {
					errorsb.append("资产总计值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2480")) {
				if (new BigDecimal(data.getZczj()).compareTo(BigDecimal.ZERO) <= 0) {
					errorsb.append("资产总计应大于0|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2481")) {
				if (StringUtils.isNotBlank(data.getFzzj()) && StringUtils.isNotBlank(data.getSyzqyhj())) {
					if (new BigDecimal(data.getZczj()).compareTo(
							new BigDecimal(data.getFzzj()).add(new BigDecimal(data.getSyzqyhj()))) != 0) {
						errorsb.append("资产总计应等于负债总计+所有者权益合计|");
						haveerror = true;
					}
				}
			}
		} else {
			if (checknumList.contains("JS1169")) {
				errorsb.append("资产总计不能为空|");
				haveerror = true;
			}
		}

		if (StringUtils.isNotBlank(data.getFzzj())) {
			Matcher match = pattern.matcher(data.getFzzj());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0485")) {
					errorsb.append("负债总计值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2483")) {
				if (new BigDecimal(data.getFzzj()).compareTo(BigDecimal.ZERO) <= 0) {
					errorsb.append("负债总计应大于0|");
				}
			}
		} else {
			if (checknumList.contains("JS1170")) {
				errorsb.append("负债总计值不能为空|");
				haveerror = true;
			}
		}

		if (StringUtils.isNotBlank(data.getSyzqyhj())) {
			Matcher match = pattern.matcher(data.getSyzqyhj());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0486")) {
					errorsb.append("所有者权益合计值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2485")) {
				if (new BigDecimal(data.getSyzqyhj()).compareTo(BigDecimal.ZERO) <= 0) {
					errorsb.append("所有者权益合计应大于0|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1171")) {
				errorsb.append("所有者权益合计值不能为空|");
				haveerror = true;
			}
		}

		//生息资产
		if (StringUtils.isNotBlank(data.getSxzc())) {
			Matcher match = pattern.matcher(data.getSxzc());
			if (match.matches()) {
				

			} else {
				if (checknumList.contains("JS0487")) {
					errorsb.append("生息资产值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2486")) {
				if (new BigDecimal(data.getSxzc()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("生息资产应大于等于0|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2487")) {
				if (StringUtils.isNotBlank(data.getZczj())) {
					if (new BigDecimal(data.getSxzc()).compareTo(new BigDecimal(data.getZczj())) > 0) {
						errorsb.append("生息资产应小于等于资产总计|");
						haveerror = true;
					}
				}
			}
		} else {
			if (checknumList.contains("JS1172")) {
				errorsb.append("生息资产值不能为空|");
				haveerror = true;
			}
		}

		if (StringUtils.isNotBlank(data.getFxzc())) {
			Matcher match = pattern.matcher(data.getFxzc());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0488")) {
					errorsb.append("付息负债值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2488")) {
				if (new BigDecimal(data.getFxzc()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("付息负债值应大于等于0|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2489")) {
				if (StringUtils.isNotBlank(data.getFzzj())) {
					if (new BigDecimal(data.getFxzc()).compareTo(new BigDecimal(data.getFzzj())) > 0) {
						errorsb.append("付息负债应小于等于负债总计|");
						haveerror = true;
					}
				}
			}
		} else {
			if (checknumList.contains("JS1173")) {
				errorsb.append("付息负债值不能为空|");
				haveerror = true;
			}
		}

		//流动性资产
		if (StringUtils.isNotBlank(data.getLdxzc())) {
			Matcher match = pattern.matcher(data.getLdxzc());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0489")) {
					errorsb.append("流动性资产值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2490")) {
				if (new BigDecimal(data.getLdxzc()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("流动性资产值大于等于0");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2491")) {
				if (StringUtils.isNotBlank(data.getZczj())) {
					if (new BigDecimal(data.getLdxzc()).compareTo(new BigDecimal(data.getZczj())) > 0) {
						errorsb.append("流动性资产应小于等于资产总计|");
						haveerror = true;
					}
				}
			}
		} else {
			if (checknumList.contains("JS1174")) {
				errorsb.append("流动性资产值不能为空|");
				haveerror = true;
			}
		}

		// 流动性负债
		if (StringUtils.isNotBlank(data.getLdxfz())) {
			Matcher match = pattern.matcher(data.getLdxfz());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0490")) {
					errorsb.append("流动性负债值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2492")) {
				if (new BigDecimal(data.getLdxfz()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("流动性负债应大于等于0|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2493")) {
				if (StringUtils.isNotBlank(data.getFzzj())) {
					if (new BigDecimal(data.getLdxfz()).compareTo(new BigDecimal(data.getFzzj())) > 0) {
						errorsb.append("流动性负债应小于等于负债总计|");
						haveerror = true;
					}
				}
			}
		} else {
			if (checknumList.contains("JS1175")) {
				errorsb.append("流动性负债不能为空|");
				haveerror = true;
			}
		}

		//正常类贷款
		if (StringUtils.isNotBlank(data.getZcldk())) {
			Matcher match = pattern.matcher(data.getZcldk());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0491")) {
					errorsb.append("正常类贷款值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2494")) {
				if (new BigDecimal(data.getZcldk()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("正常类贷款应大于等于0|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1176")) {
				errorsb.append("正常类贷款不能为空|");
				haveerror = true;
			}
		}

		//关注类贷款
		if (StringUtils.isNotBlank(data.getGzldk())) {
			Matcher match = pattern.matcher(data.getGzldk());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0492")) {
					errorsb.append("关注类贷款值小数位必须为2位");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2496")) {
				if (new BigDecimal(data.getGzldk()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("关注类贷款应大于等于0|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1177")) {
				errorsb.append("关注类贷款不能为空");
				haveerror = true;
			}
		}

		if (StringUtils.isNotBlank(data.getCjldk())) {
			if (checknumList.contains("JS0493")) {
			Matcher match = pattern.matcher(data.getCjldk());
			if (match.matches()) {
				
			} else {
				
					errorsb.append("次级类贷款值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2498")) {
				if (new BigDecimal(data.getCjldk()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("次级类贷款应大于等于0|");
					haveerror = true;

				}
			}
		} else {
			if (checknumList.contains("JS1178")) {
				errorsb.append("次级贷款值不能为空|");
				haveerror = true;
			}
		}

		//可疑类贷款
		if (StringUtils.isNotBlank(data.getJyldk())) {
			Matcher match = pattern.matcher(data.getJyldk());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0494")) {
					errorsb.append("可疑类贷款值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2500")) {
				if (new BigDecimal(data.getJyldk()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("可疑类贷款应大于等于0|");
					haveerror = true;

				}
			}
		} else {
			if (checknumList.contains("JS1179")) {
				errorsb.append("可疑类贷款值不能为空|");
				haveerror = true;
			}
		}

		//损失类贷款
		if (StringUtils.isNotBlank(data.getSsldk())) {
			Matcher match = pattern.matcher(data.getSsldk());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0495")) {
					errorsb.append("损失类贷款值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2502")) {
				if (new BigDecimal(data.getSsldk()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("损失类贷款应大于等于0|");
					haveerror = true;

				}
			}
		} else {
			if (checknumList.contains("JS1180")) {
				errorsb.append("损失类贷款值不能为空|");
				haveerror = true;
			}
		}

		//逾期贷款
		if (StringUtils.isNotBlank(data.getYqdk())) {
			Matcher match = pattern.matcher(data.getYqdk());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0496")) {
					errorsb.append("逾期贷款值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2505")) {
				if (new BigDecimal(data.getYqdk()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("逾期贷款应大于等于0|");
					haveerror = true;

				}
			}
			if (checknumList.contains("JS2506")) {
				if (StringUtils.isNotBlank(data.getGxdk())) {
					if (new BigDecimal(data.getYqdk()).compareTo(new BigDecimal(data.getGxdk())) > 0) {
						errorsb.append("逾期贷款应小于等于各项贷款|");
						haveerror = true;
					}
				}
			}
		} else {
			if (checknumList.contains("JS1181")) {
				errorsb.append("逾期贷款不能为空|");
				haveerror = true;
			}
		}

		if (StringUtils.isNotBlank(data.getYqninetytysdk())) {
			Matcher match = pattern.matcher(data.getYqninetytysdk());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0497")) {
					errorsb.append("逾期90天以上贷款值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2509")) {
				if (StringUtils.isNotBlank(data.getYqdk())) {
					if (new BigDecimal(data.getYqninetytysdk()).compareTo(new BigDecimal(data.getYqdk())) > 0) {
						errorsb.append("逾期90天以上贷款应小于等于逾期贷款|");
						haveerror = true;
					}
				}
			}
			if (checknumList.contains("JS2510")) {
				if (StringUtils.isNotBlank(data.getYqninetytysdk()) && StringUtils.isNotBlank(data.getCjldk())
						&& StringUtils.isNotBlank(data.getSsldk())) {
					if (new BigDecimal(data.getYqninetytysdk()).compareTo(new BigDecimal(data.getCjldk())
							.add(new BigDecimal(data.getJyldk())).add(new BigDecimal(data.getSsldk()))) > 0) {
						errorsb.append("逾期90天以上贷款应小于等于次级类贷款、可疑类贷款与损失类贷款之和|");
						haveerror = true;

					}
				}
			}
			if (checknumList.contains("JS2508")) {
				if (new BigDecimal(data.getYqninetytysdk()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("逾期90天以上贷款大于等于0|");
					haveerror = true;
				}
			}

		} else {
			if (checknumList.contains("JS1182")) {
				errorsb.append("逾期90天以上贷款值不能为空|");
				haveerror = true;
			}
		}

		//贷款减值准备
		if (StringUtils.isNotBlank(data.getDkjzzb())) {
			Matcher match = pattern.matcher(data.getDkjzzb());
			if (match.matches()) {
				
			} else {
				if (checknumList.contains("JS0498")) {
					errorsb.append("贷款减值准备值小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2511")) {
				if (new BigDecimal(data.getDkjzzb()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("贷款减值准备应大于等于0|");
					haveerror = true;

				}
			}
		} else {
			if (checknumList.contains("JS1183")) {
				errorsb.append("贷款减值准备不能为空|");
				haveerror = true;
			}
		}

		// 数据日期
		haveerror = CheckUtil.nullAndDate(data.getSjrq(), errorsb, haveerror, "数据日期", false);

		if (haveerror) {
			result.append("该数据的id为:" + data.getId() + "->\n" + errorsb + "\n");
			// result.append("金融机构名称:"+data.getFinorgname()+"->\n"+errorsb+"\n");
			errorsb = null;
			cuowuid.add(data.getId());
		} else {
			zhengqueid.add(data.getId());
		}
	}

	/**
	 * 对金融机构（法人）基础信息-基础情况统计表 进行规则校验
	 * 
	 * @param data
	 *            校验的数据
	 * @param result
	 *            校验的结果
	 */
	public static void guiZeJiaoYanForJCQKTJ(XjrjgfrBaseinfo data, StringBuffer result, List<String> zhengqueid,
			List<String> cuowuid, CustomSqlUtil customSqlUtil, List<String> checknumList) {
		boolean haveerror = false;
		StringBuffer errorsb = new StringBuffer();
		// 金融机构名称
		if (StringUtils.isNotBlank(data.getFinorgname())) {
			if (data.getFinorgname().length() > 200) {
				if (checknumList.contains("JS0449")) {
					errorsb.append("金融机构名称长度不能超过200|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0446")) {
				if (CheckUtil.checkStr2(data.getFinorgname())) {
					errorsb.append(
							"金融机构名称字段内容中出现“？”、“！”、“$” 、“%” 、“^” 、“*” 、“|” 、单引号、双引号、回车符、换行符等与制度内容无关的分隔符或者开头或结尾包含空格。其中“？”和“！”包含全角和半角两种格式|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1147")) {
				errorsb.append("金融机构名称不能为空|");
				haveerror = true;
			}
		}
		// 金融机构代码
		if (StringUtils.isNotBlank(data.getFinorgcode())) {
			if (checknumList.contains("JS0450")) {
				if (data.getFinorgcode().length() != 18) {
					errorsb.append("金融机构代码长度应为18|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0421")) {
				if (CheckUtil.checkStr(data.getFinorgcode())) {
					errorsb.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1148")) {
				errorsb.append("金融机构代码不能为空|");
				haveerror = true;
			}
		}

		// 金融机构编码
		if (StringUtils.isNotBlank(data.getFinorgnum())) {
			if (checknumList.contains("JS0451")) {
				if (data.getFinorgnum().length() != 14) {

					errorsb.append("金融机构编码长度应为14|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0422")) {
				Matcher mat = pat.matcher(data.getFinorgnum());
				if (mat.find()) {

					errorsb.append("金融机构编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1149")) {
				// result.append("金融机构编码不能为空,");
				errorsb.append("金融机构编码不能为空|");
				haveerror = true;
			}
		}

		// 机构类别
		if (StringUtils.isNotBlank(data.getJglb())) {
			if (checknumList.contains("JS0167")) {
				if (!ArrayUtils.contains(CheckUtil.jglb, data.getJglb())) {
					errorsb.append("机构类别需在符合要求的最底层值域范围|");
					haveerror = true;
				}
			}

		} else {
			if (checknumList.contains("JS1164")) {
				errorsb.append("机构不能为空|");
				haveerror = true;
			}

		}

		// 注册地址
		if (StringUtils.isNotBlank(data.getRegaddress())) {
			if (checknumList.contains("JS0452")) {
				if (data.getRegaddress().length() > 400) {
					errorsb.append("注册地址长度大于400|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0447")) {
				if (CheckUtil.checkStr5(data.getRegaddress())) {
					errorsb.append("注册地址字段内容中不得出现“？”、“！”、“^”  。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1150")) {
				errorsb.append("注册地址不能为空|");
				haveerror = true;
			}
		}

		// 地区代码
		if (StringUtils.isNotBlank(data.getRegarea())) {
			if (checknumList.contains("JS0168")) {
				if (areaList == null)
					areaList = customSqlUtil.getBaseCode(BaseArea.class);
				if (countryList == null)
					countryList = customSqlUtil.getBaseCode(BaseCountry.class);
				if (!areaList.contains(data.getRegarea()) && !countryList.contains(data.getRegarea())) {
					errorsb.append("地区代码码值不正确|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1151")) {
				// result.append("地区代码不能为空,");
				errorsb.append("地区代码不能为空|");
				haveerror = true;
			}
		}

		// 注册资本
		if (StringUtils.isNotBlank(data.getRegamt())) {
			if (checknumList.contains("JS0468")) {
				if (data.getRegamt().length() > 20) {

					errorsb.append("注册资本长度大于20|");
					haveerror = true;
				}
			}
			Matcher match = pattern.matcher(data.getRegamt());
			if (match.matches()) {
				if (checknumList.contains("JS2464")) {
					if (new BigDecimal(data.getRegamt()).compareTo(BigDecimal.ZERO) <= 0) {
						errorsb.append("注册资本应大于0|");
						haveerror = true;
					}
				}
				
			}else {
				if (checknumList.contains("JS0468")) {
					errorsb.append("注册资本总长度不能超过20位，小数位必须为2位|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1152")) {
				errorsb.append("注册资本不能为空|");
				haveerror = true;
			}
		}

		// 成立日期
		if (StringUtils.isNotBlank(data.getSetupdate())) {
			// if(data.getSetupdate().length() != 10) {
			// errorsb.append("成立日期长度不为10|");
			// haveerror = true;
			// }else {
			// Matcher mat = pat.matcher(data.getSetupdate());
			// if (mat.find()) {
			//
			// errorsb.append("成立日期含有特殊字符|");
			// haveerror = true;
			// } else {
			if (checknumList.contains("JS0481")) {
				if(data.getSetupdate().length() != 10) {
					errorsb.append("成立日期格式错误|");
					haveerror = true;
				}
			}
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			format.setLenient(false);
			try {
				Date date = format.parse(data.getSetupdate());
				String s1 = "1800-01-01";
				String s2 = "2100-12-31";
				Date d1 = format.parse(s1);
				Date d2 = format.parse(s2);
				if (date.after(d1) && date.before(d2)) {

				} else {
					if (checknumList.contains("JS0481")) {
						errorsb.append("成立日期需晚于1800-01-01早于2100-12-31|");
						haveerror = true;
					}
				}
				if (checknumList.contains("JS2463")) {
					if (StringUtils.isNotBlank(data.getSjrq())) {
						if (data.getSetupdate().compareTo(data.getSjrq()) > 0) {
							errorsb.append("成立日期应小于等于数据日期|");
							haveerror = true;
						}
					}
				}
			} catch (ParseException e) {
				if (checknumList.contains("JS0481")) {
					errorsb.append("成立日期格式错误|");
					haveerror = true;
				}
			}
			// }
			// }
		} else {
			if (checknumList.contains("JS1153")) {
				errorsb.append("成立日期不能为空|");
				haveerror = true;
			}
		}

		// 从业人员数
		if (StringUtils.isNotBlank(data.getPersonnum())) {

			try {
				if (checknumList.contains("JS2465")) {
					int num = Integer.valueOf(data.getPersonnum());// 把字符串强制转换为数字
					if (num <= 0) {
						errorsb.append("从业人员数应大于0|");
						haveerror = true;
					}
				}
			} catch (Exception e) {
				if (checknumList.contains("JS0469")) {
					errorsb.append("从业人员数不为整数|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0469")) {
				if (data.getPersonnum().length() > 10) {
					errorsb.append("从业人员数长度超过10位|");
					haveerror = true;

				}
			}
		} else {
			if (checknumList.contains("JS1161")) {
				errorsb.append("从业人员数不能为空|");
				haveerror = true;
			}
		}

		// 联系电话
		if (StringUtils.isNotBlank(data.getPhone())) {
			if (checknumList.contains("JS0453")) {
				if (data.getPhone().length() > 30) {

					errorsb.append("联系电话字符长度不能超过30|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0423")) {
				if (CheckUtil.checkStr2(data.getPhone())) {
					errorsb.append("联系人字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;

				}
			}

		} else {
			if (checknumList.contains("JS1154")) {
				errorsb.append("联系电话不能为空|");
				haveerror = true;
			}
		}

		// 联系人
		if (StringUtils.isNotBlank(data.getLxr())) {
			if (checknumList.contains("JS0467")) {
				if (data.getLxr().length() > 200) {
					errorsb.append("联系人字段长度不超过200|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0445")) {
				if (CheckUtil.checkStr2(data.getLxr())) {
					errorsb.append(
							"联系人字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1562")) {
				errorsb.append("联系人不能为空|");
				haveerror = true;
			}
		}

		// 经营状态
		if (StringUtils.isNotBlank(data.getOrgmanagestatus())) {
			if (checknumList.contains("JS0169")) {
				if ("01".equals(data.getOrgmanagestatus()) || "02".equals(data.getOrgmanagestatus())
						|| "03".equals(data.getOrgmanagestatus()) || "04".equals(data.getOrgmanagestatus())
						|| "05".equals(data.getOrgmanagestatus()) || "06".equals(data.getOrgmanagestatus())
						|| "07".equals(data.getOrgmanagestatus()) || "99".equals(data.getOrgmanagestatus())) {

				} else {
					// result.append("机构经营状态码值不正确,");
					errorsb.append("经营状态需在符合要求的值域范围内|");
					haveerror = true;
				}
			}

		} else {
			if (checknumList.contains("JS1155")) {
				// result.append("机构经营状态不能为空,");
				errorsb.append("经营状态不能为空|");
				haveerror = true;
			}
		}

		// 经济成分
		if (StringUtils.isNotBlank(data.getOrgstoreconomy())) {
			// if(data.getOrgstoreconomy().length() > 5) {
			// //result.append("经济成分长度大于5,");
			// errorsb.append("经济成分长度大于5|");
			// haveerror = true;
			// }else {
			if (checknumList.contains("JS0170")) {
				if ("A01".equals(data.getOrgstoreconomy()) || "A0101".equals(data.getOrgstoreconomy())
						|| "A0102".equals(data.getOrgstoreconomy()) || "A02".equals(data.getOrgstoreconomy())
						|| "A0201".equals(data.getOrgstoreconomy()) || "A0202".equals(data.getOrgstoreconomy())
						|| "B01".equals(data.getOrgstoreconomy()) || "B0101".equals(data.getOrgstoreconomy())
						|| "B0102".equals(data.getOrgstoreconomy()) || "B02".equals(data.getOrgstoreconomy())
						|| "B0201".equals(data.getOrgstoreconomy()) || "B0202".equals(data.getOrgstoreconomy())
						|| "B03".equals(data.getOrgstoreconomy()) || "B0301".equals(data.getOrgstoreconomy())
						|| "B0302".equals(data.getOrgstoreconomy())) {
				} else {
					// result.append("经济成分码值不正确,");
					errorsb.append("经济成分码值不正确|");
					haveerror = true;
				}
			}
			// }
		} else {
			if (checknumList.contains("JS1156")) {
				// result.append("经济成分不能为空,");
				errorsb.append("经济成分不能为空|");
				haveerror = true;
			}
		}

		// 企业规模
		if (StringUtils.isNotBlank(data.getInvermodel())) {
			if (checknumList.contains("JS0171")) {
				if ("01".equals(data.getInvermodel()) || "02".equals(data.getInvermodel())
						|| "03".equals(data.getInvermodel()) || "04".equals(data.getInvermodel())) {

				} else {
					// result.append("企业规模码值不正确,");
					errorsb.append("企业规模码值不正确|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1157")) {
				errorsb.append("企业规模不能为空|");
				haveerror = true;
			}
		}

		// 实际控制人证件类型
		if (StringUtils.isNotBlank(data.getActctrlidtype())) {
			if (checknumList.contains("JS2585")) {
				if (data.getActctrlidtype().length() > 60) {
					errorsb.append("实际控制人证件类型字符长度不能超过60|");
					haveerror = true;

				}
			}
			if (checknumList.contains("JS2584")) {
				String zjlx[] = data.getActctrlidtype().split(",");
				for (String i : zjlx) {
					if (!ArrayUtils.contains(CheckUtil.bzrzjlx, i)) {
						errorsb.append("实际控制人证件类型需在符合要求的最底层值域范围内|");
						haveerror = true;
					}
				}
			}
		} else {
			if (checknumList.contains("JS2586")) {
				errorsb.append("实际控制人证件类型不能为空|");
				haveerror = true;
			}
		}

		// 实际控制人名称
		if (StringUtils.isNotBlank(data.getActctrlname())) {
			if (checknumList.contains("JS0455")) {
				if (data.getActctrlname().length() > 1000) {

					errorsb.append("实际控制人名称长度大于1000|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0448")) {
				if (CheckUtil.checkStr2(data.getActctrlname())) {
					errorsb.append("实际控制人名称字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1159")) {
				errorsb.append("实际控制人名称不能为空|");
				haveerror = true;
			}
		}

		// 实际控制人证件代码
		if (StringUtils.isNotBlank(data.getActctrlcode())) {
			if (checknumList.contains("JS0456")) {
				if (data.getActctrlcode().length() > 400) {

					errorsb.append("实际控制人证件代码长度大于400|");
					haveerror = true;
				}
			}
			if(checknumList.contains("JS2824")) {
				if(StringUtils.isNotBlank(data.getActctrlidtype()) && data.getActctrlidtype().equals("A01")) {
					if(!data.getActctrlcode().contains(",") && !data.getActctrlcode().contains("，") && data.getActctrlcode().length() != 18) {
						errorsb.append("如果实际控制人证件类型为A01，实际控制人证件代码长度必须为18|");
						haveerror = true;
					}
				}
			}
			if(checknumList.contains("JS2825")) {
				if(StringUtils.isNotBlank(data.getActctrlidtype()) && data.getActctrlidtype().equals("A02")) {
					if(!data.getActctrlcode().contains(",") && !data.getActctrlcode().contains("，") && data.getActctrlcode().length() != 9) {
						errorsb.append("如果实际控制人证件类型为A02，实际控制人证件代码长度必须为9|");
						haveerror = true;
					}
				}
			}
			if(checknumList.contains("JS2826")) {
				if(StringUtils.isNotBlank(data.getActctrlidtype()) && (data.getActctrlidtype().equals("B01") || data.getActctrlidtype().equals("B08"))) {
					if(!data.getActctrlcode().contains(",") && !data.getActctrlcode().contains("，") && data.getActctrlcode().length() != 46) {
						errorsb.append("如果实际控制人证件类型为B01-身份证或B08-临时身份证，实际控制人证件代码长度必须为46|");
						haveerror = true;
					}
				}
			}
			if(checknumList.contains("JS2827")) {
				if(StringUtils.isNotBlank(data.getActctrlidtype()) && data.getActctrlidtype().startsWith("B")) {
					if(!"B01".equals(data.getActctrlidtype()) && !"B08".equals(data.getActctrlidtype())) {
						if(!data.getActctrlcode().contains(",") && !data.getActctrlcode().contains("，") && data.getActctrlcode().length() != 32) {
							errorsb.append("如果实际控制人证件类型为B开头且不是B01-身份证和B08-临时身份证的，实际控制人证件代码长度必须为32|");
							haveerror = true;
						}
					}
				}
			}
			if (checknumList.contains("JS0424")) {
				if (CheckUtil.checkStr2(data.getActctrlcode())) {
					errorsb.append("实际控制人证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1160")) {
				errorsb.append("实际控制人证件代码不能为空|");
				haveerror = true;
			}
		}

		// 第一大股东
		if (StringUtils.isNotBlank(data.getOnestockcode())) {
			if (checknumList.contains("JS0457")) {
				if (data.getOnestockcode().length() > 60) {
					errorsb.append("第一大股东代码长度不能超过60|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0425")) {
				if (CheckUtil.checkStr2(data.getOnestockcode())) {
					errorsb.append(
							"第一大股东代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1162")) {
				errorsb.append("第一大股东代码不能为空|");
				haveerror = true;
			}
		}
		// 第二大股东
		if (StringUtils.isNotBlank(data.getTwostockcode())) {
			if (checknumList.contains("JS0458")) {
				if (data.getTwostockcode().length() > 60) {
					errorsb.append("当第二大股东代码不为空时，第二大股东代码字符长度不能超过60|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0426")) {
				if (CheckUtil.checkStr2(data.getTwostockcode())) {
					errorsb.append(
							"当第二大股东代码不为空时，第二大股东代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS2373")) {
				if (StringUtils.isNotBlank(data.getTwostockprop())) {
					errorsb.append("当第二大股东持股比例不为空时，第二大股东代码不能为空|");
					haveerror = true;
				}
			}
		}

		// 第三大股东
		if (StringUtils.isNotBlank(data.getThreestockcode())) {
			if (checknumList.contains("JS0459")) {
				if (data.getThreestockcode().length() > 60) {
					errorsb.append("当第三大股东代码不为空时，第三大股东代码字符长度不能超过60|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0427")) {
				if (CheckUtil.checkStr2(data.getThreestockcode())) {
					errorsb.append(
							"当第三大股东代码不为空时，第三大股东代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}

		} else {
			if (checknumList.contains("JS2374")) {
				if (StringUtils.isNotBlank(data.getThreestockprop())) {
					errorsb.append("当第三大股东持股比例不为空时，第三大股东代码不能为空|");
					haveerror = true;
				}
			}
		}
		// 第四大股东
		if (StringUtils.isNotBlank(data.getFourstockcode())) {
			if (checknumList.contains("JS0460")) {
				if (data.getFourstockcode().length() > 60) {
					errorsb.append("当第四大股东代码不为空时，第四大股东代码字符长度不能超过60|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0428")) {
				if (CheckUtil.checkStr2(data.getFourstockcode())) {
					errorsb.append(
							"当第四大股东代码不为空时，第四大股东代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}

		} else {
			if (checknumList.contains("JS2375")) {
				if (StringUtils.isNotBlank(data.getFourstockprop())) {
					errorsb.append("当第四大股东持股比例不为空时，第四大股东代码不能为空|");
					haveerror = true;
				}
			}
		}
		// 第五大股东
		if (StringUtils.isNotBlank(data.getFivestockcode())) {
			if (checknumList.contains("JS0461")) {
				if (data.getFivestockcode().length() > 60) {
					errorsb.append("当第五大股东代码不为空时，第五大股东代码字符长度不能超过60|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0429")) {
				if (CheckUtil.checkStr2(data.getFivestockcode())) {
					errorsb.append(
							"当第五大股东代码不为空时，第五大股东代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS2376")) {
				if (StringUtils.isNotBlank(data.getFivestockprop())) {
					errorsb.append("当第五大股东持股比例不为空时，第五大股东代码不能为空|");
					haveerror = true;
				}
			}
		}
		// 第六大股东
		if (StringUtils.isNotBlank(data.getSixstockcode())) {
			if (checknumList.contains("JS0462")) {
				if (data.getSixstockcode().length() > 60) {
					errorsb.append("当第六大股东代码不为空时，第六大股东代码字符长度不能超过60|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0430")) {
				if (CheckUtil.checkStr2(data.getSixstockcode())) {
					errorsb.append(
							"当第六大股东代码不为空时，第六大股东代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS2377")) {
				if (StringUtils.isNotBlank(data.getSixstockprop())) {
					errorsb.append("当第六大股东持股比例不为空时，第六大股东代码不能为空|");
					haveerror = true;
				}
			}
		}
		// 第七大股东
		if (StringUtils.isNotBlank(data.getSevenstockcode())) {
			if (checknumList.contains("JS0463")) {
				if (data.getSevenstockcode().length() > 60) {
					errorsb.append("当第七大股东代码不为空时，第七大股东代码字符长度不能超过60|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0431")) {
				if (CheckUtil.checkStr2(data.getSevenstockcode())) {
					errorsb.append(
							"当第七大股东代码不为空时，第七大股东代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS2378")) {
				if (StringUtils.isNotBlank(data.getSevenstockprop())) {
					errorsb.append("当第七大股东持股比例不为空时，第七大股东代码不能为空|");
					haveerror = true;
				}
			}
		}
		// 第八大股东
		if (StringUtils.isNotBlank(data.getEightstockcode())) {
			if (checknumList.contains("JS0464")) {
				if (data.getEightstockcode().length() > 60) {
					errorsb.append("当第八大股东代码不为空时，第八大股东代码字符长度不能超过60|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0432")) {
				if (CheckUtil.checkStr2(data.getEightstockcode())) {
					errorsb.append(
							"当第八大股东代码不为空时，第八大股东代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS2379")) {
				if (StringUtils.isNotBlank(data.getEightstockprop())) {
					errorsb.append("当第八大股东持股比例不为空时，第八大股东代码不能为空|");
					haveerror = true;
				}
			}
		}
		// 第九大股东
		if (StringUtils.isNotBlank(data.getNinestockcode())) {
			if (checknumList.contains("JS0465")) {
				if (data.getNinestockcode().length() > 60) {
					errorsb.append("当第九大股东代码不为空时，第九大股东代码字符长度不能超过60|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0433")) {
				if (CheckUtil.checkStr2(data.getNinestockcode())) {
					errorsb.append(
							"当第九大股东代码不为空时，第九大股东代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS2380")) {
				if (StringUtils.isNotBlank(data.getNinestockprop())) {
					errorsb.append("当第九大股东持股比例不为空时，第九大股东代码不能为空|");
					haveerror = true;
				}
			}
		}
		// 第十大股东
		if (StringUtils.isNotBlank(data.getTenstockcode())) {
			if (checknumList.contains("JS0466")) {
				if (data.getTenstockcode().length() > 60) {
					errorsb.append("当第十大股东代码不为空时，第十大股东代码字符长度不能超过60|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0434")) {
				if (CheckUtil.checkStr2(data.getTenstockcode())) {
					errorsb.append(
							"当第十大股东代码不为空时，第十大股东代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS2381")) {
				if (StringUtils.isNotBlank(data.getTenstockprop())) {
					errorsb.append("当第十大股东持股比例不为空时，第十大股东代码不能为空|");
					haveerror = true;
				}
			}
		}
		// 第一大股东持股比例
		if (StringUtils.isNotBlank(data.getOnestockprop())) {
			if (checknumList.contains("JS0470")) {
				if (data.getOnestockprop().length() > 6 || !CheckUtil.checkDecimal(data.getOnestockprop(), 2)) {
					errorsb.append("第一大股东持股比例总长度不能超过5位，小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2466")) {
				if (new BigDecimal(data.getOnestockprop()).compareTo(BigDecimal.ONE) < 0
						|| new BigDecimal(data.getOnestockprop()).compareTo(new BigDecimal("100")) > 0) {
					errorsb.append("境内机构的第一大股东持股比例应大于等于1且小于等于100|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0435")) {
				if (CheckUtil.checkPerSign(data.getOnestockprop())) {
					errorsb.append("第一大股东持股比例不能包含‰或%|");
					haveerror = true;
				}
			}
		} else {
			if (checknumList.contains("JS1163")) {
				errorsb.append("第一大股东持股比例不能为空|");
				haveerror = true;
			}
		}

		// 第二大股东持股比例
		if (StringUtils.isNotBlank(data.getTwostockprop())) {
			if (checknumList.contains("JS0471")) {
				if (data.getTwostockprop().length() > 6 || !CheckUtil.checkDecimal(data.getOnestockprop(), 2)) {
					errorsb.append("第二大股东持股比例长度不能超过5位,小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0436")) {
				if (CheckUtil.checkPerSign(data.getTwostockprop())) {
					errorsb.append("第二大股东持股比例不能包含‰或%|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2467")) {
				if (StringUtils.isNotBlank(data.getTwostockcode())) {
					if (new BigDecimal(data.getTwostockprop()).compareTo(BigDecimal.ONE) < 0
							|| new BigDecimal(data.getTwostockprop()).compareTo(new BigDecimal("100")) > 0) {
						errorsb.append("当第二大股东代码不为空时，第二大股东持股比例应大于等于1且小于等于100|");
						haveerror = true;
					}
				}
			}
		}

		// 第三大股东持股比例
		if (StringUtils.isNotBlank(data.getThreestockprop())) {
			if (checknumList.contains("JS0472")) {
				if (data.getThreestockprop().length() > 6 || !CheckUtil.checkDecimal(data.getThreestockprop(), 2)) {
					errorsb.append("第三大股东持股比例长度不能超过5位,小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0437")) {
				if (CheckUtil.checkPerSign(data.getThreestockprop())) {
					errorsb.append("第三大股东持股比例不能包含‰或%|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2468")) {
				if (StringUtils.isNotBlank(data.getThreestockcode())) {
					if (new BigDecimal(data.getThreestockprop()).compareTo(BigDecimal.ONE) < 0
							|| new BigDecimal(data.getThreestockprop()).compareTo(new BigDecimal("100")) > 0) {
						errorsb.append("当第三大股东代码不为空时，第三大股东持股比例应大于等于1且小于等于100|");
						haveerror = true;
					}
				}
			}
		}

		// 第四大股东持股比例
		if (StringUtils.isNotBlank(data.getFourstockprop())) {
			if (checknumList.contains("JS0473")) {
				if (data.getFourstockprop().length() > 6 || !CheckUtil.checkDecimal(data.getFourstockprop(), 2)) {
					errorsb.append("第四大股东持股比例长度不能超过5位,小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0438")) {
				if (CheckUtil.checkPerSign(data.getFourstockprop())) {
					errorsb.append("第四大股东持股比例不能包含‰或%|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2469")) {
				if (StringUtils.isNotBlank(data.getFourstockcode())) {
					if (new BigDecimal(data.getFourstockprop()).compareTo(BigDecimal.ZERO) <= 0
							|| new BigDecimal(data.getFourstockprop()).compareTo(new BigDecimal("100")) > 0) {
						errorsb.append("当第四大股东代码不为空时，第四大股东持股比例应大于0且小于等于100|");
						haveerror = true;
					}
				}
			}
		}

		// 第五大股东持股比例
		if (StringUtils.isNotBlank(data.getFivestockprop())) {
			if (checknumList.contains("JS0474")) {
				if (data.getFivestockprop().length() > 6 || !CheckUtil.checkDecimal(data.getFivestockprop(), 2)) {
					errorsb.append("第五大股东持股比例长度不能超过5位,小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0439")) {
				if (CheckUtil.checkPerSign(data.getFivestockprop())) {
					errorsb.append("第五大股东持股比例不能包含‰或%|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2470")) {
				if (StringUtils.isNotBlank(data.getFivestockcode())) {
					if (new BigDecimal(data.getFivestockprop()).compareTo(BigDecimal.ZERO) <= 0
							|| new BigDecimal(data.getFivestockprop()).compareTo(new BigDecimal("100")) > 0) {
						errorsb.append("当第五大股东代码不为空时，第五大股东持股比例应大于0且小于等于100|");
						haveerror = true;
					}
				}
			}
		}

		// 第六大股东持股比例
		if (StringUtils.isNotBlank(data.getSixstockprop())) {
			if (checknumList.contains("JS0475")) {
				if (data.getSixstockprop().length() > 6 || !CheckUtil.checkDecimal(data.getSixstockprop(), 2)) {
					errorsb.append("第六大股东持股比例长度不能超过5位,小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0440")) {
				if (CheckUtil.checkPerSign(data.getSixstockprop())) {
					errorsb.append("第六大股东持股比例不能包含‰或%|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS2471")) {
				if (StringUtils.isNotBlank(data.getSixstockcode())) {
					if (new BigDecimal(data.getSixstockprop()).compareTo(BigDecimal.ZERO) <= 0
							|| new BigDecimal(data.getSixstockprop()).compareTo(new BigDecimal("100")) > 0) {
						errorsb.append("当第六大股东代码不为空时，第六大股东持股比例应大于0且小于等于100|");
						haveerror = true;
					}
				}
			}
		}

		// 第七大股东持股比例
		if (StringUtils.isNotBlank(data.getSevenstockprop())) {
			if (checknumList.contains("JS0476")) {
				if (data.getSevenstockprop().length() > 6 || !CheckUtil.checkDecimal(data.getSevenstockprop(), 2)) {
					errorsb.append("第七大股东持股比例长度不能超过5位,小数位必须为2位|");
					haveerror = true;
				}
			}
			if (checknumList.contains("JS0441")) {
			if (CheckUtil.checkPerSign(data.getSevenstockprop())) {
				errorsb.append("第七大股东持股比例不能包含‰或%|");
				haveerror = true;
			}
			}
			if (checknumList.contains("JS2472")) {
			if (StringUtils.isNotBlank(data.getSevenstockcode())) {
				if (new BigDecimal(data.getSevenstockprop()).compareTo(BigDecimal.ZERO) <= 0
						|| new BigDecimal(data.getSevenstockprop()).compareTo(new BigDecimal("100")) > 0) {
					errorsb.append("当第七大股东代码不为空时，第七大股东持股比例应大于0且小于等于100|");
					haveerror = true;
				}
			}
			}
		}

		// 第八大股东持股比例
		if (StringUtils.isNotBlank(data.getEightstockprop())) {
			if (checknumList.contains("JS0477")) {
			if (data.getEightstockprop().length() > 6 || !CheckUtil.checkDecimal(data.getEightstockprop(), 2)) {
				errorsb.append("第八大股东持股比例长度不能超过5位,小数位必须为2位|");
				haveerror = true;
			}
			}
			if (checknumList.contains("JS0442")) {
			if (CheckUtil.checkPerSign(data.getEightstockprop())) {
				errorsb.append("第八大股东持股比例不能包含‰或%|");
				haveerror = true;
			}
			}
			if (checknumList.contains("JS2473")) {
			if (StringUtils.isNotBlank(data.getEightstockcode())) {
				if (new BigDecimal(data.getEightstockprop()).compareTo(BigDecimal.ZERO) <= 0
						|| new BigDecimal(data.getEightstockprop()).compareTo(new BigDecimal("100")) > 0) {
					errorsb.append("当第八大股东代码不为空时，第八大股东持股比例应大于0且小于等于100|");
					haveerror = true;
				}
			}
			}

		}

		// 第九大股东持股比例
		if (StringUtils.isNotBlank(data.getNinestockprop())) {
			if (checknumList.contains("JS0478")) {
			if (data.getNinestockprop().length() > 6 || !CheckUtil.checkDecimal(data.getNinestockprop(), 2)) {
				errorsb.append("第九大股东持股比例长度不能超过5位,小数位必须为2位|");
				haveerror = true;
			}
			}
			if (checknumList.contains("JS0443")) {
			if (CheckUtil.checkPerSign(data.getNinestockprop())) {
				errorsb.append("第九大股东持股比例不能包含‰或%|");
				haveerror = true;
			}
			}
			if (checknumList.contains("JS2474")) {
			if (StringUtils.isNotBlank(data.getNinestockcode())) {
				if (new BigDecimal(data.getNinestockprop()).compareTo(BigDecimal.ZERO) <= 0
						|| new BigDecimal(data.getNinestockprop()).compareTo(new BigDecimal("100")) > 0) {
					errorsb.append("当第九大股东代码不为空时，第八大股东持股比例应大于0且小于等于100|");
					haveerror = true;
				}
			}
			}
		}

		// 第十大股东持股比例
		if (StringUtils.isNotBlank(data.getTenstockprop())) {
			if (checknumList.contains("JS0479")) {
			if (data.getTenstockprop().length() > 6 || !CheckUtil.checkDecimal(data.getTenstockprop(), 2)) {
				errorsb.append("第十大股东持股比例长度不能超过5位,小数位必须为2位|");
				haveerror = true;
			}
			}
			if (checknumList.contains("JS0444")) {
			if (CheckUtil.checkPerSign(data.getTenstockprop())) {
				errorsb.append("第十大股东持股比例不能包含‰或%|");
				haveerror = true;
			}
			}
			if (checknumList.contains("JS2475")) {
			if (StringUtils.isNotBlank(data.getTenstockcode())) {
				if (new BigDecimal(data.getTenstockprop()).compareTo(BigDecimal.ZERO) <= 0
						|| new BigDecimal(data.getTenstockprop()).compareTo(new BigDecimal("100")) > 0) {
					errorsb.append("当第十大股东代码不为空时，第十大股东持股比例应大于0且小于等于100|");
					haveerror = true;
				}
			}
			}
		}

		if (checknumList.contains("JS2348")) {
		// 股东持股比列求和判断
		BigDecimal sum = new BigDecimal(0);
		try {
		if (StringUtils.isNotBlank(data.getOnestockprop()))
			sum = sum.add(new BigDecimal(data.getOnestockprop()));
		if (StringUtils.isNotBlank(data.getTwostockprop()))
			sum = sum.add(new BigDecimal(data.getTwostockprop()));
		if (StringUtils.isNotBlank(data.getThreestockprop()))
			sum = sum.add(new BigDecimal(data.getThreestockprop()));
		if (StringUtils.isNotBlank(data.getFourstockprop()))
			sum = sum.add(new BigDecimal(data.getFourstockprop()));
		if (StringUtils.isNotBlank(data.getFivestockprop()))
			sum = sum.add(new BigDecimal(data.getFivestockprop()));
		if (StringUtils.isNotBlank(data.getSixstockprop()))
			sum = sum.add(new BigDecimal(data.getSixstockprop()));
		if (StringUtils.isNotBlank(data.getSevenstockprop()))
			sum = sum.add(new BigDecimal(data.getSevenstockprop()));
		if (StringUtils.isNotBlank(data.getEightstockprop()))
			sum = sum.add(new BigDecimal(data.getEightstockprop()));
		if (StringUtils.isNotBlank(data.getNinestockprop()))
			sum = sum.add(new BigDecimal(data.getNinestockprop()));
		if (StringUtils.isNotBlank(data.getTenstockprop()))
			sum = sum.add(new BigDecimal(data.getTenstockprop()));
		;
		if (sum.compareTo(new BigDecimal("100")) > 0) {
			errorsb.append("前十大股东持股比例相加应该小于等于100|");
			haveerror = true;
		}
		}catch(Exception e) {
			errorsb.append("股东持股比例数值不正确|");
			haveerror = true;
		}
		}
		// 股东逐个填写
		if (checknumList.contains("JS2154")) {
		Boolean A = true;
		if (StringUtils.isNotBlank(data.getTenstockprop()) && A) {
			if (!StringUtils.isNotBlank(data.getOnestockcode()) || !StringUtils.isNotBlank(data.getTwostockcode())
					|| !StringUtils.isNotBlank(data.getThreestockcode())
					|| !StringUtils.isNotBlank(data.getFourstockcode())
					|| !StringUtils.isNotBlank(data.getFivestockcode())
					|| !StringUtils.isNotBlank(data.getSixstockcode())
					|| !StringUtils.isNotBlank(data.getSevenstockprop())
					|| !StringUtils.isNotBlank(data.getEightstockcode())
					|| !StringUtils.isNotBlank(data.getNinestockcode())) {
				errorsb.append("股东代码应依次填写，中间不能跳行|");
				haveerror = true;
				A = false;

			}

		} else if (StringUtils.isNotBlank(data.getNinestockprop()) && A) {
			if (!StringUtils.isNotBlank(data.getOnestockcode()) || !StringUtils.isNotBlank(data.getTwostockcode())
					|| !StringUtils.isNotBlank(data.getThreestockcode())
					|| !StringUtils.isNotBlank(data.getFourstockcode())
					|| !StringUtils.isNotBlank(data.getFivestockcode())
					|| !StringUtils.isNotBlank(data.getSixstockcode())
					|| !StringUtils.isNotBlank(data.getSevenstockprop())
					|| !StringUtils.isNotBlank(data.getEightstockcode())) {
				errorsb.append("股东代码应依次填写，中间不能跳行|");
				haveerror = true;
				A = false;
			}

		} else if (StringUtils.isNotBlank(data.getEightstockprop()) && A) {
			if (!StringUtils.isNotBlank(data.getOnestockcode()) || !StringUtils.isNotBlank(data.getTwostockcode())
					|| !StringUtils.isNotBlank(data.getThreestockcode())
					|| !StringUtils.isNotBlank(data.getFourstockcode())
					|| !StringUtils.isNotBlank(data.getFivestockcode())
					|| !StringUtils.isNotBlank(data.getSixstockcode())
					|| !StringUtils.isNotBlank(data.getSevenstockprop())) {
				errorsb.append("股东代码应依次填写，中间不能跳行|");
				haveerror = true;
				A = false;
			}
		} else if (StringUtils.isNotBlank(data.getSevenstockprop()) && A) {
			if (!StringUtils.isNotBlank(data.getOnestockcode()) || !StringUtils.isNotBlank(data.getTwostockcode())
					|| !StringUtils.isNotBlank(data.getThreestockcode())
					|| !StringUtils.isNotBlank(data.getFourstockcode())
					|| !StringUtils.isNotBlank(data.getFivestockcode())
					|| !StringUtils.isNotBlank(data.getSixstockcode())) {
				errorsb.append("股东代码应依次填写，中间不能跳行|");
				haveerror = true;
				A = false;
			}
		} else if (StringUtils.isNotBlank(data.getSixstockprop()) && A) {
			if (!StringUtils.isNotBlank(data.getOnestockcode()) || !StringUtils.isNotBlank(data.getTwostockcode())
					|| !StringUtils.isNotBlank(data.getThreestockcode())
					|| !StringUtils.isNotBlank(data.getFourstockcode())
					|| !StringUtils.isNotBlank(data.getFivestockcode())) {
				errorsb.append("股东代码应依次填写，中间不能跳行|");
				haveerror = true;
				A = false;

			}
		} else if (StringUtils.isNotBlank(data.getFivestockprop()) && A) {
			if (!StringUtils.isNotBlank(data.getOnestockcode()) || !StringUtils.isNotBlank(data.getTwostockcode())
					|| !StringUtils.isNotBlank(data.getThreestockcode())
					|| !StringUtils.isNotBlank(data.getFourstockcode())) {
				errorsb.append("股东代码应依次填写，中间不能跳行|");
				haveerror = true;
				A = false;

			}
		} else if (StringUtils.isNotBlank(data.getFourstockprop()) && A) {
			if (!StringUtils.isNotBlank(data.getOnestockcode()) || !StringUtils.isNotBlank(data.getTwostockcode())
					|| !StringUtils.isNotBlank(data.getThreestockcode())) {
				errorsb.append("股东代码应依次填写，中间不能跳行|");
				haveerror = true;
				A = false;

			}
		} else if (StringUtils.isNotBlank(data.getThreestockprop()) && A) {
			if (!StringUtils.isNotBlank(data.getOnestockcode()) || !StringUtils.isNotBlank(data.getTwostockcode())) {
				errorsb.append("股东代码应依次填写，中间不能跳行|");
				haveerror = true;
				A = false;

			}
		} else if (StringUtils.isNotBlank(data.getTwostockprop()) && A) {
			if (!StringUtils.isNotBlank(data.getOnestockcode())) {
				errorsb.append("股东代码应依次填写，中间不能跳行|");
				haveerror = true;
				A = false;
			}

		} else {

		}
		}
		// 数据日期
		haveerror = CheckUtil.nullAndDate(data.getSjrq(), errorsb, haveerror, "数据日期", false);

		if (haveerror) {
			// result.append("该数据的id为:"+data.getId()+";");
			result.append("金融机构代码:" + data.getFinorgcode() + "->\n" + errorsb + "\n");
			errorsb = null;
			cuowuid.add(data.getId());
		} else {
			zhengqueid.add(data.getId());
		}
	}

	/**
	 * 对金融机构（法人基础信息）-利润及资本统计表进行规则校验
	 * 
	 * @param data
	 *            校验的数据
	 * @param result
	 *            校验的结果
	 */
	public static void guiZeJiaoYanForLRJZBTJ(XjrjgfrProfit data, StringBuffer result, List<String> zhengqueid,
			List<String> cuowuid, CustomSqlUtil customSqlUtil, List<String> checknumList) {
		boolean haveerror = false;
		StringBuffer errorsb = new StringBuffer();
		JDBCUtils jdbc = new JDBCUtils();
		Connection conn = jdbc.getConnection();
		ResultSet rs = null;

		try {
			// 营业收入
			if (StringUtils.isNotBlank(data.getYysr())) {
				if (checknumList.contains("JS0500")) {
				Matcher match = pattern.matcher(data.getYysr());
				if (match.matches()) {					

				} else {
					errorsb.append("营业收入小数位必须为2位|");
					haveerror = true;
				}
				}
//				if (checknumList.contains("JS2513")) {
//				if (new BigDecimal(data.getYysr()).compareTo(BigDecimal.ZERO) <= 0) {
//					errorsb.append("营业收入应大于0|");
//					haveerror = true;
//				}
//				}
				if (checknumList.contains("JS2514")) {
				try {
					BigDecimal b1;
					BigDecimal b2;
					BigDecimal b3;
					BigDecimal b4;
					BigDecimal b5;
					BigDecimal b6;
					BigDecimal b7;
					BigDecimal b8;
					BigDecimal b9;
					if (StringUtils.isNotBlank(data.getYysr()) && data.getYysr().length() > 0) {
						b1 = new BigDecimal(data.getYysr());
					} else {
						b1 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getLxjsr()) && data.getLxjsr().length() > 0) {
						b2 = new BigDecimal(data.getLxjsr());
					} else {
						b2 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getSxfjyjjsr()) && data.getSxfjyjjsr().length() > 0) {
						b3 = new BigDecimal(data.getSxfjyjjsr());
					} else {
						b3 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getZlsy()) && data.getZlsy().length() > 0) {
						b4 = new BigDecimal(data.getZlsy());
					} else {
						b4 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getTzsy()) && data.getTzsy().length() > 0) {
						b5 = new BigDecimal(data.getTzsy());
					} else {
						b5 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getGyjzbdsy()) && data.getGyjzbdsy().length() > 0) {
						b6 = new BigDecimal(data.getGyjzbdsy());
					} else {
						b6 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getHdjsy()) && data.getHdjsy().length() > 0) {
						b7 = new BigDecimal(data.getHdjsy());
					} else {
						b7 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getQtywsr()) && data.getQtywsr().length() > 0) {
						b8 = new BigDecimal(data.getQtywsr());
					} else {
						b8 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getZcczsy()) && data.getZcczsy().length() > 0) {
						b9 = new BigDecimal(data.getZcczsy());
					} else {
						b9 = new BigDecimal(0);
					}
					if (b1.compareTo(b2.add(b3).add(b4).add(b5).add(b6).add(b7).add(b8).add(b9)) == 0) {

					} else {

						errorsb.append("营业收入值应等于利息净收入、手续费及佣金净收入、租赁收益、投资收益、公允价值变动收益、汇兑净收益、资产处置收益与其他业务收入之和|");
						haveerror = true;
					}
				} catch (Exception e) {

					errorsb.append("营业收入值求和不正确|");
					haveerror = true;
					e.printStackTrace();
				}
				}
			} else {
				if (checknumList.contains("JS1186")) {
				errorsb.append("营业收入值不能为空|");
				haveerror = true;
				}
			}

			// 利息净收入
			if (StringUtils.isNotBlank(data.getLxjsr())) {
				Matcher match = pattern.matcher(data.getLxjsr());
				if (match.matches()) {
					// if(new BigDecimal(data.getLxjsr()).compareTo(BigDecimal.ZERO)<0){
					// errorsb.append("利息净收入值应大于等于0|");
					// haveerror = true;
					// }
					
				} else {
					if (checknumList.contains("JS0501")) {
					errorsb.append("利息净收入小数位必须为2位|");
					haveerror = true;
					}
				}
				if (checknumList.contains("JS2516")) {
				try {
					BigDecimal b1;
					BigDecimal b2;
					BigDecimal b3;
					if (StringUtils.isNotBlank(data.getLxjsr()) && data.getLxjsr().length() > 0) {
						b1 = new BigDecimal(data.getLxjsr());
					} else {
						b1 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getLxsr()) && data.getLxsr().length() > 0) {
						b2 = new BigDecimal(data.getLxsr());
					} else {
						b2 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getLxzc()) && data.getLxzc().length() > 0) {
						b3 = new BigDecimal(data.getLxzc());
					} else {
						b3 = new BigDecimal(0);
					}

					if (b1.compareTo(b2.subtract(b3)) == 0) {

					} else {
						errorsb.append("利息净收入值应等于利息收入减利息支出|");
						haveerror = true;
					}
				} catch (Exception e) {

					errorsb.append("利息净收入值求差不正确|");
					haveerror = true;
					e.printStackTrace();
				}
				}
			} else {
				if (checknumList.contains("JS1187")) {
				errorsb.append("利息净收入值不能为空|");
				haveerror = true;
				}
			}

			// 利息收入
			if (StringUtils.isNotBlank(data.getLxsr())) {
				Matcher match = pattern.matcher(data.getLxsr());
				if (match.matches()) {
					
				} else {
					if (checknumList.contains("JS0502")) {
					errorsb.append("利息收入小数位必须为2位|");
					haveerror = true;
					}
				}
				if (checknumList.contains("JS2517")) {
				if (new BigDecimal(data.getLxsr()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("利息收入值应大于等于0|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2518")) {
				try {
					BigDecimal b1;
					BigDecimal b2;
					BigDecimal b3;
					BigDecimal b4;
					BigDecimal b5;
					if (StringUtils.isNotBlank(data.getLxsr()) && data.getLxsr().length() > 0) {
						b1 = new BigDecimal(data.getLxsr());
					} else {
						b1 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getJrjgwllxsr()) && data.getJrjgwllxsr().length() > 0) {
						b2 = new BigDecimal(data.getJrjgwllxsr());
					} else {
						b2 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getGxdklxsr()) && data.getGxdklxsr().length() > 0) {
						b3 = new BigDecimal(data.getGxdklxsr());
					} else {
						b3 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getZqlxsr()) && data.getZqlxsr().length() > 0) {
						b4 = new BigDecimal(data.getZqlxsr());
					} else {
						b4 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getQtlxsr()) && data.getQtlxsr().length() > 0) {
						b5 = new BigDecimal(data.getQtlxsr());
					} else {
						b5 = new BigDecimal(0);
					}

					if (b1.compareTo(b2.add(b3).add(b4).add(b5)) == 0) {

					} else {
						errorsb.append("利息收入值应等于金融机构往来利息收入、各项贷款利息收入、债券利息收入与其他利息收入之和|");
						haveerror = true;
					}
				} catch (Exception e) {
					errorsb.append("利息收入值求和不正确|");
					haveerror = true;
					e.printStackTrace();
				}
				}
			} else {
				if (checknumList.contains("JS1188")) {
				errorsb.append("利息收入值不能为空|");
				haveerror = true;
				}
			}

			// 金融机构往来利息收入
			if (StringUtils.isNotBlank(data.getJrjgwllxsr())) {
				if (checknumList.contains("JS0503")) {
				Matcher match = pattern.matcher(data.getJrjgwllxsr());
				if (match.matches()) {
					
				} else {

					errorsb.append("金融机构往来利息收入小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2519")) {
				if (new BigDecimal(data.getJrjgwllxsr()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("金融机构往来利息收入值应大于等于0|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2520")) {
				BigDecimal b1;
				BigDecimal b2;
				if (StringUtils.isNotBlank(data.getJrjgwllxsr()) && data.getJrjgwllxsr().length() > 0) {
					b1 = new BigDecimal(data.getJrjgwllxsr());
				} else {
					b1 = new BigDecimal(0);
				}
				if (StringUtils.isNotBlank(data.getXtnwllxsr()) && data.getXtnwllxsr().length() > 0) {
					b2 = new BigDecimal(data.getXtnwllxsr());
				} else {
					b2 = new BigDecimal(0);
				}

				if (b1.compareTo(b2) != -1) {

				} else {
					errorsb.append("金融机构往来利息收入值应大于等于其中：系统内往来利息收入|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1189")) {
				errorsb.append("金融机构往来利息收入值不能为空|");
				haveerror = true;
				}
			}

			// 其中：系统内往来利息收入
			if (StringUtils.isNotBlank(data.getXtnwllxsr())) {
				if (checknumList.contains("JS0504")) {
				Matcher match = pattern.matcher(data.getXtnwllxsr());
				if (match.matches()) {
					
				} else {
					errorsb.append("其中：系统内往来利息收入小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2521")) {
				if (new BigDecimal(data.getXtnwllxsr()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("其中：系统内往来利息收入应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1190")) {
				errorsb.append("其中：系统内往来利息收入值不能为空|");
				haveerror = true;
				}
			}

			// 各项贷款利息收入
			if (StringUtils.isNotBlank(data.getGxdklxsr())) {
				if (checknumList.contains("JS0505")) {
				Matcher match = pattern.matcher(data.getGxdklxsr());
				if (match.matches()) {
					
				} else {

					errorsb.append("各项贷款利息收入小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2522")) {
				if (new BigDecimal(data.getGxdklxsr()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("各项贷款利息收入值应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1191")) {
				errorsb.append("各项贷款利息收入值不能为空");
				haveerror = true;
				}
			}

			//债券利息收入
			if (StringUtils.isNotBlank(data.getZqlxsr())) {
				if (checknumList.contains("JS0506")) {
				Matcher match = pattern.matcher(data.getZqlxsr());
				if (match.matches()) {
					
				} else {

					errorsb.append("债券利息收入小数位必须为2位 |");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2523")) {
				if (new BigDecimal(data.getZqlxsr()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("债券利息收入应大于等于0|");
					haveerror = true;

				}
				}
			} else {
				if (checknumList.contains("JS1192")) {
				errorsb.append("债券利息收入值不能为空|");
				haveerror = true;
				}
			}

			// 其他利息收入
			if (StringUtils.isNotBlank(data.getQtlxsr())) {
				if (checknumList.contains("JS0507")) {
				Matcher match = pattern.matcher(data.getQtlxsr());
				if (match.matches()) {
					
				} else {
					errorsb.append("其他利息收入小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2524")) {
				if (new BigDecimal(data.getQtlxsr()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("其他利息收入值应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1193")) {
				errorsb.append("其他利息收入值不能为空|");
				haveerror = true;
				}
			}

			// if(StringUtils.isNotBlank(data.getSjrq())) {
			// if(data.getSjrq().length() != 10) {
			// errorsb.append("数据日期长度不为10|");
			// haveerror = true;
			// }else {
			// Matcher mat = pat.matcher(data.getSjrq());
			// if(mat.find()) {
			//
			// errorsb.append("数据日期含有特殊字符|");
			// haveerror = true;
			// }else {
			// SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			// format.setLenient(false);
			// try {
			// Date date = format.parse(data.getSjrq());
			// String s1 = "1900-01-01";
			// String s2 = "2100-12-31";
			// Date d1 = format.parse(s1);
			// Date d2 = format.parse(s2);
			// if(date.after(d1) && date.before(d2)) {
			//
			// }else {
			//
			// errorsb.append("数据日期范围错误|");
			// haveerror = true;
			// }
			// } catch (ParseException e) {
			//
			// errorsb.append("数据日期格式错误|");
			// haveerror = true;
			// e.printStackTrace();
			// }
			// }
			// }
			// }

			// 利息支出
			if (StringUtils.isNotBlank(data.getLxzc())) {
				if (checknumList.contains("JS0508")) {
				Matcher match = pattern.matcher(data.getLxzc());
				if (match.matches()) {
					
				} else {

					errorsb.append("利息支出小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2525")) {
				if (new BigDecimal(data.getLxzc()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("利息支出值应大于等于0|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2526")) {
				try {
					BigDecimal b1;
					BigDecimal b2;
					BigDecimal b3;
					BigDecimal b4;
					BigDecimal b5;
					if (StringUtils.isNotBlank(data.getLxzc()) && data.getLxzc().length() > 0) {
						b1 = new BigDecimal(data.getLxzc());
					} else {
						b1 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getJrjgwllxzc()) && data.getJrjgwllxzc().length() > 0) {
						b2 = new BigDecimal(data.getJrjgwllxzc());
					} else {
						b2 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getGxcklxzc()) && data.getGxcklxzc().length() > 0) {
						b3 = new BigDecimal(data.getGxcklxzc());
					} else {
						b3 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getZqlxzc()) && data.getZqlxzc().length() > 0) {
						b4 = new BigDecimal(data.getZqlxzc());
					} else {
						b4 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getQtlxzc()) && data.getQtlxzc().length() > 0) {
						b5 = new BigDecimal(data.getQtlxzc());
					} else {
						b5 = new BigDecimal(0);
					}

					if (b1.compareTo(b2.add(b3).add(b4).add(b5)) == 0) {

					} else {
						errorsb.append("利息支出值应等于金融机构往来利息支出、各项存款利息支出、债券利息支出与其他利息支出之和|");
						haveerror = true;
					}
				} catch (Exception e) {

					errorsb.append("利息支出值求和不正确|");
					haveerror = true;
					e.printStackTrace();
				}
				}
			} else {
				if (checknumList.contains("JS1194")) {
				errorsb.append("利息支出值不能为空|");
				haveerror = true;
				}
			}

			// 金融机构往来利息支出
			if (StringUtils.isNotBlank(data.getJrjgwllxzc())) {
				if (checknumList.contains("JS0509")) {
				Matcher match = pattern.matcher(data.getJrjgwllxzc());
				if (match.matches()) {
					
				} else {

					errorsb.append("金融机构往来利息支出小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2527")) {
				if (new BigDecimal(data.getJrjgwllxzc()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("金融机构往来利息支出值应大于等于0|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2528")) {
				BigDecimal b1;
				BigDecimal b2;
				if (StringUtils.isNotBlank(data.getJrjgwllxzc()) && data.getJrjgwllxzc().length() > 0) {
					b1 = new BigDecimal(data.getJrjgwllxzc());
				} else {
					b1 = new BigDecimal(0);
				}
				if (StringUtils.isNotBlank(data.getXtnwllxzc()) && data.getXtnwllxzc().length() > 0) {
					b2 = new BigDecimal(data.getXtnwllxzc());
				} else {
					b2 = new BigDecimal(0);
				}

				if (b1.compareTo(b2) != -1) {

				} else {

					errorsb.append("金融机构往来利息支出值应大于等于其中：系统内往来利息支出|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1195")) {
				errorsb.append("金融机构往来利息支出值不能为空|");
				haveerror = true;
				}
			}

			// 其中：系统内往来利息支出
			if (StringUtils.isNotBlank(data.getXtnwllxzc())) {
				if (checknumList.contains("JS0510")) {
				Matcher match = pattern.matcher(data.getXtnwllxzc());
				if (match.matches()) {
					
				} else {
					errorsb.append("其中：系统内往来利息支出小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2529")) {
				if (new BigDecimal(data.getXtnwllxzc()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("其中：系统内往来利息支出值应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1196")) {
				errorsb.append("其中：系统内往来利息支出值不为空|");
				haveerror = true;
				}
			}

			// 各项存款利息支出
			if (StringUtils.isNotBlank(data.getGxcklxzc())) {
				if (checknumList.contains("JS0511")) {
				Matcher match = pattern.matcher(data.getGxcklxzc());
				if (match.matches()) {
					
				} else {

					errorsb.append("各项存款利息支出小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2530")) {
				if (new BigDecimal(data.getGxcklxzc()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("各项存款利息支出值应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1197")) {
				errorsb.append("各项存款利息支出值不能为空");
				haveerror = true;
				}
			}

			// 债券利息支出
			if (StringUtils.isNotBlank(data.getZqlxzc())) {
				if (checknumList.contains("JS0512")) {
				Matcher match = pattern.matcher(data.getZqlxzc());
				if (match.matches()) {
					
				} else {
					errorsb.append("债券利息支出小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2531")) {
				if (new BigDecimal(data.getZqlxzc()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("债券利息支出应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1198")) {
				errorsb.append("债券利息支出不能为空");
				haveerror = true;
				}
			}

			// 其他利息支出
			if (StringUtils.isNotBlank(data.getQtlxzc())) {
				if (checknumList.contains("JS0513")) {
				Matcher match = pattern.matcher(data.getQtlxzc());
				if (match.matches()) {
					
				} else {
					errorsb.append("其他利息支出小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2532")) {
				if (new BigDecimal(data.getQtlxzc()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("其他利息支出值应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1199")) {
				errorsb.append("其他利息支出值不能为空|");
				haveerror = true;
				}
			}

			// 手续费及佣金净收入
			if (StringUtils.isNotBlank(data.getSxfjyjjsr())) {
				if (checknumList.contains("JS0514")) {
				Matcher match = pattern.matcher(data.getSxfjyjjsr());
				if (match.matches()) {
					
				} else {

					errorsb.append("手续费及佣金净收入小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2533")) {
				try {
					BigDecimal b1;
					BigDecimal b2;
					BigDecimal b3;
					if (StringUtils.isNotBlank(data.getSxfjyjjsr()) && data.getSxfjyjjsr().length() > 0) {
						b1 = new BigDecimal(data.getSxfjyjjsr());
					} else {
						b1 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getSxfjyjsr()) && data.getSxfjyjsr().length() > 0) {
						b2 = new BigDecimal(data.getSxfjyjsr());
					} else {
						b2 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getJxfjyjzc()) && data.getJxfjyjzc().length() > 0) {
						b3 = new BigDecimal(data.getJxfjyjzc());
					} else {
						b3 = new BigDecimal(0);
					}

					if (b1.compareTo(b2.subtract(b3)) == 0) {

					} else {

						errorsb.append("手续费及佣金净收入值应等于手续费及佣金收入减手续费及佣金支出|");
						haveerror = true;
					}
				} catch (Exception e) {

					errorsb.append("手续费及佣金净收入值求差不正确|");
					haveerror = true;
					e.printStackTrace();
				}
				}
			} else {
				if (checknumList.contains("JS1200")) {
				errorsb.append("手续费及佣金净收入值不能为空|");
				haveerror = true;
				}
			}

			// 手续费及佣金收入
			if (StringUtils.isNotBlank(data.getSxfjyjsr())) {
				if (checknumList.contains("JS0515")) {
				Matcher match = pattern.matcher(data.getSxfjyjsr());
				if (match.matches()) {
					
				} else {
					errorsb.append("手续费及佣金收入小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2534")) {
				if (new BigDecimal(data.getSxfjyjsr()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("手续费及佣金收入应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1201")) {
				errorsb.append("手续费及佣金收入值不能为空");
				haveerror = true;
				}
			}

			// 手续费及佣金支出
			if (StringUtils.isNotBlank(data.getJxfjyjzc())) {
				if (checknumList.contains("JS0516")) {
				Matcher match = pattern.matcher(data.getJxfjyjzc());
				if (match.matches()) {
					
				} else {
					errorsb.append("手续费及佣金支出小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2535")) {
				if (new BigDecimal(data.getJxfjyjzc()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("手续费及佣金支出应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1202")) {
				errorsb.append("手续费及佣金支出值不能为空");
				haveerror = true;
				}
			}

			// 租赁收益
			if (StringUtils.isNotBlank(data.getZlsy())) {
				if (checknumList.contains("JS0517")) {
				Matcher match = pattern.matcher(data.getZlsy());
				if (match.matches()) {
					// if(new BigDecimal(data.getZlsy()).compareTo(BigDecimal.ZERO)<0){
					// errorsb.append("租赁收益值应大于等于0|");
					// haveerror = true;
					// }
				} else {
					errorsb.append("租赁收益小数位必须为2位|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1203")) {
				errorsb.append("租赁收益值不能为空|");
				haveerror = true;
				}
			}

			// 投资收益
			if (StringUtils.isNotBlank(data.getTzsy())) {
				if (checknumList.contains("JS0518")) {
				Matcher match = pattern.matcher(data.getTzsy());
				if (match.matches()) {
					// if(new BigDecimal(data.getTzsy()).compareTo(BigDecimal.ZERO)<0){
					// errorsb.append("投资收益应大于等于0|");
					// haveerror = true;
					// }
					
				} else {
					errorsb.append("投资收益小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2538")) {
				try {
					BigDecimal b1;
					BigDecimal b2;
					BigDecimal b3;
					BigDecimal b4;
					if (StringUtils.isNotBlank(data.getTzsy()) && data.getTzsy().length() > 0) {
						b1 = new BigDecimal(data.getTzsy());
					} else {
						b1 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getZqtzsy()) && data.getZqtzsy().length() > 0) {
						b2 = new BigDecimal(data.getZqtzsy());
					} else {
						b2 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getGqtzsy()) && data.getGqtzsy().length() > 0) {
						b3 = new BigDecimal(data.getGqtzsy());
					} else {
						b3 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getQttzsy()) && data.getQttzsy().length() > 0) {
						b4 = new BigDecimal(data.getQttzsy());
					} else {
						b4 = new BigDecimal(0);
					}

					if (b1.compareTo(b2.add(b3).add(b4)) == 0) {

					} else {

						errorsb.append("投资收益应等于债券投资收益、股权投资收益及其他投资收益之和|");
						haveerror = true;
					}
				} catch (Exception e) {
					errorsb.append("投资收益值求和不正确|");
					haveerror = true;
					e.printStackTrace();
				}
				}
			} else {
				if (checknumList.contains("JS1204")) {
				errorsb.append("投资收益值不能为空|");
				haveerror = true;
				}
			}

			// #债券投资收益
			if (StringUtils.isNotBlank(data.getZqtzsy())) {
				if (checknumList.contains("JS0519")) {
				Matcher match = pattern.matcher(data.getZqtzsy());
				if (match.matches()) {
					// if(new BigDecimal(data.getZqtzsy()).compareTo(BigDecimal.ZERO)<0){
					// errorsb.append("债券投资收益应大于等于0|");
					// haveerror = true;
					// }
				} else {
					errorsb.append("债券投资收益小数位必须为2位|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1205")) {
				errorsb.append("债券投资收益值不能为空");
				haveerror = true;
				}
			}

			// 股权投资收益
			if (StringUtils.isNotBlank(data.getGqtzsy())) {
				if (checknumList.contains("JS0520")) {
				Matcher match = pattern.matcher(data.getGqtzsy());
				if (match.matches()) {
				} else {

					errorsb.append("股权投资收益小数位必须为2位|");
					haveerror = true;
				}
				}
				// if(new BigDecimal(data.getGqtzsy()).compareTo(BigDecimal.ZERO)<0){
				// errorsb.append("股权投资收益值应大于等于0|");
				// haveerror = true;
				// }
			} else {
				if (checknumList.contains("JS1206")) {
				errorsb.append("股权投资收益值不能为空");
				haveerror = true;
				}
			}

			// 其他投资收益
			if (StringUtils.isNotBlank(data.getQttzsy())) {
				if (checknumList.contains("JS0521")) {
				Matcher match = pattern.matcher(data.getQttzsy());
				if (match.matches()) {
					// if(new BigDecimal(data.getQttzsy()).compareTo(BigDecimal.ZERO)<0){
					// errorsb.append("其他投资收益值应大于等于0|");
					// haveerror = true;
					// }
				} else {
					errorsb.append("其他投资收益小数位必须为2位|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1207")) {
				errorsb.append("其他投资收益值不能为空|");
				haveerror = true;
				}
			}

			// 公允价值变动收益
			if (StringUtils.isNotBlank(data.getGyjzbdsy())) {
				if (checknumList.contains("JS0522")) {
				Matcher match = pattern.matcher(data.getGyjzbdsy());
				if (match.matches()) {

				} else {
					errorsb.append("公允价值变动收益小数位必须为2位|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1208")) {
				errorsb.append("公允价值变动收益值不能为空|");
				haveerror = true;
				}
			}

			// 汇兑净收益
			if (StringUtils.isNotBlank(data.getHdjsy())) {
				if (checknumList.contains("JS0523")) {
				Matcher match = pattern.matcher(data.getHdjsy());
				if (match.matches()) {

				} else {
					errorsb.append("汇兑净收益小数位必须为2位|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1209")) {
				errorsb.append("汇兑净收益值不能为空|");
				haveerror = true;
				}
			}

			// 资产处置收益
			if (StringUtils.isNotBlank(data.getZcczsy())) {
				if (checknumList.contains("JS0524")) {
				Matcher match = pattern.matcher(data.getZcczsy());
				if (match.matches()) {

				} else {
					errorsb.append("资产处置收益小数位必须为2位|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1210")) {
				errorsb.append("资产处置收益值不能为空|");
				haveerror = true;
				}

			}

			// 其他业务收入
			if (StringUtils.isNotBlank(data.getQtywsr())) {
				if (checknumList.contains("JS0525")) {
				Matcher match = pattern.matcher(data.getQtywsr());
				if (match.matches()) {
					
				} else {
					errorsb.append("其他业务收入小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2542")) {
				if (new BigDecimal(data.getQtywsr()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("其他业务收入值大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1211")) {
				errorsb.append("其他业务收入值不能为空|");
				haveerror = true;
				}

			}

			// 营业支出
			if (StringUtils.isNotBlank(data.getYyzc())) {
				if (checknumList.contains("JS0526")) {
				Matcher match = pattern.matcher(data.getYyzc());
				if (match.matches()) {
					
				} else {
					errorsb.append("营业支出小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2544")) {
				try {
					BigDecimal b1;
					BigDecimal b2;
					BigDecimal b3;
					BigDecimal b4;
					BigDecimal b5;
					if (StringUtils.isNotBlank(data.getYyzc()) && data.getYyzc().length() > 0) {
						b1 = new BigDecimal(data.getYyzc());
					} else {
						b1 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getYwjglf()) && data.getYwjglf().length() > 0) {
						b2 = new BigDecimal(data.getYwjglf());
					} else {
						b2 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getSjjfj()) && data.getSjjfj().length() > 0) {
						b3 = new BigDecimal(data.getSjjfj());
					} else {
						b3 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getZcjzss()) && data.getZcjzss().length() > 0) {
						b4 = new BigDecimal(data.getZcjzss());
					} else {
						b4 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getQtywzc()) && data.getQtywzc().length() > 0) {
						b5 = new BigDecimal(data.getQtywzc());
					} else {
						b5 = new BigDecimal(0);
					}

					if (b1.compareTo(b2.add(b3).add(b4).add(b5)) == 0) {

					} else {
						errorsb.append("营业支出值应等于业务及管理费、税金及附加、资产减值损失及其他业务支出之和|");
						haveerror = true;
					}
				} catch (Exception e) {
					errorsb.append("营业支出值求和不正确|");
					haveerror = true;
					e.printStackTrace();
				}
				}
				if (checknumList.contains("JS2543")) {
				if (new BigDecimal(data.getYyzc()).compareTo(BigDecimal.ZERO) <= 0) {
					errorsb.append("营业支出值应大于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1212")) {
				errorsb.append("营业支出值不能为空|");
				haveerror = true;
				}

			}

			// 业务及管理费
			if (StringUtils.isNotBlank(data.getYwjglf())) {
				if (checknumList.contains("JS0527")) {
				Matcher match = pattern.matcher(data.getYwjglf());
				if (match.matches()) {
					
				} else {

					errorsb.append("业务及管理费小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2545")) {
				if (new BigDecimal(data.getYwjglf()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("业务及管理费值大于等于0|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2546")) {
				try {
					BigDecimal b1;
					BigDecimal b2;
					BigDecimal b3;
					BigDecimal b4;
					if (StringUtils.isNotBlank(data.getYwjglf()) && data.getYwjglf().length() > 0) {
						b1 = new BigDecimal(data.getYwjglf());
					} else {
						b1 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getZggz()) && data.getZggz().length() > 0) {
						b2 = new BigDecimal(data.getZggz());
					} else {
						b2 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getFlf()) && data.getFlf().length() > 0) {
						b3 = new BigDecimal(data.getFlf());
					} else {
						b3 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getZfgjjhzfbt()) && data.getZfgjjhzfbt().length() > 0) {
						b4 = new BigDecimal(data.getZfgjjhzfbt());
					} else {
						b4 = new BigDecimal(0);
					}

					if (b1.compareTo(b2.add(b3).add(b4)) >= 0) {

					} else {
						errorsb.append("业务及管理费值应大于等于其中：职工工资、福利费及住房公积金和住房补贴之和|");
						haveerror = true;
					}
				} catch (Exception e) {

					errorsb.append("业务及管理费值求和不正确|");
					haveerror = true;
					e.printStackTrace();
				}
				}
			} else {
				if (checknumList.contains("JS1213")) {
				errorsb.append("业务及管理费值不能为空|");
				haveerror = true;
				}

			}

			// 其中:职工工资
			if (StringUtils.isNotBlank(data.getZggz())) {
				if (checknumList.contains("JS0528")) {
				Matcher match = pattern.matcher(data.getZggz());
				if (match.matches()) {
					
				} else {

					errorsb.append("其中:职工工资小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2547")) {
				if (new BigDecimal(data.getZggz()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("其中:职工工资值应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1214")) {
				errorsb.append("其中:职工工资值不能为空|");
				haveerror = true;
				}

			}

			// 福利费
			if (StringUtils.isNotBlank(data.getFlf())) {
				if (checknumList.contains("JS0529")) {
				Matcher match = pattern.matcher(data.getFlf());
				if (match.matches()) {
					
				} else {

					errorsb.append("福利费小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2548")) {
				if (new BigDecimal(data.getFlf()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("福利费值大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1215")) {
				errorsb.append("福利费值不能为空|");
				haveerror = true;
				}

			}

			// 住房公积金和住房补贴
			if (StringUtils.isNotBlank(data.getZfgjjhzfbt())) {
				if (checknumList.contains("JS0530")) {
				Matcher match = pattern.matcher(data.getZfgjjhzfbt());
				if (match.matches()) {
					
				} else {

					errorsb.append("住房公积金和住房补贴小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2549")) {
				if (new BigDecimal(data.getZfgjjhzfbt()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("住房公积金和住房补贴应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1216")) {
				errorsb.append("住房公积金和住房补贴值不能为空|");
				haveerror = true;
				}
			}

			// 税金及附加
			if (StringUtils.isNotBlank(data.getSjjfj())) {
				if (checknumList.contains("JS0531")) {
				Matcher match = pattern.matcher(data.getSjjfj());
				if (match.matches()) {
					
				} else {

					errorsb.append("税金及附加小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2550")) {
				if (new BigDecimal(data.getSjjfj()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("税金及附加值大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1217")) {
				errorsb.append("税金及附加值不能为空|");
				haveerror = true;
				}
			}

			// 资产减值损失
			if (StringUtils.isNotBlank(data.getZcjzss())) {
				if (checknumList.contains("JS0532")) {
				Matcher match = pattern.matcher(data.getZcjzss());
				if (match.matches()) {
					// if(new BigDecimal(data.getZcjzss()).compareTo(BigDecimal.ZERO)<0){
					// errorsb.append("资产减值损失值大于等于0|");
					// haveerror = true;
					// }
				} else {

					errorsb.append("资产减值损失小数位必须为2位|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1218")) {
				errorsb.append("资产减值损失值不能为空|");
				haveerror = true;
				}
			}

			// 其他业务支出
			if (StringUtils.isNotBlank(data.getQtywzc())) {
				if (checknumList.contains("JS0533")) {
				Matcher match = pattern.matcher(data.getQtywzc());
				if (match.matches()) {
					
				} else {
					errorsb.append("其他业务支出小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2552")) {
				if (new BigDecimal(data.getQtywzc()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("其他业务支出值应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1219")) {
				errorsb.append("其他业务支出值不能为空|");
				haveerror = true;
				}
			}

			// 营业利润
			if (StringUtils.isNotBlank(data.getYylr())) {
				if (checknumList.contains("JS0534")) {
				Matcher match = pattern.matcher(data.getYylr());
				if (match.matches()) {
					
				} else {

					errorsb.append("营业利润小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2553")) {
				try {
					BigDecimal b1;
					BigDecimal b2;
					BigDecimal b3;
					if (StringUtils.isNotBlank(data.getYylr()) && data.getYylr().length() > 0) {
						b1 = new BigDecimal(data.getYylr());
					} else {
						b1 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getYysr()) && data.getYysr().length() > 0) {
						b2 = new BigDecimal(data.getYysr());
					} else {
						b2 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getYyzc()) && data.getYyzc().length() > 0) {
						b3 = new BigDecimal(data.getYyzc());
					} else {
						b3 = new BigDecimal(0);
					}

					if (b1.compareTo(b2.subtract(b3)) == 0) {

					} else {

						errorsb.append("营业利润值应等于营业收入-营业支出|");
						haveerror = true;
					}
				} catch (Exception e) {

					errorsb.append("营业利润值求差不正确|");
					haveerror = true;
					e.printStackTrace();
				}
				}
			} else {
				if (checknumList.contains("JS1220")) {
				errorsb.append("营业利润值不能为空|");
				haveerror = true;
				}
			}

			// 营业外收入（加）
			if (StringUtils.isNotBlank(data.getYywsr())) {
				if (checknumList.contains("JS0535")) {
				Matcher match = pattern.matcher(data.getYywsr());
				if (match.matches()) {
					
				} else {
					errorsb.append("营业外收入（加）小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2554")) {
				if (new BigDecimal(data.getYywsr()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("营业外收入（加）应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1221")) {
				errorsb.append("营业外收入（减）应不能为空|");
				haveerror = true;
				}
			}

			// 营业外支出（减）
			if (StringUtils.isNotBlank(data.getYywzc())) {
				if (checknumList.contains("JS0536")) {
				Matcher match = pattern.matcher(data.getYywzc());
				if (match.matches()) {
					
				} else {
					errorsb.append("营业外支出（减）小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2555")) {
				if (new BigDecimal(data.getYywzc()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("营业外支出（减）应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1222")) {
				errorsb.append("营业外支出（减）应不能为空|");
				haveerror = true;
				}
			}

			// 利润总额
			if (StringUtils.isNotBlank(data.getLrze())) {
				if (checknumList.contains("JS0537")) {
				Matcher match = pattern.matcher(data.getLrze());
				if (match.matches()) {
					
				} else {

					errorsb.append("利润总额小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2556")) {
				try {
					BigDecimal b1;
					BigDecimal b2;
					BigDecimal b3;
					BigDecimal b4;
					if (StringUtils.isNotBlank(data.getLrze()) && data.getLrze().length() > 0) {
						b1 = new BigDecimal(data.getLrze());
					} else {
						b1 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getYylr()) && data.getYylr().length() > 0) {
						b2 = new BigDecimal(data.getYylr());
					} else {
						b2 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getYywsr()) && data.getYywsr().length() > 0) {
						b3 = new BigDecimal(data.getYywsr());
					} else {
						b3 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getYywzc()) && data.getYywzc().length() > 0) {
						b4 = new BigDecimal(data.getYywzc());
					} else {
						b4 = new BigDecimal(0);
					}

					if (b1.compareTo((b2.add(b3)).subtract(b4)) == 0) {

					} else {
						errorsb.append("利润总额值应等于营业利润+营业外收入（加）-营业外支出（减）|");
						haveerror = true;
					}
				} catch (Exception e) {

					errorsb.append("利润总额值求和不正确|");
					haveerror = true;
					e.printStackTrace();
				}
				}
			} else {
				if (checknumList.contains("JS1223")) {
				errorsb.append("利润总额不能为空|");
				haveerror = true;
				}
			}

			// 所得税（减）
			if (StringUtils.isNotBlank(data.getSds())) {
				if (checknumList.contains("JS0538")) {
				Matcher match = pattern.matcher(data.getSds());
				if (match.matches()) {
					
				} else {
					errorsb.append("所得税（减）小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2557")) {
				if (new BigDecimal(data.getSds()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("所得税（减）应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1224")) {
				errorsb.append("所得税（减）不能为空|");
				haveerror = true;
				}
			}

			// 净利润
			if (StringUtils.isNotBlank(data.getJlr())) {
				if (checknumList.contains("JS0539")) {
				Matcher match = pattern.matcher(data.getJlr());
				if (match.matches()) {
					
				} else {
					errorsb.append("净利润小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2205")) {
				try {
					BigDecimal b1;
					BigDecimal b2;
					BigDecimal b3;
					if (StringUtils.isNotBlank(data.getJlr()) && data.getJlr().length() > 0) {
						b1 = new BigDecimal(data.getJlr());
					} else {
						b1 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getLrze()) && data.getLrze().length() > 0) {
						b2 = new BigDecimal(data.getLrze());
					} else {
						b2 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getSds()) && data.getSds().length() > 0) {
						b3 = new BigDecimal(data.getSds());
					} else {
						b3 = new BigDecimal(0);
					}

					if (b1.compareTo(b2.subtract(b3)) == 0) {

					} else {

						errorsb.append("净利润值应等于利润总额-所得税（减）|");
						haveerror = true;
					}
				} catch (Exception e) {
					errorsb.append("净利润值求差不正确|");
					haveerror = true;
					e.printStackTrace();
				}
				}
			} else {
				if (checknumList.contains("JS1225")) {
				errorsb.append("净利润值不能为空|");
				haveerror = true;
				}
			}

			// 年度损益调整（加）
			if (StringUtils.isNotBlank(data.getNdsytz())) {
				if (checknumList.contains("JS0540")) {
				Matcher match = pattern.matcher(data.getNdsytz());
				if (match.matches()) {

				} else {
					errorsb.append("年度损益调整（加）小数位必须为2位|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1226")) {
				errorsb.append("年度损益调整（加）值不能为空|");
				haveerror = true;
				}
			}

			// 留存利润
			if (StringUtils.isNotBlank(data.getLclr())) {
				if (checknumList.contains("JS0541")) {
				Matcher match = pattern.matcher(data.getLclr());
				if (match.matches()) {
					// if(new BigDecimal(data.getLclr()).compareTo(BigDecimal.ZERO)<0){
					// errorsb.append("留存利润值应大于等于0|");
					// haveerror = true;
					// }
				} else {
					errorsb.append("留存利润小数位必须为2位|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1227")) {
				errorsb.append("留存利润值不能为空|");
				haveerror = true;
				}
			}

			// 未分配利润
			if (StringUtils.isNotBlank(data.getWfplr())) {
				if (checknumList.contains("JS0542")) {
				Matcher match = pattern.matcher(data.getWfplr());
				if (match.matches()) {
					
				} else {

					errorsb.append("未分配利润小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2208")) {
				try {
					BigDecimal b1;
					BigDecimal b2;
					BigDecimal b3;
					BigDecimal b4;
					if (StringUtils.isNotBlank(data.getWfplr()) && data.getWfplr().length() > 0) {
						b1 = new BigDecimal(data.getWfplr());
					} else {
						b1 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getJlr()) && data.getJlr().length() > 0) {
						b2 = new BigDecimal(data.getJlr());
					} else {
						b2 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getNdsytz()) && data.getNdsytz().length() > 0) {
						b3 = new BigDecimal(data.getNdsytz());
					} else {
						b3 = new BigDecimal(0);
					}
					if (StringUtils.isNotBlank(data.getLclr()) && data.getLclr().length() > 0) {
						b4 = new BigDecimal(data.getLclr());
					} else {
						b4 = new BigDecimal(0);
					}

					if (b1.compareTo(b2.add(b3).add(b4)) == 0) {

					} else {
						errorsb.append("未分配利润值应等于净利润+年度损益调整（加）+留存利润|");
						haveerror = true;
					}
				} catch (Exception e) {

					errorsb.append("未分配利润值求和不正确|");
					haveerror = true;
					e.printStackTrace();
				}
				}
				// if(new BigDecimal(data.getWfplr()).compareTo(BigDecimal.ZERO)<0){
				// errorsb.append("未分配利润值应大于等于0|");
				// haveerror = true;
				// }

			} else {
				if (checknumList.contains("JS1228")) {
				errorsb.append("未分配利润值不能为空|");
				haveerror = true;
				}
			}

			// 应纳增值税
			if (StringUtils.isNotBlank(data.getYnzzs())) {
				if (checknumList.contains("JS0543")) {
				Matcher match = pattern.matcher(data.getYnzzs());
				if (match.matches()) {
					
				} else {
					errorsb.append("应纳增值税小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2209")) {
				if (new BigDecimal(data.getYnzzs()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("应纳增值税应大于等于0|");
					haveerror = true;
				}
				}
			} else {
				if (checknumList.contains("JS1229")) {
				errorsb.append("应纳增值税值不能为空|");
				haveerror = true;
				}
			}

			// 核心一级资本净额
			if (StringUtils.isNotBlank(data.getHxyjzbje())) {
				if (checknumList.contains("JS0544")) {
				Matcher match = pattern.matcher(data.getHxyjzbje());
				if (match.matches()) {
					
				} else {

					errorsb.append("当核心一级资本净额不为空时，核心一级资本净额小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2210")) {
				if (new BigDecimal(data.getHxyjzbje()).compareTo(BigDecimal.ZERO) <= 0) {
					errorsb.append("当核心一级资本净额不为空时，核心一级资本净额应大于0|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2211")) {
				String sql = "select count(*) num from xjrjgfrprofit a inner join xjrjgfrassets b on a.sjrq = b.sjrq where a.hxyjzbje is not null and a.hxyjzbje > b.syzqyhj";
				rs = jdbc.Query(sql);
				if (rs.next()) {
					if (!"0".equals(rs.getString("num"))) {
						errorsb.append("当核心一级资本净额不为空时，核心一级资本净额应小于等于金融机构（法人）基础信息-资产负债及风险统计表的所有者权益合计|");
						haveerror = true;
					}
				}
				}
			} else {
				if (checknumList.contains("JS1043")) {
				String sql = "select count(*) num from xjrjgfrprofit a inner join xjrjgfrbaseinfo b on a.sjrq = b.sjrq where b.jglb like 'C%' and a.hxyjzbje is null";
				rs = jdbc.Query(sql);
				if (rs.next()) {
					if (!"0".equals(rs.getString("num"))) {
						errorsb.append("当机构类别为银行业金融机构时，核心一级资本净额不能为空|");
						haveerror = true;
					}
				}
				}
			}

			// 一级资本净额
			if (StringUtils.isNotBlank(data.getYjzbje())) {
				if (checknumList.contains("JS0545")) {
				Matcher match = pattern.matcher(data.getYjzbje());
				if (match.matches()) {
					
				} else {
					errorsb.append("当一级资本净额不为空时，一级资本净额小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2212")) {
				if (new BigDecimal(data.getYjzbje()).compareTo(BigDecimal.ZERO) <= 0) {
					errorsb.append("当一级资本净额不为空时，一级资本净额应大于0|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2213")) {
				if (StringUtils.isNotBlank(data.getHxyjzbje())) {
					if (data.getYjzbje().compareTo(data.getHxyjzbje()) < 0) {
						errorsb.append("当一级资本净额和核心一级资本净额不为空时，一级资本净额应大于等于核心一级资本净额|");
						haveerror = true;

					}
				}
				}
			} else {
				if (checknumList.contains("JS1052")) {
				String sql = "select count(*) num from xjrjgfrprofit a inner join xjrjgfrbaseinfo b on a.sjrq = b.sjrq where b.jglb like 'C%' and a.yjzbje is null";
				rs = jdbc.Query(sql);
				if (rs.next()) {
					if (!"0".equals(rs.getString("num"))) {
						errorsb.append("当机构类别为银行业金融机构时，一级资本净额不能为空|");
						haveerror = true;
					}
				}
				}
			}

			// #资本净额
			if (StringUtils.isNotBlank(data.getZbje())) {
				if (checknumList.contains("JS0546")) {
				Matcher match = pattern.matcher(data.getZbje());
				if (match.matches()) {
					
				} else {
					errorsb.append("当资本净额不为空时，资本净额小数位必须为2位|");
					haveerror = true;

				}
				}
				if (checknumList.contains("JS2214")) {
				if (new BigDecimal(data.getZbje()).compareTo(BigDecimal.ZERO) <= 0) {
					errorsb.append("当资本净额不为空时，资本净额应大于0|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2215")) {
				if (StringUtils.isNotBlank(data.getYjzbje())) {
					if (data.getZbje().compareTo(data.getYjzbje()) < 0) {
						errorsb.append("当资本净额和一级资本净额不为空时，资本净额应大于等于一级资本净额|");
						haveerror = true;
					}
				}
				}
			} else {
				if (checknumList.contains("JS1068")) {
				String sql = "select count(*) num from xjrjgfrprofit a inner join xjrjgfrbaseinfo b on a.sjrq = b.sjrq where b.jglb like 'C%' and a.zbje is null";
				rs = jdbc.Query(sql);
				if (rs.next()) {
					if (!"0".equals(rs.getString("num"))) {
						errorsb.append("当机构类别为银行业金融机构时，资本净额不能为空|");
						haveerror = true;
					}
				}
				}
			}

			// 应用资本底线及校准后的风险加权资产合计
			if (StringUtils.isNotBlank(data.getYgzbjfxjqzchj())) {
				if (checknumList.contains("JS0547")) {
				Matcher match = pattern.matcher(data.getYgzbjfxjqzchj());
				if (match.matches()) {
					
				} else {
					errorsb.append("当应用资本底线及校准后的风险加权资产合计不为空时，应用资本底线及校准后的风险加权资产合计小数位必须为2位|");
					haveerror = true;
				}
				}
				if (checknumList.contains("JS2216")) {
				if (new BigDecimal(data.getYgzbjfxjqzchj()).compareTo(BigDecimal.ZERO) < 0) {
					errorsb.append("当应用资本底线及校准后的风险加权资产合计不为空时，应用资本底线及校准后的风险加权资产合计应大于等于0|");
					haveerror = true;
				}
				}
			}

			// 数据日期
			haveerror = CheckUtil.nullAndDate(data.getSjrq(), errorsb, haveerror, "数据日期", false);

			if (haveerror) {
				// result.append("该数据的id为:"+data.getId()+";");
				result.append("该数据的id为:" + data.getId() + "->\n" + errorsb + "\n");
				cuowuid.add(data.getId());
			} else {
				zhengqueid.add(data.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			jdbc.close();
		}
	}

	/**
	 * 校验是否存在特殊字符
	 * 
	 * @param str
	 * @return true:存在特殊字符;false:不存在特殊字符
	 */
	public static boolean checkStr(String str) {
		Matcher m = strPattern.matcher(str);
		return m.find();
	}

	public static boolean checkStr01(String str) {
		Matcher m = strPattern01.matcher(str);
		return m.find();
	}

	/**
	 * 是否存逗号
	 * @param str
	 * @return
	 */
	public static boolean comma(String str) {
		Matcher m = comma.matcher(str);
		return m.find();
	}
}
