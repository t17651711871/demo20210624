package com.geping.etl.UNITLOAN.util;

import com.geping.etl.UNITLOAN.entity.report.Xdkdbwx;
import com.geping.etl.UNITLOAN.entity.report.Xjrjgfz;
import com.geping.etl.UNITLOAN.util.check.CheckUtil;
import org.apache.commons.lang.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName: CheckxdkdbwxUtils2
 * @Description: TOOD
 * @Author: 陈根
 * @Date: 2020/8/31 9:50
 * @Version
 **/
public class CheckxdkdbwxUtils {
    @PersistenceContext
    private EntityManager entityManager;

    private static Map<String,String> ziDuanJiHe;
    private static class SingletonInstance {
        private static final Map<String,String> ziDuanJiHe = new HashMap<String,String>();
    }
    public static Map<String,String> getInstance() {
        return CheckxdkdbwxUtils.SingletonInstance.ziDuanJiHe;
    }

    private static final Pattern strPattern = Pattern.compile("[？?！!^]");
    private static final Pattern strPattern01 = Pattern.compile("[？?！!^]");

    //金融机构（分支机构）基础信息表.金融机构代码
    public static List<String> fzList;

    //金融机构（法人）基础信息表.金融机构代码
    public static List<String> frList;

    public static Map<String,String> getDWDKDBWXX(){
        ziDuanJiHe = getInstance();
        ziDuanJiHe.put("gteecontractcode", "担保合同编码");
        ziDuanJiHe.put("loancontractcode", "贷款合同编码");
        ziDuanJiHe.put("gteegoodscode", "担保物编码");
        ziDuanJiHe.put("gteegoodscategory", "担保物类别");
        ziDuanJiHe.put("warrantcode", "权证编号");
        ziDuanJiHe.put("isfirst", "是否第一顺位");
        ziDuanJiHe.put("assessmode", "评估方式");
        ziDuanJiHe.put("assessmethod", "评估方法");
        ziDuanJiHe.put("assessvalue", "评估价值");
        ziDuanJiHe.put("assessdate", "评估基准日");
        ziDuanJiHe.put("gteegoodsamt", "担保物账面价值");
        ziDuanJiHe.put("firstrightamt", "优先受偿权数额");
        ziDuanJiHe.put("gteegoodsstataus", "担保物状态");
        ziDuanJiHe.put("mortgagepgerate", "抵质押率");
        return ziDuanJiHe;
    }

    //private static String regEx = "[ _`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]|\n|\r|\t";
    private static String regEx = "[!$%^*?！？ ]|\n|\r|\t";
    private static Pattern pat = Pattern.compile(regEx);
    //private static Pattern pattern = Pattern.compile("^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){2})?$"); // 判断小数点后2位的数字的正则表达式
    private static Pattern pattern = Pattern.compile("^([\\+\\-]?[1-9]{1}[0-9]{0,16}\\.[0-9]{2})|([\\+\\-]?[0]{1}\\.[0-9]{2})?$");//判断20位的数值且包含2位小数的数字的正则表达式
    /**
     * 校验指定的数据
     * @param xdkdbwxList 需要校验数据的集合
     * @param code 需要校验数据的泛型
     * @return 返回1表示校验成功否则返回校验结果
     */
//    public static String jiaoYanShuJu(List<?> xdkdbwxList, TableCodeEnum code, Object obj, LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId) {
//        StringBuffer result = new StringBuffer();
//        List<String> zhengqueid = new ArrayList<String>();
//        List<String> cuowuid = new ArrayList<String>();
//        if(xdkdbwxList!=null&&xdkdbwxList.size()>0) {
//            if(TableCodeEnum.DWDKDBWXX.equals(code)) {
//                for(int i=0;i<xdkdbwxList.size();i++) {
//                    Xdkdbwx xdkdbwx = (Xdkdbwx)xdkdbwxList.get(i);
//                    guiZeJiaoYanForDWDKDBWXX(xdkdbwx,result,zhengqueid,cuowuid);
//                }
//                XdkdbwxService xs = (XdkdbwxService )obj;
//                if(zhengqueid.size() > 0) {
//                    xs.updateXdkdbwxOnCheckstatus("1", zhengqueid);
//                }
//                if(cuowuid.size() > 0) {
//                    xs.updateXdkdbwxOnCheckstatus("2", cuowuid);
//                }
//            }else if(TableCodeEnum.JRJGFZJCXX.equals(code)){
//                for(int i=0;i<xdkdbwxList.size();i++) {
//                    Xjrjgfz xdkdbwx = (Xjrjgfz)xdkdbwxList.get(i);
//                    guiZeJiaoYanForJRJGFZJCXX(xdkdbwx,result,zhengqueid,cuowuid);
//                }
//                XjrjgfzService xs = (XjrjgfzService)obj;
//                if(zhengqueid.size() > 0) {
//                    xs.updateXjrjgfzOnCheckstatus("1", zhengqueid);
//                }
//                if(cuowuid.size() > 0) {
//                    xs.updateXjrjgfzOnCheckstatus("2", cuowuid);
//                }
//            }
//        }else {
//            result.append("要校验的数据集合为空");
//        }
//        if(result.length() == 0) {
//            result.append("1");
//        }
//        return result.toString();
//    }

    /**
     * 校验指定类型的数据
     * @param code 需要校验数据的泛型
     * @return 返回1表示校验成功否则返回校验结果
     */
//    public static String jiaoYanShuJu(TableCodeEnum code,Object obj,LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId) {
//        StringBuffer result = new StringBuffer();
//        Connection con = null;
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        List<String> zhengqueid = errorId;
//        List<String> cuowuid = rightId;
//        Integer integerDBWXXzq=null;
//        Integer integerDBWXXcw=null;
//        Integer integerJRFZzq=null;
//        Integer integerJRFZcw=null;
//        StringBuffer buffer=new StringBuffer();
//        if(TableCodeEnum.DWDKDBWXX.equals(code)) {
//            String query = "select id,gteecontractcode,loancontractcode,gteegoodscode,gteegoodscategory,warrantcode,isfirst,assessmode,assessmethod,assessvalue,assessdate,gteegoodsamt,firstrightamt,financeorgcode,financeorginnum,gzzq,sjrq from xdkdbwx where checkstatus='0' and (xdkdbwxstatus='0' or xdkdbwxstatus='2')";
//            try {
//                con = JDBCUtil.getConnection("/intg/jdbc.properties", "mysql.driverClassName", "mysql.url", "mysql.username", "mysql.password");
//                ps = con.prepareStatement(query);
//                rs = ps.executeQuery();
//                while (rs.next()) {
//                    Xdkdbwx xx = new Xdkdbwx();
//                    xx.setId(rs.getString(1));
//                    xx.setGteecontractcode(rs.getString(2));
//                    xx.setLoancontractcode(rs.getString(3));
//                    xx.setGteegoodscode(rs.getString(4));
//                    xx.setGteegoodscategory(rs.getString(5));
//                    xx.setWarrantcode(rs.getString(6));
//                    xx.setIsfirst(rs.getString(7));
//                    xx.setAssessmode(rs.getString(8));
//                    xx.setAssessmethod(rs.getString(9));
//                    xx.setAssessvalue(rs.getString(10));
//                    xx.setAssessdate(rs.getString(11));
//                    xx.setGteegoodsamt(rs.getString(12));
//                    xx.setFirstrightamt(rs.getString(13));
//                    xx.setFinanceorgcode(rs.getString(14));
//                    xx.setFinanceorginnum(rs.getString(15));
//                    xx.setGzzq(rs.getString(16));
//                    xx.setSjrq(rs.getString(17));
//                    guiZeJiaoYanForDWDKDBWXX(xx,result,zhengqueid,cuowuid);
//                }
//                XdkdbwxService  xs = (XdkdbwxService )obj;
//                if(zhengqueid.size() > 0) {
//                    integerDBWXXzq = xs.updateXdkdbwxOnCheckstatus("1", zhengqueid);
//                }
//                if(cuowuid.size() > 0) {
//                    integerDBWXXcw = xs.updateXdkdbwxOnCheckstatus("2", cuowuid);
//                }
//                if (integerDBWXXzq!=null||integerDBWXXcw!=null){
//                    buffer.append("2");
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }finally {
//                if(rs != null) {
//                    try {
//                        rs.close();
//                    } catch (SQLException e) {
//                        e.printStackTrace();
//                    }
//                }
//                if(ps != null) {
//                    try {
//                        ps.close();
//                    } catch (SQLException e) {
//                        e.printStackTrace();
//                    }
//                }
//                if(con != null) {
//                    try {
//                        con.close();
//                    } catch (SQLException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }else if(TableCodeEnum.JRJGFZJCXX.equals(code)){
//            String query = "select id,finorgname,finorgcode,finorgnum,inorgnum,xkzh,zfhh,orglevel,highlevelorgname,highlevelfinorgcode,highlevelinorgnum,regarea,regareacode,setupdate,mngmestus,sjrq from xjrjgfz where checkstatus='0' and (xdkdbwxstatus='0' or xdkdbwxstatus='2')";
//            try {
//                con = JDBCUtil.getConnection("/intg/jdbc.properties", "mysql.driverClassName", "mysql.url", "mysql.username", "mysql.password");
//                ps = con.prepareStatement(query);
//                rs = ps.executeQuery();
//                while (rs.next()) {
//                    Xjrjgfz xx = new Xjrjgfz();
//                    xx.setId(rs.getString(1));
//                    xx.setFinorgname(rs.getString(2));
//                    xx.setFinorgcode(rs.getString(3));
//                    xx.setFinorgnum(rs.getString(4));
//                    xx.setInorgnum(rs.getString(5));
//                    xx.setXkzh(rs.getString(6));
//                    xx.setZfhh(rs.getString(7));
//                    xx.setOrglevel(rs.getString(8));
//                    xx.setHighlevelorgname(rs.getString(9));
//                    xx.setHighlevelfinorgcode(rs.getString(10));
//                    xx.setHighlevelinorgnum(rs.getString(11));
//                    xx.setRegarea(rs.getString(12));
//                    xx.setRegareacode(rs.getString(13));
//                    xx.setSetupdate(rs.getString(14));
//                    xx.setMngmestus(rs.getString(15));
//                    xx.setSjrq(rs.getString(16));
//                    guiZeJiaoYanForJRJGFZJCXX(xx,result,zhengqueid,cuowuid);
//                }
//                XjrjgfzService xs = (XjrjgfzService)obj;
//                if(zhengqueid.size() > 0) {
//                    integerJRFZzq  = xs.updateXjrjgfzOnCheckstatus("1", zhengqueid);
//                }
//                if(cuowuid.size() > 0) {
//                    integerJRFZcw = xs.updateXjrjgfzOnCheckstatus("2", cuowuid);
//                }
//                if (integerJRFZzq!=null || integerJRFZcw!=null){
//                    buffer.append("2");
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }finally {
//                if(rs != null) {
//                    try {
//                        rs.close();
//                    } catch (SQLException e) {
//                        e.printStackTrace();
//                    }
//                }
//                if(ps != null) {
//                    try {
//                        ps.close();
//                    } catch (SQLException e) {
//                        e.printStackTrace();
//                    }
//                }
//                if(con != null) {
//                    try {
//                        con.close();
//                    } catch (SQLException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }
//
//        if(result.length() == 0) {
//            result.append("1");
//        }
//
//        if (buffer.length() ==0){
//            result.append("3");
//
//        }
//        return result.toString();
//    }

    /**
     * 对单位贷款担保物信息进行规则校验
     * @param xdkdbwx 校验的数据
     * @param  
     */
    public static void guiZeJiaoYanForDWDKDBWXX(CustomSqlUtil customSqlUtil,Xdkdbwx xdkdbwx,  List<String> checknumList,LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        boolean haveerror = false;
        StringBuffer errorsb = new StringBuffer("");

        try {
            if(StringUtils.isNotBlank(xdkdbwx.getFinanceorgcode())) {
                if(xdkdbwx.getFinanceorgcode().length() >18 || xdkdbwx.getFinanceorgcode().length() < 18) {
                    if (checknumList.contains("JS0365"))
                    {
                        errorsb.append("金融机构代码长度应为18位|");
                        haveerror = true;
                    }
                }else {
                    if (checkStr(xdkdbwx.getFinanceorgcode())){
                        if (checknumList.contains("JS0359"))
                        {
                            errorsb.append("金融机构代码含有特殊字符|");
                            haveerror = true;
                        }
                    }
                }

                if (fzList == null) {
                    String sql = "select distinct finorgcode from xjrjgfz";
                    fzList = customSqlUtil.executeQuery(sql);
                }
                if (frList == null) {
                    String sql = "select distinct finorgcode from xjrjgfrbaseinfo";
                    frList = customSqlUtil.executeQuery(sql);
                }
                if(checknumList.contains("JS1887")) {
                    if(!fzList.contains(xdkdbwx.getFinanceorgcode()) && !frList.contains(xdkdbwx.getFinanceorgcode())) {
                        errorsb.append("金融机构代码必须在金融机构（法人）基础信息表.金融机构代码或者金融机构（分支机构）基础信息表.金融机构代码中存在|");
                        haveerror = true;
                    }
                }
            }else {
                if (checknumList.contains("JS1071"))
                {
                    errorsb.append("金融机构代码为空|");
                    haveerror = true;
                }
            }


//            if (StringUtils.isNotBlank(xdkdbwx.getSjrq())){
////                if(xdkdbwx.getSjrq().length() != 10) {
////                    errorsb.append("数据日期长度不为10|");
////                    haveerror = true;
////                }else {
////                    Matcher mat = pat.matcher(xdkdbwx.getSjrq());
////                    if(mat.find()) {
////                        errorsb.append("数据日期含有特殊字符|");
////                        haveerror = true;
////                    }else {
//                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//                        format.setLenient(false);
//                        try {
//                            Date date = format.parse(xdkdbwx.getSjrq());
//                            String s1 = "1900-01-01";
//                            String s2 = "2100-12-31";
//                            Date d1 = format.parse(s1);
//                            Date d2 = format.parse(s2);
//                            if(date.after(d1) && date.before(d2)) {
//
//                            }else {
//                                errorsb.append("数据日期范围错误|");
//                                haveerror = true;
//                            }
//                        } catch (ParseException e) {
//                            errorsb.append("数据日期范围错误|");
//                            haveerror = true;
//                            e.printStackTrace();
//                        }
//                    }
////                }
////            }else {
////                errorsb.append("数据日期为空|");
////                haveerror = true;
////            }

            haveerror = CheckUtil.nullAndDate(xdkdbwx.getSjrq(), errorsb, haveerror, "数据日期",false);

            if(StringUtils.isNotBlank(xdkdbwx.getFinanceorginnum())) {
                if(xdkdbwx.getFinanceorginnum().length() > 30) {
                    if (checknumList.contains("JS0366"))
                    {
                        errorsb.append("内部机构号长度大于30|");
                        haveerror = true;
                    }
                }else {
                    if(checkStr01(xdkdbwx.getFinanceorginnum())){
                        if (checknumList.contains("JS0360"))
                        {
                            errorsb.append("内部机构号含有特殊字符|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1540")) { errorsb.append("内部机构号为空|");haveerror = true;}
            }

            if(StringUtils.isNotBlank(xdkdbwx.getGteecontractcode())) {
                if(xdkdbwx.getGteecontractcode().length() > 100) {
                    if (checknumList.contains("JS0367"))
                    {
                        errorsb.append("担保合同编码长度大于100|");
                        haveerror = true;
                    }
                }else {
                    if (checkStr01(xdkdbwx.getGteecontractcode())){
                        if (checknumList.contains("JS0361"))
                        {
                            errorsb.append("担保合同编码含有特殊字符|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1072")) { errorsb.append("担保合同编码为空|");haveerror = true; }
            }

            if(StringUtils.isNotBlank(xdkdbwx.getLoancontractcode())) {
                if(xdkdbwx.getLoancontractcode().length() > 100) {
                    if (checknumList.contains("JS0368")) {
                        errorsb.append("被担保合同编码长度大于100|");
                        haveerror = true;
                    }
                }else {
                    if (checkStr01(xdkdbwx.getLoancontractcode())){
                        if (checknumList.contains("JS0362")) {
                            errorsb.append("被担保合同编码含有特殊字符|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1073 ")) {errorsb.append("被担保合同编码为空|");haveerror = true; }

            }

            if(StringUtils.isNotBlank(xdkdbwx.getGteegoodscode())) {
                if(xdkdbwx.getGteegoodscode().length() > 100) {
                    if (checknumList.contains("JS0369"))
                    {
                        errorsb.append("担保物编码长度大于100|");
                        haveerror = true;
                    }
                }else {
                    if (checkStr01(xdkdbwx.getGteegoodscode())){
                        if (checknumList.contains("JS0363"))
                        {
                            errorsb.append("担保物编码含有特殊字符|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1074")) {errorsb.append("担保物编码为空|");haveerror = true;}
            }

            if(StringUtils.isNotBlank(xdkdbwx.getGteegoodscategory())) {
                if(xdkdbwx.getGteegoodscategory().length() != 3) {
                    errorsb.append("担保物类别长度不为3|");
                    haveerror = true;
                }else {
                    if("A01".equals(xdkdbwx.getGteegoodscategory()) || "A02".equals(xdkdbwx.getGteegoodscategory()) || "A03".equals(xdkdbwx.getGteegoodscategory())
                            || "A04".equals(xdkdbwx.getGteegoodscategory()) || "A05".equals(xdkdbwx.getGteegoodscategory()) || "A06".equals(xdkdbwx.getGteegoodscategory())
                            || "A07".equals(xdkdbwx.getGteegoodscategory()) || "A08".equals(xdkdbwx.getGteegoodscategory()) || "A09".equals(xdkdbwx.getGteegoodscategory())
                            || "A99".equals(xdkdbwx.getGteegoodscategory()) || "B01".equals(xdkdbwx.getGteegoodscategory()) || "B02".equals(xdkdbwx.getGteegoodscategory())
                            || "B99".equals(xdkdbwx.getGteegoodscategory()) || "C01".equals(xdkdbwx.getGteegoodscategory()) || "C02".equals(xdkdbwx.getGteegoodscategory())
                            || "C03".equals(xdkdbwx.getGteegoodscategory()) || "C04".equals(xdkdbwx.getGteegoodscategory()) || "C05".equals(xdkdbwx.getGteegoodscategory())
                            || "C99".equals(xdkdbwx.getGteegoodscategory()) || "D01".equals(xdkdbwx.getGteegoodscategory()) || "D02".equals(xdkdbwx.getGteegoodscategory())
                            || "D03".equals(xdkdbwx.getGteegoodscategory()) || "D04".equals(xdkdbwx.getGteegoodscategory()) || "D05".equals(xdkdbwx.getGteegoodscategory())
                            || "D06".equals(xdkdbwx.getGteegoodscategory()) || "D07".equals(xdkdbwx.getGteegoodscategory()) || "D08".equals(xdkdbwx.getGteegoodscategory())
                            || "D09".equals(xdkdbwx.getGteegoodscategory()) || "D99".equals(xdkdbwx.getGteegoodscategory())) {

                    }else {
                        if (checknumList.contains("JS0126")) {  errorsb.append("担保物类别需在符合要求的最底层值域范围内|");haveerror = true; }

                    }
                }
            }else {
                if (checknumList.contains("JS1075")) { errorsb.append("担保物类别为空|");haveerror = true; }

            }

            if(StringUtils.isNotBlank(xdkdbwx.getWarrantcode())) {
                if(xdkdbwx.getWarrantcode().length() > 500) {
                    if (checknumList.contains("JS0370"))
                    {
                        errorsb.append("权证编号长度大于500|");
                        haveerror = true;
                    }
                }else {
                    if (checkStr01(xdkdbwx.getWarrantcode())){
                        if (checknumList.contains("JS0364"))
                        {
                            errorsb.append("权证编号含有特殊字符|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1076")){ errorsb.append("权证编号为空|");haveerror = true;}
            }

            if(StringUtils.isNotBlank(xdkdbwx.getIsfirst())) {
                if(xdkdbwx.getIsfirst().length() != 1) {
                    errorsb.append("是否第一顺位长度不为1|");
                    haveerror = true;
                }else {
                    if("0".equals(xdkdbwx.getIsfirst()) || "1".equals(xdkdbwx.getIsfirst())) {

                    }else {
                        if (checknumList.contains("JS0127")){ errorsb.append("是否第一顺位码值不正确|");haveerror = true;}
                    }
                }
            }
//            else {
//                errorsb.append("是否第一顺位为空|");
//                haveerror = true;
//            }

            if(StringUtils.isNotBlank(xdkdbwx.getAssessmode())) {
                if(xdkdbwx.getAssessmode().length() != 2) {
                    if (checknumList.contains("JS0128"))
                    {
                        errorsb.append("评估方式长度不为2|");
                        haveerror = true;
                    }
                }else {
                    if("01".equals(xdkdbwx.getAssessmode()) || "02".equals(xdkdbwx.getAssessmode()) || "03".equals(xdkdbwx.getAssessmode())) {

                    }else {
                        if (checknumList.contains("JS0128"))
                        {
                            errorsb.append("评估方式码值不正确|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS0128")) { errorsb.append("评估方式为空|");haveerror = true; }
            }

            if(StringUtils.isNotBlank(xdkdbwx.getAssessmethod())) {
                if(xdkdbwx.getAssessmethod().length() != 2) {
                    if (checknumList.contains("JS0129"))
                    {
                        errorsb.append("评估方法长度不为2|");
                        haveerror = true;
                    }
                }else {
                    if("01".equals(xdkdbwx.getAssessmethod()) || "02".equals(xdkdbwx.getAssessmethod()) || "03".equals(xdkdbwx.getAssessmethod()) || "04".equals(xdkdbwx.getAssessmethod()) || "09".equals(xdkdbwx.getAssessmethod()) ) {

                    }else {
                        if (checknumList.contains("JS0129"))
                        {
                            errorsb.append("评估方法码值不正确|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS0129"))
                {
                    errorsb.append("评估方法为空|");
                    haveerror = true;
                }
            }

            if(StringUtils.isNotBlank(xdkdbwx.getAssessvalue())) {
                if (Double.valueOf(xdkdbwx.getAssessvalue())<1000 || Double.valueOf(xdkdbwx.getAssessvalue())>10000000000L){
                    if (checknumList.contains("JS2778"))
                    {
                        errorsb.append("评估价值一般在1000元至100亿元范围内|");
                        haveerror = true;
                    }
                }

                if(xdkdbwx.getAssessvalue().length() > 20) {
                    if (checknumList.contains("JS0371"))
                    {
                        errorsb.append("评估价值长度大于20|");
                        haveerror = true;
                    }

                }else {
                    Matcher match=pattern.matcher(xdkdbwx.getAssessvalue());
                    if(match.matches()) {

                    }else {
                        if (checknumList.contains("JS0371"))
                        {
                            errorsb.append("评估价值值不正确|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1080")) { errorsb.append("评估价值为空|");haveerror = true;}

            }
//
          if(StringUtils.isNotBlank(xdkdbwx.getAssessdate())) {
                if(xdkdbwx.getAssessdate().length() != 10) {
                    errorsb.append("评估基准日长度不为10|");
                    haveerror = true;
                }else {
                    boolean b1 = CheckUtil.checkDate(xdkdbwx.getAssessdate(), "yyyy-MM-dd");
                    if (b1){
                        if (xdkdbwx.getAssessdate().compareTo("1800-01-01") >= 0 && xdkdbwx.getAssessdate().compareTo("2100-12-31") <= 0) {
                            if (StringUtils.isNotBlank(xdkdbwx.getSjrq()) &&
                                    xdkdbwx.getAssessdate().compareTo(xdkdbwx.getSjrq()) > 0) {
                                if (checknumList.contains("JS2448"))
                                {
                                    errorsb.append("评估基准日应小于等于数据日期|");
                                    haveerror = true;
                                }
                            }
                        }
                    }
                    Matcher mat = pat.matcher(xdkdbwx.getAssessdate());
                    if(mat.find()) {
                        if (checknumList.contains("JS0375"))
                        {
                            errorsb.append("评估基准日含有特殊字符|");
                            haveerror = true;
                        }
                    }else {
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        format.setLenient(false);
                        try {
                            Date date = format.parse(xdkdbwx.getAssessdate());
                            String s1 = "1800-01-01";
                            String s2 = "2100-12-31";
                            Date d1 = format.parse(s1);
                            Date d2 = format.parse(s2);
                            if(date.after(d1) && date.before(d2)) {

                            }else {
                                if (checknumList.contains("JS0375"))
                                {
                                    errorsb.append("评估基准日范围错误|");
                                    haveerror = true;
                                }
                            }
                        } catch (ParseException e) {
                            if (checknumList.contains("JS0375")) {  errorsb.append("评估基准日范围错误|");haveerror = true;}
                            e.printStackTrace();
                        }
                    }
                }
            }

            if(StringUtils.isNotBlank(xdkdbwx.getGteegoodsamt())) {
                if (Double.valueOf(xdkdbwx.getGteegoodsamt())<0){
                    if (checknumList.contains("JS2449"))
                    {
                        errorsb.append("当担保物票面价值不为空时，担保物票面价值应大于等于0|");
                        haveerror = true;
                    }
                }
                if (Double.valueOf(xdkdbwx.getGteegoodsamt())<1000 || Double.valueOf(xdkdbwx.getGteegoodsamt())>10000000000L){
                    if (checknumList.contains("JS2779"))
                    {
                        errorsb.append("当担保物票面价值不为空时，担保物票面价值一般在1000元至100亿元范围内|");
                        haveerror = true;
                    }
                }

                if(xdkdbwx.getGteegoodsamt().length() > 20 || !CheckUtil.checkDecimal(xdkdbwx.getGteegoodsamt(), 2)) {
                    if (checknumList.contains("JS0372"))
                    {
                        errorsb.append("担保物票面价值总长度不能超过20位，小数位必须为2位|");
                        haveerror = true;
                    }
                }else {
                    Matcher match=pattern.matcher(xdkdbwx.getGteegoodsamt());
                    if(match.matches()) {

                    }else {
                        if (checknumList.contains("JS0372"))
                        {
                            errorsb.append("担保物票面价值值不正确|");
                            haveerror = true;
                        }
                    }
                }
            }

            if (StringUtils.isNotBlank(xdkdbwx.getGteegoodscategory())){
                if (!xdkdbwx.getGteegoodscategory().startsWith("A")&&StringUtils.isNotBlank(xdkdbwx.getGteegoodsamt())){
                    if (checknumList.contains("JS2780"))
                    {
                        errorsb.append("当担保物类别不为A开头的金融质押品时，担保物票面价值必须为空|");
                        haveerror = true;
                    }
                }

                if ("A02".equals(xdkdbwx.getGteegoodscategory()) || "A04".equals(xdkdbwx.getGteegoodscategory()) || "A05".equals(xdkdbwx.getGteegoodscategory()) || "A06".equals(xdkdbwx.getGteegoodscategory())
                        || "A07".equals(xdkdbwx.getGteegoodscategory())) {
                    if (StringUtils.isBlank(xdkdbwx.getGteegoodsamt())) {
                        if (checknumList.contains("JS1082"))
                        {
                            errorsb.append("当担保物类别为存单、债券、票据、股票和基金时，担保物票面价值不能为空|");
                            haveerror = true;
                        }
                        
                    }
                }

                
            }

            if(StringUtils.isNotBlank(xdkdbwx.getFirstrightamt())) {
                if(xdkdbwx.getFirstrightamt().length() > 20) {
                    if (checknumList.contains("JS0373"))
                    {
                        errorsb.append("优先受偿权数额长度大于20|");
                        haveerror = true;
                    }
                }else {
                    Matcher match=pattern.matcher(xdkdbwx.getFirstrightamt());
                    if(match.matches()) {

                    }else {
                        if (checknumList.contains("JS0373"))
                        {
                            errorsb.append("优先受偿权数额值不正确|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1083")) { errorsb.append("优先受偿权数额为空|");haveerror = true;}
            }

            if(StringUtils.isNotBlank(xdkdbwx.getGzzq())) {
                if(xdkdbwx.getGzzq().length() != 2) {
                    if (checknumList.contains("JS0130"))
                    {
                        errorsb.append("估值周期长度不为2|");
                        haveerror = true;
                    }
                }else {
                    if("01".equals(xdkdbwx.getGzzq()) || "02".equals(xdkdbwx.getGzzq()) || "03".equals(xdkdbwx.getGzzq())
                            || "04".equals(xdkdbwx.getGzzq()) || "05".equals(xdkdbwx.getGzzq()) || "06".equals(xdkdbwx.getGzzq())
                            || "07".equals(xdkdbwx.getGzzq()) || "99".equals(xdkdbwx.getGzzq())) {

                    }else {
                        if (checknumList.contains("JS0130")) {    errorsb.append("估值周期码值不正确|");haveerror = true;}
                    }
                }
            }else {
                if (checknumList.contains("JS1084")) { errorsb.append("估值周期为空|");haveerror = true;}
            }

		/*if(StringUtils.isNotBlank(xdkdbwx.getGteegoodsstataus())) {
			if(xdkdbwx.getGteegoodsstataus().length() != 2) {
				//result.append("担保物状态长度不为2,");
				errorsb.append("担保物状态长度不为2|");
				haveerror = true;
			}else {
				if("01".equals(xdkdbwx.getGteegoodsstataus()) || "02".equals(xdkdbwx.getGteegoodsstataus()) || "03".equals(xdkdbwx.getGteegoodsstataus())
                 || "04".equals(xdkdbwx.getGteegoodsstataus()) || "05".equals(xdkdbwx.getGteegoodsstataus())) {

				}else {
					//result.append("担保物状态码值不正确,");
					errorsb.append("担保物状态码值不正确|");
					haveerror = true;
				}
			}
		}else {
			//result.append("担保物状态为空,");
			errorsb.append("担保物状态为空|");
			haveerror = true;
		}*/

		/*if(StringUtils.isNotBlank(xdkdbwx.getMortgagepgerate())) {
			if(xdkdbwx.getMortgagepgerate().length() > 10) {
				//result.append("抵质押率长度大于10,");
				errorsb.append("抵质押率长度大于10|");
				haveerror = true;
			}else {
				Matcher match=pattern.matcher(xdkdbwx.getMortgagepgerate());
				if(match.matches()) {

				}else {
					//result.append("抵质押率值不正确,");
					errorsb.append("抵质押率值不正确|");
					haveerror = true;
				}
			}
		}else {
			//result.append("抵质押率为空,");
			errorsb.append("抵质押率为空|");
			haveerror = true;
		}*/

            if(haveerror) {
                if(!errorMsg.containsKey(xdkdbwx.getId())) {
                    errorMsg.put(xdkdbwx.getId(), "担保合同编码:"+xdkdbwx.getGteecontractcode()+"，"+"担保物编码:"+xdkdbwx.getGteegoodscode()+"]->\r\n");
                }
                String str = errorMsg.get(xdkdbwx.getId());
                str = str + errorsb;
                errorMsg.put(xdkdbwx.getId(), str);
                errorId.add(xdkdbwx.getId());
            }else {
                if(!errorId.contains(xdkdbwx.getId())) {
                    rightId.add(xdkdbwx.getId());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     机构级别 * 对金融机构（机构分支）基础信息进行规则校验
     *
     */
    public static void guiZeJiaoYanForJRJGFZJCXX(Xjrjgfz xjrjgfz,List<String> checknumList, LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId) {
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;
        boolean haveerror = false;
        StringBuffer errorsb = new StringBuffer("");

        try {

            if(StringUtils.isNotBlank(xjrjgfz.getFinorgname())) {
                if(xjrjgfz.getFinorgname().length() > 200) {
                    if (checknumList.contains("JS0559"))
                    {
                        errorsb.append("金融机构名称长度大于200|");
                        haveerror = true;
                    }
                }
                if (CheckUtil.checkStr(xjrjgfz.getFinorgname())){
                    if (checknumList.contains("JS0556"))
                    {
                        errorsb.append("金融机构名称字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。|");
                        haveerror = true;
                    }
                }
            }else {
                if (checknumList.contains("JS1234"))
                {
                    errorsb.append("金融机构名称为空|");
                    haveerror = true;
                }
            }

            if(StringUtils.isNotBlank(xjrjgfz.getFinorgcode())) {
                if(xjrjgfz.getFinorgcode().length() != 18) {
                    if (checknumList.contains("JS0560"))
                    {
                        errorsb.append("金融机构代码长度不为18|");
                        haveerror = true;
                    }
                }else {
                    if (checkStr(xjrjgfz.getFinorgcode())){
                        if (checknumList.contains("JS0549"))
                        {
                            errorsb.append("金融机构代码含有特殊字符|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1235")) { errorsb.append("金融机构代码为空|");haveerror = true;}
            }

            if(StringUtils.isNotBlank(xjrjgfz.getFinorgnum())) {
                if(xjrjgfz.getFinorgnum().length() != 14) {
                    if (checknumList.contains("JS0561"))
                    {
                        errorsb.append("金融机构编码长度不为14|");
                        haveerror = true;
                    }
                }else {
                    if (checkStr01(xjrjgfz.getFinorgnum())){
                        if (checknumList.contains("JS0550"))
                        {
                            errorsb.append("金融机构编码含有特殊字符|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1236"))
                {
                    errorsb.append("金融机构编码为空|");
                    haveerror = true;
                }
            }

            if(StringUtils.isNotBlank(xjrjgfz.getInorgnum())) {
                if (xjrjgfz.getInorgnum().equals(xjrjgfz.getHighlevelinorgnum())&&!xjrjgfz.getOrglevel().equals("01")){
                    if (checknumList.contains("JS2785"))
                    {
                        errorsb.append("内部机构号等于直属上级管理机构内部机构号，则机构级别为01-总行|");
                        haveerror = true;
                    }
                }

                if(xjrjgfz.getInorgnum().length() > 30) {
                    if (checknumList.contains("JS0562"))
                    {
                        errorsb.append("内部机构号长度大于30|");
                        haveerror = true;
                    }
                }else {
                    if (checkStr01(xjrjgfz.getInorgnum())){
                        if (checknumList.contains("JS0551"))
                        {
                            errorsb.append("内部机构号含有特殊字符|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1237")) { errorsb.append("内部机构号为空|");haveerror = true;}
            }

            if(StringUtils.isNotBlank(xjrjgfz.getOrglevel())) {
                if(xjrjgfz.getOrglevel().length() != 2) {
                    errorsb.append("机构级别长度不为2|");
                    haveerror = true;
                }else {
                    if("01".equals(xjrjgfz.getOrglevel()) || "02".equals(xjrjgfz.getOrglevel()) || "03".equals(xjrjgfz.getOrglevel())
                            || "04".equals(xjrjgfz.getOrglevel()) || "05".equals(xjrjgfz.getOrglevel()) || "99".equals(xjrjgfz.getOrglevel())) {

                    }else {
                        if (checknumList.contains("JS0173"))
                        {
                            errorsb.append("机构级别码值不正确|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1238")) {   errorsb.append("机构级别为空|");haveerror = true;   }
            }
            if (StringUtils.isNotBlank(xjrjgfz.getOrglevel())){
                if (xjrjgfz.getOrglevel().equals("01")){
                    if (!xjrjgfz.getHighlevelorgname().equals(xjrjgfz.getFinorgname())){
                        if (checknumList.contains("JS2685"))
                        {
                            errorsb.append("机构级别为01-总行，直属上级管理机构名称应该等于金融机构名称|");
                            haveerror = true;
                        }
                    }
                    if (!xjrjgfz.getHighlevelfinorgcode().equals(xjrjgfz.getFinorgnum())){
                        if (checknumList.contains("JS2686"))
                        {
                            errorsb.append("机构级别为01-总行，直属上级管理机构金融机构编码应该等于金融机构编码|");
                            haveerror = true;
                        }
                    }
                    if (!xjrjgfz.getHighlevelinorgnum().equals(xjrjgfz.getInorgnum())){
                        if (checknumList.contains("JS2687"))
                        {
                            errorsb.append("机构级别为01-总行，直属上级管理机构内部机构号应该等于内部机构号|");
                            haveerror = true;
                        }
                    }

                }

            }
            if(StringUtils.isNotBlank(xjrjgfz.getHighlevelorgname())) {
                if(xjrjgfz.getHighlevelorgname().length() > 200) {
                    if (checknumList.contains("JS0563"))
                    {
                        errorsb.append("直属上级管理机构名称长度大于200|");
                        haveerror = true;
                    }
                }else {
                    if (checkStr01(xjrjgfz.getHighlevelorgname())){
                        if (checknumList.contains("JS0558"))
                        {
                            errorsb.append("直属上级管理机构名称含有特殊字符|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1239"))
                {
                    errorsb.append("直属上级管理机构名称为空|");
                    haveerror = true;
                }
            }

            if(StringUtils.isNotBlank(xjrjgfz.getHighlevelfinorgcode())) {
                if(xjrjgfz.getHighlevelfinorgcode().length() != 14) {
                    if (checknumList.contains("JS0564"))
                    {
                        errorsb.append("直属上级管理机构金融机构编码长度不为14|");
                        haveerror = true;
                    }
                }else {
                    if (checkStr01(xjrjgfz.getHighlevelfinorgcode())){
                        if (checknumList.contains("JS0552"))
                        {
                            errorsb.append("直属上级管理机构金融机构编码含有特殊字符|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1240"))
                {
                    errorsb.append("直属上级管理机构金融机构编码为空|");
                    haveerror = true;
                }
            }

            if(StringUtils.isNotBlank(xjrjgfz.getHighlevelinorgnum())) {
                if(xjrjgfz.getHighlevelinorgnum().length() > 30) {
                    if (checknumList.contains("JS0565"))
                    {
                        errorsb.append("直属上级管理机构内部机构号长度大于30|");
                        haveerror = true;
                    }
                }else {
                    if (checkStr01(xjrjgfz.getHighlevelinorgnum())){
                        if (checknumList.contains("JS0553"))
                        {
                            errorsb.append("直属上级管理机构内部机构号含有特殊字符|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1241"))
                {
                    errorsb.append("直属上级管理机构内部机构号为空|");
                    haveerror = true;
                }
            }

            if(StringUtils.isNotBlank(xjrjgfz.getXkzh())) {
                if(xjrjgfz.getXkzh().length() > 15) {
                    if (checknumList.contains("JS0567"))
                    {
                        errorsb.append("许可证号长度大于15|");
                        haveerror = true;
                    }
                }else {
                    if (checkStr01(xjrjgfz.getXkzh())){
                        if (checknumList.contains("JS0554"))
                        {
                            errorsb.append("许可证号含有特殊字符|");
                            haveerror = true;
                        }
                    }

//                    Matcher mat = pat.matcher(xjrjgfz.getXkzh());
//                    if(mat.find()) {
//                        errorsb.append("许可证号含有特殊字符|");
//                        haveerror = true;
//                    }
                }
            }else {
                if (checknumList.contains("JS1563")) {     errorsb.append("许可证号为空|");haveerror = true;  }
            }

            if(StringUtils.isNotBlank(xjrjgfz.getZfhh())) {
                if(xjrjgfz.getZfhh().length() == 12 || xjrjgfz.getZfhh().length() == 14) {

                }else {
                    if (checknumList.contains("JS0568"))
                    {
                        errorsb.append("支付行号长度必须为12或14|");
                        haveerror = true;
                    }
                }

                if (checkStr01(xjrjgfz.getZfhh())){
                    if (checknumList.contains("JS0555"))
                    {
                        errorsb.append("支付行号含有特殊字符|");
                        haveerror = true;
                    }
                }
            }else {
                if (checknumList.contains("JS1564")) {  errorsb.append("支付行号为空|");haveerror = true;}
            }

            if(StringUtils.isNotBlank(xjrjgfz.getRegarea())) {
                if(xjrjgfz.getRegarea().length() > 400) {
                    if (checknumList.contains("JS0566"))
                    {
                        errorsb.append("注册地址字符长度不能超过400|");
                        haveerror = true;
                    }
                }
            }else {
                if (checknumList.contains("JS1242"))
                {
                    errorsb.append("注册地为空|");
                    haveerror = true;
                }
            }

            if(StringUtils.isNotBlank(xjrjgfz.getRegareacode())) {
                if(xjrjgfz.getRegareacode().length() != 6) {
                    if (checknumList.contains("JS0174"))
                    {
                        errorsb.append("地区代码长度不为6|");
                        haveerror = true;
                    }
                }else {
                    boolean flag = true;
                    for(int i = 0;i<XApplicationRunnerImpl.baseAreaList.size();i++) {
                        if(xjrjgfz.getRegareacode().equals(XApplicationRunnerImpl.baseAreaList.get(i).getAreacode())) {
                            flag = false;
                            break;
                        }
                    }
                    if(flag) {
                        if (checknumList.contains("JS0174"))
                        {
                            errorsb.append("地区代码码值不正确|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1243")) { errorsb.append("地区代码为空|");haveerror = true;}
            }

            if(StringUtils.isNotBlank(xjrjgfz.getSetupdate())) {
                if(xjrjgfz.getSetupdate().length() != 10) {
                    errorsb.append("成立时间长度不为10|");
                    haveerror = true;
                }else {
                    boolean b1 = CheckUtil.checkDate(xjrjgfz.getSetupdate(), "yyyy-MM-dd");
                    if (b1){
                        if (xjrjgfz.getSetupdate().compareTo("1900-01-01") >= 0 && xjrjgfz.getSetupdate().compareTo("2100-12-31") <= 0) {
                            if (StringUtils.isNotBlank(xjrjgfz.getSjrq()) &&
                                    xjrjgfz.getSetupdate().compareTo(xjrjgfz.getSjrq()) > 0) {

                                if (checknumList.contains("JS2217"))
                                {
                                    errorsb.append("成立时间应小于等于数据日期|");
                                    haveerror = true;
                                }
                            }
                        }
                    }
                    Matcher mat = pat.matcher(xjrjgfz.getSetupdate());
                    if(mat.find()) {
                        errorsb.append("成立时间含有特殊字符|");
                        haveerror = true;
                    }else {
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        format.setLenient(false);
                        try {
                            Date date = format.parse(xjrjgfz.getSetupdate());
                            String s1 = "1900-01-01";
                            String s2 = "2100-12-31";
                            Date d1 = format.parse(s1);
                            Date d2 = format.parse(s2);
                            if(date.after(d1) && date.before(d2)) {

                            }else {
                                if (checknumList.contains("JS0570")) {  errorsb.append("成立时间范围错误|");haveerror = true; }
                            }
                        } catch (ParseException e) {
                            if (checknumList.contains("JS0570")) {  errorsb.append("成立时间范围错误|");haveerror = true; }
                            e.printStackTrace();
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS0570")) {  errorsb.append("成立时间为空|");haveerror = true;}
            }

            if(StringUtils.isNotBlank(xjrjgfz.getMngmestus())) {
                if(xjrjgfz.getMngmestus().length() != 2) {
                    if (checknumList.contains("JS0175"))
                    {
                        errorsb.append("营业状态长度不为2|");haveerror = true;
                    }

                }else {
                    if("01".equals(xjrjgfz.getMngmestus()) || "02".equals(xjrjgfz.getMngmestus()) || "03".equals(xjrjgfz.getMngmestus())
                            || "04".equals(xjrjgfz.getMngmestus()) || "05".equals(xjrjgfz.getMngmestus()) || "06".equals(xjrjgfz.getMngmestus())
                            || "07".equals(xjrjgfz.getMngmestus()) || "99".equals(xjrjgfz.getMngmestus())) {

                    }else {
                        if (checknumList.contains("JS0175"))
                        {
                            errorsb.append("营业状态码值不正确|");
                            haveerror = true;
                        }
                    }
                }
            }else {
                if (checknumList.contains("JS1245")) {   errorsb.append("营业状态为空|");haveerror = true;}
            }

//            if (StringUtils.isNotBlank(xjrjgfz.getSjrq())){
//                if(xjrjgfz.getSjrq().length() != 10) {
//                    errorsb.append("数据日期长度不为10|");
//                    haveerror = true;
//                }else {
//                    Matcher mat = pat.matcher(xjrjgfz.getSjrq());
//                    if(mat.find()) {
//                        errorsb.append("数据日期含有特殊字符|");
//                        haveerror = true;
//                    }else {
//                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//                        format.setLenient(false);
//                        try {
//                            Date date = format.parse(xjrjgfz.getSjrq());
//                            String s1 = "1900-01-01";
//                            String s2 = "2100-12-31";
//                            Date d1 = format.parse(s1);
//                            Date d2 = format.parse(s2);
//                            if(date.after(d1) && date.before(d2)) {
//
//                            }else {
//                                errorsb.append("数据日期范围错误|");
//                                haveerror = true;
//                            }
//                        } catch (ParseException e) {
//                            errorsb.append("数据日期范围错误|");
//                            haveerror = true;
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }else {
//                errorsb.append("数据日期为空|");
//                haveerror = true;
//            }

            haveerror = CheckUtil.nullAndDate(xjrjgfz.getSjrq(), errorsb, haveerror, "数据日期",false);

            if(haveerror) {
                if(!errorMsg.containsKey(xjrjgfz.getId())) {
                    errorMsg.put(xjrjgfz.getId(), "金融机构名称:"+xjrjgfz.getFinorgname()+"]->\r\n");
                }
                String str = errorMsg.get(xjrjgfz.getId());
                str = str + errorsb;
                errorMsg.put(xjrjgfz.getId(), str);
                errorId.add(xjrjgfz.getId());

            }else {
                if(!errorId.contains(xjrjgfz.getId())) {
                    rightId.add(xjrjgfz.getId());
                }
        }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (con!=null){
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps!=null){
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps1!=null){
                try {
                    ps1.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


    }


    /**
     * 校验是否存在特殊字符
     * @param str
     * @return true:存在特殊字符;false:不存在特殊字符
     */
    public static boolean checkStr(String str){
        Matcher m = strPattern.matcher(str);
        return m.find();
    }
    public static boolean checkStr01(String str){
        Matcher m = strPattern01.matcher(str);
        return m.find();
    }
    public static boolean checkDate(String date,String pattern){
        boolean convertSuccess=true;
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            format.parse(date);
        } catch (Exception e) {
            convertSuccess=false;
        }
        return convertSuccess;
    }

}
