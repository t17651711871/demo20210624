package com.geping.etl.UNITLOAN.util;

import com.geping.etl.utils.jdbc.JDBCUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * @Author: wangzd
 * @Date: 9:48 2020/4/13
 */
public class Stringutil {
    // 把一个字符串的第一个字母大写
    public static String getMethodName(String fieldName) {
        byte[] items = fieldName.getBytes();
        items[0] = (byte) ((char) items[0] - 'a' + 'A');
        return new String(items);
    }

    // 把一个字符串的第一个字母小写
    public static String getClassName(String className) {
        byte[] items = className.getBytes();
        items[0] = (byte) ((char) items[0] - 'A' + 'a');
        return new String(items);
    }

    //获取去掉横线的uuid
    public static String getUUid(){
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static<T> T getEntity(ResultSet resultSet, T entity) throws NoSuchFieldException, NoSuchMethodException, SQLException, InvocationTargetException, IllegalAccessException {
        Class<?> aClass = entity.getClass();
        Field[] fields = aClass.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible( true );
            Field declaredField = aClass.getDeclaredField(field.getName());
            Class<?> type = declaredField.getType();
            Method method = aClass.getDeclaredMethod("set" + Stringutil.getMethodName(field.getName()), type);
            method.setAccessible(true);
            if (type == Integer.class) {
                method.invoke(entity, resultSet.getInt(field.getName()));
            } else if (type == BigDecimal.class) {
                method.invoke(entity, resultSet.getBigDecimal(field.getName()));
            } else {
                method.invoke(entity, resultSet.getString(field.getName()));
            }
        }
        return entity;
    }

    /**
     * 生成报文中获取生成数据的方法
     * @param entity 实体
     * @param tableName 表名
     * @param <T> 泛型
     * @return List
     */
    public static<T> List<T> getReportList(T entity,String tableName){
        List<T> list=new ArrayList<>();
        String sql="select * from "+tableName+" where datastatus='3'";
        Connection connection = null;
        ResultSet resultSet=null;
        try {
            connection = JDBCUtils.getConnection();
            resultSet = JDBCUtils.Query(connection, sql);
            while (true) {
                if (!resultSet.next()) break;
                entity = (T) entity.getClass().newInstance();
                Stringutil.getEntity(resultSet, entity);
                list.add(entity);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (connection!=null){
                JDBCUtils.close();
            }
        }
        return list;
    }
    
    /**
     * 求数组中最小值
     * @param strings数组
     * @return 返回最小数
     */
    public static String getMinString(String... strings) {
    	String result = "";
    	try {
    		if(strings.length > 0) {
    			BigDecimal b0 = new BigDecimal("0");
        		for(int i=0;i<strings.length;i++) {
        			if(i==0) {
        				BigDecimal b1 = new BigDecimal(strings[i]);
        				b0 = b1;
        			}else {
        				BigDecimal b1 = new BigDecimal(strings[i]);
            			if(b1.compareTo(b0) > 0) {
            				
            			}else {
            				b0 = b1;
            				result = strings[i];
            			}
        			}
        		}
        	}else {
        		System.out.println("数组为空");
        	}
		} catch (Exception e) {
			System.out.println("数组存在非数值");
		}
    	return result;
    }
    
    /**
     * 判断传入的数值是否为零
     * @param strings 数值集合
     * @return 全为零返回0否则返回1
     */
    public static String panDuanShuZhiWeiLing(String... strings) {
    	String result = "0";
    	try {
    		if(strings.length > 0) {
    			BigDecimal b0 = new BigDecimal("0");
        		for(int i=0;i<strings.length;i++) {
        			BigDecimal b1 = new BigDecimal(strings[i]);
        			if(b1.compareTo(b0) > 0) {
        				result = "1";
        				break;
        			}
        		}
        	}else {
        		System.out.println("数组为空");
        	}
		} catch (Exception e) {
			System.out.println("数组存在非数值");
		}
    	return result;
    }
    
    /**
     * 格式化数值保留两位小数
     * @param str 需要格式化的字符串
     * @return 正常转换否则原样返回
     */
    public static String zhengLiJinE(String str) {
    	String result = "";
    	try {
    		if(StringUtils.isNotBlank(str)) {
    			DecimalFormat ft = new DecimalFormat("0.00");
        		BigDecimal big = new BigDecimal(str);
        		result = ft.format(big);
    		}
		} catch (Exception e) {
			e.printStackTrace();
			result = str;
			System.out.println("数值转换失败，已忽略");
		}
    	return result;
    }
}