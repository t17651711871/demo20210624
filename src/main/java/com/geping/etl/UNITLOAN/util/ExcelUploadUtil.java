package com.geping.etl.UNITLOAN.util;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 导入Excel工具类
 */
@Component
@PropertySource(value = "classpath:messagePath.properties")
public class ExcelUploadUtil {
    /**
     * excel 上传地址
     */
    @Value(value = "${uploadexcel.path}")
    private String uploadExcelPath;

    public File uploadFile(MultipartFile file) throws IOException {
        InputStream fileInput = null;
        FileOutputStream fileOutput = null;
        try {
            //上传到服务器
            File uploadDir = new File(uploadExcelPath);
            if (!uploadDir.exists() && !uploadDir.isDirectory()) {
                uploadDir.mkdirs();
            }
            String fileName = file.getOriginalFilename();
            int startIndex = fileName.lastIndexOf("\\") + 1;
            int endIndex = fileName.lastIndexOf(".");
            String excelName = fileName.substring(startIndex, endIndex);
            String sufix = fileName.substring(endIndex, fileName.length());
            String uploadFileName = uploadDir + File.separator + excelName + System.currentTimeMillis() + sufix;
            File uploadFile = new File(uploadFileName);
            fileInput = file.getInputStream();
            fileOutput = new FileOutputStream(uploadFile);
            byte[] readByte = new byte[4056];
            int size = 0;
            while ((size = fileInput.read(readByte)) != -1) {
                fileOutput.write(readByte, 0, size);
            }
            return uploadFile;
        } finally {
            try {
                if (null != fileOutput) {
                    fileOutput.flush();
                    fileOutput.close();
                }
                if (null != fileInput) {
                    fileInput.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
