package com.geping.etl.UNITLOAN.util.response;

/**
 * @Author: wangzd
 * @Date: 17:29 2020/6/22
 */
public enum CodeDetail {

    OPERATESUCCESS(20000,"操作成功"),
    CHECKSUCCESS(20001,"校验成功"),

    OPERATEERROR(30000,"操作失败"),
    CHECKERROR(30001,"校验失败");

    private int code;
    private String detail;

    CodeDetail(int code,String detail){
        this.code=code;
        this.detail=detail;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
