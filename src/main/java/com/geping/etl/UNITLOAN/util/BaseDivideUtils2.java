package com.geping.etl.UNITLOAN.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geping.etl.UNITLOAN.entity.vo.AdministrativeVo;


/**
 *
 * @author liujianan
 * @date 2020年9月2日 上午9:14:51
 */
@Controller
public class BaseDivideUtils2 {

    @Autowired
    private CustomSqlUtil customSqlUtil;
    
    @Autowired
    private XDataBaseTypeUtil xDataBaseTypeUtil;

    private static List<AdministrativeVo> jsonObjectList = new ArrayList<>();

    /**
     * 获取行政区划代码 （一级）
     * @return
     */
    @PostMapping("/XgetAdmindivideJson2")
    @ResponseBody
    public List<AdministrativeVo> getBaseCodeadmindivideJson(String id) {
        //如果參數為空說明是获取一级县市
        if (StringUtils.isBlank(id)) {
            if (jsonObjectList.size() == 0) {
                String sql = "select areacode,areaname from basearea where areacode like '%0000' order by areacode";
                List<Object[]> list = customSqlUtil.executeQuery(sql);
                jsonObjectList = list.stream()
                        .map(l -> {
                            AdministrativeVo administrativeVo = new AdministrativeVo();
                            administrativeVo.setId((String) l[0]);
                            administrativeVo.setText((String) l[0]+"-"+(String) l[1]);
                            administrativeVo.setState("closed");
                            administrativeVo.setChildren(new ArrayList<>(0));
                            return administrativeVo;
                        }).collect(Collectors.toList());

            /*    String sql2 = "select countrycode,countryname from basecountry  order by countrycode";
                List<Object[]> list2 = customSqlUtil.executeQuery(sql2);
                List<AdministrativeVo> countryList = list2.stream()
                        .map(l -> {
                            AdministrativeVo administrativeVo = new AdministrativeVo();
                            administrativeVo.setId((String) l[0]);
                            administrativeVo.setText((String) l[0]+"-"+(String) l[1]);
                            return administrativeVo;
                        }).collect(Collectors.toList());
                jsonObjectList.addAll(countryList);*/
                AdministrativeVo chooseNode = new AdministrativeVo();
                //效率不高考虑链表
            }
                return jsonObjectList;
        } else {
            if(id.endsWith("0000")) {
                //查詢二級列表
            	String sql = null;
				if(xDataBaseTypeUtil.equalsMySql()) {
					sql = "select areacode,areaname from basearea where left(areacode,2) = '"+id.substring(0, 2)+"' and areacode like '%00' and areacode not like '%0000' order by areacode";
				}else if(xDataBaseTypeUtil.equalsOracle()){
					sql = "select areacode,areaname from basearea where substr(areacode,1,2) = '"+id.substring(0, 2)+"' and areacode like '%00' and areacode not like '%0000' order by areacode";
				}else if(xDataBaseTypeUtil.equalsSqlServer()){
					sql = "select areacode,areaname from basearea where substring(areacode,1,2) = '"+id.substring(0, 2)+"' and areacode like '%00' and areacode not like '%0000' order by areacode";
				}
                List<Object[]> thirdList = customSqlUtil.executeQuery(sql);
                List<AdministrativeVo> childList = thirdList.stream()
                        .map(l -> {
                            AdministrativeVo administrativeVo = new AdministrativeVo();
                            administrativeVo.setId((String) l[0]);
                            administrativeVo.setText((String) l[0]+"-"+(String) l[1]);
                            administrativeVo.setState("closed");
                            administrativeVo.setChildren(new ArrayList<>(0));
                            return administrativeVo;
                        }).collect(Collectors.toList());
                return childList;
            }else {
                //查詢三級列表
            	String sql = null;
				if(xDataBaseTypeUtil.equalsMySql()) {
					sql = "select areacode,areaname from basearea where left(areacode,4) = '"+id.substring(0, 4)+"' and areacode not like '%0' order by areacode";
				}else  if(xDataBaseTypeUtil.equalsOracle()){
					sql = "select areacode,areaname from basearea where substr(areacode,1,4) = '"+id.substring(0, 4)+"' and areacode not like '%0' order by areacode";
				}else if(xDataBaseTypeUtil.equalsSqlServer()){
					sql = "select areacode,areaname from basearea where substring(areacode,1,4) = '"+id.substring(0, 4)+"' and areacode not like '%0' order by areacode";
				}
                List<Object[]> thirdList = customSqlUtil.executeQuery(sql);
                List<AdministrativeVo> childList = thirdList.stream()
                        .map(l -> {
                            AdministrativeVo administrativeVo = new AdministrativeVo();
                            administrativeVo.setId((String) l[0]);
                            administrativeVo.setText((String) l[0]+"-"+(String) l[1]);
                            return administrativeVo;
                        }).collect(Collectors.toList());
                return childList;
            }

        }
    }
}
