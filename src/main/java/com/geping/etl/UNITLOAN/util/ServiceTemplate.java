package com.geping.etl.UNITLOAN.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

public class ServiceTemplate {
	
	public static void main(String[] args) {
		// 创建表的SQL语句
		//String sql = "update xdkdbwx set operationname='申请修改',operator='admin',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where 1=1;";
		String sql = "select * from xdkdbwx where id in ('12312','121123','123456')";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(DataSourceUtils.getDataSource());
		//jdbcTemplate.execute(sql);
		//List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
		for (Map<String, Object> map : list) {
		      System.out.println(map);
		}
		//Map<String, Object> map = jdbcTemplate.queryForMap(sql,"12312");
		//Map<String, Object> map = jdbcTemplate.queryForMap(sql);
		//System.out.println("map="+map);
		//int result = jdbcTemplate.update(sql);
		//System.out.println("影响的行数: " + result);
	}
	
	/**
	 * 查询数据
	 * @param sql要查询数据的语句
	 * @return 返回查询的数据或者空
	 */
	public static List<Map<String, Object>> findData(String sql){
		JdbcTemplate jdbcTemplate = new JdbcTemplate(DataSourceUtils.getDataSource());
		return jdbcTemplate.queryForList(sql);
		
	}
	
	/**
	 * 申请修改，申请删除，同意申请，拒绝申请，审核通过，审核不通过，删除
	 * @param caozuoren 操作人
	 * @param caozuoming 操作名
	 * @param caozuobiao 操作的表名
	 * @param butongguoyuanyin 审核不通过原因
	 * @param idList 操作数据的主键集合
	 * @return 返回成功的行数
	 */
	public static int YeWuMoBan(String caozuoren,String caozuoming,String caozuobiao,String butongguoyuanyin,List<String> idList) {
		String sql = "";
		String id = "";
		for(int i=0;i<idList.size();i++) {
			if(i == 0) {
				id += idList.get(i);
			}else {
				id += ","+idList.get(i);
			}
		}
		if("删除".equals(caozuoming)) {
			sql = "delete from " + caozuobiao + " where id in ("+id.substring(1)+");";
		}else if("申请修改".equals(caozuoming) || "申请删除".equals(caozuoming)) {
			sql = "update " + caozuobiao + "set operationname='" + caozuoming + "',operator='" + caozuoren + "',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in ("+id.substring(1)+");";
		}else if("同意申请".equals(caozuoming)) {
			sql = "update " + caozuobiao + "set operationname='" + caozuoming + "',operator='" + caozuoren + "',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in ("+id.substring(1)+");";
		}else if("拒绝申请".equals(caozuoming)) {
			sql = "update " + caozuobiao + "set operationname=' ',operator='" + caozuoren + "',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in ("+id.substring(1)+");";
		}else if("审核通过".equals(caozuoming)) {
			sql = "update " + caozuobiao + "set datastatus='3',operator='" + caozuoren + "',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in ("+id.substring(1)+");";
		}else if("审核不通过".equals(caozuoming)) {
			sql = "update " + caozuobiao + "set datastatus='2',nopassreason='" + butongguoyuanyin + "',operator='" + caozuoren + "',operationtime='"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"' where id in ("+id.substring(1)+");";
		}
		return updateData(sql);
	}
	
	/**
	 * 申请修改，申请删除，同意申请，拒绝申请，审核通过，审核不通过，删除
	 * @param sql 要执行的语句
	 * @return 返回成功的行数
	 */
	public static int YeWuMoBan(String sql){
		return updateData(sql);
	}
	
	public static int updateData(String sql) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(DataSourceUtils.getDataSource());
		int result = jdbcTemplate.update(sql);
		System.out.println("影响的行数: " + result);
		return result;
	}

}
