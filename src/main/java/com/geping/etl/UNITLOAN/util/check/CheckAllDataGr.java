package com.geping.etl.UNITLOAN.util.check;

import static com.geping.etl.UNITLOAN.util.check.CheckUtil.checkStr2;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.report.Xclgrdkxx;
import com.geping.etl.UNITLOAN.entity.report.Xgrdkfsxx;
import com.geping.etl.UNITLOAN.service.report.XCheckRuleService;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.TuoMinUtils;


/**
 * @Author: wangzd
 * @Date: 15:21 2020/6/10
 */
public class CheckAllDataGr {
    //判断是否达到校验阈值
    public static boolean limitFlag = false;
    //币种代码
    private static List<String> currencyList;
    //行政区划代码
    private static List<String> areaList;
    //国家代码
    private static List<String> countryList;
    //大行业代码
    private static List<String> aindustryList;

    //C0017-行业中类代码
    private static List<String> bindustryList;
    @Autowired
    private CustomSqlUtil customSqlUtil;

    @Autowired
    private XCheckRuleService xCheckRuleService;

    //金融机构（分支机构）基础信息表.金融机构代码
    public static List<String> fzList;

    //金融机构（法人）基础信息表.金融机构代码
    public static List<String> frList;

    
    //存量个人贷款信息(带阈值)
    public static int checkXclgrdkxxLimit(CustomSqlUtil customSqlUtil, Xclgrdkxx xclgrdkxx,
                                          List<Integer> errorId0, List<String> errorId, List<String> rightId,
                                          List<String> xclgrdkxx1List, Map<String,String> limitMap, int checknumLimit) {
        boolean isError = false;
        StringBuffer tempMsg = new StringBuffer("");
        String nullError = "不能为空";
        String lengthError = "超过限制长度";
        String spaceStr = "|";
        //#金融机构代码
        String contractcode = xclgrdkxx.getContractcode();
        String receiptcode = xclgrdkxx.getReceiptcode();

//      isError = CheckUtil.grantNullAndLengthAndStr(xcldkxx.getFinanceorgcode(), 18, tempMsg, isError, "金融机构代码");
        if(StringUtils.isNotBlank(xclgrdkxx.getFinanceorgcode())){
            if (xclgrdkxx1List.contains("JS0910")){
                if(xclgrdkxx.getFinanceorgcode().length()!=18){
                    putMap("金融机构代码长度应为18",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (xclgrdkxx1List.contains("JS0902")){
                if(CheckUtil.checkStr(xclgrdkxx.getFinanceorgcode())){
                    putMap("金融机构代码字段内容中不得出现“？”、“！” 、“^” 、“*”其中“？”和“！”包含全角和半角两种格式。" ,contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }

            if (fzList == null) {
                String sql = "select distinct finorgcode from xjrjgfz";
                fzList = customSqlUtil.executeQuery(sql);
            }
            if (frList == null) {
                String sql = "select distinct finorgcode from xjrjgfrbaseinfo";
                frList = customSqlUtil.executeQuery(sql);
            }
            if(xclgrdkxx1List.contains("JS1887")) {
                if(!fzList.contains(xclgrdkxx.getFinanceorgcode()) && !frList.contains(xclgrdkxx.getFinanceorgcode())) {
                    putMap("金融机构代码必须在金融机构（法人）基础信息表.金融机构代码或者金融机构（分支机构）基础信息表.金融机构代码中存在" ,contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
        }else {
            if (xclgrdkxx1List.contains("JS1619")){
                putMap("金融机构代码不能为空",contractcode,receiptcode,limitMap, checknumLimit);
                isError = true;
            }
        }

        //#金融机构地区代码
        if (StringUtils.isNotBlank(xclgrdkxx.getFinanceorgareacode())) {
            if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
            if (xclgrdkxx1List.contains("JS0215")) {
                if (!areaList.contains(xclgrdkxx.getFinanceorgareacode())) {
                    putMap("金融机构地区代码不在规定的代码范围内",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
        } else {
            if (xclgrdkxx1List.contains("JS1621")){  putMap("金融机构地区代码不能为空" ,contractcode,receiptcode,limitMap, checknumLimit);isError = true; }
        }

        //#贷款合同编码
        if (StringUtils.isNotBlank(xclgrdkxx.getContractcode())){
            if (xclgrdkxx.getContractcode().length()>100){
                if (xclgrdkxx1List.contains("JS0914")){   putMap("贷款合同编码字符长度不能超过100" ,contractcode,receiptcode,limitMap, checknumLimit);isError = true;  }
            }
            if (xclgrdkxx1List.contains("JS0906")){
                if(CheckUtil.checkStr(xclgrdkxx.getContractcode())){
                    putMap("贷款合同编码字段内容中不得出现“？”、“！” 、“^” 、“*”其中“？”和“！”包含全角和半角两种格式。",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
        }else {
            if (xclgrdkxx1List.contains("JS1626")){ putMap("贷款合同编码不能为空",contractcode,receiptcode,limitMap, checknumLimit);isError = true; }
        }
        //查重

        //#币种
        if (currencyList==null) currencyList = customSqlUtil.getBaseCode(BaseCurrency.class);

        if (xclgrdkxx1List.contains("JS1631")){
            if (StringUtils.isBlank(xclgrdkxx.getCurrency())){
                putMap("币种不能为空",contractcode,receiptcode,limitMap, checknumLimit);
                isError = true;
            }
        }
        if (StringUtils.isNotBlank(xclgrdkxx.getCurrency())){
            if (xclgrdkxx1List.contains("JS0218")){
                isError = CheckUtil.checkCurrency(xclgrdkxx.getCurrency(), currencyList, customSqlUtil, tempMsg, isError, "币种");
            }
        }
//        isError = CheckUtil.checkCurrency(xcldkxx.getCurrency(), currencyList, customSqlUtil, tempMsg, isError, "币种");

        //#贷款财政扶持方式
        if (xclgrdkxx1List.contains("JS0221")){
            isError = CheckUtil.checkLoanczfcfs(xclgrdkxx.getLoanfinancesupport(), tempMsg, isError, "贷款财政扶持方式");
        }

        //#贷款产品类别
        if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())) {
            if (!ArrayUtils.contains(CheckUtil.dkfsecplb, xclgrdkxx.getProductcetegory())) {
                if (xclgrdkxx1List.contains("JS0217")){
                    putMap("贷款产品类别需在贷款品种码值中是否采集为是对应的最底层值域范围内",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
            if ("F03".equals(xclgrdkxx.getProductcetegory())) {
                if (xclgrdkxx1List.contains("JS2196")){
                    putMap("贷款产品不能出现F03拆借",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
        }
        else{
            if (xclgrdkxx1List.contains("JS1627")){
                putMap("贷款产品类别不能为空",contractcode,receiptcode,limitMap, checknumLimit);
                isError = true;
            }
        }


        if(StringUtils.isNotBlank(xclgrdkxx.getGuaranteemethod())){
            if (xclgrdkxx1List.contains("JS0222")){
                //#贷款担保方式
                isError = CheckUtil.checkdkdbfs(xclgrdkxx.getGuaranteemethod(), tempMsg, isError, "贷款担保方式");
            }
        }

        if (xclgrdkxx1List.contains("JS1617")){
            if(StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&xclgrdkxx.getProductcetegory().length()>2){
                String abc = xclgrdkxx.getProductcetegory().substring(0, 3);
//                String abd[] = abc.split(",");
                if (!ArrayUtils.contains( CheckUtil.dklb,abc)) {
                    if (!StringUtils.isNotBlank(xclgrdkxx.getGuaranteemethod())&&StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())) {
                        putMap("当贷款产品类别为资产类贷款时，贷款担保方式不能为空",contractcode,receiptcode,limitMap, checknumLimit);
                        isError = true;
                    }
                }
            }
        }


        //数据日期
//        if(StringUtils.isNotBlank(xcldkxx.getSjrq())){
//            boolean b=CheckUtil.checkDate(xcldkxx.getSjrq(),"yyyy-MM-dd");
//            if(b){
//                if(xcldkxx.getSjrq().compareTo("1900-01-01")<0||xcldkxx.getSjrq().compareTo("2100-12-31")>0){
//                    putMap("数据日期必须满足日期格式为：YYYY-MM-DD且日期范围在1900-01-01到2100-12-31之间",contractcode,receiptcode,limitMap, checknumLimit);
//                    isError = true;
//                }
//
//            }else {
//                putMap("数据日期不符合yyyy-MM-dd格式",contractcode,receiptcode,limitMap, checknumLimit);
//                isError = true;
//            }
//
//        }else {
//            putMap("数据日期" + nullError,contractcode,receiptcode,limitMap, checknumLimit);
//            isError = true;
//        }


        //#贷款到期日期
        //数据日期
//        isError = CheckUtil.nullAndDate(xclgrdkxx.getSjrq(), tempMsg, isError, "数据日期",false);



        if (StringUtils.isNotBlank(xclgrdkxx.getLoanenddate())) {

            boolean b = CheckUtil.checkDate(xclgrdkxx.getLoanenddate(), "yyyy-MM-dd");
            if (b) {
                if (xclgrdkxx.getLoanenddate().compareTo("1800-01-01") >= 0 && xclgrdkxx.getLoanenddate().compareTo("2100-12-31") <= 0) {

                    if (StringUtils.isNotBlank(xclgrdkxx.getLoanstatus()) && xclgrdkxx.getLoanstatus().equals("LS02") && StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())) {
                        if (xclgrdkxx.getLoanenddate().compareTo(xclgrdkxx.getExtensiondate()) >= 0) {
                            if (xclgrdkxx1List.contains("JS2395")){
                                putMap("当贷款展期到期日期不为空和贷款到期日期不为空，且贷款状态为LS02-展期时，贷款到期日期应小于贷款展期到期日期",contractcode,receiptcode,limitMap, checknumLimit);
                                isError = true;
                            }
                        }
                    }

                    if (StringUtils.isNotBlank(xclgrdkxx.getExtensiondate()) && StringUtils.isNotBlank(xclgrdkxx.getLoanstatus()) && "LS04".equals(xclgrdkxx.getLoanstatus())) {
//                        if (xclgrdkxx.getLoanstatus().equals("LS02") && StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())) {
                        if (xclgrdkxx.getLoanenddate().compareTo(xclgrdkxx.getExtensiondate()) <= 0) {
                            if (xclgrdkxx1List.contains("JS2613")){
                                putMap("当贷款展期到期日期和贷款到期日期不为空，且贷款状态为LS04-缩期时，贷款到期日期应大于贷款展期到期日期",contractcode,receiptcode,limitMap, checknumLimit);
                                isError = true;
                            }
                        }
                    }
//                    }
                } else {
                    if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory()) && !ArrayUtils.contains(CheckUtil.tzyw, xclgrdkxx.getProductcetegory())) {
                        if (xclgrdkxx1List.contains("JS0922")) {
                            putMap("除透支类业务以外的贷款到期日期需早于1800-01-01晚于2100-12-31",contractcode,receiptcode,limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                }
            } else {
                if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory()) && !ArrayUtils.contains(CheckUtil.tzyw, xclgrdkxx.getProductcetegory())) {
                    if (xclgrdkxx1List.contains("JS0922")) {
                        putMap("除透支类业务以外的贷款到期日期不符合yyyy-MM-dd格式",contractcode,receiptcode,limitMap, checknumLimit);
                        isError = true;
                    }
                }
            }
        }
        else {
            if (!ArrayUtils.contains(CheckUtil.tzyw,xclgrdkxx.getProductcetegory()) && StringUtils.isNotBlank(xclgrdkxx.getIssupportliveloan()) && !xclgrdkxx.getIssupportliveloan().contains("线上联合消费贷款")){
                if (xclgrdkxx1List.contains("JS1629")){
                    putMap("除透支类业务以外的贷款到期日期" + nullError,contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
        }

        //#贷款定价基准类型
        if (StringUtils.isNotBlank(xclgrdkxx.getFixpricetype())) {
            if (xclgrdkxx.getFixpricetype().length() == 4) {
                if (!ArrayUtils.contains(CheckUtil.loanjzlx, xclgrdkxx.getFixpricetype())) {
                    if (xclgrdkxx1List.contains("JS0220")){
                        putMap("贷款定价基准类型不在符合要求的值域范围内",contractcode,receiptcode,limitMap, checknumLimit);
                        isError = true;
                    }
                }
            } else {
                putMap("贷款定价基准类型长度不等于4",contractcode,receiptcode,limitMap, checknumLimit);
                isError = true;
            }
        } else {
            if (xclgrdkxx1List.contains("JS1636")){
                putMap("贷款定价基准类型" + nullError,contractcode,receiptcode,limitMap, checknumLimit);
                isError = true;
            }
        }
        //#贷款发放日期

        if (StringUtils.isNotBlank(xclgrdkxx.getLoanstartdate())) {
            boolean b1 = CheckUtil.checkDate(xclgrdkxx.getLoanstartdate(), "yyyy-MM-dd");
            if (b1) {
                if (xclgrdkxx.getLoanstartdate().compareTo("1800-01-01") >0 && xclgrdkxx.getLoanstartdate().compareTo("2100-12-31") < 0){

                    if(StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())){
                        if (StringUtils.isNotBlank(xclgrdkxx.getLoanenddate()) && xclgrdkxx.getLoanstartdate().compareTo(xclgrdkxx.getLoanenddate()) <= 0) {

                            if (StringUtils.isNotBlank(xclgrdkxx.getExtensiondate()) &&
                                    xclgrdkxx.getLoanstartdate().compareTo(xclgrdkxx.getExtensiondate()) > 0) {
                                if (xclgrdkxx1List.contains("JS2394"))
                                {
                                    putMap("当贷款展期到期日期不为空时，贷款发放日期应小于等于贷款展期到期日期",contractcode,receiptcode,limitMap, checknumLimit);
                                    isError = true;
                                }
                            }
                        }

                    }
                    if(StringUtils.isNotBlank(xclgrdkxx.getLoanenddate())) {
                        if(xclgrdkxx.getLoanstartdate().compareTo(xclgrdkxx.getLoanenddate())>0) {
                            if (xclgrdkxx1List.contains("JS2393"))
                            {
                                putMap("贷款发放日期不为空时，应小于等于贷款到期日期",contractcode,receiptcode,limitMap, checknumLimit);
                                isError = true;
                            }
                        }
                    }
                    if(StringUtils.isNotBlank(xclgrdkxx.getSjrq())){
                        if(xclgrdkxx.getLoanstartdate().compareTo(xclgrdkxx.getSjrq())>0){
                            if (xclgrdkxx1List.contains("JS2392"))
                            {
                                putMap("贷款发放日期不为空时，应小于等于数据日期",contractcode,receiptcode,limitMap, checknumLimit);
                                isError = true;
                            }
                        }
                    }

                }else {
                    //透支类除外
                    if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory()) && !xclgrdkxx.getProductcetegory().startsWith("F04")){
                        if (xclgrdkxx1List.contains("JS0921")) {
                            putMap("除透支类业务以外的贷款发放日期需早于1800-01-01晚于2100-12-31",contractcode,receiptcode,limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                }

            } else {
                //透支类除外
                if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory()) && xclgrdkxx.getProductcetegory().length()>2 && !xclgrdkxx.getProductcetegory().startsWith("F04")) {
                    if (xclgrdkxx1List.contains("JS0921")) {
                        putMap("除透支类业务以外的贷款发放日期不符合yyyy-MM-dd格式",contractcode,receiptcode,limitMap, checknumLimit);
                        isError = true;
                    }
                }
            }
        }else {
            if (!ArrayUtils.contains(CheckUtil.tzyw,xclgrdkxx.getProductcetegory()) && StringUtils.isNotBlank(xclgrdkxx.getIssupportliveloan()) && !xclgrdkxx.getIssupportliveloan().contains("线上联合消费贷款")){
                if (xclgrdkxx1List.contains("JS1628")){
                    putMap("除透支类业务以外的贷款发放日期" + nullError,contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
        }



        //#贷款借据编码
//        isError = CheckUtil.grantNullAndLengthAndStr2(xcldkxx.getReceiptcode(), 100, tempMsg, isError, "贷款借据编码");
        if (StringUtils.isNotBlank(xclgrdkxx.getReceiptcode())){
            if (CheckUtil.checkStr(xclgrdkxx.getReceiptcode())){
                if (xclgrdkxx1List.contains("JS0905"))
                {
                    putMap("贷款借据编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。",contractcode,receiptcode,limitMap, checknumLimit); isError = true;
                }
            }
            if (xclgrdkxx.getReceiptcode().length()>100){
                if (xclgrdkxx1List.contains("JS0913"))
                {
                    putMap("贷款借据编码字符长度不能超过100",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
        } else {
            if (xclgrdkxx1List.contains("JS1625"))
            {
                putMap("贷款借据编码" +nullError,contractcode,receiptcode,limitMap, checknumLimit);
                isError = true;
            }
        }



        //#贷款余额
        if (StringUtils.isNotBlank(xclgrdkxx.getReceiptbalance())) {
            if (xclgrdkxx.getReceiptbalance().length() > 20 || !CheckUtil.checkDecimal(xclgrdkxx.getReceiptbalance(), 2)) {
                if (xclgrdkxx1List.contains("JS0916"))
                {
                    putMap("贷款余额总长度不能超过20位,小数位应保留2位",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (Double.valueOf(xclgrdkxx.getReceiptbalance()) <= 0) {
                if (xclgrdkxx1List.contains("JS2400"))
                {
                    putMap("贷款余额应大于0",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }

        } else {
            if (xclgrdkxx1List.contains("JS1632")){  putMap("贷款余额" + nullError,contractcode,receiptcode,limitMap, checknumLimit);isError = true;}
        }


        //#贷款余额折人民币
        if (StringUtils.isNotBlank(xclgrdkxx.getReceiptcnybalance())) {
//            贷款余额折人民币一般应在1000元至100亿元之间
//            if (Double.valueOf(xclgrdkxx.getReceiptcnybalance())<1000 || Double.valueOf(xclgrdkxx.getReceiptcnybalance())> 10000000000L){
//                if (xclgrdkxx1List.contains("JS2771"))
//                {
//                    putMap("贷款余额折人民币一般应在1000元至100亿元之间",contractcode,receiptcode,limitMap, checknumLimit);
//                    isError = true;
//                }
//            }

            if (Double.valueOf(xclgrdkxx.getReceiptcnybalance())>100000000){
                if (xclgrdkxx1List.contains("JS2881")){  tempMsg.append("个人贷款的余额折人民币应不超过1亿" + spaceStr);isError = true;}
            }


            if (xclgrdkxx.getReceiptcnybalance().length() > 20 || !CheckUtil.checkDecimal(xclgrdkxx.getReceiptcnybalance(), 2)) {
                if (xclgrdkxx1List.contains("JS0917"))
                {
                    putMap("贷款余额折人民币总长度不能超过20位,小数位应保留2位",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (Double.valueOf(xclgrdkxx.getReceiptcnybalance()) <= 0) {
                if (xclgrdkxx1List.contains("JS2401"))
                {
                    putMap("贷款余额折人民币应大于0",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }

            if (xclgrdkxx.getCurrency().equals("CNY")) {
                if (!xclgrdkxx.getReceiptbalance().equals(xclgrdkxx.getReceiptcnybalance())) {
                    if (xclgrdkxx1List.contains("JS2711"))
                    {
                        putMap("币种为人民币的，贷款余额折人民币应该与贷款余额的值相等",contractcode,receiptcode,limitMap, checknumLimit);
                        isError = true;
                    }
                }
            }
        } else {
            if (xclgrdkxx1List.contains("JS1633")) { putMap("贷款余额折人民币" + nullError,contractcode,receiptcode,limitMap, checknumLimit);isError = true;}

        }
        //#贷款利率重新定价日
        isError = nullAndDateCldkxx2(xclgrdkxx,xclgrdkxx1List,tempMsg, isError, "贷款利率重新定价日", false);


        if (StringUtils.isNotBlank(xclgrdkxx.getLoaninterestrepricedate())) {
            if(StringUtils.isNotBlank(xclgrdkxx.getLoanstartdate())) {
//                if (xclgrdkxx.getLoaninterestrepricedate().compareTo(xclgrdkxx.getLoanstartdate()) < 0) {
//                    if (xclgrdkxx1List.contains("JS2366"))
//                    {
//                        putMap("贷款利率重新定价日应大于等于贷款发放日期",contractcode,receiptcode,limitMap, checknumLimit);isError = true;
//                    }
//                }
                if (StringUtils.isNotBlank(xclgrdkxx.getExtensiondate()) && "RF02".equals(xclgrdkxx.getInterestisfixed())) {
                    if (xclgrdkxx.getLoaninterestrepricedate().compareTo(xclgrdkxx.getExtensiondate()) > 0) {
                        if (xclgrdkxx1List.contains("JS2396"))
                        {
                            putMap("当贷款展期到期日期不为空和贷款利率重新定价日不为空且为浮动利率贷款时，贷款利率重新定价日应小于等于贷款展期到期日期",contractcode,receiptcode,limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                }
                if ((!StringUtils.isNotBlank(xclgrdkxx.getExtensiondate()))&& "RF02".equals(xclgrdkxx.getInterestisfixed())) {
                    if (xclgrdkxx.getLoaninterestrepricedate().compareTo(xclgrdkxx.getLoanenddate()) > 0) {
                        if (xclgrdkxx1List.contains("JS2398"))
                        {
                            putMap("当贷款展期到期日期为空，贷款利率重新定价日不为空且为浮动利率贷款时，贷款利率重新定价日应小于等于贷款到期日期",contractcode,receiptcode,limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                }
                if (StringUtils.isNotBlank(xclgrdkxx.getExtensiondate()) && "RF01".equals(xclgrdkxx.getInterestisfixed())) {
                    if (!xclgrdkxx.getLoaninterestrepricedate().equals(xclgrdkxx.getExtensiondate())) {
                        if (xclgrdkxx1List.contains("JS2397"))
                        {
                            putMap("当贷款展期到期日期不为空和贷款利率重新定价日不为空且为固定利率贷款时，贷款利率重新定价日应等于贷款展期到期日期",contractcode,receiptcode,limitMap, checknumLimit);
                            isError = true;
                        }
                    }

                }
                if (!StringUtils.isNotBlank(xclgrdkxx.getExtensiondate()) && "RF01".equals(xclgrdkxx.getInterestisfixed())) {
                    if (!xclgrdkxx.getLoaninterestrepricedate().equals(xclgrdkxx.getLoanenddate())) {
                        if (xclgrdkxx1List.contains("JS2399"))
                        {
                            putMap("当贷款展期到期日期为空，贷款利率重新定价日不为空且为固定利率贷款时，贷款利率重新定价日应等于贷款到期日期",contractcode,receiptcode,limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                }

            }
        }




        //#贷款展期到期日期 ========================待定  因为找不到资产负债类型字段
        if (StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())) {
            boolean b = CheckUtil.checkDate(xclgrdkxx.getExtensiondate(), "yyyy-MM-dd");
            if (b) {
                if (xclgrdkxx.getExtensiondate().compareTo("1800-01-01") >= 0 && xclgrdkxx.getExtensiondate().compareTo("2100-12-31") <= 0) {

                } else {
                    if (xclgrdkxx1List.contains("JS0923")){putMap("贷款展期到期日期需晚于1800-01-01早于2100-12-31",contractcode,receiptcode,limitMap, checknumLimit);isError = true;}
                }
            } else {
                if (xclgrdkxx1List.contains("JS0923")){ putMap("贷款展期到期日期不符合yyyy-MM-dd格式",contractcode,receiptcode,limitMap, checknumLimit);isError = true;}
            }

        } else {
            if ("LS02".equals(xclgrdkxx.getLoanstatus())){
                if(StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&xclgrdkxx.getProductcetegory().length()>2){
                    String ccd = xclgrdkxx.getProductcetegory().substring(0, 3);
//                    String[] cce = ccd.split(",");
                    if(!ArrayUtils.contains(CheckUtil.dklb,ccd)){
                        if(!StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())){
                            if (xclgrdkxx1List.contains("JS1615"))
                            {
                                putMap("当贷款状态为展期时，贷款展期到期日期不能为空",contractcode,receiptcode,limitMap, checknumLimit);
                                isError = true;
                            }
                        }
                    }
                }
            }
        }


        //#贷款质量

        if (StringUtils.isNotBlank(xclgrdkxx.getLoanquality())) {

            if (!ArrayUtils.contains(CheckUtil.dkzl, xclgrdkxx.getLoanquality())) {
                if (xclgrdkxx1List.contains("JS0224")){  putMap("贷款质量不符合要求的值域范围内",contractcode,receiptcode,limitMap, checknumLimit);isError = true; }
            }


        } else if(StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&xclgrdkxx.getProductcetegory().length()>2){
            String sbr = xclgrdkxx.getProductcetegory().substring(0, 3);
//            String[] sbs = sbr.split(",");
            if (!ArrayUtils.contains(CheckUtil.dklb,sbr)) {
                if (!StringUtils.isNotBlank(xclgrdkxx.getLoanquality())) {
                    if (xclgrdkxx1List.contains("JS1630"))
                    {
                        putMap("当贷款产品类别为资产类贷款时，贷款质量不能为空",contractcode,receiptcode,limitMap, checknumLimit);
                        isError = true;
                    }
                }
            }

        } else {
//            putMap("贷款质量" + nullError,contractcode,receiptcode,limitMap, checknumLimit);
//            isError = true;
        }




        //#贷款状态
        if (StringUtils.isNotBlank(xclgrdkxx.getLoanstatus())) {
            if (xclgrdkxx.getLoanstatus().length() == 4) {
                if (!ArrayUtils.contains(CheckUtil.loanstatus, xclgrdkxx.getLoanstatus())) {
                    if (xclgrdkxx1List.contains("JS0225"))
                    {
                        putMap("贷款状态不符合要求的值域范围内",contractcode,receiptcode,limitMap, checknumLimit);
                        isError = true;
                    }
                }
            } else {
                putMap("贷款状态长度不等于4",contractcode,receiptcode,limitMap, checknumLimit);
                isError = true;
            }


            if(StringUtils.isNotBlank(xclgrdkxx.getSjrq())){
                if(StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())){
                    if(xclgrdkxx.getSjrq().compareTo(xclgrdkxx.getExtensiondate())>=1){
                        if(!"LS03".equals(xclgrdkxx.getLoanstatus())){
                            if (xclgrdkxx1List.contains("JS2188"))
                            {
                                putMap("逾期贷款的贷款状态应该为LS03-逾期",contractcode,receiptcode,limitMap, checknumLimit);
                                isError = true;
                            }
                        }
                    }
                }
                else if(StringUtils.isNotBlank(xclgrdkxx.getLoanenddate())){
                    if(xclgrdkxx.getSjrq().compareTo(xclgrdkxx.getLoanenddate())>=1){
                        if(!"LS03".equals(xclgrdkxx.getLoanstatus())){
                            putMap("逾期贷款的贷款状态应该为LS03-逾期",contractcode,receiptcode,limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                }
            }

            if(StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())){
//                if(xcldkxx.getExtensiondate().compareTo(xcldkxx.getSjrq())>=0){
                if(!"LS02".equals(xclgrdkxx.getLoanstatus()) && !"LS04".equals(xclgrdkxx.getLoanstatus()) && !"LS03".equals(xclgrdkxx.getLoanstatus())){
                    if (xclgrdkxx1List.contains("JS2189"))
                    {
                        putMap("展期贷款的贷款状态应该为LS02-展期 或者 LS04-缩期 或者LS03-逾期",contractcode,receiptcode,limitMap, checknumLimit);
                        isError = true;
                    }
                }
//                }
            }
            if(!StringUtils.isNotBlank(xclgrdkxx.getExtensiondate())){
                if("LS02".equals(xclgrdkxx.getLoanstatus())){
                    if (xclgrdkxx1List.contains("JS2200"))
                    {
                        putMap("如果贷款展期日期为空，则贷款状态不能为LS02-展期",contractcode,receiptcode,limitMap, checknumLimit);
                        isError = true;
                    }
                }
            }
            if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&xclgrdkxx.getIsfarmerloan().startsWith("F04")){
                if (!"LS01".equals(xclgrdkxx.getLoanstatus()) || !"LS03".equals(xclgrdkxx.getLoanstatus())){
                    if (xclgrdkxx1List.contains("JS2800"))
                    {
                        putMap("信用卡透支的贷款状态只能为LS01-正常或LS03-逾期",contractcode,receiptcode,limitMap, checknumLimit);
                        isError = true;
                    }
                }
            }
        }else {
            if (xclgrdkxx1List.contains("JS1642"))
            {
                putMap("贷款状态" + nullError,contractcode,receiptcode,limitMap, checknumLimit);
                isError = true;
            }
        }



        //基准利率
        if (StringUtils.isNotBlank(xclgrdkxx.getBaseinterest())) {
            if (xclgrdkxx.getBaseinterest().length() > 10 || !CheckUtil.checkDecimal(xclgrdkxx.getBaseinterest(), 5)) {
                if (xclgrdkxx1List.contains("JS0919"))
                {
                    putMap("当基准利率不为空时，基准利率总长度不能超过10位，小数位必须为5位",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (CheckUtil.checkPerSign(xclgrdkxx.getInterestisfixed())) {
                if (xclgrdkxx1List.contains("JS0908")) {    putMap("当基准利率不为空时，基准利率不能包含‰或%",contractcode,receiptcode,limitMap, checknumLimit);isError = true;}
            }

        }

        if ("RF02".equals(xclgrdkxx.getInterestisfixed()))
        {
            if (StringUtils.isNotBlank(xclgrdkxx.getBaseinterest())) {
                if ((new BigDecimal(xclgrdkxx.getBaseinterest()).compareTo(BigDecimal.ZERO) < 0 || new BigDecimal(xclgrdkxx.getBaseinterest()).compareTo(new BigDecimal("30")) > 0)) {
                    if (xclgrdkxx1List.contains("JS2403"))
                    {
                        putMap("当为浮动利率贷款时，基准利率应大于等于0且小于等于30",contractcode,receiptcode,limitMap, checknumLimit);
                        isError = true;
                    }
                }
            } else {
                if (xclgrdkxx1List.contains("JS1637")) { putMap("当为浮动利率贷款时，基准利率不能为空",contractcode,receiptcode,limitMap, checknumLimit);isError = true;}

            }

        } else if ("RF01".equals(xclgrdkxx.getInterestisfixed())) {
            if (StringUtils.isNotBlank(xclgrdkxx.getBaseinterest())) {
                if (xclgrdkxx1List.contains("JS2159")) { putMap("利率类型为固定利率的，基准利率必须为空",contractcode,receiptcode,limitMap, checknumLimit);isError = true; }

            }

        }

        //#借款人地区代码
        if (StringUtils.isNotBlank(xclgrdkxx.getBrrowerareacode()))
        {

//            if (xclgrdkxx.getBrrowerareacode().length() == 6) {
                if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
                if (countryList == null) countryList = customSqlUtil.getBaseCode(BaseCountry.class);
                if (!areaList.contains(xclgrdkxx.getBrrowerareacode()) &&
                        !countryList.contains(xclgrdkxx.getBrrowerareacode())) {
                    if (xclgrdkxx1List.contains("JS0216")) {  putMap("借款人地区代码不在符合要求的值域范围内",contractcode,receiptcode,limitMap, checknumLimit);isError = true;}
                }
//            } else {
//                putMap("借款人地区代码长度不等于6",contractcode,receiptcode,limitMap, checknumLimit);
//                isError = true;
//            }
        }
        else {
            if (xclgrdkxx1List.contains("JS1624")) { putMap("借款人地区代码" + nullError,contractcode,receiptcode,limitMap, checknumLimit);isError = true; }
        }


        //#借款人证件代码
//        isError = CheckUtil.grantNullAndLengthAndStr2(xcldkxx.getBrroweridnum(), 60, tempMsg, isError, "借款人证件代码");
        if (StringUtils.isNotBlank(xclgrdkxx.getBrroweridnum())){
            if (xclgrdkxx.getBrroweridnum().length()<=60){
                if (CheckUtil.checkStr(xclgrdkxx.getBrroweridnum())){
                    if (xclgrdkxx1List.contains("JS0904"))
                    {
                        putMap("借款人证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。",contractcode,receiptcode,limitMap, checknumLimit);isError=true;
                    }
                }
            }else {
                putMap("借款人证件代码字段超过限制长度60|",contractcode,receiptcode,limitMap, checknumLimit);isError=true;
            }
        }else {
            if (xclgrdkxx1List.contains("JS1623")) {  putMap("借款人证件代码不能为空",contractcode,receiptcode,limitMap, checknumLimit);isError = true;}
        }

        if (StringUtils.isNotBlank(xclgrdkxx.getBrroweridnum())) {
            if (xclgrdkxx.getBrroweridnum().length() > 60) {
                if (xclgrdkxx1List.contains("JS0912"))
                {
                    putMap("借款人证件代码字符长度不能超过60",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
        }

        //#查重在外层写#end
        //#内部机构号
//        isError = CheckUtil.grantNullAndLengthAndStr2(xcldkxx.getFinanceorginnum(), 30, tempMsg, isError, "内部机构号");
        if (StringUtils.isNotBlank(xclgrdkxx.getFinanceorginnum())){
            if (CheckUtil.checkStr(xclgrdkxx.getFinanceorginnum())){
                if (xclgrdkxx1List.contains("JS0903"))
                {
                    putMap("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (xclgrdkxx.getFinanceorginnum().length()>30){
                if (xclgrdkxx1List.contains("JS0911"))
                {
                    putMap("内部机构号字符长度不能超过30",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }

        }else {
            if (xclgrdkxx1List.contains("JS1620")){putMap("内部机构号不能为空",contractcode,receiptcode,limitMap, checknumLimit);isError = true;}
        }

        //#利率是否固定
        if (StringUtils.isNotBlank(xclgrdkxx.getInterestisfixed())){
            if (xclgrdkxx1List.contains("JS0219"))
            {
                isError = CheckUtil.equalNullAndCode(xclgrdkxx.getInterestisfixed(), 4, Arrays.asList(CheckUtil.llsfgd), tempMsg, isError, "利率是否固定");
            }
        }else {
            if (xclgrdkxx1List.contains("JS1634"))
            {
                putMap("利率是否固定不能为空",contractcode,receiptcode,limitMap, checknumLimit);
                isError = true;
            }
        }


        //#利率水平
        if (StringUtils.isNotBlank(xclgrdkxx.getInterestislevel())) {
            if (CheckUtil.checkPerSign(xclgrdkxx.getInterestislevel())) {
                if (xclgrdkxx1List.contains("JS0907"))
                {
                    putMap("利率水平不能包含‰或%",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (!CheckUtil.checkPerSign(xclgrdkxx.getInterestislevel())){}
            if (new BigDecimal(xclgrdkxx.getInterestislevel()).compareTo(BigDecimal.ZERO) < 0||new BigDecimal(xclgrdkxx .getInterestislevel()).compareTo(new BigDecimal("30")) >0) {
                if (xclgrdkxx1List.contains("JS2402"))
                {
                    putMap("利率水平应大于等于0且小于等于30",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (xclgrdkxx.getInterestislevel().length() <= 10 && CheckUtil.checkDecimal(xclgrdkxx.getInterestislevel(), 5)) {


            } else {
                if (xclgrdkxx1List.contains("JS0918"))
                {
                    putMap("利率水平总长度不能超过10位，小数位必须为5位",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }

        } else {
            if (xclgrdkxx1List.contains("JS1635"))
            {
                putMap("利率水平" + nullError,contractcode,receiptcode,limitMap, checknumLimit);
                isError = true;
            }
        }

        //#贷款用途
        if (StringUtils.isNotBlank(xclgrdkxx.getIssupportliveloan())) {
            if (CheckUtil.checkStr6(xclgrdkxx.getIssupportliveloan())) {
                if (xclgrdkxx1List.contains("JS0909"))
                {
                    putMap("当贷款用途不为空时，贷款用途字段内容中不得出现“？”、“！” 、“^”  。其中“？”和“！”包含全角和半角两种格式。",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (xclgrdkxx.getIssupportliveloan().length() > 3000) {
                if (xclgrdkxx1List.contains("JS0915"))
                {
                    putMap("当贷款用途不为空时，贷款用途字符长度不能超过3000",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&xclgrdkxx.getProductcetegory().length()>2) {
                String qwe = xclgrdkxx.getProductcetegory().substring(0, 3);
//                String Qwe[] = qwe.split(",");
                if (ArrayUtils.contains(CheckUtil.dklb,qwe)) {
                    if(!StringUtils.isNotBlank(xclgrdkxx.getIssupportliveloan())){
                        if (xclgrdkxx1List.contains("JS1639"))
                        {
                            putMap("当贷款产品类别为资产类贷款时，贷款用途不能为空",contractcode,receiptcode,limitMap, checknumLimit);
                            isError = true;
                        }
                    }

                }
            }

        }




        //#是否首次贷款
        isError = equalNullAndCode(xclgrdkxx.getIsplatformloan(), xclgrdkxx1List,1, Arrays.asList(CheckUtil.sfcommon), tempMsg, isError, "是否首次贷款");

        //#借款人证件类型
        if (StringUtils.isNotBlank(xclgrdkxx.getIsfarmerloan())) {
            if (xclgrdkxx.getIsfarmerloan().equals("A01")&&StringUtils.isNotBlank(xclgrdkxx.getBrroweridnum())&&xclgrdkxx.getBrroweridnum().length() != 18){
                if (xclgrdkxx1List.contains("JS2174")){ putMap("借款人证件类型为统一社会信用代码的，证件代码长度必须为18位",contractcode,receiptcode,limitMap, checknumLimit); isError = true; }
            }


            if (xclgrdkxx.getIsfarmerloan().startsWith("B")){
                if (xclgrdkxx.getIsfarmerloan().equals("B01") || xclgrdkxx.getIsfarmerloan().equals("B08")){
                    if (StringUtils.isNotBlank(xclgrdkxx.getBrroweridnum())&&TuoMinUtils.getB01(xclgrdkxx.getBrroweridnum()).length() != 46){
                        if (xclgrdkxx1List.contains("JS2655")){  putMap("借款人证件类型为B01-身份证或者B08-临时身份证的，借款人证件代码脱敏后的长度应该为46",contractcode,receiptcode,limitMap, checknumLimit);isError = true;}
                    }
                }else {
                    if (StringUtils.isNotBlank(xclgrdkxx.getBrroweridnum())&&TuoMinUtils.getB01otr(xclgrdkxx.getBrroweridnum()).length() != 32){
                        if (xclgrdkxx1List.contains("JS2665")){ putMap("借款人证件类型为B开头的个人证件且不是B01-身份证和B08-临时身份证的，借款人证件代码脱敏后的长度应该为32",contractcode,receiptcode,limitMap, checknumLimit);isError=true;}
                    }
                }
            }
//            441202197603061000
            if (xclgrdkxx.getIsfarmerloan().equals("B01")&&StringUtils.isNotBlank(xclgrdkxx.getBrroweridnum())){
                if (StringUtils.isNotBlank(xclgrdkxx.getSjrq())&&xclgrdkxx.getBrroweridnum().substring(6,14).compareTo(xclgrdkxx.getSjrq().replace("-","")) >=0){
                    if (xclgrdkxx1List.contains("JS2675")){
                        putMap("证件类型为B01-身份证的，第7-14位的截取结果应该<数据日期",contractcode,receiptcode,limitMap, checknumLimit);isError=true;
                    }

                }
            }


            //TODO
            if (!ArrayUtils.contains(CheckUtil.grzjlx, xclgrdkxx.getIsfarmerloan())) {
                if (xclgrdkxx1List.contains("JS0214")){
                    putMap("借款人证件类型不在符合要求的最底层值域范围内",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
        } else {
            if (xclgrdkxx1List.contains("JS1622")){ putMap("借款人证件类型不能为空",contractcode,receiptcode,limitMap, checknumLimit);isError = true;}
        }
        //#逾期类型  ========================待定  条件冲突
        if (StringUtils.isNotBlank(xclgrdkxx.getLoanstatus()) && "LS03".equals(xclgrdkxx.getLoanstatus())) {
            if (StringUtils.isNotBlank(xclgrdkxx.getOverduetype())) {
                CheckUtil.equalNullAndCode(xclgrdkxx.getOverduetype(), 2, Arrays.asList(CheckUtil.jylx), tempMsg, isError, "借款人证件类型");
            } else {
                if (xclgrdkxx1List.contains("JS1643"))
                {
                    putMap("当贷款状态为逾期时，逾期类型不能为空",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
        } else {
            if (StringUtils.isNotBlank(xclgrdkxx.getOverduetype())) {
                if (xclgrdkxx1List.contains("JS0226")){
                    CheckUtil.equalNullAndCode(xclgrdkxx.getOverduetype(), 2, Arrays.asList(CheckUtil.jylx), tempMsg, isError, "逾期类型");
                }

            }
        }
        //透支业务
        if (StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&xclgrdkxx.getProductcetegory().startsWith("F04")){
            if (!StringUtils.isNotBlank(xclgrdkxx.getLoanstartdate())){
                if (xclgrdkxx1List.contains("JS2808"))
                {
                    putMap("透支类业务的贷款发放日期应为空",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (!StringUtils.isNotBlank(xclgrdkxx.getLoanenddate())){
                if (xclgrdkxx1List.contains("JS2809"))
                {
                    putMap("透支类业务的贷款到期日期应为空",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (!StringUtils.isNotBlank(xclgrdkxx.getLoaninterestrepricedate())){
                if (xclgrdkxx1List.contains("JS2810"))
                {
                    putMap("透支类业务的贷款利率重新定价日应为空",contractcode,receiptcode,limitMap, checknumLimit);
                    isError = true;
                }
            }
        }


        if (isError || errorId0.contains(xclgrdkxx.getId())) {
            errorId.add(xclgrdkxx.getId().toString());
            return 1;
        } else {
            if (!errorId.contains(xclgrdkxx.getId().toString())) {
                rightId.add(xclgrdkxx.getId().toString());
            }
            return 0;
        }


    }
    

    //个人贷款发生额信息
    public static int checkXgrdkfsxxLimit(CustomSqlUtil customSqlUtil, Xgrdkfsxx xgrdkfsxx,
                                          List<Integer> errorId0, List<String> errorId, List<String> rightId,
                                          List<String> checknumList, Map<String,String> limitMap, int checknumLimit) {
        boolean isError = false;
        StringBuffer tempMsg = new StringBuffer("");

        String nullError = "不能为空";
        String lengthError = "超过限制长度";
        String spaceStr = "|";
        String loancontractcode = xgrdkfsxx.getLoancontractcode();
        String loanbrowcode = xgrdkfsxx.getLoanbrowcode();


        //#贷款币种
        if (currencyList==null) currencyList = customSqlUtil.getBaseCode(BaseCurrency.class);

        if (StringUtils.isNotBlank(xgrdkfsxx.getLoancurrency())){
            if (checknumList.contains("JS0231"))
            {
                isError = CheckUtil.checkCurrency(xgrdkfsxx.getLoancurrency(), currencyList, customSqlUtil, tempMsg, isError, "贷款币种");
            }

        }else {
            if (checknumList.contains("JS1599")){ putMap("贷款币种不能为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true; }
        }


        //#贷款财政扶持方式
        if (checknumList.contains("JS0234")){
            isError = CheckUtil.checkLoanczfcfs(xgrdkfsxx.getLoanfinancesupport(), tempMsg, isError, "贷款财政扶持方式");
        }

        //#贷款产品类别
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())) {
            if(!ArrayUtils.contains(CheckUtil.dkfsecplb, xgrdkfsxx.getLoanprocode())) {
                if (checknumList.contains("JS0230"))
                {
                    putMap("贷款产品类别不在符合要求的值域范围内" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (xgrdkfsxx.getLoanprocode().equals("F03")){
                if (checknumList.contains("JS2197"))
                {
                    putMap("贷款产品不能出现F03拆借" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1595"))
            {
                putMap("贷款产品类别" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                isError = true;
            }
        }
        //#贷款担保方式
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&ArrayUtils.contains(CheckUtil.dkfsecplb, xgrdkfsxx.getLoanprocode())) {
            String temp = xgrdkfsxx.getLoanprocode().substring(0, 3);
            if(!"F01".equals(temp)&&!"F03".equals(temp)&&!"F06".equals(temp)&&!"F07".equals(temp)) {
                if (StringUtils.isNotBlank(xgrdkfsxx.getGteemethod())){
                    if(!ArrayUtils.contains(CheckUtil.dkdbfs, xgrdkfsxx.getGteemethod())) {
                        if (checknumList.contains("JS0235"))
                        {
                            putMap("贷款担保方式不在符合要求的值域范围内" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                }else {
                    if (checknumList.contains("JS1536"))
                    {
                        putMap("当贷款产品类别为资产类贷款时，贷款担保方式"+nullError, loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError=true;
                    }
                }
            }
        }
        //#贷款到期日期
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanenddate())&&StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&!xgrdkfsxx.getLoanprocode().startsWith("F04")) {
            if (xgrdkfsxx.getLoanenddate().length() == 10) {
                boolean b = CheckUtil.checkDate(xgrdkfsxx.getLoanenddate(), "yyyy-MM-dd");
                if (b) {
                    if (xgrdkfsxx.getLoanenddate().compareTo("1800-01-01") < 0 || xgrdkfsxx.getLoanenddate().compareTo("2100-12-31") > 0) {
                        if (checknumList.contains("JS0948")) {
                            putMap("除透支类业务以外的贷款到期日期需晚于1800-01-01早于2100-12-31" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                } else {
                    if (checknumList.contains("JS0948")) {
                        putMap("除透支类业务以外的贷款到期日期不符合yyyy-MM-dd格式" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError = true;
                    }

                }
            }
        } else {
            if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&!xgrdkfsxx.getLoanprocode().startsWith("F04") && StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan()) && !xgrdkfsxx.getIssupportliveloan().contains("线上联合消费贷款")){
                if (checknumList.contains("JS1597")) { putMap("除透支类业务以外的贷款到期日期" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;}
            }
        }

        //#贷款定价基准类型
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanfixamttype())) {
            if (!ArrayUtils.contains(CheckUtil.loanjzlx, xgrdkfsxx.getLoanfixamttype())) {
                if (checknumList.contains("JS0233"))
                {
                    putMap("贷款定价基准类型不在符合要求的值域范围内" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1604"))
            {
                putMap("贷款定价基准类型" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                isError = true;
            }
        }

        //#贷款发放日期
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanstartdate())&&StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&!xgrdkfsxx.getLoanprocode().startsWith("F04")) {
            if (xgrdkfsxx.getLoanstartdate().length() == 10) {
                boolean b1 = CheckUtil.checkDate(xgrdkfsxx.getLoanstartdate(), "yyyy-MM-dd");
                if (b1) {
                    if (xgrdkfsxx.getLoanstartdate().compareTo("1800-01-01") >= 0 && xgrdkfsxx.getLoanstartdate().compareTo("2100-12-31") <= 0) {
                        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanenddate()) &&
                                xgrdkfsxx.getLoanstartdate().compareTo(xgrdkfsxx.getLoanenddate()) > 0) {
                            if (checknumList.contains("JS2383"))
                            {
                                putMap("贷款发放日期不为空时，应小于等于贷款到期日期" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                                isError = true;
                            }
                        }
                        if(StringUtils.isNotBlank(xgrdkfsxx.getSjrq()) &&
                                xgrdkfsxx.getLoanstartdate().compareTo(xgrdkfsxx.getSjrq()) > 0) {
                            if (checknumList.contains("JS2382"))
                            {
                                putMap("贷款发放日期不为空时，应小于等于数据日期" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                                isError = true;
                            }
                        }
                        if("1".equals(xgrdkfsxx.getGivetakeid())) {
                            if(StringUtils.isNotBlank(xgrdkfsxx.getSjrq()) && CheckUtil.checkDate(xgrdkfsxx.getSjrq(), "yyyy-MM-dd")) {
                                if(!xgrdkfsxx.getLoanstartdate().substring(0,7).equals(xgrdkfsxx.getSjrq().substring(0,7))&&!xgrdkfsxx.getLoanstatus().equals("LF04")&&!xgrdkfsxx.getLoanprocode().startsWith("F04") && StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan()) && !xgrdkfsxx.getIssupportliveloan().contains("线上联合消费贷款")) {
                                    if (checknumList.contains("JS2561"))
                                    {
                                        putMap("当月发放的贷款且不是转入贷款的，贷款发放日期应该在当月范围内（透支类和、线上联合消费贷款类贷款除外）" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                                        isError = true;
                                    }
                                }
                            }
                        }
                    } else {
                        if (checknumList.contains("JS0947")) {
                            putMap("除透支类业务以外的贷款发放日期需晚于1800-01-01早于2100-12-31" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                } else {
                    if (checknumList.contains("JS0947")) {
                        putMap("除透支类业务以外的贷款发放日期不符合yyyy-MM-dd格式" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError = true;
                    }

                }
            } else {
//                if (checknumList.contains("JS0335")) { putMap("贷款发放日期长度不等于10" , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;}
            }
        } else {
            if (!ArrayUtils.contains(CheckUtil.tzyw,xgrdkfsxx.getLoanprocode()) && StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan()) && !xgrdkfsxx.getIssupportliveloan().contains("线上联合消费贷款")){
                if (checknumList.contains("JS1596")) {  putMap("除透支类业务以外的贷款发放日期" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;}
            }
        }

        //#贷款发生金额
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanamt())) {
            if (xgrdkfsxx.getLoanamt().length() > 20 || !CheckUtil.checkDecimal(xgrdkfsxx.getLoanamt(), 2)) {
                if (checknumList.contains("JS0942"))
                {
                    putMap("贷款发生金额总长度不能超过20位,小数位应保留2位" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
            if(new BigDecimal(xgrdkfsxx.getLoanamt()).compareTo(BigDecimal.ZERO)<=0) {
                if (checknumList.contains("JS2385"))
                {
                    putMap("贷款发生金额应大于0" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1600"))
            {
                putMap("贷款发生金额" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                isError = true;
            }
        }
        //#贷款发生金额折人民币
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoancnyamt())) {
            if (xgrdkfsxx.getLoancnyamt().length() > 20 || !CheckUtil.checkDecimal(xgrdkfsxx.getLoancnyamt(), 2)) {
                if (checknumList.contains("JS0943"))
                {
                    putMap("贷款发生金额折人民币总长度不能超过20位,小数位应保留2位" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }

            if (Double.valueOf(xgrdkfsxx.getLoancnyamt())>100000000){
                if (checknumList.contains("JS2882"))
                {
                    tempMsg.append("个人贷款的发生金额折人民币应不超过1亿" + spaceStr);
                    isError = true;
                }
            }


            if(new BigDecimal(xgrdkfsxx.getLoancnyamt()).compareTo(BigDecimal.ZERO)<=0) {
                if (checknumList.contains("JS2386"))
                {
                    putMap("贷款发生金额折人民币应大于0" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
            if("CNY".equals(xgrdkfsxx.getLoancurrency())){
                if (StringUtils.isNotBlank(xgrdkfsxx.getLoanamt())) {
                    if(!xgrdkfsxx.getLoanamt().equals(xgrdkfsxx.getLoancnyamt())) {
                        if (checknumList.contains("JS2712"))
                        {
                            putMap("币种为人民币的，贷款发生金额折人民币应该与贷款发生金额的值相等" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                }else {
                    if (checknumList.contains("JS2712"))
                    {
                        putMap("币种为人民币的，贷款发生金额折人民币应该与贷款发生金额的值相等" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError = true;
                    }
                }
            }
        } else {
            if (checknumList.contains("JS1601"))
            {
                putMap("贷款发生金额折人民币" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                isError = true;
            }
        }
        //#贷款合同编码
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoancontractcode())){
            if (xgrdkfsxx.getLoancontractcode().length()<=100){
                if (checkStr2(xgrdkfsxx.getLoancontractcode())){
                    if (checknumList.contains("JS0930"))
                    {
                        putMap("贷款合同编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。", loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS0939")) {  putMap("贷款合同编码字符长度不能超过100", loancontractcode,loanbrowcode, limitMap, checknumLimit);isError=true;}

            }
        }else {
            if (checknumList.contains("JS1594")) {   putMap("贷款合同编码不能为空" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;}
        }

        //#贷款借据编码
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanbrowcode())){
            if (xgrdkfsxx.getLoanbrowcode().length()<=100){
                if (checkStr2(xgrdkfsxx.getLoanbrowcode())){
                    if (checknumList.contains("JS0929"))
                    {
                        putMap("贷款借据编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。", loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS0938")) {  putMap("贷款借据编码字符长度不能超过100", loancontractcode,loanbrowcode, limitMap, checknumLimit);isError=true;}

            }
        }else {
            if (checknumList.contains("JS1593")) {   putMap("贷款借据编码不能为空" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;}
        }


        //#贷款利率重新定价日
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanraterepricedate())) {
            boolean b = CheckUtil.checkDate(xgrdkfsxx.getLoanraterepricedate(), "yyyy-MM-dd");
            if (b) {
                if (xgrdkfsxx.getLoanraterepricedate().compareTo("1800-01-01") < 0 || xgrdkfsxx.getLoanraterepricedate().compareTo("2100-12-31") > 0) {
                    //排除透支业务
                    if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode()) && !xgrdkfsxx.getLoanprocode().startsWith("F04")) {
                        if (checknumList.contains("JS0950")) {
                            putMap("除透支类业务以外的贷款利率重新定价日需晚于1800-01-01早于2100-12-31" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                }
                if(StringUtils.isNotBlank(xgrdkfsxx.getLoanstartdate())) {
                    if(xgrdkfsxx.getLoanraterepricedate().compareTo(xgrdkfsxx.getLoanstartdate()) < 0) {
                        if (checknumList.contains("JS2389"))
                        {
                            putMap("贷款利率重新定价日不为空时，应大于等于贷款发放日期" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                }
            } else {
                //排除透支业务
                if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode()) &&!xgrdkfsxx.getLoanprocode().startsWith("F04")) {
                    if (checknumList.contains("JS0950")) {
                        putMap("除透支类业务以外的贷款利率重新定价日不符合yyyy-MM-dd格式" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError = true;
                    }

                }
            }
        } else {
            if (!ArrayUtils.contains(CheckUtil.tzyw,xgrdkfsxx.getLoanprocode()) && StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan())&& !xgrdkfsxx.getIssupportliveloan().contains("线上联合消费贷款")){
                if (checknumList.contains("JS1606")) {  putMap("除透支类业务以外的贷款利率重新定价日" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;  }
            }
        }

        //#贷款实际终止日期  //排除透支业务
        if (StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan())&&!xgrdkfsxx.getIssupportliveloan().contains("线上联合消费贷款")&&!ArrayUtils.contains(CheckUtil.tzyw,xgrdkfsxx.getLoanprocode())&&"0".equals(xgrdkfsxx.getGivetakeid())&&((StringUtils.isNotBlank(xgrdkfsxx.getLoanstatus()) && ArrayUtils.contains(CheckUtil.loanstatus4, xgrdkfsxx.getLoanstatus()))||("LF05".equals(xgrdkfsxx.getLoanstatus())&&("01".equals(xgrdkfsxx.getLoanrestructuring())||"02".equals(xgrdkfsxx.getLoanrestructuring()))))) {
            if (StringUtils.isNotBlank(xgrdkfsxx.getLoanactenddate())) {
                if (xgrdkfsxx.getLoanactenddate().length() == 10) {
                    boolean b1 = CheckUtil.checkDate(xgrdkfsxx.getLoanactenddate(), "yyyy-MM-dd");
                    if (b1) {
                        if (xgrdkfsxx.getLoanactenddate().compareTo("1800-01-01") >= 0 && xgrdkfsxx.getLoanactenddate().compareTo("2100-12-31") <= 0) {
                            if (StringUtils.isNotBlank(xgrdkfsxx.getLoanstartdate()) &&
                                    xgrdkfsxx.getLoanstartdate().compareTo(xgrdkfsxx.getLoanenddate()) > 0) {
                                if (checknumList.contains("JS2384"))
                                {
                                    putMap("当贷款实际终止日期不为空时，贷款实际终止日期应大于等于贷款发放日期" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                                    isError = true;
                                }
                            }
                        } else {
                            if (checknumList.contains("JS0949")) { putMap("贷款实际终止日期需晚于1800-01-01早于2100-12-31" , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;}
                        }
                    } else {
                        if (checknumList.contains("JS0949")) {putMap("贷款实际终止日期不符合yyyy-MM-dd格式" , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;}
                    }
                } else {
//                    if (checknumList.contains("JS0337")) {  putMap("贷款实际终止日期长度不等于10" , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;}
                }
            } else {
                if (checknumList.contains("JS1535")) { putMap("除透支类业务以外的贷款，当贷款状态为核销、剥离、转让、重组、以物抵债、债转股且发放/收回标识为收回时，贷款实际终止日期" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;}

            }
        }


        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanactenddate())){
//            Calendar cal = Calendar.getInstance();
//            int year = cal.get(Calendar.YEAR);
//            int month = cal.get(Calendar.MONTH) + 1;
//            String monthStr;
//            if(month<10) {
//                monthStr = year+"-0"+month;
//            }else {
//                monthStr = year+"-"+month;
//            }
            if (StringUtils.isNotBlank(xgrdkfsxx.getSjrq()) &&
                    (xgrdkfsxx.getLoanactenddate().compareTo(xgrdkfsxx.getSjrq()) > 0) ||!xgrdkfsxx.getLoanactenddate().substring(0,7).equals(xgrdkfsxx.getSjrq().substring(0,7))) {
                if (checknumList.contains("JS2390"))
                {
                    putMap("当贷款实际终止日期不为空时，贷款实际终止日期应小于等于数据日期且应该在报送期内" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        }

        //#贷款用途
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&ArrayUtils.contains(CheckUtil.dkfsecplb, xgrdkfsxx.getLoanprocode())) {
            String temp = xgrdkfsxx.getLoanprocode().substring(0,3);
            if(!"F01".equals(temp)&&!"F03".equals(temp)&&!"F06".equals(temp)&&!"F07".equals(temp)) {
                if (StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan())){
                    if (xgrdkfsxx.getIssupportliveloan().length()<=3000){
                        if (CheckUtil.checkStr5(xgrdkfsxx.getIssupportliveloan())){
                            if (checknumList.contains("JS0934")) {
                                putMap("贷款用途不能包含空格和特殊字符", loancontractcode,loanbrowcode, limitMap, checknumLimit);
                                isError=true;
                            }
                        }
                    }else {
                        if (checknumList.contains("JS0941")) {   putMap("贷款用途"+lengthError+3000, loancontractcode,loanbrowcode, limitMap, checknumLimit);isError=true;   }
                    }
                }else {
                    if (checknumList.contains("JS1612")) { 	putMap("当贷款产品类别为资产类贷款时，贷款用途"+nullError, loancontractcode,loanbrowcode, limitMap, checknumLimit);isError=true;   }
                }
            }
        }

        //#贷款重组方式
        if("LF05".equals(xgrdkfsxx.getLoanstatus())) {
            if (StringUtils.isNotBlank(xgrdkfsxx.getLoanrestructuring())){
                if (!ArrayUtils.contains(CheckUtil.dkczfs, xgrdkfsxx.getLoanrestructuring())){
                    if (checknumList.contains("JS0238"))
                    {
                        putMap("贷款重组方式不在符合要求的值域范围内", loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS1610"))
                {
                    putMap("当贷款状态为重组时，贷款重组方式"+nullError, loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError=true;
                }
            }
        }else {
            if (StringUtils.isNotBlank(xgrdkfsxx.getLoanrestructuring())){
                if (checknumList.contains("JS2652"))
                {
                    putMap("贷款状态不为重组时，贷款重组方式必须为空", loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError=true;
                }
            }
        }

        //#贷款状态
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanstatus())) {
            if (!ArrayUtils.contains(CheckUtil.loanstatus5, xgrdkfsxx.getLoanstatus())) {
                if (checknumList.contains("JS0237"))
                {
                    putMap("贷款状态不符合要求的值域范围内" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1609")) {  putMap("贷款状态" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true; }
        }

        //#发放/收回标识
        if (StringUtils.isNotBlank(xgrdkfsxx.getGivetakeid())) {
            if (!ArrayUtils.contains(CheckUtil.sfcommon, xgrdkfsxx.getGivetakeid())) {
                if (checknumList.contains("JS0239"))
                {
                    putMap("发放/收回标识不符合要求的值域范围内" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
            if(StringUtils.isNotBlank(xgrdkfsxx.getLoanactenddate())) {
                if(!"0".equals(xgrdkfsxx.getGivetakeid())) {
                    if (checknumList.contains("JS2198"))
                    {
                        putMap("贷款实际终止日期不为空的则发放/收回标识应该为0-收回" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError = true;
                    }
                }
            }
            if (StringUtils.isNotBlank(xgrdkfsxx.getLoanstatus())&&ArrayUtils.contains(CheckUtil.loanstatus6, xgrdkfsxx.getLoanstatus())){
                if (xgrdkfsxx.getGivetakeid().equals("1")){
                    if (checknumList.contains("JS2191")) {
                        putMap("发生核销、剥离、以物抵债、资产证券化转让、债转股等交易的贷款发放/收回标识不能为1-发放" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError = true;}
                }
            }
        } else {
            if (checknumList.contains("JS1611 ")) {   putMap("发放/收回标识" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;  }
        }

        //#基准利率
        if ("RF02".equals(xgrdkfsxx.getRateisfix())) {
            if (StringUtils.isNotBlank(xgrdkfsxx.getRate())) {
                if(xgrdkfsxx.getRate().length()>10||!CheckUtil.checkDecimal(xgrdkfsxx.getRate(), 5)) {
                    if (checknumList.contains("JS0945"))
                    {
                        putMap("当基准利率不为空时，基准利率总长度不能超过10位，小数位必须为5位" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError = true;
                    }
                }else {
                    if (CheckUtil.checkPerSign(xgrdkfsxx.getRate())) {
                        if (checknumList.contains("JS0932"))
                        {
                            putMap("利率水平不能包含‰或%" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                    if(new BigDecimal(xgrdkfsxx.getRate()).compareTo(BigDecimal.ZERO) < 0||new BigDecimal(xgrdkfsxx.getRate()).compareTo(new BigDecimal("30")) >0) {
                        if (checknumList.contains("JS2388"))
                        {
                            putMap("当为浮动利率贷款时，基准利率应大于等于0且小于等于30" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                }
            } else {
                if (checknumList.contains("JS1605")) { putMap("当为浮动利率贷款时，基准利率不能为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true; }
            }
        }else if("RF01".equals(xgrdkfsxx.getRateisfix())){
            if (StringUtils.isNotBlank(xgrdkfsxx.getRate())) {
                if (checknumList.contains("JS2160"))
                {
                    putMap("利率类型为固定利率的，基准利率必须为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        }

        //#交易流水号
//        isError = CheckUtil.grantNullAndLengthAndStr2(xdkfsxx.getTransactionnum(), 60, tempMsg, isError, "交易流水号");
        if (StringUtils.isNotBlank(xgrdkfsxx.getTransactionnum())&&StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&!xgrdkfsxx.getLoanprocode().startsWith("F04")){
            if (xgrdkfsxx.getTransactionnum().length()<=60){
                if (checkStr2(xgrdkfsxx.getTransactionnum())){
                    if (checknumList.contains("JS0952"))
                    {
                        putMap("交易流水号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError = true;
                    }
                }
            }else {
                if (checknumList.contains("JS0953"))
                {
                    putMap("交易流水号字段长度不能超过60" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }

        }else {
            if (!ArrayUtils.contains(CheckUtil.tzyw,xgrdkfsxx.getLoanprocode()) && StringUtils.isNotBlank(xgrdkfsxx.getIssupportliveloan())&& !xgrdkfsxx.getIssupportliveloan().contains("线上联合消费贷款")){
                if (checknumList.contains("JS1616"))
                {
                    putMap("除透支类业务以外的交易流水号不能为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        }



        //#借款人地区代码
        if (StringUtils.isNotBlank(xgrdkfsxx.getBrowareacode())) {
            if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
            if (countryList == null) countryList = customSqlUtil.getBaseCode(BaseCountry.class);
            if (!areaList.contains(xgrdkfsxx.getBrowareacode())&& !countryList.contains(xgrdkfsxx.getBrowareacode())) {
                if (checknumList.contains("JS0228"))
                {
                    putMap("借款人地区代码不在符合要求的值域范围内" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1592")) {  putMap("借款人地区代码" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true; }
        }


        //#借款人证件代码
        if (StringUtils.isNotBlank(xgrdkfsxx.getBrowidcode())){
            if (StringUtils.isNotBlank(xgrdkfsxx.getIsfarmerloan())&& xgrdkfsxx.getIsfarmerloan().equals("A01")){
                if(xgrdkfsxx.getBrowidcode().length() != 18){
                    if (checknumList.contains("JS2178"))
                    {
                        putMap("借款人证件类型为统一社会信用代码的，证件代码长度必须为18位", loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError=true;
                    }
                }
            }

            if (checkStr2(xgrdkfsxx.getBrowidcode())){
                if (checknumList.contains("JS0928"))
                {
                    putMap("借款人证件代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。", loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError=true;
                }
            }
            if (StringUtils.isNotBlank(xgrdkfsxx.getBrowidcode())) {
                if (xgrdkfsxx.getBrowidcode().length() > 60) {
                    if (checknumList.contains("JS0937"))
                    {
                        putMap("借款人证件代码字符长度不能超过60" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError = true;
                    }
                }
            }
            if (StringUtils.isNotBlank(xgrdkfsxx.getIsfarmerloan())&&xgrdkfsxx.getIsfarmerloan().startsWith("B")){
                if (xgrdkfsxx.getIsfarmerloan().equals("B01") || xgrdkfsxx.getIsfarmerloan().equals("B08")){
                    if (TuoMinUtils.getB01(xgrdkfsxx.getBrowidcode()).length() != 46){
                        if (checknumList.contains("JS2656")){  putMap("借款人证件类型为B01-身份证或者B08-临时身份证的，借款人证件代码脱敏后的长度应该为46" , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;}
                    }
                }else {
                    if (TuoMinUtils.getB01otr(xgrdkfsxx.getBrowidcode()).length() != 32){
                        if (checknumList.contains("JS2666")){ putMap("借款人证件类型为B开头的个人证件且不是B01-身份证和B08-临时身份证的，借款人证件代码脱敏后的长度应该为32", loancontractcode,loanbrowcode, limitMap, checknumLimit);isError=true;}
                    }
                }

                if (xgrdkfsxx.getIsfarmerloan().equals("B01")){
                    if (StringUtils.isNotBlank(xgrdkfsxx.getSjrq())&&xgrdkfsxx.getBrowidcode().substring(6,14).compareTo(xgrdkfsxx.getSjrq().replace("-","")) >=0){
                        if (checknumList.contains("JS2676")){
                            putMap("证件类型为B01-身份证的，第7-14位的截取结果应该<数据日期", loancontractcode,loanbrowcode, limitMap, checknumLimit);isError=true;
                        }

                    }
                }
            }

        }else {
            if (checknumList.contains("JS1591"))
            {
                putMap("借款人证件代码不能为空", loancontractcode,loanbrowcode, limitMap, checknumLimit);
                isError=true;
            }
        }

        //#借款人证件类型
        if (StringUtils.isNotBlank(xgrdkfsxx.getIsfarmerloan())) {
            if (!ArrayUtils.contains(CheckUtil.grkhzjlx, xgrdkfsxx.getIsfarmerloan())) {
                if (checknumList.contains("JS0227"))
                {
                    putMap("借款人证件类型不在符合要求的最底层值域范围内" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1590")) { putMap("借款人证件类型不能为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;  }
        }

        //#金融机构代码
        if (StringUtils.isNotBlank(xgrdkfsxx.getFinorgcode())){
            if (xgrdkfsxx.getFinorgcode().length()==18){
                if (CheckUtil.checkStr(xgrdkfsxx.getFinorgcode())){
                    if (checknumList.contains("JS0926")) {
                        putMap("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。", loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS0935")) { putMap("金融机构代码字符长度应该为18位", loancontractcode,loanbrowcode, limitMap, checknumLimit);isError=true;  }
            }

            if (fzList == null) {
                String sql = "select distinct finorgcode from xjrjgfz";
                fzList = customSqlUtil.executeQuery(sql);
            }
            if (frList == null) {
                String sql = "select distinct finorgcode from xjrjgfrbaseinfo";
                frList = customSqlUtil.executeQuery(sql);
            }
            if(checknumList.contains("JS1887")) {
                if(!fzList.contains(xgrdkfsxx.getFinorgcode()) && !frList.contains(xgrdkfsxx.getFinorgcode())) {
                    putMap("金融机构代码必须在金融机构（法人）基础信息表.金融机构代码或者金融机构（分支机构）基础信息表.金融机构代码中存在" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }

        }else {
            if (checknumList.contains("JS1587")) { putMap("金融机构代码不能为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;}
        }

        //#金融机构地区代码
        if (StringUtils.isNotBlank(xgrdkfsxx.getFinorgareacode())) {
            if (areaList == null) areaList = customSqlUtil.getBaseCode(BaseArea.class);
            if (!areaList.contains(xgrdkfsxx.getFinorgareacode())) {
                if (checknumList.contains("JS0229")) {
                    putMap("金融机构地区代码不在规定的代码范围内" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1589")){  putMap("金融机构地区代码不能为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true; }
        }

        //#利率是否固定
        if (StringUtils.isNotBlank(xgrdkfsxx.getRateisfix())) {
            if (!ArrayUtils.contains(CheckUtil.llsfgd, xgrdkfsxx.getRateisfix())) {
                if (checknumList.contains("JS0232")){
                    putMap("个人贷款发生额信息中的利率是否固定需在符合要求的值域范围内" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        }else {
            if (checknumList.contains("JS1602")){  putMap("利率是否固定不能为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;  }
        }

        //#利率水平
        if (StringUtils.isNotBlank(xgrdkfsxx.getRatelevel())) {
            if (xgrdkfsxx.getRatelevel().length() <= 10 && CheckUtil.checkDecimal(xgrdkfsxx.getRatelevel(), 5)) {
                if (CheckUtil.checkPerSign(xgrdkfsxx.getRatelevel())) {
                    if (checknumList.contains("JS0931")){
                        putMap("利率水平不能包含‰或%" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError = true;
                    }
                }
                if (!CheckUtil.checkPerSign(xgrdkfsxx.getRatelevel())){
                    if(new BigDecimal(xgrdkfsxx.getRatelevel()).compareTo(BigDecimal.ZERO) < 0||new BigDecimal(xgrdkfsxx.getRatelevel()).compareTo(new BigDecimal("30")) >0) {
                        if (checknumList.contains("JS2387")){
                            putMap("利率水平应大于等于0且小于等于30" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                            isError = true;
                        }
                    }
                }
            } else {
                if (checknumList.contains("JS0944")){
                    putMap("利率水平总长度不能超过10位,小数位应保留5位" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        } else {
            if (checknumList.contains("JS1603")){  putMap("利率水平" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true; }
        }

        //#内部机构号
//        isError = CheckUtil.grantNullAndLengthAndStr2(xdkfsxx.getFinorgincode(), 30, tempMsg, isError, "内部机构号");

        if (StringUtils.isNotBlank(xgrdkfsxx.getFinorgincode())){
            if (xgrdkfsxx.getFinorgincode().length()<=30){
                if (checkStr2(xgrdkfsxx.getFinorgincode())){
                    if (checknumList.contains("JS0927")){     putMap("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。", loancontractcode,loanbrowcode, limitMap, checknumLimit);isError=true;   }
                }
            }else {
                if (checknumList.contains("JS0936")){   putMap("内部机构号字符长度不能超过30", loancontractcode,loanbrowcode, limitMap, checknumLimit);isError=true; }
            }
        }else {
            if (checknumList.contains("JS1588")){    putMap("内部机构号不能为空", loancontractcode,loanbrowcode, limitMap, checknumLimit);isError=true;}
        }



        //#是否首次贷款
        if (StringUtils.isNotBlank(xgrdkfsxx.getIsplatformloan())) {
            if (!ArrayUtils.contains(CheckUtil.sfcommon, xgrdkfsxx.getIsplatformloan())) {
                if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&!xgrdkfsxx.getLoanprocode().startsWith("F04")){
                    if (checknumList.contains("JS0236"))
                    {
                        putMap("是否首次贷款需在符合要求的值域范围内（透支类业务除外）" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError = true;
                    }
                }
            }
        }else {
            if (checknumList.contains("JS1608")) {   putMap("是否首次贷款不能为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);isError = true;  }
        }

        //#数据日期
//        if (StringUtils.isNotBlank(xdkfsxx.getSjrq())) {
//        	boolean b = CheckUtil.checkDate(xdkfsxx.getSjrq(), "yyyy-MM-dd");
//            if (b) {
//            	if (xdkfsxx.getSjrq().compareTo("1900-01-01") < 0 || xdkfsxx.getSjrq().compareTo("2100-12-31") > 0) {
//            		putMap("数据日期需晚于1900-01-01早于2100-12-31" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
//            		isError = true;
//                }
//            } else {
//            	putMap("数据日期不符合yyyy-MM-dd格式" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
//            	isError = true;
//            }
//        } else {
//            putMap("数据日期" + nullError , loancontractcode,loanbrowcode, limitMap, checknumLimit);
//            isError = true;
//        }
//
        //#资产证券化产品代码
        //数据日期
//        isError = CheckUtil.nullAndDate(xgrdkfsxx.getSjrq(), tempMsg, isError, "数据日期",false);




        if("LF07".equals(xgrdkfsxx.getLoanstatus())) {
            if (StringUtils.isNotBlank(xgrdkfsxx.getAssetproductcode())){
                if (xgrdkfsxx.getAssetproductcode().length()<=400){
                    if (checkStr2(xgrdkfsxx.getAssetproductcode())){
                        if (checknumList.contains("JS0933")) {
                            putMap("资产证券化产品代码不能包含空格和特殊字符", loancontractcode,loanbrowcode, limitMap, checknumLimit);
                            isError=true;
                        }
                    }
                }else {
                    if (checknumList.contains("JS0940")) {
                        putMap("当资产证券化产品代码不为空时，资产证券化产品代码长度不超过400", loancontractcode,loanbrowcode, limitMap, checknumLimit);
                        isError=true;
                    }
                }
            }else {
                if (checknumList.contains("JS1614")) {
                    putMap("当贷款状态为资产证券化转让时，资产证券化产品代码不能为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        }else {
            if (StringUtils.isNotBlank(xgrdkfsxx.getAssetproductcode())){
                if (checknumList.contains("JS2654")) {
                    putMap("当贷款状态不是资产证券化转让时，资产证券化产品代码必须为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        }
        //透支业务
        if (StringUtils.isNotBlank(xgrdkfsxx.getLoanprocode())&&xgrdkfsxx.getLoanprocode().startsWith("F04")){
            if (!StringUtils.isNotBlank(xgrdkfsxx.getLoanactenddate())){
                if (checknumList.contains("JS2801")) {
                    putMap("透支类业务的贷款实际终止日期应为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (!StringUtils.isNotBlank(xgrdkfsxx.getLoanstartdate())){
                if (checknumList.contains("JS2802")) {
                    putMap("透支类业务的贷款发放日期应为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (!StringUtils.isNotBlank(xgrdkfsxx.getLoanenddate())){
                if (checknumList.contains("JS2803")) {
                    putMap("透支类业务的贷款到期日期应为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (!StringUtils.isNotBlank(xgrdkfsxx.getLoanraterepricedate())){
                if (checknumList.contains("JS2804")) {
                    putMap("透支类业务的贷款利率重新定价日应为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
            if (!StringUtils.isNotBlank(xgrdkfsxx.getTransactionnum())){
                if (checknumList.contains("JS2807")) {
                    putMap("透支类业务的交易流水号应为空" , loancontractcode,loanbrowcode, limitMap, checknumLimit);
                    isError = true;
                }
            }
        }
        //#整表
        if (isError || errorId0.contains(xgrdkfsxx.getId())) {
            errorId.add(xgrdkfsxx.getId().toString());
            return 1;
        } else {
            if (!errorId.contains(xgrdkfsxx.getId().toString())) {
                rightId.add(xgrdkfsxx.getId().toString());
            }
            return 0;
        }
    }

    
    public static void putMap(String rule,String code,String code2,Map<String,String> map,int checknumLimit) {
        if(map.containsKey(rule)) {
            String value = map.get(rule);
            int num = Integer.valueOf(value.substring(0,value.indexOf("#")));
            if(num >= checknumLimit) {
                limitFlag = true;
                return;
            }
            num++;
            map.put(rule, num+"#"+value+code+"+"+code2+"\r\n");
        }else {
            map.put(rule, "1#"+code+"+"+code2+"\r\n");
        }
    }

    
    public static Boolean nullAndDateCldkxx2(Xclgrdkxx xclgrdkxx,List<String>xcldkxx1List,StringBuffer errorMsg,boolean isError,String fieldName,boolean isHasNull){
        if (StringUtils.isNotBlank(xclgrdkxx.getLoaninterestrepricedate())&&StringUtils.isNotBlank(xclgrdkxx.getProductcetegory())&&!xclgrdkxx.getProductcetegory().startsWith("F04")){
            if (xclgrdkxx.getLoaninterestrepricedate().length()==10){
                boolean b1 = checkDate(xclgrdkxx.getLoaninterestrepricedate(), "yyyy-MM-dd");
                if (b1){
                    if (xclgrdkxx.getLoaninterestrepricedate().compareTo("1800-01-01")<0 && xclgrdkxx.getLoaninterestrepricedate().compareTo("2100-12-31")>0)
                    {
                            if (xcldkxx1List.contains("JS0924")) {
                                errorMsg.append(fieldName + "除透支类业务以外的需晚于1800-01-01早于2100-12-31|");
                                isError = true;
                            }
                    }
                }else {
                        if (xcldkxx1List.contains("JS0924")) {
                            errorMsg.append(fieldName + "除透支类业务以外的不符合yyyy-MM-dd格式|");
                            isError = true;
                        }

                }
            }else {
                errorMsg.append(fieldName+"长度不等于10|");isError=true;
            }
        }else {
            if (!isHasNull){
                if (!ArrayUtils.contains(CheckUtil.tzyw, xclgrdkxx.getProductcetegory()) && StringUtils.isNotBlank(xclgrdkxx.getIssupportliveloan()) && !xclgrdkxx.getIssupportliveloan().contains("线上联合消费贷款")) {
                    if (xcldkxx1List.contains("JS1638")){ errorMsg.append(fieldName+"除透支类业务以外不能为空|");isError=true;}
                }
            }

        }
        return isError;
    }
    /**
     * 校验日期
     * @param date
     * @return
     */
    public static boolean checkDate(String date,String pattern){
        boolean convertSuccess=true;
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            format.parse(date);
        } catch (Exception e) {
            convertSuccess=false;
        }
        return convertSuccess;
    }
    public static Boolean   equalNullAndCode(String fieldValue,List<String>xcldkxx1List, int length, List<String> list,StringBuffer errorMsg, boolean isError, String fieldName){
        if (StringUtils.isNotBlank(fieldValue)){
            if (fieldValue.length()==length){
                if (!list.contains(fieldValue)){
                    if (xcldkxx1List.contains("JS0100") || xcldkxx1List.contains("JS0223")){
                        errorMsg.append(fieldName+"不在规定的代码范围内|");isError=true;
                    }
                }
            }else {
                errorMsg.append(fieldName+"长度不等于"+length+"|");isError=true;
            }
        }else {
            if (xcldkxx1List.contains("JS1022") || xcldkxx1List.contains("JS1640")){
                errorMsg.append(fieldName+"不能为空|");isError=true;
            }
        }
        return isError;
    }
}

