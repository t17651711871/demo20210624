package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.report.Xcltyckxx;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import static com.geping.etl.UNITLOAN.util.check.CheckUtil.checkStr2;

/**
 * @ClassName: CheckAllDataTyckfse
 * @Description: TOOD
 * @Author: 陈根
 * @Date: 2021/2/24 13:53
 * @Version
 **/
public class CheckAllDataCltyck {
    //币种代码
    private static List<String> currencyList;
    //行政区划代码
    private static List<String> areaList;
    //国家代码
    private static List<String> countryList;
    //大行业代码
    private static List<String> aindustryList;

    //C0017-行业中类代码
    private static List<String> bindustryList;

    //金融机构（分支机构）基础信息表.金融机构代码
    public static List<String> fzList;

    //金融机构（法人）基础信息表.金融机构代码
    public static List<String> frList;

    //存量同业存款信息
    public static void checkXcltyckxx(CustomSqlUtil customSqlUtil, List<String> checknumList, Xcltyckxx xcltyckxx, LinkedHashMap<String, String> errorMsg, List<String> errorId, List<String> rightId) {
        boolean isError = false;
        StringBuffer tempMsg = new StringBuffer("");

        String nullError = "不能为空";
        String lengthError = "超过限制长度";
        String spaceStr = "|";
        //金融机构代码
        String financeorgcode = xcltyckxx.getFinanceorgcode();
        //内部机构号
        String financeorginnum = xcltyckxx.getFinanceorginnum();
        //业务类型
        String ywlx = xcltyckxx.getYwlx();
        //交易对手证件类型
        String jydszjlx = xcltyckxx.getJydszjlx();
        //交易对手代码
        String jydsdm = xcltyckxx.getJydsdm();
        //存款账户编码
        String ckzhbm = xcltyckxx.getCkzhbm();
        //存款协议代码
        String ckxydm = xcltyckxx.getCkxydm();
        //协议起始日期
        String startdate = xcltyckxx.getStartdate();
        //协议到期日期
        String enddate = xcltyckxx.getEnddate();
        //币种
        String currency = xcltyckxx.getCurrency();
        //存款余额
        String receiptbalance = xcltyckxx.getReceiptbalance();
        //存款余额折人民币
        String receiptcnybalance = xcltyckxx.getReceiptcnybalance();
        //利率水平
        String llsp = xcltyckxx.getInterestislevel();
        //缴存准备金方式
        String jczbjfs = xcltyckxx.getJczbjfs();
        //数据日期
        String sjrq = xcltyckxx.getSjrq();


        //金融机构代码
        if (StringUtils.isNotBlank(financeorgcode)){
            if (CheckUtil.checkStr(financeorgcode)){
                if (checknumList.contains("JS0639")) {
                    tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);
                    isError=true;
                }
            }

            if (financeorgcode.length() !=18 ){
                if (checknumList.contains("JS0645")) { tempMsg.append("金融机构代码字符长度应该为18位"+spaceStr);isError=true;  }
            }
        }else {
            if (checknumList.contains("JS1296")) { tempMsg.append("金融机构代码不能为空" + spaceStr);isError = true;}
        }

        //内部机构号
        if (StringUtils.isNotBlank(financeorginnum)){

            if (checkStr2(financeorginnum)){
                if (checknumList.contains("JS0640")){     tempMsg.append("内部机构号字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }

            if (financeorginnum.length() > 30){
                if (checknumList.contains("JS0646")){   tempMsg.append("内部机构号字符长度不能超过30"+spaceStr);isError=true; }
            }
        }else {
            if (checknumList.contains("JS1297")){    tempMsg.append("内部机构号不能为空"+spaceStr);isError=true;}
        }

        //业务类型
        if (StringUtils.isNotBlank(ywlx)){
            if (!ArrayUtils.contains(CheckUtil.ywlx,ywlx)){
                if (checknumList.contains("JS0001")){
                    tempMsg.append("存量同业存款信息表中的业务类型需在符合要求的值域范围内"+spaceStr); isError=true;
                }
            }
        }else {
            if (checknumList.contains("JS1298")){
                tempMsg.append("业务类型不能为空"+spaceStr); isError=true;
            }
        }

        //交易对手证件类型
        if (StringUtils.isNotBlank(jydszjlx)){
            if (!ArrayUtils.contains(CheckUtil.jydszjlx,jydszjlx)){
                if (checknumList.contains("JS0072")){
                    tempMsg.append("交易对手证件类型应该在符合要求的值域范围内"+spaceStr); isError=true;
                }
            }
        }else {
            if (checknumList.contains("JS1525")){
                tempMsg.append("交易对手证件类型不能为空"+spaceStr); isError=true;
            }
        }

        //交易对手代码
        if (StringUtils.isNotBlank(jydsdm)){
            if (checkStr2(jydsdm)){
                if (checknumList.contains("JS0642")){     tempMsg.append("交易对手代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }

            if (jydsdm.length() > 60){
                if (checknumList.contains("JS0647")){   tempMsg.append("交易对手代码字符长度不能超过60"+spaceStr);isError=true; }
            }

            if (StringUtils.isNotBlank(jydszjlx) && (jydszjlx.equals("A01") || jydszjlx.equals("A02"))){
                if (jydsdm.length()!= 9 && jydsdm.length() != 18){
                    if (checknumList.contains("JS2407")){
                        tempMsg.append("境内非SPV客户的交易对手代码字符长度应该为9位或18位"+spaceStr);isError=true;
                    }
                }
            }
        }else {
            if (checknumList.contains("JS1299")){    tempMsg.append("交易对手代码不能为空"+spaceStr);isError=true;}
        }


        //存款账户编码
        if (StringUtils.isNotBlank(ckzhbm)){
            if (checkStr2(ckzhbm)){
                if (checknumList.contains("JS0643")){     tempMsg.append("存款账户编码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }

            if (ckzhbm.length() > 60){
                if (checknumList.contains("JS0648")){   tempMsg.append("存款账户编码字符长度不能超过60"+spaceStr);isError=true; }
            }
        }else {
            if (checknumList.contains("JS1300")){   tempMsg.append("存款账户编码不能为空"+spaceStr);isError=true; }
        }


        //存款协议代码
        if (StringUtils.isNotBlank(ckxydm)){
            if (checkStr2(ckxydm)){
                if (checknumList.contains("JS0644")){     tempMsg.append("存款协议代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。"+spaceStr);isError=true;   }
            }

            if (ckxydm.length() > 60){
                if (checknumList.contains("JS0649")){   tempMsg.append("存款协议代码字符长度不能超过60"+spaceStr);isError=true; }
            }
        }else {
            if (checknumList.contains("JS1526")){   tempMsg.append("存款协议代码不能为空"+spaceStr);isError=true; }
        }

        //协议起始日期
        if (StringUtils.isNotBlank(startdate)){
            if(checknumList.contains("JS0654")) {
                boolean b1 = CheckUtil.checkDate(startdate, "yyyy-MM-dd");
                if (b1) {
                    if (startdate.compareTo("1800-01-01") < 0 || startdate.compareTo("2100-12-31") > 0) {
                        tempMsg.append("协议起始日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
                        isError = true;
                    }
                } else {
                    tempMsg.append("协议起始日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + spaceStr);
                    isError = true;
                }
            }
            if(checknumList.contains("JS2233")) {
                if(StringUtils.isNotBlank(sjrq) && startdate.compareTo(sjrq) > 0) {
                    tempMsg.append("协议起始日期应小于等于数据日期" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if(checknumList.contains("JS1301")) {
                tempMsg.append("协议起始日期不能为空" + spaceStr);
                    isError = true;

            }
        }


        //协议到期日期
        if (StringUtils.isNotBlank(enddate)){
            if(checknumList.contains("JS0655")) {
                boolean b1 = CheckUtil.checkDate(enddate, "yyyy-MM-dd");
                if (b1) {
                    if (enddate.compareTo("1800-01-01") < 0 || enddate.compareTo("2100-12-31") > 0) {
                        if (enddate.compareTo("9999-12-31") != 0) {
                            tempMsg.append("协议到期日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间，或者为9999-12-31" + spaceStr);
                            isError = true;
                        }
                    }
                } else {
                    tempMsg.append("协议到期日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间，或者为9999-12-31" + spaceStr);
                    isError = true;
                }
            }
            if(checknumList.contains("JS2234")&&StringUtils.isNotBlank(startdate) &&StringUtils.isNotBlank(enddate)) {
                if(enddate.compareTo(startdate) < 0 &&enddate.compareTo("1999-01-01")!=0&&enddate.compareTo("1999-01-07")!=0 ) {
                    tempMsg.append("协议到期日期不等于1999-01-01、1999-01-07时，应大于等于协议起始日期" + spaceStr);
                    isError = true;
                }
            }
        }else {
            if(checknumList.contains("JS1302")) {
                tempMsg.append("协议到期日期不能为空" + spaceStr);
                isError = true;

            }
        }


        //币种
        if(StringUtils.isNotBlank(currency)) {
            if(currencyList == null) currencyList = customSqlUtil.getBaseCode(BaseCurrency.class);
            if(checknumList.contains("JS0003")) {
                if(!currencyList.contains(currency)) {
                    tempMsg.append("同业存款发生额信息表中的币种需在值域范围内" + spaceStr);
                    isError = true;
                }
            }

        }else {
            if (checknumList.contains("JS1303")){ tempMsg.append("币种不能为空" + spaceStr);isError = true; }
        }


        //存款余额
        if (StringUtils.isNotBlank(receiptbalance)){
            if(checknumList.contains("JS0650")) {
                if (receiptbalance.length() > 20 || !CheckUtil.checkDecimal(receiptbalance, 2)) {
                    tempMsg.append("存款余额总长度不能超过20位，小数位必须为2位" + spaceStr);
                    isError = true;
                }
            }


            if(checknumList.contains("JS2231")) {
                try {
                    if(new BigDecimal(receiptbalance).compareTo(BigDecimal.ZERO) <= 0) {
                        tempMsg.append("存款余额应大于0" + spaceStr);
                        isError = true;
                    }
                }catch(Exception e) {
                    tempMsg.append("存款余额应大于0" + spaceStr);
                    isError = true;
                }
            }

        }else {
            if(checknumList.contains("JS1304")) {
                tempMsg.append("存款余额不能为空" + spaceStr);
                isError = true;
            }
        }

        // 存款余额折人民币
        if (StringUtils.isNotBlank(receiptcnybalance)){
            if(checknumList.contains("JS0651")) {
                if (receiptcnybalance.length() > 20 || !CheckUtil.checkDecimal(receiptcnybalance, 2)) {
                    tempMsg.append("存款余额折人民币总长度不能超过20位，小数位必须为2位" + spaceStr);
                    isError = true;
                }
            }


            if(checknumList.contains("JS2232")) {
                try {
                    if(new BigDecimal(receiptcnybalance).compareTo(BigDecimal.ZERO) <= 0) {
                        tempMsg.append("存款余额折人民币应大于0" + spaceStr);
                        isError = true;
                    }
                }catch(Exception e) {
                    tempMsg.append("存款余额折人民币应大于0" + spaceStr);
                    isError = true;
                }
            }


            if (StringUtils.isNotBlank(currency) && currency.equals("CNY")){
                if (StringUtils.isNotBlank(receiptbalance) && !receiptbalance.equals(receiptcnybalance)){
                    if(checknumList.contains("JS2695")) {
                        tempMsg.append("币种为人民币的，存款余额应该与交易金额的值相等" + spaceStr);
                        isError = true;
                    }
                }
            }
        }else {
            if(checknumList.contains("JS1305")) {
                tempMsg.append("存款余额折人民币不能为空" + spaceStr);
                isError = true;
            }
        }





        //利率水平
        if (StringUtils.isNotBlank(llsp)){
            if (CheckUtil.checkPerSign(llsp)) {
                if (checknumList.contains("JS0641")) {
                    tempMsg.append("利率水平不能包含‰或%" + spaceStr);
                    isError = true;
                }
            }

            if(checknumList.contains("JS0652")) {
                if (llsp.length() > 10 || !CheckUtil.checkDecimal(llsp, 5)) {
                    tempMsg.append("利率水平总长度不能超过10位，小数位必须为5位" + spaceStr);
                    isError = true;
                }
            }

            if(checknumList.contains("JS2235")) {
                try {
                    if(new BigDecimal(llsp).compareTo(BigDecimal.ZERO) == -1 || new BigDecimal(llsp).compareTo(new BigDecimal("30")) == 1) {
                        tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
                        isError = true;
                    }
                }catch(Exception e) {
                    tempMsg.append("利率水平应大于等于0且小于等于30" + spaceStr);
                    isError = true;
                }
            }

        }else {
            if(checknumList.contains("JS1306")) {
                tempMsg.append("利率水平不能为空" + spaceStr);
                isError = true;

            }
        }

        //缴存准备金方式
        if (StringUtils.isNotBlank(jczbjfs)){
            if (!ArrayUtils.contains(CheckUtil.jczbjfs,jczbjfs)){
                if (checknumList.contains("JS0002")) { tempMsg.append("存量同业存款信息表中的缴存准备金方式需在符合要求的值域范围内" + spaceStr);isError = true;}
            }

            if(StringUtils.isNotBlank(ywlx) && (ywlx.substring(0,3).equals("T02") || ywlx.substring(0,3).equals("T04"))){
                if (jczbjfs.equals("DR02") || jczbjfs.equals("DR03") || jczbjfs.equals("DR04")){
                    if (checknumList.contains("JS2060")) {
                        tempMsg.append("业务类型为T02开头存放同业或者T04-同业存单投资等资产业务时，缴存准备金方式不应该为DR02-全额缴存、DR03-比例缴存或者DR04-净额缴存" + spaceStr);
                        isError = true;
                    }
                }
            }


        }else {
            if (checknumList.contains("JS1307")) { tempMsg.append("缴存准备金方式不能为空" + spaceStr);isError = true;}
        }




        //数据日期
        isError = CheckUtil.nullAndDate(sjrq, tempMsg, isError, "数据日期",false);

        //#整表
        if (isError) {
            if(!errorMsg.containsKey(xcltyckxx.getId())) {
                errorMsg.put(xcltyckxx.getId(), "交易对手代码:"+xcltyckxx.getJydsdm()+"，"+"存款账户编码:"+xcltyckxx.getCkzhbm()+"]->\r\n");
            }
            String str = errorMsg.get(xcltyckxx.getId());
            str = str + tempMsg;
            errorMsg.put(xcltyckxx.getId(), str);
            errorId.add(xcltyckxx.getId());
        } else {
            if(!errorId.contains(xcltyckxx.getId())) {
                rightId.add(xcltyckxx.getId());
            }
        }
    }

}
