package com.geping.etl.UNITLOAN.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.DigestUtils;

/**    
*  
* @author liuweixin  
* @date 2020年8月22日 上午9:23:53  
*/
public class TuoMinUtils {

	//个人身份证脱敏
	public static String getB01(String cardOld) {
		if(StringUtils.isNotBlank(cardOld)) {
			try {
				//1.转成大写字母
				cardOld = cardOld.toUpperCase();
				//2.取前14位
				String card14 = cardOld.substring(0,14);
				//3.MD5(18 位身份证件号码)（32 字符，英文按小写输出）
				String card32 = DigestUtils.md5DigestAsHex(cardOld.getBytes()).toLowerCase();
				//4.拼接成46位
				String cardNew = card14 + card32;
				return cardNew;
			}catch(Exception e) {
				return cardOld;
			}
			
		}else {
			return cardOld;
		}
				
	}
	
	//个人其他证件脱敏
	public static String getB01otr(String cardOld) {
		if(StringUtils.isNotBlank(cardOld)) {
			try {
				//1.转成大写字母
				cardOld = cardOld.toUpperCase();
				//2.转成utf-8编码
				try {
					cardOld = URLEncoder.encode(cardOld,"utf-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				//3.MD5(证件号码)（32 字符，英文按小写输出）
				String cardNew = DigestUtils.md5DigestAsHex(cardOld.getBytes()).toLowerCase();
				return cardNew;
			}catch(Exception e) {
				return cardOld;
			}
			
		}else {
			return cardOld;
		}
	}

}
