package com.geping.etl.UNITLOAN.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**    
*  
* @author liuweixin  
* @date 2020年9月9日 下午1:56:40  
*/
@Component
@PropertySource(value = "classpath:messagePath.properties")
public class XDataBaseTypeUtil {

    private static final String Mysql = "MySql";

    private static final String SqlServer = "SqlServer";

    private static final String Oracle = "Oracle";

    @Value(value = "${dataBase.type}")
    private String dataBaseType;


    //判断是否为MySQL数据库
    public boolean equalsMySql(){
        return Mysql.equals(dataBaseType);
    }

    //判断是否为SqlServer数据库
    public boolean equalsSqlServer(){
        return SqlServer.equals(dataBaseType);
    }

    //判断是否为Oracle数据库
    public boolean equalsOracle(){
        return Oracle.equals(dataBaseType);
    }

}