package com.geping.etl.UNITLOAN.util.check;

import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.check.CheckHelper;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.dataDictionary.ZqFieldUtils;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.entity.report.Xzqfxfsexx;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DateUtils;
import com.geping.etl.utils.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/***
 *   债券发行发生额信息 row校验处理器
 * @author liang.xu
 * @date 2021.4.26
 */
@Service
public class CheckXzqfxfsexxRowData implements CheckRowData {

    @Autowired
    private CustomSqlUtil customSqlUtil;

    @Override
    public void checkRow(String header, CheckParamContext checkParamContext, Object object) {
        Xzqfxfsexx xclzqfxxx = (Xzqfxfsexx) object;
        StringBuffer tempMsg = new StringBuffer();
        Boolean isError = false;
        List<String> checknumList = checkParamContext.getCheckNums();
        //1.金融机构属性校验
        if (checkXclzqfxxxRowFinanceorgcode(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //2.债券代码属性校验
        if (checkXclzqfxxxRowZqdm(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //3.债券总托管机构属性校验
        if (checkXclzqfxxxRowZqztgjg(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //4.债券品种属性校验
        if (checkXclzqfxxxRowZqpz(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //5.债券信用级别属性校验
        if (checkXclzqfxxxRowZqxyjg(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //6.续发次数属性校验
        if (checkXclzqfxxxRowXfcs(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //7.币种属性校验
        if (checkXclzqfxxxRowBz(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //8.发行/兑付债券面值属性校验
        if (checkXclzqfxxxRowFxdhz(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //9.发行/兑付债券面值折人民币属性校验
        if (checkXclzqfxxxRowFxdhzqmzzrmb(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //10:发行/兑付债券金额属性校验
        if (checkXclzqfxxxRowFxdfzqje(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //11.发行/兑付债券金额折人民币 属性校验
        if (checkXclzqfxxxRowFxdfzqjezrmb(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //12 债权债务登记日 属性校验
        if (checkXclzqfxxxRowZqzwdj(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //13 起息日属性校验
        if (checkXclzqfxxxRowQxr(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //14 兑付日期属性校验
        if (checkXclzqfxxxRowDfrq(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //15 交易日期属性校验
        if (checkXclzqfxxxRowJyrq(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //16 票面利率属性校验
        if (checkXclzqfxxxRowPmll(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //17 发行/兑付标识属性校验
        if (checkXclzqfxxxRowFxdfbs(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //18 票面利率
        if (checkXclzqfxxxRowTPmll(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        //18 数据日期
        if (checkXclzqfxxxRowSjrq(checknumList, xclzqfxxx, tempMsg)) {
            isError = true;
        }
        CheckHelper checkHelper = (CheckHelper) SpringContextUtil.getBean(CheckHelper.class);
        //单条实体交验完,处理结果
        checkHelper.handleRowCheckResult(isError, xclzqfxxx.getId(), String.format(header, xclzqfxxx.getFinanceorgcode(), xclzqfxxx.getZqdm()), tempMsg.toString(), checkParamContext);
    }

    private boolean checkXclzqfxxxRowSjrq(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        String sjrq = xclzqfxxx.getSjrq();//数据日期
        if (StringUtils.isNotBlank(sjrq)) {
            if (sjrq.length() == 10) {
                boolean b1 = CheckUtil.checkDate(sjrq, "yyyy-MM-dd");
                if (b1) {
                    if (sjrq.compareTo("1800-01-01") < 0 || sjrq.compareTo("2100-12-31") > 0) {
                        tempMsg.append("数据日期需晚于1800-01-01早于2100-12-31" + SysConstants.spaceStr);
                        result = true;
                    }
                } else {
                    tempMsg.append("数据日期不符合yyyy-MM-dd格式" + SysConstants.spaceStr);
                    result = true;
                }
            } else {
                tempMsg.append("数据日期长度不等于10" + SysConstants.spaceStr);
                result = true;
            }
        } else {
            tempMsg.append("数据日期不能为空" + SysConstants.spaceStr);
            result = true;
        }
        return result;
    }

    private boolean checkXclzqfxxxRowTPmll(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        String pmll = xclzqfxxx.getPmll();
        BigDecimal bigDecimal1 = new BigDecimal(0);
        BigDecimal bigDecimal2 = new BigDecimal(30);
        if (checknumList.contains("JS2280")) {
            if (StringUtils.isNotBlank(pmll)) {
                try {
                    if (new BigDecimal(pmll).compareTo(bigDecimal1) < 0 || new BigDecimal(pmll).compareTo(bigDecimal2) > 0) {
                        result = true;
                        tempMsg.append("当票面利率不为空时，应大于等于0且小于等于30" + SysConstants.spaceStr);
                    }
                } catch (Exception e) {
                    result = true;
                    tempMsg.append("当票面利率不为空时，应大于等于0且小于等于30" + SysConstants.spaceStr);
                }
            }
        }


        return result;
    }

    /**
     * 发行/兑付标识属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowFxdfbs(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        String fxdfbs = xclzqfxxx.getFxdfbs();
        if (StringUtils.isNotBlank(xclzqfxxx.getFxdfbs())) {
            if (checknumList.contains("JS0049") && StringUtils.isNotBlank(fxdfbs)) {
                if (!fxdfbs.equals("1") && !fxdfbs.equals("0")) {
                    tempMsg.append("债券发行发生额信息表中的发行/兑付标识需在符合要求的值域范围内" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1412")) {
                if (StringUtils.isBlank(xclzqfxxx.getFxdfbs())) {
                    tempMsg.append("发行/兑付标识不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 票面利率属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowPmll(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getPmll())) {
            if (checknumList.contains("JS0753")) {
                if (xclzqfxxx.getPmll().contains("‰") || xclzqfxxx.getPmll().contains("%")) {
                    tempMsg.append("当票面利率不为空时，票面利率不能包含‰或%" + SysConstants.spaceStr);
                    return true;
                }
            }
            if (checknumList.contains("JS0760")) {
                if (xclzqfxxx.getPmll().length() > 10 || !CheckUtil.checkDecimal(xclzqfxxx.getPmll(), 5)) {
                    tempMsg.append("当票面利率不为空时，票面利率总长度不能超过10位，小数位必须为5位" + SysConstants.spaceStr);
                    return true;
                }
            }
        }

        return result;
    }

    /**
     * 交易日期属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowJyrq(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean reuslt = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getJyrq())) {
            if (checknumList.contains("JS0766")) {
                boolean ckJyrqDate = CheckUtil.checkDate(xclzqfxxx.getJyrq(), SysConstants.yyyyMMdd);
                if (!ckJyrqDate || (xclzqfxxx.getDfrq().compareTo("1800-01-01") < 0 || xclzqfxxx.getDfrq().compareTo("2100-12-31") > 0)) {
                    tempMsg.append("交易日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
                    return true;
                }
            }
            if (checknumList.contains("JS2281")) {
                if (StringUtils.isNotBlank(xclzqfxxx.getQxr())) {
                    if (xclzqfxxx.getJyrq().compareTo(xclzqfxxx.getQxr()) < 0) {
                        tempMsg.append("交易日期应大于等于起息日" + SysConstants.spaceStr);
                        reuslt = true;
                    }
                }
            }
            if (checknumList.contains("JS2282")) {
                if (StringUtils.isNotBlank(xclzqfxxx.getDfrq())) {
                    if (xclzqfxxx.getJyrq().compareTo(xclzqfxxx.getDfrq()) > 0) {
                        tempMsg.append("交易日期应小于等于兑付日期" + SysConstants.spaceStr);
                        reuslt = true;
                    }
                }
            }
            if (checknumList.contains("JS2568")) {
                if (!DateUtils.isInMonth(xclzqfxxx.getJyrq(), xclzqfxxx.getSjrq())) {
                    tempMsg.append("交易日期应该在当月范围内" + SysConstants.spaceStr);
                    reuslt = true;
                }
            }
        } else {
            if (checknumList.contains("JS1409")) {
                if (StringUtils.isBlank(xclzqfxxx.getJyrq())) {
                    tempMsg.append("交易日期不能为空" + SysConstants.spaceStr);
                    return true;
                }
            }
        }
        return reuslt;
    }

    /**
     * 兑付日期属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowDfrq(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getDfrq())) {
            if (checknumList.contains("JS0765")) {
                boolean ckDfrqDate = CheckUtil.checkDate(xclzqfxxx.getDfrq(), SysConstants.yyyyMMdd);
                if (!ckDfrqDate || (xclzqfxxx.getDfrq().compareTo("1800-01-01") < 0 || xclzqfxxx.getDfrq().compareTo("2100-12-31") > 0)) {
                    tempMsg.append("兑付日期必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
                    return true;
                }
            }
        } else {
            if (checknumList.contains("JS1408")) {
                if (StringUtils.isNotBlank(xclzqfxxx.getDfrq())) {
                    tempMsg.append("兑付日期不能为空" + SysConstants.spaceStr);
                    return true;
                }
            }
        }
        return result;
    }

    /**
     * 起息日属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowQxr(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getQxr())) {
            if (checknumList.contains("JS0764")) {
                boolean ckQxrDate = CheckUtil.checkDate(xclzqfxxx.getQxr(), SysConstants.yyyyMMdd);
                if (!ckQxrDate || (xclzqfxxx.getQxr().compareTo("1800-01-01") < 0 || xclzqfxxx.getQxr().compareTo("2100-12-31") > 0)) {
                    tempMsg.append("起息日必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS2279")) {
                if (StringUtils.isNotBlank(xclzqfxxx.getDfrq())) {
                    if (xclzqfxxx.getQxr().compareTo(xclzqfxxx.getDfrq()) > 0) {
                        tempMsg.append("起息日应小于等于兑付日期" + SysConstants.spaceStr);
                        result = true;
                    }
                }
            }
            if (checknumList.contains("JS2278")) {
                if (StringUtils.isNotBlank(xclzqfxxx.getSjrq())) {
                    if (xclzqfxxx.getQxr().compareTo(xclzqfxxx.getSjrq()) > 0) {
                        tempMsg.append("起息日应小于等于数据日期" + SysConstants.spaceStr);
                        result = true;
                    }
                }
            }
            if (checknumList.contains("JS2567") && StringUtils.isNotBlank(xclzqfxxx.getFxdfbs())) {
                if (!DateUtils.isInMonth(xclzqfxxx.getQxr(), xclzqfxxx.getSjrq()) && xclzqfxxx.getFxdfbs().equals("1")) {
                    tempMsg.append("当月发行的债券，起息日应该在当月范围内" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1407")) {
                if (StringUtils.isBlank(xclzqfxxx.getQxr())) {
                    tempMsg.append("起息日不能为空" + SysConstants.spaceStr);
                    return true;
                }
            }
        }
        return result;
    }

    /**
     * 债权债务登记日 属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowZqzwdj(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getZqzwdj())) {
            if (checknumList.contains("JS0763")) {
                boolean ckZqzwdjDate = CheckUtil.checkDate(xclzqfxxx.getZqzwdj(), SysConstants.yyyyMMdd);
                if (!ckZqzwdjDate || !(xclzqfxxx.getZqzwdj().compareTo("1800-01-01") >= 0 && xclzqfxxx.getZqzwdj().compareTo("2100-12-31") <= 0)) {
                    tempMsg.append("债权债务登记日必须满足日期格式为：YYYY-MM-DD且日期范围在1800-01-01到2100-12-31之间" + SysConstants.spaceStr);
                    return true;
                }
            }
//            if (checknumList.contains("JS2277")) {
//                if (StringUtils.isNotBlank(xclzqfxxx.getQxr())) {
//                    if (xclzqfxxx.getZqzwdj().compareTo(xclzqfxxx.getQxr()) > 0) {
//                        tempMsg.append("债权债务登记日应小于等于起息日" + SysConstants.spaceStr);
//                        result = true;
//                    }
//                }
//            }
            if (checknumList.contains("JS2276")) {
                if (StringUtils.isNotBlank(xclzqfxxx.getSjrq())) {
                    if (xclzqfxxx.getZqzwdj().compareTo(xclzqfxxx.getSjrq()) > 0) {
                        tempMsg.append("债权债务登记日应小于等于数据日期" + SysConstants.spaceStr);
                        result = true;
                    }
                }
            }
//            if (checknumList.contains("JS2566") && StringUtils.isNotBlank(xclzqfxxx.getFxdfbs())) {
//                if (!DateUtils.isInMonth(xclzqfxxx.getZqzwdj(), xclzqfxxx.getSjrq()) && xclzqfxxx.getFxdfbs().equals("1")) {
//                    tempMsg.append("当月发行的债券，债权债务登记日应该在当月范围内" + SysConstants.spaceStr);
//                    result = true;
//                }
//            }
        } else {
            if (checknumList.contains("JS1406")) {
                if (StringUtils.isBlank(xclzqfxxx.getZqzwdj())) {
                    tempMsg.append("债权债务登记日不能为空" + SysConstants.spaceStr);
                    return true;
                }
            }
        }
        return result;
    }

    /**
     * 发行/兑付债券金额折人民币 属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowFxdfzqjezrmb(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getFxdfzqjezrmb())) {
            if (checknumList.contains("JS0759")) {
                if (xclzqfxxx.getFxdfzqjezrmb().length() > 20 || !CheckUtil.checkDecimal(xclzqfxxx.getFxdfzqjezrmb(), 2)) {
                    tempMsg.append("发行/兑付债券金额折人民币总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS2275")) {
                if (new BigDecimal(xclzqfxxx.getFxdfzqjezrmb()).compareTo(BigDecimal.ZERO) <= 0) {
                    tempMsg.append("发行/兑付债券金额折人民币应大于0" + SysConstants.spaceStr);
                    result = true;
                }
            }
            String bz = xclzqfxxx.getBz();
            String fxdfzqje = xclzqfxxx.getFxdfzqje();
            String fxdfzqjezrmb = xclzqfxxx.getFxdfzqjezrmb();
            if (checknumList.contains("JS2703") && StringUtils.isNotBlank(fxdfzqje) && StringUtils.isNotBlank(fxdfzqjezrmb) && StringUtils.isNotBlank(bz)) {
                if (bz.equals("CNY") && fxdfzqje.compareTo(fxdfzqjezrmb) != 0) {
                    tempMsg.append("币种为人民币的，发行/兑付债券金额折人民币应该与发行/兑付债券金额的值相等" + SysConstants.spaceStr);
                    return true;
                }
            }
        } else {
            if (checknumList.contains("JS1405")) {
                if (StringUtils.isBlank(xclzqfxxx.getFxdfzqjezrmb())) {
                    tempMsg.append("发行/兑付债券金额折人民币不能为空" + SysConstants.spaceStr);
                    return true;
                }
            }
        }
        return result;
    }

    /**
     * 发行/兑付债券金额属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowFxdfzqje(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        String fxdfzqje = xclzqfxxx.getFxdfzqje();
        if (checknumList.contains("JS2274") && StringUtils.isNotBlank(fxdfzqje)) {
            if (new BigDecimal(fxdfzqje).compareTo(BigDecimal.ZERO) <= 0) {
                tempMsg.append("发行/兑付债券金额应大于0" + SysConstants.spaceStr);
                result = true;
            }
        }
        if (StringUtils.isNotBlank(xclzqfxxx.getFxdfzqje())) {
            if (checknumList.contains("JS0758")) {
                if (xclzqfxxx.getFxdfzqje().length() > 20 || !CheckUtil.checkDecimal(xclzqfxxx.getFxdfzqje(), 2)) {
                    tempMsg.append("发行/兑付债券金额总长度不能超过20位，小数位必须为2位，小数位必须为2位" + SysConstants.spaceStr);
                    return true;
                }
            }
        } else {
            if (checknumList.contains("JS1404")) {
                if (StringUtils.isBlank(xclzqfxxx.getFxdfzqje())) {
                    tempMsg.append("发行/兑付债券金额不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 发行/兑付债券面值折人民币属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowFxdhzqmzzrmb(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        String bz = xclzqfxxx.getBz();//币种
        String fxdhzqmzzrmb = xclzqfxxx.getFxdhzqmzzrmb();//发行/兑付债券面值折人民币
        String fxdhz = xclzqfxxx.getFxdhz();//发行/兑付债券面值
        if (checknumList.contains("JS2273") && StringUtils.isNotBlank(fxdhzqmzzrmb)) {
            if (new BigDecimal(fxdhzqmzzrmb).compareTo(BigDecimal.ZERO) <= 0) {
                tempMsg.append("发行/兑付债券面值折人民币应大于0" + SysConstants.spaceStr);
                result = true;
            }
        }
        if (StringUtils.isNotBlank(xclzqfxxx.getFxdhzqmzzrmb())) {
            if (checknumList.contains("JS0757")) {
                if (xclzqfxxx.getFxdhzqmzzrmb().length() > 20 || !CheckUtil.checkDecimal(xclzqfxxx.getFxdhzqmzzrmb(), 2)) {
                    tempMsg.append("发行/兑付债券面值折人民币折人民币总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
                    return true;
                }
            }
            if (checknumList.contains("JS2702") && StringUtils.isNotBlank(bz) && StringUtils.isNotBlank(fxdhzqmzzrmb) && StringUtils.isNotBlank(fxdhz)) {
                if (bz.equals("CNY") && !fxdhz.equals(fxdhzqmzzrmb)) {
                    tempMsg.append("币种为人民币的，发行/兑付债券面值折人民币应该与发行/兑付债券面值相等" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1403")) {
                if (StringUtils.isBlank(xclzqfxxx.getFxdhzqmzzrmb())) {
                    tempMsg.append("发行/兑付债券面值折人民币不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 发行/兑付债券面值属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowFxdhz(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getFxdhz())) {
            if (checknumList.contains("JS0756")) {
                if (xclzqfxxx.getFxdhz().length() > 20 || !CheckUtil.checkDecimal(xclzqfxxx.getFxdhz(), 2)) {
                    tempMsg.append("发行/兑付债券面值总长度不能超过20位，小数位必须为2位" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS2272")) {
                if (new BigDecimal(xclzqfxxx.getFxdhz()).compareTo(BigDecimal.ZERO) <= 0) {
                    tempMsg.append("发行/兑付债券面值应大于0" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1402")) {
                if (StringUtils.isBlank(xclzqfxxx.getFxdhz())) {
                    tempMsg.append("发行/兑付债券面值不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 币种属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowBz(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getBz())) {
            String bz = xclzqfxxx.getBz();
            if (StringUtils.isNotBlank(xclzqfxxx.getBz())) {
                if (checknumList.contains("JS0048") && StringUtils.isNotBlank(bz)) {
                    List baseCode = customSqlUtil.getBaseCode(BaseCurrency.class);
                    if (!baseCode.contains(bz)) {
                        result = true;
                        tempMsg.append("存量债券发行信息中的币种需在《表示货币和资金的代码》（GB/T 12406）中的三位字母范围内" + SysConstants.spaceStr);
                    }
                }
            }
        } else {
            if (checknumList.contains("JS0048")) {
                if (StringUtils.isBlank(xclzqfxxx.getBz())) {
                    tempMsg.append("币种不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 续发次数属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowXfcs(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getXfcs())) {
            if (checknumList.contains("JS2283")) {
                if (!CheckUtil.checkeCyryPattern(xclzqfxxx.getXfcs())) {
                    tempMsg.append("续发次数应大于等于0" + SysConstants.spaceStr);
                    return true;
                }
            }
            if (checknumList.contains("JS0761")) {
                if (xclzqfxxx.getXfcs().length() > 3) {
                    tempMsg.append("续发次数字符长度不能超过3位的整数" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1410")) {
                if (StringUtils.isBlank(xclzqfxxx.getXfcs())) {
                    tempMsg.append("续发次数不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 债券信用级别属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowZqxyjg(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getZqxyjg())) {
            String zqxyjg = xclzqfxxx.getZqxyjg();
            if (checknumList.contains("JS0047") && StringUtils.isNotBlank(zqxyjg)) {
                if (!ZqFieldUtils.zqxyjbList().contains(zqxyjg)) {
                    result = true;
                    tempMsg.append("债券发行发生额信息表中的债券信用级别需在符合要求的值域范围内" + SysConstants.spaceStr);
                }
            }
        } else {
            if (checknumList.contains("JS1400")) {
                if (StringUtils.isBlank(xclzqfxxx.getZqxyjg())) {
                    tempMsg.append("债券信用级别不能为空" + SysConstants.spaceStr);
                    return true;
                }
            }
        }
        return result;
    }

    /**
     * 债券品种属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowZqpz(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        String zqpz = xclzqfxxx.getZqpz();//债券品种
        if (checknumList.contains("JS2921") && StringUtils.isNotBlank(zqpz)){
            try {
                if (zqpz.substring(0,4).equals("TB99") && zqpz.length() > 200){
                    result = true;
                    tempMsg.append("债券品种为TB99开头时，长度不能超过200" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (checknumList.contains("JS2922") && StringUtils.isNotBlank(zqpz)){
            try {
                if (!zqpz.substring(0,4).equals("TB99") && zqpz.length() > 6){
                    result = true;
                    tempMsg.append("债券品种不为TB99开头时，长度不能超过6" + SysConstants.spaceStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (StringUtils.isNotBlank(xclzqfxxx.getZqpz())) {
            if (checknumList.contains("JS0046")) {
                try {
                    if (!ZqFieldUtils.zqpzList.contains(xclzqfxxx.getZqpz()) && !xclzqfxxx.getZqpz().substring(0, 5).equals("TB99-")) {
                        result = true;
                        tempMsg.append("债券发行发生额信息表中的债券品种需在符合要求的值域范围内" + SysConstants.spaceStr);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (checknumList.contains("JS1399")) {
                if (StringUtils.isBlank(xclzqfxxx.getZqpz())) {
                    tempMsg.append("债券品种不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 3.债券总托管机构
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowZqztgjg(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getZqztgjg())) {
            String zqztgjg = xclzqfxxx.getZqztgjg();
            if (checknumList.contains("JS0045") && StringUtils.isNotBlank(zqztgjg)) {
                if (!(zqztgjg.equals("000") || zqztgjg.equals("400") || zqztgjg.equals("900"))) {
                    result = true;
                    tempMsg.append("债券发行发生额信息表中的债券总托管机构需在符合要求的值域范围内" + SysConstants.spaceStr);
                }
            }
        } else {
            if (checknumList.contains("JS1398")) {
                if (StringUtils.isBlank(xclzqfxxx.getZqztgjg())) {
                    tempMsg.append("债券总托管机构不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 2.债券代码属性校验
     *
     * @param checknumList
     * @param xclzqfxxx
     * @param tempMsg
     * @return
     */
    private boolean checkXclzqfxxxRowZqdm(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getZqdm())) {
            if (checknumList.contains("JS0755")) {
                if (xclzqfxxx.getZqdm().length() > 60) {
                    tempMsg.append("债券代码字符长度不能超过60" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS0752")) {
                if (CheckUtil.checkStr(xclzqfxxx.getZqdm())) {
                    tempMsg.append("债券代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1397")) {
                if (StringUtils.isBlank(xclzqfxxx.getZqdm())) {
                    tempMsg.append("债券代码不能为空" + SysConstants.spaceStr);
                    return true;
                }
            }
        }
        return result;
    }

    //1.金融机构属性校验
    private boolean checkXclzqfxxxRowFinanceorgcode(List<String> checknumList, Xzqfxfsexx xclzqfxxx, StringBuffer tempMsg) {
        boolean result = false;
        if (StringUtils.isNotBlank(xclzqfxxx.getFinanceorgcode())) {
            if (checknumList.contains("JS0754")) {
                if (xclzqfxxx.getFinanceorgcode().length() != 18) {
                    tempMsg.append("金融机构代码字符长度应该为18位" + SysConstants.spaceStr);
                    result = true;
                }
            }
            if (checknumList.contains("JS0751")) {
                if (CheckUtil.checkStr(xclzqfxxx.getFinanceorgcode())) {
                    tempMsg.append("金融机构代码字段内容中不得出现“？”、“！”、“^” 。其中“？”和“！”包含全角和半角两种格式。" + SysConstants.spaceStr);
                    result = true;
                }
            }
        } else {
            if (checknumList.contains("JS1396")) {
                if (StringUtils.isBlank(xclzqfxxx.getFinanceorgcode())) {
                    tempMsg.append("金融机构代码不能为空" + SysConstants.spaceStr);
                    result = true;
                }
            }
        }
        return result;
    }
}