package com.geping.etl.UNITLOAN.util;

import com.geping.etl.UNITLOAN.util.response.CodeDetail;

import java.util.List;

/**
 * jpa分页返回值封装
 * @Author: wangzd
 * @Date: 18:04 2019/9/6
 */
public class ResponseResult<T> {

    //总数量
    private long total;
    //数据
    private List<T> rows;

    //其它信息
    private Integer code;
    private String msg;


    public ResponseResult() {
    }

    public ResponseResult(CodeDetail codeDetail) {
        this.code = codeDetail.getCode();
        this.msg = codeDetail.getDetail();
    }
    public static ResponseResult success(CodeDetail codeDetail){
        return new ResponseResult(codeDetail);
    }

    public ResponseResult(long total, List<T> rows) {
        this.total = total;
        this.rows = rows;
    }

    public static ResponseResult success(long total, List rows){
        return new ResponseResult(total,rows);
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
