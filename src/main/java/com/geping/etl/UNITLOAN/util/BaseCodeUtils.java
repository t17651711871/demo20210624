package com.geping.etl.UNITLOAN.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseAindustryService;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseAreaService;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseBindustryService;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseCountryService;
import com.geping.etl.UNITLOAN.service.baseInfo.BaseCurrencyService;

/**
 * 基础代码获取工具
 * @author WuZengWen
 * @date 2020年6月11日 下午4:09:48
 */
public class BaseCodeUtils {
	
	@Autowired
	private BaseAindustryService ads;
	@Autowired
	private BaseAreaService bas;
	@Autowired
	private BaseBindustryService bbs;
	@Autowired
	private BaseCountryService cts;
	@Autowired
	private BaseCurrencyService ces;
	/**
	 * 一级行业代码
	 */
	public static List<BaseAindustry>  BaseAindustryList;
	/**
	 * 行政区划代码
	 */
	public static List<BaseArea>  BaseAreaList;
	/**
	 * 二级行业代码
	 */
	public static List<BaseBindustry>  BaseBindustryList;
	/**
	 * 国家代码
	 */
	public static List<BaseCountry>  BaseCountryList;
	/**
	 * 币种代码
	 */
	public static List<BaseCurrency>  BaseCurrencyList;
	private static class SingletonInstance {
		private static final List<BaseAindustry>  BaseAindustryList = new ArrayList<BaseAindustry>();
		private static final List<BaseArea>  BaseAreaList = new ArrayList<BaseArea>();
		private static final List<BaseBindustry>  BaseBindustryList = new ArrayList<BaseBindustry>();
		private static final List<BaseCountry>  BaseCountryList = new ArrayList<BaseCountry>();
		private static final List<BaseCurrency>  BaseCurrencyList = new ArrayList<BaseCurrency>();
	}
	private static List<BaseAindustry> getInstanceAindustry() {
		return SingletonInstance.BaseAindustryList;
    }
	private static List<BaseArea> getInstanceArea() {
		return SingletonInstance.BaseAreaList;
    }
	private static List<BaseBindustry> getInstanceBindustry() {
		return SingletonInstance.BaseBindustryList;
    }
	private static List<BaseCountry> getInstanceCountry() {
		return SingletonInstance.BaseCountryList;
    }
	private static List<BaseCurrency> getInstanceCurrency() {
		return SingletonInstance.BaseCurrencyList;
    }
	
	public void getBaseCodeResource(TableCodeEnum code) {
		if(TableCodeEnum.YJHYDM.equals(code)) {
			getInstanceAindustry();
			BaseAindustryList = ads.findAll();
		}else if(TableCodeEnum.XZQHDM.equals(code)) {
			getInstanceArea();
			BaseAreaList = bas.findAll();
		}else if(TableCodeEnum.EJHYDM.equals(code)) {
			getInstanceBindustry();
			BaseBindustryList = bbs.findAll();
		}else if(TableCodeEnum.GJDM.equals(code)) {
			getInstanceCountry();
			BaseCountryList = cts.findAll();
		}else if(TableCodeEnum.BZDM.equals(code)) {
			getInstanceCurrency();
			BaseCurrencyList = ces.findAll();
		}			
	}

	public static void getBaseCodeResource(BaseCurrencyService bcs) {
		if(BaseCurrencyList == null || BaseCurrencyList.size() == 0) {
			BaseCurrencyList = bcs.findAll();
		}			
	}
}
