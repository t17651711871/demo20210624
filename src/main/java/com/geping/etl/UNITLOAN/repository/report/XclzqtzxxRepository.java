package com.geping.etl.UNITLOAN.repository.report;

import com.geping.etl.UNITLOAN.entity.report.Xclzqfxxx;
import com.geping.etl.UNITLOAN.entity.report.Xclzqtzxx;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.repository.report
 * @USER: tangshuai
 * @DATE: 2021/3/30
 * @TIME: 16:15
 * @描述:
 */
public interface XclzqtzxxRepository extends JpaRepository<Xclzqtzxx, String>, JpaSpecificationExecutor<Xclzqtzxx> {
}
