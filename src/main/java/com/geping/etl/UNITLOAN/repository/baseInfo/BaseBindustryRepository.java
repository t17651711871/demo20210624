package com.geping.etl.UNITLOAN.repository.baseInfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry;

@Repository
public interface BaseBindustryRepository extends JpaRepository<BaseBindustry,String>,JpaSpecificationExecutor<BaseBindustry> {

}
