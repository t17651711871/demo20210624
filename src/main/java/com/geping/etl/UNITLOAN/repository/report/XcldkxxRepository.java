package com.geping.etl.UNITLOAN.repository.report;

import com.geping.etl.UNITLOAN.entity.report.Xcldkxx;
import com.geping.etl.UNITLOAN.entity.report.Xftykhxbl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: wangzd
 * @Date: 16:04 2020/6/9
 */
@Repository
public interface XcldkxxRepository extends JpaRepository<Xcldkxx, String>, JpaSpecificationExecutor<Xcldkxx> {


}
