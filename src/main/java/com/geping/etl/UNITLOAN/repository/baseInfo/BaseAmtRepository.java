package com.geping.etl.UNITLOAN.repository.baseInfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAmt;
@Repository
public interface BaseAmtRepository extends JpaRepository<BaseAmt,String>,JpaSpecificationExecutor<BaseAmt> {
	
	//查询数量
	@Query("SELECT COUNT(id) FROM BaseAmt")
	public Integer findTotal();
}
