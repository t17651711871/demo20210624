package com.geping.etl.UNITLOAN.repository.report;

import com.geping.etl.UNITLOAN.entity.report.Xftykhx;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @Author: wangzd
 * @Date: 2020/6/30
 */
public interface XftykhxRepository extends JpaRepository<Xftykhx, String>, JpaSpecificationExecutor<Xftykhx> {
}
