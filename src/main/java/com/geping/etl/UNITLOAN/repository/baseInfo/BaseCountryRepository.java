package com.geping.etl.UNITLOAN.repository.baseInfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry;

@Repository
public interface BaseCountryRepository extends JpaRepository<BaseCountry,String>,JpaSpecificationExecutor<BaseCountry> {

}
