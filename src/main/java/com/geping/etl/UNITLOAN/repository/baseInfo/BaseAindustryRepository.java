package com.geping.etl.UNITLOAN.repository.baseInfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry;

@Repository
public interface BaseAindustryRepository extends JpaRepository<BaseAindustry,String>,JpaSpecificationExecutor<BaseAindustry> {

}
