package com.geping.etl.UNITLOAN.repository.report;

import com.geping.etl.UNITLOAN.entity.report.Xcldkxx;
import com.geping.etl.UNITLOAN.entity.report.Xclwtdkxx;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author: wangzd
 * @Date: 16:04 2020/6/9
 */
@Repository
public interface XclwtdkxxRepository extends JpaRepository<Xclwtdkxx, String>, JpaSpecificationExecutor<Xclwtdkxx> {


}
