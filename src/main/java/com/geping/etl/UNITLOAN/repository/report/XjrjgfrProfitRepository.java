package com.geping.etl.UNITLOAN.repository.report;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.geping.etl.UNITLOAN.entity.report.XjrjgfrProfit;
@Repository
public interface XjrjgfrProfitRepository extends JpaRepository<XjrjgfrProfit,String>,JpaSpecificationExecutor<XjrjgfrProfit> {

	//查询数量
	@Query("SELECT COUNT(id) FROM XjrjgfrProfit where datastatus='0' or datastatus='1' or datastatus='2' or datastatus='3'")
	public Integer findTotal();
	
	//更改校验状态
	@Modifying
	@Query("UPDATE XjrjgfrProfit s SET s.checkstatus = ?1,operationname='' where s.checkstatus = '0' or s.checkstatus = '2'")
	public Integer updateXjrjgfrProfitOnCheckstatus(String checkStatus);
	
	//更改校验状态
	@Modifying
	@Query("UPDATE XjrjgfrProfit s SET s.checkstatus = ?1,operationname='' where s.id in (?2)")
	public Integer updateXjrjgfrProfitOnCheckstatus(String checkStatus,List<String> idList);
	
	//更改数据状态
	@Modifying
	@Query("UPDATE XjrjgfrProfit s SET s.datastatus = ?1,operator = ?2 where s.checkstatus = '1' and (s.datastatus ='0' or s.datastatus = '2') and departid like ?3")
	public Integer updateXjrjgfrProfitOnDatastatus(String dataStatus,String user,String departId);
	
	//更改数据状态
	@Modifying
	@Query("UPDATE XjrjgfrProfit s SET s.datastatus = ?1,operator = ?2 where s.id in (?3)")
	public Integer updateXjrjgfrProfitOnDatastatus(String dataStatus,String user,List<String> idList);
}
