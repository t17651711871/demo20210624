package com.geping.etl.UNITLOAN.repository.baseInfo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountryTwo;

/**    
*  
* @author liuweixin  
* @date 2020年11月4日 下午3:47:15  
*/
@Repository
public interface BaseCountryTwoRepository extends JpaRepository<BaseCountryTwo,String>,JpaSpecificationExecutor<BaseCountryTwo>{

	@Query("from BaseCountryTwo order by countryTwoCode")
	public List<BaseCountryTwo> getAll();
}
