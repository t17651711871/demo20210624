package com.geping.etl.UNITLOAN.repository.report;

import com.geping.etl.UNITLOAN.entity.report.XreportInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface XreportInfoRepository extends JpaRepository<XreportInfo, String>, JpaSpecificationExecutor<XreportInfo> {

    
}
