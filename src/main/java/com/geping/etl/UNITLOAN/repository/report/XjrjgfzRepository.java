package com.geping.etl.UNITLOAN.repository.report;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.geping.etl.UNITLOAN.entity.report.Xjrjgfz;
@Repository
public interface XjrjgfzRepository extends JpaRepository<Xjrjgfz,String>,JpaSpecificationExecutor<Xjrjgfz> {


	//更改校验状态
	@Modifying
	@Query("UPDATE Xjrjgfz s SET s.checkstatus = ?1,operationname='' where s.checkstatus = '0' or s.checkstatus = '2'")
	public Integer updateXjrjgfzOnCheckstatus(String checkStatus);
	
	//更改校验状态
	@Modifying
	@Query("UPDATE Xjrjgfz s SET s.checkstatus = ?1,operationname='' where s.id in (?2)")
	public Integer updateXjrjgfzOnCheckstatus(String checkStatus,List<String> idList);
	
	//更改数据状态
	@Modifying
	@Query("UPDATE Xjrjgfz s SET s.datastatus = ?1,operator = ?2 where s.checkstatus = '1' and (s.datastatus ='0' or s.datastatus = '2') and s.departid like ?3")
	public Integer updateXjrjgfzOnDatastatus(String dataStatus,String user,String departid);
	
	//更改数据状态
	@Modifying
	@Query("UPDATE Xjrjgfz s SET s.datastatus = ?1,operator = ?2 where s.id in (?3)")
	public Integer updateXjrjgfzOnDatastatus(String dataStatus,String user,List<String> idList);
}
