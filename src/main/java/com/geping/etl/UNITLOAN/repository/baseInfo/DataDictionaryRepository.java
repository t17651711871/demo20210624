package com.geping.etl.UNITLOAN.repository.baseInfo;

import com.geping.etl.UNITLOAN.entity.baseInfo.DataDictionary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DataDictionaryRepository extends JpaRepository<DataDictionary,String>,JpaSpecificationExecutor<DataDictionary> {

}
