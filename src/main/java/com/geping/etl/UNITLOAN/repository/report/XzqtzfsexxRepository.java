package com.geping.etl.UNITLOAN.repository.report;

import com.geping.etl.UNITLOAN.entity.report.Xzqfxfsexx;
import com.geping.etl.UNITLOAN.entity.report.Xzqtzfsexx;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.repository.report
 * @USER: tangshuai
 * @DATE: 2021/3/30
 * @TIME: 16:22
 * @描述:
 */
public interface XzqtzfsexxRepository extends JpaRepository<Xzqtzfsexx, String>, JpaSpecificationExecutor<Xzqtzfsexx> {
}
