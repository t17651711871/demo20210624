package com.geping.etl.UNITLOAN.repository.report;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.geping.etl.UNITLOAN.entity.report.Xgrkhxx;

/**    
*  
* @author liuweixin  
* @date 2020年11月3日 下午2:17:47  
*/
@Repository
public interface XgrkhxxRepository extends JpaRepository<Xgrkhxx, String>, JpaSpecificationExecutor<Xgrkhxx>{

}
