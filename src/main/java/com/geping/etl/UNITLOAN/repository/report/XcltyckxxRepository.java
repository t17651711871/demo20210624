package com.geping.etl.UNITLOAN.repository.report;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.geping.etl.UNITLOAN.entity.report.Xcltyckxx;

/**    
*  
* @author liuweixin  
* @date 2021年1月6日 上午10:12:07  
*/
@Repository
public interface XcltyckxxRepository extends JpaRepository<Xcltyckxx, String>, JpaSpecificationExecutor<Xcltyckxx>{

}
