package com.geping.etl.UNITLOAN.repository.report;

import com.geping.etl.UNITLOAN.entity.report.Xcltdmdzttzxx;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.repository.report
 * @USER: tangshuai
 * @DATE: 2021/5/17
 * @TIME: 17:27
 * @描述:
 */
@Repository
public interface XcltdmdzttzxxRepository extends JpaRepository<Xcltdmdzttzxx, String>, JpaSpecificationExecutor<Xcltdmdzttzxx> {
}
