package com.geping.etl.UNITLOAN.repository.baseInfo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.geping.etl.UNITLOAN.entity.baseInfo.BaseEducation;
import com.geping.etl.UNITLOAN.entity.baseInfo.BaseNation;

/**    
*  
* @author liuweixin  
* @date 2020年11月4日 下午3:49:16  
*/
@Repository
public interface BaseNationRepository extends JpaRepository<BaseNation,String>,JpaSpecificationExecutor<BaseNation>{

	@Query("from BaseNation order by nationCode")
	public List<BaseNation> getAll();
}
