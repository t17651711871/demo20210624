package com.geping.etl.UNITLOAN.common.excel.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/***
 *  导导入服务
 * @author liang.xu
 * @date 2021.4.12
 */
public interface ExcelImportService {



    /**
     * 导入Excel
     * @param request
     * @param response
     */
    void importExcel(HttpServletRequest request, HttpServletResponse response);
}