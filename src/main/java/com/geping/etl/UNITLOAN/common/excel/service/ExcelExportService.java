package com.geping.etl.UNITLOAN.common.excel.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/***
 *  导出服务
 * @author liang.xu
 * @date 2021.4.12
 */
public interface ExcelExportService {

    /**
     * 下载模板
     * @param request
     * @param response
     */
    void downloadExcelTemplate(HttpServletRequest request, HttpServletResponse response);



    /**
     * 导出Excel
     * @param request
     * @param response
     */
    void downloadExcel(HttpServletRequest request, HttpServletResponse response);


    /**
     * 导出历史Excel
     * @param request
     * @param response
     */
    void downloadHisExcel(HttpServletRequest request, HttpServletResponse response);
}