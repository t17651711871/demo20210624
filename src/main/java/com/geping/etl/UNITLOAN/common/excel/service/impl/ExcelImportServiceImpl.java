package com.geping.etl.UNITLOAN.common.excel.service.impl;

import com.geping.etl.UNITLOAN.common.excel.*;
import com.geping.etl.UNITLOAN.common.excel.service.ExcelImportService;
import com.geping.etl.UNITLOAN.common.report.ReportHelperService;
import com.geping.etl.UNITLOAN.controller.dataManager.immportExcel.XCommonExcel;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.UNITLOAN.util.ExcelUploadUtil;
import com.geping.etl.UNITLOAN.util.ReflectionUtils;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import net.sf.json.JSONObject;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.PrintWriter;

@Service
public class ExcelImportServiceImpl implements ExcelImportService {

    @Autowired
    private ExcelUploadUtil excelUploadUtil;

    @Autowired
    private DepartUtil departUtil;

    @Autowired
    private XcommonService commonService;

    @Autowired
    private CustomSqlUtil customSqlUtil;


    @Autowired
    private ReportHelperService reportHelperService;

    @Override
    public void importExcel(HttpServletRequest request, HttpServletResponse response) {
        Sys_UserAndOrgDepartment sys_user=((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));
        response.setContentType("text/html");
        response.setContentType("text/plain; charset=utf-8");
        String url = request.getRequestURI();
        String className = url.substring(url.lastIndexOf("/") + 12);
        File uploadFile = null;
        OPCPackage opcPackage=null;
        PrintWriter out = null;
        try {
            Class clazz = ReflectionUtils.getEntityClass(className);
            out = response.getWriter();
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multiRequest.getFile("excelfile");
            JSONObject json = new JSONObject();
            if (file == null) {
                json.put("msg", "导入文件为空");
            } else {
                uploadFile = excelUploadUtil.uploadFile(file);
                opcPackage = OPCPackage.open(uploadFile);
                String[] headers = HeadersZh.getHeadersZhMap().get(className);
                String[] values = HeadersEnglish.getHeadersEngMap().get(className);
                String[] amountFields = AmountFields.getAmountMap().get(className);
                String[] dateFields = DateFields.getDateMap().get(className);
                String[] rateFields= RateFields.getRateMap().get(className);
                Object t=clazz.newInstance();
                XCommonExcel commonExcel = new XCommonExcel<>(sys_user, opcPackage, customSqlUtil,t, headers, values,amountFields,dateFields,null,rateFields);
                commonExcel.process(0);
                String departId = departUtil.getDepart(sys_user);
                if (commonExcel.msg.toString().length() == 0) {
                    commonService.importDelete(ReflectionUtils.getTableName(clazz),departId);
                    commonExcel.process(1);
                    json.put("msg","导入成功");
                } else {
                    json.put("msg",commonExcel.msg.toString());
                }
            }
            String fileName=reportHelperService.getFileName(className);
            String logContext="导入成功".equals(json.getString("msg"))?json.getString("msg"):"导入失败";
            customSqlUtil.saveLog(fileName+"->"+logContext,"导入");
            out.write(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                opcPackage.close();
                //将上传的文件删除
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}