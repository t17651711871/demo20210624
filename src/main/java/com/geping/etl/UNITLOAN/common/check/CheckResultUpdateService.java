package com.geping.etl.UNITLOAN.common.check;

import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;

/***
 *  校验结果操作服务
 * @author liang.xu
 * @date 2021.4.17
 */
public interface CheckResultUpdateService {


    /**
     * 处理校验成功结果
     * @param baseCheckParam
     */
    boolean tryHandleRightId(CheckParamContext baseCheckParam);


    /**
     * 处理校验失败结果
     * @param baseCheckParam
     */
   boolean tryHandleErrorId(CheckParamContext baseCheckParam);

}