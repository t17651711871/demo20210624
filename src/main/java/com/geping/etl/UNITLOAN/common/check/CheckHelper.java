package com.geping.etl.UNITLOAN.common.check;

import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/***
 *  校验帮助类
 * @author liang.xu
 * @date 2021.4.17
 */
@Service
public class CheckHelper {

    private static Logger logger = LoggerFactory.getLogger(CheckHelper.class);

    @Autowired
    private CustomSqlUtil customSqlUtil;

    /**
     * 表件校验
     * @param header
     * @param  checkParamContext
     * @param sql
     */
    public void inTableCalibration(String header,CheckParamContext checkParamContext, String sql,String errorinfo) {
        List<Object[]> exist = customSqlUtil.executeQuery(sql);
        if (exist != null && exist.size() > 0) {
            exist.forEach(errId -> {
                checkParamContext.getErrorId().add(String.valueOf(errId[0]));
                if (!checkParamContext.getErrorMsg().containsKey(String.valueOf(errId[0]))) {
                    checkParamContext.getErrorMsg().put(String.valueOf(errId[0]),String.format(header,errId[1],errId[2]) );
                }
                String str = checkParamContext.getErrorMsg().get(String.valueOf(errId[0]));
                str = str + errorinfo + "|";
                checkParamContext.getErrorMsg().put(String.valueOf(errId[0]), str);
            });
        }
    }

    /**
     * 行校验
     */
/*    public void rowCalibration(Connection connection, String sql, Class claz, List<String> xclzqfxxxCheckNums, String  header, LinkedHashMap<String, String>errorMsg, List<String> errorId, List<String> rightId, CheckRowData checkRow) {
        ResultSet resultSet = JDBCUtils.Query(connection, sql);
        try {
            while (true) {
                if (!resultSet.next()) break;
                Object t=claz.newInstance();
                Stringutil.getEntity(resultSet, t);
                checkRow.checkRow(header,xclzqfxxxCheckNums, t, errorMsg, errorId, rightId);
            }
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }
    }*/

    /**
     * 处理校验结果
     */
    public  void handleRowCheckResult(boolean isError, String id, String header, String tempMsg, CheckParamContext checkParamContext){
        if (isError) {
            if(!checkParamContext.getErrorMsg().containsKey(id)) {
                checkParamContext.getErrorMsg().put(id, header);
            }
            String str = checkParamContext.getErrorMsg().get(id);
            str = str + tempMsg;
            checkParamContext.getErrorMsg().put(id, str);
            checkParamContext.getErrorId().add(id);
        } else {
            if(!checkParamContext.getErrorId().contains(id)) {
                checkParamContext.getRightId().add(id);
            }
        }
    }
}