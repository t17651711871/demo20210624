package com.geping.etl.UNITLOAN.common.check.impl;


import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.common.check.CheckResultUpdateService;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.util.CustomSqlUtil;
import com.geping.etl.UNITLOAN.util.DownloadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Map;

@Service
public class CheckResultUpdateServiceImpl  implements CheckResultUpdateService {

    @Autowired
    private CustomSqlUtil customSqlUtil;

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Override
    public boolean tryHandleRightId(CheckParamContext baseCheckParam) {
        if (baseCheckParam.getRightId()!=null && baseCheckParam.getRightId().size()>0){
            baseCheckParam.addSetMap("checkstatus","1");
            baseCheckParam.addSetMap("operationname"," ");
            customSqlUtil.updateByWhereBatch(baseCheckParam.getClassName().toLowerCase(),baseCheckParam.getSetMap(),baseCheckParam.getRightId());
            return true;
        }
        return false;
    }

    @Override
    public boolean tryHandleErrorId(CheckParamContext baseCheckParam) {
        if (baseCheckParam.getErrorId()!=null && baseCheckParam.getErrorId().size()>0) {
            StringBuffer errorMsgAll = new StringBuffer("");
            for (Map.Entry<String, String> entry : baseCheckParam.getErrorMsg().entrySet()) {
                errorMsgAll.append(entry.getValue() + "\r\n");
            }
            String target = suisService.findAll().get(0).getMessagepath();
            DownloadUtil.downLoad(errorMsgAll.toString(), target + File.separator + "checkout", baseCheckParam.getFileName() + ".txt");
            baseCheckParam.addSetMap("checkstatus", "2");
            baseCheckParam.addSetMap("operationname", " ");
            customSqlUtil.updateByWhereBatch(baseCheckParam.getClassName().toLowerCase(), baseCheckParam.getSetMap(), baseCheckParam.getErrorId());
            return true;
        }
        return false;
    }
}