package com.geping.etl.UNITLOAN.common.check.bean.dto.selectDTO;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.common.check.bean.dto.selectDTO
 * @USER: tangshuai
 * @DATE: 2021/5/17
 * @TIME: 17:28
 * @描述:
 */
public class SelectXcltdmdzttzxxDTO extends BaseSelectParam{
    private String operationtimeParam;

    public String getOperationtimeParam() {
        return operationtimeParam;
    }

    public void setOperationtimeParam(String operationtimeParam) {
        this.operationtimeParam = operationtimeParam;
    }
}
