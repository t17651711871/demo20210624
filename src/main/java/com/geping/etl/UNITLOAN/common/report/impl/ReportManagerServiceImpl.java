package com.geping.etl.UNITLOAN.common.report.impl;

import com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet;
import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.report.ReportInfo;
import com.geping.etl.UNITLOAN.common.report.ReportManagerService;
import com.geping.etl.UNITLOAN.entity.report.XreportInfo;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.util.DownloadUtil;
import com.geping.etl.UNITLOAN.util.ReflectionUtils;
import com.geping.etl.UNITLOAN.util.Stringutil;
import com.geping.etl.common.entity.Report_Info;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import com.geping.etl.common.service.Report_InfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class ReportManagerServiceImpl implements ReportManagerService {

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private XcommonService commonService;

    @Autowired
    private Report_InfoService report_InfoService;

    @Override
    @Transactional
    public void createReport(HttpServletResponse response, HttpSession session, HttpServletRequest request) {
        String url = request.getRequestURI();
        String className = url.substring(url.lastIndexOf("/") + 13);
        doCreateReport(response,session,request,className);
    }

    public void doCreateReport(HttpServletResponse response, HttpSession session, HttpServletRequest request,String className) {
        Class clazz = ReflectionUtils.getEntityClass(className);
        SUpOrgInfoSet sUpOrgInfoSet = suisService.findAll().get(0);
        String messagePath = sUpOrgInfoSet.getMessagepath();
        String bankcodewangdian = sUpOrgInfoSet.getBankcodewangdian();
        String date = request.getParameter("date");
        try {
            List<Object> list = Stringutil.getReportList(clazz.newInstance(), ReflectionUtils.getTableName(clazz));
            String localDate = new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(date));
            String target = messagePath + File.separator + "report" + File.separator + localDate;
            Report_Info reportInfo=report_InfoService.findByCode(ReportInfo.getReportCodeByEntityName(className));
            String fileName = bankcodewangdian + SysConstants.UNDERLINE+reportInfo.getEnName()+SysConstants.UNDERLINE + localDate + ".dat";
            fileName = DownloadUtil.writeZIP(list, target, fileName);
            Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment) session.getAttribute("sys_User"));
            String departId = "%%";
            String orgid = sys_user.getOrgid();
            commonService.moveToHistoryTable(ReflectionUtils.getTableName(clazz), orgid, departId);
            XreportInfo xreportInfo = new XreportInfo(Stringutil.getUUid(), fileName, target + File.separator + fileName, SysConstants.yyyyMMddHHmmss.format(new Date()), sys_user.getLoginid(), orgid, sys_user.getDepartid());
            commonService.save(xreportInfo);
            PrintWriter out = response.getWriter();
            out.write(String.valueOf(list.size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    @Transactional
    public void createReportByCode(HttpServletResponse response, HttpSession session, HttpServletRequest request) {
        String url = request.getRequestURI();
        String code = url.substring(url.lastIndexOf("/") + 19);
        String className=ReportInfo.getReportEntityName(code);
        doCreateReport(response,session,request,className);
    }
}