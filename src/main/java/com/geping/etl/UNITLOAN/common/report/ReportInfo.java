package com.geping.etl.UNITLOAN.common.report;


import com.geping.etl.UNITLOAN.entity.report.*;

import java.util.HashMap;
import java.util.Map;

/***
 *   报表信息
 * @author liang.xu
 * @date 2021.4.10
 */
public class ReportInfo {

    /**
     * 存量单位贷款基础数据信息 报表code
     */
    private static String CLDKXX = "CLDKXX";


    /**
     * 单位贷款发生额信息 报表code
     */
    private static String DKFSXX = "DKFSXX";

    /**
     * 单位贷款担保合同信息 报表code
     */
    private static String DKDBHT = "DKDBHT";

    /**
     * 单位贷款担保物信息 报表code
     */
    private static String DKDBWX = "DKDBWX";

    /**
     * 金融机构（分支机构）基础信息 报表code
     */
    private static String JRJGFZ = "JRJGFZ";


    /**
     * 非同业单位客户基础信息 报表code
     */
    private static String FTYKHX = "FTYKHX";


    /**
     * 金融机构（法人）基础信息 报表code
     */
    private static String JRJGFR = "JRJGFR";


    /**
     * 存量个人贷款信息 报表code
     */
    private static String CLGRDKXX = "CLGRDKXX";


    /**
     * 个人贷款发生额信息 报表code
     */
    private static String GRDKFSXX = "GRDKFSXX";

    /**
     * 个人客户基础信息 报表code
     */
    private static String GRKHXX = "GRKHXX";


    /**
     * 存量委托贷款信息 报表code
     */
    private static String CLWTDKXX = "CLWTDKXX";


    /**
     * 委托贷款发生额信息 报表code
     */
    private static String WTDKFSE = "WTDKFSE";


    /**
     * 存量同业借贷信息 报表code
     */
    private static String CLTYJDXX = "CLTYJDXX";


    /**
     * 同业借贷发生额信息 报表code
     */
    private static String TYJDFSEXX = "TYJDFSEXX";

    /**
     * 同业客户基础信息 报表code
     */
    private static String TYKHJCXX = "TYKHJCXX";

    /**
     * 存量同业存款信息 报表code
     */
    private static String CLTYCKXX = "CLTYCKXX";


    /**
     * 同业存款发生额信息 报表code
     */
    private static String TYCKFSEXX = "TYCKFSEXX";

    /**
     * 存量债券投资信息 报表code
     */
    private static String CLZQTZXX = "CLZQTZXX";

    /**
     * 债券投资发生额信息 报表code
     */
    private static String ZQTZFSEXX = "ZQTZFSEXX";

    /**
     * 存量债券发行信息 报表code
     */
    private static String CLZQFXXX = "CLZQFXXX";

    /**
     * 债券发行发生额信息 报表code
     */
    private static String ZQFXFSEXX = "ZQFXFSEXX";
    /**
     * 存量股权投资信息 报表code
     */
    private static String CLGQTZXX = "CLGQTZXX";
    /**
     * 股权投资发生额信息 报表code
     */
    private static String GQTZFSEXX = "GQTZFSEXX";
    /**
     * 存量特定目的载体投资信息 报表code
     */

    private static String CLTDMDZTTZXX = "CLTDMDZTTZXX";
    /**
     * 特定目的载体投资发生额信息 报表code
     */
    private static String TDMDZTTZFSEXX = "TDMDZTTZFSEXX";
        /**
     * 报表code和实体映射表
     */
    private static Map<String, Class> reportEntityMap = new HashMap<>();

    static {
        reportEntityMap.put(CLDKXX, Xcldkxx.class);
        reportEntityMap.put(DKFSXX, Xdkfsxx.class);
        reportEntityMap.put(DKDBHT, Xdkdbht.class);
        reportEntityMap.put(DKDBWX, Xdkdbwx.class);
        reportEntityMap.put(JRJGFZ, Xjrjgfz.class);
        reportEntityMap.put(FTYKHX, Xftykhx.class);
        reportEntityMap.put(JRJGFR, null);
        reportEntityMap.put(CLGRDKXX, Xclgrdkxx.class);
        reportEntityMap.put(GRDKFSXX, Xgrdkfsxx.class);
        reportEntityMap.put(GRKHXX, Xgrkhxx.class);
        reportEntityMap.put(CLWTDKXX, Xclwtdkxx.class);
        reportEntityMap.put(WTDKFSE, Xwtdkfse.class);
        reportEntityMap.put(CLTYJDXX, Xcltyjdxx.class);
        reportEntityMap.put(TYJDFSEXX, Xtyjdfsexx.class);
        reportEntityMap.put(TYKHJCXX, Xtykhjcxx.class);
        reportEntityMap.put(CLTYCKXX, Xcltyckxx.class);
        reportEntityMap.put(TYCKFSEXX, Xtyckfsexx.class);
        reportEntityMap.put(CLZQTZXX, Xclzqtzxx.class);
        reportEntityMap.put(ZQTZFSEXX, Xzqtzfsexx.class);
        reportEntityMap.put(CLZQFXXX, Xclzqfxxx.class);
        reportEntityMap.put(ZQFXFSEXX, Xzqfxfsexx.class);
        reportEntityMap.put(CLGQTZXX, Xclgqtzxx.class);
        reportEntityMap.put(GQTZFSEXX, Xgqtzfsexx.class);
        reportEntityMap.put(CLTDMDZTTZXX, Xcltdmdzttzxx.class);
        reportEntityMap.put(TDMDZTTZFSEXX, Xtdmdzttzfsexx.class);
    }


    /**
     * 获取报表
     *
     * @param code
     * @return
     */
    public static Class getReport(String code) {
        return reportEntityMap.get(code);
    }

    /**
     * 获取报表实体名
     *
     * @param code
     * @return
     */
    public static String getReportEntityName(String code) {
        return reportEntityMap.get(code).getSimpleName();
    }

    /**
     * 根据实体名查询报表code
     *
     * @param entityName
     * @return
     */
    public static String getReportCodeByEntityName(String entityName) {
        for (Map.Entry<String, Class> entry : reportEntityMap.entrySet()) {
            if(entry.getValue()==null){
                continue;
            }
            if (entityName.equals(entry.getValue().getSimpleName())) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(ReportInfo.getReportCodeByEntityName("Xclzqfxxx"));
    }

}