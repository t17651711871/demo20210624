package com.geping.etl.UNITLOAN.common.check.bean.dto.selectDTO;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.common.check.bean.dto.selectDTO
 * @USER: tangshuai
 * @DATE: 2021/4/20
 * @TIME: 10:35
 * @描述:
 */
public class SelectXzqtzfsexxDTO extends BaseSelectParam{
    private String operationtimeParam;

    public String getOperationtimeParam() {
        return operationtimeParam;
    }

    public void setOperationtimeParam(String operationtimeParam) {
        this.operationtimeParam = operationtimeParam;
    }
}
