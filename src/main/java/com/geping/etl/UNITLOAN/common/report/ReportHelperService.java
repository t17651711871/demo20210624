package com.geping.etl.UNITLOAN.common.report;

/***
 *   报表帮助类
 * @author liang.xu
 * @date 2021.4.10
 */
public interface ReportHelperService {

    /**
     * 获取文件名
     *
     * @param className
     * @return
     */
    String getFileName(String className);
}