package com.geping.etl.UNITLOAN.common.excel;

import java.util.HashMap;
import java.util.Map;

/***
 *   Excel date 映射
 * @author liang.xu
 * @date 2021.4.12
 */
public class DateFields {

    /**
     * date 映射
     */
    private static Map<String, String[]> DATE_MAP = new HashMap<>();


    static {
        DATE_MAP.put("A", new String[]{"name"});
    }

    public static Map<String, String[]> getDateMap() {
        return DATE_MAP;
    }
}