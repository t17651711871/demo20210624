package com.geping.etl.UNITLOAN.common.excel;


import com.geping.etl.UNITLOAN.common.excel.service.ExcelExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/***
 *  导出相关
 * @author liang.xu
 * @date 2021.4.12
 */
@RestController
public class ExcelExportController {

    @Autowired
    private ExcelExportService excelExportService;

    //导出模板
    @GetMapping(value = "downloadExcelTemplate*")
    public void downloadExcelTemplate(HttpServletRequest request, HttpServletResponse response) {
        excelExportService.downloadExcelTemplate(request, response);
    }


    //导出excel
    @GetMapping(value = "exportExcel*")
    public void exportExcel(HttpServletRequest request, HttpServletResponse response) {
        excelExportService.downloadExcel(request, response);
    }

    //导出excel
    @GetMapping(value = "exportExcelH*")
    public void exportExcelH(HttpServletRequest request, HttpServletResponse response) {
        excelExportService.downloadHisExcel(request, response);
    }
}
