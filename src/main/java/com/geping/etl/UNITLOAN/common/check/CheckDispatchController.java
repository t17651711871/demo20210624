package com.geping.etl.UNITLOAN.common.check;

import com.geping.etl.SHGJG.service.UpOrgInfoSet.SUpOrgInfoSetService;
import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import com.geping.etl.UNITLOAN.common.report.ReportHelperService;
import com.geping.etl.UNITLOAN.util.DownloadUtil;
import com.geping.etl.UNITLOAN.util.ReflectionUtils;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/***
 *   校验相关处理
 * @author liang.xu
 * @date 2021.4.10
 */
@RestController
public class CheckDispatchController {


    private  Logger logger = LoggerFactory.getLogger(CheckDispatchController.class);

    @Autowired
    private SUpOrgInfoSetService suisService;

    @Autowired
    private ReportHelperService reportHelperService;

    /**
     * 校验转发
     */
    @PostMapping("checkDispatch*")
    public  void checkResult(HttpServletRequest request,HttpServletResponse response) {
        String target = suisService.findAll().get(0).getMessagepath();
        String url = request.getRequestURI();
        String className = url.substring(url.lastIndexOf("/") + 14);
        String fileName = reportHelperService.getFileName(className);
        CheckParamContext checkParamContext = new CheckParamContext().setId(request.getParameter("id")).setClassName(className).setFileName(fileName).setTarget(target).setClazz(ReflectionUtils.getEntityClass(className));
        checkParamContext.setSysUser((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));
        JSONObject json= CheckProcessorContainerService.doCheckData(className, checkParamContext);
        try {
            PrintWriter out = response.getWriter();
            out.write(json.toString());
            out.flush();
            out.close();
        } catch (IOException e) {
            logger.error("校验转发发生错误",e);
        }
    }

    /**
     * 下载校验结果
     *
     * @param response
     */
    @GetMapping("downLoadCheckResult*")
    public void downLoadCheckResult(HttpServletRequest request, HttpServletResponse response) {
        String target = suisService.findAll().get(0).getMessagepath();
        String url = request.getRequestURI();
        String className = url.substring(url.lastIndexOf("/") + 20);
        String fileName = reportHelperService.getFileName(className);
        DownloadUtil.downLoadCheckFile(response, target + File.separator + "checkout", fileName + ".txt");
    }

}
