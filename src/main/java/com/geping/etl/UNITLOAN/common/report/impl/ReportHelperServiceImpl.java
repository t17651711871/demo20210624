package com.geping.etl.UNITLOAN.common.report.impl;

import com.geping.etl.UNITLOAN.common.report.ReportHelperService;
import com.geping.etl.UNITLOAN.common.report.ReportInfo;
import com.geping.etl.common.entity.Report_Info;
import com.geping.etl.common.service.Report_InfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportHelperServiceImpl implements ReportHelperService {

    @Autowired
    private Report_InfoService reportInfoService;


    @Override
    public String getFileName(String className) {
        Report_Info reportInfo = reportInfoService.findByCode(ReportInfo.getReportCodeByEntityName(className));
        if (reportInfo != null) {
            return reportInfo.getName();
        }
        return "";
    }
}