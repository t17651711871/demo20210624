package com.geping.etl.UNITLOAN.common.report;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/***
 *   报表管理
 * @author liang.xu
 * @date 2021.4.12
 */
public interface ReportManagerService {

    /**
     * 生成报文
     *
     * @param response
     * @param session
     * @param request
     */
    void createReport(HttpServletResponse response, HttpSession session, HttpServletRequest request);

    /**
     * 生成报文,根据code
     * @param response
     * @param session
     * @param request
     */
    void createReportByCode(HttpServletResponse response, HttpSession session, HttpServletRequest request);
}