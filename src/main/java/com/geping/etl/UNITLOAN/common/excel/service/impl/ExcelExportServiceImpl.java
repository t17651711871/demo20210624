package com.geping.etl.UNITLOAN.common.excel.service.impl;

import com.geping.etl.UNITLOAN.SysConstants;
import com.geping.etl.UNITLOAN.common.excel.service.ExcelExportService;
import com.geping.etl.UNITLOAN.common.excel.utils.ExcelUtils;
import com.geping.etl.UNITLOAN.common.report.ReportHelperService;
import com.geping.etl.UNITLOAN.controller.dataManager.XCommenFileOut;
import com.geping.etl.UNITLOAN.service.report.XcommonService;
import com.geping.etl.UNITLOAN.util.DepartUtil;
import com.geping.etl.common.entity.Sys_UserAndOrgDepartment;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/***
 *  导出服务
 * @author liang.xu
 * @date 2021.4.12
 */
@Service
public class ExcelExportServiceImpl implements ExcelExportService {

    private Logger logger = LoggerFactory.getLogger(ExcelExportServiceImpl.class);

    @Autowired
    private DepartUtil departUtil;

    @Autowired
    private XcommonService commonService;

    @Autowired
    private ReportHelperService reportHelperService;

    @Override
    public void downloadExcelTemplate(HttpServletRequest request, HttpServletResponse response) {
        String url = request.getRequestURI();
        String className = url.substring(url.lastIndexOf("/") + 22);
        String fileName = reportHelperService.getFileName(className)+SysConstants.EXCEL_SUFFIX;
        try {
            Map<String, String> headers = ExcelUtils.getExcelHeads(className);

            XCommenFileOut.fileOut(request, response, new ArrayList<>(), fileName, headers, className);
        } catch (Exception e) {
            logger.error("downloadExcel 发生错误", e);
        }
    }

    @Override
    public void downloadExcel(HttpServletRequest request, HttpServletResponse response) {
        String url = request.getRequestURI();
        String datastatus = request.getParameter("datastatus");
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));
        String className = url.substring(url.lastIndexOf("/") + 12);
        String fileName = reportHelperService.getFileName(className)+SysConstants.EXCEL_SUFFIX;
        try {
            Class clazz = Class.forName(SysConstants.REPORT_ENTITY_PKG + className);
            List<T> list = queryData(request, response, sys_user, clazz);
            Map<String, String> headers = ExcelUtils.getExcelHeads(className);
            XCommenFileOut.fileOut(request, response, list, fileName, headers, className);
        } catch (Exception e) {
            logger.error("downloadExcel 发生错误", e);
        }
    }

    @Override
    public void downloadHisExcel(HttpServletRequest request, HttpServletResponse response) {
        String url = request.getRequestURI();
        Sys_UserAndOrgDepartment sys_user = ((Sys_UserAndOrgDepartment) request.getSession().getAttribute("sys_User"));
        String className = url.substring(url.lastIndexOf("/") + 13);
        String fileName = reportHelperService.getFileName(className.replace("H",""))+SysConstants.EXCEL_SUFFIX;
        try {
            Class clazz = Class.forName(SysConstants.REPORT_ENTITY_PKG + className);
            List<T> list = queryData(request, response, sys_user, clazz);
            Map<String, String> headers = ExcelUtils.getExcelHeads(className);
            XCommenFileOut.fileOut(request, response, list, fileName, headers, className);
        } catch (Exception e) {
            logger.error("downloadExcel 发生错误", e);
        }
    }

    /**
     * 查询导出数据
     *
     * @param request
     * @param response
     * @param sys_user
     * @param clazz
     * @return
     */
    private List<T> queryData(HttpServletRequest request, HttpServletResponse response, Sys_UserAndOrgDepartment sys_user, Class clazz) {
        String departId = departUtil.getDepart(sys_user);
        String datastatus = request.getParameter("datastatus");
        Enumeration<String> enumeration = request.getParameterNames();
        Map<String, String> param = new HashMap<>();
        while (enumeration.hasMoreElements()) {
            String key = enumeration.nextElement();
            if (key.endsWith("Param") && StringUtils.isNotBlank(request.getParameter(key))) {
                param.put(key.substring(0, key.length() - 5), request.getParameter(key));
            }
        }
        String orgId = sys_user.getOrgid();
        param.put("departid", departId);
        param.put("orgid", orgId);
        param.put("datastatus", datastatus);
        String id = request.getParameter("id");
        if (StringUtils.isNotBlank(id)) {
            id = id.substring(0, id.length() - 1);
            id = "'" + id.replace(",", "','") + "'";
            param.put("id", id);
        }
        if (StringUtils.isNotBlank(datastatus)) {
            param.put("datastatus", datastatus);
        }
        return commonService.findByFielOut(param, clazz);
    }
}