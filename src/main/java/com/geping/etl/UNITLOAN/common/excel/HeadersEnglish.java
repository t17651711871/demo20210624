package com.geping.etl.UNITLOAN.common.excel;

import java.util.HashMap;
import java.util.Map;

/***
 *   Excel 表头英文映射
 * @author liang.xu
 * @date 2021.4.12
 */
public class HeadersEnglish {

    /**
     * 表头英文映射
     */
    private static Map<String, String[]> HEADERS_ENG_MAP = new HashMap<>();

    /**
     * 存量债券投资信息
     */
    static {
        HEADERS_ENG_MAP.put("Xclzqtzxx", new String[]{"financeorgcode","financeorginnum","zqdm","zqztgjg","zqpz","zqxyjg","bz","zqye","zqyezrmb","zqzwdj","qxr" ,"dfrq","pmll","fxrzjdm","fxrdqdm","fxrhy","fxrqygm","fxrjjcf","fxrgmjjbm","sjrq"});
    }
    /**
     * 存量债券投资信息历史表
     */
    static {
        HEADERS_ENG_MAP.put("XclzqtzxxH", new String[]{"financeorgcode","financeorginnum","zqdm","zqztgjg","zqpz","zqxyjg","bz","zqye","zqyezrmb","zqzwdj","qxr" ,"dfrq","pmll","fxrzjdm","fxrdqdm","fxrhy","fxrqygm","fxrjjcf","fxrgmjjbm","sjrq"});
    }

    /**
     * 债券投资发生额信息
     */
    static {
        HEADERS_ENG_MAP.put("Xzqtzfsexx", new String[]{"financeorgcode","financeorginnum","zqdm","zqztgjg","zqpz","zqxyjg","bz","zqzwdj","qxr","dfrq","pmll","fxrzjdm","fxrdqdm","fxrhy","fxrqygm","fxrjjcf","fxrgmjjbm","jyrq","jylsh","cjje","cjjezrmb","mrmcbz","sjrq"});
    }
    /**
     * 债券投资发生额信息历史表
     */
    static {
        HEADERS_ENG_MAP.put("XzqtzfsexxH", new String[]{"financeorgcode","financeorginnum","zqdm","zqztgjg","zqpz","zqxyjg","bz","zqzwdj","qxr","dfrq","pmll","fxrzjdm","fxrdqdm","fxrhy","fxrqygm","fxrjjcf","fxrgmjjbm","jyrq","jylsh","cjje","cjjezrmb","mrmcbz","sjrq"});
    }
    /**
     * 存量债券发行信息
     */
    static {
        HEADERS_ENG_MAP.put("Xclzqfxxx", new String[]{"financeorgcode","zqdm","zqztgjg","zqpz","zqxyjg","xfcs","bz","qmzqmz","qmzqmzzrmb","qmzqye","qmzqyezrmb","zqzwdj","qxr","dfrq","fxfs","pmll","sjrq"});
    }

    /**
     * 存量债券发行信息历史表
     */
    static {
        HEADERS_ENG_MAP.put("XclzqfxxxH", new String[]{"financeorgcode","zqdm","zqztgjg","zqpz","zqxyjg","xfcs","bz","qmzqmz","qmzqmzzrmb","qmzqye","qmzqyezrmb","zqzwdj","qxr","dfrq","fxfs","pmll","sjrq"});
    }

    /**
     * 债券发行发生额信息
     */
    static {
        HEADERS_ENG_MAP.put("Xzqfxfsexx", new String[]{"financeorgcode","zqdm","zqztgjg","zqpz","zqxyjg","xfcs","bz","fxdhz","fxdhzqmzzrmb","fxdfzqje","fxdfzqjezrmb","zqzwdj","qxr","dfrq","jyrq","pmll","fxdfbs","sjrq"});
    }
    /**
     * 债券发行发生额信息历史表
     */
    static {
        HEADERS_ENG_MAP.put("XzqfxfsexxH", new String[]{"financeorgcode","zqdm","zqztgjg","zqpz","zqxyjg","xfcs","bz","fxdhz","fxdhzqmzzrmb","fxdfzqje","fxdfzqjezrmb","zqzwdj","qxr","dfrq","jyrq","pmll","fxdfbs","sjrq"});
    }
    /**
     * 存量股权投资信息
     */
    static {
        HEADERS_ENG_MAP.put("Xclgqtzxx", new String[]{"financeorgcode","financeorginnum","pzbm","gqlx","jglx","jgzjdm","dqdm","bz","tzye","tzyezrmb","sjrq"});
    }
    static {
        HEADERS_ENG_MAP.put("XclgqtzxxH", new String[]{"financeorgcode","financeorginnum","pzbm","gqlx","jglx","jgzjdm","dqdm","bz","tzye","tzyezrmb","sjrq"});
    }
    /**
     * 股权投资发生额信息
     */
    static {
        HEADERS_ENG_MAP.put("Xgqtzfsexx", new String[]{"financeorgcode","financeorginnum","pzbm","gqlx","jglx","jgzjdm","dqdm","jyrq","bz","jyje","jyjezrmb","jyfx","sjrq"});
    }
    static {
        HEADERS_ENG_MAP.put("XgqtzfsexxH", new String[]{"financeorgcode","financeorginnum","pzbm","gqlx","jglx","jgzjdm","dqdm","jyrq","bz","jyje","jyjezrmb","jyfx","sjrq"});
    }
    /**
     * 存量特定目的载体投资信息
     */
    static {
        HEADERS_ENG_MAP.put("Xcltdmdzttzxx", new String[]{"financeorgcode","financeorginnum","tdmdztlx","zgcptjbm","tdmdztdm","fxrdm","fxrdqdm","yxfs","rgrq","dqrq","bz","tzye","tzyezrmb","sjrq"});
    }
    static {
        HEADERS_ENG_MAP.put("XcltdmdzttzxxH", new String[]{"financeorgcode","financeorginnum","tdmdztlx","zgcptjbm","tdmdztdm","fxrdm","fxrdqdm","yxfs","rgrq","dqrq","bz","tzye","tzyezrmb","sjrq"});
    }
    /**
     * 特定目的载体投资发生额信息
     */
    static {
        HEADERS_ENG_MAP.put("Xtdmdzttzfsexx", new String[]{"financeorgcode","financeorginnum","tdmdztlx","zgcptjbm","tdmdztdm","fxrdm","fxrdqdm","yxfs","rgrq","dqrq","jyrq","bz","jyje","jyjezrmb","jyfx","sjrq"});
    }
    static {
        HEADERS_ENG_MAP.put("XtdmdzttzfsexxH", new String[]{"financeorgcode","financeorginnum","tdmdztlx","zgcptjbm","tdmdztdm","fxrdm","fxrdqdm","yxfs","rgrq","dqrq","jyrq","bz","jyje","jyjezrmb","jyfx","sjrq"});
    }




    public static Map<String, String[]> getHeadersEngMap() {
        return HEADERS_ENG_MAP;
    }
}
