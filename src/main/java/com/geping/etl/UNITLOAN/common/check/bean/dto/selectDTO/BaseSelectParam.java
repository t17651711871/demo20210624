package com.geping.etl.UNITLOAN.common.check.bean.dto.selectDTO;

import com.geping.etl.UNITLOAN.PageInfo;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.common.check.bean.dto
 * @USER: tangshuai
 * @DATE: 2021/4/20
 * @TIME: 9:33
 * @描述: 查询基础字段
 */
public class BaseSelectParam {
    //分页
    private int page;
    private int rows;
    //数据状态
    private String datastatus;
    //金融机构代码
    private String financeorgcodeParam;
    //校验类型
    private String checkstatusParam;
    //操作名称
    private String operationnameParam;


    public int getPage() {
        if(page==0){
            return PageInfo.DEFAULT_PAGE;
        }
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getRows() {
        if(rows==0){
            return PageInfo.DEFAULT_ROWS;
        }
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public String getDatastatus() {
        return datastatus;
    }

    public void setDatastatus(String datastatus) {
        this.datastatus = datastatus;
    }

    public String getFinanceorgcodeParam() {
        return financeorgcodeParam;
    }

    public void setFinanceorgcodeParam(String financeorgcodeParam) {
        this.financeorgcodeParam = financeorgcodeParam;
    }

    public String getCheckstatusParam() {
        return checkstatusParam;
    }

    public void setCheckstatusParam(String checkstatusParam) {
        this.checkstatusParam = checkstatusParam;
    }

    public String getOperationnameParam() {
        return operationnameParam;
    }

    public void setOperationnameParam(String operationnameParam) {
        this.operationnameParam = operationnameParam;
    }


}
