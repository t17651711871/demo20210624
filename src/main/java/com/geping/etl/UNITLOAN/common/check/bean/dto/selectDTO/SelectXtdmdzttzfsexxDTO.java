package com.geping.etl.UNITLOAN.common.check.bean.dto.selectDTO;

/**
 * @PACKAGE_NAME: com.geping.etl.UNITLOAN.common.check.bean.dto.selectDTO
 * @USER: tangshuai
 * @DATE: 2021/5/17
 * @TIME: 17:40
 * @描述:
 */
public class SelectXtdmdzttzfsexxDTO extends BaseSelectParam{
    private String operationtimeParam;

    public String getOperationtimeParam() {
        return operationtimeParam;
    }

    public void setOperationtimeParam(String operationtimeParam) {
        this.operationtimeParam = operationtimeParam;
    }
}
