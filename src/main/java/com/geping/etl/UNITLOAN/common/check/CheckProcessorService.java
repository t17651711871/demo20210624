package com.geping.etl.UNITLOAN.common.check;

import com.geping.etl.UNITLOAN.common.check.bean.dto.CheckParamContext;
import net.sf.json.JSONObject;


/***
 *  报表检验接口
 * @author liang.xu
 * @date 2021.4.17
 */
public interface CheckProcessorService {

    /**
     * 校验数据
     *
     * @param checkParamContext
     */
    JSONObject checkData(CheckParamContext checkParamContext);

    /**
     * 校验key
     *
     * @return
     */
    String getKey();
}