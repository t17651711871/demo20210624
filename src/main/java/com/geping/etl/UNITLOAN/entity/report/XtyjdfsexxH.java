package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**    
*  
* @author liuweixin  
* @date 2021年2月20日 上午9:33:27 
* 同业借贷发生额信息 
*/
@Entity
@Table(name = "xtyjdfsexxh")
public class XtyjdfsexxH {

	@Id
	private String id;
	
	/**
     * 金融机构代码
	 */
	private String financeorgcode;
	
	/**
	 * 内部机构号
	 */
	private String financeorginnum;
	
	/**
	 * 交易对手代码
	 */
	private String jydsdm;
	
	/**
	 * 交易对手代码类别
	 */
	private String jydsdmlb;
	
	/**
	 * 合同编码
	 */
	private String contractcode;
	
	/**
	 * 交易流水号
	 */
	private String jylsh;
	
	/**
	 * 资产负债类型
	 */
	private String zcfzlx;
	
	/**
	 * 产品类别
	 */
	private String productcetegory;
	
	/**
	 * 合同起始日期
	 */
	private String startdate;
	
	/**
	 * 合同到期日期
	 */
	private String enddate;
	
	/**
	 * 合同实际终止日期
	 */
	private String finaldate;
	
	/**
	 * 币种
	 */
	private String currency;
	
	/**
	 * 发生金额
	 */
	private String receiptbalance;
	
	/**
	 * 发生金额折人民币
	 */
	private String receiptcnybalance;
	
	/**
	 * 利率是否固定
	 */
	private String interestisfixed;
	
	/**
	 * 利率水平
	 */
	private String interestislevel;
	
	/**
	 * 定价基准类型
	 */
	private String fixpricetype;
	
	/**
	 * 基准利率
	 */
	private String baseinterest;
	
	/**
	 * 计息方式
	 */
	private String jxfs;
	
	/**
	 * 发生/结清标识
	 */
	private String fsjqbs;

	/**
     * 数据日期
     */
    private String sjrq;

    /**
	 * 校验状态 0:未校验;1:校验成功;2:校验失败
	 */
	private String checkstatus;

	/**
	 * 数据状态 0:待提交;1:待审核;2:审核不通过;3:审核通过
	 */
	private String datastatus;

	/**
	 * 操作人
	 */
	private String operator;

	/**
	 * 操作名
	 */
	private String operationname;

	/**
	 * 操作时间
	 */
	private String operationtime;

	/**
	 * 组织机构id
	 */
	private String orgid;

	/**
	 * 部门id
	 */
	private String departid;

	/**
	 * 审核不通过原因
	 */
	private String nopassreason;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFinanceorgcode() {
		return financeorgcode;
	}

	public void setFinanceorgcode(String financeorgcode) {
		this.financeorgcode = financeorgcode;
	}

	public String getFinanceorginnum() {
		return financeorginnum;
	}

	public void setFinanceorginnum(String financeorginnum) {
		this.financeorginnum = financeorginnum;
	}

	public String getJydsdm() {
		return jydsdm;
	}

	public void setJydsdm(String jydsdm) {
		this.jydsdm = jydsdm;
	}

	public String getJydsdmlb() {
		return jydsdmlb;
	}

	public void setJydsdmlb(String jydsdmlb) {
		this.jydsdmlb = jydsdmlb;
	}

	public String getContractcode() {
		return contractcode;
	}

	public void setContractcode(String contractcode) {
		this.contractcode = contractcode;
	}

	public String getJylsh() {
		return jylsh;
	}

	public void setJylsh(String jylsh) {
		this.jylsh = jylsh;
	}

	public String getZcfzlx() {
		return zcfzlx;
	}

	public void setZcfzlx(String zcfzlx) {
		this.zcfzlx = zcfzlx;
	}

	public String getProductcetegory() {
		return productcetegory;
	}

	public void setProductcetegory(String productcetegory) {
		this.productcetegory = productcetegory;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public String getFinaldate() {
		return finaldate;
	}

	public void setFinaldate(String finaldate) {
		this.finaldate = finaldate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getReceiptbalance() {
		return receiptbalance;
	}

	public void setReceiptbalance(String receiptbalance) {
		this.receiptbalance = receiptbalance;
	}

	public String getReceiptcnybalance() {
		return receiptcnybalance;
	}

	public void setReceiptcnybalance(String receiptcnybalance) {
		this.receiptcnybalance = receiptcnybalance;
	}

	public String getInterestisfixed() {
		return interestisfixed;
	}

	public void setInterestisfixed(String interestisfixed) {
		this.interestisfixed = interestisfixed;
	}

	public String getInterestislevel() {
		return interestislevel;
	}

	public void setInterestislevel(String interestislevel) {
		this.interestislevel = interestislevel;
	}

	public String getFixpricetype() {
		return fixpricetype;
	}

	public void setFixpricetype(String fixpricetype) {
		this.fixpricetype = fixpricetype;
	}

	public String getBaseinterest() {
		return baseinterest;
	}

	public void setBaseinterest(String baseinterest) {
		this.baseinterest = baseinterest;
	}

	public String getJxfs() {
		return jxfs;
	}

	public void setJxfs(String jxfs) {
		this.jxfs = jxfs;
	}

	public String getFsjqbs() {
		return fsjqbs;
	}

	public void setFsjqbs(String fsjqbs) {
		this.fsjqbs = fsjqbs;
	}

	public String getSjrq() {
		return sjrq;
	}

	public void setSjrq(String sjrq) {
		this.sjrq = sjrq;
	}

	public String getCheckstatus() {
		return checkstatus;
	}

	public void setCheckstatus(String checkstatus) {
		this.checkstatus = checkstatus;
	}

	public String getDatastatus() {
		return datastatus;
	}

	public void setDatastatus(String datastatus) {
		this.datastatus = datastatus;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getOperationname() {
		return operationname;
	}

	public void setOperationname(String operationname) {
		this.operationname = operationname;
	}

	public String getOperationtime() {
		return operationtime;
	}

	public void setOperationtime(String operationtime) {
		this.operationtime = operationtime;
	}

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	public String getDepartid() {
		return departid;
	}

	public void setDepartid(String departid) {
		this.departid = departid;
	}

	public String getNopassreason() {
		return nopassreason;
	}

	public void setNopassreason(String nopassreason) {
		this.nopassreason = nopassreason;
	}
	
	
}
