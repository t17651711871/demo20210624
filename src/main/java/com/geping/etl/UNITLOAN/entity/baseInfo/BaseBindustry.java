package com.geping.etl.UNITLOAN.entity.baseInfo;

import javax.persistence.*;

/**
 * 二级行业代码 
 * @Author  wangzd
 * @Date 2020-06-11 
 */

@Entity
@Table (name ="basebindustry")
public class BaseBindustry {

	@Id
	private String id;

	/**
	 * 二级行业代码
	 */
	private String bindustrycode;

	/**
	 * 二级行业名称
	 */
	private String bindustryname;

	/**
	 * 二级行业描述
	 */
	private String bindustrydesc;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getBindustrycode() {
    return bindustrycode;
  }

  public void setBindustrycode(String bindustrycode) {
    this.bindustrycode = bindustrycode;
  }


  public String getBindustryname() {
    return bindustryname;
  }

  public void setBindustryname(String bindustryname) {
    this.bindustryname = bindustryname;
  }


  public String getBindustrydesc() {
    return bindustrydesc;
  }

  public void setBindustrydesc(String bindustrydesc) {
    this.bindustrydesc = bindustrydesc;
  }

}
