package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.*;

/**
 * 金融机构（法人）基础信息-基础情况统计表 
 * @Author  wangzd
 * @Date 2020-06-11 
 */

@Entity
@Table (name ="xjrjgfrbaseinfo")
public class XjrjgfrBaseinfo {

	@Id
	private String id;

	/**
	 * 金融机构名称

	 */
	private String finorgname;

	/**
	 * 金融机构代码
	 */
	private String finorgcode;

	/**
	 * 金融机构编码

	 */
	private String finorgnum;
	
	/**
	 * 机构类别

	 */
	private String jglb;

	/**
	 * 注册地址

	 */
	private String regaddress;

	/**
	 * 地区代码

	 */
	private String regarea;

	/**
	 * 注册资本

	 */
	private String regamt;

	/**
	 * 成立日期

	 */
	private String setupdate;
	
	/**
	 * 联系人

	 */
	private String lxr;

	/**
	 * 联系电话

	 */
	private String phone;

	/**
	 * 经营状态

	 */
	private String orgmanagestatus;

	/**
	 * 经济成分

	 */
	private String orgstoreconomy;

	/**
	 * 行业分类0

	 */
	// private String industrycetegory;

	/**
	 * 企业规模
	 */
	private String invermodel;	

	/**
	 * 实际控制人名称

	 */
	private String actctrlname;
	
	/**
	 * 实际控制人证件类型
	 */
	private String actctrlidtype;

	/**
	 * 实际控制人证件代码

	 */
	private String actctrlcode;

	/**
	 * 从业人员数

	 */
	private String personnum;

	/**
	 * 第一大股东代码

	 */
	private String onestockcode;

	/**
	 * 第二大股东代码

	 */
	private String twostockcode;

	/**
	 * 第三大股东代码

	 */
	private String threestockcode;

	/**
	 * 第四大股东代码

	 */
	private String fourstockcode;

	/**
	 * 第五大股东代码

	 */
	private String fivestockcode;

	/**
	 * 第六大股东代码

	 */
	private String sixstockcode;

	/**
	 * 第七大股东代码

	 */
	private String sevenstockcode;

	/**
	 * 第八大股东代码

	 */
	private String eightstockcode;

	/**
	 * 第九大股东代码

	 */
	private String ninestockcode;

	/**
	 * 第十大股东代码
	 */
	private String tenstockcode;

	/**
	 * 第一大股东持股比例
	 */
	private String onestockprop;

	/**
	 * 第二大股东持股比例

	 */
	private String twostockprop;

	/**
	 * 第三大股东持股比例

	 */
	private String threestockprop;

	/**
	 * 第四大股东持股比例

	 */
	private String fourstockprop;

	/**
	 * 第五大股东持股比例

	 */
	private String fivestockprop;

	/**
	 * 第六大股东持股比例

	 */
	private String sixstockprop;

	/**
	 * 第七大股东持股比例

	 */
	private String sevenstockprop;

	/**
	 * 第八大股东持股比例

	 */
	private String eightstockprop;

	/**
	 * 第九大股东持股比例
	 */
	private String ninestockprop;

	/**
	 * 第十大股东持股比例
	 */
	private String tenstockprop;

    /*
     *   数据日期
     */
    private  String sjrq;



	/**
	 * 校验状态 0:未校验;1:校验成功;2:校验失败
	 */
	private String checkstatus;

	/**
	 * 数据状态 0:待提交;1:待审核;2:审核不通过;3:审核通过
	 */
	private String datastatus;

	/**
	 * 操作人
	 */
	private String operator;

	/**
	 * 操作名
	 */
	private String operationname;

	/**
	 * 操作时间
	 */
	private String operationtime;

	/**
	 * 组织机构id
	 */
	private String orgid;

	/**
	 * 部门id
	 */
	private String departid;

	/**
	 * 审核不通过原因
	 */
	private String nopassreason;

    public String getSjrq() {
        return sjrq;
    }

    public void setSjrq(String sjrq) {
        this.sjrq = sjrq;
    }


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getFinorgname() {
    return finorgname;
  }

  public void setFinorgname(String finorgname) {
    this.finorgname = finorgname;
  }


  public String getFinorgcode() {
    return finorgcode;
  }

  public void setFinorgcode(String finorgcode) {
    this.finorgcode = finorgcode;
  }


  public String getFinorgnum() {
    return finorgnum;
  }

  public void setFinorgnum(String finorgnum) {
    this.finorgnum = finorgnum;
  }


  public String getRegaddress() {
    return regaddress;
  }

  public void setRegaddress(String regaddress) {
    this.regaddress = regaddress;
  }


  public String getRegarea() {
    return regarea;
  }

  public void setRegarea(String regarea) {
    this.regarea = regarea;
  }


  public String getRegamt() {
    return regamt;
  }

  public void setRegamt(String regamt) {
    this.regamt = regamt;
  }


  public String getSetupdate() {
    return setupdate;
  }

  public void setSetupdate(String setupdate) {
    this.setupdate = setupdate;
  }


  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }


  public String getOrgmanagestatus() {
    return orgmanagestatus;
  }

  public void setOrgmanagestatus(String orgmanagestatus) {
    this.orgmanagestatus = orgmanagestatus;
  }


  public String getOrgstoreconomy() {
    return orgstoreconomy;
  }

  public void setOrgstoreconomy(String orgstoreconomy) {
    this.orgstoreconomy = orgstoreconomy;
  }

  /*
  public String getIndustrycetegory() {
    return industrycetegory;
  }

  public void setIndustrycetegory(String industrycetegory) {
    this.industrycetegory = industrycetegory;
  }
  */

  public String getInvermodel() {
    return invermodel;
  }

  public void setInvermodel(String invermodel) {
    this.invermodel = invermodel;
  }


  public String getActctrlidtype() {
    return actctrlidtype;
  }

  public void setActctrlidtype(String actctrlidtype) {
    this.actctrlidtype = actctrlidtype;
  }


  public String getActctrlname() {
    return actctrlname;
  }

  public void setActctrlname(String actctrlname) {
    this.actctrlname = actctrlname;
  }


  public String getActctrlcode() {
    return actctrlcode;
  }

  public void setActctrlcode(String actctrlcode) {
    this.actctrlcode = actctrlcode;
  }


  public String getPersonnum() {
    return personnum;
  }

  public void setPersonnum(String personnum) {
    this.personnum = personnum;
  }


  public String getOnestockcode() {
    return onestockcode;
  }

  public void setOnestockcode(String onestockcode) {
    this.onestockcode = onestockcode;
  }


  public String getTwostockcode() {
    return twostockcode;
  }

  public void setTwostockcode(String twostockcode) {
    this.twostockcode = twostockcode;
  }


  public String getThreestockcode() {
    return threestockcode;
  }

  public void setThreestockcode(String threestockcode) {
    this.threestockcode = threestockcode;
  }


  public String getFourstockcode() {
    return fourstockcode;
  }

  public void setFourstockcode(String fourstockcode) {
    this.fourstockcode = fourstockcode;
  }


  public String getFivestockcode() {
    return fivestockcode;
  }

  public void setFivestockcode(String fivestockcode) {
    this.fivestockcode = fivestockcode;
  }


  public String getSixstockcode() {
    return sixstockcode;
  }

  public void setSixstockcode(String sixstockcode) {
    this.sixstockcode = sixstockcode;
  }


  public String getSevenstockcode() {
    return sevenstockcode;
  }

  public void setSevenstockcode(String sevenstockcode) {
    this.sevenstockcode = sevenstockcode;
  }


  public String getEightstockcode() {
    return eightstockcode;
  }

  public void setEightstockcode(String eightstockcode) {
    this.eightstockcode = eightstockcode;
  }


  public String getNinestockcode() {
    return ninestockcode;
  }

  public void setNinestockcode(String ninestockcode) {
    this.ninestockcode = ninestockcode;
  }


  public String getTenstockcode() {
    return tenstockcode;
  }

  public void setTenstockcode(String tenstockcode) {
    this.tenstockcode = tenstockcode;
  }


  public String getOnestockprop() {
    return onestockprop;
  }

  public void setOnestockprop(String onestockprop) {
    this.onestockprop = onestockprop;
  }


  public String getTwostockprop() {
    return twostockprop;
  }

  public void setTwostockprop(String twostockprop) {
    this.twostockprop = twostockprop;
  }


  public String getThreestockprop() {
    return threestockprop;
  }

  public void setThreestockprop(String threestockprop) {
    this.threestockprop = threestockprop;
  }


  public String getFourstockprop() {
    return fourstockprop;
  }

  public void setFourstockprop(String fourstockprop) {
    this.fourstockprop = fourstockprop;
  }


  public String getFivestockprop() {
    return fivestockprop;
  }

  public void setFivestockprop(String fivestockprop) {
    this.fivestockprop = fivestockprop;
  }


  public String getSixstockprop() {
    return sixstockprop;
  }

  public void setSixstockprop(String sixstockprop) {
    this.sixstockprop = sixstockprop;
  }


  public String getSevenstockprop() {
    return sevenstockprop;
  }

  public void setSevenstockprop(String sevenstockprop) {
    this.sevenstockprop = sevenstockprop;
  }


  public String getEightstockprop() {
    return eightstockprop;
  }

  public void setEightstockprop(String eightstockprop) {
    this.eightstockprop = eightstockprop;
  }


  public String getNinestockprop() {
    return ninestockprop;
  }

  public void setNinestockprop(String ninestockprop) {
    this.ninestockprop = ninestockprop;
  }


  public String getTenstockprop() {
    return tenstockprop;
  }

  public void setTenstockprop(String tenstockprop) {
    this.tenstockprop = tenstockprop;
  }


  public String getCheckstatus() {
    return checkstatus;
  }

  public void setCheckstatus(String checkstatus) {
    this.checkstatus = checkstatus;
  }


  public String getDatastatus() {
    return datastatus;
  }

  public void setDatastatus(String datastatus) {
    this.datastatus = datastatus;
  }


  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }


  public String getOperationname() {
    return operationname;
  }

  public void setOperationname(String operationname) {
    this.operationname = operationname;
  }


  public String getOperationtime() {
    return operationtime;
  }

  public void setOperationtime(String operationtime) {
    this.operationtime = operationtime;
  }


  public String getOrgid() {
    return orgid;
  }

  public void setOrgid(String orgid) {
    this.orgid = orgid;
  }


  public String getDepartid() {
    return departid;
  }

  public void setDepartid(String departid) {
    this.departid = departid;
  }


  public String getNopassreason() {
    return nopassreason;
  }

  public void setNopassreason(String nopassreason) {
    this.nopassreason = nopassreason;
  }

  public String getJglb() {
	  return jglb;
  }

  public void setJglb(String jglb) {
	  this.jglb = jglb;
  }

  public String getLxr() {
	  return lxr;
  }

  public void setLxr(String lxr) {
	  this.lxr = lxr;
  }
  
}
