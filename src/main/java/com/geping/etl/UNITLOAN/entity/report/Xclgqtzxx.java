package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.*;

@Entity
@Table (name ="xclgqtzxx")
public class Xclgqtzxx {
  @Id
  private String id;
  private String financeorgcode;//金融机构代码
  private String financeorginnum;//内部机构号
  private String pzbm;//凭证编码
  private String gqlx;//股权类型
  private String jglx;//机构类型
  private String jgzjdm;//机构证件代码
  private String dqdm;//地区代码
  private String bz;//币种
  private String tzye;//投资余额
  private String tzyezrmb;// 投资余额折人民币
  private String checkstatus;
  private String datastatus;
  private String operator;
  private String operationname;
  private String operationtime;
  private String orgid;
  private String departid;
  private String nopassreason;
  private String sjrq;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getFinanceorgcode() {
    return financeorgcode;
  }

  public void setFinanceorgcode(String financeorgcode) {
    this.financeorgcode = financeorgcode;
  }


  public String getFinanceorginnum() {
    return financeorginnum;
  }

  public void setFinanceorginnum(String financeorginnum) {
    this.financeorginnum = financeorginnum;
  }


  public String getPzbm() {
    return pzbm;
  }

  public void setPzbm(String pzbm) {
    this.pzbm = pzbm;
  }


  public String getGqlx() {
    return gqlx;
  }

  public void setGqlx(String gqlx) {
    this.gqlx = gqlx;
  }


  public String getJglx() {
    return jglx;
  }

  public void setJglx(String jglx) {
    this.jglx = jglx;
  }


  public String getJgzjdm() {
    return jgzjdm;
  }

  public void setJgzjdm(String jgzjdm) {
    this.jgzjdm = jgzjdm;
  }


  public String getDqdm() {
    return dqdm;
  }

  public void setDqdm(String dqdm) {
    this.dqdm = dqdm;
  }


  public String getBz() {
    return bz;
  }

  public void setBz(String bz) {
    this.bz = bz;
  }


  public String getTzye() {
    return tzye;
  }

  public void setTzye(String tzye) {
    this.tzye = tzye;
  }


  public String getTzyezrmb() {
    return tzyezrmb;
  }

  public void setTzyezrmb(String tzyezrmb) {
    this.tzyezrmb = tzyezrmb;
  }


  public String getCheckstatus() {
    return checkstatus;
  }

  public void setCheckstatus(String checkstatus) {
    this.checkstatus = checkstatus;
  }


  public String getDatastatus() {
    return datastatus;
  }

  public void setDatastatus(String datastatus) {
    this.datastatus = datastatus;
  }


  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }


  public String getOperationname() {
    return operationname;
  }

  public void setOperationname(String operationname) {
    this.operationname = operationname;
  }


  public String getOperationtime() {
    return operationtime;
  }

  public void setOperationtime(String operationtime) {
    this.operationtime = operationtime;
  }


  public String getOrgid() {
    return orgid;
  }

  public void setOrgid(String orgid) {
    this.orgid = orgid;
  }


  public String getDepartid() {
    return departid;
  }

  public void setDepartid(String departid) {
    this.departid = departid;
  }


  public String getNopassreason() {
    return nopassreason;
  }

  public void setNopassreason(String nopassreason) {
    this.nopassreason = nopassreason;
  }

  public String getSjrq() {
    return sjrq;
  }

  public void setSjrq(String sjrq) {
    this.sjrq = sjrq;
  }

}
