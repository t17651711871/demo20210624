package com.geping.etl.UNITLOAN.entity.baseInfo;

import javax.persistence.*;

/**
 * 国家代码  
 * @Author  wangzd
 * @Date 2020-06-11 
 */

@Entity
@Table (name ="basecountry")
public class BaseCountry {

	@Id
	private String id;

	/**
	 * 国家代码
	 */
	private String countrycode;

	/**
	 * 国家名称
	 */
	private String countryname;

	/**
	 * 国家描述-备用
	 */
	private String countrydesc;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getCountrycode() {
    return countrycode;
  }

  public void setCountrycode(String countrycode) {
    this.countrycode = countrycode;
  }


  public String getCountryname() {
    return countryname;
  }

  public void setCountryname(String countryname) {
    this.countryname = countryname;
  }


  public String getCountrydesc() {
    return countrydesc;
  }

  public void setCountrydesc(String countrydesc) {
    this.countrydesc = countrydesc;
  }

}
