package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**    
*  
* @author liuweixin  
* @date 2021年2月20日 上午10:07:19  
* 同业客户基础信息
*/
@Entity
@Table(name = "xtykhjcxxh")
public class XtykhjcxxH {

	@Id
	private String id;
	
	/**
     * 金融机构代码
	 */
	private String financeorgcode;
	
	/**
	 * 客户名称
	 */
	private String khname;
	
	/**
	 * 客户代码
	 */
	private String khcode;
	
	/**
	 * 客户金融机构编码
	 */
	private String khjrjgbm;
	
	/**
	 * 客户内部编码
	 */
	private String khnbbm;
	
	/**
	 * 基本存款账号
	 */
	private String jbckzh;
	
	/**
	 * 基本账户开户行名称
	 */
	private String jbzhkhhmc;
	
	/**
	 * 注册地址
	 */
	private String zcdz;
	
	/**
	 * 地区代码
	 */
	private String dqdm;
	
	/**
	 * 客户类别
	 */
	private String khtype;
	
	/**
	 * 成立日期
	 */
	private String setupdate;
	
	/**
	 * 是否关联方
	 */
	private String sfglf;
	
	/**
	 * 客户经济成分
	 */
	private String khjjcf;

	/**
	 * 客户国民经济部门
	 */
	private String khgmjjbm;
	
	/**
	 * 客户信用级别总等级数
	 */
	private String khxyjbzdjs;

	/**
	 * 客户信用评级
	 */
	private String khxypj;
	
	/**
     * 数据日期
     */
    private String sjrq;

    /**
	 * 校验状态 0:未校验;1:校验成功;2:校验失败
	 */
	private String checkstatus;

	/**
	 * 数据状态 0:待提交;1:待审核;2:审核不通过;3:审核通过
	 */
	private String datastatus;

	/**
	 * 操作人
	 */
	private String operator;

	/**
	 * 操作名
	 */
	private String operationname;

	/**
	 * 操作时间
	 */
	private String operationtime;

	/**
	 * 组织机构id
	 */
	private String orgid;

	/**
	 * 部门id
	 */
	private String departid;

	/**
	 * 审核不通过原因
	 */
	private String nopassreason;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFinanceorgcode() {
		return financeorgcode;
	}

	public void setFinanceorgcode(String financeorgcode) {
		this.financeorgcode = financeorgcode;
	}

	public String getKhname() {
		return khname;
	}

	public void setKhname(String khname) {
		this.khname = khname;
	}

	public String getKhcode() {
		return khcode;
	}

	public void setKhcode(String khcode) {
		this.khcode = khcode;
	}

	public String getKhjrjgbm() {
		return khjrjgbm;
	}

	public void setKhjrjgbm(String khjrjgbm) {
		this.khjrjgbm = khjrjgbm;
	}

	public String getKhnbbm() {
		return khnbbm;
	}

	public void setKhnbbm(String khnbbm) {
		this.khnbbm = khnbbm;
	}

	public String getJbckzh() {
		return jbckzh;
	}

	public void setJbckzh(String jbckzh) {
		this.jbckzh = jbckzh;
	}

	public String getJbzhkhhmc() {
		return jbzhkhhmc;
	}

	public void setJbzhkhhmc(String jbzhkhhmc) {
		this.jbzhkhhmc = jbzhkhhmc;
	}

	public String getZcdz() {
		return zcdz;
	}

	public void setZcdz(String zcdz) {
		this.zcdz = zcdz;
	}

	public String getDqdm() {
		return dqdm;
	}

	public void setDqdm(String dqdm) {
		this.dqdm = dqdm;
	}

	public String getKhtype() {
		return khtype;
	}

	public void setKhtype(String khtype) {
		this.khtype = khtype;
	}

	public String getSetupdate() {
		return setupdate;
	}

	public void setSetupdate(String setupdate) {
		this.setupdate = setupdate;
	}

	public String getSfglf() {
		return sfglf;
	}

	public void setSfglf(String sfglf) {
		this.sfglf = sfglf;
	}

	public String getKhjjcf() {
		return khjjcf;
	}

	public void setKhjjcf(String khjjcf) {
		this.khjjcf = khjjcf;
	}

	public String getKhgmjjbm() {
		return khgmjjbm;
	}

	public void setKhgmjjbm(String khgmjjbm) {
		this.khgmjjbm = khgmjjbm;
	}

	public String getKhxyjbzdjs() {
		return khxyjbzdjs;
	}

	public void setKhxyjbzdjs(String khxyjbzdjs) {
		this.khxyjbzdjs = khxyjbzdjs;
	}

	public String getKhxypj() {
		return khxypj;
	}

	public void setKhxypj(String khxypj) {
		this.khxypj = khxypj;
	}

	public String getSjrq() {
		return sjrq;
	}

	public void setSjrq(String sjrq) {
		this.sjrq = sjrq;
	}

	public String getCheckstatus() {
		return checkstatus;
	}

	public void setCheckstatus(String checkstatus) {
		this.checkstatus = checkstatus;
	}

	public String getDatastatus() {
		return datastatus;
	}

	public void setDatastatus(String datastatus) {
		this.datastatus = datastatus;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getOperationname() {
		return operationname;
	}

	public void setOperationname(String operationname) {
		this.operationname = operationname;
	}

	public String getOperationtime() {
		return operationtime;
	}

	public void setOperationtime(String operationtime) {
		this.operationtime = operationtime;
	}

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	public String getDepartid() {
		return departid;
	}

	public void setDepartid(String departid) {
		this.departid = departid;
	}

	public String getNopassreason() {
		return nopassreason;
	}

	public void setNopassreason(String nopassreason) {
		this.nopassreason = nopassreason;
	}
	
	
}
