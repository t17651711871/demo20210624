package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**    
*  
* @author liuweixin  
* @date 2021年2月20日 上午10:48:51  
* 同业存款发生额信息
*/
@Entity
@Table(name="xtyckfsexxh")
public class XtyckfsexxH {

	@Id
	private String id;
	
	/**
     * 金融机构代码
	 */
	private String financeorgcode;
	
	/**
	 * 内部机构号
	 */
	private String financeorginnum;
	
	/**
	 * 业务类型
	 */
	private String ywlx;
	
	/**
	 * 交易对手证件类型
	 */
	private String jydszjlx;
	
	/**
	 * 交易对手代码
	 */
	private String jydsdm;
	
	/**
	 * 存款账户编码
	 */
	private String ckzhbm;
	
	/**
	 * 存款协议代码
	 */
	private String ckxydm;
	
	/**
	 * 协议起始日期
	 */
	private String startdate;
	
	/**
	 * 协议到期日期
	 */
	private String enddate;
	
	/**
	 * 币种
	 */
	private String currency;
	
	/**
	 * 交易金额
	 */
	private String receiptbalance;
	
	/**
	 * 交易金额折人民币
	 */
	private String receiptcnybalance;
	
	/**
	 * 交易日期
	 */
	private String jyrq;
	
	/**
	 * 交易流水号
	 */
	private String jylsh;
	
	/**
	 * 利率水平
	 */
	private String llsp;
	
	/**
	 * 交易账户号
	 */
	private String jyzhh;
	
	/**
	 * 交易账户开户行号
	 */
	private String jyzhkhhh;
	
	/**
	 * 交易对手账户号
	 */
	private String jydszhh;
	
	/**
	 * 交易方向
	 */
	private String jyfx;
	
	/**
     * 数据日期
     */
    private String sjrq;

    /**
	 * 校验状态 0:未校验;1:校验成功;2:校验失败
	 */
	private String checkstatus;

	/**
	 * 数据状态 0:待提交;1:待审核;2:审核不通过;3:审核通过
	 */
	private String datastatus;

	/**
	 * 操作人
	 */
	private String operator;

	/**
	 * 操作名
	 */
	private String operationname;

	/**
	 * 操作时间
	 */
	private String operationtime;

	/**
	 * 组织机构id
	 */
	private String orgid;

	/**
	 * 部门id
	 */
	private String departid;

	/**
	 * 审核不通过原因
	 */
	private String nopassreason;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFinanceorgcode() {
		return financeorgcode;
	}

	public void setFinanceorgcode(String financeorgcode) {
		this.financeorgcode = financeorgcode;
	}

	public String getFinanceorginnum() {
		return financeorginnum;
	}

	public void setFinanceorginnum(String financeorginnum) {
		this.financeorginnum = financeorginnum;
	}

	public String getYwlx() {
		return ywlx;
	}

	public void setYwlx(String ywlx) {
		this.ywlx = ywlx;
	}

	public String getJydszjlx() {
		return jydszjlx;
	}

	public void setJydszjlx(String jydszjlx) {
		this.jydszjlx = jydszjlx;
	}

	public String getJydsdm() {
		return jydsdm;
	}

	public void setJydsdm(String jydsdm) {
		this.jydsdm = jydsdm;
	}

	public String getCkzhbm() {
		return ckzhbm;
	}

	public void setCkzhbm(String ckzhbm) {
		this.ckzhbm = ckzhbm;
	}

	public String getCkxydm() {
		return ckxydm;
	}

	public void setCkxydm(String ckxydm) {
		this.ckxydm = ckxydm;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getReceiptbalance() {
		return receiptbalance;
	}

	public void setReceiptbalance(String receiptbalance) {
		this.receiptbalance = receiptbalance;
	}

	public String getReceiptcnybalance() {
		return receiptcnybalance;
	}

	public void setReceiptcnybalance(String receiptcnybalance) {
		this.receiptcnybalance = receiptcnybalance;
	}

	public String getJyrq() {
		return jyrq;
	}

	public void setJyrq(String jyrq) {
		this.jyrq = jyrq;
	}

	public String getJylsh() {
		return jylsh;
	}

	public void setJylsh(String jylsh) {
		this.jylsh = jylsh;
	}

	public String getLlsp() {
		return llsp;
	}

	public void setLlsp(String llsp) {
		this.llsp = llsp;
	}

	public String getJyzhh() {
		return jyzhh;
	}

	public void setJyzhh(String jyzhh) {
		this.jyzhh = jyzhh;
	}

	public String getJyzhkhhh() {
		return jyzhkhhh;
	}

	public void setJyzhkhhh(String jyzhkhhh) {
		this.jyzhkhhh = jyzhkhhh;
	}

	public String getJydszhh() {
		return jydszhh;
	}

	public void setJydszhh(String jydszhh) {
		this.jydszhh = jydszhh;
	}

	public String getJyfx() {
		return jyfx;
	}

	public void setJyfx(String jyfx) {
		this.jyfx = jyfx;
	}

	public String getSjrq() {
		return sjrq;
	}

	public void setSjrq(String sjrq) {
		this.sjrq = sjrq;
	}

	public String getCheckstatus() {
		return checkstatus;
	}

	public void setCheckstatus(String checkstatus) {
		this.checkstatus = checkstatus;
	}

	public String getDatastatus() {
		return datastatus;
	}

	public void setDatastatus(String datastatus) {
		this.datastatus = datastatus;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getOperationname() {
		return operationname;
	}

	public void setOperationname(String operationname) {
		this.operationname = operationname;
	}

	public String getOperationtime() {
		return operationtime;
	}

	public void setOperationtime(String operationtime) {
		this.operationtime = operationtime;
	}

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	public String getDepartid() {
		return departid;
	}

	public void setDepartid(String departid) {
		this.departid = departid;
	}

	public String getNopassreason() {
		return nopassreason;
	}

	public void setNopassreason(String nopassreason) {
		this.nopassreason = nopassreason;
	}

	
}
