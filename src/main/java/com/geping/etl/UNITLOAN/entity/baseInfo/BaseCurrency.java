package com.geping.etl.UNITLOAN.entity.baseInfo;

import javax.persistence.*;

/**
 * 币种代码  
 * @Author  wangzd
 * @Date 2020-06-11 
 */

@Entity
@Table (name ="basecurrency")
public class BaseCurrency {

	@Id
	private String id;

	/**
	 * 币种代码
	 */
	private String currencycode;

	/**
	 * 币种名称
	 */
	private String currencyname;

	/**
	 * 币种描述-备用
	 */
	private String currencydesc;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getCurrencycode() {
    return currencycode;
  }

  public void setCurrencycode(String currencycode) {
    this.currencycode = currencycode;
  }


  public String getCurrencyname() {
    return currencyname;
  }

  public void setCurrencyname(String currencyname) {
    this.currencyname = currencyname;
  }


  public String getCurrencydesc() {
    return currencydesc;
  }

  public void setCurrencydesc(String currencydesc) {
    this.currencydesc = currencydesc;
  }

}
