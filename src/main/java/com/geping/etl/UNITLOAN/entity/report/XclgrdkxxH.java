package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @ClassName: Xclgrdkxx
 * @Description: TOOD
 * @Author: 陈根
 * @Date: 2020/11/3 14:01
 * @Version
 **/

@Entity
@Table(name ="xclgrdkxxh")
public class XclgrdkxxH {
    @Id
    private Integer id;
    /**
     * 金融机构代码
     */
    private String financeorgcode;

    /**
     * 内部机构号
     */
    private String financeorginnum;

    /**
     * 金融机构地区代码
     */
    private String financeorgareacode;

    /**
     * 借款人证件类型
     */
    private String isfarmerloan;

    /**
     * 借款人证件代码
     */
    private String brroweridnum;

    /**
     * 借款人地区代码
     */
    private String brrowerareacode;

    /**
     * 贷款借据编码
     */
    private String receiptcode;


    /**
     * 贷款合同编码
     */
    private String contractcode;

    /**
     * 贷款产品类别
     */
    private String productcetegory;


    /**
     * 贷款发放日期
     */
    private String loanstartdate;

    /**
     * 贷款到期日期
     */
    private String loanenddate;

    /**
     * 贷款展期到期日期
     */
    private String extensiondate;



    /**
     * 币种
     */
    private String currency;

    /**
     * 贷款余额
     */
    private String receiptbalance;

    /**
     * 贷款余额折人民币
     */
    private String receiptcnybalance;

    /**
     * 利率是否固定
     */
    private String interestisfixed;

    /**
     * 利率水平
     */
    private String interestislevel;

    /**
     * 贷款定价基准类型
     */
    private String fixpricetype;

    /**
     * 基准利率
     */
    private String baseinterest;

    /**
     * 贷款财政扶持方式
     */
    private String loanfinancesupport;

    /**
     * 贷款利率重新定价日
     */
    private String loaninterestrepricedate;

    /**
     * 贷款担保方式
     */
    private String guaranteemethod;

    /**
     * 是否首次贷款
     */
    private String isplatformloan;


    /**
     * 贷款质量
     */
    private String loanquality;

    /**
     * 贷款状态
     */
    private String loanstatus;

    /**
     * 逾期类型
     */
    private String overduetype;


    /**
     * 贷款用途
     */
    private String issupportliveloan;


    /**
     * 数据日期
     */

    private String sjrq;

    /**
     * 校验状态 0:未校验;1:校验成功;2:校验失败
     */
    private String checkstatus;

    /**
     * 数据状态 0:待提交;1:待审核;2:审核不通过;3:审核通过
     */
    private String datastatus;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 操作名
     */
    private String operationname;

    /**
     * 操作时间
     */
    private String operationtime;

    /**
     * 组织机构id
     */
    private String orgid;

    /**
     * 部门id
     */
    private String departid;

    /**
     * 审核不通过原因
     */
    private String nopassreason;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSjrq() { return sjrq; }

    public void setSjrq(String sjrq) { this.sjrq = sjrq; }

    public String getFinanceorgcode() {
        return financeorgcode;
    }

    public void setFinanceorgcode(String financeorgcode) {
        this.financeorgcode = financeorgcode;
    }


    public String getFinanceorginnum() {
        return financeorginnum;
    }

    public void setFinanceorginnum(String financeorginnum) {
        this.financeorginnum = financeorginnum;
    }


    public String getFinanceorgareacode() {
        return financeorgareacode;
    }

    public void setFinanceorgareacode(String financeorgareacode) {
        this.financeorgareacode = financeorgareacode;
    }


    public String getBrroweridnum() {
        return brroweridnum;
    }

    public void setBrroweridnum(String brroweridnum) {
        this.brroweridnum = brroweridnum;
    }

    public String getBrrowerareacode() {
        return brrowerareacode;
    }

    public void setBrrowerareacode(String brrowerareacode) {
        this.brrowerareacode = brrowerareacode;
    }

    public String getReceiptcode() {
        return receiptcode;
    }

    public void setReceiptcode(String receiptcode) {
        this.receiptcode = receiptcode;
    }


    public String getContractcode() {
        return contractcode;
    }

    public void setContractcode(String contractcode) {
        this.contractcode = contractcode;
    }


    public String getProductcetegory() {
        return productcetegory;
    }

    public void setProductcetegory(String productcetegory) {
        this.productcetegory = productcetegory;
    }

    public String getLoanstartdate() {
        return loanstartdate;
    }

    public void setLoanstartdate(String loanstartdate) {
        this.loanstartdate = loanstartdate;
    }


    public String getLoanenddate() {
        return loanenddate;
    }

    public void setLoanenddate(String loanenddate) {
        this.loanenddate = loanenddate;
    }


    public String getExtensiondate() {
        return extensiondate;
    }

    public void setExtensiondate(String extensiondate) {
        this.extensiondate = extensiondate;
    }


    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public String getReceiptbalance() {
        return receiptbalance;
    }

    public void setReceiptbalance(String receiptbalance) {
        this.receiptbalance = receiptbalance;
    }


    public String getReceiptcnybalance() {
        return receiptcnybalance;
    }

    public void setReceiptcnybalance(String receiptcnybalance) {
        this.receiptcnybalance = receiptcnybalance;
    }


    public String getInterestisfixed() {
        return interestisfixed;
    }

    public void setInterestisfixed(String interestisfixed) {
        this.interestisfixed = interestisfixed;
    }


    public String getInterestislevel() {
        return interestislevel;
    }

    public void setInterestislevel(String interestislevel) {
        this.interestislevel = interestislevel;
    }


    public String getFixpricetype() {
        return fixpricetype;
    }

    public void setFixpricetype(String fixpricetype) {
        this.fixpricetype = fixpricetype;
    }


    public String getBaseinterest() {
        return baseinterest;
    }

    public void setBaseinterest(String baseinterest) {
        this.baseinterest = baseinterest;
    }


    public String getLoanfinancesupport() {
        return loanfinancesupport;
    }

    public void setLoanfinancesupport(String loanfinancesupport) {
        this.loanfinancesupport = loanfinancesupport;
    }


    public String getLoaninterestrepricedate() {
        return loaninterestrepricedate;
    }

    public void setLoaninterestrepricedate(String loaninterestrepricedate) {
        this.loaninterestrepricedate = loaninterestrepricedate;
    }


    public String getGuaranteemethod() {
        return guaranteemethod;
    }

    public void setGuaranteemethod(String guaranteemethod) {
        this.guaranteemethod = guaranteemethod;
    }


    public String getLoanquality() {
        return loanquality;
    }

    public void setLoanquality(String loanquality) {
        this.loanquality = loanquality;
    }


    public String getLoanstatus() {
        return loanstatus;
    }

    public void setLoanstatus(String loanstatus) {
        this.loanstatus = loanstatus;
    }


    public String getOverduetype() {
        return overduetype;
    }

    public void setOverduetype(String overduetype) {
        this.overduetype = overduetype;
    }


    public String getIsfarmerloan() {
        return isfarmerloan;
    }

    public void setIsfarmerloan(String isfarmerloan) {
        this.isfarmerloan = isfarmerloan;
    }

    public String getIsplatformloan() {
        return isplatformloan;
    }

    public void setIsplatformloan(String isplatformloan) {
        this.isplatformloan = isplatformloan;
    }


    public String getIssupportliveloan() {
        return issupportliveloan;
    }

    public void setIssupportliveloan(String issupportliveloan) {
        this.issupportliveloan = issupportliveloan;
    }


    public String getCheckstatus() {
        return checkstatus;
    }

    public void setCheckstatus(String checkstatus) {
        this.checkstatus = checkstatus;
    }


    public String getDatastatus() {
        return datastatus;
    }

    public void setDatastatus(String datastatus) {
        this.datastatus = datastatus;
    }


    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }


    public String getOperationname() {
        return operationname;
    }

    public void setOperationname(String operationname) {
        this.operationname = operationname;
    }


    public String getOperationtime() {
        return operationtime;
    }

    public void setOperationtime(String operationtime) {
        this.operationtime = operationtime;
    }


    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }


    public String getDepartid() {
        return departid;
    }

    public void setDepartid(String departid) {
        this.departid = departid;
    }


    public String getNopassreason() {
        return nopassreason;
    }

    public void setNopassreason(String nopassreason) {
        this.nopassreason = nopassreason;
    }



}
