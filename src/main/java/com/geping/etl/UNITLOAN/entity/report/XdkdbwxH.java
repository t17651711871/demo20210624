package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 单位贷款担保物信息
 * @Author  wangzd
 * @Date 2020-06-09 
 */

@Entity
@Table (name ="xdkdbwxH")
public class XdkdbwxH {

	@Id
	private String id;
	/**
     * 金融机构代码
	 */
	private String financeorgcode;

	/**
	 * 内部机构号
	 */
	private String financeorginnum;
	/**
	 * 担保合同编码
	 */
	private String gteecontractcode;

	/**
	 * 贷款合同编码->被担保合同编码
	 */
	private String loancontractcode;

	/**
	 * 担保物编码
	 */
	private String gteegoodscode;

	/**
	 * 担保物类别
	 */
	private String gteegoodscategory;

	/**
	 * 权证编号
	 */
	private String warrantcode;

	/**
	 * 是否第一顺位
	 */
	private String isfirst;

	/**
	 * 评估方式
	 */
	private String assessmode;

	/**
	 * 评估方法
	 */
	private String assessmethod;

	/**
	 * 评估价值
	 */
	private String assessvalue;

	/**
	 * 评估基准日
	 */
	private String assessdate;

	/**
	 * 担保物账面价值->担保物票面价值
	 */
	private String gteegoodsamt;

	/**
	 * 优先受偿权数额
	 */
	private String firstrightamt;
	/**
	 * 估值周期
	 */
	private String gzzq;

	/**
	 * 数据日期
	 */
	private String sjrq;
	/**
	 * 担保物状态0  不需要字段
	 */
	private String gteegoodsstataus;

	/**
	 * 抵质押率0  不需要字段
	 */
	private String mortgagepgerate;

	/**
	 * 校验状态 0:未校验;1:校验成功;2:校验失败
	 */
	private String checkstatus;

	/**
	 * 数据状态 0:待提交;1:待审核;2:审核不通过;3:审核通过;4生成报文
	 */
	private String datastatus;

	/**
	 * 操作人
	 */
	private String operator;

	/**
	 * 操作名
	 */
	private String operationname;

	/**
	 * 操作时间
	 */
	private String operationtime;

	/**
	 * 组织机构id
	 */
	private String orgid;

	/**
	 * 部门id
	 */
	private String departid;

	/**
	 * 审核不通过原因
	 */
	private String nopassreason;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFinanceorgcode() {
		return financeorgcode;
	}

	public void setFinanceorgcode(String financeorgcode) {
		this.financeorgcode = financeorgcode;
	}

	public String getFinanceorginnum() {
		return financeorginnum;
	}

	public void setFinanceorginnum(String financeorginnum) {
		this.financeorginnum = financeorginnum;
	}

	public String getGteecontractcode() {
		return gteecontractcode;
	}

	public void setGteecontractcode(String gteecontractcode) {
		this.gteecontractcode = gteecontractcode;
	}

	public String getLoancontractcode() {
		return loancontractcode;
	}

	public void setLoancontractcode(String loancontractcode) {
		this.loancontractcode = loancontractcode;
	}

	public String getGteegoodscode() {
		return gteegoodscode;
	}

	public void setGteegoodscode(String gteegoodscode) {
		this.gteegoodscode = gteegoodscode;
	}

	public String getGteegoodscategory() {
		return gteegoodscategory;
	}

	public void setGteegoodscategory(String gteegoodscategory) {
		this.gteegoodscategory = gteegoodscategory;
	}

	public String getWarrantcode() {
		return warrantcode;
	}

	public void setWarrantcode(String warrantcode) {
		this.warrantcode = warrantcode;
	}

	public String getIsfirst() {
		return isfirst;
	}

	public void setIsfirst(String isfirst) {
		this.isfirst = isfirst;
	}

	public String getAssessmode() {
		return assessmode;
	}

	public void setAssessmode(String assessmode) {
		this.assessmode = assessmode;
	}

	public String getAssessmethod() {
		return assessmethod;
	}

	public void setAssessmethod(String assessmethod) {
		this.assessmethod = assessmethod;
	}

	public String getAssessvalue() {
		return assessvalue;
	}

	public void setAssessvalue(String assessvalue) {
		this.assessvalue = assessvalue;
	}

	public String getAssessdate() {
		return assessdate;
	}

	public void setAssessdate(String assessdate) {
		this.assessdate = assessdate;
	}

	public String getGteegoodsamt() {
		return gteegoodsamt;
	}

	public void setGteegoodsamt(String gteegoodsamt) {
		this.gteegoodsamt = gteegoodsamt;
	}

	public String getFirstrightamt() {
		return firstrightamt;
	}

	public void setFirstrightamt(String firstrightamt) {
		this.firstrightamt = firstrightamt;
	}

	public String getGzzq() {
		return gzzq;
	}

	public void setGzzq(String gzzq) {
		this.gzzq = gzzq;
	}

	public String getGteegoodsstataus() {
		return gteegoodsstataus;
	}

	public void setGteegoodsstataus(String gteegoodsstataus) {
		this.gteegoodsstataus = gteegoodsstataus;
	}

	public String getMortgagepgerate() {
		return mortgagepgerate;
	}

	public void setMortgagepgerate(String mortgagepgerate) {
		this.mortgagepgerate = mortgagepgerate;
	}

	public String getCheckstatus() {
		return checkstatus;
	}

	public void setCheckstatus(String checkstatus) {
		this.checkstatus = checkstatus;
	}

	public String getDatastatus() {
		return datastatus;
	}

	public void setDatastatus(String datastatus) {
		this.datastatus = datastatus;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getOperationname() {
		return operationname;
	}

	public void setOperationname(String operationname) {
		this.operationname = operationname;
	}

	public String getOperationtime() {
		return operationtime;
	}

	public void setOperationtime(String operationtime) {
		this.operationtime = operationtime;
	}

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	public String getDepartid() {
		return departid;
	}

	public void setDepartid(String departid) {
		this.departid = departid;
	}

	public String getNopassreason() {
		return nopassreason;
	}

	public void setNopassreason(String nopassreason) {
		this.nopassreason = nopassreason;
	}
	public String getSjrq() { return this.sjrq; }

	public void setSjrq(String sjrq) { this.sjrq = sjrq; }

	@Override
	public String toString() {
		return "XdkdbwxH [id=" + id + ", financeorgcode=" + financeorgcode + ", financeorginnum=" + financeorginnum
				+ ", gteecontractcode=" + gteecontractcode + ", loancontractcode=" + loancontractcode
				+ ", gteegoodscode=" + gteegoodscode + ", gteegoodscategory=" + gteegoodscategory + ", warrantcode="
				+ warrantcode + ", isfirst=" + isfirst + ", assessmode=" + assessmode + ", assessmethod=" + assessmethod
				+ ", assessvalue=" + assessvalue + ", assessdate=" + assessdate + ", gteegoodsamt=" + gteegoodsamt
				+ ", firstrightamt=" + firstrightamt + ", gzzq=" + gzzq + ", sjrq="+sjrq+", gteegoodsstataus=" + gteegoodsstataus
				+ ", mortgagepgerate=" + mortgagepgerate + ", checkstatus=" + checkstatus + ", datastatus=" + datastatus
				+ ", operator=" + operator + ", operationname=" + operationname + ", operationtime=" + operationtime
				+ ", orgid=" + orgid + ", departid=" + departid + ", nopassreason=" + nopassreason + "]";
	}

}
