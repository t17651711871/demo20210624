package com.geping.etl.UNITLOAN.entity.baseInfo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**    
*  学历代码
* @author liuweixin  
* @date 2020年11月4日 下午3:32:55  
*/
@Entity
@Table (name ="baseeducation")
public class BaseEducation {

	@Id
	private String id;

	/**
	 * 代码
	 */
	private String educationcode;

	/**
	 * 名称
	 */
	private String educationname;

	/**
	 * 说明
	 */
	private String educationdesc;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEducationcode() {
		return educationcode;
	}

	public void setEducationcode(String educationcode) {
		this.educationcode = educationcode;
	}

	public String getEducationname() {
		return educationname;
	}

	public void setEducationname(String educationname) {
		this.educationname = educationname;
	}

	public String getEducationdesc() {
		return educationdesc;
	}

	public void setEducationdesc(String educationdesc) {
		this.educationdesc = educationdesc;
	}
	
	
}
