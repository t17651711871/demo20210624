package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Description  
 * @Author  wangzd
 * @Date 2020-06-09 
 */

@Entity
@Table (name ="xdkdbhtH")
public class XdkdbhtH {

	@Id
	private String id;

	/**
     * 金融机构代码
     */
    private String financeorgcode;

    /**
     * 内部机构号
     */
    private String financeorginnum;

	/**
	 * 担保合同编码
	 */
	private String gteecontractcode;

	/**
	 * 被担保合同编码
	 */
	private String loancontractcode;

	/**
	 * 担保合同类型
	 */
	private String gteecontracttype;

    /**
     * 交易类型
     */
    private String transactiontype;

	/**
	 * 担保合同起始日期
	 */
	private String gteestartdate;

	/**
	 * 担保合同到期日期
	 */
	private String gteeenddate;

	/**
	 * 币种
	 */
	private String gteecurrency;

	/**
	 * 担保合同金额
	 */
	private String gteeamount;

	/**
	 * 担保合同金额折人民币
	 */
	private String gteecnyamount;

    /**
     * 抵质押率
     */
    private String pledgerate;

	/**
	 * 担保人证件类型
	 */
	private String gteeidtype;

	/**
	 * 担保人证件代码
	 */
	private String gteeidnum;

    /**
     * 担保人国民经济部门
     */
    private String isgreenloan;

    /**
     * 担保人行业
     */
    private String brrowerindustry;

    /**
     * 担保人地区代码
     */
    private String brrowerareacode;

    /**
     * 担保人企业规模
     */
    private String enterprisescale;
    
    /**
     * 数据日期
     */
    private String sjrq;

	/**
	 * 校验状态 0:未校验;1:校验成功;2:校验失败
	 */
	private String checkstatus;

	/**
	 * 数据状态 0:待提交;1:待审核;2:审核不通过;3:审核通过
	 */
	private String datastatus;

	/**
	 * 操作人
	 */
	private String operator;

	/**
	 * 操作名
	 */
	private String operationname;

	/**
	 * 操作时间
	 */
	private String operationtime;

	/**
	 * 组织机构id
	 */
	private String orgid;

	/**
	 * 部门id
	 */
	private String departid;

	/**
	 * 审核不通过原因
	 */
	private String nopassreason;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getGteecontractcode() {
    return gteecontractcode;
  }

  public void setGteecontractcode(String gteecontractcode) {
    this.gteecontractcode = gteecontractcode;
  }


  public String getLoancontractcode() {
    return loancontractcode;
  }

  public void setLoancontractcode(String loancontractcode) {
    this.loancontractcode = loancontractcode;
  }


  public String getGteecontracttype() {
    return gteecontracttype;
  }

  public void setGteecontracttype(String gteecontracttype) {
    this.gteecontracttype = gteecontracttype;
  }


  public String getGteestartdate() {
    return gteestartdate;
  }

  public void setGteestartdate(String gteestartdate) {
    this.gteestartdate = gteestartdate;
  }


  public String getGteeenddate() {
    return gteeenddate;
  }

  public void setGteeenddate(String gteeenddate) {
    this.gteeenddate = gteeenddate;
  }


  public String getGteecurrency() {
    return gteecurrency;
  }

  public void setGteecurrency(String gteecurrency) {
    this.gteecurrency = gteecurrency;
  }


  public String getGteeamount() {
    return gteeamount;
  }

  public void setGteeamount(String gteeamount) {
    this.gteeamount = gteeamount;
  }


  public String getGteecnyamount() {
    return gteecnyamount;
  }

  public void setGteecnyamount(String gteecnyamount) {
    this.gteecnyamount = gteecnyamount;
  }


  public String getGteeidtype() {
    return gteeidtype;
  }

  public void setGteeidtype(String gteeidtype) {
    this.gteeidtype = gteeidtype;
  }


  public String getGteeidnum() {
    return gteeidnum;
  }

  public void setGteeidnum(String gteeidnum) {
    this.gteeidnum = gteeidnum;
  }


  public String getCheckstatus() {
    return checkstatus;
  }

  public void setCheckstatus(String checkstatus) {
    this.checkstatus = checkstatus;
  }


  public String getDatastatus() {
    return datastatus;
  }

  public void setDatastatus(String datastatus) {
    this.datastatus = datastatus;
  }


  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }


  public String getOperationname() {
    return operationname;
  }

  public void setOperationname(String operationname) {
    this.operationname = operationname;
  }


  public String getOperationtime() {
    return operationtime;
  }

  public void setOperationtime(String operationtime) {
    this.operationtime = operationtime;
  }


  public String getOrgid() {
    return orgid;
  }

  public void setOrgid(String orgid) {
    this.orgid = orgid;
  }


  public String getDepartid() {
    return departid;
  }

  public void setDepartid(String departid) {
    this.departid = departid;
  }


  public String getNopassreason() {
    return nopassreason;
  }

  public void setNopassreason(String nopassreason) {
    this.nopassreason = nopassreason;
  }

    public String getFinanceorgcode() {
        return financeorgcode;
    }

    public void setFinanceorgcode(String financeorgcode) {
        this.financeorgcode = financeorgcode;
    }

    public String getFinanceorginnum() {
        return financeorginnum;
    }

    public void setFinanceorginnum(String financeorginnum) {
        this.financeorginnum = financeorginnum;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getPledgerate() {
        return pledgerate;
    }

    public void setPledgerate(String pledgerate) {
        this.pledgerate = pledgerate;
    }

    public String getIsgreenloan() {
        return isgreenloan;
    }

    public void setIsgreenloan(String isgreenloan) {
        this.isgreenloan = isgreenloan;
    }

    public String getBrrowerindustry() {
        return brrowerindustry;
    }

    public void setBrrowerindustry(String brrowerindustry) {
        this.brrowerindustry = brrowerindustry;
    }

    public String getBrrowerareacode() {
        return brrowerareacode;
    }

    public void setBrrowerareacode(String brrowerareacode) {
        this.brrowerareacode = brrowerareacode;
    }

    public String getEnterprisescale() {
        return enterprisescale;
    }

    public void setEnterprisescale(String enterprisescale) {
        this.enterprisescale = enterprisescale;
    }
    
    public String getSjrq() {
		return sjrq;
	}

	public void setSjrq(String sjrq) {
		this.sjrq = sjrq;
	}
}
