package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name ="xftykhxbl")
public class Xftykhxbl {

	@Id
	private String id;

    /*
     *客户号码
     * */
    private String customernum;

    /*
     *客户金融机构代码
     * */
    private String customerfinorgcode;

    /*
     *客户金融机构内部机构号
     * */
    private String customerfinorginside;

    /*
     *客户金融机构地区代码
     * */
    private String customerfinorgarea;

    /**
	 * 金融机构代码
	 */
	private String finorgcode;

	/**
	 * 客户名称
	 */
	private String customername;

	/**
	 * 客户证件类型
	 */
	private String regamtcreny;
	
	/**
	 * 客户证件代码
	 */
	private String customercode;

	/**
	 * 基本存款账号
	 */
	private String depositnum;

	/**
	 * 基本账户开户行名称
	 */
	private String countopenbankname;

	/**
	 * 经营范围
	 */
	private String netamt;

	/**
	 * 注册资本
	 */
	private String regamt;

	/**
	 * 实收资本
	 */
	private String actamt;

	/**
	 * 总资产
	 */
	private String totalamt;

	/**
	 * 营业收入
	 */
	private String workarea;

	/**
	 * 从业人员数
	 */
	private String jobpeoplenum;

	/**
	 * 是否上市公司
	 */
	private String islistcmpy;
	
	/**
	 * 首次建立信贷关系日期
	 */
	private String fistdate;

	/**
	 * 注册地址
	 */
	private String regarea;

	/**
	 * 地区代码
	 */
	private String regareacode;
	
	/**
	 * 经营状态
	 */
	private String mngmestus;

	/**
	 * 成立日期
	 */
	private String setupdate;

	/**
	 * 所属行业
	 */
	private String industry;

	/**
	 * 企业规模
	 */
	private String entpmode;

	/**
	 * 客户经济成分
	 */
	private String entpczjjcf;

	/**
	 * 客户国民经济部门
	 */
	private String actamtcreny;

	/**
	 * 授信额度
	 */
	private String creditamt;

	/**
	 * 已用额度
	 */
	private String usedamt;

	/**
	 * 是否关联方
	 */
	private String isrelation;

	/**
	 * 实际控制人证件类型
	 */
	private String actctrlidtype;

	/**
	 * 实际控制人证件代码
	 */
	private String actctrlidcode;

	/**
	 * 客户信用级别总等级数
	 */
	private String workareacode;

	/**
	 * 客户信用评级
	 */
	private String actctrltype;

	/**
	 * 操作人
	 */
	private String operator;

	/**
	 * 操作时间
	 */
	private String operationtime;

	/**
	 * 组织机构id
	 */
	private String orgid;

	/**
	 * 部门id
	 */
	private String departid;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getFinorgcode() {
    return finorgcode;
  }

  public void setFinorgcode(String finorgcode) {
    this.finorgcode = finorgcode;
  }


  public String getCustomername() {
    return customername;
  }

  public void setCustomername(String customername) {
    this.customername = customername;
  }


  public String getCustomercode() {
    return customercode;
  }

  public void setCustomercode(String customercode) {
    this.customercode = customercode;
  }


  public String getDepositnum() {
    return depositnum;
  }

  public void setDepositnum(String depositnum) {
    this.depositnum = depositnum;
  }


  public String getCountopenbankname() {
    return countopenbankname;
  }

  public void setCountopenbankname(String countopenbankname) {
    this.countopenbankname = countopenbankname;
  }


  public String getRegamt() {
    return regamt;
  }

  public void setRegamt(String regamt) {
    this.regamt = regamt;
  }


  public String getRegamtcreny() {
    return regamtcreny;
  }

  public void setRegamtcreny(String regamtcreny) {
    this.regamtcreny = regamtcreny;
  }


  public String getActamt() {
    return actamt;
  }

  public void setActamt(String actamt) {
    this.actamt = actamt;
  }


  public String getActamtcreny() {
    return actamtcreny;
  }

  public void setActamtcreny(String actamtcreny) {
    this.actamtcreny = actamtcreny;
  }


  public String getTotalamt() {
    return totalamt;
  }

  public void setTotalamt(String totalamt) {
    this.totalamt = totalamt;
  }


  public String getNetamt() {
    return netamt;
  }

  public void setNetamt(String netamt) {
    this.netamt = netamt;
  }


  public String getIslistcmpy() {
    return islistcmpy;
  }

  public void setIslistcmpy(String islistcmpy) {
    this.islistcmpy = islistcmpy;
  }


  public String getFistdate() {
    return fistdate;
  }

  public void setFistdate(String fistdate) {
    this.fistdate = fistdate;
  }


  public String getJobpeoplenum() {
    return jobpeoplenum;
  }

  public void setJobpeoplenum(String jobpeoplenum) {
    this.jobpeoplenum = jobpeoplenum;
  }


  public String getRegarea() {
    return regarea;
  }

  public void setRegarea(String regarea) {
    this.regarea = regarea;
  }


  public String getRegareacode() {
    return regareacode;
  }

  public void setRegareacode(String regareacode) {
    this.regareacode = regareacode;
  }


  public String getWorkarea() {
    return workarea;
  }

  public void setWorkarea(String workarea) {
    this.workarea = workarea;
  }


  public String getWorkareacode() {
    return workareacode;
  }

  public void setWorkareacode(String workareacode) {
    this.workareacode = workareacode;
  }


  public String getMngmestus() {
    return mngmestus;
  }

  public void setMngmestus(String mngmestus) {
    this.mngmestus = mngmestus;
  }


  public String getSetupdate() {
    return setupdate;
  }

  public void setSetupdate(String setupdate) {
    this.setupdate = setupdate;
  }


  public String getIndustry() {
    return industry;
  }

  public void setIndustry(String industry) {
    this.industry = industry;
  }


  public String getEntpmode() {
    return entpmode;
  }

  public void setEntpmode(String entpmode) {
    this.entpmode = entpmode;
  }


  public String getEntpczjjcf() {
    return entpczjjcf;
  }

  public void setEntpczjjcf(String entpczjjcf) {
    this.entpczjjcf = entpczjjcf;
  }


  public String getCreditamt() {
    return creditamt;
  }

  public void setCreditamt(String creditamt) {
    this.creditamt = creditamt;
  }


  public String getUsedamt() {
    return usedamt;
  }

  public void setUsedamt(String usedamt) {
    this.usedamt = usedamt;
  }


  public String getIsrelation() {
    return isrelation;
  }

  public void setIsrelation(String isrelation) {
    this.isrelation = isrelation;
  }


  public String getActctrltype() {
    return actctrltype;
  }

  public void setActctrltype(String actctrltype) {
    this.actctrltype = actctrltype;
  }


  public String getActctrlidtype() {
    return actctrlidtype;
  }

  public void setActctrlidtype(String actctrlidtype) {
    this.actctrlidtype = actctrlidtype;
  }


  public String getActctrlidcode() {
    return actctrlidcode;
  }

  public void setActctrlidcode(String actctrlidcode) {
    this.actctrlidcode = actctrlidcode;
  }


  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }


  public String getOperationtime() {
    return operationtime;
  }

  public void setOperationtime(String operationtime) {
    this.operationtime = operationtime;
  }


  public String getOrgid() {
    return orgid;
  }

  public void setOrgid(String orgid) {
    this.orgid = orgid;
  }


  public String getDepartid() {
    return departid;
  }

  public void setDepartid(String departid) {
    this.departid = departid;
  }

    public String getCustomernum() {
        return customernum;
    }

    public void setCustomernum(String customernum) {
        this.customernum = customernum;
    }

    public String getCustomerfinorgcode() {
        return customerfinorgcode;
    }

    public void setCustomerfinorgcode(String customerfinorgcode) {
        this.customerfinorgcode = customerfinorgcode;
    }

    public String getCustomerfinorginside() {
        return customerfinorginside;
    }

    public void setCustomerfinorginside(String customerfinorginside) {
        this.customerfinorginside = customerfinorginside;
    }

    public String getCustomerfinorgarea() {
        return customerfinorgarea;
    }

    public void setCustomerfinorgarea(String customerfinorgarea) {
        this.customerfinorgarea = customerfinorgarea;
    }
    
}
