package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Description  
 * @Author  wangzd
 * @Date 2020-06-09
 * 个人客户基础信息历史表
 */

@Entity
@Table (name ="xgrkhxxh")
public class Xgrkhxxh {

	@Id
	private String id;

	/**
	 * 金融机构代码
	 */
	private String finorgcode;

	/**
	 * 客户证件类型
	 */
	private String regamtcreny;
	
	/**
	 * 客户证件代码
	 */
	private String customercode;

	/**
	 * 国籍
	 */
	private String country;

	/**
	 * 民族
	 */
	private String nation;

	/**
	 * 性别
	 */
	private String sex;

	/**
	 * 最高学历
	 */
	private String education;

	/**
	 * 出生日期
	 */
	private String birthday;

	/**
	 * 地区代码
	 */
	private String regareacode;
	
	/**
	 * 个人年收入
	 */
	private String grincome;

	/**
	 * 家庭年收入
	 */
	private String familyincome;

	/**
	 * 婚姻情况
	 */
	private String marriage;
	
	/**
	 * 是否关联方
	 */
	private String isrelation;

	/**
	 * 授信额度
	 */
	private String creditamt;

	/**
	 * 已用额度
	 */
	private String usedamt;

	/**
	 * 个人客户身份标识
	 */
	private String grkhsfbs;

	/**
	 * 个体工商户营业执照代码
	 */
	private String gtgshyyzzdm;

	/**
	 * 小微企业社会统一信用代码
	 */
	private String xwqyshtyxydm;
	
	/**
	 * 客户信用级别总等级数
	 */
	private String workareacode;

	/**
	 * 客户信用评级
	 */
	private String actctrltype;
	
	/**
     * 数据日期
     */
    private String sjrq;

	/**
	 * 校验状态 0:未校验;1:校验成功;2:校验失败
	 */
	private String checkstatus;

	/**
	 * 数据状态 0:待提交;1:待审核;2:审核不通过;3:审核通过
	 */
	private String datastatus;

	/**
	 * 操作人
	 */
	private String operator;

	/**
	 * 操作名
	 */
	private String operationname;

	/**
	 * 操作时间
	 */
	private String operationtime;

	/**
	 * 组织机构id
	 */
	private String orgid;

	/**
	 * 部门id
	 */
	private String departid;

	/**
	 * 审核不通过原因
	 */
	private String nopassreason;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFinorgcode() {
		return finorgcode;
	}

	public void setFinorgcode(String finorgcode) {
		this.finorgcode = finorgcode;
	}

	public String getRegamtcreny() {
		return regamtcreny;
	}

	public void setRegamtcreny(String regamtcreny) {
		this.regamtcreny = regamtcreny;
	}

	public String getCustomercode() {
		return customercode;
	}

	public void setCustomercode(String customercode) {
		this.customercode = customercode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getRegareacode() {
		return regareacode;
	}

	public void setRegareacode(String regareacode) {
		this.regareacode = regareacode;
	}

	public String getGrincome() {
		return grincome;
	}

	public void setGrincome(String grincome) {
		this.grincome = grincome;
	}

	public String getFamilyincome() {
		return familyincome;
	}

	public void setFamilyincome(String familyincome) {
		this.familyincome = familyincome;
	}

	public String getMarriage() {
		return marriage;
	}

	public void setMarriage(String marriage) {
		this.marriage = marriage;
	}

	public String getIsrelation() {
		return isrelation;
	}

	public void setIsrelation(String isrelation) {
		this.isrelation = isrelation;
	}

	public String getCreditamt() {
		return creditamt;
	}

	public void setCreditamt(String creditamt) {
		this.creditamt = creditamt;
	}

	public String getUsedamt() {
		return usedamt;
	}

	public void setUsedamt(String usedamt) {
		this.usedamt = usedamt;
	}

	public String getGrkhsfbs() {
		return grkhsfbs;
	}

	public void setGrkhsfbs(String grkhsfbs) {
		this.grkhsfbs = grkhsfbs;
	}

	public String getGtgshyyzzdm() {
		return gtgshyyzzdm;
	}

	public void setGtgshyyzzdm(String gtgshyyzzdm) {
		this.gtgshyyzzdm = gtgshyyzzdm;
	}

	public String getXwqyshtyxydm() {
		return xwqyshtyxydm;
	}

	public void setXwqyshtyxydm(String xwqyshtyxydm) {
		this.xwqyshtyxydm = xwqyshtyxydm;
	}

	public String getWorkareacode() {
		return workareacode;
	}

	public void setWorkareacode(String workareacode) {
		this.workareacode = workareacode;
	}

	public String getActctrltype() {
		return actctrltype;
	}

	public void setActctrltype(String actctrltype) {
		this.actctrltype = actctrltype;
	}

	public String getSjrq() {
		return sjrq;
	}

	public void setSjrq(String sjrq) {
		this.sjrq = sjrq;
	}

	public String getCheckstatus() {
		return checkstatus;
	}

	public void setCheckstatus(String checkstatus) {
		this.checkstatus = checkstatus;
	}

	public String getDatastatus() {
		return datastatus;
	}

	public void setDatastatus(String datastatus) {
		this.datastatus = datastatus;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getOperationname() {
		return operationname;
	}

	public void setOperationname(String operationname) {
		this.operationname = operationname;
	}

	public String getOperationtime() {
		return operationtime;
	}

	public void setOperationtime(String operationtime) {
		this.operationtime = operationtime;
	}

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	public String getDepartid() {
		return departid;
	}

	public void setDepartid(String departid) {
		this.departid = departid;
	}

	public String getNopassreason() {
		return nopassreason;
	}

	public void setNopassreason(String nopassreason) {
		this.nopassreason = nopassreason;
	}

	

}
