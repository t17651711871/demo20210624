package com.geping.etl.UNITLOAN.entity.baseInfo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**    
*  民族代码
* @author liuweixin  
* @date 2020年11月4日 下午3:32:39  
*/
@Entity
@Table (name ="basenation")
public class BaseNation {

	@Id
	private String id;

	/**
	 * 代码
	 */
	private String nationcode;

	/**
	 * 名称
	 */
	private String nationname;

	/**
	 * 说明
	 */
	private String nationdesc;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNationcode() {
		return nationcode;
	}

	public void setNationcode(String nationcode) {
		this.nationcode = nationcode;
	}

	public String getNationname() {
		return nationname;
	}

	public void setNationname(String nationname) {
		this.nationname = nationname;
	}

	public String getNationdesc() {
		return nationdesc;
	}

	public void setNationdesc(String nationdesc) {
		this.nationdesc = nationdesc;
	}
	
	
}
