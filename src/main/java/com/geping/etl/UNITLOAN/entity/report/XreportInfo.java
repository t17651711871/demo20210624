package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name ="xreportinfo")
public class XreportInfo {

	@Id
	private String id;

	/**
	 * 报文名
	 */
	private String reportname;

    /**
     * 报文路径
     */
    private String reporturl;

	/**
	 * 报文生成时间
	 */
	private String reportdate;

	/**
	 * 操作人
	 */
	private String operator;

	/**
	 * 组织机构id
	 */
	private String orgid;

	/**
	 * 部门id
	 */
	private String departid;

    public XreportInfo() {
    }

    public XreportInfo(String id, String reportname, String reporturl, String reportdate, String operator, String orgid, String departid) {
        this.id = id;
        this.reportname = reportname;
        this.reporturl = reporturl;
        this.reportdate = reportdate;
        this.operator = operator;
        this.orgid = orgid;
        this.departid = departid;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReportname() {
        return reportname;
    }

    public void setReportname(String reportname) {
        this.reportname = reportname;
    }

    public String getReportdate() {
        return reportdate;
    }

    public void setReportdate(String reportdate) {
        this.reportdate = reportdate;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getDepartid() {
        return departid;
    }

    public void setDepartid(String departid) {
        this.departid = departid;
    }

    public String getReporturl() {
        return reporturl;
    }

    public void setReporturl(String reporturl) {
        this.reporturl = reporturl;
    }
}
