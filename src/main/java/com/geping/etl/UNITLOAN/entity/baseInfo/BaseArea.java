package com.geping.etl.UNITLOAN.entity.baseInfo;

import javax.persistence.*;

/**
 * 行政区划代码  
 * @Author  wangzd
 * @Date 2020-06-11 
 */

@Entity
@Table (name ="basearea")
public class BaseArea {

	@Id
	private String id;

	/**
	 * 行政区划代码 
	 */
	private String areacode;

	/**
	 * 地区名称
	 */
	private String areaname;

	/**
	 * 地区描述-备用
	 */
	private String areadesc;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getAreacode() {
    return areacode;
  }

  public void setAreacode(String areacode) {
    this.areacode = areacode;
  }


  public String getAreaname() {
    return areaname;
  }

  public void setAreaname(String areaname) {
    this.areaname = areaname;
  }


  public String getAreadesc() {
    return areadesc;
  }

  public void setAreadesc(String areadesc) {
    this.areadesc = areadesc;
  }

}
