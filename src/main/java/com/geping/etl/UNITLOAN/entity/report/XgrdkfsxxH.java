package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Description  
 * @Author  chenggen
 * @Date 2020-11-06
 */

@Entity
@Table (name ="xgrdkfsxxh")
public class XgrdkfsxxH {

	@Id
	private Integer id;
	/**
	 * 1.金融机构代码
	 */
	private String finorgcode;
	/**
	 * 2.内部机构号
	 */
	private String finorgincode;
	/**
	 * 3.金融机构地区代码
	 */
	private String finorgareacode;
	/**
	 * 4.借款人证件类型
	 */
	private String isfarmerloan;
	/**
	 * 5.借款人证件代码
	 */
	private String browidcode;

	/**
	 * 8.借款人地区代码
	 */
	private String browareacode;

	/**
	 * 9.贷款借据编码
	 */
	private String loanbrowcode;
	/**
	 * 10.贷款合同编码
	 */
	private String loancontractcode;
	/**
	 * 11.贷款产品类别
	 */
	private String loanprocode;

	/**
	 * 11.贷款发放日期
	 */
	private String loanstartdate;
	/**
	 * 12.贷款到期日期
	 */
	private String loanenddate;
	/**
	 * 13.贷款实际终止日期
	 */
	private String loanactenddate;
	/**
	 * 14.贷款币种
	 */
	private String loancurrency;
	/**
	 * 15.贷款发生金额
	 */
	private String loanamt;
	/**
	 * 16.贷款发生金额折人民币
	 */
	private String loancnyamt;
	/**
	 * 17.利率是否固定
	 */
	private String rateisfix;
	/**
	 * 18.利率水平
	 */
	private String ratelevel;
	/**
	 * 19.贷款定价基准类型
	 */
	private String loanfixamttype;
	/**
	 * 20.基准利率
	 */
	private String rate;
	/**
	 * 21.贷款财政扶持方式
	 */
	private String loanfinancesupport;
	/**
	 * 22.贷款利率重新定价日
	 */
	private String loanraterepricedate;
	/**
	 * 23.贷款担保方式
	 */
	private String gteemethod;
	/**
	 * 24.是否首次贷款
	 */
	private String isplatformloan;
	/**
	 * 25.贷款状态
	 */
	private String loanstatus;
	/**
     * 26.资产证券化产品代码
     */
    private String assetproductcode;
	/**
     * 27.贷款重组方式
     */
    private String loanrestructuring;
	/**
	 * 28.发放/收回标识
	 */
	private String givetakeid;
    /**
     * 29.交易流水号
     */
    private String transactionnum;
	/**
	 * 30.贷款用途
	 */
	private String issupportliveloan;

    /**
     * 数据日期
     */
    private String sjrq;
	/**
	 * 校验状态 0:未校验;1:校验成功;2:校验失败
	 */
	private String checkstatus;
	/**
	 * 数据状态 0:待提交;1:待审核;2:审核不通过;3:审核通过
	 */
	private String datastatus;
	/**
	 * 操作人
	 */
	private String operator;
	/**
	 * 操作名
	 */
	private String operationname;
	/**
	 * 操作时间
	 */
	private String operationtime;
	/**
	 * 组织机构id
	 */
	private String orgid;
	/**
	 * 部门id
	 */
	private String departid;
	/**
	 * 审核不通过原因
	 */
	private String nopassreason;


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public String getFinorgcode() {
    return finorgcode;
  }

  public void setFinorgcode(String finorgcode) {
    this.finorgcode = finorgcode;
  }


  public String getFinorgincode() {
    return finorgincode;
  }

  public void setFinorgincode(String finorgincode) {
    this.finorgincode = finorgincode;
  }


  public String getFinorgareacode() {
    return finorgareacode;
  }

  public void setFinorgareacode(String finorgareacode) {
    this.finorgareacode = finorgareacode;
  }


  public String getBrowidcode() {
    return browidcode;
  }

  public void setBrowidcode(String browidcode) {
    this.browidcode = browidcode;
  }


  public String getBrowareacode() {
    return browareacode;
  }

  public void setBrowareacode(String browareacode) {
    this.browareacode = browareacode;
  }


  public String getLoanbrowcode() {
    return loanbrowcode;
  }

  public void setLoanbrowcode(String loanbrowcode) {
    this.loanbrowcode = loanbrowcode;
  }


  public String getLoancontractcode() {
    return loancontractcode;
  }

  public void setLoancontractcode(String loancontractcode) {
    this.loancontractcode = loancontractcode;
  }


  public String getLoanprocode() {
    return loanprocode;
  }

  public void setLoanprocode(String loanprocode) {
    this.loanprocode = loanprocode;
  }

  public String getLoanstartdate() {
    return loanstartdate;
  }

  public void setLoanstartdate(String loanstartdate) {
    this.loanstartdate = loanstartdate;
  }


  public String getLoanenddate() {
    return loanenddate;
  }

  public void setLoanenddate(String loanenddate) {
    this.loanenddate = loanenddate;
  }


  public String getLoanactenddate() {
    return loanactenddate;
  }

  public void setLoanactenddate(String loanactenddate) {
    this.loanactenddate = loanactenddate;
  }


  public String getLoancurrency() {
    return loancurrency;
  }

  public void setLoancurrency(String loancurrency) {
    this.loancurrency = loancurrency;
  }


  public String getLoanamt() {
    return loanamt;
  }

  public void setLoanamt(String loanamt) {
    this.loanamt = loanamt;
  }


  public String getLoancnyamt() {
    return loancnyamt;
  }

  public void setLoancnyamt(String loancnyamt) {
    this.loancnyamt = loancnyamt;
  }


  public String getRateisfix() {
    return rateisfix;
  }

  public void setRateisfix(String rateisfix) {
    this.rateisfix = rateisfix;
  }


  public String getRatelevel() {
    return ratelevel;
  }

  public void setRatelevel(String ratelevel) {
    this.ratelevel = ratelevel;
  }


  public String getLoanfixamttype() {
    return loanfixamttype;
  }

  public void setLoanfixamttype(String loanfixamttype) {
    this.loanfixamttype = loanfixamttype;
  }


  public String getRate() {
    return rate;
  }

  public void setRate(String rate) {
    this.rate = rate;
  }


  public String getLoanfinancesupport() {
    return loanfinancesupport;
  }

  public void setLoanfinancesupport(String loanfinancesupport) {
    this.loanfinancesupport = loanfinancesupport;
  }


  public String getLoanraterepricedate() {
    return loanraterepricedate;
  }

  public void setLoanraterepricedate(String loanraterepricedate) {
    this.loanraterepricedate = loanraterepricedate;
  }


  public String getGteemethod() {
    return gteemethod;
  }

  public void setGteemethod(String gteemethod) {
    this.gteemethod = gteemethod;
  }


  public String getLoanstatus() {
    return loanstatus;
  }

  public void setLoanstatus(String loanstatus) {
    this.loanstatus = loanstatus;
  }


  public String getGivetakeid() {
    return givetakeid;
  }

  public void setGivetakeid(String givetakeid) {
    this.givetakeid = givetakeid;
  }


  public String getIsfarmerloan() {
    return isfarmerloan;
  }

  public void setIsfarmerloan(String isfarmerloan) {
    this.isfarmerloan = isfarmerloan;
  }


  public String getIsplatformloan() {
    return isplatformloan;
  }

  public void setIsplatformloan(String isplatformloan) {
    this.isplatformloan = isplatformloan;
  }


  public String getIssupportliveloan() {
    return issupportliveloan;
  }

  public void setIssupportliveloan(String issupportliveloan) {
    this.issupportliveloan = issupportliveloan;
  }


  public String getCheckstatus() {
    return checkstatus;
  }

  public void setCheckstatus(String checkstatus) {
    this.checkstatus = checkstatus;
  }


  public String getDatastatus() {
    return datastatus;
  }

  public void setDatastatus(String datastatus) {
    this.datastatus = datastatus;
  }


  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }


  public String getOperationname() {
    return operationname;
  }

  public void setOperationname(String operationname) {
    this.operationname = operationname;
  }


  public String getOperationtime() {
    return operationtime;
  }

  public void setOperationtime(String operationtime) {
    this.operationtime = operationtime;
  }


  public String getOrgid() {
    return orgid;
  }

  public void setOrgid(String orgid) {
    this.orgid = orgid;
  }


  public String getDepartid() {
    return departid;
  }

  public void setDepartid(String departid) {
    this.departid = departid;
  }


  public String getNopassreason() {
    return nopassreason;
  }

  public void setNopassreason(String nopassreason) {
    this.nopassreason = nopassreason;
  }

    public String getAssetproductcode() {
        return assetproductcode;
    }

    public void setAssetproductcode(String assetproductcode) {
        this.assetproductcode = assetproductcode;
    }

    public String getLoanrestructuring() {
        return loanrestructuring;
    }

    public void setLoanrestructuring(String loanrestructuring) {
        this.loanrestructuring = loanrestructuring;
    }

    public String getTransactionnum() {
        return transactionnum;
    }

    public void setTransactionnum(String transactionnum) {
        this.transactionnum = transactionnum;
    }

	public String getSjrq() {
		return sjrq;
	}

	public void setSjrq(String sjrq) {
		this.sjrq = sjrq;
	}
    
}
