package com.geping.etl.UNITLOAN.entity.report;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *存量债券发行信息
 */
@Entity
@Table(name = "xclzqfxxxh")
public class XclzqfxxxH {
  @Id
  private String id;
  private String financeorgcode;//金融机构代码
  private String zqdm;//债券代码
  private String zqztgjg;//债券总托管机构
  private String zqpz;//债券品种
  private String zqxyjg;//债券信用级别
  private String xfcs;//续发次数
  private String bz;//币种
  private String qmzqmz;//期末债券面值
  private String qmzqmzzrmb;//期末债券面值折人民币
  private String qmzqye;//期末债券余额
  private String qmzqyezrmb;//期末债券余额折人名币
  private String zqzwdj;//债权债务登记日
  private String qxr;//起息日
  private String dfrq;//兑付日期
  private String fxfs;//付息方式
  private String pmll;//票面利率
  private String checkstatus;
  private String datastatus;
  private String operator;
  private String operationname;
  private String operationtime;
  private String orgid;
  private String departid;
  private String nopassreason;
  private String sjrq;
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFinanceorgcode() {
    return financeorgcode;
  }

  public void setFinanceorgcode(String financeorgcode) {
    this.financeorgcode = financeorgcode;
  }

  public String getZqdm() {
    return zqdm;
  }

  public void setZqdm(String zqdm) {
    this.zqdm = zqdm;
  }

  public String getZqztgjg() {
    return zqztgjg;
  }

  public void setZqztgjg(String zqztgjg) {
    this.zqztgjg = zqztgjg;
  }

  public String getZqpz() {
    return zqpz;
  }

  public void setZqpz(String zqpz) {
    this.zqpz = zqpz;
  }

  public String getZqxyjg() {
    return zqxyjg;
  }

  public void setZqxyjg(String zqxyjg) {
    this.zqxyjg = zqxyjg;
  }

  public String getXfcs() {
    return xfcs;
  }

  public void setXfcs(String xfcs) {
    this.xfcs = xfcs;
  }

  public String getBz() {
    return bz;
  }

  public void setBz(String bz) {
    this.bz = bz;
  }

  public String getQmzqmz() {
    return qmzqmz;
  }

  public void setQmzqmz(String qmzqmz) {
    this.qmzqmz = qmzqmz;
  }

  public String getQmzqmzzrmb() {
    return qmzqmzzrmb;
  }

  public void setQmzqmzzrmb(String qmzqmzzrmb) {
    this.qmzqmzzrmb = qmzqmzzrmb;
  }

  public String getQmzqye() {
    return qmzqye;
  }

  public void setQmzqye(String qmzqye) {
    this.qmzqye = qmzqye;
  }

  public String getQmzqyezrmb() {
    return qmzqyezrmb;
  }

  public void setQmzqyezrmb(String qmzqyezrmb) {
    this.qmzqyezrmb = qmzqyezrmb;
  }

  public String getZqzwdj() {
    return zqzwdj;
  }

  public void setZqzwdj(String zqzwdj) {
    this.zqzwdj = zqzwdj;
  }

  public String getQxr() {
    return qxr;
  }

  public void setQxr(String qxr) {
    this.qxr = qxr;
  }

  public String getDfrq() {
    return dfrq;
  }

  public void setDfrq(String dfrq) {
    this.dfrq = dfrq;
  }

  public String getFxfs() {
    return fxfs;
  }

  public void setFxfs(String fxfs) {
    this.fxfs = fxfs;
  }

  public String getPmll() {
    return pmll;
  }

  public void setPmll(String pmll) {
    this.pmll = pmll;
  }

  public String getCheckstatus() {
    return checkstatus;
  }

  public void setCheckstatus(String checkstatus) {
    this.checkstatus = checkstatus;
  }

  public String getDatastatus() {
    return datastatus;
  }

  public void setDatastatus(String datastatus) {
    this.datastatus = datastatus;
  }

  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public String getOperationname() {
    return operationname;
  }

  public void setOperationname(String operationname) {
    this.operationname = operationname;
  }

  public String getOperationtime() {
    return operationtime;
  }

  public void setOperationtime(String operationtime) {
    this.operationtime = operationtime;
  }

  public String getOrgid() {
    return orgid;
  }

  public void setOrgid(String orgid) {
    this.orgid = orgid;
  }

  public String getDepartid() {
    return departid;
  }

  public void setDepartid(String departid) {
    this.departid = departid;
  }

  public String getNopassreason() {
    return nopassreason;
  }

  public void setNopassreason(String nopassreason) {
    this.nopassreason = nopassreason;
  }

  public String getSjrq() {
    return sjrq;
  }

  public void setSjrq(String sjrq) {
    this.sjrq = sjrq;
  }
}
