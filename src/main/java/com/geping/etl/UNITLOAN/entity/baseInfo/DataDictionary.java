package com.geping.etl.UNITLOAN.entity.baseInfo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/***
 * 数据字典实体
 * @author liang.xu
 * @date 2021.4.29
 */
@Entity
@Table (name ="data_dictionary")
public class DataDictionary {

    @Id
    private String id;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 代码
     */
    private String code;

    /**
     * 名称
     */
    private String text;


    /**
     * 排序字段
     */
    private Integer px;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
    return id;
  }

    public void setId(String id) {
    this.id = id;
  }

    public Integer getPx() {
        return px;
    }

    public void setPx(Integer px) {
        this.px = px;
    }
}
