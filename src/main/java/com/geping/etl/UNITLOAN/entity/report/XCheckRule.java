package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**    
*  
* @author liuweixin  
* @date 2020年9月21日 上午9:08:31  
*/
@Entity
@Table(name = "xcheckrule")
public class XCheckRule {

	@Id
	private String id;
	
	private String checknum;	//检核标准编号
	private String reportname;	//报文名称
	private String tablename;	//表名
	private String fieldname;	//字段名
	private String checkrule;	//规则说明
	private String tablesj;		//涉及的表
	private String rulesx;		//规则实现逻辑
	private String rulelimit;	//限定条件
	private String rulegl;		//关联关系
	private String isstart;		//是否启用
	private String isselect;	//是否选择
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getChecknum() {
		return checknum;
	}
	public void setChecknum(String checknum) {
		this.checknum = checknum;
	}
	public String getReportname() {
		return reportname;
	}
	public void setReportname(String reportname) {
		this.reportname = reportname;
	}
	public String getTablename() {
		return tablename;
	}
	public void setTablename(String tablename) {
		this.tablename = tablename;
	}
	public String getFieldname() {
		return fieldname;
	}
	public void setFieldname(String fieldname) {
		this.fieldname = fieldname;
	}
	public String getCheckrule() {
		return checkrule;
	}
	public void setCheckrule(String checkrule) {
		this.checkrule = checkrule;
	}
	public String getTablesj() {
		return tablesj;
	}
	public void setTablesj(String tablesj) {
		this.tablesj = tablesj;
	}
	public String getRulesx() {
		return rulesx;
	}
	public void setRulesx(String rulesx) {
		this.rulesx = rulesx;
	}
	public String getRulelimit() {
		return rulelimit;
	}
	public void setRulelimit(String rulelimit) {
		this.rulelimit = rulelimit;
	}
	public String getRulegl() {
		return rulegl;
	}
	public void setRulegl(String rulegl) {
		this.rulegl = rulegl;
	}
	public String getIsstart() {
		return isstart;
	}
	public void setIsstart(String isstart) {
		this.isstart = isstart;
	}
	public String getIsselect() {
		return isselect;
	}
	public void setIsselect(String isselect) {
		this.isselect = isselect;
	}
	
	
}
