package com.geping.etl.UNITLOAN.entity.report;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 债券投资发生额信息
 */
@Entity
@Table(name = "xzqtzfsexxh")
public class XzqtzfsexxH {
  @Id
  private String id;
  private String financeorgcode;//金融机构代码
  private String financeorginnum;//内部机构号
  private String zqdm;//债券代码
  private String zqztgjg;//债券总托管机构
  private String zqpz;//债券品种
  private String zqxyjg;//债券信用级别
  private String bz;//币种
  private String zqzwdj;//债权债务登记 日
  private String qxr;//起息日
  private String dfrq;//兑付日期
  private String pmll;//票面利率
  private String fxrzjdm;//发行人证件代 码
  private String fxrdqdm;//发行人地区代 码
  private String fxrhy;//发行人行业
  private String fxrqygm;//发行人企业规 模
  private String fxrjjcf;//发行人经济成 分
  private String fxrgmjjbm;//发行人国民经 济部门
  private String jyrq;//交易日期
  private String jylsh;//交易流水号
  private String cjje;//成交金额
  private String cjjezrmb;//成交金额折人名币
  private String mrmcbz;//买入/卖出标 志
  private String checkstatus;
  private String datastatus;
  private String operator;
  private String operationname;
  private String operationtime;
  private String orgid;
  private String departid;
  private String nopassreason;
  private String sjrq;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getFinanceorgcode() {
    return financeorgcode;
  }

  public void setFinanceorgcode(String financeorgcode) {
    this.financeorgcode = financeorgcode;
  }


  public String getFinanceorginnum() {
    return financeorginnum;
  }

  public void setFinanceorginnum(String financeorginnum) {
    this.financeorginnum = financeorginnum;
  }


  public String getZqdm() {
    return zqdm;
  }

  public void setZqdm(String zqdm) {
    this.zqdm = zqdm;
  }


  public String getZqztgjg() {
    return zqztgjg;
  }

  public void setZqztgjg(String zqztgjg) {
    this.zqztgjg = zqztgjg;
  }


  public String getZqpz() {
    return zqpz;
  }

  public void setZqpz(String zqpz) {
    this.zqpz = zqpz;
  }


  public String getZqxyjg() {
    return zqxyjg;
  }

  public void setZqxyjg(String zqxyjg) {
    this.zqxyjg = zqxyjg;
  }


  public String getBz() {
    return bz;
  }

  public void setBz(String bz) {
    this.bz = bz;
  }


  public String getZqzwdj() {
    return zqzwdj;
  }

  public void setZqzwdj(String zqzwdj) {
    this.zqzwdj = zqzwdj;
  }


  public String getQxr() {
    return qxr;
  }

  public void setQxr(String qxr) {
    this.qxr = qxr;
  }


  public String getDfrq() {
    return dfrq;
  }

  public void setDfrq(String dfrq) {
    this.dfrq = dfrq;
  }


  public String getPmll() {
    return pmll;
  }

  public void setPmll(String pmll) {
    this.pmll = pmll;
  }


  public String getFxrzjdm() {
    return fxrzjdm;
  }

  public void setFxrzjdm(String fxrzjdm) {
    this.fxrzjdm = fxrzjdm;
  }


  public String getFxrdqdm() {
    return fxrdqdm;
  }

  public void setFxrdqdm(String fxrdqdm) {
    this.fxrdqdm = fxrdqdm;
  }


  public String getFxrhy() {
    return fxrhy;
  }

  public void setFxrhy(String fxrhy) {
    this.fxrhy = fxrhy;
  }


  public String getFxrqygm() {
    return fxrqygm;
  }

  public void setFxrqygm(String fxrqygm) {
    this.fxrqygm = fxrqygm;
  }


  public String getFxrjjcf() {
    return fxrjjcf;
  }

  public void setFxrjjcf(String fxrjjcf) {
    this.fxrjjcf = fxrjjcf;
  }


  public String getFxrgmjjbm() {
    return fxrgmjjbm;
  }

  public void setFxrgmjjbm(String fxrgmjjbm) {
    this.fxrgmjjbm = fxrgmjjbm;
  }


  public String getJyrq() {
    return jyrq;
  }

  public void setJyrq(String jyrq) {
    this.jyrq = jyrq;
  }


  public String getJylsh() {
    return jylsh;
  }

  public void setJylsh(String jylsh) {
    this.jylsh = jylsh;
  }


  public String getCjje() {
    return cjje;
  }

  public void setCjje(String cjje) {
    this.cjje = cjje;
  }


  public String getCjjezrmb() {
    return cjjezrmb;
  }

  public void setCjjezrmb(String cjjezrmb) {
    this.cjjezrmb = cjjezrmb;
  }


  public String getMrmcbz() {
    return mrmcbz;
  }

  public void setMrmcbz(String mrmcbz) {
    this.mrmcbz = mrmcbz;
  }


  public String getCheckstatus() {
    return checkstatus;
  }

  public void setCheckstatus(String checkstatus) {
    this.checkstatus = checkstatus;
  }


  public String getDatastatus() {
    return datastatus;
  }

  public void setDatastatus(String datastatus) {
    this.datastatus = datastatus;
  }


  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }


  public String getOperationname() {
    return operationname;
  }

  public void setOperationname(String operationname) {
    this.operationname = operationname;
  }


  public String getOperationtime() {
    return operationtime;
  }

  public void setOperationtime(String operationtime) {
    this.operationtime = operationtime;
  }


  public String getOrgid() {
    return orgid;
  }

  public void setOrgid(String orgid) {
    this.orgid = orgid;
  }


  public String getDepartid() {
    return departid;
  }

  public void setDepartid(String departid) {
    this.departid = departid;
  }


  public String getNopassreason() {
    return nopassreason;
  }

  public void setNopassreason(String nopassreason) {
    this.nopassreason = nopassreason;
  }


  public String getSjrq() {
    return sjrq;
  }

  public void setSjrq(String sjrq) {
    this.sjrq = sjrq;
  }




}
