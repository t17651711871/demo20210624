package com.geping.etl.UNITLOAN.entity.baseInfo;

import javax.persistence.*;

/**
 * @Description  
 * @Author  wangzd
 * @Date 2020-06-23 
 */

@Entity
@Table (name ="baseamt")
public class BaseAmt {

	@Id
	private String id;

	/**
	 * 代码
	 */
	private String code;

	/**
	 * 编号
	 */
	private String number;

	/**
	 * 金额
	 */
	private String amt;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }


  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }


  public String getAmt() {
    return amt;
  }

  public void setAmt(String amt) {
    this.amt = amt;
  }

}
