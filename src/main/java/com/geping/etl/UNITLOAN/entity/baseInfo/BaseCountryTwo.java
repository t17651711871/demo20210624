package com.geping.etl.UNITLOAN.entity.baseInfo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**    
*  国籍代码
* @author liuweixin  
* @date 2020年11月4日 下午3:32:11  
*/
@Entity
@Table (name ="basecountrytwo")
public class BaseCountryTwo {

	@Id
	private String id;

	/**
	 * 代码
	 */
	private String countrytwocode;

	/**
	 * 名称
	 */
	private String countrytwoname;

	/**
	 * 说明
	 */
	private String countrytwodesc;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCountrytwocode() {
		return countrytwocode;
	}

	public void setCountrytwocode(String countrytwocode) {
		this.countrytwocode = countrytwocode;
	}

	public String getCountrytwoname() {
		return countrytwoname;
	}

	public void setCountrytwoname(String countrytwoname) {
		this.countrytwoname = countrytwoname;
	}

	public String getCountrytwodesc() {
		return countrytwodesc;
	}

	public void setCountrytwodesc(String countrytwodesc) {
		this.countrytwodesc = countrytwodesc;
	}
	
	
}
