package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.*;

/**
 * 金融机构（法人基础信息）-利润及资本统计表正式表
 * @Author  wuzengwen
 * @Date 2020-07-08 
 */

@Entity
@Table (name ="xjrjgfrprofitH")
public class XjrjgfrProfitH {

	@Id
	private String id;

	/**
	 * 一、营业收入

	 */
	private String yysr;

	/**
	 *   1.利息净收入
	 */
	private String lxjsr;

	/**
	 *     利息收入

	 */
	private String lxsr;

	/**
	 *       金融机构往来利息收入

	 */
	private String jrjgwllxsr;

	/**
	 *         其中：系统内往来利息收入

	 */
	private String xtnwllxsr;

	/**
	 *       各项贷款利息收入
	 */
	private String gxdklxsr;

	/**
	 *       债券利息收入

	 */
	private String zqlxsr;

	/**
	 *       其他利息收入

	 */
	private String qtlxsr;

	/**
	 *     利息支出

	 */
	private String lxzc;

	/**
	 *       金融机构往来利息支出

	 */
	private String jrjgwllxzc;

	/**
	 *         其中：系统内往来利息支出

	 */
	private String xtnwllxzc;

	/**
	 *       各项存款利息支出

	 */
	private String gxcklxzc;

	/**
	 *       债券利息支出

	 */
	private String zqlxzc;

	/**
	 *       其他利息支出

	 */
	private String qtlxzc;

	/**
	 *   2.手续费及佣金净收入

	 */
	private String sxfjyjjsr;

	/**
	 *     手续费及佣金收入

	 */
	private String sxfjyjsr;

	/**
	 *     手续费及佣金支出

	 */
	private String jxfjyjzc;

	/**
	 *   3.租赁收益

	 */
	private String zlsy;

	/**
	 *   4.投资收益

	 */
	private String tzsy;

	/**
	 *     债券投资收益

	 */
	private String zqtzsy;

	/**
	 *     股权投资收益

	 */
	private String gqtzsy;

	/**
	 *     其他投资收益
	 */
	private String qttzsy;

	/**
	 *   5.公允价值变动收益

	 */
	private String gyjzbdsy;

	/**
	 *   6.汇兑净收益

	 */
	private String hdjsy;
	
	/**
	 *   7.资产处置收益

	 */
	private String zcczsy;

	/**
	 *   8.其他业务收入

	 */
	private String qtywsr;

	/**
	 * 二、营业支出

	 */
	private String yyzc;

	/**
	 *   1.业务及管理费

	 */
	private String ywjglf;

	/**
	 *     其中:职工工资

	 */
	private String zggz;

	/**
	 *     福利费

	 */
	private String flf;

	/**
	 *     住房公积金和住房补贴

	 */
	private String zfgjjhzfbt;

	/**
	 *   2.税金及附加

	 */
	private String sjjfj;

	/**
	 *   3.资产减值损失

	 */
	private String zcjzss;

	/**
	 *   4.其他业务支出

	 */
	private String qtywzc;

	/**
	 * 三、营业利润

	 */
	private String yylr;

	/**
	 *   营业外收入（加）

	 */
	private String yywsr;

	/**
	 *   营业外支出（减）

	 */
	private String yywzc;

	/**
	 * 四、利润总额

	 */
	private String lrze;

	/**
	 *   所得税（减）

	 */
	private String sds;

	/**
	 * 五、净利润

	 */
	private String jlr;

	/**
	 *   年度损益调整（加）

	 */
	private String ndsytz;

	/**
	 *   留存利润

	 */
	private String lclr;

	/**
	 * 六、未分配利润

	 */
	private String wfplr;

	/**
	 * 应纳增值税

	 */
	private String ynzzs;

	/**
	 * 核心一级资本净额

	 */
	private String hxyjzbje;

	/**
	 * 一级资本净额

	 */
	private String yjzbje;

	/**
	 * 资本净额

	 */
	private String zbje;

	/**
	 * 应用资本底线及校准后的风险加权资产合计
	 */
	private String ygzbjfxjqzchj;

	/*
	 *   数据日期
	 */
	private  String sjrq;

	/**
	 * 校验状态 0:未校验;1:校验成功;2:校验失败
	 */
	private String checkstatus;

	/**
	 * 数据状态 0:待提交;1:待审核;2:审核不通过;3:审核通过
	 */
	private String datastatus;

	/**
	 * 操作人
	 */
	private String operator;

	/**
	 * 操作名
	 */
	private String operationname;

	/**
	 * 操作时间
	 */
	private String operationtime;

	/**
	 * 组织机构id
	 */
	private String orgid;

	/**
	 * 部门id
	 */
	private String departid;

	/**
	 * 审核不通过原因
	 */
	private String nopassreason;

	public String getSjrq() {
		return sjrq;
	}

	public void setSjrq(String sjrq) {
		this.sjrq = sjrq;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getYysr() {
		return yysr;
	}

	public void setYysr(String yysr) {
		this.yysr = yysr;
	}

	public String getLxjsr() {
		return lxjsr;
	}

	public void setLxjsr(String lxjsr) {
		this.lxjsr = lxjsr;
	}

	public String getLxsr() {
		return lxsr;
	}

	public void setLxsr(String lxsr) {
		this.lxsr = lxsr;
	}

	public String getJrjgwllxsr() {
		return jrjgwllxsr;
	}

	public void setJrjgwllxsr(String jrjgwllxsr) {
		this.jrjgwllxsr = jrjgwllxsr;
	}

	public String getXtnwllxsr() {
		return xtnwllxsr;
	}

	public void setXtnwllxsr(String xtnwllxsr) {
		this.xtnwllxsr = xtnwllxsr;
	}

	public String getGxdklxsr() {
		return gxdklxsr;
	}

	public void setGxdklxsr(String gxdklxsr) {
		this.gxdklxsr = gxdklxsr;
	}

	public String getZqlxsr() {
		return zqlxsr;
	}

	public void setZqlxsr(String zqlxsr) {
		this.zqlxsr = zqlxsr;
	}

	public String getQtlxsr() {
		return qtlxsr;
	}

	public void setQtlxsr(String qtlxsr) {
		this.qtlxsr = qtlxsr;
	}

	public String getLxzc() {
		return lxzc;
	}

	public void setLxzc(String lxzc) {
		this.lxzc = lxzc;
	}

	public String getJrjgwllxzc() {
		return jrjgwllxzc;
	}

	public void setJrjgwllxzc(String jrjgwllxzc) {
		this.jrjgwllxzc = jrjgwllxzc;
	}

	public String getXtnwllxzc() {
		return xtnwllxzc;
	}

	public void setXtnwllxzc(String xtnwllxzc) {
		this.xtnwllxzc = xtnwllxzc;
	}

	public String getGxcklxzc() {
		return gxcklxzc;
	}

	public void setGxcklxzc(String gxcklxzc) {
		this.gxcklxzc = gxcklxzc;
	}

	public String getZqlxzc() {
		return zqlxzc;
	}

	public void setZqlxzc(String zqlxzc) {
		this.zqlxzc = zqlxzc;
	}

	public String getQtlxzc() {
		return qtlxzc;
	}

	public void setQtlxzc(String qtlxzc) {
		this.qtlxzc = qtlxzc;
	}

	public String getSxfjyjjsr() {
		return sxfjyjjsr;
	}

	public void setSxfjyjjsr(String sxfjyjjsr) {
		this.sxfjyjjsr = sxfjyjjsr;
	}

	public String getSxfjyjsr() {
		return sxfjyjsr;
	}

	public void setSxfjyjsr(String sxfjyjsr) {
		this.sxfjyjsr = sxfjyjsr;
	}

	public String getJxfjyjzc() {
		return jxfjyjzc;
	}

	public void setJxfjyjzc(String jxfjyjzc) {
		this.jxfjyjzc = jxfjyjzc;
	}

	public String getZlsy() {
		return zlsy;
	}

	public void setZlsy(String zlsy) {
		this.zlsy = zlsy;
	}

	public String getTzsy() {
		return tzsy;
	}

	public void setTzsy(String tzsy) {
		this.tzsy = tzsy;
	}

	public String getZqtzsy() {
		return zqtzsy;
	}

	public void setZqtzsy(String zqtzsy) {
		this.zqtzsy = zqtzsy;
	}

	public String getGqtzsy() {
		return gqtzsy;
	}

	public void setGqtzsy(String gqtzsy) {
		this.gqtzsy = gqtzsy;
	}

	public String getQttzsy() {
		return qttzsy;
	}

	public void setQttzsy(String qttzsy) {
		this.qttzsy = qttzsy;
	}

	public String getGyjzbdsy() {
		return gyjzbdsy;
	}

	public void setGyjzbdsy(String gyjzbdsy) {
		this.gyjzbdsy = gyjzbdsy;
	}

	public String getHdjsy() {
		return hdjsy;
	}

	public void setHdjsy(String hdjsy) {
		this.hdjsy = hdjsy;
	}

	public String getZcczsy() {
		return zcczsy;
	}

	public void setZcczsy(String zcczsy) {
		this.zcczsy = zcczsy;
	}

	public String getQtywsr() {
		return qtywsr;
	}

	public void setQtywsr(String qtywsr) {
		this.qtywsr = qtywsr;
	}

	public String getYyzc() {
		return yyzc;
	}

	public void setYyzc(String yyzc) {
		this.yyzc = yyzc;
	}

	public String getYwjglf() {
		return ywjglf;
	}

	public void setYwjglf(String ywjglf) {
		this.ywjglf = ywjglf;
	}

	public String getZggz() {
		return zggz;
	}

	public void setZggz(String zggz) {
		this.zggz = zggz;
	}

	public String getFlf() {
		return flf;
	}

	public void setFlf(String flf) {
		this.flf = flf;
	}

	public String getZfgjjhzfbt() {
		return zfgjjhzfbt;
	}

	public void setZfgjjhzfbt(String zfgjjhzfbt) {
		this.zfgjjhzfbt = zfgjjhzfbt;
	}

	public String getSjjfj() {
		return sjjfj;
	}

	public void setSjjfj(String sjjfj) {
		this.sjjfj = sjjfj;
	}

	public String getZcjzss() {
		return zcjzss;
	}

	public void setZcjzss(String zcjzss) {
		this.zcjzss = zcjzss;
	}

	public String getQtywzc() {
		return qtywzc;
	}

	public void setQtywzc(String qtywzc) {
		this.qtywzc = qtywzc;
	}

	public String getYylr() {
		return yylr;
	}

	public void setYylr(String yylr) {
		this.yylr = yylr;
	}

	public String getYywsr() {
		return yywsr;
	}

	public void setYywsr(String yywsr) {
		this.yywsr = yywsr;
	}

	public String getYywzc() {
		return yywzc;
	}

	public void setYywzc(String yywzc) {
		this.yywzc = yywzc;
	}

	public String getLrze() {
		return lrze;
	}

	public void setLrze(String lrze) {
		this.lrze = lrze;
	}

	public String getSds() {
		return sds;
	}

	public void setSds(String sds) {
		this.sds = sds;
	}

	public String getJlr() {
		return jlr;
	}

	public void setJlr(String jlr) {
		this.jlr = jlr;
	}

	public String getNdsytz() {
		return ndsytz;
	}

	public void setNdsytz(String ndsytz) {
		this.ndsytz = ndsytz;
	}

	public String getLclr() {
		return lclr;
	}

	public void setLclr(String lclr) {
		this.lclr = lclr;
	}

	public String getWfplr() {
		return wfplr;
	}

	public void setWfplr(String wfplr) {
		this.wfplr = wfplr;
	}

	public String getYnzzs() {
		return ynzzs;
	}

	public void setYnzzs(String ynzzs) {
		this.ynzzs = ynzzs;
	}

	public String getHxyjzbje() {
		return hxyjzbje;
	}

	public void setHxyjzbje(String hxyjzbje) {
		this.hxyjzbje = hxyjzbje;
	}

	public String getYjzbje() {
		return yjzbje;
	}

	public void setYjzbje(String yjzbje) {
		this.yjzbje = yjzbje;
	}

	public String getZbje() {
		return zbje;
	}

	public void setZbje(String zbje) {
		this.zbje = zbje;
	}

	public String getYgzbjfxjqzchj() {
		return ygzbjfxjqzchj;
	}

	public void setYgzbjfxjqzchj(String ygzbjfxjqzchj) {
		this.ygzbjfxjqzchj = ygzbjfxjqzchj;
	}

	public String getCheckstatus() {
		return checkstatus;
	}

	public void setCheckstatus(String checkstatus) {
		this.checkstatus = checkstatus;
	}

	public String getDatastatus() {
		return datastatus;
	}

	public void setDatastatus(String datastatus) {
		this.datastatus = datastatus;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getOperationname() {
		return operationname;
	}

	public void setOperationname(String operationname) {
		this.operationname = operationname;
	}

	public String getOperationtime() {
		return operationtime;
	}

	public void setOperationtime(String operationtime) {
		this.operationtime = operationtime;
	}

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	public String getDepartid() {
		return departid;
	}

	public void setDepartid(String departid) {
		this.departid = departid;
	}

	public String getNopassreason() {
		return nopassreason;
	}

	public void setNopassreason(String nopassreason) {
		this.nopassreason = nopassreason;
	}

}
