package com.geping.etl.UNITLOAN.entity.report;

import javax.persistence.*;

/**
 * 金融机构（机构分支）基础信息正式表 
 * @Author  wuzengwen
 * @Date 2020-07-08 
 */

@Entity
@Table (name ="xjrjgfzH")
public class XjrjgfzH {

	@Id
	private String id;

	/**
	 * 金融机构名称
	 */
	private String finorgname;

	/**
	 * 金融机构代码
	 */
	private String finorgcode;

	/**
	 * 金融机构编码
	 */
	private String finorgnum;

	/**
	 * 内部机构号
	 */
	private String inorgnum;
	
	/**
	 * 许可证号
	 */
	private String xkzh;
	
	/**
	 * 支付行号
	 */
	private String zfhh;

	/**
	 * 机构级别
	 */
	private String orglevel;

	/**
	 * 直属上级管理机构名称
	 */
	private String highlevelorgname;

	/**
	 * 直属上级管理机构金融机构编码
	 */
	private String highlevelfinorgcode;

	/**
	 * 直属上级管理机构内部机构号
	 */
	private String highlevelinorgnum;

	/**
	 * 注册地址
	 */
	private String regarea;

	/**
	 * 注册地行政区划代码->地区代码
	 */
	private String regareacode;

	/**
	 * 办公地址0
	 */
	private String workarea;

	/**
	 * 办公地行政区划代码0
	 */
	private String workareacode;

	/**
	 * 成立时间
	 */
	private String setupdate;

	/**
	 * 营业状态
	 */
	private String mngmestus;

	/**
	 *数据日期
	 */
	private String sjrq;


	/**
	 * 校验状态 0:未校验;1:校验成功;2:校验失败
	 */
	private String checkstatus;

	/**
	 * 数据状态 0:待提交;1:待审核;2:审核不通过;3:审核通过
	 */
	private String datastatus;

	/**
	 * 操作人
	 */
	private String operator;

	/**
	 * 操作名
	 */
	private String operationname;

	/**
	 * 操作时间
	 */
	private String operationtime;

	/**
	 * 组织机构id
	 */
	private String orgid;

	/**
	 * 部门id
	 */
	private String departid;

	/**
	 * 审核不通过原因
	 */
	private String nopassreason;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFinorgname() {
		return finorgname;
	}

	public void setFinorgname(String finorgname) {
		this.finorgname = finorgname;
	}

	public String getFinorgcode() {
		return finorgcode;
	}

	public void setFinorgcode(String finorgcode) {
		this.finorgcode = finorgcode;
	}

	public String getFinorgnum() {
		return finorgnum;
	}

	public void setFinorgnum(String finorgnum) {
		this.finorgnum = finorgnum;
	}

	public String getInorgnum() {
		return inorgnum;
	}

	public void setInorgnum(String inorgnum) {
		this.inorgnum = inorgnum;
	}

	public String getXkzh() {
		return xkzh;
	}

	public void setXkzh(String xkzh) {
		this.xkzh = xkzh;
	}

	public String getZfhh() {
		return zfhh;
	}

	public void setZfhh(String zfhh) {
		this.zfhh = zfhh;
	}

	public String getOrglevel() {
		return orglevel;
	}

	public void setOrglevel(String orglevel) {
		this.orglevel = orglevel;
	}

	public String getHighlevelorgname() {
		return highlevelorgname;
	}

	public void setHighlevelorgname(String highlevelorgname) {
		this.highlevelorgname = highlevelorgname;
	}

	public String getHighlevelfinorgcode() {
		return highlevelfinorgcode;
	}

	public void setHighlevelfinorgcode(String highlevelfinorgcode) {
		this.highlevelfinorgcode = highlevelfinorgcode;
	}

	public String getHighlevelinorgnum() {
		return highlevelinorgnum;
	}

	public void setHighlevelinorgnum(String highlevelinorgnum) {
		this.highlevelinorgnum = highlevelinorgnum;
	}

	public String getRegarea() {
		return regarea;
	}

	public void setRegarea(String regarea) {
		this.regarea = regarea;
	}

	public String getRegareacode() {
		return regareacode;
	}

	public void setRegareacode(String regareacode) {
		this.regareacode = regareacode;
	}

	public String getWorkarea() {
		return workarea;
	}

	public void setWorkarea(String workarea) {
		this.workarea = workarea;
	}

	public String getWorkareacode() {
		return workareacode;
	}

	public void setWorkareacode(String workareacode) {
		this.workareacode = workareacode;
	}

	public String getSetupdate() {
		return setupdate;
	}

	public void setSetupdate(String setupdate) {
		this.setupdate = setupdate;
	}

	public String getMngmestus() {
		return mngmestus;
	}

	public void setMngmestus(String mngmestus) {
		this.mngmestus = mngmestus;
	}

	public String getCheckstatus() {
		return checkstatus;
	}

	public void setCheckstatus(String checkstatus) {
		this.checkstatus = checkstatus;
	}

	public String getDatastatus() {
		return datastatus;
	}

	public void setDatastatus(String datastatus) {
		this.datastatus = datastatus;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getOperationname() {
		return operationname;
	}

	public void setOperationname(String operationname) {
		this.operationname = operationname;
	}

	public String getOperationtime() {
		return operationtime;
	}

	public void setOperationtime(String operationtime) {
		this.operationtime = operationtime;
	}

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	public String getDepartid() {
		return departid;
	}

	public void setDepartid(String departid) {
		this.departid = departid;
	}

	public String getNopassreason() {
		return nopassreason;
	}

	public void setNopassreason(String nopassreason) {
		this.nopassreason = nopassreason;
	}

	@Override
	public String toString() {
		return "XjrjgfzH [id=" + id + ", finorgname=" + finorgname + ", finorgcode=" + finorgcode + ", finorgnum="
				+ finorgnum + ", inorgnum=" + inorgnum + ", xkzh=" + xkzh + ", zfhh=" + zfhh + ", orglevel=" + orglevel
				+ ", highlevelorgname=" + highlevelorgname + ", highlevelfinorgcode=" + highlevelfinorgcode
				+ ", highlevelinorgnum=" + highlevelinorgnum + ", regarea=" + regarea + ", regareacode=" + regareacode
				+ ", workarea=" + workarea + ", workareacode=" + workareacode + ", setupdate=" + setupdate
				+ ", mngmestus=" + mngmestus + ", sjrq="+sjrq+", checkstatus=" + checkstatus + ", datastatus=" + datastatus
				+ ", operator=" + operator + ", operationname=" + operationname + ", operationtime=" + operationtime
				+ ", orgid=" + orgid + ", departid=" + departid + ", nopassreason=" + nopassreason + "]";
	}

}
