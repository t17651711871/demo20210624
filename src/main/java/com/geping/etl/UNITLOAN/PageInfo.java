package com.geping.etl.UNITLOAN;
/***
 * 分页信息
 * @author liang.xu
 * @date 2021.4.27
 */
public interface PageInfo {

    /**
     * 默认页码
     */
    int DEFAULT_PAGE=1;

    /**
     * 默认行
     */
    int DEFAULT_ROWS=20;
}