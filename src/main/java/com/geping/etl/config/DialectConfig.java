package com.geping.etl.config;

import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.type.StandardBasicTypes;

import java.sql.Types;

/**
 * @Author: David
 * @Date: Created in 10:23 2019/6/4
 * @Description: oracle 方言配置
 * @Modified By:
 * @Version:
 */
public class DialectConfig extends Oracle10gDialect {

    public DialectConfig() {
        registerColumnType(520, "numeric(11)");

        registerHibernateType(1, "string");
        registerHibernateType(-9, "string");
        registerHibernateType(-16, "string");
        registerHibernateType(3, "double");
        registerHibernateType(520, "int");

        registerHibernateType(Types.CHAR, StandardBasicTypes.STRING.getName());
        registerHibernateType(Types.NVARCHAR, StandardBasicTypes.STRING.getName());
        registerHibernateType(Types.LONGNVARCHAR, StandardBasicTypes.STRING.getName());
        registerHibernateType(Types.DECIMAL, StandardBasicTypes.DOUBLE.getName());
        registerHibernateType(Types.NUMERIC, StandardBasicTypes.INTEGER.getName());
    }
}
