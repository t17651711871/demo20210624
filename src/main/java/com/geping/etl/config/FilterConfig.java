package com.geping.etl.config;

import com.geping.etl.common.safe.SafeFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *  Filter 配置类
 * @author  liang.xu
 * @date 2021年5月10日
 */
@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean registerXssFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new SafeFilter());
        registration.addUrlPatterns("/*");
        registration.setName("safeFilter");
        registration.setOrder(1);  //Filter 顺序,值越小,Filter越靠前。
        return registration;
    }
}