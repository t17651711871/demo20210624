package com.geping.etl.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/***
 *  SQL 配置文件导入
 * @author liang.xu
 * @date 2021.4.2
 */
@Configuration
@ImportResource(locations= {"classpath:sql/*-sql.xml"})
public class SqlConfig {

}