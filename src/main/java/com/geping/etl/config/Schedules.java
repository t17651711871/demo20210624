package com.geping.etl.config;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.geping.etl.common.entity.Sys_User;
import com.geping.etl.common.service.Sys_UserService;


@Configuration
@EnableScheduling
public class Schedules {
	@Autowired
	private Sys_UserService s;

	@Scheduled(cron = "0/59 59 23 * * *")
	public void timer(){
		try {
			List<Sys_User> list = s.findAllSysUser();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			for(Sys_User user : list) {
				int days = (int)((new Date().getTime() - sdf.parse(user.getLastLoginDate()).getTime()) / (1000*60*60*24));
				if(days >= 30) {
					s.agreenLock(user.getId(),"Y");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}  
}
