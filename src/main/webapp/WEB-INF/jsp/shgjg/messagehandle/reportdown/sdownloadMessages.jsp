<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>下载报文</title>
<%@include file="../../../common/head.jsp"%>
</head>
<body>
	<!--数据表格-->
	<table id="dg" style="height: 600px;"></table>
	
	<!--表格工具栏-->
	<div id="tb" style="padding: 5px; height: auto;">
	          <!-- 报文名称：<input class="easyui-textbox" style="width: 173px; height: 23px" id="reportname" />&nbsp;&nbsp;
		生成日期：<input class="easyui-datebox" style="width: 173px; height: 23px" id="createdate" />&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="selectRecordByDate()">查询</a>&nbsp;&nbsp; -->
		<a class="easyui-linkbutton" iconCls="icon-download" onclick="downLoad()">下载</a>
	</div>
	<script type="text/javascript">
        //页面加载事件
        $(function() {
            $("#dg").datagrid({
                method:'post',
                url:'SDownMessageList',
                loadMsg:'数据加载中,请稍后...',
                checkOnSelect:true,
                autoRowHeight:false,
                pagination:true,
                rownumbers:true,
                toolbar:'#tb',
                fitColumns:true,
                pageSize:20,
                pageList:[15,20,30,50],
                columns:[[
                    {field:'id', checkbox: true, width:150,align:'center'},
                    {field:'reportype',title:'报文类型',width:150,align:'center'},
                    {field:'reportname',title:'报文名称',width:150,align:'center'},
                    {field:'createdate',title:'生成时间',width:150,align:'center'},
                    {field:'createname',title:'生成人',width:150,align:'center'},
                    {field:'standby1',title:'备用字段1',width:150,align:'center',hidden:true},
                    {field:'standby2',title:'备用字段2',width:150,align:'center',hidden:true},
                    {field:'filepath',width:150,hidden:true}
                ]]
            });
        });

        function downLoad(){
            var rows = $('#dg').datagrid('getSelections');
        	var messageNames = '';
        	if(rows.length > 0){
        		for(var i = 0;i<rows.length;i++){
        			if(i == rows.length -1){
        				messageNames = messageNames + rows[i].reportname;
    				}else{
    					messageNames = messageNames + rows[i].reportname + ',';
    				}
        		}
        	}
            
			if (rows.length == 0) {
                $.messager.confirm('提示','您确认要下载全部报文吗?',function(r){
                    if (r){
                    	window.location.href = "sdownmessage";
                    }
                });
			} else {
                $.messager.confirm('提示','您确认要下载选中的报文吗?',function(r){
                    if (r){
                    	window.location.href = "sdownmessage?messageNames="+encodeURI(messageNames);
                    }
                });
			}
        }
	</script>
</body>
</html>
