<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet"%>
<%@ page import="com.geping.etl.common.util.VariableUtils"%>
<%@ page import="com.geping.etl.common.entity.Sys_Auth_Role_Resource"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	
	String status = (String)request.getAttribute("status");
	String isusedelete = (String)request.getAttribute("isusedelete");
	String isuseupdate = (String)request.getAttribute("isuseupdate");
	String bankcodefaren = (String)request.getAttribute("bankcodefaren");
	String bankcodelei = (String)request.getAttribute("bankcodelei");
	String bankcodewangdian = (String)request.getAttribute("bankcodewangdian");
	
	VariableUtils vu = (VariableUtils)request.getSession().getAttribute("vu");
	Set<Sys_Auth_Role_Resource> operateSet = vu.getOperateReportSet();
	String loginid = vu.getLoginId();//当前用户
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>个人银行结算账户信息表</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
	<table id="dg1" style="height: 480px;" title="个人银行结算账户信息"></table>
	
	<!--表格工具栏-->
	<div id="tb1" style="padding: 5px; height: auto;">
	    <a class="easyui-linkbutton" href="SDataInfoManageUi" iconCls="icon-return">返回</a>&nbsp;&nbsp;
	   
	       账户名称:<input type="text" id="accname" style="height: 23px; width:190px"/>&nbsp;&nbsp;
	       账户账号:<input type="text" id="accno" style="height: 23px; width:190px"/>&nbsp;&nbsp;
	       
	    <a class="easyui-linkbutton" iconCls="icon-search" onclick="button1()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	       
	    <%
        if(status.equals("dtj")){
           for(Sys_Auth_Role_Resource sarr : operateSet){
			        if(sarr.getResValue().equals("CHECKOUT")){
	%> 		   
			               <a class="easyui-linkbutton" iconCls="icon-accept" onclick="button2()">校验</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%		   
				   }else if(sarr.getResValue().equals("IMPORT")){
	%>
		                       <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="importExcel()">导入</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%		   
				   }else if(sarr.getResValue().equals("EXPORT")){
	%>
		                       <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="exportExcel()">导出</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%			     
				   }else if(sarr.getResValue().equals("ADD")){
	%>
		                       <a class="easyui-linkbutton" iconCls="icon-add" onclick="add()">手工补录</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%			   
				   }else if(sarr.getResValue().equals("EDIT")){
	%>
		                       <a class="easyui-linkbutton" iconCls="icon-edit" onclick="KIupdate()">补足</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%			    
				   }else if(sarr.getResValue().equals("DELETE")){
	%> 
	                           <a class="easyui-linkbutton" iconCls="icon-remove" onclick="KIdelete()">删除</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%			   
				   }
			   }
	%>
           
<%
              }else if(status.equals("dsh")){
                  for(Sys_Auth_Role_Resource sarr : operateSet){
					  if(sarr.getResValue().equals("EDIT")){
						    if(isuseupdate.equals("yes")){
		%>
			                       <a class="easyui-linkbutton" iconCls="icon-edit" onclick="applyUpdate()">申请修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<%			   
						    }else{
		%>				    	
						    	   <a class="easyui-linkbutton" iconCls="icon-edit" onclick="KIupdate()">补足</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<%				    	
						    }
					   }else if(sarr.getResValue().equals("DELETE")){
						   if(isusedelete.equals("yes")){
		%>
			                       <a class="easyui-linkbutton" iconCls="icon-remove" onclick="KIdelete()">申请删除</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<%			    
						   }else{
		%>				    	
					    	      <a class="easyui-linkbutton" iconCls="icon-remove" onclick="KIdelete()">删除</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    <%						   
							   
						   }
					   }else if(sarr.getResValue().equals("CHECK")){
		%>
		                   <a class="easyui-linkbutton" iconCls="icon-application-get" onclick="pass()">审核通过</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			               <a class="easyui-linkbutton" iconCls="icon-cross" onclick="notpass()">审核不通过</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			               <a class="easyui-linkbutton" iconCls="icon-ok" onclick="agreeapply()">同意申请</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			               <a class="easyui-linkbutton" iconCls="icon-cross" onclick="refuseapply()">拒绝申请</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%			   
			        }
		       }
          }
%>   
	</div>


    <!-- 导入文件dialog -->
    <div style="visibility: hidden;">
        <div id="importFileDialog" class="easyui-dialog" style="width: 600px; height: 160px; padding-left: 30px" title="导入数据">
            <h2>请选择要导入的Excel文件</h2>
            <table id="importTable" border="0">
                <tr>
                    <td><input type="file" name="file_info" id="file_info" size="60" onchange="fileSelected()" />&nbsp;</td>
                </tr>
                <tr>
                    <td><input type="button" id="importId" value="提交" size="60" onclick="fileUp()" /></td>
                </tr>
            </table>
        </div>
    </div>
    
    
    <!-- 新增 -->
	<div style="visibility: hidden;">
		<div id="addDialog" class="easyui-dialog" data-options="iconCls:'icon-save',toolbar:'#tbForAddDialog'" style="width: 600px; height: 460px; align-items: center;">
			<!--   新增对话框的工具栏  -->
			<div id="tbForAddDialog" style="padding-left: 30px; padding-top: 10px">
				<a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveForAddDialog()">保存</a>&nbsp;&nbsp; 
				<a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="resetForAdd()">重置</a>
			</div>

			<!-- 信息录入 -->
			<form id="formForAddUser" method="post">
				<div style="padding-top: 20px">
					<table style="padding-left: 30px">
					    <input type="hidden" name="id" id="id"/>
					    <input type="hidden" name="checkstatus" id="checkstatus"/>
					    <input type="hidden" name="datastatus" id="datastatus"/>
					    <input type="hidden" name="operationname" id="operationname"/>
					    <input type="hidden" name="shbtgyy" id="shbtgyy"/>
					    <input type="hidden" name="czr" id="czr"/>
					    <input type="hidden" name="czsj" id="czsj"/>
					    <input type="hidden" name="shr" id="shr"/>
					    <input type="hidden" name="shsj" id="shsj"/>
						<tr>
							<td align="right">开户银行金融机构代码(法人):</td>
							<td><input class="easyui-validatebox" name="org_code" id="org_code" style="height: 21px; width: 173px" data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">开户机构法人机构识别编码(LEI):</td>
							<td><input class="easyui-validatebox" name="org_lei" id="org_lei" style="height: 21px; width: 173px" data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">开户银行金融机构代码(网点):</td>
							<td><input class="easyui-validatebox" name="open_code" id="open_code" style="height: 21px; width: 173px" data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">客户编号:</td>
							<td><input class="easyui-validatebox" name="cst_code" id="cst_code" style="height: 21px; width: 173px" data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">账户名称:</td>
							<td><input class="easyui-validatebox" name="acc_name" id="acc_name" style="height: 21px; width: 173px" data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">账户账号:</td>
							<td><input class="easyui-validatebox" name="acc_no" id="acc_no" style="height: 21px; width: 173px" data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">账户类型:</td>
							<td>
							   <select class="easyui-combobox" name="acc_type" id="acc_type" style="height: 21px; width: 173px">
							        <option value="10" selected="selected">10-I类账户</option>
							        <option value="11">11-II类账户</option>
							        <option value="12">12-III类账户</option>
							        <option value="13">13-贷记账户</option>
							        <option value="19">19-其他类型账户</option>
							   </select>
							</td>
						</tr>

						<tr>
							<td align="right">开户日期:</td>
							<td><input class="easyui-datebox" name="open_date" id="open_date" style="height: 21px; width: 173px" editable=false data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">开户时间:</td>
							<td><input type="text" name="open_time" id="open_time" style="height: 21px; width: 173px" data-options="required:true"/></td>
						</tr>

						<tr>
							<td align="right">销户日期:</td>
							<td><input class="easyui-datebox" name="close_date" id="close_date" style="height: 21px; width: 173px" value="9999-12-31" editable=false/></td>
						</tr>
						
						<tr>
							<td align="right">销户时间:</td>
							<td><input type="text" name="close_time" id="close_time" style="height: 21px; width: 173px" value="999900"/></td>
						</tr>
						
						<tr>
							<td align="right">账户状态:</td>
							<td>
							   <select class="easyui-combobox" name="acc_state" id="acc_state" style="height: 21px; width: 173px">
							        <option value="10" selected="selected">10-正常</option>
							        <option value="11">11-未激活</option>
							        <option value="12">12-只收不付</option>
							        <option value="13">13-不收不付</option>
							        <option value="14">14-已注销</option>
							        <option value="15">15-已冻结</option>
							        <option value="19">19-其他</option>
							   </select>
						    </td>
						</tr>
						
						<tr>
							<td align="right">证件种类:</td>
							<td>
							<select class="easyui-combobox" name="id_type" id="id_type" style="height: 21px; width: 173px">
							        <option value="10" selected="selected">10-居民身份证</option>
							        <option value="11">11-临时身份证</option>
							        <option value="12">12-户口簿</option>
							        <option value="13">13-军人或武警身份证</option>
							        <option value="14">14-港澳居民来往内地通行证,台湾居民来往大陆通行证</option>
							        <option value="15">15-外国公民护照</option>
							        <option value="16">16-外国人永久居留证</option>
							        <option value="17">17-港澳台居民居住证</option>
							        <option value="18">18-中国护照</option>
							        <option value="19">19-边民出入境通行证</option>
							        <option value="20">20-其他类个人身份有效证件</option>
							   </select>
							</td>
						</tr>
						
						<tr>
							<td align="right">身份证件号码:</td>
							<td><input type="text" name="id_no" id="id_no" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">身份证件有效期开始日:</td>
							<td><input class="easyui-datebox" name="id_startline" id="id_startline" style="height: 21px; width: 173px" data-options="required:true" editable=false/></td>
						</tr>
						
						<tr>
							<td align="right">身份证件有效期截止日:</td>
							<td><input class="easyui-datebox" name="id_deadline" id="id_deadline" style="height: 21px; width: 173px" value="9999-12-31" editable=false/></td>
						</tr>
						
						<tr>
							<td align="right">身份证件发证机关所在地的地区代码:</td>
							<td><input type="text" name="id_region" id="id_region" style="height: 21px; width: 173px" /></td>
						</tr>
						
						
						<tr>
							<td align="right">客户所属国家(地区):</td>
							<td><input type="text" name="nation" id="nation" style="height: 21px; width: 173px" /></td>
						</tr>
						
						
						<tr>
							<td align="right">客户性别:</td>
							<td>
							<select class="easyui-combobox" name="cst_sex" id="cst_sex" style="height: 21px; width: 173px">
							        <option value="10" selected="selected">10-男</option>
							        <option value="11">11-女</option>
							   </select></td>
						</tr>
						
						<tr>
							<td align="right">联系地址:</td>
							<td><input type="text" name="address" id="address" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">身份核验方式:</td>
							<td>
							<select class="easyui-combobox" name="id_ver" id="id_ver" style="height: 21px; width: 173px">
							        <option value="10" selected="selected">10-面对面</option>
							        <option value="11">11-非面对面</option>
							   </select></td>
						</tr>
						
						<tr>
							<td align="right">绑定的手机号码:</td>
							<td><input type="text" name="bind_mob" id="bind_mob" style="height: 21px; width: 173px" /></td>
						</tr>
						
						
						<tr>
							<td align="right">绑定的银行账户账号:</td>
							<td><input type="text" name="bind_acc" id="bind_acc" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">绑定银行账号日期:</td>
							<td><input class="easyui-datebox" name="bind_date" id="bind_date" style="height: 21px; width: 173px" editable=false/></td>
						</tr>
						
						<tr>
							<td align="right">绑定银行账号时间:</td>
							<td><input type="text" name="bind_time" id="bind_time" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">被绑定的银行账户开户行(法人)代码:</td>
							<td><input type="text" name="bind_bank" id="bind_bank" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">代理人姓名:</td>
							<td><input type="text" name="id_name2" id="id_name2" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">代理人证件种类:</td>
							<td>
							<select class="easyui-combobox" name="id_type2" id="id_type2" style="height: 21px; width: 173px">
							        <option value="10" selected="selected">10-居民身份证</option>
							        <option value="11">11-临时身份证</option>
							        <option value="12">12-户口簿</option>
							        <option value="13">13-军人或武警身份证</option>
							        <option value="14">14-港澳居民来往内地通行证,台湾居民来往大陆通行证</option>
							        <option value="15">15-外国公民护照</option>
							        <option value="16">16-外国人永久居留证</option>
							        <option value="17">17-港澳台居民居住证</option>
							        <option value="18">18-中国护照</option>
							        <option value="19">19-边民出入境通行证</option>
							        <option value="20">20-其他类个人身份有效证件</option>
							   </select>
							</td>
						</tr>
						
						<tr>
							<td align="right">代理人证件号码:</td>
							<td><input type="text" name="id_no2" id="id_no2" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">代理人证件有效期开始日:</td>
							<td><input class="easyui-datebox" name="id_startline2" id="id_startline2" style="height: 21px; width: 173px" editable=false/></td>
						</tr>
						
						<tr>
							<td align="right">代理人证件有效期截止日:</td>
							<td><input class="easyui-datebox" name="id_deadline2" id="id_deadline2" style="height: 21px; width: 173px" value="9999-12-31" editable=false/></td>
						</tr>
						
						<tr>
							<td align="right">代理人联系电话:</td>
							<td><input type="text" name="contact2" id="contact2" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">开户地地区代码:</td>
							<td><input type="text" name="acc_region" id="acc_region" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">介质号:</td>
							<td><input type="text" name="media_id" id="media_id" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">介质类型:</td>
							<td><input type="text" name="media_type" id="media_type" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">介质状态:</td>
							<td><input type="text" name="media_state" id="media_state" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">用户开立银行账户时的IP地址:</td>
							<td><input type="text" name="ip_addr" id="ip_addr" style="height: 21px; width: 173px" /></td>
						</tr>
						
						<tr>
							<td align="right">用户开立银行账户时的MAC地址:</td>
							<td><input type="text" name="mac_addr" id="mac_addr" style="height: 21px; width: 173px" /></td>
						</tr>
					</table>
				</div>
			</form>
		</div>
	</div>

<script type="text/javascript">
$(function(){
	$("#importFileDialog").dialog("close");
	$("#addDialog").dialog("close");
	
	$("#dg1").datagrid({
		method:'POST',
		url:'SGRYHJSZHXX_TData?status='+'<%=status%>',
		loadMsg:'数据加载中,请稍后...',
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			{field:'ck',checkbox:true},
			{field:'id',title:'id',width:150,align:'center',hidden:true},
			{field:'checkstatus',title:'校验结果',width:150,align:'center',formatter:function(value,row,index){
				if(value == '0'){
					return '校验通过';
				}else if(value == '1'){
					return '未校验';
				}else if(value == '2'){
					return '<span style="color:red;">校验不通过</span>';
				}
			}},
			{field:'datastatus',title:'数据状态',width:150,align:'center',hidden:true},
			{field:'operationname',title:'操作名',width:150,align:'center'},
			{field:'shbtgyy',title:'审核不通过原因',width:150,align:'center'},
			{field:'org_code',title:'开户银行金融机构代码(法人)',width:200,align:'center'},
			{field:'org_lei',title:'开户机构法人机构识别编码(LEI)',width:200,align:'center'},
			{field:'open_code',title:'开户银行金融机构代码(网点)',width:200,align:'center'},
			{field:'cst_code',title:'客户编号',width:150,align:'center'},
			{field:'acc_name',title:'账户名称',width:150,align:'center'},
			{field:'acc_no',title:'账户账号',width:150,align:'center'},
			{field:'acc_type',title:'账户类型',width:150,align:'center',formatter:function(value,row,index){
				if(value == '10'){
					return '10-I类账户';
				}else if(value == '11'){
					return '11-II类账户';
				}else if(value == '12'){
					return '12-III类账户';
				}else if(value == '13'){
					return '13-贷记账户';
				}else if(value == '19'){
					return '19-其他类型账户';
				}
			}},
			{field:'open_date',title:'开户日期',width:150,align:'center'},
			{field:'open_time',title:'开户时间',width:150,align:'center'},
			{field:'close_date',title:'销户日期',width:150,align:'center'},
			{field:'close_time',title:'销户时间',width:150,align:'center'},
			{field:'acc_state',title:'账户状态',width:150,align:'center',formatter:function(value,row,index){
				if(value == '10'){
					return '10-正常';
				}else if(value == '11'){
					return '11-未激活';
				}else if(value == '12'){
					return '12-只收不付';
				}else if(value == '13'){
					return '13-不收不付';
				}else if(value == '14'){
					return '14-已注销';
				}else if(value == '15'){
					return '15-已冻结';
				}else if(value == '19'){
					return '19-其他';
				}
			}},
			{field:'id_type',title:'证件种类',width:150,align:'center',formatter:function(value,row,index){
				if(value == '10'){
					return '10-居民身份证';
				}else if(value == '11'){
					return '11-临时身份证';
				}else if(value == '12'){
					return '12-户口簿';
				}else if(value == '13'){
					return '13-军人或武警身份证';
				}else if(value == '14'){
					return '14-港澳居民来往内地通行证,台湾居民来往大陆通行证';
				}else if(value == '15'){
					return '15-外国公民护照';
				}else if(value == '16'){
					return '16-外国人永久居留证';
				}else if(value == '17'){
					return '17-港澳台居民居住证';
				}else if(value == '18'){
					return '18-中国护照';
				}else if(value == '19'){
					return '19-边民出入境通行证';
				}else if(value == '20'){
					return '20-其他类个人身份有效证件';
				}
			}},
			{field:'id_no',title:'身份证件号码',width:150,align:'center'},
			{field:'id_startline',title:'身份证件有效期开始日',width:150,align:'center'},
			{field:'id_deadline',title:'身份证件有效期截止日',width:150,align:'center'},
			{field:'id_region',title:'身份证件发证机关所在地的地区代码',width:150,align:'center'},
			{field:'nation',title:'客户所属国家(地区)',width:150,align:'center'},
			{field:'cst_sex',title:'客户性别',width:150,align:'center',formatter:function(value,row,index){
				if(value == '10'){
					return '10-男';
				}else if(value == '11'){
					return '11-女';
				}
			}},
			{field:'address',title:'联系地址',width:150,align:'center'},
			{field:'id_ver',title:'身份核验方式',width:150,align:'center',formatter:function(value,row,index){
				if(value == '10'){
					return '10-面对面';
				}else if(value == '11'){
					return '11-非面对面';
				}
			}},
			{field:'bind_mob',title:'绑定的手机号码',width:150,align:'center'},
			{field:'bind_acc',title:'绑定的银行账户账号',width:150,align:'center'},
			{field:'bind_date',title:'绑定银行账号日期',width:150,align:'center'},
			{field:'bind_time',title:'绑定银行账号时间',width:150,align:'center'},
			{field:'bind_bank',title:'被绑定的银行账户开户行(法人)代码',width:150,align:'center'},
			{field:'id_name2',title:'代理人姓名',width:150,align:'center'},
			{field:'id_type2',title:'代理人证件种类',width:150,align:'center',formatter:function(value,row,index){
				if(value == '10'){
					return '10-居民身份证';
				}else if(value == '11'){
					return '11-临时身份证';
				}else if(value == '12'){
					return '12-户口簿';
				}else if(value == '13'){
					return '13-军人或武警身份证';
				}else if(value == '14'){
					return '14-港澳居民来往内地通行证,台湾居民来往大陆通行证';
				}else if(value == '15'){
					return '15-外国公民护照';
				}else if(value == '16'){
					return '16-外国人永久居留证';
				}else if(value == '17'){
					return '17-港澳台居民居住证';
				}else if(value == '18'){
					return '18-中国护照';
				}else if(value == '19'){
					return '19-边民出入境通行证';
				}else if(value == '20'){
					return '20-其他类个人身份有效证件';
				}
			}},
			{field:'id_no2',title:'代理人证件号码',width:150,align:'center'},
			{field:'id_startline2',title:'代理人证件有效期开始日',width:150,align:'center'},
			{field:'id_deadline2',title:'代理人证件有效期截止日',width:150,align:'center'},
			{field:'contact2',title:'代理人联系电话',width:150,align:'center'},
			{field:'acc_region',title:'开户地地区代码',width:150,align:'center'},
			{field:'media_id',title:'介质号',width:150,align:'center'},
			{field:'media_type',title:'介质类型',width:150,align:'center'},
			{field:'media_state',title:'介质状态',width:150,align:'center'},
			{field:'ip_addr',title:'用户开立银行账户时的IP地址',width:150,align:'center'},
			{field:'mac_addr',title:'用户开立银行账户时的MAC地址',width:150,align:'center'},
			{field:'czr',title:'操作人',width:150,align:'center'},
			{field:'czsj',title:'操作时间',width:150,align:'center'},
			{field:'shr',title:'审核人',width:150,align:'center'},
			{field:'shsj',title:'审核时间',width:150,align:'center'}
	    ]]
	});
})

//动态查询
function button1(){
	var accname = $('#accname').val();
	var accno = $('#accno').val();
	$("#dg1").datagrid("reload", {
		accname: accname,
		accno: accno
	});
}

//============================待提交页面==============================================
function importExcel(){
	 var file = document.getElementById("file_info").files[0];
     if (file == null) {
         document.getElementById("importId").disabled = "disabled";
     } else {
         var fileName = file.name;
         var fileType = fileName.substring(fileName.lastIndexOf('.'),
             fileName.length);
         if (fileType == '.xls' || fileType == '.xlsx') {
             if (file) {
                 document.getElementById("importId").disabled = "";
             }
         } else {
             //$.messager.alert('警告', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！","error");
             document.getElementById("importId").disabled = "disabled";
         }
     }
     $('#importFileDialog').dialog('open');
     $('#importFileDialog').dialog({modal: true});
}


function fileSelected() {
	var file = document.getElementById("file_info").files[0];
	var fileName = file.name;
	//判断导入文件格式是否正确
	var fileType = fileName.substring(fileName.lastIndexOf('.'), fileName.length);
	if(fileType == '.xlsx') {
		if(file) {
			//启用导入按钮
			document.getElementById("importId").disabled = "";
		}
	} else {
		$.messager.alert('警告', "导入文件应该是.xlsx为后缀而不是" + fileType + "请重新选择文件！", "error");
		document.getElementById("importId").disabled = "disabled";
	}
}


function fileUp(){
	$.messager.confirm('操作提示', '确认要导入Excel表格吗？', function(r) {
		if(r) {
			$.messager.progress({
				title: '请稍等',
				msg: '数据正在导入中......',
			});
			
			$.ajaxFileUpload({
				url: 'Simportgr',
				fileElementId: 'file_info',
				type: "POST",
				secureuri: false,
				dataType: 'json',
				success: function(data, status) {
					$.messager.progress('close');
					if(data) {
						if(data.msg == '导入文件成功！') {
							$.messager.alert('', data.msg, 'info', function(r) {
								$('#dg1').datagrid('reload');
								$('#importFileDialog').dialog('close');
							});
						} else {
							$.messager.alert('', "<div style='overflow-y:scroll;height:275px;width:375px;mapping-top:20px;'>" + data.msg.replace(/,/g, '<br>') + "</div>", 'info');
						}
					}
				},
				error: function(data, status, e) {
					if(e.length > 200) {
						$.messager.show({
							title: '提示信息',
							msg: e,
							timeout: 0,
							showType: 'show',
							width: 800,
							height: 500,
							style: {
								right: '100',
								top: document.body.scrollTop + document.documentElement.scrollTop
							}
						});
					} else {
						$.messager.alert('提示', e);
					}
				}
			})
		}
	});
}


function exportExcel(){
	var rows = $('#dg1').datagrid('getSelections');
	var ids = '';
	if(rows.length > 0){
		for(var i = 0;i<rows.length;i++){
			if(i == rows.length -1){
				ids = ids + rows[i].id;
			}else{
				ids = ids + rows[i].id + ',';
			}
		}
	}
	
	$.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
		if(r){
			window.location.href = "Sexportgr?ids="+ids;
		}
	})
}

function button2(){
	var rows = $('#dg1').datagrid('getSelections');
	var ids = '';
	if(rows.length > 0){
		for(var i = 0;i<rows.length;i++){
			if(i == rows.length -1){
				ids = ids + rows[i].id;
			}else{
				ids = ids + rows[i].id + ',';
			}
		}
	}
	
	$.messager.confirm('操作提示','确认要校验吗？',function(r){
		if(r){
			$.messager.progress({
				title: '请稍等',
				msg: '数据正在校验中......',
			});
			$.ajax({
				url : 'scheckoutgryhjszhxx',
				type : 'POST', //GET
				async : true, //或false,是否异步
				contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
				data:{"ids":ids},
				dataType : 'text', //返回的数据格式：json/xml/html/script/jsonp/text
				success : function(data) {
					$.messager.progress('close');
					if (data == '0') {
						$.messager.alert('','校验成功','info',function(r) {
							$('#dg1').datagrid('reload');
							$("#addDialog").dialog("close");
					    });
					} else if(data == '2'){
						$.messager.alert('操作提示','请在上报机构设置中设置报文存放地址','error');
					}else{
						$('#dg1').datagrid('reload');
						$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
						window.location.href = "sdownloadinfo?name="+data;
					}
				}
			});
		}
	})
}

function add(){
	$('#id').val('');
	$('#org_code').val('<%=bankcodefaren%>');
	$('#org_lei').val('<%=bankcodelei%>');
	$('#open_code').val('<%=bankcodewangdian%>');
	$('#cst_code').val('');
	$('#acc_name').val('');
	$('#acc_no').val('');
	$('#acc_type').combobox('setValue','10');
	$('#open_date').datebox('setValue','');
	$('#open_time').val('');
	$('#close_date').datebox('setValue','9999-12-31');
	$('#close_time').val('999900');
	$('#acc_state').combobox('setValue','10');
	$('#id_type').combobox('setValue','10');
	$('#id_no').val('');
	$('#id_startline').datebox('setValue','');
	$('#id_deadline').datebox('setValue','9999-12-31');
	$('#id_region').val('');
	$('#nation').val('');
	$('#cst_sex').combobox('setValue','10');
	$('#address').val('');
	$('#id_ver').combobox('setValue','10');
	$('#bind_mob').val('');
	$('#bind_acc').val('');
	$('#bind_date').datebox('setValue','');
	$('#bind_time').val('');
	$('#bind_bank').val('');
	$('#id_name2').val('');
	$('#id_type2').combobox('setValue','10');
	$('#id_no2').val('');
	$('#id_startline2').datebox('setValue','');
	$('#id_deadline2').datebox('setValue','9999-12-31');
	$('#contact2').val('');
	$('#acc_region').val('');
	$('#media_id').val('');
	$('#media_type').val('');
	$('#media_state').val('');
	$('#ip_addr').val('');
	$('#mac_addr').val('');
	
	$('#checkstatus').val('');
	$('#datastatus').val('');
	$('#operationname').val('');
	$('#shbtgyy').val('');
	$('#czr').val('');
	$('#czsj').val('');
	$('#shr').val('');
	$('#shsj').val('');
	
	$("#addDialog").dialog({title:"新增"});
	$("#addDialog").dialog("open");
	$("#formForAddUser").form('validate');
	$("#addDialog").dialog({modal:true});
}

function saveForAddDialog(){
	$.ajax({
		url : 'saddgryhjszhxx',
		type : 'POST', //GET
		async : true, //或false,是否异步
		contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
		data : $('#formForAddUser').serialize(),
		dataType : 'text', //返回的数据格式：json/xml/html/script/jsonp/text
		success : function(data) {
			if (data == '0') {
				$.messager.alert('','保存成功','info',function(r) {
					$('#dg1').datagrid('reload');
					$("#addDialog").dialog("close");
			    });
			} else {
				$.messager.alert('操作提示','保存失败','error');
			}
		}
	});
}

function KIupdate(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 1){
		$.messager.alert('操作提示','请选择一条数据补足','info');
	}else{
		$('#id').val(rows[0].id);
		$('#org_code').val(rows[0].org_code);
		$('#org_lei').val(rows[0].org_lei);
		$('#open_code').val(rows[0].open_code);
		$('#cst_code').val(rows[0].cst_code);
		$('#acc_name').val(rows[0].acc_name);
		$('#acc_no').val(rows[0].acc_no);
		$('#acc_type').combobox('setValue',rows[0].acc_type);
		$('#open_date').datebox('setValue',rows[0].open_date);
		$('#open_time').val(rows[0].open_time);
		$('#close_date').datebox('setValue',rows[0].close_date);
		$('#close_time').val(rows[0].close_time);
		$('#acc_state').combobox('setValue',rows[0].acc_state);
		$('#id_type').combobox('setValue',rows[0].id_type);
		$('#id_no').val(rows[0].id_no);
		$('#id_startline').datebox('setValue',rows[0].id_startline);
		$('#id_deadline').datebox('setValue',rows[0].id_deadline);
		$('#id_region').val(rows[0].id_region);
		$('#nation').val(rows[0].nation);
		$('#cst_sex').combobox('setValue',rows[0].cst_sex);
		$('#address').val(rows[0].address);
		$('#id_ver').combobox('setValue',rows[0].id_ver);
		$('#bind_mob').val(rows[0].bind_mob);
		$('#bind_acc').val(rows[0].bind_acc);
		$('#bind_date').datebox('setValue',rows[0].bind_date);
		$('#bind_time').val(rows[0].bind_time);
		$('#bind_bank').val(rows[0].bind_bank);
		$('#id_name2').val(rows[0].id_name2);
		$('#id_type2').combobox('setValue',rows[0].id_type2);
		$('#id_no2').val(rows[0].id_no2);
		$('#id_startline2').datebox('setValue',rows[0].id_startline2);
		$('#id_deadline2').datebox('setValue',rows[0].id_deadline2);
		$('#contact2').val(rows[0].contact2);
		$('#acc_region').val(rows[0].acc_region);
		$('#media_id').val(rows[0].media_id);
		$('#media_type').val(rows[0].media_type);
		$('#media_state').val(rows[0].media_state);
		$('#ip_addr').val(rows[0].ip_addr);
		$('#mac_addr').val(rows[0].mac_addr);
		
		$('#checkstatus').val(rows[0].checkstatus);
		$('#datastatus').val(rows[0].datastatus);
		$('#operationname').val(rows[0].operationname);
		$('#shbtgyy').val(rows[0].shbtgyy);
		$('#czr').val(rows[0].czr);
		$('#czsj').val(rows[0].czsj);
		$('#shr').val(rows[0].shr);
		$('#shsj').val(rows[0].shsj);
		
		$("#addDialog").dialog({title:"补足"});
		$("#addDialog").dialog("open");
		$("#addDialog").dialog({modal:true});
	}
}

function KIdelete(){
	//是否启用了审核流程
	var isusedelete = '<%=isusedelete%>';
	
	var status = '<%=status%>';
	
	var rows = $('#dg1').datagrid('getSelections');
	var ids = '';
	if(rows.length > 0){
		for(var i = 0;i<rows.length;i++){
			if(i == rows.length -1){
				ids = ids + rows[i].id;
			}else{
				ids = ids + rows[i].id + ',';
			}
		}
	}
	
	var message = '';
	if(isusedelete == 'yes'){
		message = '申请删除';
	}else{
		message = '删除';
	}
	
	$.messager.confirm('操作提示','确认要'+message+'吗？',function(r){
		if(r){
			$.ajax({
				url : 'sdeletegryhjszhxx',
				type : 'POST', //GET
				async : true, //或false,是否异步
				contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
				data : {"status":status,"isusedelete":isusedelete,"ids":ids},
				dataType : 'text', //返回的数据格式：json/xml/html/script/jsonp/text
				success : function(data) {
					if (data == '0') {
						$.messager.alert('',message+'成功','info',function(r) {
							$('#dg1').datagrid('reload');
							$("#addDialog").dialog("close");
					    });
					} else {
						$.messager.alert('操作提示',message+'失败','error');
					}
				}
			});
		}
	})
}


//============================待审核页面==============================================
function applyUpdate(){
	var rows = $('#dg1').datagrid('getSelections');
	var ids = '';
	if(rows.length > 0){
		for(var i = 0;i<rows.length;i++){
			if(i == rows.length -1){
				ids = ids + rows[i].id;
			}else{
				ids = ids + rows[i].id + ',';
			}
		}
	}
	
	$.messager.confirm('操作提示','确认要申请修改吗？',function(r){
		if(r){
			$.ajax({
				url : 'sapplyeditgryhjszhxx',
				type : 'POST', //GET
				async : true, //或false,是否异步
				contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
				data : {"ids":ids},
				dataType : 'text', //返回的数据格式：json/xml/html/script/jsonp/text
				success : function(data) {
					if (data == '0') {
						$.messager.alert('','申请修改成功','info',function(r) {
							$('#dg1').datagrid('reload');
							$("#addDialog").dialog("close");
					    });
					} else {
						$.messager.alert('操作提示','申请修改失败','error');
					}
				}
			});
		}
	})
}

function notpass(){
	var loginid = '<%=loginid%>';
	var rows = $('#dg1').datagrid('getSelections');
	var ids = '';
	if(rows.length > 0){
		for(var i = 0;i<rows.length;i++){
			if(loginid != rows[i].czr){
				if(i == rows.length -1){
					ids = ids + rows[i].id;
				}else{
					ids = ids + rows[i].id + ',';
				}
			}
		}
	}
	
	if(rows.length > 0 && ids == ''){
		$.messager.alert("提示","审核员不能审核操作人员是自己的数据","info");
	}else{
		$.messager.prompt('提示','<span style="color:red;">注意:审核员不能审核操作人员是自己的数据</span></br>请填写审核不通过的原因!',function (e){
			if(e){
				$.ajax({
					url:"sshbtggryhjszhxx",
				    dataType:"json",
				    contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
				    async:true,
				    data:{"value":e,"ids":ids,"loginid":loginid},
				    type:"POST",
				    success:function(res){
				        if(res == '0'){
				        	$.messager.alert("提示","审核不通过成功","info");
				        	$("#dg1").datagrid('reload');
				        }else{
				        	$.messager.alert("提示","审核不通过失败！","error");
				        }
				    },
				    error:function(){
				    	$.messager.alert("提示","操作出错，请重新审核","error");
				    }
				});
			}else{
				$.messager.alert('提示','请填写审核不通过原因！','warning');
			} 
	   });
	}
}


function refuseapply(){
	var loginid = '<%=loginid%>';
	var rows = $('#dg1').datagrid('getSelections');
	var ids = '';
	if(rows.length > 0){
		for(var i = 0;i<rows.length;i++){
			if(loginid != rows[i].czr){
				if(i == rows.length -1){
					ids = ids + rows[i].id;
				}else{
					ids = ids + rows[i].id + ',';
				}
			}
		}
	}
	
	if(rows.length > 0 && ids == ''){
		$.messager.alert("提示","审核员不能审核操作人员是自己的数据","info");
	}else{
		$.messager.confirm('操作提示','<span style="color:red;">注意:审核员不能审核操作人员是自己的数据</span></br>确认要拒绝申请吗？',function(r){
			if(r){
				$.ajax({
					url:"sjjsqgryhjszhxx",
				    dataType:"json",
				    contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
				    async:true,
				    data:{"ids":ids,"loginid":loginid},
				    type:"POST",
				    success:function(res){
				        if(res == '0'){
				        	$.messager.alert("提示","拒绝申请成功","info");
				        	$("#dg1").datagrid('reload');
				        }else if(res == '1'){
				        	$.messager.alert("提示","没有需要拒绝申请的数据","info");
				        }else{
				        	$.messager.alert("提示","拒绝申请失败！","error");
				        }
				    },
				    error:function(){
				    	$.messager.alert("提示","操作出错，请重新审核","error");
				    }
				});
			}
		})
	}
}

function agreeapply(){
	var loginid = '<%=loginid%>';
	var rows = $('#dg1').datagrid('getSelections');
	var ids = '';
	if(rows.length > 0){
		for(var i = 0;i<rows.length;i++){
			if(loginid != rows[i].czr){
				if(i == rows.length -1){
					ids = ids + rows[i].id;
				}else{
					ids = ids + rows[i].id + ',';
				}
			}
		}
	}
	
	if(rows.length > 0 && ids == ''){
		$.messager.alert("提示","审核员不能审核操作人员是自己的数据","info");
	}else{
		$.messager.confirm('操作提示','<span style="color:red;">注意:审核员不能审核操作人员是自己的数据</span></br>确认要同意申请吗？',function(r){
			if(r){
				$.ajax({
					url:"stysqgryhjszhxx",
				    dataType:"json",
				    contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
				    async:true,
				    data:{"ids":ids,"loginid":loginid},
				    type:"POST",
				    success:function(res){
				        if(res == '0'){
				        	$.messager.alert("提示","同意申请成功","info");
				        	$("#dg1").datagrid('reload');
				        }else if(res == '1'){
				        	$.messager.alert("提示","没有需要同意申请的数据","info");
				        }else{
				        	$.messager.alert("提示","同意申请失败！","error");
				        }
				    },
				    error:function(){
				    	$.messager.alert("提示","操作出错，请重新审核","error");
				    }
				});
			}
		})
	}
}

function pass(){
	var loginid = '<%=loginid%>';
	var rows = $('#dg1').datagrid('getSelections');
	var ids = '';
	if(rows.length > 0){
		for(var i = 0;i<rows.length;i++){
			if(loginid != rows[i].czr){
				if(i == rows.length -1){
					ids = ids + rows[i].id;
				}else{
					ids = ids + rows[i].id + ',';
				}
			}
		}
	}
	
	if(rows.length > 0 && ids == ''){
		$.messager.alert("提示","审核员不能审核操作人员是自己的数据","info");
	}else{
		$.messager.confirm('操作提示','<span style="color:red;">注意:审核员不能审核操作人员是自己的数据</span></br>确认要审核通过吗？',function(r){
			if(r){
				$.ajax({
					url:"sshtggryhjszhxx",
				    dataType:"json",
				    contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
				    async:true,
				    data:{"ids":ids,"loginid":loginid},
				    type:"POST",
				    success:function(res){
				        if(res == '0'){
				        	$.messager.alert("提示","审核通过成功","info");
				        	$("#dg1").datagrid('reload');
				        }else{
				        	$.messager.alert("提示","审核通过失败！","error");
				        }
				    },
				    error:function(){
				    	$.messager.alert("提示","操作出错，请重新审核","error");
				    }
				});
			}
		})
	}
}
</script>
</body>
</html>
