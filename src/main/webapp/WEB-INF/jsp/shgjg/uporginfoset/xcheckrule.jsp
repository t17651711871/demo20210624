<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.geping.etl.common.entity.Sys_UserAndOrgDepartment" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String isusedelete = request.getSession().getAttribute("isusedelete")+"";
	String isuseupdate = request.getSession().getAttribute("isuseupdate")+"";
	
	List<String> reportnameList = (List<String>) request.getAttribute("reportnameList");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>校验规则</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
	<table id="dg1" style="height: 480px;" title="校验规则配置"></table>
	<div id="tb1" style="padding: 5px; height: auto;">
	     <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daoru()">导入</a>&nbsp;&nbsp; 
	     报文名称:<select class="easyui-combobox" id="reportname" data-options="editable:false">
	     	<option value=''>全部</option>
	     	<c:forEach items='${reportnameList}' var='aa'>
                                <option value='${aa}'>${aa}</option>
                            </c:forEach>
	     </select>&nbsp;&nbsp;
	      字段名称:<select class="easyui-combobox" id="fieldname" data-options="editable:false,valueField:'id',textField:'text'" style="width:200px">
	     </select>&nbsp;&nbsp;
	      状态:<select class="easyui-combobox" id="status" data-options="editable:false">
                    <option selected="selected" value="">===全部===</option>
                    <option value="y">已选择</option>
                    <option value="n">未选择</option>
               </select>&nbsp;&nbsp;   	     
	     <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveForRule()">保存设置</a>&nbsp;&nbsp;
	</div>
<!-- 导入文件dialog -->
<div style="visibility: hidden;">
        <div id="importExcel" class="easyui-dialog" style="width: 600px; height: 160px; padding-left: 30px" title="导入数据">
            文件导入:<input type="file" accept=".xls,.xlsx" name="excelfile" id="excelfile" onchange="inexcel()"/><br>
        <input type="button" id="importbtn" value="提交" style="width:50px;margin-top:20px;" onclick="excel()"/>
        </div>
</div>
	
<script type="text/javascript">
$(function(){
	$("#importExcel").dialog("close");
    
	$("#dg1").datagrid({
		method:'post',
		url:'XCheckRuleList',
		loadMsg:'数据加载中,请稍后...',
		//singleSelect:true,
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		fitColumns:true,
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			{field:'ck',checkbox:true},
		    {field:'checknum',title:'检核标准编号',width:100,align:'center'},
		    {field:'reportname',title:'报文名称',width:100,align:'center'},
		    {field:'tablename',title:'表名',width:100,align:'center',hidden:true},
		    {field:'fieldname',title:'字段名称',width:100,align:'center'},
		    {field:'checkrule',title:'规则说明',width:100,align:'center'},
		    {field:'tablesj',title:'涉及的表',width:100,align:'center',hidden:true},
		    {field:'rulesx',title:'规则实现逻辑',width:100,align:'center',hidden:true},
		    {field:'rulelimit',title:'限定条件',width:100,align:'center',hidden:true},
		    {field:'rulegl',title:'关联关系',width:100,align:'center',hidden:true},
		    {field:'isstart',title:'是否启用',width:100,align:'center',hidden:true},
		    {field:'isselect',title:'是否选择',width:100,align:'center',
				formatter:function(value,row,index){
					if(value == 'n'){
						return '未选择';
					}else if(value == 'y'){
						return '<a style="color:red">已选择</a>';
					}
				}}
    	]],
    	onLoadSuccess: function(index,field,value){
	    	var rows = index.rows;
	    	var count = 0;
			for(var i = 0;i<rows.length;i++){
				if(rows[i].isselect == 'y'){
					count = count + 1;
					$('#dg1').datagrid('selectRow',i);
				}
			}
			if(count != rows.length){
				$("#dg1").parent().find("div.datagrid-header-check").children("input[type='checkbox']").eq(0).attr("checked", false);
			}
		},
		onDblClickCell: function(index,field,value){
			$.messager.alert('',value,'info');
		}
	});
	
	
	$('#reportname').combobox({
		onSelect: function(record){
			$("#fieldname").combobox("clear");
			var reportname = $('#reportname').combobox('getValue');
			var fieldname = $('#fieldname').combobox('getValue');
			var status = $('#status').combobox('getValue');			
			$("#dg1").datagrid("load",{reportname:reportname,fieldname:fieldname,status:status});
			//$("#fieldname").combobox("reload","XgetFieldnameJson?reportname="+reportname);
			$("#fieldname").combobox({
				url:"XgetFieldnameJson?reportname="+encodeURI(reportname),
				valueField:"id",
				textField:"text"
			});
		}
	});
	
	$('#fieldname').combobox({
		onSelect: function(record){
			var reportname = $('#reportname').combobox('getValue');
			var fieldname = $('#fieldname').combobox('getValue');
			var status = $('#status').combobox('getValue');
			$("#dg1").datagrid("load",{reportname:reportname,fieldname:fieldname,status:status});
		}
	});
	
	$('#status').combobox({
		onSelect: function(record){
			var reportname = $('#reportname').combobox('getValue');
			var fieldname = $('#fieldname').combobox('getValue');
			var status = $('#status').combobox('getValue');
			$("#dg1").datagrid("load",{reportname:reportname,fieldname:fieldname,status:status});
		}
	});
});

function initForm(){
	document.getElementById("formForMore").reset(); 
}
//导入Excel相关
function daoru(){
    document.getElementById("importbtn").disabled = "disabled";
    $('#excelfile').val('');
    $('#importExcel').dialog('open').dialog('center');
    $('#importExcel').dialog({modal:true});
}
//导入选择文件按钮
function inexcel(){
    var file = document.getElementById("excelfile").files[0];
    if(file == null){
        document.getElementById("importbtn").disabled = "disabled";
    }else{
        var fileName = file.name;
        var fileType = fileName.substring(fileName.lastIndexOf('.'),
            fileName.length);
        if (fileType == '.xls' || fileType == '.xlsx'){
            if (file) {
                document.getElementById("importbtn").disabled = "";
            }
        } else {
            $.messager.alert('提示', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！","error");
            document.getElementById("importbtn").disabled = "disabled";
        }
    }
}
//导入提交按钮
function excel(){
    $.messager.progress({
        title: '请稍等',
        msg: '数据正在导入中......'
    });
    $.ajaxFileUpload({
        type: "post",
        url: 'XCheckRuleImport',
        fileElementId: 'excelfile',
        secureuri: false,
        dataType: 'json',
        success: function (data) {
            const that = data.msg;
            $.messager.progress('close');
            if (that == "导入成功") {
                $.messager.alert('提示', "导入成功", 'info');
                $("#dg1").datagrid('reload');
                $('#importExcel').dialog('close');
                window.location.href="XCheckRuleUi";
            } else if (that.startsWith("导入模板不正确")) {
                $.messager.alert('提示', that, 'error');
            } else {
            	$.messager.alert('提示', that, 'error');
            }
        },
        error: function () {
            $.messager.progress('close');
            $.messager.alert('提示', '导入文件发生未知错误，请重新刷新', 'error');
        }
    });
}

//保存校验规则配置
function saveForRule(){
	var currentrows = $('#dg1').datagrid('getRows');
	var selectedrows = $('#dg1').datagrid('getSelections');
	$.messager.progress({
		title:'请稍等',
		msg:'正在保存设置......'
    });
	var reportname = $('#reportname').combobox('getValue');
	var fieldname = $('#fieldname').combobox('getValue');
	var status = $('#status').combobox('getValue');
	
	var idstr = "";
	var selectidstr = "";
	if(currentrows.length !=  0){
		idstr = "(";
		for(var i= 0;i<currentrows.length;i++){
			if(i == currentrows.length-1){
				idstr = idstr + "'" + currentrows[i].id + "'";
			}else{
				idstr = idstr + "'" + currentrows[i].id + "',";
			}
		}
		idstr = idstr + ")";
	}
	
	if(selectedrows.length !=  0){
		selectidstr = "(";
		for(var i= 0;i<selectedrows.length;i++){
			if(i == selectedrows.length-1){
				selectidstr = selectidstr + "'" + selectedrows[i].id + "'";
			}else{
				selectidstr = selectidstr + "'" + selectedrows[i].id + "',";
			}
		}
		selectidstr = selectidstr + ")";
	}
			
		$.ajax({
	        url:'XSavePeiZhi',
	        type:'POST',
	        async:false,
	        contentType:'application/x-www-form-urlencoded; charset=UTF-8',
	        data:{"idstr":idstr,"selectidstr":selectidstr,"reportname":reportname,"fieldname":fieldname,"status":status},
	        dataType:'text',
	        success:function(data){
	        	$.messager.progress('close');
	        	if(data == '0'){
	        		$.messager.alert('提示',"设置成功",'info');
	        		$('#dg1').datagrid('reload');
	        	}
	        	
	        	if(data == '1'){
	        		$.messager.alert('提示',"设置失败",'error');
	        	}
            },
            error : function(data, status, e){
        	   $.messager.progress('close');
        	   $.messager.alert('错误提示',e,'error');
   		    }
      	})
}
</script>
</body>
</html>
