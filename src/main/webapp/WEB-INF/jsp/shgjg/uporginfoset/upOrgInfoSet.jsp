<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.SHGJG.entity.UpOrgInfoSet.SUpOrgInfoSet"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	
	SUpOrgInfoSet info = (SUpOrgInfoSet)request.getAttribute("orginfo");
	String id = "";
	String bankcode = "";
	String bankcodefaren = "";
	String bankcodelei = "";
	String bankcodewangdian = "";
	String bankname = "";
	String isusedelete = "yes";
	String isuseupdate = "yes";
	String messagepath = "";
	String isusedepart = "no";
	String isusedepartformessage = "no";
	String shifoushenheziji = "no";
	String checklimit = "";
	if(info != null){
		id = info.getId();
		bankcodefaren = info.getBankcodefaren() == null ?"" : info.getBankcodefaren();
		bankcodelei = info.getBankcodelei() == null  ? "" : info.getBankcodelei();
		bankcodewangdian = info.getBankcodewangdian() == null  ? "" : info.getBankcodewangdian();
		bankname = info.getBankname() == null  ? "" : info.getBankname();
		isusedelete = info.getIsusedelete();
		isuseupdate = info.getIsuseupdate();
		messagepath = info.getMessagepath() == null  ? "" : info.getMessagepath();
		isusedepart = info.getIsusedepart();
		isusedepartformessage = info.getIsusedepartformessage();
		shifoushenheziji = info.getShifoushenheziji();
		checklimit = info.getChecklimit();
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>上报机构信息设置</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
 <!--信息编辑的对话框 -->
	<div style="visibility: hidden;">
		<div id="editDialog" class="easyui-dialog" title="上报机构信息设置" data-options="closable:false,draggable:false" style="width: 600px; height: 390px;top:10px;left:10px;">
			<form id="infoedit" method="post">
				<table id="info_edit" style="padding-left: 15px;padding-top: 5px;font-size: 8px;">
				     <input type="hidden" id="id" name="id"/>
				
					 <tr>
						<td align="right">开户银行金融机构代码(法人):</td>
						<td><input type="text" id="bankcodefaren" name="bankcodefaren" style="height: 25px;width: 190px"/></td>
				     </tr>
				     
				     <tr>
						<td align="right">开户机构法人机构识别编码(LEI):</td>
						<td><input type="text" id="bankcodelei" name="bankcodelei" style="height: 25px;width: 190px"/></td>
				     </tr>
				     
				     <tr>
						<td align="right">开户银行金融机构代码(网点):</td>
						<td><input type="text" id="bankcodewangdian" name="bankcodewangdian" style="height: 25px;width: 190px"/></td>
				     </tr>
				     
				     <tr>
						<td align="right">银行机构名称:</td>
						<td><input type="text" id="bankname" name="bankname" style="height: 25px;width: 190px"/></td>
				     </tr>
				    <!--  
				     <tr>
						<td align="right">是否启用申请删除:</td>
						<td>
							  <select class="easyui-combobox" id="isusedelete" name="isusedelete" style="height: 25px;width: 190px">
							       <option value="yes">是</option>
							       <option value="no">否</option>
							  </select>
					    </td>
				     </tr>
				     
				     <tr>
						<td align="right">是否启用申请修改:</td>
						<td>
						  <select class="easyui-combobox" id="isuseupdate" name="isuseupdate" style="height: 25px;width: 190px">
						       <option value="yes">是</option>
						       <option value="no">否</option>
						  </select>
						</td>
				     </tr>
				      -->
				      
				      <tr>
						<td align="right">是否启用审核自己:</td>
						<td>
						  <select class="easyui-combobox" id="shifoushenheziji" name="shifoushenheziji" style="height: 25px;width: 190px">
						       <option value="no">否</option>
						       <option value="yes">是</option>
						  </select>
						</td>
				     </tr>
				      
				     <tr>
						<td align="right">是否启用分部门处理:</td>
						<td>
						  <select class="easyui-combobox" id="isusedepart" name="isusedepart" style="height: 25px;width: 190px">
						       <option value="no">否</option>
						       <option value="yes">是</option>
						  </select>
						</td>
				     </tr>
				     
				     <tr>
						<td align="right">生成报文是否启用分部门处理:</td>
						<td>
						  <select class="easyui-combobox" id="isusedepartformessage" name="isusedepartformessage" style="height: 25px;width: 190px">
						       <option value="no">否</option>
						       <option value="yes">是</option>
						  </select>
						</td>
				     </tr>
				     
				     <tr>
						<td align="right">校验阈值设置:</td>
						<td><input type="text" id="checklimit" name="checklimit" style="height: 25px;width: 190px"/></td>
				     </tr>
				     
				     <tr>
						<td align="right">报文存放地址:</td>
						<td><input type="text" id="messagepath" name="messagepath" style="height: 25px;width: 290px"/></td>
				     </tr>
				     
				     
				     <tr>
				        <td align="left"></td>
				        <td><a class="easyui-linkbutton" iconCls="icon-save" onclick="save()">保存</a></td>
				     </tr>
				</table>
		    </form>
		</div>
	</div>
	
	
<script type="text/javascript">
$(function(){
	$('#bankcodefaren').val('<%=bankcodefaren%>');
	$('#bankcodelei').val('<%=bankcodelei%>');
	$('#bankcodewangdian').val('<%=bankcodewangdian%>');
	$('#bankname').val('<%=bankname%>');
	$('#isusedelete').combobox('setValue','<%=isusedelete%>');
	$('#isuseupdate').combobox('setValue','<%=isuseupdate%>');
	$('#messagepath').val('<%=messagepath%>');
	$('#isusedepart').combobox('setValue','<%=isusedepart%>');
	$('#isusedepartformessage').combobox('setValue','<%=isusedepartformessage%>');
	$('#shifoushenheziji').combobox('setValue','<%=shifoushenheziji%>');
	$('#checklimit').val('<%=checklimit%>');
});


function save() {
	$.ajax({
		url : 'saddinfoset',
		type : 'POST', //GET
		async : true, //或false,是否异步
		contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
		data : $('#infoedit').serialize(),
		dataType : 'text', //返回的数据格式：json/xml/html/script/jsonp/text
		success : function(data) {
			if (data == '0') {
				$.messager.alert('','保存成功,请重新登录','info',function(r){
					window.location.href = "userOut";
           		});
			} else {
				$.messager.alert('操作提示','保存失败','error');
			}
		}
	});
}
</script>
</body>
</html>
