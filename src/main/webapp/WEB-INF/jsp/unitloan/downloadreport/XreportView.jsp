<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="../../common/head.jsp"%>
<html>
<head>
    <title>下载报文</title>
</head>
<script type="text/javascript">
    $(function () {
        $("#dg").datagrid({
            loadMsg:'数据加载中，请稍后...',
            url:'XdownloadreportView',
            cache:false,
            fitColumns:true,
            autoRowHeight:false,        //自动行高
            rownumbers:true,            //开启行号
            pagination: true,     //开启分页
            sortName:'reportdate',
            sortOrder:'desc',
            remoteSort:false,
            toolbar:"#tb",
            pageSize: 10,         //分页大小
            pageNumber:1,         //第几页显示（默认第一页，可以省略）
            pageList: [10, 20, 30, 50], //设置每页记录条数的列表
            columns:[[
                {field:'checkbox',checkbox:true},
                {field:'id',title:'编号',width:60,align:'center'},
                {field:'reportname',title:'报文名称',width:60,align:'center'},
                {field:'reportdate',title:'生成日期',width:60,align:'center'},
                {field:'operator',title:'操作人',hidden:true},
                {field:'operation',title:'操作',width:60,align:'center', formatter: function (value, row) {
                	var id = "'" + row.id + "'";
                	return '<a href="javascript:void(0);" onclick="downloadReport('+id+')" class="easyui-linkbutton">下载报文</a>';
                }}
            ]]
        });
    });

    function selectPmoce() {
    	var reportdateParam = $("#reportdateParam").datebox('getValue');
    	var reportnameParam = $("#reportnameParam").val();
        $("#dg").datagrid("reload", {"reportdateParam" : reportdateParam,"reportnameParam" : reportnameParam});
    }

    function downloadReport(id){
        window.location.href="XReportDownload?id=" + id;
    }
</script>
<body>
<div id="tb">
     报文名称:&nbsp;<input type="text" style="width: 173px; height: 20px" id="reportnameParam" />
     报文日期:&nbsp;<input class="easyui-datebox" style="width: 173px; height: 23px" id="reportdateParam" />
    &nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-search" onclick="selectPmoce();">查询</a>
</div>
<table id="dg" style="height: 480px;">
</table>
</body>
</html>
