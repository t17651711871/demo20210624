<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.geping.etl.common.entity.Sys_UserAndOrgDepartment" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String isusedelete = request.getSession().getAttribute("isusedelete")+"";
	String isuseupdate = request.getSession().getAttribute("isuseupdate")+"";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>行业大类</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
	<table id="dg1" style="height: 480px;" title="行业大类"></table>
	<div id="tb1" style="padding: 5px; height: auto;">
	    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daoru()">导入</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochu()">导出</a>
	</div>
	<!--表格工具栏-->
	<!-- <div id="tb1" style="padding: 5px; height: auto;">
	    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;
	</div> -->
<!-- 导入文件dialog -->
<div style="visibility: hidden;">
        <div id="importExcel" class="easyui-dialog" style="width: 600px; height: 160px; padding-left: 30px" title="导入数据">
            文件导入:<input type="file" accept=".xls,.xlsx" name="excelfile" id="excelfile" onchange="inexcel()"/><br>
        <input type="button" id="importbtn" value="提交" style="width:50px;margin-top:20px;" onclick="excel()"/>
        </div>
</div>
<!-- 查看详情 -->
<div style="visibility: hidden;">
    <div id="dialog_m" class="easyui-dialog" title="行业分类" data-options="iconCls:'icon-more'" style="width:500px;height:480px;">
          
         <form id="formForMore" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>一级行业代码 :</td>
                 <td><input class="easyui-validatebox" id="bindustrycode_m" name="bindustrycode" style="height: 23px; width:230px" readonly="readonly"/></td>
                </tr> 
                 
               <tr>
                 <td>一级行业名称:</td>
                 <td><input class="easyui-validatebox" id="bindustryname_m" name="bindustryname" style="height: 23px; width:230px" readonly="readonly"/></td>
              </tr>
                
               <tr> 
                 <td>一级行业描述 :</td>
                 <td><input class="easyui-validatebox" id="bindustrydesc_m" name="bindustrydesc" style="height: 23px; width:230px" readonly="readonly"/></td>
			   </tr>
			   
              </table>
           </div>
         </form>
	</div>
</div>
	
<script type="text/javascript">
$(function(){
	$("#importExcel").dialog("close");
	$('#dialog_m').dialog('close');
    
	$("#dg1").datagrid({
		method:'post',
		url:'XfindBaseAindustry',
		loadMsg:'数据加载中,请稍后...',
		singleSelect:true,
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		fitColumns:true,
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			{field:'ck',checkbox:true,hidden:true},
		    {field:'aindustrycode',title:'一级行业代码',width:100,align:'center'},
		    {field:'aindustryname',title:'一级行业名称',width:100,align:'center'},
		    {field:'aindustrydesc',title:'一级行业描述',width:100,align:'center'}
    	]],
	    onDblClickRow :function(rowIndex,rowData){
	    	var dia = $('#dialog_m').dialog('open');
	    	$("#formForMore").form('load',rowData);
	    	$('#dialog_m').dialog({modal : true});	
	    	//关闭弹窗默认隐藏div
			$(dia).window({
		    	onBeforeClose: function () {
		    		//初始化表单的元素的状态
		    		initForm();
		    	}
			});
	    }
	});
	
});

function initForm(){
	document.getElementById("formForMore").reset(); 
}
//导入Excel相关
function daoru(){
    document.getElementById("importbtn").disabled = "disabled";
    $('#excelfile').val('');
    $('#importExcel').dialog('open').dialog('center');
    $('#importExcel').dialog({modal:true});
}
//导入选择文件按钮
function inexcel(){
    var file = document.getElementById("excelfile").files[0];
    if(file == null){
        document.getElementById("importbtn").disabled = "disabled";
    }else{
        var fileName = file.name;
        var fileType = fileName.substring(fileName.lastIndexOf('.'),
            fileName.length);
        if (fileType == '.xls' || fileType == '.xlsx'){
            if (file) {
                document.getElementById("importbtn").disabled = "";
            }
        } else {
            $.messager.alert('提示', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！","error");
            document.getElementById("importbtn").disabled = "disabled";
        }
    }
}
//导入提交按钮
function excel(){
    $.messager.progress({
        title: '请稍等',
        msg: '数据正在导入中......'
    });
    $.ajaxFileUpload({
        type: "post",
        url: 'Xdaoruhangyedalei',
        fileElementId: 'excelfile',
        secureuri: false,
        dataType: 'json',
        success: function (data) {
            const that = data.msg;
            $.messager.progress('close');
            if (that == "导入成功") {
                $.messager.alert('提示', "导入成功", 'info');
                $("#dg1").datagrid('reload');
                $('#importExcel').dialog('close');
            } else if (that.startsWith("导入模板不正确")) {
                $.messager.alert('提示', that, 'error');
            } else {
            	$.messager.alert('提示', that, 'error');
            }
        },
        error: function () {
            $.messager.progress('close');
            $.messager.alert('提示', '导入文件发生未知错误，请重新刷新', 'error');
        }
    });
}

//导出
function daochu(){
	$.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
		if(r){
			window.location.href = "Xdaochuhangyedalei";
		}
	});	
}
</script>
</body>
</html>
