<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.geping.etl.common.entity.Sys_UserAndOrgDepartment" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String isusedelete = request.getSession().getAttribute("isusedelete")+"";
	String isuseupdate = request.getSession().getAttribute("isuseupdate")+"";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>企业规模</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
	<table id="dg1" style="height: 480px;" title="企业规模"></table>
	
	<!--表格工具栏-->
	<!-- <div id="tb1" style="padding: 5px; height: auto;">
	    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;
	</div> -->

<!-- 查看详情 -->
<div style="visibility: hidden;">
    <div id="dialog_m" class="easyui-dialog" title="企业规模" data-options="iconCls:'icon-more'" style="width:500px;height:480px;">
          
         <form id="formForMore" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>担保合同编码 :</td>
                 <td><input class="easyui-validatebox" id="gteecontractcode_m" name="gteecontractcode" style="height: 23px; width:230px" readonly="readonly"/></td>
                </tr> 
                 
               <tr>
                 <td>贷款合同编码:</td>
                 <td><input class="easyui-validatebox" id="loancontractcode_m" name="loancontractcode" style="height: 23px; width:230px" readonly="readonly"/></td>
              </tr>
                
               <tr> 
                 <td>担保物编码 :</td>
                 <td><input class="easyui-validatebox" id="gteegoodscode_m" name="gteegoodscode" style="height: 23px; width:230px" readonly="readonly"/></td>
			   </tr>
			   
			   <tr>
			     <td>担保物类别 :</td>
                 <td>
                 <select class="easyui-combobox" id="gteegoodscategory_m" name="gteegoodscategory" editable=false style="height: 23px; width:230px">
						<!-- <option value="A">A:金融质押品</option> -->
						<option value="A01">A01:保证金</option>
						<option value="A02">A02:存单</option>
						<option value="A03">A03:贵金属</option>
						<option value="A04">A04:债券</option>
						<option value="A05">A05:票据</option>
						<option value="A06">A06:股票（权）</option>
						<option value="A07">A07:基金</option>
						<option value="A08">A08:保单</option>
						<option value="A09">A09:资产管理产品（不含公募金）</option>
						<option value="A99">A99:其他金融质押品</option>
						<!-- <option value="B">B:应收账款押品</option> -->
						<option value="B01">B01:普通应收账款</option>
						<option value="B02">B02:各类收费（益）权</option>
						<option value="B99">B99:其他应收账款</option>
						<!-- <option value="C">C:房地产类押品</option> -->
						<option value="C01">C01:居住用房地产</option>
						<option value="C02">C02:商业用房地产</option>
						<option value="C03">C03:居住用房地产建设用地使用权</option>
						<option value="C04">C04:商业用房地产建设用地使用权</option>
						<option value="C05">C05:房产类在建工程</option>
						<option value="C99">C99:其他房地产类押品</option>
						<!-- <option value="D">D:其他类押品</option> -->
						<option value="D01">D01:存货、仓单和提单</option>
						<option value="D02">D02:机器设备</option>
						<option value="D03">D03:交通运输设备</option>
						<option value="D04">D04:资源资产</option>
						<option value="D05">D05:知识产权</option>
						<option value="D99">D99:其他以上未包括的押品</option>
	                </select>
                 </td>
               </tr> 
                
               <tr>  
                 <td>权证编号 :</td>
                 <td><input class="easyui-validatebox" id="warrantcode_m" name="warrantcode" style="height: 23px; width:230px" readonly="readonly"/></td>
               </tr> 
                
               <tr> 
                 <td>是否第一顺位 :</td>
                 <td> 
                    <select class="easyui-combobox" id="isfirst_m" name="isfirst" editable=false style="height: 23px; width:230px">
						<option value="0">0:否</option>
					   <option value="1">1:是</option>
	                </select>
	              </td>
			   <tr>
			   
			   <tr>
			     <td>评估方式 :</td>
			     <td>
			        <select class="easyui-combobox" id="assessmode_m" name="assessmode" editable=false style="height: 23px; width:230px">
						<option value="01">01:外部评估</option>
					    <option value="02">02:内部评估</option>
					    <option value="03">03:内外部合作评估</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>评估方法 :</td>
			     <td>
			        <select class="easyui-combobox" id="assessmethod_m" name="assessmethod" editable=false style="height: 23px; width:230px">
						<option value="01">01:收益法</option>
					    <option value="02">02:市场法</option>
					    <option value="03">03:成本法</option>
					    <option value="04">04:其他</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>评估价值 :</td>
			     <td><input class="easyui-validatebox" id="assessvalue_m" name="assessvalue" style="height: 23px; width:230px" readonly="readonly"/></td>
			   </tr>
			   
			   <tr>
			     <td>评估基准日 :</td>
			     <td><input class="easyui-datebox" id="assessdate_m" name="assessdate" editable=false style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>担保物账面价值 :</td>
			     <td><input class="easyui-validatebox" id="gteegoodsamt_m" name="gteegoodsamt" style="height: 23px; width:230px" readonly="readonly"/></td>
			   </tr>
			   
			   <tr>
			     <td>优先受偿权数额 :</td>
			     <td><input class="easyui-validatebox" id="firstrightamt_m" name="firstrightamt" style="height: 23px; width:230px" readonly="readonly"/></td>
			   </tr>
			   
			   <tr>
			     <td>担保物状态 :</td>
			     <td>
			        <select class="easyui-combobox" id="gteegoodsstataus_m" name="gteegoodsstataus" editable=false style="height: 23px; width:230px">
						<option value="01">01:正常</option>
					    <option value="02">02:已变现处置</option>
					    <option value="03">03:待处理抵债资产</option>
					    <option value="04">04:灭失损毁</option>
					    <option value="05">05:其他</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>抵质押率 :</td>
			     <td>
			        <input class="easyui-validatebox" id="mortgagepgerate_m" name="mortgagepgerate" style="height: 23px; width:230px" readonly="readonly"/>
			     </td>
			   </tr>
			   
              </table>
           </div>
         </form>
	</div>
</div>
	
<script type="text/javascript">
$(function(){
	$('#dialog_m').dialog('close');
    
	$("#dg1").datagrid({
		method:'post',
		url:'XfindEnterprise',
		loadMsg:'数据加载中,请稍后...',
		//singleSelect:true,
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		fitColumns:true,
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			{field:'ck',checkbox:true},
		    {field:'daima',title:'企业规模代码',width:100,align:'center'},
		    {field:'mingcheng',title:'企业规模名称',width:100,align:'center'},
		    {field:'miaoshu',title:'企业规模描述',width:100,align:'center'}
    	]],
	    onDblClickRow :function(rowIndex,rowData){
	    	var dia = $('#dialog_m').dialog('open');
	    	$("#formForMore").form('load',rowData);
	    	$('#dialog_m').dialog({modal : true});	
	    	//关闭弹窗默认隐藏div
			$(dia).window({
		    	onBeforeClose: function () {
		    		//初始化表单的元素的状态
		    		initForm();
		    	}
			});
	    }
	});
	
	//$("#dg1").datagrid('loadData',datas);
});

function initForm(){
	document.getElementById("formForMore").reset(); 
}

</script>
</body>
</html>
