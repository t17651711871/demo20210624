<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.geping.etl.common.entity.Sys_UserAndOrgDepartment" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String isusedelete = request.getSession().getAttribute("isusedelete")+"";
	String isuseupdate = request.getSession().getAttribute("isuseupdate")+"";
	String shifoushenheziji = request.getSession().getAttribute("shifoushenheziji")+"";
	Sys_UserAndOrgDepartment sys_User = ((Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User"));
    String shenheren = sys_User.getLoginid();
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>金融机构（分支机构）基础信息</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
	<table id="dg1" style="height: 480px;" title="金融机构（分支机构）基础信息"></table>
	
	<!--表格工具栏-->
	<div id="tb1" style="padding: 5px; height: auto;">
	    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;
		金融机构名称 ：<input type="text" id="selectjrjgmc" name="selectjrjgmc" style="height: 23px; width:130px"/>&nbsp;&nbsp;
		金融机构代码 ：<input type="text" id="selectjrjgdm" name="selectjrjgdm" style="height: 23px; width:130px"/>&nbsp;&nbsp;
		校验类型 ：
		<select class="easyui-combobox" id="selectjylx" name="selectjylx" editable=false style="height: 23px; width:145px" >
			<option value="">-未选择-</option>
			<option value="0">0:未校验</option>
			<option value="1">1:校验成功</option>
			<option value="2">2:校验失败</option>
	    </select>&nbsp;&nbsp;
	         操作名 ：
		<select class="easyui-combobox" id="selectczm" name="selectczm" editable=false style="height: 23px; width:145px" >
			<option value="">-未选择-</option>
			<option value="1">1:申请删除</option>
	    </select>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="chaxun()">查询</a></br>
		<%-- <%if("yes".equals(isusedelete)) {%>
	      <a class="easyui-linkbutton" iconCls="icon-remove" onclick="shenqingshanchu()">申请删除</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<%}%>
		<%if("yes".equals(isuseupdate)) {%>
	      <a class="easyui-linkbutton" iconCls="icon-edit" onclick="shenqingxiugai()">申请修改</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<%}%>
		<a class="easyui-linkbutton" iconCls="icon-ok" onclick="tongyishenqing()">同意申请</a>&nbsp;&nbsp; --%>
		<a class="easyui-linkbutton" iconCls="icon-ok" onclick="tongyishenqingshanchu()">同意申请删除</a>&nbsp;&nbsp;
        <a class="easyui-linkbutton" iconCls="icon-cross" onclick="jujueshenqing()">拒绝申请删除</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-application-get" onclick="shenhetongguo()">审核通过</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-cross" onclick="shenhebutongguo()">审核不通过</a>
		<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochu()">导出</a>&nbsp;&nbsp;
	</div>

<!-- 详情 -->
<div style="visibility: hidden;">
    <div id="dialog_m" class="easyui-dialog" title="金融机构（分支机构）基础信息"  data-options="iconCls:'icon-more'" style="width:520px;height:500px;">
          
         <form id="formForMore" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>金融机构名称 :</td>
                 <td><input class="easyui-validatebox" id="finorgname_m" name="finorgname" style="height: 23px; width:230px" disabled="disabled"/></td>
                </tr> 
                 
               <tr>
                 <td>金融机构代码:</td>
                 <td><input class="easyui-validatebox" id="finorgcode_m" name="finorgcode" style="height: 23px; width:230px" disabled="disabled"/></td>
              </tr>
                
               <tr> 
                 <td>金融机构编码 :</td>
                 <td><input class="easyui-validatebox" id="finorgnum_m" name="finorgnum" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>  
                 <td>内部机构号 :</td>
                 <td><input class="easyui-validatebox" id="inorgnum_m" name="inorgnum" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
			   
			   <tr> 
                 <td>许可证号 :</td>
                 <td><input class="easyui-validatebox" id="xkzh_m" name="xkzh" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>  
                 <td>支付行号 :</td>
                 <td><input class="easyui-validatebox" id="zfhh_m" name="zfhh" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
			   
			   <tr>
			     <td>机构级别:</td>
                 <td>
                 <select id="orglevel_m" name="orglevel" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:总行</option>
						<option value="02">02:分行</option>
						<option value="03">03:支行</option>
						<option value="04">04:网点（分理处、储蓄所等）</option>
						<option value="05">05:事业部</option>
						<option value="99">99:其他</option>
	                </select>
                 </td>
               </tr> 
                
               <tr>  
                 <td>直属上级管理机构名称:</td>
                 <td><input class="easyui-validatebox" id="highlevelorgname_m" name="highlevelorgname" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>  
                 <td>直属上级管理机构金融机构编码 :</td>
                 <td><input class="easyui-validatebox" id="highlevelfinorgcode_m" name="highlevelfinorgcode" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>
			     <td>直属上级管理机构内部机构号 :</td>
			     <td><input class="easyui-validatebox" id="highlevelinorgnum_m" name="highlevelinorgnum" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>注册地址 :</td>
			     <td><input class="easyui-validatebox" id="regarea_m" name="regarea" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>地区代码 :</td>
			     <td>
			        <input id="regareacode_m" name="regareacode" style="height: 23px; width:230px" data-options="valueField:'id',textField:'text'" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <!-- <tr>
			     <td>办公地址 :</td>
			     <td>
			        <input class="easyui-validatebox" id="workarea_m" name="workarea" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>办公地行政区划代码 :</td>
			     <td>
			        <input id="workareacode_m" name="workareacode" style="height: 23px; width:230px" data-options="valueField:'id',textField:'text'" disabled="disabled"/>
			     </td>
			   </tr> -->
			   
			   <tr>
			     <td>成立时间 :</td>
			     <td><input id="setupdate_m" name="setupdate" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业状态 :</td>
			     <td>
			        <select id="mngmestus_m" name="mngmestus" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:正常运营</option>
					    <option value="02">02:停业（歇业）</option>
					    <option value="03">03:筹建</option>
					    <option value="04">04:当年关闭</option>
					    <option value="05">05:当年破产</option>
					    <option value="06">06:当年注销</option>
					    <option value="07">07:当年吊销</option>
					    <option value="99">99:其他</option>
	                </select>
			     </td>
			   </tr>


				 <tr>
					 <td>数据日期 :</td>
					 <td><input id="sjrq_m" name="sjrq" style="height: 23px; width:230px" disabled="disabled"/></td>
				 </tr>

			 </table>
           </div>
         </form>
	</div>
</div>
	
<script type="text/javascript">
var shenheren = "";
var shifoushenheziji = "";
$(function(){
	$('#dialog_m').dialog('close');
	shenheren = '<%=shenheren%>';
	shifoushenheziji = '<%= shifoushenheziji%>';
	getbasecode();
	
	$("#dg1").datagrid({
		method:'post',
		url:'XfindJRJGFZJCXXdsh',
		loadMsg:'数据加载中,请稍后...',
		//singleSelect:true,
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		fitColumns:true,
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			{field:'ck',checkbox:true},
			{field:'operationname',title:'操作名',width:100,align:'center'},
			{field:'nopassreason',title:'审核不通过原因',width:100,align:'center'},
			{field:'checkstatus',title:'校验结果',width:100,align:'center',formatter: function(value,row,index){					
				if(value == '2'){
					return '<span style="color:red;">' + '校验失败' + '</span>';
				}else if(value == '1'){
					return '校验成功';
				}else if(value == '0'){
					return '未校验';
				}else{
					return value;
				}
			}},
			{field:'finorgname',title:'金融机构名称',width:100,align:'center'},
			{field:'finorgcode',title:'金融机构代码',width:100,align:'center'},
			{field:'finorgnum',title:'金融机构编码',width:100,align:'center'},
			{field:'inorgnum',title:'内部机构号',width:100,align:'center'},
			{field:'xkzh',title:'许可证号',width:100,align:'center'},
		    {field:'zfhh',title:'支付行号',width:100,align:'center'},
			{field:'orglevel',title:'机构级别',width:100,align:'center'},
			{field:'highlevelorgname',title:'直属上级管理机构名称',width:100,align:'center'},
			{field:'highlevelfinorgcode',title:'直属上级管理机构金融机构编码',width:100,align:'center'},
			{field:'highlevelinorgnum',title:'直属上级管理机构内部机构号',width:100,align:'center'},
			{field:'regarea',title:'注册地址',width:100,align:'center'},
			{field:'regareacode',title:'地区代码',width:100,align:'center'},
			{field:'workarea',title:'办公地址',width:100,align:'center',hidden:true},
			{field:'workareacode',title:'办公地行政区划代码',width:100,align:'center',hidden:true},
			{field:'setupdate',title:'成立时间',width:100,align:'center'},
			{field:'mngmestus',title:'营业状态',width:100,align:'center'},
            {field:'sjrq',title:'数据日期',width:100,align:'center'},
			{field:'operator',title:'操作人',width:100,align:'center'},
		    {field:'operationtime',title:'操作时间',width:100,align:'center'}
	    ]],
	    onDblClickRow :function(rowIndex,rowData){
	    	initForm();
	    	var dia = $('#dialog_m').dialog('open');
        	$("#formForMore").form('load',rowData);
        	$('#dialog_m').dialog({modal : true});	
        	//$("#orglevel_m").combobox({disabled: true});
        	//$("#regareacode_m").combobox({disabled: true});
        	//$("#workareacode_m").combobox({disabled: true});
        	//$("#setupdate_m").datebox({disabled: true});
        	//$("#mngmestus_m").combobox({disabled: true});
        	//关闭弹窗默认隐藏div
    		/* $(dia).window({
    	    	onBeforeClose: function () {
    	    		//初始化表单的元素的状态
    	    		initForm();
    	    	}
    		}); */
	    }
	});
	
	//$("#dg1").datagrid('loadData',datas);
});

//获取基础代码
function getbasecode(){
	//getAindustry("CURRENCY"); //一级行业代码
	getArea(); //行政区划代码
	//getBindustry("customerIdType"); //二级行业代码
	//getCountry("customerIdType2"); //国家代码
	//getCurrency("artificialPersonType"); //币种代码
}

//获取行政区划代码
function getArea(){
	$.ajax({
	    url:'getAreaCode',
	    type:'POST', //GET
	    async:true,    //或false,是否异步
	    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
	    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
	    success:function(data){
	    	var areaList = data.list;
		    if(areaList != null && areaList != ""){
		    	var jsonStr='[';
		    	for(var i = 0;i<areaList.length;i++){
		     		 jsonStr = jsonStr + '{"id":"'+areaList[i].areacode+'",'+'"val":"'+areaList[i].areaname+'",'+'"text":"'+areaList[i].areacode+'-'+areaList[i].areaname+'"},'
		        }
		    	jsonStr = jsonStr.substring(0,jsonStr.lastIndexOf(','));   //如果是以,结尾，则截取,前面的字符串
		        jsonStr = jsonStr + ']';
		    	
		        var jsonObj = JSON.parse(jsonStr);     //将json格式的字符串转为json对象
		 	   $("#regareacode_m").combobox("loadData",jsonObj);
		 	   //$("#workareacode_m").combobox("loadData",jsonObj);
		    }
	    }
	});
}


//查询
function chaxun(){
	var selectjrjgmc = $("#selectjrjgmc").val();
	var selectjrjgdm = $("#selectjrjgdm").val();
	var selectjylx = $('#selectjylx').combobox('getValue');
	var selectczm = $('#selectczm').combobox('getValue');
	$("#dg1").datagrid("load",{selectjrjgmc:selectjrjgmc,selectjrjgdm:selectjrjgdm,selectjylx:selectjylx,selectczm:selectczm});
}

function initForm(){
	//document.getElementById("formForMore").reset(); 
	$("#finorgname_m").val('');
	$("#finorgcode_m").val('');
	$("#finorgnum_m").val('');
	$("#inorgnum_m").val('');
	$("#xkzh_m").val('');
	$("#zfhh_m").val('');
	$("#orglevel_m").val('setValue','');
	$("#highlevelorgname_m").val('');
	$("#highlevelfinorgcode_m").val('');
	$("#highlevelinorgnum_m").val('');
	$("#regarea_m").val('');
	$("#regareacode_m").val('setValue','');
	//$("#workarea_m").val('');
	//$("#workareacode_m").val('setValue','');
	$("#setupdate_m").val('setValue','');
    $("#sjrq_m").val('setValue','');
	$("#mngmestus_m").val('setValue','');
}

//申请删除
function shenqingshanchu(){
	//var rows = $('#dg1').datagrid('getRows');
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要申请删除选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				for (var i = 0; i < rows.length; i++){
					deleteid = deleteid+rows[i].id+"-";
				}
				$.ajax({
		            url:'Xshenqingshanchujrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"some","deleteid":deleteid},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','申请删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','申请删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要申请删除所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'Xshenqingshanchujrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','申请删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','申请删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
	//$("#dg1").datagrid('loadData',rows);
	//$.messager.alert('提示','申请删除成功','info');
}

//申请修改
function shenqingxiugai(){
	//var rows = $('#dg1').datagrid('getRows');
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要申请修改选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				for (var i = 0; i < rows.length; i++){
					deleteid = deleteid+rows[i].id+"-";
				}
				$.ajax({
		            url:'Xshenqingxiugaijrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"some","deleteid":deleteid},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','申请修改成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','申请修改失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要申请修改所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'Xshenqingxiugaijrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','申请修改成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','申请修改失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
	//$("#dg1").datagrid('loadData',rows);
	//$.messager.alert('提示','申请修改成功','info');
}

//同意申请
function tongyishenqing(){
	//var rows = $('#dg1').datagrid('getRows');
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要同意申请选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				var pass = true;
				var tongren = true;
				for (var i = 0; i < rows.length; i++){
					if(rows[i].operationname==' '){
						pass = false;
						break;
					}else{
						if(rows[i].operator==shenheren){
							tongren = false;
							break;
						}else{
							deleteid = deleteid+rows[i].id+"-";
						}
					}
				}
				if(pass){
					if(tongren){
						$.ajax({
				            url:'Xtongyishenqingjrjgfz',
				            type:'POST', //GET
				            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				            //data:{"rows":JSON.stringify(rows)},
				            data:{"deletetype":"some","deleteid":deleteid},
				            dataType:'json',    
				            success:function(data){
					           	if(data == '1'){ 
					           		$.messager.alert('','同意申请成功','info',function(r){
					           			$('#dg1').datagrid('reload');
					           		});
					           	}else{  //否则删除失败
					           		$.messager.alert('操作提示','同意申请成功','info',function(r){
					           			$('#dg1').datagrid('reload');
					           		});
					           	}
				            },
				           	error : function(){
				    			$.messager.alert('操作提示','网络异常请稍后再试','error');
				    		}
				        });
					}else{
						$.messager.alert('操作提示','不能同意申请自己操作的数据','info');
					}
				}else{
					$.messager.alert('操作提示','只能同意申请操作名不为空的数据','info');
				}
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要同意申请所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'Xtongyishenqingjrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','同意申请成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','同意申请成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
	//$("#dg1").datagrid('loadData',rows);
	//$.messager.alert('提示','申请修改成功','info');
}

//同意申请删除
function tongyishenqingshanchu(){
	//var rows = $('#dg1').datagrid('getRows');
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要同意申请删除选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				var pass = true;
				var tongren = true;
				for (var i = 0; i < rows.length; i++){
					if(rows[i].operationname!='申请删除'){
						pass = false;
						break;
					}else{
						if(rows[i].operator==shenheren && shifoushenheziji == 'no'){
							tongren = false;
							break;
						}else{
							deleteid = deleteid+rows[i].id+"-";
						}
					}
				}
				if(pass){
					if(tongren){
						$.ajax({
				            url:'Xtongyishenqingshanchujrjgfz',
				            type:'POST', //GET
				            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				            //data:{"rows":JSON.stringify(rows)},
				            data:{"deletetype":"some","deleteid":deleteid},
				            dataType:'json',    
				            success:function(data){
					           	if(data == '1'){ 
					           		$.messager.alert('','同意申请删除成功','info',function(r){
					           			$('#dg1').datagrid('reload');
					           		});
					           	}else{  //否则删除失败
					           		$.messager.alert('操作提示','同意申请删除成功','info',function(r){
					           			$('#dg1').datagrid('reload');
					           		});
					           	}
				            },
				           	error : function(){
				    			$.messager.alert('操作提示','网络异常请稍后再试','error');
				    		}
				        });
					}else{
						$.messager.alert('操作提示','不能同意申请删除自己操作的数据','info');
					}
				}else{
					$.messager.alert('操作提示','请选择操作名为申请删除的数据','info');
				}
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要同意申请删除所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'Xtongyishenqingshanchujrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','同意申请删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','同意申请删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//拒绝申请
function jujueshenqing(){
	//var rows = $('#dg1').datagrid('getRows');
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要拒绝申请选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				var pass = true;
				var tongren = true;
				for (var i = 0; i < rows.length; i++){
					if(rows[i].operationname!='申请删除'){
						pass = false;
						break;
					}else{
						if(rows[i].operator==shenheren && shifoushenheziji == 'no'){
							tongren = false;
							break;
						}else{
							deleteid = deleteid+rows[i].id+"-";
						}
					}
				}
				if(pass){
					if(tongren){
						$.ajax({
				            url:'Xjujueshenqingjrjgfz',
				            type:'POST', //GET
				            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				            //data:{"rows":JSON.stringify(rows)},
				            data:{"deletetype":"some","deleteid":deleteid},
				            dataType:'json',    
				            success:function(data){
					           	if(data == '1'){ 
					           		$.messager.alert('','拒绝申请成功','info',function(r){
					           			$('#dg1').datagrid('reload');
					           		});
					           	}else{  //否则删除失败
					           		$.messager.alert('操作提示','拒绝申请成功','info',function(r){
					           			$('#dg1').datagrid('reload');
					           		});
					           	}
				            },
				           	error : function(){
				    			$.messager.alert('操作提示','网络异常请稍后再试','error');
				    		}
				        });
					}else{
						$.messager.alert('操作提示','不能拒绝申请自己操作的数据','info');
					}
				}else{
					$.messager.alert('操作提示','请选择操作名为申请删除的数据','info');
				}
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要拒绝申请所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'Xjujueshenqingjrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','拒绝申请成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','拒绝申请成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
	//$("#dg1").datagrid('loadData',rows);
	//$.messager.alert('提示','申请修改成功','info');
}

//审核通过
function shenhetongguo(){
	//var rows = $('#dg1').datagrid('getRows');
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要审核通过选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				var tongren = true;
				var caozuoming = true;
				for (var i = 0; i < rows.length; i++){
					if(rows[i].operator==shenheren && shifoushenheziji == 'no'){
						tongren = false;
						break;
					}else if(rows[i].operationname!=' '){
						caozuoming = false;
						break;
					}else{
						deleteid = deleteid+rows[i].id+"-";
					}
				}
				if(caozuoming){
					if(tongren){
						$.ajax({
				            url:'Xshenhetongguojrjgfz',
				            type:'POST', //GET
				            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				            //data:{"rows":JSON.stringify(rows)},
				            data:{"deletetype":"some","deleteid":deleteid},
				            dataType:'json',    
				            success:function(data){
					           	if(data == '1'){ 
					           		$.messager.alert('','审核通过成功','info',function(r){
					           			$('#dg1').datagrid('reload');
					           		});
					           	}else{  //否则删除失败
					           		$.messager.alert('操作提示','审核通过成功','info',function(r){
					           			$('#dg1').datagrid('reload');
					           		});
					           	}
				            },
				           	error : function(){
				    			$.messager.alert('操作提示','网络异常请稍后再试','error');
				    		}
				        });
					}else{
						$.messager.alert('操作提示','不能审核通过自己操作的数据','info');
					}
				}else{
					$.messager.alert('操作提示','请选择操作名为空的数据','info');
				}
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要审核通过所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'Xshenhetongguojrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','审核通过成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','审核通过成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
	//$("#dg1").datagrid('loadData',rows);
	//$.messager.alert('提示','申请修改成功','info');
}

//审核不通过
function shenhebutongguo(){
	//var rows = $('#dg1').datagrid('getRows');
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要审核不通过选中的数据吗',function(r){
			if(r){
				$.messager.prompt('提示', '请输入审核不通过原因', function(n){
					if (n){
						var deleteid = "";
						var tongren = true;
						var caozuoming = true;
						for (var i = 0; i < rows.length; i++){
							if(rows[i].operator==shenheren && shifoushenheziji == 'no'){
								tongren = false;
								break;
							}else if(rows[i].operationname!=' '){
								caozuoming = false;
								break;
							}else{
								deleteid = deleteid+rows[i].id+"-";
							}
						}
						if(caozuoming){
							if(tongren){
								$.ajax({
						            url:'Xshenhebutongguojrjgfz',
						            type:'POST', //GET
						            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
						            //data:{"rows":JSON.stringify(rows)},
						            data:{"deletetype":"some","deleteid":deleteid,"yuanyin":n},
						            dataType:'json',    
						            success:function(data){
							           	if(data == '1'){ 
							           		$.messager.alert('','审核不通过成功','info',function(r){
							           			$('#dg1').datagrid('reload');
							           		});
							           	}else{  //否则删除失败
							           		$.messager.alert('操作提示','审核不通过成功','info',function(r){
							           			$('#dg1').datagrid('reload');
							           		});
							           	}
						            },
						           	error : function(){
						    			$.messager.alert('操作提示','网络异常请稍后再试','error');
						    		}
						        });
							}else{
								$.messager.alert('操作提示','不能审核不通过自己操作的数据','info');
							}
						}else{
							$.messager.alert('操作提示','请选择操作名为空的数据','info');
						}
					}
				});
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要审核不通过所有数据吗',function(r){
			if(r){
				$.messager.prompt('提示', '请输入审核不通过原因', function(n){
					if(n){
						$.ajax({
				            url:'Xshenhebutongguojrjgfz',
				            type:'POST', //GET
				            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				            //data:{"rows":JSON.stringify(rows)},
				            data:{"deletetype":"all","yuanyin":n},
				            dataType:'json',    
				            success:function(data){
					           	if(data == '1'){ 
					           		$.messager.alert('','审核不通过成功','info',function(r){
					           			$('#dg1').datagrid('reload');
					           		});
					           	}else{  //否则删除失败
					           		$.messager.alert('操作提示','审核不通过成功','info',function(r){
					           			$('#dg1').datagrid('reload');
					           		});
					           	}
				            },
				           	error : function(){
				    			$.messager.alert('操作提示','网络异常请稍后再试','error');
				    		}
				        });
					}
				});
			}
		});
	}
	//$("#dg1").datagrid('loadData',rows);
	//$.messager.alert('提示','申请修改成功','info');
}

//导出
function daochu(){
    var rows = $("#dg1").datagrid('getSelections'); //获取选中行对象
    if(rows.length > 0){
        $.messager.confirm('操作提示','确认将选中的'+rows.length+'条数据导出到Excel表格吗？',function(r){
            if(r){
                var selectid = "";
                for (var i = 0; i < rows.length; i++){
                    selectid = selectid+rows[i].id+"-";
                }
                window.location.href = "Xdaochujrjgfz?zuhagntai=dsh&selectid="+selectid;
            }
        });
    }else{
        $.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
            if(r){
                window.location.href = "Xdaochujrjgfz?zuhagntai=dsh";
            }
        });
    }
}

</script>
</body>
</html>
