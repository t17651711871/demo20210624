<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>金融机构（法人）基础信息</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
<div id="jrjg_tabs" class="easyui-tabs" style="height:500px;">
	<div title="金融机构（法人）基础信息-基础情况统计表" style="padding:10px;">
		<table id="dg1" style="height: 480px;" title="金融机构（法人）基础信息-基础情况统计表"></table>     
		<!--表格工具栏-->
		<div id="tb1" style="padding: 5px; height: auto;">
		    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return" onclick="fanhui()">返回</a></br>
			<!-- 担保合同编码 ：<input type="text" id="selectdbhtbm" name="selectdbhtbm" style="height: 23px; width:200px"/>&nbsp;&nbsp;&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-search" onclick="chaxun_jc()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp; -->
			<a class="easyui-linkbutton" iconCls="icon-add" onclick="xinzeng_jc()">新增</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-edit" onclick="xiugai_jc()">修改</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-remove" onclick="shanchu_jc()">删除</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-accept" onclick="jiaoyan_jc()">校验</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-easyicon-net-16" onclick="tijiao_jc()">提交</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daoru_jc()">导入</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochu_jc()">导出</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochumoban_jc()">导出模版</a>
		</div>
	</div>
	<div title="金融机构（法人）基础信息-资产负债及风险统计表" style="padding:10px;">       
		<table id="dg2" style="height: 480px;" title="金融机构（法人）基础信息-资产负债及风险统计表"></table>
		<!--表格工具栏-->
		<div id="tb2" style="padding: 5px; height: auto;">
		    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return" onclick="fanhui()">返回</a></br>
			<!-- 担保合同编码 ：<input type="text" id="selectdbhtbm" name="selectdbhtbm" style="height: 23px; width:200px"/>&nbsp;&nbsp;&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-search" onclick="chaxun_zc()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp; -->
			<a class="easyui-linkbutton" iconCls="icon-add" onclick="xinzeng_zc()">新增</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-edit" onclick="xiugai_zc()">修改</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-remove" onclick="shanchu_zc()">删除</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-accept" onclick="jiaoyan_zc()">校验</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-easyicon-net-16" onclick="tijiao_zc()">提交</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daoru_zc()">导入</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochu_zc()">导出</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochumoban_zc()">导出模版</a>
		</div>
	</div>
	<div title="金融机构（法人）基础信息-利润及资本统计表" style="padding:10px;">
		<table id="dg3" style="height: 480px;" title="金融机构（法人）基础信息-利润及资本统计表"></table>         
		<!--表格工具栏-->
		<div id="tb3" style="padding: 5px; height: auto;">
		    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return" onclick="fanhui()">返回</a></br>
			<!-- 担保合同编码 ：<input type="text" id="selectdbhtbm" name="selectdbhtbm" style="height: 23px; width:200px"/>&nbsp;&nbsp;&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-search" onclick="chaxun_lr()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp; -->
			<a class="easyui-linkbutton" iconCls="icon-add" onclick="xinzeng_lr()">新增</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-edit" onclick="xiugai_lr()">修改</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-remove" onclick="shanchu_lr()">删除</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-accept" onclick="jiaoyan_lr()">校验</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-easyicon-net-16" onclick="tijiao_lr()">提交</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daoru_lr()">导入</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochu_lr()">导出</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochumoban_lr()">导出模版</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="importIJ()">导入IJ文件</a>
		</div>
	</div>
</div>
	
<!-- 导入文件dialog -->
    <div style="visibility: hidden;">
        <div id="importExcel1" class="easyui-dialog" style="width: 600px; height: 160px; padding-left: 30px" title="导入数据">
            文件导入:<input type="file" accept=".xls,.xlsx" name="excelfile" id="excelfile1" onchange="inexcel1()"/><br>
        <input type="button" id="importbtn1" value="提交" style="width:50px;margin-top:20px;" onclick="excel1()"/>
        </div>
    </div>	
<!-- 导入文件dialog -->
    <div style="visibility: hidden;">
        <div id="importExcel2" class="easyui-dialog" style="width: 600px; height: 160px; padding-left: 30px" title="导入数据">
            文件导入:<input type="file" accept=".xls,.xlsx" name="excelfile" id="excelfile2" onchange="inexcel2()"/><br>
        <input type="button" id="importbtn2" value="提交" style="width:50px;margin-top:20px;" onclick="excel2()"/>
        </div>
    </div>
<!-- 导入文件dialog -->
    <div style="visibility: hidden;">
        <div id="importExcel3" class="easyui-dialog" style="width: 600px; height: 160px; padding-left: 30px" title="导入数据">
            文件导入:<input type="file" accept=".xls,.xlsx" name="excelfile" id="excelfile3" onchange="inexcel3()"/><br>
        <input type="button" id="importbtn3" value="提交" style="width:50px;margin-top:20px;" onclick="excel3()"/>
        </div>
    </div>
    <!--导入IJ文件-->
<div style="visibility: hidden;">
    <div id="importIJ" class="easyui-dialog" style="width: 600px; height: 150px;top: 100px;padding: 20px;" title="IJ文件导入" data-options="modal:true,closed:true">
        <label>文件导入:</label><input type="file" accept=".zip" name="IJfile" id="IJfile" onchange="inIJ()"/><br>
        <input type="button" id="importIJbtn" value="提交" style="width:50px;margin-top:20px;" onclick="SubmitIJ()"/>
    </div>
</div>	
<!-- 导出进度条 -->
<div style="visibility: hidden;">
  <div id="exportFileDialog" class="easyui-dialog" style="width:550px;height:100px;padding-left: 10px;top:200px;" title="导出数据"> 
	<div align="center"><span id="daorutishi">请您稍等,数据正在导出中......</span></div>
	<div id="exportFile" class="easyui-progressbar" style="width:500px;heigth:50px;"></div>
  </div>
</div>
	
<!-- 金融机构（法人）基础信息-基础情况统计表 -->	
<div style="margin-top:100px;visibility: hidden;">
    <div id="dialog1" class="easyui-dialog" title=""  data-options="iconCls:'icon-save',toolbar:'#tbFordialog1'" style="width:900px;height:650px;">
          <div id="tbFordialog1" style="padding-left: 30px;padding-top: 10px">
	           <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" id="queding_jc" onclick="queding_jc()">确定</a>&nbsp;&nbsp;
               <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="quxiao_jc()">取消</a>
          </div>
          
         <form id="formFordialog1" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>金融机构名称 :</td>
                 <td><input class="easyui-validatebox" id="finorgname" name="finorgname" style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项"/></td>
                
                 <td>金融机构代码:</td>
                 <td><input class="easyui-validatebox" id="finorgcode" name="finorgcode" style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项"/></td>
              </tr>
                
               <tr> 
                 <td>金融机构编码 :</td>
                 <td><input class="easyui-validatebox" id="finorgnum" name="finorgnum" style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项"/></td>
			   
			     <td>机构类别 :</td>
			     <td>
					<select class="easyui-combobox" id="jglb" name="jglb" editable=false style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项">
						<option value="A01">A01:中国人民银行</option>
						<option value="A02">A02:国家外汇管理局</option>
						<option value="B01">B01:中国银行保险监督管理委员会</option>
						<option value="B02">B02:中国证券监督管理委员会</option>
						<option value="C01">C01:开发性金融机构及政策性银行</option>
						<option value="C02">C02:国有商业银行</option>
						<option value="C03">C03:股份制商业银行</option>
						<option value="C04">C04:城市商业银行</option>
						<option value="C05">C05:农村商业银行</option>
						<option value="C06">C06:农村合作银行</option>
						<option value="C07">C07:村镇银行</option>
						<option value="C08">C08:农村信用社</option>
						<option value="C09">C09:城市信用合作社</option>
						<option value="C10">C10:农村资金互助社</option>
						<option value="C11">C11:财务公司</option>
						<option value="C12">C12:外资银行</option>
						<option value="D01">D01:信托公司</option>
						<option value="D02">D02:金融资产管理公司</option>
						<option value="D03">D03:金融租赁公司</option>
						<option value="D04">D04:汽车金融公司</option>
						<option value="D05">D05:贷款公司</option>
						<option value="D06">D06:货币经纪公司</option>
						<option value="D07">D07:消费金融公司</option>
						<option value="D08">D08:其他</option>
						<option value="E">E:证券业金融机构</option>
						<option value="F">F:保险业金融机构</option>
						<option value="G">G:交易及结算类金融机构</option>
						<option value="H">H:金融控股公司</option>
						<option value="Z">Z:其他</option>
	                </select>
				 </td>
			   </tr>
			   
			   <tr>
			   	 <td>注册地址 :</td>
			     <td><input class="easyui-validatebox" id="regaddress" name="regaddress" style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项"/></td>
			     
			     <td>地区代码 :</td>
			     <td>
			     	<input class="easyui-combotree" id="regarea" name="regarea" style="height: 23px; width:230px" editable=false data-options="required:true,url:'XgetAdmindivideJson',valueField:'id',textField:'text',editable:false,value:''" missingMessage="必填项"/>
			     </td>
               </tr> 
			   
			   <tr>
			   	 <td>注册资本 :</td>
                 <td><input class="easyui-validatebox" id="regamt" name="regamt" style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项"/></td>
                 
			     <td>成立日期 :</td>
			     <td><input class="easyui-datebox" id="setupdate" name="setupdate" style="height: 23px; width:230px" editable=true data-options="required:true,validType:['dateFormat','normalDate']" missingMessage="必填项"/></td>
               </tr>
               
               <tr>
			   	 <td>联系人 :</td>
                 <td><input class="easyui-validatebox" id="lxr" name="lxr" style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项"/></td>
                 
			     <td>联系电话:</td>
                 <td><input class="easyui-validatebox"  id="phone" name="phone" style="height: 23px; width:230px;" data-options= "required:true,validType:['length']" missingMessage="必填项"/></td>
               </tr>
			   
			   <tr>
			     <td>经营状态:</td>
                 <td>
                 <select class="easyui-combobox" id="orgmanagestatus" name="orgmanagestatus" editable=false style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项">
						<option value="01">01:正常运营</option>
						<option value="02">02:停业（歇业）</option>
						<option value="03">03:筹建</option>
						<option value="04">04:当年关闭</option>
						<option value="05">05:当年破产</option>
						<option value="06">06:当年注销</option>
						<option value="07">07:当年吊销</option>
						<option value="99">99:其他</option>
	                </select>
                 </td>
               
			     <td>出资人经济成分:</td>
                 <td>
                 <select class="easyui-combobox" id="orgstoreconomy" name="orgstoreconomy" editable=false style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项">
						<option value="A01">A01:国有控股</option>
						<option value="A0101">A0101:国有相对控股</option>
						<option value="A0102">A0102:国有绝对控股</option>
						<option value="A02">A02:集体控股</option>
						<option value="A0201">A0201:集体相对控股</option>
						<option value="A0202">A0202:集体绝对控股</option>
						<option value="B01">B01:私人控股</option>
						<option value="B0101">B0101:私人相对控股</option>
						<option value="B0102">B0102:私人绝对控股</option>
						<option value="B02">B02:港澳台控股</option>
						<option value="B0201">B0201:港澳台相对控股</option>
						<option value="B0202">B0202:港澳台绝对控股</option>
						<option value="B03">B03:外商控股</option>
						<option value="B0301">B0301:外商相对控股</option>
						<option value="B0302">B0302:外商绝对控股</option>
	                </select>
                 </td>
               </tr>
			   
			   <tr>
				   <td>数据日期 :</td>
				   <td><input class="easyui-datebox" id="sjrq_a1" name="sjrq" style="height: 23px; width:230px" editable=true data-options="required:true,validType:['dateFormat','normalDate']" missingMessage="必填项"/></td>
			   
			     <td>企业规模:</td>
                 <td>
                 <select class="easyui-combobox" id="invermodel" name="invermodel" required=true editable=false style="height: 23px; width:230px">
						<option value="01">01:大型</option>
						<option value="02">02:中型</option>
						<option value="03">03:小型</option>
						<option value="04">04:微型</option>
	                </select>
                 </td>
               </tr>
			   
			   <tr>
			     <td>实际控制人名称 :</td>
			     <td><input class="easyui-validatebox" id="actctrlname" name="actctrlname" style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项"/></td>
			     
			     <td>实际控制人证件类型:</td>
                 <td>                                                                                                                                                             
					<input class="easyui-validatebox" id='actctrlidtype' name='actctrlidtype' style="height: 23px; width:230px" data-options="required:true,readonly:true" missingMessage="必填项"/><a class='easyui-linkbutton'' iconCls='icon-add' onclick='addactctrlidtype()'></a>
				 </td>
			   </tr>
			   
			   <tr>
			     <td>实际控制人证件代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="actctrlcode" name="actctrlcode" style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项"/>
			     </td>
			   
			     <td>从业人员数 :</td>
			     <td>
			        <input class="easyui-validatebox" id="personnum" name="personnum" data-options="required:true, validType:['lengtha']" missingMessage="必填项" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第一大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="onestockcode" name="onestockcode" style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项"/>
			     </td>
			   
			     <td>第一大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="onestockprop" name="onestockprop" style="height: 23px; width:230px" data-options="required:true,precision:2 ,validType:['rate','ratelevel']" missingMessage="必填项"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第二大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="twostockcode" name="twostockcode" data-options="validType:['rateTwo']" style="height: 23px; width:230px"/>
			     </td>
			   
			     <td>第二大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="twostockprop" name="twostockprop" data-options="precision:2,validType:['rate','ratelevel']" data- style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第三大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="threestockcode" name="threestockcode"  style="height: 23px; width:230px"/>
			     </td>
			   
			     <td>第三大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="threestockprop" name="threestockprop"data-options="precision:2,validType:['rate','ratelevel']" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第四大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="fourstockcode" name="fourstockcode" style="height: 23px; width:230px"/>
			     </td>
			   
			     <td>第四大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="fourstockprop" name="fourstockprop" data-options="precision:2,validType:['rate','ratelevel']" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第五大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="fivestockcode" name="fivestockcode" style="height: 23px; width:230px"/>
			     </td>
			   
			     <td>第五大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="fivestockprop" name="fivestockprop" data-options="precision:2,validType:['rate','ratelevel']" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第六大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="sixstockcode" name="sixstockcode" style="height: 23px; width:230px"/>
			     </td>
			   
			     <td>第六大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="sixstockprop" name="sixstockprop" data-options="precision:2,validType:['rate','ratelevel']" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第七大股东代码:</td>
			     <td>
			        <input class="easyui-validatebox" id="sevenstockcode" name="sevenstockcode" style="height: 23px; width:230px"/>
			     </td>
			   
			     <td>第七大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="sevenstockprop" name="sevenstockprop" data-options="precision:2,validType:['rate','ratelevel']" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第八大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="eightstockcode" name="eightstockcode" style="height: 23px; width:230px"/>
			     </td>
			   
			     <td>第八大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="eightstockprop" name="eightstockprop" data-options="precision:2,validType:['rate','ratelevel']" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第九大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="ninestockcode" name="ninestockcode" style="height: 23px; width:230px"/>
			     </td>
			   
			     <td>第九大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="ninestockprop" name="ninestockprop" data-options="precision:2,validType:['rate','ratelevel']" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第十大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="tenstockcode" name="tenstockcode" style="height: 23px; width:230px"/>
			     </td>
			   
			     <td>第十大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="tenstockprop" name="tenstockprop" data-options="precision:2,validType:['rate','ratelevel']" style="height: 23px; width:230px"/>
			        <input type="hidden" id="id_jc" name="id">
			        <input type="hidden" id="orgid_jc" name="orgid">
			        <input type="hidden" id="departid_jc" name="departid">
			        <input type="hidden" id="checkstatus_jc" name="checkstatus">
			        <input type="hidden" id="datastatus_jc" name="datastatus">
			        <input type="hidden" id="operator_jc" name="operator">
			        <input type="hidden" id="operationname_jc" name="operationname">
			        <input type="hidden" id="operationtime_jc" name="operationtime">
			        <input type="hidden" id="nopassreason_jc" name="nopassreason">
			     </td>
			   </tr>
			   
              </table>
           </div>
         </form>
	</div>
</div>
<!-- 金融机构（法人）基础信息-资产负债及风险统计表 -->
<div style="visibility: hidden;">
    <div id="dialog2" class="easyui-dialog" title=""  data-options="iconCls:'icon-save',toolbar:'#tbFordialog2'" style="width:520px;height:520px;">
          <div id="tbFordialog2" style="padding-left: 30px;padding-top: 10px">
	           <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" id="queding_zc" onclick="queding_zc()">确定</a>&nbsp;&nbsp;
               <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="quxiao_zc()">取消</a>
          </div>
          
         <form id="formFordialog2" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>各项存款 :</td>
                 <td><input class="easyui-validatebox easyui-numberbox" id="gxck" data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项" name="gxck" style="height: 23px; width:230px"/></td>
                </tr> 
                 
               <tr>
                 <td>各项贷款:</td>
                 <td><input class="easyui-validatebox easyui-numberbox" id="gxdk" name="gxdk" data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项" style="height: 23px; width:230px"/></td>
              </tr>
                
               <tr> 
                 <td>资产总计 :</td>
                 <td><input class="easyui-validatebox easyui-numberbox" id="zczj" name="zczj" data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项"  style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>  
                 <td>负债总计 :</td>
                 <td><input class="easyui-validatebox easyui-numberbox" id="fzzj" name="fzzj" data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项" style="height: 23px; width:230px"/></td>
               </tr> 
                
               <tr>  
                 <td>所有者权益合计 :</td>
                 <td><input class="easyui-validatebox easyui-numberbox" id="syzqyhj" name="syzqyhj" data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项" style="height: 23px; width:230px"/></td>
               </tr>
			   
			   <tr>  
                 <td>生息资产 :</td>
                 <td><input class="easyui-validatebox easyui-numberbox" id="sxzc" name="sxzc" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项" style="height: 23px; width:230px"/></td>
               </tr>
			   
			   <tr>
			     <td>付息负债 :</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="fxzc" name="fxzc"  data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>流动性资产 :</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="ldxzc" name="ldxzc" data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>流动性负债 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="ldxfz" name="ldxfz" data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>正常类贷款 :</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="zcldk" name="zcldk" data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>关注类贷款 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="gzldk" name="gzldk"  data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>次级类贷款 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="cjldk" name="cjldk" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项"style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>可疑类贷款 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="jyldk" name="jyldk" data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>损失类贷款 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="ssldk" name="ssldk" data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>逾期贷款 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="yqdk" name="yqdk" data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>逾期90天以上贷款 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="yqninetytysdk" name="yqninetytysdk" data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项" style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>贷款减值准备 :</td>
			     <td>
			        <input class="easyui-validatebox easyui-numberbox" id="dkjzzb" name="dkjzzb" data-options="required:true,precision:2,validType:['xiaoshu']"missingMessage="必填项"  style="height: 23px; width:230px"/>
					 <input type="hidden" id="id_zc" name="id">
					 <input type="hidden" id="orgid_zc" name="orgid">
					 <input type="hidden" id="departid_zc" name="departid">
					 <input type="hidden" id="checkstatus_zc" name="checkstatus">
					 <input type="hidden" id="datastatus_zc" name="datastatus">
					 <input type="hidden" id="operator_zc" name="operator">
					 <input type="hidden" id="operationname_zc" name="operationname">
					 <input type="hidden" id="operationtime_zc" name="operationtime">
					 <input type="hidden" id="nopassreason_zc" name="nopassreason">
			     </td>
			   </tr>
			   
			   <tr>
			   	<td>数据日期 :</td>
				   <td><input class="easyui-datebox" id="sjrq_a2" name="sjrq" style="height: 23px; width:230px" editable=true data-options="required:true" missingMessage="必填项"/></td>
			   </tr>
              </table>
           </div>
         </form>
	</div>
</div>
<!-- 金融机构（法人）基础信息-利润及资本统计表 -->
<div style="visibility: hidden;">
    <div id="dialog3" class="easyui-dialog" title=""  data-options="iconCls:'icon-save',toolbar:'#tbFordialog3'" style="width:520px;height:520px;">
          <div id="tbFordialog3" style="padding-left: 30px;padding-top: 10px">
	           <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" id="queding" onclick="queding_lr()">确定</a>&nbsp;&nbsp;
               <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="quxiao_lr()">取消</a>
          </div>
          
         <form id="formFordialog3" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>营业收入 :</td>
                 <td><input class="easyui-validatebox easyui-numberbox" id="yysr" name="yysr" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项" style="height: 23px; width:230px"/></td>
                </tr> 
                 
               <tr>
                 <td>利息净收入 :</td>
                 <td><input class="easyui-validatebox easyui-numberbox" id="lxjsr" name="lxjsr" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项" style="height: 23px; width:230px"/></td>
              </tr>
                
               <tr> 
                 <td>利息收入 :</td>
                 <td><input class="easyui-validatebox" id="lxsr" name="lxsr" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>  
                 <td>金融机构往来利息收入:</td>
                 <td><input class="easyui-validatebox" id="jrjgwllxsr" name="jrjgwllxsr" required=true style="height: 23px; width:230px"/></td>
               </tr> 
			   
               <tr>  
                 <td>其中：系统内往来利息收入:</td>
                 <td><input class="easyui-validatebox" id="xtnwllxsr" name="xtnwllxsr" required=true style="height: 23px; width:230px"/></td>
               </tr>
			   
			   <tr>  
                 <td>各项贷款利息收入 :</td>
                 <td><input class="easyui-validatebox" id="gxdklxsr" name="gxdklxsr" required=true style="height: 23px; width:230px"/></td>
               </tr>
			   
			   <tr>
			     <td>债券利息收入 :</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="zqlxsr" name="zqlxsr"  data-options="required:true,precision:2,validType:['xiaoshu']" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他利息收入 :</td>
			     <td><input class="easyui-validatebox" id="qtlxsr" name="qtlxsr" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>利息支出 :</td>
			     <td>
			        <input class="easyui-validatebox" id="lxzc" name="lxzc" required=true style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>金融机构往来利息支出 :</td>
			     <td><input class="easyui-validatebox" id="jrjgwllxzc" name="jrjgwllxzc" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>其中：系统内往来利息支出 :</td>
			     <td>
			        <input class="easyui-validatebox" id="xtnwllxzc" name="xtnwllxzc" required=true style="height: 23px; width:230px"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>各项存款利息支出:</td>
			     <td><input class="easyui-validatebox" id="gxcklxzc" name="gxcklxzc" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>债券利息支出:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="zqlxzc" name="zqlxzc" required=true data-options="required:true,precision:2,validType:['xiaoshu']"  style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他利息支出:</td>
			     <td><input class="easyui-validatebox" id="qtlxzc" name="qtlxzc" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>手续费及佣金净收入:</td>
			     <td><input class="easyui-validatebox" id="sxfjyjjsr" name="sxfjyjjsr" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>手续费及佣金收入:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="sxfjyjsr" name="sxfjyjsr" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项"  style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>手续费及佣金支出:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="jxfjyjzc" name="jxfjyjzc" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>租赁收益:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="zlsy" name="zlsy" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>投资收益:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="tzsy" name="tzsy"  data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>债券投资收益:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="zqtzsy" name="zqtzsy" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>股权投资收益:</td>
			     <td><input class="easyui-validatebox" id="gqtzsy" name="gqtzsy" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他投资收益:</td>
			     <td><input class="easyui-validatebox" id="qttzsy" name="qttzsy" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>公允价值变动收益:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="gyjzbdsy" name="gyjzbdsy" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>汇兑净收益:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="hdjsy" name="hdjsy" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>资产处置收益:</td>
			     <td><input class="easyui-validatebox" id="zcczsy" name="zcczsy" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他业务收入:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="qtywsr" name="qtywsr" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业支出:</td>
			     <td><input class="easyui-validatebox" id="yyzc" name="yyzc" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>业务及管理费:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="ywjglf" name="ywjglf" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>其中:职工工资:</td>
			     <td><input class="easyui-validatebox" id="zggz" name="zggz" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>福利费:</td>
			     <td><input class="easyui-validatebox" id="flf" name="flf" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>住房公积金和住房补贴:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="zfgjjhzfbt" name="zfgjjhzfbt" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项"  style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>税金及附加:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="sjjfj" name="sjjfj" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项"  style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>资产减值损失:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="zcjzss" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项" name="zcjzss" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他业务支出:</td>
			     <td><input class="easyui-validatebox" id="qtywzc" name="qtywzc" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业利润:</td>
			     <td><input class="easyui-validatebox" id="yylr" name="yylr" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业外收入（加）:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="yywsr" name="yywsr" required=true data-options="precision:2,validType:['xiaoshu']"  style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业外支出（减）:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="yywzc" name="yywzc" required=true data-options="precision:2,validType:['xiaoshu']" style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>利润总额:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="lrze" name="lrze" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项"  style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>所得税（减）:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="sds" name="sds" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项"   style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>净利润:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="jlr" name="jlr" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项"  style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>年度损益调整（加）:</td>
			     <td><input class="easyui-validatebox" id="ndsytz" name="ndsytz" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>留存利润:</td>
			     <td><input class="easyui-validatebox" id="lclr" name="lclr" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>未分配利润:</td>
			     <td><input class="easyui-validatebox" id="wfplr" name="wfplr" required=true style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>应纳增值税:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="ynzzs" name="ynzzs" data-options="required:true,precision:2,validType:['xiaoshu']" missingMessage="必填项"  style="height: 23px; width:230px"/></td>
			   </tr>
			   
			   <tr>
			     <td>核心一级资本净额:</td>
			     <td><input class="easyui-validatebox" id="hxyjzbje" name="hxyjzbje" style="height: 23px; width:230px" /></td>
			   </tr>
			   
			   <tr>
			     <td>一级资本净额:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="yjzbje" name="yjzbje" style="height: 23px; width:230px" data-options="precision:2,validType:['xiaoshu']"/></td>
			   </tr>
			   
			   <tr>
			     <td>资本净额:</td>
			     <td><input class="easyui-validatebox easyui-numberbox" id="zbje" name="zbje" style="height: 23px; width:230px" data-options="precision:2,validType:['xiaoshu']"/></td>
			   </tr>
			   <tr>
			     <td>应用资本底线及校准后的风险加权资产合计 :</td>
			     <td>
			        <input class="easyui-validatebox" id="ygzbjfxjqzchj" name="ygzbjfxjqzchj" style="height: 23px; width:230px"/>
			        <input type="hidden" id="id_lr" name="id">
			        <input type="hidden" id="orgid_lr" name="orgid">
			        <input type="hidden" id="departid_lr" name="departid">
			        <input type="hidden" id="checkstatus_lr" name="checkstatus">
			        <input type="hidden" id="datastatus_lr" name="datastatus">
			        <input type="hidden" id="operator_lr" name="operator">
			        <input type="hidden" id="operationname_lr" name="operationname">
			        <input type="hidden" id=operationtime_lr name="operationtime">
			        <input type="hidden" id="nopassreason_lr" name="nopassreason">
			     </td>
			   </tr>
			   
			   <tr>
			   	<td>数据日期 :</td>
				   <td><input class="easyui-datebox" id="sjrq_a3" name="sjrq" style="height: 23px; width:230px" editable=true data-options="required:true" missingMessage="必填项"/></td>
			   </tr>
              </table>
           </div>
         </form>
	</div>
</div>

<!-- 金融机构（法人）基础信息-基础情况统计表 详情 -->	
<div style="visibility: hidden;">
    <div id="dialog_m1" class="easyui-dialog" title="金融机构（法人）基础信息-基础情况统计表"  data-options="iconCls:'icon-more'" style="width:900px;height:620px;">
          
         <form id="formForMore1" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>金融机构名称 :</td>
                 <td><input class="easyui-validatebox" id="finorgname_m" name="finorgname" style="height: 23px; width:230px" disabled="disabled"/></td>
                
                 <td>金融机构代码:</td>
                 <td><input class="easyui-validatebox" id="finorgcode_m" name="finorgcode" style="height: 23px; width:230px" disabled="disabled"/></td>
              </tr>
                
               <tr> 
                 <td>金融机构编码 :</td>
                 <td><input class="easyui-validatebox" id="finorgnum_m" name="finorgnum" style="height: 23px; width:230px" disabled="disabled"/></td>
			   
			   	 <td>机构类别 :</td>
			     <td>
					<select class="easyui-combobox" id="jglb_m" name="jglb" style="height: 23px; width:230px" disabled="disabled">
						<option value="A01">A01:中国人民银行</option>
						<option value="A02">A02:国家外汇管理局</option>
						<option value="B01">B01:中国银行保险监督管理委员会</option>
						<option value="B02">B02:中国证券监督管理委员会</option>
						<option value="C01">C01:开发性金融机构及政策性银行</option>
						<option value="C02">C02:国有商业银行</option>
						<option value="C03">C03:股份制商业银行</option>
						<option value="C04">C04:城市商业银行</option>
						<option value="C05">C05:农村商业银行</option>
						<option value="C06">C06:农村合作银行</option>
						<option value="C07">C07:村镇银行</option>
						<option value="C08">C08:农村信用社</option>
						<option value="C09">C09:城市信用合作社</option>
						<option value="C10">C10:农村资金互助社</option>
						<option value="C11">C11:财务公司</option>
						<option value="C12">C12:外资银行</option>
						<option value="D01">D01:信托公司</option>
						<option value="D02">D02:金融资产管理公司</option>
						<option value="D03">D03:金融租赁公司</option>
						<option value="D04">D04:汽车金融公司</option>
						<option value="D05">D05:贷款公司</option>
						<option value="D06">D06:货币经纪公司</option>
						<option value="D07">D07:消费金融公司</option>
						<option value="D08">D08:其他</option>
						<option value="E">E:证券业金融机构</option>
						<option value="F">F:保险业金融机构</option>
						<option value="G">G:交易及结算类金融机构</option>
						<option value="H">H:金融控股公司</option>
						<option value="Z">Z:其他</option>
	                </select>
				 </td>
			   </tr>
			   
			   <tr>
			   	 <td>注册地址 :</td>
			     <td><input class="easyui-validatebox" id="regaddress_m" name="regaddress" style="height: 23px; width:230px" disabled="disabled"/></td>
			     
			     <td>地区代码 :</td>
			     <td>
			        <input class="easyui-combotree" id="regarea_m" name="regarea" style="height: 23px; width:230px" data-options="valueField:'id',textField:'text'" disabled="disabled" data-options="url:'XgetAdmindivideJson',valueField:'id',textField:'text',editable:false,value:''" missingMessage="必填项"/>
			     </td>
               </tr> 
			   
			   <tr>
			   	 <td>注册资本 :</td>
                 <td><input class="easyui-validatebox" id="regamt_m" name="regamt" style="height: 23px; width:230px" disabled="disabled"/></td>
                 
			     <td>成立日期 :</td>
			     <td><input class="easyui-datebox" id="setupdate_m" editable=true name="setupdate" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
               
               <tr>
			     <td>联系人 :</td>
                 <td><input class="easyui-validatebox" id="lxr_m" name="lxr" style="height: 23px; width:230px" disabled="disabled"/></td>
			     
                 <td>联系电话:</td>
                 <td><input class="easyui-validatebox" id="phone_m" name="phone" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>
			     <td>经营状态:</td>
                 <td>
                 <select class="easyui-combobox" id="orgmanagestatus_m" name="orgmanagestatus" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:正常运营</option>
						<option value="02">02:停业（歇业）</option>
						<option value="03">03:筹建</option>
						<option value="04">04:当年关闭</option>
						<option value="05">05:当年破产</option>
						<option value="06">06:当年注销</option>
						<option value="07">07:当年吊销</option>
						<option value="99">99:其他</option>
	                </select>
                 </td>
               
			     <td>出资人经济成分:</td>
                 <td>
                 <select class="easyui-combobox" id="orgstoreconomy_m" name="orgstoreconomy" style="height: 23px; width:230px" disabled="disabled">
						<option value="A01">A01:国有控股</option>
						<option value="A0101">A0101:国有相对控股</option>
						<option value="A0102">A0102:国有绝对控股</option>
						<option value="A02">A02:集体控股</option>
						<option value="A0201">A0201:集体相对控股</option>
						<option value="A0202">A0202:集体绝对控股</option>
						<option value="B01">B01:私人控股</option>
						<option value="B0101">B0101:私人相对控股</option>
						<option value="B0102">B0102:私人绝对控股</option>
						<option value="B02">B02:港澳台控股</option>
						<option value="B0201">B0201:港澳台相对控股</option>
						<option value="B0202">B0202:港澳台绝对控股</option>
						<option value="B03">B03:外商控股</option>
						<option value="B0301">B0301:外商相对控股</option>
						<option value="B0302">B0302:外商绝对控股</option>
	                </select>
                 </td>
               </tr>
			   
			   <tr>
				   <td>数据日期 :</td>
				   <td><input class="easyui-datebox" id="sjrq_m1" name="sjrq" style="height: 23px; width:230px" editable=true data-options="required:true" disabled="disabled"/></td>
			   
			     <td>企业规模:</td>
                 <td>
                 <select class="easyui-combobox" id="invermodel_m" name="invermodel" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:大型</option>
						<option value="02">02:中型</option>
						<option value="03">03:小型</option>
						<option value="04">04:微型</option>
	                </select>
                 </td>
               </tr>
			   
			   <tr>
			     <td>实际控制人名称 :</td>
			     <td><input class="easyui-validatebox" id="actctrlname_m" name="actctrlname" style="height: 23px; width:230px" disabled="disabled"/></td>
			     
			     <td>实际控制人证件类型:</td>
                 <td>                                                                                                                                                             
					<input class="easyui-validatebox" id='actctrlidtype_m' name='actctrlidtype' style="height: 23px; width:230px" disabled="disabled"/>
				 </td>
			   </tr>
			   
			   <tr>
			     <td>实际控制人证件代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="actctrlcode_m" name="actctrlcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>从业人员数 :</td>
			     <td>
			        <input class="easyui-validatebox" id="personnum_m" name="personnum" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第一大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="onestockcode_m" name="onestockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第一大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="onestockprop_m" name="onestockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第二大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="twostockcode_m" name="twostockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第二大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="twostockprop_m" name="twostockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第三大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="threestockcode_m" name="threestockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第三大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="threestockprop_m" name="threestockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第四大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="fourstockcode_m" name="fourstockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第四大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="fourstockprop_m" name="fourstockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第五大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="fivestockcode_m" name="fivestockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第五大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="fivestockprop_m" name="fivestockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第六大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="sixstockcode_m" name="sixstockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第六大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="sixstockprop_m" name="sixstockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第七大股东代码:</td>
			     <td>
			        <input class="easyui-validatebox" id="sevenstockcode_m" name="sevenstockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第七大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="sevenstockprop_m" name="sevenstockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第八大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="eightstockcode_m" name="eightstockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第八大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="eightstockprop_m" name="eightstockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第九大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="ninestockcode_m" name="ninestockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第九大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="ninestockprop_m" name="ninestockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第十大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="tenstockcode_m" name="tenstockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第十大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="tenstockprop_m" name="tenstockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
              </table>
           </div>
         </form>
	</div>
</div>
<!-- 金融机构（法人）基础信息-资产负债及风险统计表  详情-->
<div style="visibility: hidden;">
    <div id="dialog_m2" class="easyui-dialog" title="金融机构（法人）基础信息-资产负债及风险统计表"  data-options="iconCls:'icon-more'" style="width:520px;height:500px;">
          
         <form id="formForMore2" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>各项存款 :</td>
                 <td><input class="easyui-validatebox" id="gxck_m" name="gxck" style="height: 23px; width:230px" disabled="disabled"/></td>
                </tr> 
                 
               <tr>
                 <td>各项贷款:</td>
                 <td><input class="easyui-validatebox" id="gxdk_m" name="gxdk" style="height: 23px; width:230px" disabled="disabled"/></td>
              </tr>
                
               <tr> 
                 <td>资产总计 :</td>
                 <td><input class="easyui-validatebox" id="zczj_m" name="zczj" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>  
                 <td>负债总计 :</td>
                 <td><input class="easyui-validatebox" id="fzzj_m" name="fzzj" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
                
               <tr>  
                 <td>所有者权益合计 :</td>
                 <td><input class="easyui-validatebox" id="syzqyhj_m" name="syzqyhj" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>  
                 <td>生息资产 :</td>
                 <td><input class="easyui-validatebox" id="sxzc_m" name="sxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>
			     <td>付息负债 :</td>
			     <td><input class="easyui-validatebox" id="fxzc_m" name="fxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>流动性资产 :</td>
			     <td><input class="easyui-validatebox" id="ldxzc_m" name="ldxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>流动性负债 :</td>
			     <td>
			        <input class="easyui-validatebox" id="ldxfz_m" name="ldxfz" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>正常类贷款 :</td>
			     <td><input class="easyui-validatebox" id="zcldk_m" name="zcldk" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>关注类贷款 :</td>
			     <td>
			        <input class="easyui-validatebox" id="gzldk_m" name="gzldk" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>次级类贷款 :</td>
			     <td>
			        <input class="easyui-validatebox" id="cjldk_m" name="cjldk" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>可疑类贷款 :</td>
			     <td>
			        <input class="easyui-validatebox" id="jyldk_m" name="jyldk" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>损失类贷款 :</td>
			     <td>
			        <input class="easyui-validatebox" id="ssldk_m" name="ssldk" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>逾期贷款 :</td>
			     <td>
			        <input class="easyui-validatebox" id="yqdk_m" name="yqdk" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>逾期90天以上贷款 :</td>
			     <td>
			        <input class="easyui-validatebox" id="yqninetytysdk_m" name="yqninetytysdk" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>贷款减值准备 :</td>
			     <td>
			        <input class="easyui-validatebox" id="dkjzzb_m" name="dkjzzb" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			   <td>数据日期 :</td>
				   <td><input id="sjrq_m2" name="sjrq" style="height: 23px; width:230px" editable=true data-options="required:true" disabled="disabled"/></td>
			   </tr>
              </table>
           </div>
         </form>
	</div>
</div>
<!-- 金融机构（法人）基础信息-利润及资本统计表  详情-->
<div style="visibility: hidden;">
    <div id="dialog_m3" class="easyui-dialog" title="金融机构（法人）基础信息-利润及资本统计表"  data-options="iconCls:'icon-more'" style="width:520px;height:500px;">
          
         <form id="formForMore3" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>营业收入 :</td>
                 <td><input class="easyui-validatebox" id="yysr_m" name="yysr" style="height: 23px; width:230px" disabled="disabled"/></td>
                </tr> 
                 
               <tr>
                 <td>利息净收入 :</td>
                 <td><input class="easyui-validatebox" id="lxjsr_m" name="lxjsr" style="height: 23px; width:230px" disabled="disabled"/></td>
              </tr>
                
               <tr> 
                 <td>利息收入 :</td>
                 <td><input class="easyui-validatebox" id="lxsr_m" name="lxsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>  
                 <td>金融机构往来利息收入:</td>
                 <td><input class="easyui-validatebox" id="jrjgwllxsr_m" name="jrjgwllxsr" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
			   
               <tr>  
                 <td>其中：系统内往来利息收入:</td>
                 <td><input class="easyui-validatebox" id="xtnwllxsr_m" name="xtnwllxsr" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>  
                 <td>各项贷款利息收入 :</td>
                 <td><input class="easyui-validatebox" id="gxdklxsr_m" name="gxdklxsr" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>
			     <td>债券利息收入 :</td>
			     <td><input class="easyui-validatebox" id="zqlxsr_m" name="zqlxsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他利息收入 :</td>
			     <td><input class="easyui-validatebox" id="qtlxsr_m" name="qtlxsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>利息支出 :</td>
			     <td>
			        <input class="easyui-validatebox" id="lxzc_m" name="lxzc" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>金融机构往来利息支出 :</td>
			     <td><input class="easyui-validatebox" id="jrjgwllxzc_m" name="jrjgwllxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其中：系统内往来利息支出 :</td>
			     <td>
			        <input class="easyui-validatebox" id="xtnwllxzc_m" name="xtnwllxzc" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>各项存款利息支出:</td>
			     <td><input class="easyui-validatebox" id="gxcklxzc_m" name="gxcklxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>债券利息支出:</td>
			     <td><input class="easyui-validatebox" id="zqlxzc_m" name="zqlxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他利息支出:</td>
			     <td><input class="easyui-validatebox" id="qtlxzc_m" name="qtlxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>手续费及佣金净收入:</td>
			     <td><input class="easyui-validatebox" id="sxfjyjjsr_m" name="sxfjyjjsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>手续费及佣金收入:</td>
			     <td><input class="easyui-validatebox" id="sxfjyjsr_m" name="sxfjyjsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>手续费及佣金支出:</td>
			     <td><input class="easyui-validatebox" id="jxfjyjzc_m" name="jxfjyjzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>租赁收益:</td>
			     <td><input class="easyui-validatebox" id="zlsy_m" name="zlsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>投资收益:</td>
			     <td><input class="easyui-validatebox" id="tzsy_m" name="tzsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>债券投资收益:</td>
			     <td><input class="easyui-validatebox" id="zqtzsy_m" name="zqtzsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>股权投资收益:</td>
			     <td><input class="easyui-validatebox" id="gqtzsy_m" name="gqtzsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他投资收益:</td>
			     <td><input class="easyui-validatebox" id="qttzsy_m" name="qttzsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>公允价值变动收益:</td>
			     <td><input class="easyui-validatebox" id="gyjzbdsy_m" name="gyjzbdsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>汇兑净收益:</td>
			     <td><input class="easyui-validatebox" id="hdjsy_m" name="hdjsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>资产处置收益:</td>
			     <td><input class="easyui-validatebox" id="zcczsy_m" name="zcczsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他业务收入:</td>
			     <td><input class="easyui-validatebox" id="qtywsr_m" name="qtywsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业支出:</td>
			     <td><input class="easyui-validatebox" id="yyzc_m" name="yyzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>业务及管理费:</td>
			     <td><input class="easyui-validatebox" id="ywjglf_m" name="ywjglf" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其中:职工工资:</td>
			     <td><input class="easyui-validatebox" id="zggz_m" name="zggz" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>福利费:</td>
			     <td><input class="easyui-validatebox" id="flf_m" name="flf" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>住房公积金和住房补贴:</td>
			     <td><input class="easyui-validatebox" id="zfgjjhzfbt_m" name="zfgjjhzfbt" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>税金及附加:</td>
			     <td><input class="easyui-validatebox" id="sjjfj_m" name="sjjfj" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>资产减值损失:</td>
			     <td><input class="easyui-validatebox" id="zcjzss_m" name="zcjzss" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他业务支出:</td>
			     <td><input class="easyui-validatebox" id="qtywzc_m" name="qtywzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业利润:</td>
			     <td><input class="easyui-validatebox" id="yylr_m" name="yylr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业外收入（加）:</td>
			     <td><input class="easyui-validatebox" id="yywsr_m" name="yywsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业外支出（减）:</td>
			     <td><input class="easyui-validatebox" id="yywzc_m" name="yywzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>利润总额:</td>
			     <td><input class="easyui-validatebox" id="lrze_m" name="lrze" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>所得税（减）:</td>
			     <td><input class="easyui-validatebox" id="sds_m" name="sds" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>净利润:</td>
			     <td><input class="easyui-validatebox " id="jlr_m" name="jlr" style="height: 23px; width:230px"  disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>年度损益调整（加）:</td>
			     <td><input class="easyui-validatebox" id="ndsytz_m" name="ndsytz" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>留存利润:</td>
			     <td><input class="easyui-validatebox" id="lclr_m" name="lclr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>未分配利润:</td>
			     <td><input class="easyui-validatebox" id="wfplr_m" name="wfplr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>应纳增值税:</td>
			     <td><input class="easyui-validatebox" id="ynzzs_m" name="ynzzs" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>核心一级资本净额:</td>
			     <td><input class="easyui-validatebox" id="hxyjzbje_m" name="hxyjzbje" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>一级资本净额:</td>
			     <td><input class="easyui-validatebox" id="yjzbje_m" name="yjzbje" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>资本净额:</td>
			     <td><input class="easyui-validatebox" id="zbje_m" name="zbje" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   <tr>
			     <td>应用资本底线及校准后的风险加权资产合计 :</td>
			     <td>
			        <input class="easyui-validatebox" id="ygzbjfxjqzchj_m" name="ygzbjfxjqzchj" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   <tr>
			   <td>数据日期 :</td>
				   <td><input id="sjrq_m3" name="sjrq" style="height: 23px; width:230px" editable=true data-options="required:true" disabled="disabled"/></td>
			   </tr>
              </table>
           </div>
         </form>
	</div>
</div>

<!--新增实际控制人证件类型窗口-->
<div id="dialogactctrlidtype" class="easyui-dialog" style="width:450px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div id="tbForAddDialogactctrlidtype">
        <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveactctrlidtype()">确定</a>&nbsp;&nbsp;
        <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="cancelactctrlidtype()">取消</a>
    </div><br>
    <div align="center">
        <textarea id="areaactctrlidtype" style="width: 360px;height: 40px" disabled="disabled"></textarea>
    </div><br>
    <!-- 信息录入 -->
    <div style="position:absolute; height:240px; margin-left: 45px; overflow:auto"; align="center">
        <table align="center">
        	<tr>
                <td align="left">A-单位</td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;A01-统一社会信用代码</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('A01')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('A01')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;A02-组织机构代码</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('A02')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('A02')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;A03-其他</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('A03')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('A03')"></a></td>
            </tr>
            <tr>
                <td align="left">B-自然人</td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;B01-身份证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B01')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B01')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;B02-户口簿</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B02')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B02')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;B03-护照</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B03')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B03')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;B04-军官证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B04')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B04')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;B05-士兵证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B05')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B05')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;B06-港澳居民来往内地通行证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B06')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B06')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;B07-台湾同胞来往内地通行证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B07')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B07')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;B08-临时身份证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B08')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B08')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;B09-外国人居留证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B09')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B09')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;B10-警官证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B10')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B10')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;B11-外国人永久居留身份证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B11')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B11')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;B12-港澳台居民居住证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B12')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B12')"></a></td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;B99-其他证件</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B99')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B99')"></a></td>
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript">
$(function(){
	$('#dialog1').dialog({
		onClose: function () {
			$('.validatebox-tip').remove();
		}
	});
	$('#dialog2').dialog({
		onClose: function () {
			$('.validatebox-tip').remove();
		}
	});
	$('#dialog3').dialog({
		onClose: function () {
			$('.validatebox-tip').remove();
		}
	});
	$('#dialog1').dialog('close');
	$('#dialog2').dialog('close');
	$('#dialog3').dialog('close');
	$('#dialog_m1').dialog('close');
	$('#dialog_m2').dialog('close');
	$('#dialog_m3').dialog('close');
	$("#importExcel1").dialog("close");
	$("#importExcel2").dialog("close");
	$("#importExcel3").dialog("close");
	$("#importIJ").dialog("close");
	$('#exportFileDialog').dialog('close');
	getbasecode();
	//intiMonthBox('setupdate'); 
	
	$('#jrjg_tabs').tabs({
		onSelect:function(id){
			var t1 = "金融机构（法人）基础信息-基础情况统计表";
			var t2 = "金融机构（法人）基础信息-资产负债及风险统计表";
			var t3 = "金融机构（法人）基础信息-利润及资本统计表";
			if(id == t1){
				$("#dg1").datagrid({
					method:'post',
					url:'XfindJRJGFRJCXXJCdtj',
					loadMsg:'数据加载中,请稍后...',
					//singleSelect:true,
					checkOnSelect:true,
					autoRowHeight:false,
					//fitColumns:false,
					pagination:true,
					rownumbers:true,
					toolbar:'#tb1',
					pageSize:20,
					pageList:[15,20,30,50],
					columns:[[
							{field:'ck',checkbox:true},
						    {field:'operationname',title:'操作名',width:100,align:'center'},
						    {field:'nopassreason',title:'审核不通过原因',width:100,align:'center'},
						    {field:'checkstatus',title:'校验结果',width:100,align:'center',formatter: function(value,row,index){					
								if(value == '2'){
									return '<span style="color:red;">' + '校验失败' + '</span>';
								}else if(value == '1'){
									return '校验成功';
								}else if(value == '0'){
									return '未校验';
								}else{
									return value;
								}
								}},
						    {field:'finorgname',title:'金融机构名称',width:100,align:'center'},
						    {field:'finorgcode',title:'金融机构代码',width:100,align:'center'},
						    {field:'finorgnum',title:'金融机构编码',width:100,align:'center'},
						    {field:'jglb',title:'机构类别',width:100,align:'center'},
						    {field:'regaddress',title:'注册地址',width:100,align:'center'},
						    {field:'regarea',title:'地区代码',width:100,align:'center'},
						    {field:'regamt',title:'注册资本',width:100,align:'center'},
						    {field:'setupdate',title:'成立日期',width:100,align:'center'},
						    {field:'lxr',title:'联系人',width:100,align:'center'},
						    {field:'phone',title:'联系电话',width:100,align:'center'},
						    {field:'orgmanagestatus',title:'经营状态',width:100,align:'center'},
						    {field:'orgstoreconomy',title:'出资人经济成分',width:100,align:'center'},
						    {field:'industrycetegory',title:'行业分类',width:100,align:'center',hidden:true},
						    {field:'invermodel',title:'企业规模',width:100,align:'center'},
						    {field:'actctrlname',title:'实际控制人名称',width:100,align:'center'},
						    {field:'actctrlidtype',title:'实际控制人证件类型',width:120,align:'center'},
						    {field:'actctrlcode',title:'实际控制人证件代码',width:120,align:'center'},
						    {field:'personnum',title:'从业人员数',width:100,align:'center'},
						    {field:'onestockcode',title:'第一大股东代码',width:100,align:'center'},
						    {field:'twostockcode',title:'第二大股东代码',width:100,align:'center'},
						    {field:'threestockcode',title:'第三大股东代码',width:100,align:'center'},
						    {field:'fourstockcode',title:'第四大股东代码',width:100,align:'center'},
						    {field:'fivestockcode',title:'第五大股东代码',width:100,align:'center'},
						    {field:'sixstockcode',title:'第六大股东代码',width:100,align:'center'},
						    {field:'sevenstockcode',title:'第七大股东代码',width:100,align:'center'},
						    {field:'eightstockcode',title:'第八大股东代码',width:100,align:'center'},
						    {field:'ninestockcode',title:'第九大股东代码',width:100,align:'center'},
						    {field:'tenstockcode',title:'第十大股东代码',width:100,align:'center'},
						    {field:'onestockprop',title:'第一大股东持股比例',width:120,align:'center'},
						    {field:'twostockprop',title:'第二大股东持股比例',width:120,align:'center'},
						    {field:'threestockprop',title:'第三大股东持股比例',width:120,align:'center'},
						    {field:'fourstockprop',title:'第四大股东持股比例',width:120,align:'center'},
						    {field:'fivestockprop',title:'第五大股东持股比例',width:120,align:'center'},
						    {field:'sixstockprop',title:'第六大股东持股比例',width:120,align:'center'},
						    {field:'sevenstockprop',title:'第七大股东持股比例',width:120,align:'center'},
						    {field:'eightstockprop',title:'第八大股东持股比例',width:120,align:'center'},
						    {field:'ninestockprop',title:'第九大股东持股比例',width:120,align:'center'},
						    {field:'tenstockprop',title:'第十大股东持股比例',width:120,align:'center'},
						    {field:'sjrq',title:'数据日期',width:100,align:'center'},
						    {field:'operator',title:'操作人',width:100,align:'center'},
						    {field:'operationtime',title:'操作时间',width:120,align:'center'}
				    ]],
				    onDblClickRow :function(rowIndex,rowData){
				    	initForm();
				    	var dia = $('#dialog_m1').dialog('open');
				    	$('#dialog_m1').window('center');
			        	$("#formForMore1").form('load',rowData);
			        	$('#dialog_m1').dialog({modal : true});	

				    }
				});
				scrollShow($('#dg1'));
			}else if(id == t2){
				$("#dg2").datagrid({
					method:'post',
					url:'XfindJRJGFRJCXXZCdtj',
					loadMsg:'数据加载中,请稍后...',
					//singleSelect:true,
					checkOnSelect:true,
					autoRowHeight:false,
					//fitColumns:false,
					pagination:true,
					rownumbers:true,
					toolbar:'#tb2',
					pageSize:20,
					pageList:[15,20,30,50],
					columns:[[
							{field:'ck',checkbox:true},
						    {field:'operationname',title:'操作名',width:100,align:'center'},
						    {field:'nopassreason',title:'审核不通过原因',width:100,align:'center'},
						    {field:'checkstatus',title:'校验结果',width:100,align:'center',formatter: function(value,row,index){					
								if(value == '2'){
									return '<span style="color:red;">' + '校验失败' + '</span>';
								}else if(value == '1'){
									return '校验成功';
								}else if(value == '0'){
									return '未校验';
								}else{
									return value;
								}
								}},
						    {field:'gxck',title:'各项存款',width:100,align:'center'},
						    {field:'gxdk',title:'各项贷款',width:100,align:'center'},
						    {field:'zczj',title:'资产总计',width:100,align:'center'},
						    {field:'fzzj',title:'负债总计',width:100,align:'center'},
						    {field:'syzqyhj',title:'所有者权益合计',width:100,align:'center'},
						    {field:'sxzc',title:'生息资产',width:100,align:'center'},
						    {field:'fxzc',title:'付息负债',width:100,align:'center'},
						    {field:'ldxzc',title:'流动性资产',width:100,align:'center'},
						    {field:'ldxfz',title:'流动性负债',width:100,align:'center'},
						    {field:'zcldk',title:'正常类贷款',width:100,align:'center'},
						    {field:'gzldk',title:'关注类贷款',width:100,align:'center'},
						    {field:'cjldk',title:'次级类贷款',width:100,align:'center'},
						    {field:'jyldk',title:'可疑类贷款',width:100,align:'center'},
						    {field:'ssldk',title:'损失类贷款',width:100,align:'center'},
						    {field:'yqdk',title:'逾期贷款',width:100,align:'center'},
						    {field:'yqninetytysdk',title:'逾期90天以上贷款',width:100,align:'center'},
						    {field:'dkjzzb',title:'贷款减值准备',width:100,align:'center'},
						    {field:'sjrq',title:'数据日期',width:100,align:'center'},
						    {field:'operator',title:'操作人',width:100,align:'center'},
						    {field:'operationtime',title:'操作时间',width:100,align:'center'}
				    ]],
				    onDblClickRow :function(rowIndex,rowData){
				    	initForm();
				    	var dia = $('#dialog_m2').dialog('open');
			        	$("#formForMore2").form('load',rowData);
			        	$('#dialog_m2').dialog({modal : true});	

				    }
				});
				scrollShow($('#dg2'));
			}else if(id == t3){
				$("#dg3").datagrid({
					method:'post',
					url:'XfindJRJGFRJCXXLRdtj',
					loadMsg:'数据加载中,请稍后...',
					//singleSelect:true,
					checkOnSelect:true,
					autoRowHeight:false,
					//fitColumns:false,
					pagination:true,
					rownumbers:true,
					toolbar:'#tb3',
					pageSize:20,
					pageList:[15,20,30,50],
					columns:[[
							{field:'ck',checkbox:true},
						    {field:'operationname',title:'操作名',width:100,align:'center'},
						    {field:'nopassreason',title:'审核不通过原因',width:100,align:'center'},
						    {field:'checkstatus',title:'校验结果',width:100,align:'center',formatter: function(value,row,index){					
								if(value == '2'){
									return '<span style="color:red;">' + '校验失败' + '</span>';
								}else if(value == '1'){
									return '校验成功';
								}else if(value == '0'){
									return '未校验';
								}else{
									return value;
								}
								}},
						    {field:'yysr',title:'营业收入',width:100,align:'center'},
						    {field:'lxjsr',title:'利息净收入',width:100,align:'center'},
						    {field:'lxsr',title:'利息收入',width:100,align:'center'},
						    {field:'jrjgwllxsr',title:'金融机构往来利息收入',width:100,align:'center'},
						    {field:'xtnwllxsr',title:'系统内往来利息收入',width:100,align:'center'},
						    {field:'gxdklxsr',title:'各项贷款利息收入',width:100,align:'center'},
						    {field:'zqlxsr',title:'债券利息收入',width:100,align:'center'},
						    {field:'qtlxsr',title:'其他利息收入',width:100,align:'center'},
						    {field:'lxzc',title:'利息支出',width:100,align:'center'},
						    {field:'jrjgwllxzc',title:'金融机构往来利息支出',width:100,align:'center'},
						    {field:'xtnwllxzc',title:'系统内往来利息支出',width:100,align:'center'},
						    {field:'gxcklxzc',title:'各项存款利息支出',width:100,align:'center'},
						    {field:'zqlxzc',title:'债券利息支出',width:100,align:'center'},
						    {field:'qtlxzc',title:'其他利息支出',width:100,align:'center'},
						    {field:'sxfjyjjsr',title:'手续费及佣金净收入',width:100,align:'center'},
						    {field:'sxfjyjsr',title:'手续费及佣金收入',width:100,align:'center'},
						    {field:'jxfjyjzc',title:'手续费及佣金支出',width:100,align:'center'},
						    {field:'zlsy',title:'租赁收益',width:100,align:'center'},
						    {field:'tzsy',title:'投资收益',width:100,align:'center'},
						    {field:'zqtzsy',title:'债券投资收益',width:100,align:'center'},
						    {field:'gqtzsy',title:'股权投资收益',width:100,align:'center'},
						    {field:'qttzsy',title:'其他投资收益',width:100,align:'center'},
						    {field:'gyjzbdsy',title:'公允价值变动收益',width:100,align:'center'},
						    {field:'hdjsy',title:'汇兑净收益',width:100,align:'center'},
						    {field:'zcczsy',title:'资产处置收益',width:100,align:'center'},
						    {field:'qtywsr',title:'其他业务收入',width:100,align:'center'},
						    {field:'yyzc',title:'营业支出',width:100,align:'center'},
						    {field:'ywjglf',title:'业务及管理费',width:100,align:'center'},
						    {field:'zggz',title:'职工工资',width:100,align:'center'},
						    {field:'flf',title:'福利费',width:100,align:'center'},
						    {field:'zfgjjhzfbt',title:'住房公积金和住房补贴',width:100,align:'center'},
						    {field:'sjjfj',title:'税金及附加',width:100,align:'center'},
						    {field:'zcjzss',title:'资产减值损失',width:100,align:'center'},
						    {field:'qtywzc',title:'其他业务支出',width:100,align:'center'},
						    {field:'yylr',title:'营业利润',width:100,align:'center'},
						    {field:'yywsr',title:'营业外收入（加）',width:100,align:'center'},
						    {field:'yywzc',title:'营业外支出（减）',width:100,align:'center'},
						    {field:'lrze',title:'利润总额',width:100,align:'center'},
						    {field:'sds',title:'所得税（减）',width:100,align:'center'},
						    {field:'jlr',title:'净利润',width:100,align:'center'},
						    {field:'ndsytz',title:'年度损益调整（加）',width:100,align:'center'},
						    {field:'lclr',title:'留存利润',width:100,align:'center'},
						    {field:'wfplr',title:'未分配利润',width:100,align:'center'},
						    {field:'ynzzs',title:'应纳增值税',width:100,align:'center'},
						    {field:'hxyjzbje',title:'核心一级资本净额',width:100,align:'center'},
						    {field:'yjzbje',title:'一级资本净额',width:100,align:'center'},
						    {field:'zbje',title:'资本净额',width:100,align:'center'},
						    {field:'ygzbjfxjqzchj',title:'应用资本底线及校准后的风险加权资产合计',width:100,align:'center'},
						    {field: 'sjrq',title: '数据日期',width: 100,align: 'center'},
						    {field:'operator',title:'操作人',width:100,align:'center'},
						    {field:'operationtime',title:'操作时间',width:100,align:'center'}
				    ]],
				    onDblClickRow :function(rowIndex,rowData){
				    	initForm();
				    	var dia = $('#dialog_m3').dialog('open');
			        	$("#formForMore3").form('load',rowData);
			        	$('#dialog_m3').dialog({modal : true});	
			        	//关闭弹窗默认隐藏div
			    		/* $(dia).window({
			    	    	onBeforeClose: function () {
			    	    		//初始化表单的元素的状态
			    	    		initForm("formForMore3");
			    	    	}
			    		}); */
				    }
				});
				scrollShow($('#dg3'));
			}
		}
	});

	
	//$("#dg1").datagrid('loadData',datas);
});

//显示水平滚动条
function scrollShow(datagrid) {  
  datagrid.prev(".datagrid-view2").children(".datagrid-body").html("<div style='width:" + datagrid.prev(".datagrid-view2").find(".datagrid-header-row").width() + "px;border:solid 0px;height:1px;'></div>");  
} 

//获取基础代码
function getbasecode(){
	//getAindustry(); //一级行业代码
	//getArea(); //行政区划代码
	//getBindustry(); //二级行业代码
	//getCountry("customerIdType2"); //国家代码
	//getCurrency("artificialPersonType"); //币种代码
}

//获取一级行业代码
function getAindustry(){
	$.ajax({
	    url:'getAindustryCode',
	    type:'POST', //GET
	    async:true,    //或false,是否异步
	    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
	    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
	    success:function(data){
	    	var areaList = data.list;
		    if(areaList != null && areaList != ""){
		    	var jsonStr='[';
		    	for(var i = 0;i<areaList.length;i++){
		     		 jsonStr = jsonStr + '{"id":"'+areaList[i].aindustrycode+'",'+'"val":"'+areaList[i].aindustryname+'",'+'"text":"'+areaList[i].aindustrycode+'-'+areaList[i].aindustryname+'"},'
		        }
		    	jsonStr = jsonStr.substring(0,jsonStr.lastIndexOf(','));   //如果是以,结尾，则截取,前面的字符串
		        jsonStr = jsonStr + ']';
		    	
		        var jsonObj = JSON.parse(jsonStr);     //将json格式的字符串转为json对象
		 	   $("#industrycetegory").combobox("loadData",jsonObj);
		 	   $("#industrycetegory").combobox({
		        	filter: function(q, row){
		        		var opts = $(this).combobox('options');
		        		return match(row[opts.textField],q)!=-1;
		        	}
		        });
		 	  $("#industrycetegory_m").combobox("loadData",jsonObj);
		    }
	    }
	});
}

//获取行政区划代码
function getArea(){
	$.ajax({
	    url:'getAreaCode',
	    type:'POST', //GET
	    async:true,    //或false,是否异步
	    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
	    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
	    success:function(data){
	    	var areaList = data.list;
		    if(areaList != null && areaList != ""){
		    	var jsonStr='[';
		    	for(var i = 0;i<areaList.length;i++){
		     		 jsonStr = jsonStr + '{"id":"'+areaList[i].areacode+'",'+'"val":"'+areaList[i].areaname+'",'+'"text":"'+areaList[i].areacode+'-'+areaList[i].areaname+'"},'
		        }
		    	jsonStr = jsonStr.substring(0,jsonStr.lastIndexOf(','));   //如果是以,结尾，则截取,前面的字符串
		        jsonStr = jsonStr + ']';
		    	
		        var jsonObj = JSON.parse(jsonStr);     //将json格式的字符串转为json对象
		        $("#regarea").combobox("loadData",jsonObj);
		        $("#regarea").combobox({
		        	filter: function(q, row){
		        		var opts = $(this).combobox('options');
		        		return match(row[opts.textField],q)!=-1;
		        	}
		        });
		 	    $("#regarea_m").combobox("loadData",jsonObj);   
		    }
	    }
	});
}

var intiMonthBox = function(id){
    var db = $('#'+id);
    db.datebox({
           onShowPanel: function () {//显示日趋选择对象后再触发弹出月份层的事件，初始化时没有生成月份层
               span.trigger('click'); //触发click事件弹出月份层
               if (!tds) setTimeout(function () {//延时触发获取月份对象，因为上面的事件触发和对象生成有时间间隔
                   tds = p.find('div.calendar-menu-month-inner td');
                   tds.click(function (e) {
                       e.stopPropagation(); //禁止冒泡执行easyui给月份绑定的事件
                       var year = /\d{4}/.exec(span.html())[0]//得到年份
                       , month = parseInt($(this).attr('abbr'), 10); //月份，这里不需要+1
                       db.datebox('hidePanel')//隐藏日期对象
                       .datebox('setValue', year + '-' + month); //设置日期的值
                   });
               }, 0);
               yearIpt.unbind();//解绑年份输入框中任何事件
           },
           parser: function (s) {
               if (!s) return new Date();
               var arr = s.split('-');
               return new Date(parseInt(arr[0], 10), parseInt(arr[1], 10) - 1, 1);
           },
           formatter: function (d) {
        	 //解决月份小于10时，未加'0'问题
        	 var yearstr = d.getFullYear();
        	 var month = d.getMonth() + 1;
        	 var monthstr = month<10? "0"+ month : month;                                   
        	 return yearstr+ '-'+ monthstr;
             //return d.getFullYear() + '-' + (d.getMonth() + 1);
           }
       });
       var p = db.datebox('panel'), //日期选择对象
           tds = false, //日期选择对象中月份
           yearIpt = p.find('input.calendar-menu-year'),//年份输入框
           span = p.find('span.calendar-text'); //显示月份层的触发控件    
}

//查询
function chaxun_jc(){
	var finorgname = $("#finorgname").val();
	$("#dg1").datagrid("load",{finorgname:finorgname});
}
//查询
function chaxun_zc(){
	var selectdbhtbm = $("#selectdbhtbm").val();
	$("#dg1").datagrid("load",{selectdbhtbm:selectdbhtbm});
}

//查询
function chaxun_lr(){
	var selectdbhtbm = $("#selectdbhtbm").val();
	$("#dg1").datagrid("load",{selectdbhtbm:selectdbhtbm});
}

function initForm(){
	//document.getElementById(id).reset(); 
	//$('#'+id).form('clear'); 
	$("#finorgname").val('');
	$("#finorgname_m").val('');
	$("#finorgcode").val('');
	$("#finorgcode_m").val('');
	$("#finorgnum").val('');
	$("#finorgnum_m").val('');
	$("#jglb").combobox('setValue','');
	$("#jglb_m").combobox('setValue','');
	$("#regaddress").val('');
	$("#regaddress_m").val('');
	$("#regarea").combotree('setValue','');
	$("#regarea_m").combotree('setValue','');
	$("#regamt").val('');
	$("#regamt_m").val('');
	$("#setupdate").datebox('setValue','');
	$("#setupdate_m").datebox('setValue','');
	$("#lxr").val('');
	$("#lxr_m").val('');
	$("#phone").val('');
	$("#phone_m").val('');
	$("#orgmanagestatus").combobox('setValue','');
	$("#orgmanagestatus_m").combobox('setValue','');
	$("#orgstoreconomy").combobox('setValue','');
	$("#orgstoreconomy_m").combobox('setValue','');
	//$("#industrycetegory").combobox('setValue','');
	//$("#industrycetegory_m").val('');
	$("#invermodel").combobox('setValue','');
	$("#invermodel_m").combobox('setValue','');
	$("#actctrlidtype").val('');
	$("#actctrlidtype_m").val('');
	$("#actctrlname").val('');
	$("#actctrlname_m").val('');
	$("#actctrlcode").val('');
	$("#actctrlcode_m").val('');
	$("#personnum").val('');
	$("#personnum_m").val('');
	$("#onestockcode").val('');
	$("#onestockcode_m").val('');
	$("#onestockprop").val('');
	$("#onestockprop_m").val('');
	$("#twostockcode").val('');
	$("#twostockcode_m").val('');
	$("#twostockprop").val('');
	$("#twostockprop_m").val('');
	$("#threestockcode").val('');
	$("#threestockcode_m").val('');
	$("#threestockprop").val('');
	$("#threestockprop_m").val('');
	$("#fourstockcode").val('');
	$("#fourstockcode_m").val('');
	$("#fourstockprop").val('');
	$("#fourstockprop_m").val('');
	$("#fivestockcode").val('');
	$("#fivestockcode_m").val('');
	$("#fivestockprop").val('');
	$("#fivestockprop_m").val('');
	$("#sixstockcode").val('');
	$("#sixstockcode_m").val('');
	$("#sixstockprop").val('');
	$("#sixstockprop_m").val('');
	$("#sevenstockcode").val('');
	$("#sevenstockcode_m").val('');
	$("#sevenstockprop").val('');
	$("#sevenstockprop_m").val('');
	$("#eightstockcode").val('');
	$("#eightstockcode_m").val('');
	$("#eightstockprop").val('');
	$("#eightstockprop_m").val('');
	$("#ninestockcode").val('');
	$("#ninestockcode_m").val('');
	$("#ninestockprop").val('');
	$("#ninestockprop_m").val('');
	$("#tenstockcode").val('');
	$("#tenstockcode_m").val('');
	$("#tenstockprop").val('');
	$("#tenstockprop_m").val('');
	$("#id_jc").val('');
	$("#orgid_jc").val('');
	$("#departid_jc").val('');
	$("#checkstatus_jc").val('');
	$("#datastatus_jc").val('');
	$("#operator_jc").val('');
	$("#operationname_jc").val('');
	$("#operationtime_jc").val('');
	$("#nopassreason_jc").val('');
	$("#gxck").val('');
	$("#gxck_m").val('');
	$("#gxdk").val('');
	$("#gxdk_m").val('');
	$("#zczj").val('');
	$("#zczj_m").val('');
	$("#fzzj").val('');
	$("#fzzj_m").val('');
	$("#syzqyhj").val('');
	$("#syzqyhj_m").val('');
	$("#sxzc").val('');
	$("#sxzc_m").val('');
	$("#fxzc").val('');
	$("#fxzc_m").val('');
	$("#ldxzc").val('');
	$("#ldxzc_m").val('');
	$("#ldxfz").val('');
	$("#ldxfz_m").val('');
	$("#zcldk").val('');
	$("#zcldk_m").val('');
	$("#gzldk").val('');
	$("#gzldk_m").val('');
	$("#cjldk").val('');
	$("#cjldk_m").val('');
	$("#jyldk").val('');
	$("#jyldk_m").val('');
	$("#ssldk").val('');
	$("#ssldk_m").val('');
	$("#yqdk").val('');
	$("#yqdk_m").val('');
	$("#yqninetytysdk").val('');
	$("#yqninetytysdk_m").val('');
	$("#dkjzzb").val('');
	$("#dkjzzb_m").val('');
	$("#id_zc").val('');
	$("#orgid_zc").val('');
	$("#departid_zc").val('');
	$("#checkstatus_zc").val('');
	$("#datastatus_zc").val('');
	$("#operator_zc").val('');
	$("#operationname_zc").val('');
	$("#operationtime_zc").val('');
	$("#nopassreason_zc").val('');
	$("#yysr").val('');
	$("#yysr_m").val('');
	$("#lxjsr").val('');
	$("#lxjsr_m").val('');
	$("#lxsr").val('');
	$("#lxsr_m").val('');
	$("#jrjgwllxsr").val('');
	$("#jrjgwllxsr_m").val('');
	$("#xtnwllxsr").val('');
	$("#xtnwllxsr_m").val('');
	$("#gxdklxsr").val('');
	$("#gxdklxsr_m").val('');
	$("#zqlxsr").val('');
	$("#zqlxsr_m").val('');
	$("#qtlxsr").val('');
	$("#qtlxsr_m").val('');
	$("#lxzc").val('');
	$("#lxzc_m").val('');
	$("#jrjgwllxzc").val('');
	$("#jrjgwllxzc_m").val('');
	$("#xtnwllxzc").val('');
	$("#xtnwllxzc_m").val('');
	$("#gxcklxzc").val('');
	$("#gxcklxzc_m").val('');
	$("#zqlxzc").val('');
	$("#zqlxzc_m").val('');
	$("#qtlxzc").val('');
	$("#qtlxzc_m").val('');
	$("#sxfjyjjsr").val('');
	$("#sxfjyjjsr_m").val('');
	$("#sxfjyjsr").val('');
	$("#sxfjyjsr_m").val('');
	$("#jxfjyjzc").val('');
	$("#jxfjyjzc_m").val('');
	$("#zlsy").val('');
	$("#zlsy_m").val('');
	$("#tzsy").val('');
	$("#tzsy_m").val('');
	$("#zqtzsy").val('');
	$("#zqtzsy_m").val('');
	$("#gqtzsy").val('');
	$("#gqtzsy_m").val('');
	$("#qttzsy").val('');
	$("#qttzsy_m").val('');
	$("#gyjzbdsy").val('');
	$("#gyjzbdsy_m").val('');
	$("#hdjsy").val('');
	$("#hdjsy_m").val('');
	$("#zcczsy").val('');
	$("#zcczsy_m").val('');
	$("#qtywsr").val('');
	$("#qtywsr_m").val('');
	$("#yyzc").val('');
	$("#yyzc_m").val('');
	$("#ywjglf").val('');
	$("#ywjglf_m").val('');
	$("#zggz").val('');
	$("#zggz_m").val('');
	$("#flf").val('');
	$("#flf_m").val('');
	$("#zfgjjhzfbt").val('');
	$("#zfgjjhzfbt_m").val('');
	$("#sjjfj").val('');
	$("#sjjfj_m").val('');
	$("#zcjzss").val('');
	$("#zcjzss_m").val('');
	$("#qtywzc").val('');
	$("#qtywzc_m").val('');
	$("#yylr").val('');
	$("#yylr_m").val('');
	$("#yywsr").val('');
	$("#yywsr_m").val('');
	$("#yywzc").val('');
	$("#yywzc_m").val('');
	$("#lrze").val('');
	$("#lrze_m").val('');
	$("#sds").val('');
	$("#sds_m").val('');
	$("#jlr").val('');
	$("#jlr_m").val('');
	$("#ndsytz").val('');
	$("#ndsytz_m").val('');
	$("#lclr").val('');
	$("#lclr_m").val('');
	$("#wfplr").val('');
	$("#wfplr_m").val('');
	$("#ynzzs").val('');
	$("#ynzzs_m").val('');
	$("#hxyjzbje").val('');
	$("#hxyjzbje_m").val('');
	$("#yjzbje").val('');
	$("#yjzbje_m").val('');
	$("#zbje").val('');
	$("#zbje_m").val('');
	$("#ygzbjfxjqzchj").val('');
	$("#ygzbjfxjqzchj_m").val('');
	$("#id_lr").val('');
	$("#orgid_lr").val('');
	$("#departid_lr").val('');
	$("#checkstatus_lr").val('');
	$("#datastatus_lr").val('');
	$("#operator_lr").val('');
	$("#operationname_lr").val('');
	$("#operationtime_lr").val('');
	$("#nopassreason_lr").val('');
}
//新增
function xinzeng_jc(){
	$('#queding_jc').linkbutton('enable');
	var dia = $('#dialog1').dialog('open');
	$('#dialog1').dialog({modal:true});
	$('#dialog1').dialog({title:"新增金融机构（法人）基础信息-基础情况"});
	//关闭弹窗默认隐藏div
	/* $(dia).window({
    	onBeforeClose: function () {
    		//初始化表单的元素的状态
    		initForm("formFordialog1");
    	}
	}); */
	initForm("formFordialog1");
	$('#formFordialog1').form('validate');
}

//新增
function xinzeng_zc(){
	$('#queding_zc').linkbutton('enable');
	var dia = $('#dialog2').dialog('open');
	$('#dialog2').dialog({modal:true});
	$('#dialog2').dialog({title:"新增金融机构（法人）基础信息-资产负债及风险"});
	//关闭弹窗默认隐藏div
	/* $(dia).window({
    	onBeforeClose: function () {
    		//初始化表单的元素的状态
    		initForm("formFordialog2");
    	}
	}); */
	initForm("formFordialog2");
	$('#formFordialog2').form('validate');
}

//新增
function xinzeng_lr(){
	$('#queding_lr').linkbutton('enable');
	$('#dialog3').dialog('open');
	$('#dialog3').dialog({modal:true});
	$('#dialog3').dialog({title:"新增金融机构（法人）基础信息-利润及资本"});
	//关闭弹窗默认隐藏div
	/* $(dia).window({
    	onBeforeClose: function () {
    		//初始化表单的元素的状态
    		initForm("formFordialog3");
    	}
	}); */
	initForm("formFordialog3");
}

//修改
function xiugai_jc(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length != 1){
		$.messager.alert('操作提示','请选择一条数据修改','info');
	}else{
		initForm();
		$("#formFordialog1").form('load',rows[0]);
		$('#queding_jc').linkbutton('enable');
		var dia = $('#dialog1').dialog('open');
		$('#dialog1').dialog({modal:true});
		$('#dialog1').dialog({title:"修改金融机构（法人）基础信息-基础情况"});
		//关闭弹窗默认隐藏div
		/* $(dia).window({
	    	onBeforeClose: function () {
	    		//初始化表单的元素的状态
	    		initForm("formFordialog1");
	    	}
		}); */
	}
}

//修改
function xiugai_zc(){
	var rows = $('#dg2').datagrid('getSelections');
	if(rows.length != 1){
		$.messager.alert('操作提示','请选择一条数据修改','info');
	}else{
		initForm();
		$("#formFordialog2").form('load',rows[0]);
		$('#queding_zc').linkbutton('enable');
		var dia = $('#dialog2').dialog('open');
		$('#dialog2').dialog({modal:true});
		$('#dialog2').dialog({title:"修改金融机构（法人）基础信息-资产负债及风险"});
		//关闭弹窗默认隐藏div
		/* $(dia).window({
	    	onBeforeClose: function () {
	    		//初始化表单的元素的状态
	    		initForm("formFordialog2");
	    	}
		}); */
	}
}

//修改
function xiugai_lr(){
	var rows = $('#dg3').datagrid('getSelections');
	if(rows.length != 1){
		$.messager.alert('操作提示','请选择一条数据修改','info');
	}else{
		initForm();
		$("#formFordialog3").form('load',rows[0]);
		$('#queding_lr').linkbutton('enable');
		var dia = $('#dialog3').dialog('open');
		$('#dialog3').dialog({modal:true});
		$('#dialog3').dialog({title:"修改金融机构（法人）基础信息-利润及资本"});
		//关闭弹窗默认隐藏div
		/* $(dia).window({
	    	onBeforeClose: function () {
	    		//初始化表单的元素的状态
	    		initForm("formFordialog3");
	    	}
		}); */
	}
}

//新增或修改点击确定保存
function queding_jc(){
	//$('#queding_jc').linkbutton('disable');
	$("#formFordialog1").form('submit',{
		url:'XsaveOrUpdatejrjgfrBaseinfo',
        contentType:'application/x-www-form-urlencoded; charset=UTF-8',
        dataType:'json',
       	onSubmit:function(){
       		var isValidate = $("#formFordialog1").form('validate');
       		if(!isValidate){
       			return isValidate;
       		}else{
       			$.messager.progress({
					title:'请稍等',
					msg:'处理中......',
					timeout:10000,
				});  		
       		}
       	},
       	success:function(res){
       		$.messager.progress('close');
       		if(res == '2'){
       			$.messager.alert('操作提示','已有一条数据只能修改','info');
       		}else if(res == '1'){
   				$.messager.alert('','保存成功','info',function(r){
   					$('#dialog1').dialog('close');
   					$('#dg1').datagrid('reload');
   	            });
   			}else if(res == '0'){
   				$.messager.alert('操作提示','保存失败','error');
   			} 	
       	},
       	error : function(){
			$.messager.progress('close');
			$('#dialog1').dialog('close');
			$.messager.alert('操作提示','网络异常请稍后再试','error');
		}
	});
}

//新增或修改点击确定保存
function queding_zc(){
	$('#queding_zc').linkbutton('disable');
	$("#formFordialog2").form('submit',{
		url:'XsaveOrUpdatejrjgfrAssets',
        contentType:'application/x-www-form-urlencoded; charset=UTF-8',
        dataType:'json',
       	onSubmit:function(){
       		var isValidate = $("#formFordialog2").form('validate');
       		if(!isValidate){
       			return isValidate;
       		}else{
       			$.messager.progress({
					title:'请稍等',
					msg:'处理中......',
					timeout:10000,
				});  		
       		}
       	},
       	success:function(res){
       		$.messager.progress('close');
       		if(res == '2'){
       			$.messager.alert('操作提示','已有一条数据只能修改','info');
       		}else if(res == '1'){
   				$.messager.alert('','保存成功','info',function(r){
   					$('#dialog2').dialog('close');
   					$('#dg2').datagrid('reload');
   	            });
   			}else if(res == '0'){
   				$.messager.alert('操作提示','保存失败','error');
   			} 	
       	},
       	error : function(){
			$.messager.progress('close');
			$('#dialog2').dialog('close');
			$.messager.alert('操作提示','网络异常请稍后再试','error');
		}
	});
}

//新增或修改点击确定保存
function queding_lr(){
	$('#queding_lr').linkbutton('disable');
	$("#formFordialog3").form('submit',{
		url:'XsaveOrUpdatejrjgfrProfit',
        contentType:'application/x-www-form-urlencoded; charset=UTF-8',
        dataType:'json',
       	onSubmit:function(){
       		var isValidate = $("#formFordialog3").form('validate');
       		if(!isValidate){
      			return isValidate;
       		}else{
      			$.messager.progress({
					title:'请稍等',
					msg:'处理中......',
					timeout:10000,
				});  		
       		}
       	},
       	success:function(res){
       		$.messager.progress('close');
       		if(res == '2'){
       			$.messager.alert('操作提示','已有一条数据只能修改','info');
       		}else if(res == '1'){
   				$.messager.alert('','保存成功','info',function(r){
   					$('#dialog3').dialog('close');
   					$('#dg3').datagrid('reload');
   	            });
   			}else if(res == '0'){
   				$.messager.alert('操作提示','保存失败','error');
   			} 	
       	},
       	error : function(){
			$.messager.progress('close');
			$('#dialog3').dialog('close');
			$.messager.alert('操作提示','网络异常请稍后再试','error');
		}
	});
}

//取消，关闭新增或修改弹窗
function quxiao_jc(){
	$('#dialog1').dialog('close');
}

//取消，关闭新增或修改弹窗
function quxiao_zc(){
	$('#dialog2').dialog('close');
}

//取消，关闭新增或修改弹窗
function quxiao_lr(){
	$('#dialog3').dialog('close');
}

//删除
function shanchu_jc(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要删除选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				for (var i = 0; i < rows.length; i++){
					deleteid = deleteid+rows[i].id+"-";
				}
				$.ajax({
		            url:'XdeletejrjgfrBaseinfo',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"some","deleteid":deleteid},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要删除所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'XdeletejrjgfrBaseinfo',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//删除
function shanchu_zc(){
	var rows = $('#dg2').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要删除选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				for (var i = 0; i < rows.length; i++){
					deleteid = deleteid+rows[i].id+"-";
				}
				$.ajax({
		            url:'XdeletejrjgfrAssets',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"some","deleteid":deleteid},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','删除成功','info',function(r){
			           			$('#dg2').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要删除所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'XdeletejrjgfrAssets',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','删除成功','info',function(r){
			           			$('#dg2').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//删除
function shanchu_lr(){
	var rows = $('#dg3').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要删除选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				for (var i = 0; i < rows.length; i++){
					deleteid = deleteid+rows[i].id+"-";
				}
				$.ajax({
		            url:'XdeletejrjgfrProfit',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"some","deleteid":deleteid},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','删除成功','info',function(r){
			           			$('#dg3').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要删除所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'XdeletejrjgfrProfit',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','删除成功','info',function(r){
			           			$('#dg3').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//校验-基础
function jiaoyan_jc(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要校验选中的数据吗',function(r){
			if(r){
				var pass = true;
				for (var i = 0; i < rows.length; i++){
					if(rows[i].checkstatus=='0'){
						
					}else{
						pass = false;
						break;
					}
				}
				if(pass){
					$.ajax({
			            url:'XcheckjrjgfrBaseinfo',
			            type:'POST', //GET
			            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			            data:{"checktype":"some","rows":JSON.stringify(rows)},
			            //data:{"checktype":"some","checkid":checkid},
			            dataType:'text',    
			            success:function(res){
				           	if(res == '1'){ 
				           		$.messager.alert('','校验成功','info',function(r){
				           			$('#dg1').datagrid('reload');
				           		});
				           	}else{  
				           		//否则删除失败
				           		/* $.messager.alert('','校验失败'+res,'error',function(r){
				           			$('#dg1').datagrid('reload');
				           		}); */
				           		$('#dg1').datagrid('reload');
								$.messager.alert('操作提示','校验完成,将自动下载校验结果','info', function (r) {
									window.location.href = "XdownFileCheckjrjgfrBaseinfo";
								});
				           	}
			            },
			           	error : function(){
			    			$.messager.alert('操作提示','网络异常请稍后再试','error');
			    		}
			        });
				}else{
					$.messager.alert('操作提示','只能校验未校验的数据','info');
				}
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要校验所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'XcheckjrjgfrBaseinfo',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"checktype":"all"},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','校验成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则删除失败
			           		//$.messager.alert('操作提示','校验失败','error');
			           		$('#dg1').datagrid('reload');
							$.messager.alert('操作提示','校验完成,将自动下载校验结果','info', function (r) {
								window.location.href = "XdownFileCheckjrjgfrBaseinfo";
							});
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//校验-资产
function jiaoyan_zc(){
	var rows = $('#dg2').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要校验选中的数据吗',function(r){
			if(r){
				var pass = true;
				for (var i = 0; i < rows.length; i++){
					if(rows[i].checkstatus=='0'){
						
					}else{
						pass = false;
						break;
					}
				}
				if(pass){
					$.ajax({
			            url:'XcheckjrjgfrAssets',
			            type:'POST', //GET
			            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			            data:{"checktype":"some","rows":JSON.stringify(rows)},
			            //data:{"checktype":"some","checkid":checkid},
			            dataType:'text',    
			            success:function(res){
				           	if(res == '1'){ 
				           		$.messager.alert('','校验成功','info',function(r){
				           			$('#dg2').datagrid('reload');
				           		});
				           	}else{  
				           		//否则删除失败
				           		/* $.messager.alert('','校验失败'+res,'error',function(r){
				           			$('#dg1').datagrid('reload');
				           		}); */
				           		$('#dg2').datagrid('reload');
								$.messager.alert('操作提示','校验完成,将自动下载校验结果','info', function (r) {
									window.location.href = "XdownFileCheckjrjgfrAssets";
								});
				           	}
			            },
			           	error : function(){
			    			$.messager.alert('操作提示','网络异常请稍后再试','error');
			    		}
			        });
				}else{
					$.messager.alert('操作提示','只能校验未校验的数据','info');
				}
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要校验所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'XcheckjrjgfrAssets',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"checktype":"all"},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','校验成功','info',function(r){
			           			$('#dg2').datagrid('reload');
			           		});
			           	}else{  
			           		//否则删除失败
			           		//$.messager.alert('操作提示','校验失败','error');
			           		$('#dg2').datagrid('reload');
							$.messager.alert('操作提示','校验完成,将自动下载校验结果','info', function (r) {
								window.location.href = "XdownFileCheckjrjgfrAssets";
							});
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//校验-利润
function jiaoyan_lr(){
	var rows = $('#dg3').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要校验选中的数据吗',function(r){
			if(r){
				var pass = true;
				for (var i = 0; i < rows.length; i++){
					if(rows[i].checkstatus=='0'){
						
					}else{
						pass = false;
						break;
					}
				}
				if(pass){
					$.ajax({
			            url:'XcheckjrjgfrProfit',
			            type:'POST', //GET
			            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			            data:{"checktype":"some","rows":JSON.stringify(rows)},
			            //data:{"checktype":"some","checkid":checkid},
			            dataType:'text',    
			            success:function(res){
				           	if(res == '1'){ 
				           		$.messager.alert('','校验成功','info',function(r){
				           			$('#dg3').datagrid('reload');
				           		});
				           	}else if(res == '2'){
				           		$.messager.alert('操作提示','请先导入IJ文件后再校验','info');
				           	}else{  
				           		//否则删除失败
				           		/* $.messager.alert('','校验失败'+res,'error',function(r){
				           			$('#dg1').datagrid('reload');
				           		}); */
				           		$('#dg3').datagrid('reload');
								$.messager.alert('操作提示','校验完成,将自动下载校验结果','info', function (r) {
									window.location.href = "XdownFileCheckjrjgfrProfit";
								});
				           	}
			            },
			           	error : function(){
			    			$.messager.alert('操作提示','网络异常请稍后再试','error');
			    		}
			        });
				}else{
					$.messager.alert('操作提示','只能校验未校验的数据','info');
				}
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要校验所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'XcheckjrjgfrProfit',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"checktype":"all"},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','校验成功','info',function(r){
			           			$('#dg3').datagrid('reload');
			           		});
			           	}else if(res == '2'){
			           		$.messager.alert('操作提示','请先导入IJ文件后再校验','info');
			           	}else{  
			           		//否则删除失败
			           		//$.messager.alert('操作提示','校验失败','error');
			           		$('#dg3').datagrid('reload');
							$.messager.alert('操作提示','校验完成,将自动下载校验结果','info', function (r) {
								window.location.href = "XdownFileCheckjrjgfrProfit";
							});
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//提交
function tijiao_jc(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要提交选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				var pass = true;
				for (var i = 0; i < rows.length; i++){
					if(rows[i].checkstatus=="1"){
						deleteid = deleteid+rows[i].id+"-";
					}else{
						pass = false;
						break;
					}
				}
				if(pass){
					$.ajax({
			            url:'XtijiaojrjgfrBaseinfo',
			            type:'POST', //GET
			            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			            //data:{"rows":JSON.stringify(rows)},
			            data:{"deletetype":"some","deleteid":deleteid},
			            dataType:'json',    
			            success:function(data){
				           	if(data == '1'){ 
				           		$.messager.alert('','提交成功','info',function(r){
				           			$('#dg1').datagrid('reload');
				           		});
				           	}else{  //否则删除失败
				           		$.messager.alert('操作提示','提交失败','error');
				           	}
			            },
			           	error : function(){
			    			$.messager.alert('操作提示','网络异常请稍后再试','error');
			    		}
			        });
				}else{
					$.messager.alert('操作提示','只能提交校验成功的数据','info');
				}
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要提交所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'XtijiaojrjgfrBaseinfo',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','提交成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','提交失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//提交
function tijiao_zc(){
	var rows = $('#dg2').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要提交选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				var pass = true;
				for (var i = 0; i < rows.length; i++){
					if(rows[i].checkstatus=="1"){
						deleteid = deleteid+rows[i].id+"-";
					}else{
						pass = false;
						break;
					}
				}
				if(pass){
					$.ajax({
			            url:'XtijiaojrjgfrAssets',
			            type:'POST', //GET
			            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			            //data:{"rows":JSON.stringify(rows)},
			            data:{"deletetype":"some","deleteid":deleteid},
			            dataType:'json',    
			            success:function(data){
				           	if(data == '1'){ 
				           		$.messager.alert('','提交成功','info',function(r){
				           			$('#dg2').datagrid('reload');
				           		});
				           	}else{  //否则删除失败
				           		$.messager.alert('操作提示','提交失败','error');
				           	}
			            },
			           	error : function(){
			    			$.messager.alert('操作提示','网络异常请稍后再试','error');
			    		}
			        });
				}else{
					$.messager.alert('操作提示','只能提交校验成功的数据','info');
				}
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要提交所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'XtijiaojrjgfrAssets',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','提交成功','info',function(r){
			           			$('#dg2').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','提交失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//提交
function tijiao_lr(){
	var rows = $('#dg3').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要提交选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				var pass = true;
				for (var i = 0; i < rows.length; i++){
					if(rows[i].checkstatus=="1"){
						deleteid = deleteid+rows[i].id+"-";
					}else{
						pass = false;
						break;
					}
				}
				if(pass){
					$.ajax({
			            url:'XtijiaojrjgfrProfit',
			            type:'POST', //GET
			            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			            //data:{"rows":JSON.stringify(rows)},
			            data:{"deletetype":"some","deleteid":deleteid},
			            dataType:'json',    
			            success:function(data){
				           	if(data == '1'){ 
				           		$.messager.alert('','提交成功','info',function(r){
				           			$('#dg3').datagrid('reload');
				           		});
				           	}else{  //否则删除失败
				           		$.messager.alert('操作提示','提交失败','error');
				           	}
			            },
			           	error : function(){
			    			$.messager.alert('操作提示','网络异常请稍后再试','error');
			    		}
			        });
				}else{
					$.messager.alert('操作提示','只能提交校验成功的数据','info');
				}
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要提交所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'XtijiaojrjgfrProfit',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','提交成功','info',function(r){
			           			$('#dg3').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','提交失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//导入Excel相关
function daoru_jc(){
    document.getElementById("importbtn1").disabled = "disabled";
    $('#excelfile1').val('');
    $('#importExcel1').dialog('open').dialog('center');
    $('#importExcel1').dialog({modal:true});
}
//导入选择文件按钮
function inexcel1(){
    var file = document.getElementById("excelfile1").files[0];
    if(file == null){
        document.getElementById("importbtn1").disabled = "disabled";
    }else{
        var fileName = file.name;
        var fileType = fileName.substring(fileName.lastIndexOf('.'),
            fileName.length);
        if (fileType == '.xls' || fileType == '.xlsx'){
            if (file) {
                document.getElementById("importbtn1").disabled = "";
            }
        } else {
            $.messager.alert('提示', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！","error");
            document.getElementById("importbtn1").disabled = "disabled";
        }
    }
}
//导入提交按钮
function excel1(){
    $.messager.progress({
        title: '请稍等',
        msg: '数据正在导入中......'
    });
    $.ajaxFileUpload({
        type: "post",
        url: 'XdaorujrjgfrBaseinfo',
        fileElementId: 'excelfile1',
        secureuri: false,
        dataType: 'json',
        success: function (data) {
            const that = data.msg;
            $.messager.progress('close');
            if (that == "导入成功") {
                $.messager.alert('提示', "导入成功", 'info');
                $("#dg1").datagrid('reload');
                $('#importExcel1').dialog('close');
            } else if (that.startsWith("导入模板不正确")) {
                $.messager.alert('提示', that, 'error');
            } else {
            	$.messager.alert('提示', that, 'error');
            }
        },
        error: function () {
            $.messager.progress('close');
            $.messager.alert('提示', '导入文件错误，请重新导入', 'error');
        }
    });
}

//导入Excel相关
function daoru_zc(){
    document.getElementById("importbtn2").disabled = "disabled";
    $('#excelfile2').val('');
    $('#importExcel2').dialog('open').dialog('center');
    $('#importExcel2').dialog({modal:true});
}
//导入选择文件按钮
function inexcel2(){
    var file = document.getElementById("excelfile2").files[0];
    if(file == null){
        document.getElementById("importbtn2").disabled = "disabled";
    }else{
        var fileName = file.name;
        var fileType = fileName.substring(fileName.lastIndexOf('.'),
            fileName.length);
        if (fileType == '.xls' || fileType == '.xlsx'){
            if (file) {
                document.getElementById("importbtn2").disabled = "";
            }
        } else {
            $.messager.alert('提示', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！","error");
            document.getElementById("importbtn2").disabled = "disabled";
        }
    }
}
//导入提交按钮
function excel2(){
    $.messager.progress({
        title: '请稍等',
        msg: '数据正在导入中......'
    });
    $.ajaxFileUpload({
        type: "post",
        url: 'XdaorujrjgfrAssets',
        fileElementId: 'excelfile2',
        secureuri: false,
        dataType: 'json',
        success: function (data) {
            const that = data.msg;
            $.messager.progress('close');
            if (that == "导入成功") {
                $.messager.alert('提示', "导入成功", 'info');
                $("#dg2").datagrid('reload');
                $('#importExcel2').dialog('close');
            } else if (that.startsWith("导入模板不正确")) {
                $.messager.alert('提示', that, 'error');
            } else {
            	$.messager.alert('提示', that, 'error');
            }
        },
        error: function () {
            $.messager.progress('close');
            $.messager.alert('提示', '导入文件错误，请重新导入', 'error');
        }
    });
}

//导入Excel相关
function daoru_lr(){
    document.getElementById("importbtn3").disabled = "disabled";
    $('#excelfile3').val('');
    $('#importExcel3').dialog('open').dialog('center');
    $('#importExcel3').dialog({modal:true});
}
//导入选择文件按钮
function inexcel3(){
    var file = document.getElementById("excelfile3").files[0];
    if(file == null){
        document.getElementById("importbtn3").disabled = "disabled";
    }else{
        var fileName = file.name;
        var fileType = fileName.substring(fileName.lastIndexOf('.'),
            fileName.length);
        if (fileType == '.xls' || fileType == '.xlsx'){
            if (file) {
                document.getElementById("importbtn3").disabled = "";
            }
        } else {
            $.messager.alert('提示', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！","error");
            document.getElementById("importbtn3").disabled = "disabled";
        }
    }
}
//导入提交按钮
function excel3(){
    $.messager.progress({
        title: '请稍等',
        msg: '数据正在导入中......'
    });
    $.ajaxFileUpload({
        type: "post",
        url: 'XdaorujrjgfrProfit',
        fileElementId: 'excelfile3',
        secureuri: false,
        dataType: 'json',
        success: function (data) {
            const that = data.msg;
            $.messager.progress('close');
            if (that == "导入成功") {
                $.messager.alert('提示', "导入成功", 'info');
                $("#dg3").datagrid('reload');
                $('#importExcel3').dialog('close');
            } else if (that.startsWith("导入模板不正确")) {
                $.messager.alert('提示', that, 'error');
            } else {
            	$.messager.alert('提示', that, 'error');
            }
        },
        error: function () {
            $.messager.progress('close');
            $.messager.alert('提示', '导入文件错误，请重新导入', 'error');
        }
    });
}

//导出
function daochu_jc(){
	var rows = $("#dg1").datagrid('getSelections'); //获取选中行对象
	if(rows.length > 0){
		$.messager.confirm('操作提示','确认将选中的'+rows.length+'条数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
			    var exportExcelUrl = "XdaochujrjgfrBaseinfo?zuhagntai=dtj&selectid="+selectid;
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				window.location.href = "XdaochujrjgfrBaseinfo?zuhagntai=dtj&selectid="+selectid;
			}
		});
	}else{
		$.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
				var exportExcelUrl = "XdaochujrjgfrBaseinfo";
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				window.location.href = "XdaochujrjgfrBaseinfo";
			}
		});	
	}
}
function daochu_zc(){
	var rows = $("#dg2").datagrid('getSelections'); //获取选中行对象
	if(rows.length > 0){
		$.messager.confirm('操作提示','确认将选中的'+rows.length+'条数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
			    var exportExcelUrl = "XdaochujrjgfrAssets?zuhagntai=dtj&selectid="+selectid;
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				window.location.href = "XdaochujrjgfrAssets?zuhagntai=dtj&selectid="+selectid;
			}
		});
	}else{
		$.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
				var exportExcelUrl = "XdaochujrjgfrAssets";
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				window.location.href = "XdaochujrjgfrAssets";
			}
		});	
	}
}
function daochu_lr(){
	var rows = $("#dg3").datagrid('getSelections'); //获取选中行对象
	if(rows.length > 0){
		$.messager.confirm('操作提示','确认将选中的'+rows.length+'条数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
			    var exportExcelUrl = "XdaochujrjgfrProfit?zuhagntai=dtj&selectid="+selectid;
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				window.location.href = "XdaochujrjgfrProfit?zuhagntai=dtj&selectid="+selectid;
			}
		});
	}else{
		$.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
				var exportExcelUrl = "XdaochujrjgfrProfit";
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				window.location.href = "XdaochujrjgfrProfit";
			}
		});	
	}
}
/**
 * 导出excel（带进度条）
 * @param exportExcelUrl
 * @param scanTime 检测是否导出完毕请求间隔 单位毫秒
 * @param interval 进度条更新间隔（每次更新进度10%）  单位毫秒  导出时间越长 请设置越大 200 对应2秒导出时间
 */
function exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval){
    window.location.href = exportExcelUrl;
     var timer = setInterval(function(){
         $.ajax({
                url: isExportUrl,
                type:'POST', //GET
        	    async:true,    //或false,是否异步
                success: function(data){
                    if(data == "success"){
                        $('#exportFile').progressbar('setValue','100');
                        clearInterval(timer);
                        $('#exportFileDialog').dialog('close');
                        $('#exportFile').progressbar('setValue','0');
                    }else{
                    	var value = $('#exportFile').progressbar('getValue');
                    	if(value <= 100){
                    		value += Math.floor(Math.random() * 10);
                        	$('#exportFile').progressbar('setValue', value);
                    	}else{
                        	$('#exportFile').progressbar('setValue', '0');
                    	}
                    }
                },
                error:function(e){
                    console.log(e.responseText);
                }
            }); 
          }, scanTime);
}

//导出模版
function daochumoban_jc(){
	$.messager.confirm('操作提示','确认要导出Excel模版文件吗？',function(r){
		if(r){
			/* $('#exportFileDialog').dialog('open');
			$("#exportFileDialog").dialog({
				   closable: false
			});
			$('#exportFileDialog').dialog({modal:true});
			var scanTime = 2000; //请求间隔毫秒
			var interval = 2000;
			var isExportUrl = "exportJinDuTiao";
			var exportExcelUrl = "XdaochumobanjrjgfrBaseinfo";
			exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
			window.location.href = "XdaochumobanjrjgfrBaseinfo";
		}
	});
}
function daochumoban_zc(){
	$.messager.confirm('操作提示','确认要导出Excel模版文件吗？',function(r){
		if(r){
			/* $('#exportFileDialog').dialog('open');
			$("#exportFileDialog").dialog({
				   closable: false
			});
			$('#exportFileDialog').dialog({modal:true});
			var scanTime = 2000; //请求间隔毫秒
			var interval = 2000;
			var isExportUrl = "exportJinDuTiao";
			var exportExcelUrl = "XdaochumobanjrjgfrAssets";
			exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
			window.location.href = "XdaochumobanjrjgfrAssets";
		}
	});
}
function daochumoban_lr(){
	$.messager.confirm('操作提示','确认要导出Excel模版文件吗？',function(r){
		if(r){
			/* $('#exportFileDialog').dialog('open');
			$("#exportFileDialog").dialog({
				   closable: false
			});
			$('#exportFileDialog').dialog({modal:true});
			var scanTime = 2000; //请求间隔毫秒
			var interval = 2000;
			var isExportUrl = "exportJinDuTiao";
			var exportExcelUrl = "XdaochumobanjrjgfrProfit";
			exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
			window.location.href = "XdaochumobanjrjgfrProfit";
		}
	});
}

//弹出导入IJ文件提示框
function importIJ() {
    document.getElementById("importIJbtn").disabled = "disabled";
    $('#IJfile').val('');
    $('#importIJ').dialog('open').dialog('center');
}
//校验导入文件是否为zip
function inIJ(){
    var file = document.getElementById("IJfile").files[0];
    if(file == null){
        document.getElementById("importIJbtn").disabled = "disabled";
    }else{
        var fileName = file.name;
        var fileType = fileName.substring(fileName.lastIndexOf('.'),
            fileName.length);
        if (fileType == '.zip' || fileType == '.ZIP'){
            if (file) {
                document.getElementById("importIJbtn").disabled = "";
            }
        } else {
            $.messager.alert('提示', "导入文件应该是.zip为后缀,而不是" + fileType + ",请重新选择文件！","error");
            document.getElementById("importIJbtn").disabled = "disabled";
        }
    }
}
//提交IJ文件
function SubmitIJ() {
    $.messager.progress({
        title: '请稍等',
        msg: '数据正在导入中......'
    });
    const formData = new FormData();
    const IJFile = $("#IJfile")[0].files[0];
    formData.append('file', IJFile);
    $.ajax({
        url: 'XImportIJFile',
        data: formData,
        type: 'post',
        processData: false,
        contentType: false,
        success: function (data) {
            $.messager.progress('close');
            const code = data.code;
            const msg = data.msg;
            $.messager.alert('操作提示', msg, 'info');
            $("#dg").datagrid("reload");
            $("#importIJ").dialog('close');
        }
    })
}

//返回
function fanhui(){
	$.messager.progress({
        title: '请稍等',
        msg: '数据正在加载中......'
    });
}

// 新增实际控制人证件类型
function addactctrlidtype(){
    var actctrlidtype = $('#actctrlidtype').val();
    $('#areaactctrlidtype').val(actctrlidtype);
    $('#dialogactctrlidtype').dialog('open').dialog('center').dialog('setTitle','实际控制人证件类型');
}

//新增按钮实际控制人证件类型
function addactctrlidtypeCode(code){
    var areaactctrlidtype = $('#areaactctrlidtype').val();
    if (areaactctrlidtype.length < 60){
        if (areaactctrlidtype == null || areaactctrlidtype == ''){
            $('#areaactctrlidtype').val(code);
        }else {
            $('#areaactctrlidtype').val(areaactctrlidtype + "," + code);
        }
    }else {
        $.messager.alert('提示', '实际控制人证件类型字符长度不能超过60', 'info');
    }
}

//删除按钮实际控制人证件类型
function deleteactctrlidtypeCode(code){
    var areaactctrlidtype = $('#areaactctrlidtype').val();
    if (areaactctrlidtype != null && areaactctrlidtype != ''){
        var areaactctrlidtypes = areaactctrlidtype.split(",")
        for (var i = areaactctrlidtypes.length-1;i>=0;i--){
            if (areaactctrlidtypes[i] == code){
                areaactctrlidtypes.splice(i,1);
                break;
            }
        }
        var areaactctrlidtypeafter = "";
        for (var i = 0;i<areaactctrlidtypes.length;i++){
            areaactctrlidtypeafter = areaactctrlidtypeafter + areaactctrlidtypes[i] + ",";
        }
        $('#areaactctrlidtype').val(areaactctrlidtypeafter.substring(0,areaactctrlidtypeafter.length-1));
    }
}

// 保存实际控制人证件类型
function saveactctrlidtype(){
    var areaactctrlidtype = $('#areaactctrlidtype').val();
    $('#actctrlidtype').val(areaactctrlidtype);
    $('#dialogactctrlidtype').dialog('close');
}

// 取消实际控制人证件类型
function cancelactctrlidtype(){
    $('#dialogactctrlidtype').dialog('close');
}
$.extend($.fn.datebox.defaults.rules, {
	teshu:{
		validator: function (value) {
			var patrn = /[？?！!$%^*|-]|[\s"'\n\r]/;
			return !patrn.test(value);
		},
		message: '不能包含特殊符号和空格'
	},
	normalDate : {
		validator : function(value) {
			var startTmp = new Date('1800-01-01');
			var endTmp = new Date('2100-12-31');
			var nowTmp = new Date(value);
			return nowTmp > startTmp && nowTmp < endTmp;
		},
		message : '日期早于1800-01-01且晚于2100-12-31'
	},
	dateFormat : {
		validator : function(value) {
			var reg = /^\d{4}-\d{2}-\d{2}$/;
			return reg.test(value);
		},
		message : '日期格式必须满足:YYYY-MM-DD。'
	},
	length:{
		validator : function(value) {
			return value.length<30;
		},
		message : '长度小于30'
	},
	lengtha:{
		validator : function(value) {
			return value.length<10;
		},
		message : '长度小于10'
	},
	rate : {
		validator : function(value) {
			if (value.indexOf("%") == -1 && value.indexOf("‰") == -1){
				return true
			}
			return false;
		},
		message : '不能包含‰或%'
	},
	ratelevel : {
		validator : function(value) {
			var reg = /^([0-9]\d{0,2}(\.\d{2})|0\.\d{2})$/;
			return reg.test(value);
		},
		message : '总长度不能超过5位，小数位应保留2位'
	},
	rateTwo:{
		validator : function(value) {
			var twostockprop = $('#twostockprop').val();

			if(twostockprop ==null|| twostockprop==''){
				return  true;

			}else {
				if(value !=null){
					return true;
				}
			}
		},
		message : '不能为空'
	},
	xiaoshu : {
		validator : function(value) {
			var reg = /^(([-?1-9]{1}\d*)|(0{1}))(\.\d{2})$/;
			return reg.test(value);
		},
		message : '小数位应保留2位'
	},


});
</script>
</body>
</html>
