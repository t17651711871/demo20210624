<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.geping.etl.common.entity.Sys_UserAndOrgDepartment" %>
<%@ page import="com.geping.etl.common.util.VariableUtils" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	VariableUtils vu = (VariableUtils)request.getSession().getAttribute("vu");
	String role = vu.getRole();
	String isusedelete = request.getSession().getAttribute("isusedelete")+"";
	String isuseupdate = request.getSession().getAttribute("isuseupdate")+"";
	Sys_UserAndOrgDepartment sys_User = ((Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User"));
	String handlename = sys_User.getLoginid();
	String shifoushenheziji = request.getSession().getAttribute("shifoushenheziji")+"";
    String shenheren = sys_User.getLoginid();
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>单位贷款担保物信息</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
	<table id="dg1" style="height: 480px;" title="单位贷款担保物信息"></table>
	
	<!--表格工具栏-->
	<div id="tb1" style="padding: 5px; height: auto;">
	    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;
		担保合同编码：<input type="text" id="gteecontractcodeParam"  style="height: 23px; width:130px"/>&nbsp;&nbsp;
		贷款合同编码 ：<input type="text" id="loancontractcodeParam"  style="height: 23px; width:130px"/>&nbsp;&nbsp;
		担保物编码 ：  <input type="text" id="gteegoodscodeParam"  style="height: 23px; width:130px"/>&nbsp;&nbsp;
		校验类型 ：
		<select class="easyui-combobox" id="checkstatusParam"  editable=false style="height: 23px; width:145px" >
			<option value="">-未选择-</option>
			<option value="0">0:未校验</option>
			<option value="1">1:校验成功</option>
			<option value="2">2:校验失败</option>
	    </select>&nbsp;&nbsp;
	    操作名 ：
		<select class="easyui-combobox" id="operationnameParam"  editable=false style="height: 23px; width:145px" >
			<option value="">-未选择-</option>
			<option value="1">1:申请删除</option>
	    </select>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="chaxun()">查询</a></br>
		
		<%-- <%if("yes".equals(isusedelete)) {%>
	      <a class="easyui-linkbutton" iconCls="icon-remove" onclick="shenqingshanchu()">申请删除</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<%}%>
		<%if("yes".equals(isuseupdate)) {%>
	      <a class="easyui-linkbutton" iconCls="icon-edit" onclick="shenqingxiugai()">申请修改</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<%}%>
		<a class="easyui-linkbutton" iconCls="icon-ok" onclick="tongyishenqing()">同意申请</a>&nbsp;&nbsp; --%>
		<a class="easyui-linkbutton" iconCls="icon-ok" onclick="agreeApply()">同意申请删除</a>&nbsp;&nbsp;
        <a class="easyui-linkbutton" iconCls="icon-cross" onclick="noAgreeApply()">拒绝申请删除</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-application-get" onclick="agreeAudit()">审核通过</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-cross" onclick="noAgreeAudit()">审核不通过</a>
		<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochu()">导出</a>&nbsp;&nbsp;
	</div>

<!-- 查看详情 -->
<div style="visibility: hidden;">
    <div id="dialog_m" class="easyui-dialog" title="单位贷款担保物信息" data-options="iconCls:'icon-more'" style="width:500px;height:480px;">
          
         <form id="formForMore" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>金融机构代码 :</td>
                 <td><input class="easyui-validatebox" id="financeorgcode_m" name="financeorgcode" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
                 
               <tr>
                 <td>内部机构号:</td>
                 <td><input class="easyui-validatebox" id="financeorginnum_m" name="financeorginnum" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
               <tr>
               <tr>
                 <td>担保合同编码 :</td>
                 <td><input class="easyui-validatebox" id="gteecontractcode_m" name="gteecontractcode" style="height: 23px; width:230px" disabled="disabled"/></td>
                </tr> 
                 
               <tr>
                 <td>被担保合同编码:</td>
                 <td><input class="easyui-validatebox" id="loancontractcode_m" name="loancontractcode" style="height: 23px; width:230px" disabled="disabled"/></td>
              </tr>
                
               <tr> 
                 <td>担保物编码 :</td>
                 <td><input class="easyui-validatebox" id="gteegoodscode_m" name="gteegoodscode" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>担保物类别 :</td>
                 <td>
                 <select id="gteegoodscategory_m" name="gteegoodscategory" style="height: 23px; width:230px" disabled="disabled">
						<!-- <option value="A">A:金融质押品</option> -->
						<option value="A01">A01:保证金</option>
						<option value="A02">A02:存单</option>
						<option value="A03">A03:贵金属</option>
						<option value="A04">A04:债券</option>
						<option value="A05">A05:票据</option>
						<option value="A06">A06:股票（权）</option>
						<option value="A07">A07:基金</option>
						<option value="A08">A08:保单</option>
						<option value="A09">A09:资产管理产品（不含公募金）</option>
						<option value="A99">A99:其他金融质押品</option>
						<!-- <option value="B">B:应收账款押品</option> -->
					    <option value="B01">B01:交易类应收账款</option>
						<option value="B02">B02:各类收费（益）权</option>
						<option value="B99">B99:其他应收账款</option>
						<!-- <option value="C">C:房地产类押品</option> -->
						<option value="C01">C01:居住用房地产</option>
						<option value="C02">C02:商业用房地产</option>
						<option value="C03">C03:居住用房地产建设用地使用权</option>
						<option value="C04">C04:商业用房地产建设用地使用权</option>
						<option value="C05">C05:房产类在建工程</option>
						<option value="C99">C99:其他房地产类押品</option>
						<!-- <option value="D">D:其他类押品</option> -->
					    <option value="D01">D01:存货、仓单和提单</option>
					    <option value="D02">D02:机器设备</option>
					    <option value="D03">D03:交通运输设备</option>
					    <option value="D04">D04:采（探）矿权</option>
					    <option value="D05">D05:林权</option>
					    <option value="D06">D06:其他资源资产</option>
					    <option value="D07">D07:知识产权</option>
					    <option value="D08">D08:农村承包土地的经营权</option>
					    <option value="D09">D09:农民住房财产权</option>
					    <option value="D99">D99:其他以上未包括的押品</option>
	                </select>
                 </td>
               </tr> 
                
               <tr>  
                 <td>权证编号 :</td>
                 <td><input class="easyui-validatebox" id="warrantcode_m" name="warrantcode" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
                
               <tr> 
                 <td>是否第一顺位 :</td>
                 <td> 
                    <select id="isfirst_m" name="isfirst" style="height: 23px; width:230px" disabled="disabled">
						<option value="0">0:否</option>
					   <option value="1">1:是</option>
	                </select>
	              </td>
			   <tr>
			   
			   <tr>
			     <td>评估方式 :</td>
			     <td>
			        <select id="assessmode_m" name="assessmode" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:外部评估</option>
					    <option value="02">02:内部评估</option>
					    <option value="03">03:内外部合作评估</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>评估方法 :</td>
			     <td>
			        <select id="assessmethod_m" name="assessmethod" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:收益法</option>
					    <option value="02">02:市场法</option>
					    <option value="03">03:成本法</option>
					    <option value="04">04:组合法</option>
					    <option value="09">09:其他</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>评估价值 :</td>
			     <td><input class="easyui-validatebox" id="assessvalue_m" name="assessvalue" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>评估基准日 :</td>
			     <td><input id="assessdate_m" name="assessdate" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>担保物票面价值 :</td>
			     <td><input class="easyui-validatebox" id="gteegoodsamt_m" name="gteegoodsamt" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>优先受偿权数额 :</td>
			     <td><input class="easyui-validatebox" id="firstrightamt_m" name="firstrightamt" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>估值周期 :</td>
			     <td>
			        <select id="gzzq_m" name="gzzq" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:年</option>
					    <option value="02">02:半年</option>
					    <option value="03">03:季度</option>
					    <option value="04">04:月</option>
					    <option value="05">05:旬</option>
					    <option value="06">06:周</option>
					    <option value="07">07:日</option>
					    <option value="99">99:其他</option>
	                </select>
			     </td>
			   </tr>

				 <tr>
					 <td>数据日期 :</td>
					 <td><input id="sjrq_m" name="sjrq" style="height: 23px; width:230px" disabled="disabled"/></td>
				 </tr>

				 <!-- <tr>
                   <td>担保物状态 :</td>
                   <td>
                      <select id="gteegoodsstataus_m" name="gteegoodsstataus" style="height: 23px; width:230px" disabled="disabled">
                          <option value="01">01:正常</option>
                          <option value="02">02:已变现处置</option>
                          <option value="03">03:待处理抵债资产</option>
                          <option value="04">04:灭失损毁</option>
                          <option value="05">05:其他</option>
                      </select>
                   </td>
                 </tr>

                 <tr>
                   <td>抵质押率 :</td>
                   <td>
                      <input class="easyui-validatebox" id="mortgagepgerate_m" name="mortgagepgerate" style="height: 23px; width:230px" disabled="disabled"/>
                   </td>
                 </tr> -->
			   
              </table>
           </div>
         </form>
	</div>
</div>
	
<script type="text/javascript">
var shenheren = "";
var shifoushenheziji = "";
var handlename = '<%= handlename%>';
var role = '<%= role%>';
$(function(){
	$('#dialog_m').dialog('close');
    shenheren = '<%= shenheren%>';
    shifoushenheziji = '<%= shifoushenheziji%>';
    
	$("#dg1").datagrid({
		method:'post',
		url:'XfindDWDKDBWXXdsh',
		loadMsg:'数据加载中,请稍后...',
		//singleSelect:true,
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		fitColumns:true,
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			{field:'ck',checkbox:true},
		    {field:'operationname',title:'操作名',width:100,align:'center'},
		    {field:'nopassreason',title:'审核不通过原因',width:100,align:'center'},
		    {field:'checkstatus',title:'校验结果',width:100,align:'center',formatter: function(value,row,index){					
				if(value == '2'){
					return '<span style="color:red;">' + '校验失败' + '</span>';
				}else if(value == '1'){
					return '校验成功';
				}else if(value == '0'){
					return '未校验';
				}else{
					return value;
				}
			}},
			{field:'financeorgcode',title:'金融机构代码',width:100,align:'center'},
			{field:'financeorginnum',title:'内部机构号',width:100,align:'center'},
		    {field:'gteecontractcode',title:'担保合同编码',width:100,align:'center'},
		    {field:'loancontractcode',title:'被担保合同编码',width:100,align:'center'},
		    {field:'gteegoodscode',title:'担保物编码',width:100,align:'center'},
		    {field:'gteegoodscategory',title:'担保物类别',width:100,align:'center'},
		    {field:'warrantcode',title:'权证编号',width:100,align:'center'},
		    {field:'isfirst',title:'是否第一顺位',width:100,align:'center'},
		    {field:'assessmode',title:'评估方式',width:100,align:'center'},
		    {field:'assessmethod',title:'评估方法',width:100,align:'center'},
		    {field:'assessvalue',title:'评估价值',width:100,align:'center'},
		    {field:'assessdate',title:'评估基准日',width:100,align:'center'},
		    {field:'gteegoodsamt',title:'担保物票面价值',width:100,align:'center'},
		    {field:'firstrightamt',title:'优先受偿权数额',width:100,align:'center'},
		    {field:'gzzq',title:'估值周期',width:100,align:'center'},
            {field:'sjrq',title:'数据日期',width:100,align:'center'},
		    // {field:'gteegoodsstataus',title:'担保物状态',width:100,align:'center',hidden:true},
		    // {field:'mortgagepgerate',title:'抵质押率',width:100,align:'center',hidden:true},
		    {field:'operator',title:'操作人',width:100,align:'center'},
		    {field:'operationtime',title:'操作时间',width:100,align:'center'}
    	]],
	    onDblClickRow :function(rowIndex,rowData){
	    	initForm();
	    	var dia = $('#dialog_m').dialog('open');
	    	$("#formForMore").form('load',rowData);
	    	$('#dialog_m').dialog({modal : true});	
	    	//$("#gteegoodscategory_m").combobox({disabled: true});
        	//$("#isfirst_m").combobox({disabled: true});
        	//$("#assessmode_m").combobox({disabled: true});
        	//$("#assessmethod_m").combobox({disabled: true});
        	//$("#assessdate_m").datebox({disabled: true});
        	//$("#gteegoodsstataus_m").combobox({disabled: true});
	    	//关闭弹窗默认隐藏div
			/* $(dia).window({
		    	onBeforeClose: function () {
		    		//初始化表单的元素的状态
		    		initForm();
		    	}
			}); */
	    }
	});
	
	//$("#dg1").datagrid('loadData',datas);
});

//查询
function chaxun(){
    var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
    var loancontractcodeParam = $("#loancontractcodeParam").val();
    var gteegoodscodeParam = $("#gteegoodscodeParam").val();
    var checkstatusParam = $('#checkstatusParam').combobox('getValue');
    var operationnameParam = $('#operationnameParam').combobox('getValue');
    $("#dg1").datagrid("load",{gteecontractcodeParam:gteecontractcodeParam,loancontractcodeParam:loancontractcodeParam,gteegoodscodeParam:gteegoodscodeParam,checkstatusParam:checkstatusParam,operationnameParam:operationnameParam});
}

function initForm(){
	$("#financeorgcode_m").val('');
	$("#financeorginnum_m").val('');
	$("#gteecontractcode_m").val('');
	$("#loancontractcode_m").val('');
	$("#gteegoodscode_m").val('');
	$("#gteegoodscategory_m").val('setValue','');
	$("#warrantcode_m").val('');
	$("#isfirst_m").val('setValue','');
	$("#assessmode_m").val('setValue','');
	$("#assessmethod_m").val('setValue','');
	$("#assessvalue_m").val('');
	$("#assessdate_m").val('setValue','');
    $("#sjrq_m").val('setValue','');
	$("#gteegoodsamt_m").val('');
	$("#firstrightamt_m").val('');
	$("#gzzq_m").val('');
	//$("#gteegoodsstataus_m").val('setValue','');
	//$("#mortgagepgerate_m").val('');
}

function importExcel(){
    $('#importFileDialog').dialog('open');
    $('#importFileDialog').dialog({modal: true});
}


function fileUp(){
	$('#importFileDialog').dialog('close');
	$.messager.alert('提示','导入成功!','info');
}


//导出
function daochu(){
    var rows = $("#dg1").datagrid('getSelections'); //获取选中行对象
    if(rows.length > 0){
        $.messager.confirm('操作提示','确认将选中的'+rows.length+'条数据导出到Excel表格吗？',function(r){
            if(r){
                var selectid = "";
                for (var i = 0; i < rows.length; i++){
                    selectid = selectid+rows[i].id+"-";
                }
                window.location.href = "Xdaochudkdbwx?zuhagntai=dsh&selectid="+selectid;
            }
        });
    }else{
        $.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
            if(r){
                window.location.href = "Xdaochudkdbwx?zuhagntai=dsh";
            }
        });
    }
}

/*同意申请*/
function agreeApply() {
    var rows = $('#dg1').datagrid('getSelections');
    var info = '';
    var id = '';
    for (var i = 0;i < rows.length;i++){
        id = id + rows[i].id + ',';
        if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
            $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
            return;
        }
        if (rows[i].operationname != '申请删除'){
            $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
            return;
        }
    }
    if(rows.length > 0){
        info = '是否同意所选中的申请？';
    }else {
        info = '是否同意所有的申请？';
    }
    var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
    var loancontractcodeParam = $("#loancontractcodeParam").val();
    var gteegoodscodeParam = $("#gteegoodscodeParam").val();
    var checkstatusParam = $('#checkstatusParam').combobox('getValue');
    var operationnameParam = $('#operationnameParam').combobox('getValue');
    $.messager.confirm('提示',info,function (r){
        if(r){
            $.ajax({
                sync: true,
                type: "POST",
                dataType: "json",
                url: "XAgreeApplyXdkdbwx",
                data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"gteegoodscodeParam" : gteegoodscodeParam,"checkstatusParam":checkstatusParam,"operationnameParam":operationnameParam},
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (data) {
                    if (data == 0){
                        $.messager.alert('操作提示','同意成功','info');
                        $("#dg1").datagrid("reload");
                    }else {
                        $.messager.alert('操作提示','同意失败','info');
                    }
                },
                error: function (err) {
                    $.messager.alert('操作提示','同意失败','error');
                }
            });
        }
    });
}

/*拒绝申请*/
function noAgreeApply() {
    var rows = $('#dg1').datagrid('getSelections');
    var info = '';
    var id = '';
    for (var i = 0;i < rows.length;i++){
        id = id + rows[i].id + ',';
        if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
            $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
            return;
        }
        if (rows[i].operationname != '申请删除'){
            $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
            return;
        }
    }
    if(rows.length > 0){
        info = '是否拒绝所选中的申请？';
    }else {
        info = '是否拒绝所有的申请？';
    }
    var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
    var loancontractcodeParam = $("#loancontractcodeParam").val();
    var gteegoodscodeParam = $("#gteegoodscodeParam").val();
    var checkstatusParam = $('#checkstatusParam').combobox('getValue');
    var operationnameParam = $('#operationnameParam').combobox('getValue');
    $.messager.confirm('提示',info,function (r){
        if(r){
            $.ajax({
                sync: true,
                type: "POST",
                dataType: "json",
                url: "XNoAgreeApplyXdkdbwx",
                data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"gteegoodscodeParam" : gteegoodscodeParam,"checkstatusParam":checkstatusParam,"operationnameParam":operationnameParam},
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (data) {
                    if (data == 0){
                        $.messager.alert('操作提示','拒绝成功','info');
                        $("#dg1").datagrid("reload");
                    }else {
                        $.messager.alert('操作提示','拒绝失败','info');
                    }
                },
                error: function (err) {
                    $.messager.alert('操作提示','拒绝意失败','error');
                }
            });
        }
    });
}

/*审核通过*/
function agreeAudit() {
    var rows = $('#dg1').datagrid('getSelections');
    var info = '';
    var id = '';
    for (var i = 0;i < rows.length;i++){
        id = id + rows[i].id + ',';
        if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
            $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
            return;
        }
        if (rows[i].checkstatus != 1){
            $.messager.alert("提示","请选择校验通过的数据！","error");
            return;
        }
        if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
        }else {
            $.messager.alert("提示","请选择操作名为空的数据！","error");
            return;
        }
    }
    if(rows.length > 0){
        info = '是否审核通过所选中的数据？';
    }else {
        info = '是否审核通过所有数据？';
    }
    var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
    var loancontractcodeParam = $("#loancontractcodeParam").val();
    var gteegoodscodeParam = $("#gteegoodscodeParam").val();
    var checkstatusParam = $('#checkstatusParam').combobox('getValue');
    var operationnameParam = $('#operationnameParam').combobox('getValue');
    $.messager.confirm('提示',info,function (r){
        if(r){
            $.ajax({
                sync: true,
                type: "POST",
                dataType: "json",
                url: "XAgreeAuditXdkdbwx",
                data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"gteegoodscodeParam" : gteegoodscodeParam,"checkstatusParam":checkstatusParam,"operationnameParam":operationnameParam},
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (data) {
                    if (data == 0){
                        $.messager.alert('操作提示','审核通过成功','info');
                        $("#dg1").datagrid("reload");
                    }else {
                        $.messager.alert('操作提示','审核通过失败','info');
                    }
                },
                error: function (err) {
                    $.messager.alert('操作提示','审核通过失败','error');
                }
            });
        }
    });
}

/*审核不通过*/
function noAgreeAudit() {
    var rows = $('#dg1').datagrid('getSelections');
    var info = '';
    var id = '';
    for (var i = 0;i < rows.length;i++){
        id = id + rows[i].id + ',';
        if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
            $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
            return;
        }
        if (rows[i].checkstatus != 1){
            $.messager.alert("提示","请选择校验通过的数据！","error");
            return;
        }
        if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
        }else {
            $.messager.alert("提示","请选择操作名为空的数据！","error");
            return;
        }
    }
    if(rows.length > 0){
        info = '是否审核不通过所选中的数据？';
    }else {
        info = '是否审核不通过所有数据？';
    }
    var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
    var loancontractcodeParam = $("#loancontractcodeParam").val();
    var gteegoodscodeParam = $("#gteegoodscodeParam").val();
    var checkstatusParam = $('#checkstatusParam').combobox('getValue');
    var operationnameParam = $('#operationnameParam').combobox('getValue');
    $.messager.prompt('提示',info,function (r){
        if(r){
            console.info("12")
            $.ajax({
                sync: true,
                type: "POST",
                dataType: "json",
                url: "XNoAgreeAuditXdkdbwx",
                data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"gteegoodscodeParam" : gteegoodscodeParam,"checkstatusParam":checkstatusParam,"operationnameParam":operationnameParam,"reason":r},
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (data) {
                    if (data == 0){
                        $.messager.alert('操作提示','审核不通过成功','info');
                        $("#dg1").datagrid("reload");
                    }else {
                        $.messager.alert('操作提示','审核不通过失败','info');
                    }
                },
                error: function (err) {
                    $.messager.alert('操作提示','审核不通过失败','error');
                }
            });
        }else {
            $.messager.alert('操作提示','审核不通过原因必填','info');
        }
    });
}


</script>
</body>
</html>
