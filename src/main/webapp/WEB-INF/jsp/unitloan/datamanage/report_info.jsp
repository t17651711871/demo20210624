<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.geping.etl.common.util.VariableUtils" %>
<%@ page import="com.geping.etl.common.entity.Sys_Auth_Role_Resource" %>
<%@ page import="com.geping.etl.common.entity.Report_Info" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    VariableUtils vu = (VariableUtils) request.getSession().getAttribute("vu");
    String loginId = vu.getLoginId();
    String role = vu.getRole();
    Set<Sys_Auth_Role_Resource> operateReportSet = vu.getOperateReportSet();
    Set<Sys_Auth_Role_Resource> reportSet = vu.getReportSet();
    List<Report_Info> allReportList = (List<Report_Info>) request.getAttribute("reportlist");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>数据管理</title>
    <%@include file="../../common/head.jsp" %>
    <style type="text/css">
        a {
            text-decoration: none;
        }

    </style>
</head>
<body>
<table class="easyui-datagrid" fitColumns="true" title="数据管理" id="dg1" fit="true"
       data-options="autoRowHeight:false,pagination:true,rownumbers:true,toolbar:'#tb',singleSelect:true,pageSize:20,pageList:[20]"
       style="height: 550px">
    <thead>
    <tr>
        <th data-options="field:'name',width:150,align:'center'">表单名称</th>
        <%
            if (role.equals("checker")) {
        %>
        <th data-options="field:'pending_audit',width:80,align:'center'">待审核</th>
        <%
        } else {
        %>
        <th data-options="field:'pending',width:100,align:'center'">待提交</th>
        <th data-options="field:'pending_audit',width:80,align:'center'">待审核</th>
        <th data-options="field:'checkFail',width:80,align:'center'">已审核</th>
        <%
            }
        %>
    </tr>
    </thead>
    <tbody>
    <%
        for (Report_Info li : allReportList) {
            for (Sys_Auth_Role_Resource report : reportSet) {
                if (report.getResValue().equals(li.getCode())) {
    %>
    <tr>
        <td><%=li.getName()%>
        </td>
        <%--报表名称--%>
        <%
            if (role.equals("checker")) {
        %>
        <td><a href="<%=li.getCursorUrl() %>?status=dsh" style="color: blue;"><%=li.getPendingAudit()%></a></td>
        <%
            }else {
        %>
        <td><a href="<%=li.getCursorUrl()%>?status=dtj" style="color: blue"><%=li.getPending()%></a></td>
        <td><a href="<%=li.getCursorUrl() %>?status=dsh" style="color: blue;"><%=li.getPendingAudit()%></a></td>
        <td><a href="<%=li.getGenerateMessageUrl()%>?status=datamanege" style="color: blue"><%=li.getCheckSuccess()%></a></td>
    </tr>
    <%
                    }
                    break;
                }
            }
        }
    %>
    </tbody>
</table>
<div id="tb" style="padding:5px;height:auto;">
    <%
        if (operateReportSet != null) {
            if (!operateReportSet.isEmpty()) {
                for (Sys_Auth_Role_Resource sarr : operateReportSet) {
                    String operateReportName = sarr.getResValue();
    %>
    <%
        if (operateReportName.equals("SHOW_REPORT")) {
    %>
    表单名称:
    <input type="text" style="width:10%;height:32px" id="reportName" name="reportName" value="${reportName}">&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="selectRecord()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <%
        }
    %>
    <%
            }
        }
    } else if (loginId.equals("admin")) {
    %>
    表单名称:
    <input type="text" style="width:10%;height:32px" id="reportName" name="reportName" value="${reportName}">&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="selectRecord()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <!-- <a class="easyui-linkbutton" iconCls="icon-more" onclick="more()">报表数据</a> -->
    <%
        }
    %>
</div>
<script type="text/javascript">
    $(function () {

        $('#dg1').datagrid({loadFilter: pagerFilter});//.datagrid('loadData', getData());

        $(".datagrid-cell-group").attr('style', 'color:red;font-size:large');
    })

    function pagerFilter(data) {
        if (typeof data.length == 'number' && typeof data.splice == 'function') {	// is array
            data = {
                total: data.length,
                rows: data
            }
        }
        var dg = $(this);
        var opts = dg.datagrid('options');
        var pager = dg.datagrid('getPager');
        pager.pagination({
            onSelectPage: function (pageNum, pageSize) {
                opts.pageNumber = pageNum;
                opts.pageSize = pageSize;
                pager.pagination('refresh', {
                    pageNumber: pageNum,
                    pageSize: pageSize
                });
                dg.datagrid('loadData', data);
            }
        });
        if (!data.originalRows) {
            data.originalRows = (data.rows);
        }
        var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
        var end = start + parseInt(opts.pageSize);
        data.rows = (data.originalRows.slice(start, end));
        return data;
    }

    function selectRecord() {
        var reportName = $("#reportName").val();
        window.location.href = "Dreport_info?reportName=" + encode(reportName);
    }

    function handledata(code) {
        $.messager.confirm('操作提示', '手动抓取数据会清空表,也包括已经操作过的数据,是否还需要手动抓取？', function (r) {
            if (r) {
                $.messager.progress({
                    title: '请稍等',
                    msg: '正在抓取数据中......',
                });
                $.ajax({
                    url: 'shandledata',
                    type: 'POST', //GET
                    async: true, //或false,是否异步
                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                    data: {"code": code},
                    dataType: 'text', //返回的数据格式：json/xml/html/script/jsonp/text
                    success: function (data) {
                        $.messager.progress('close');
                        if (data == '0') {
                            $.messager.alert('', '抓取完成', 'info', function (r) {
                                $('#dg1').datagrid('reload');
                                window.location.href = "SDataInfoManageUi";
                            });
                        } else if (data == '1') {
                            $.messager.alert('提示', '抓取过程中遇到异常', 'info');
                        } else if (data == '2') {
                            $.messager.alert('提示', '单位今天暂无数据！如有问题请联系IT人员。', 'info');
                        } else if (data == '3') {
                            $.messager.alert('提示', '个人今天暂无数据！如有问题请联系IT人员。', 'info');
                        }
                    }
                });
            }
        })
    }
</script>
</body>
</html>
