<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>单位贷款担保物信息</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
	<table id="dg1" style="height: 480px;" title="单位贷款担保物信息"></table>
	
	<!--表格工具栏-->
	<div id="tb1" style="padding: 5px; height: auto;">
	    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;
		担保合同编码 ：<input type="text" id="gteecontractcodeParam" name="gteecontractcodeParam" style="height: 23px; width:130px"/>&nbsp;&nbsp;
		贷款合同编码 ：<input type="text" id="loancontractcodeParam" name="loancontractcodeParam" style="height: 23px; width:130px"/>&nbsp;&nbsp;
        担保物编码 ：  <input type="text" id="gteegoodscodeParam" name="gteegoodscodeParam" style="height: 23px; width:130px"/>&nbsp;&nbsp;
		校验类型 ：
		<select class="easyui-combobox" id="checkstatusParam" name="checkstatusParam" editable=false style="height: 23px; width:145px" >
			<option value="">-未选择-</option>
			<option value="0">0:未校验</option>
			<option value="1">1:校验成功</option>
			<option value="2">2:校验失败</option>
	    </select>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="chaxun()">查询</a></br>
		<a class="easyui-linkbutton" iconCls="icon-add" onclick="xinzeng()">新增</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-edit" onclick="xiugai()">修改</a>&nbsp;&nbsp;
		<!-- <a class="easyui-linkbutton" iconCls="icon-remove" onclick="shanchu()">删除</a>&nbsp;&nbsp; -->
		<a class="easyui-linkbutton" iconCls="icon-remove" onclick="shenqingshanchu()">申请删除</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-accept" onclick="jiaoyan()">校验</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-easyicon-net-16" onclick="submitit()">提交</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daoru()">导入</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochu()">导出</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochumoban()">导出模版</a>
	</div>
	
	
<!-- 导入文件dialog -->
<div style="visibility: hidden;">
        <div id="importExcel" class="easyui-dialog" style="width: 600px; height: 160px; padding-left: 30px" title="导入数据">
            <!-- <h2>请选择要导入的Excel文件</h2>
            <table id="importTable" border="0">
                <tr>
                    <td><input type="file" name="file_info" id="file_info" size="60" onchange="fileSelected()" />&nbsp;</td>
                </tr>
                <tr>
                    <td><input type="button" id="importId" value="提交" size="60" onclick="fileUp()" /></td>
                </tr>
            </table> -->
            文件导入:<input type="file" accept=".xls,.xlsx" name="excelfile" id="excelfile" onchange="inexcel()"/><br>
        <input type="button" id="importbtn" value="提交" style="width:50px;margin-top:20px;" onclick="excel()"/>
        </div>
    </div>    	
<!-- 导出进度条 -->
<div style="visibility: hidden;">
  <div id="exportFileDialog" class="easyui-dialog" style="width:550px;height:100px;padding-left: 10px;top:200px;" title="导出数据"> 
	<div align="center"><span id="daorutishi">请您稍等,数据正在导出中......</span></div>
	<div id="exportFile" class="easyui-progressbar" style="width:500px;heigth:50px;"></div>
  </div>
</div>	
	
<div style="visibility: hidden;">
    <div id="dialog1" class="easyui-dialog" title=""  data-options="iconCls:'icon-save',toolbar:'#tbForEditDialog'" style="width:500px;height:500px;">
          <div id="tbForEditDialog" style="padding-left: 30px;padding-top: 10px">
	           <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" id="queding" onclick="saveForEditDialog()">确定</a>&nbsp;&nbsp;
               <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="resetForEdit()">取消</a>
          </div>
          
         <form id="formForEditDepartment" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>金融机构代码 :</td>
                 <td><input class="easyui-validatebox" id="financeorgcode" name="financeorgcode" style="height: 23px; width:230px" maxlength="18" data-options="required:true,validType:['teshu']"/></td>
               </tr> 
                 
               <tr>
                 <td>内部机构号:</td>
                 <td><input class="easyui-validatebox" id="financeorginnum" name="financeorginnum" style="height: 23px; width:230px" maxlength="30" data-options="required:true,validType:['teshua']"/></td>
               </tr>
               <tr>
                 <td>担保合同编码 :</td>
                 <td><input class="easyui-validatebox" id="gteecontractcode" name="gteecontractcode" style="height: 23px; width:230px" data-options="required:true,validType:['teshua','length[1,100]']"/></td>
                </tr> 
                 
               <tr>
                 <td>被担保合同编码:</td>
                 <td><input class="easyui-validatebox" id="loancontractcode" name="loancontractcode" style="height: 23px; width:230px" data-options="required:true,validType:['teshua','length[1,100]']"/></td>
              </tr>
                
               <tr> 
                 <td>担保物编码 :</td>
                 <td><input class="easyui-validatebox" id="gteegoodscode" name="gteegoodscode" style="height: 23px; width:230px" data-options="required:true,validType:['teshua','length[1,100]']"/></td>
			   </tr>
			   
			   <tr>
			     <td>担保物类别 :</td>
                 <td>
                 <select class="easyui-combobox" id="gteegoodscategory" name="gteegoodscategory" editable=false style="height: 23px; width:230px" data-options="required:true">
						<!-- <option value="A">A:金融质押品</option> -->
						<option value="A01">A01:保证金</option>
						<option value="A02">A02:存单</option>
						<option value="A03">A03:贵金属</option>
						<option value="A04">A04:债券</option>
						<option value="A05">A05:票据</option>
						<option value="A06">A06:股票（权）</option>
						<option value="A07">A07:基金</option>
						<option value="A08">A08:保单</option>
						<option value="A09">A09:资产管理产品（不含公募金）</option>
						<option value="A99">A99:其他金融质押品</option>
						<!-- <option value="B">B:应收账款押品</option> -->
						<option value="B01">B01:交易类应收账款</option>
						<option value="B02">B02:各类收费（益）权</option>
						<option value="B99">B99:其他应收账款</option>
						<!-- <option value="C">C:房地产类押品</option> -->
						<option value="C01">C01:居住用房地产</option>
						<option value="C02">C02:商业用房地产</option>
						<option value="C03">C03:居住用房地产建设用地使用权</option>
						<option value="C04">C04:商业用房地产建设用地使用权</option>
						<option value="C05">C05:房产类在建工程</option>
						<option value="C99">C99:其他房地产类押品</option>
						<!-- <option value="D">D:其他类押品</option> -->
						<option value="D01">D01:存货、仓单和提单</option>
						<option value="D02">D02:机器设备</option>
						<option value="D03">D03:交通运输设备</option>
						<option value="D04">D04:采（探）矿权</option>
						<option value="D05">D05:林权</option>
					    <option value="D06">D06:其他资源资产</option>
					    <option value="D07">D07:知识产权</option>
					    <option value="D08">D08:农村承包土地的经营权</option>
					    <option value="D09">D09:农民住房财产权</option>
						<option value="D99">D99:其他以上未包括的押品</option>
	                </select>
                 </td>
               </tr> 
                
               <tr>  
                 <td>权证编号 :</td>
                 <td><input class="easyui-validatebox" id="warrantcode" name="warrantcode" style="height: 23px; width:230px" data-options="required:true,validType:['teshua','length[1,500]']"/></td>
               </tr> 
                
               <tr> 
                 <td>是否第一顺位 :</td>
                 <td> 
                    <select class="easyui-combobox" id="isfirst" name="isfirst" editable=false style="height: 23px; width:230px" >
						<option value="0">0:否</option>
					   <option value="1">1:是</option>
	                </select>
	              </td>
			   <tr>
			   
			   <tr>
			     <td>评估方式 :</td>
			     <td>
			        <select class="easyui-combobox" id="assessmode" name="assessmode" editable=false style="height: 23px; width:230px">
						<option value="01">01:外部评估</option>
					    <option value="02">02:内部评估</option>
					    <option value="03">03:内外部合作评估</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>评估方法 :</td>
			     <td>
			        <select class="easyui-combobox" id="assessmethod" name="assessmethod" editable=false style="height: 23px; width:230px" >
						<option value="01">01:收益法</option>
					    <option value="02">02:市场法</option>
					    <option value="03">03:成本法</option>
					    <option value="04">04:组合法</option>
					    <option value="09">09:其他</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>评估价值 :</td>
			     <td><input class="easyui-validatebox" id="assessvalue" name="assessvalue" style="height: 23px; width:230px" data-options="required:true,validType:['amount']"/></td>
			   </tr>
			   
			   <tr>
			     <td>评估基准日 :</td>
			     <td><input class="easyui-datebox" id="assessdate" name="assessdate" editable=false style="height: 23px; width:230px" data-options="validType:['dateFormat','normalDate']"/></td>
			   </tr>
			   
			   <tr>
			     <td>担保物票面价值 :</td>
			     <td><input class="easyui-validatebox" id="gteegoodsamt" name="gteegoodsamt" style="height: 23px; width:230px" data-options="validType:['amount']"/></td>
			   </tr>
			   
			   <tr>
			     <td>优先受偿权数额 :</td>
			     <td><input class="easyui-validatebox" id="firstrightamt" name="firstrightamt" style="height: 23px; width:230px" data-options="required:true,validType:['amount']"/></td>
			   </tr>
			   
			   <tr>
			     <td>估值周期 :</td>
			     <td>
			        <select class="easyui-combobox" id="gzzq" name="gzzq" style="height: 23px; width:230px" editable=false data-options="required:true">
						<option value="01">01:年</option>
					    <option value="02">02:半年</option>
					    <option value="03">03:季度</option>
					    <option value="04">04:月</option>
					    <option value="05">05:旬</option>
					    <option value="06">06:周</option>
					    <option value="07">07:日</option>
					    <option value="99">99:其他</option>
	                </select>
	                <input type="hidden" id="id" name="id">
			        <input type="hidden" id="orgid" name="orgid">
			        <input type="hidden" id="departid" name="departid">
			        <input type="hidden" id="checkstatus" name="checkstatus">
			        <input type="hidden" id="datastatus" name="datastatus">
			        <input type="hidden" id="operator" name="operator">
			        <input type="hidden" id="operationname" name="operationname">
			        <input type="hidden" id="operationtime" name="operationtime">
			        <input type="hidden" id="nopassreason" name="nopassreason">
			     </td>
			   </tr>

				 <tr>
					 <td>数据日期 :</td>
					 <td><input class="easyui-datebox" id="sjrq" name="sjrq" editable=false style="height: 23px; width:230px" data-options="required:true,validType:['dateFormat','normalDate']"/></td>
				 </tr>

				 <!-- <tr>
                   <td>担保物状态 :</td>
                   <td>
                      <select class="easyui-combobox" id="gteegoodsstataus" name="gteegoodsstataus" editable=false style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项">
                          <option value="01">01:正常</option>
                          <option value="02">02:已变现处置</option>
                          <option value="03">03:待处理抵债资产</option>
                          <option value="04">04:灭失损毁</option>
                          <option value="05">05:其他</option>
                      </select>
                   </td>
                 </tr>

                 <tr>
                   <td>抵质押率 :</td>
                   <td>
                      <input class="easyui-numberbox" id="mortgagepgerate" name="mortgagepgerate" style="height: 23px; width:230px" data-options="required:true,max:9999999999,precision:2" missingMessage="必填项"/>
                   </td>
                 </tr> -->
			   
              </table>
           </div>
         </form>
	</div>
</div>

<!-- 查看详情 -->
<div style="visibility: hidden;">
    <div id="dialog_m" class="easyui-dialog" title="单位贷款担保物信息" data-options="iconCls:'icon-more'" style="width:500px;height:480px;">
          
         <form id="formForMore" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>金融机构代码 :</td>
                 <td><input class="easyui-validatebox" id="financeorgcode_m" name="financeorgcode" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
                 
               <tr>
                 <td>内部机构号:</td>
                 <td><input class="easyui-validatebox" id="financeorginnum_m" name="financeorginnum" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
               <tr>
                 <td>担保合同编码 :</td>
                 <td><input class="easyui-validatebox" id="gteecontractcode_m" name="gteecontractcode" style="height: 23px; width:230px" disabled="disabled"/></td>
                </tr> 
                 
               <tr>
                 <td>被担保合同编码:</td>
                 <td><input class="easyui-validatebox" id="loancontractcode_m" name="loancontractcode" style="height: 23px; width:230px" disabled="disabled"/></td>
              </tr>
                
               <tr> 
                 <td>担保物编码 :</td>
                 <td><input class="easyui-validatebox" id="gteegoodscode_m" name="gteegoodscode" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>担保物类别 :</td>
                 <td>
                 <select id="gteegoodscategory_m" name="gteegoodscategory" style="height: 23px; width:230px" disabled="disabled">
						<!-- <option value="A">A:金融质押品</option> -->
						<option value="A01">A01:保证金</option>
						<option value="A02">A02:存单</option>
						<option value="A03">A03:贵金属</option>
						<option value="A04">A04:债券</option>
						<option value="A05">A05:票据</option>
						<option value="A06">A06:股票（权）</option>
						<option value="A07">A07:基金</option>
						<option value="A08">A08:保单</option>
						<option value="A09">A09:资产管理产品（不含公募金）</option>
						<option value="A99">A99:其他金融质押品</option>
						<!-- <option value="B">B:应收账款押品</option> -->
					    <option value="B01">B01:交易类应收账款</option>
						<option value="B02">B02:各类收费（益）权</option>
						<option value="B99">B99:其他应收账款</option>
						<!-- <option value="C">C:房地产类押品</option> -->
						<option value="C01">C01:居住用房地产</option>
						<option value="C02">C02:商业用房地产</option>
						<option value="C03">C03:居住用房地产建设用地使用权</option>
						<option value="C04">C04:商业用房地产建设用地使用权</option>
						<option value="C05">C05:房产类在建工程</option>
						<option value="C99">C99:其他房地产类押品</option>
						<!-- <option value="D">D:其他类押品</option> -->
					    <option value="D01">D01:存货、仓单和提单</option>
					    <option value="D02">D02:机器设备</option>
					    <option value="D03">D03:交通运输设备</option>
					    <option value="D04">D04:采（探）矿权</option>
					    <option value="D05">D05:林权</option>
					    <option value="D06">D06:其他资源资产</option>
					    <option value="D07">D07:知识产权</option>
					    <option value="D08">D08:农村承包土地的经营权</option>
					    <option value="D09">D09:农民住房财产权</option>
					    <option value="D99">D99:其他以上未包括的押品</option>
	                </select>
                 </td>
               </tr> 
                
               <tr>  
                 <td>权证编号 :</td>
                 <td><input class="easyui-validatebox" id="warrantcode_m" name="warrantcode" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
                
               <tr> 
                 <td>是否第一顺位 :</td>
                 <td> 
                    <select id="isfirst_m" name="isfirst" style="height: 23px; width:230px" disabled="disabled">
						<option value="0">0:否</option>
					   <option value="1">1:是</option>
	                </select>
	              </td>
			   <tr>
			   
			   <tr>
			     <td>评估方式 :</td>
			     <td>
			        <select id="assessmode_m" name="assessmode" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:外部评估</option>
					    <option value="02">02:内部评估</option>
					    <option value="03">03:内外部合作评估</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>评估方法 :</td>
			     <td>
			        <select id="assessmethod_m" name="assessmethod" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:收益法</option>
					    <option value="02">02:市场法</option>
					    <option value="03">03:成本法</option>
					    <option value="04">04:组合法</option>
					    <option value="09">09:其他</option>
	                </select>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>评估价值 :</td>
			     <td><input class="easyui-validatebox" id="assessvalue_m" name="assessvalue" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>评估基准日 :</td>
			     <td><input id="assessdate_m" name="assessdate" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>担保物票面价值 :</td>
			     <td><input class="easyui-validatebox" id="gteegoodsamt_m" name="gteegoodsamt" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>优先受偿权数额 :</td>
			     <td><input class="easyui-validatebox" id="firstrightamt_m" name="firstrightamt" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>估值周期 :</td>
			     <td>
			        <select id="gzzq_m" name="gzzq" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:年</option>
					    <option value="02">02:半年</option>
					    <option value="03">03:季度</option>
					    <option value="04">04:月</option>
					    <option value="05">05:旬</option>
					    <option value="06">06:周</option>
					    <option value="07">07:日</option>
					    <option value="99">99:其他</option>
	                </select>
			     </td>
			   </tr>

				 <tr>
					 <td>数据日期 :</td>
					 <td><input id="sjrq_m" name="sjrq" style="height: 23px; width:230px" disabled="disabled"/></td>
				 </tr>

			 </table>
           </div>
         </form>
	</div>
</div>

<script type="text/javascript">
$(function(){
	$('#dialog1').dialog('close');
	$('#dialog_m').dialog('close');
	$("#importExcel").dialog("close");
	$('#exportFileDialog').dialog('close');
	
	$("#dg1").datagrid({
		method:'post',
		url:'XfindDWDKDBWXXdtj',
		loadMsg:'数据加载中,请稍后...',
		//singleSelect:true,
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		fitColumns:true,
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			{field:'ck',checkbox:true},
			{field:'operationname',title:'操作名',width:100,align:'center'},
			{field:'nopassreason',title:'审核不通过原因',width:100,align:'center'},
			{field:'checkstatus',title:'校验结果',width:100,align:'center',formatter: function(value,row,index){					
				if(value == '2'){
					return '<span style="color:red;">' + '校验失败' + '</span>';
				}else if(value == '1'){
					return '校验成功';
				}else if(value == '0'){
					return '未校验';
				}else{
					return value;
				}
			}},
			{field:'financeorgcode',title:'金融机构代码',width:100,align:'center'},
			{field:'financeorginnum',title:'内部机构号',width:100,align:'center'},
			{field:'gteecontractcode',title:'担保合同编码',width:100,align:'center'},
			{field:'loancontractcode',title:'被担保合同编码',width:100,align:'center'},
			{field:'gteegoodscode',title:'担保物编码',width:100,align:'center'},
			{field:'gteegoodscategory',title:'担保物类别',width:100,align:'center'},
			{field:'warrantcode',title:'权证编号',width:100,align:'center'},
			{field:'isfirst',title:'是否第一顺位',width:100,align:'center'},
			{field:'assessmode',title:'评估方式',width:100,align:'center'},
			{field:'assessmethod',title:'评估方法',width:100,align:'center'},
			{field:'assessvalue',title:'评估价值',width:100,align:'center'},
			{field:'assessdate',title:'评估基准日',width:100,align:'center'},
			{field:'gteegoodsamt',title:'担保物票面价值',width:100,align:'center'},
			{field:'firstrightamt',title:'优先受偿权数额',width:100,align:'center'},
			{field:'gzzq',title:'估值周期',width:100,align:'center'},
            {field:'sjrq',title:'数据日期',width:100,align:'center'},
			// {field:'gteegoodsstataus',title:'担保物状态',width:100,align:'center',hidden:true},
			// {field:'mortgagepgerate',title:'抵质押率',width:100,align:'center',hidden:true},
			{field:'operator',title:'操作人',width:100,align:'center'},
		    {field:'operationtime',title:'操作时间',width:100,align:'center'}
	    ]],
	    onDblClickRow :function(rowIndex,rowData){
	    	initForm();
	    	var dia = $('#dialog_m').dialog('open');
        	$("#formForMore").form('load',rowData);
        	$('#dialog_m').dialog({modal : true});
        	//$("#gteegoodscategory_m").combobox({disabled: true});
        	//$("#isfirst_m").combobox({disabled: true});
        	//$("#assessmode_m").combobox({disabled: true});
        	//$("#assessmethod_m").combobox({disabled: true});
        	//$("#assessdate_m").datebox({disabled: true});
        	//$("#gteegoodsstataus_m").combobox({disabled: true});
        	//关闭弹窗默认隐藏div
    		/* $(dia).window({
    	    	onBeforeClose: function () {
    	    		//初始化表单的元素的状态
    	    		initForm();
    	    	}
    		}); */
	    }
	});
	
	//$("#dg1").datagrid('loadData',datas);
});

//查询
function chaxun(){
	var selectdbhtbm = $("#gteecontractcodeParam").val();
	var selectdkhtbm = $("#loancontractcodeParam").val();
    var selectdkdbwbm = $("#gteegoodscodeParam").val();
	var selectjylx = $('#checkstatusParam').combobox('getValue');
	$("#dg1").datagrid("load",{selectdbhtbm:selectdbhtbm,selectdkhtbm:selectdkhtbm,selectdkdbwbm:selectdkdbwbm,selectjylx:selectjylx});
}
function initForm(){
	//document.getElementById("formForEditDepartment").reset(); 
	//document.getElementById("formForMore").reset(); 
	$("#financeorgcode").val('');
	$("#financeorgcode_m").val('');
	$("#financeorginnum").val('');
	$("#financeorginnum_m").val('');
	$("#gteecontractcode").val('');
	$("#gteecontractcode_m").val('');
	$("#loancontractcode").val('');
	$("#loancontractcode_m").val('');
	$("#gteegoodscode").val('');
	$("#gteegoodscode_m").val('');
	$("#gteegoodscategory").combobox('setValue','');
	$("#gteegoodscategory_m").val('setValue','');
	$("#warrantcode").val('');
	$("#warrantcode_m").val('');
	$("#isfirst").combobox('setValue','');
	$("#isfirst_m").val('setValue','');
	$("#assessmode").combobox('setValue','');
	$("#assessmode_m").val('setValue','');
	$("#assessmethod").combobox('setValue','');
	$("#assessmethod_m").val('setValue','');
	$("#assessvalue").val('');
	$("#assessvalue_m").val('');
	$("#assessdate").datebox('setValue','');
	$("#assessdate_m").val('setValue','');
    $("#sjrq").datebox('setValue','');
    $("#sjrq_m").val('setValue','');
	$("#gteegoodsamt").val('');
	$("#gteegoodsamt_m").val('');
	$("#firstrightamt").val('');
	$("#firstrightamt_m").val('');
	$("#gzzq").combobox('setValue','');
	$("#gzzq_m").val('setValue','');
	//$("#gteegoodsstataus").combobox('setValue','');
	//$("#gteegoodsstataus_m").val('setValue','');
	//$("#mortgagepgerate").val('');
	//$("#mortgagepgerate_m").val('');
	$("#id").val('');
	$("#orgid").val('');
	$("#departid").val('');
	$("#checkstatus").val('');
	$("#datastatus").val('');
	$("#operator").val('');
	$("#operationname").val('');
	$("#operationtime").val('');
	$("#nopassreason").val('');
}
//新增
function xinzeng(){
	$('#queding').linkbutton('enable');
	var dia = $('#dialog1').dialog('open');
	$('#dialog1').dialog({modal:true});
	$('#dialog1').dialog({title:"新增单位贷款担保物信息"});
	initForm();
	//关闭弹窗默认隐藏div
	/* $(dia).window({
    	onBeforeClose: function () {
    		//初始化表单的元素的状态
    		initForm();
    	}
	}); */
}

//修改
function xiugai(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length != 1){
		$.messager.alert('操作提示','请选择一条数据修改','info');
	}else{
		initForm();
		$("#formForEditDepartment").form('load',rows[0]);
		$('#queding').linkbutton('enable');
		var dia = $('#dialog1').dialog('open');
		$('#dialog1').dialog({modal:true});
		$('#dialog1').dialog({title:"修改单位贷款担保物信息"});
		//关闭弹窗默认隐藏div
		/* $(dia).window({
	    	onBeforeClose: function () {
	    		//初始化表单的元素的状态
	    		initForm();
	    	}
		}); */
	}
}

//新增或修改点击确定保存
function saveForEditDialog(){
	//$('#queding').linkbutton('disable');
	$("#formForEditDepartment").form('submit',{
		url:'XsaveOrUpdatedkdbwx',
        contentType:'application/x-www-form-urlencoded; charset=UTF-8',
        dataType:'json',
       	onSubmit:function(){
       		var isValidate = $("#formForEditDepartment").form('validate');
       		if(!isValidate){
       			return isValidate;
       		}else{
       			$.messager.progress({
					title:'请稍等',
					msg:'处理中......',
					timeout:10000,
				});  		
       		}
       	},
       	success:function(res){
       		$.messager.progress('close');
       		if(res == '1'){
   				$.messager.alert('','保存成功','info',function(r){
   					//$('#formForEditDepartment').form('clear');  
   					$('#dialog1').dialog('close');
   					$('#dg1').datagrid('reload');
   	            });
   			}else if(res == '0'){
   				$.messager.alert('操作提示','保存失败','error');
   			} 	
       	},
       	error : function(){
			$.messager.progress('close');
			$('#dialog1').dialog('close');
			$.messager.alert('操作提示','网络异常请稍后再试','error');
		}
	});
}

//取消，关闭新增或修改弹窗
function resetForEdit(){
	$('#dialog1').dialog('close');
}

//删除
function shanchu(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要删除选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				for (var i = 0; i < rows.length; i++){
					deleteid = deleteid+rows[i].id+"-";
				}
				$.ajax({
		            url:'Xdeletedkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"some","deleteid":deleteid},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要删除所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'Xdeletedkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//申请删除
function shenqingshanchu(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要申请删除选中的数据吗',function(r){
			if(r){
				var deleteid = "";
				for (var i = 0; i < rows.length; i++){
					deleteid = deleteid+rows[i].id+"-";
				}
				$.ajax({
		            url:'Xshenqingdeletedkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"some","deleteid":deleteid},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','申请删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','申请删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要申请删除所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'Xshenqingdeletedkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','申请删除成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','申请删除失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//校验
// function jiaoyan(){
// 	var rows = $('#dg1').datagrid('getSelections');
// 	if(rows.length > 0){
// 		$.messager.confirm('操作提示','确定要校验选中的数据吗',function(r){
// 			if(r){
// 				var pass = true;
// 				for (var i = 0; i < rows.length; i++){
// 					if(rows[i].checkstatus=='0'){
//
// 					}else{
// 						pass = false;
// 						break;
// 					}
// 				}
// 				if(pass){
// 					$.ajax({
// 			            url:'Xcheckdkdbwx',
// 			            type:'POST', //GET
// 			            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
// 			            data:{"checktype":"some","rows":JSON.stringify(rows)},
// 			            //data:{"checktype":"some","checkid":checkid},
// 			            dataType:'text',
// 			            success:function(res){
// 				           	if(res == '1'){
// 				           		$.messager.alert('','校验成功','info',function(r){
// 				           			$('#dg1').datagrid('reload');
// 				           		});
// 				           	}else{
// 				           		//否则删除失败
// 				           		/* $.messager.alert('','校验失败'+res,'error',function(r){
// 				           			$('#dg1').datagrid('reload');
// 				           		}); */
// 				           		$('#dg1').datagrid('reload');
// 								$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
// 								window.location.href = "XdownFileCheckdkdbwx";
// 				           	}
// 			            },
// 			           	error : function(){
// 			    			$.messager.alert('操作提示','网络异常请稍后再试','error');
// 			    		}
// 			        });
// 				}else{
// 					$.messager.alert('操作提示','只能校验未校验的数据','info');
// 				}
// 			}
// 		});
// 	}else{
// 		$.messager.confirm('操作提示','确定要校验所有数据吗',function(r){
// 			if(r){
// 				$.ajax({
// 		            url:'Xcheckdkdbwx',
// 		            type:'POST', //GET
// 		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
// 		            //data:{"rows":JSON.stringify(rows)},
// 		            data:{"checktype":"all"},
// 		            dataType:'text',
// 		            success:function(res){
// 			           	if(res == '1'){
// 			           		$.messager.alert('','校验成功','info',function(r){
// 			           			$('#dg1').datagrid('reload');
// 			           		});
// 			           	}else if (res =='13'){
//                             $.messager.alert('操作提示','无可校验数据','error');
// 						} else{
// 			           		//否则删除失败
// 			           		//$.messager.alert('操作提示','校验失败','error');
// 			           		$('#dg1').datagrid('reload');
// 							$.messager.alert('操作提示','校验完成,将自动下载校验结果','info', function (r) {
// 								window.location.href = "XdownFileCheckdkdbwx";
// 							});
// 			           	}
// 		            },
// 		           	error : function(){
// 		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
// 		    		}
// 		        });
// 			}
// 		});
// 	}
// }
function jiaoyan(){
    var row = $("#dg1").datagrid("getSelections");
    var id = '';
    for( var dataIndex in row){
        id = id + row[dataIndex].id + ",";
        if (row[dataIndex].checkstatus != 0){
            $.messager.alert("提示", "只能校验未校验的数据", "info");
            return;
        }
    }
    var info = '';
    if(id != ''){
        info = '是否校验选中的数据？';
    }else {
        info = '是否校验所有数据？'
    }
	var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
    var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
    var gteegoodscodeParam = $("#gteegoodscodeParam").val().trim()
    var checkstatusParam = $("#checkstatusParam").combobox("getValue");
    $.messager.confirm('提示',info,function (r) {
        if (r) {
            $.messager.progress({
                title: '请稍等',
                msg: '正在校验数据中......',
                timeout: 100000,
            });
            $.ajax({
                url: "Xcheckdkdbwx",
                dataType: "json",
                async: true,
                data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"gteegoodscodeParam":gteegoodscodeParam,"checkstatusParam" : checkstatusParam},
                type: "POST",
                success: function (data) {
                    $("#dg1").datagrid("reload");
                    $.messager.progress('close');
                    if (data.msg == "1") {
                        $.messager.alert('提示', '校验成功！', 'info');
                    } else if (data.msg == "-1") {
                        $.messager.alert('提示', '没有可校验的数据！', 'info');
                    } else {
                        $.messager.alert('提示', '校验完成,将自动下载校验结果', 'info', function (r) {
                            window.location.href = "XDownLoadCheckDBW";
                        });
                    }
                },
                error: function () {
                    $.messager.progress('close');
                    $.messager.alert("提示", "校验出错，请重新校验！", "error");
                }
            });
        }
    })
}
//提交
function tijiao(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要提交选中的数据吗',function(r){
			if(r){
				var pass = true;
				var deleteid = "";
				for (var i = 0; i < rows.length; i++){
					if(rows[i].checkstatus=="1"){
						deleteid = deleteid+rows[i].id+"-";
					}else{
						pass = false;
						break;
					}
				}
				if(pass){
					$.ajax({
			            url:'Xtijiaodkdbwx',
			            type:'POST', //GET
			            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			            //data:{"rows":JSON.stringify(rows)},
			            data:{"deletetype":"some","deleteid":deleteid},
			            dataType:'json',    
			            success:function(data){
				           	if(data == '1'){ 
				           		$.messager.alert('','提交成功','info',function(r){
				           			$('#dg1').datagrid('reload');
				           		});
				           	}else{  //否则删除失败
				           		$.messager.alert('操作提示','提交失败','error');
				           	}
			            },
			           	error : function(){
			    			$.messager.alert('操作提示','网络异常请稍后再试','error');
			    		}
			        });
				}else{
					$.messager.alert('操作提示','只能提交校验成功的数据','info');
				}
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要提交所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'Xtijiaodkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"deletetype":"all"},
		            dataType:'json',    
		            success:function(data){
			           	if(data == '1'){ 
			           		$.messager.alert('','提交成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  //否则删除失败
			           		$.messager.alert('操作提示','提交失败','error');
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}





//提交
function submitit(){
    var rows = $('#dg1').datagrid('getSelections');
    var info = '';
    var id = '';
    if(rows.length > 0){
        for(var i = 0;i < rows.length;i++){
            if(rows[i].checkstatus != '1'){
                $.messager.alert("提示", "只能提交校验成功的数据", "info");
                return;
            }
            id = id + rows[i].id + ',';
        }
        info = '是否提交所选中的数据？';
    }else {
        info = '您没有选择数据，是否提交所有数据？';
    }
    $.messager.confirm('提示',info,function (r){
        if(r){
            var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
            var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
            var gteegoodscodeParam = $("#gteegoodscodeParam").val().trim()
            var checkstatusParam = $("#checkstatusParam").combobox("getValue");

            $.messager.confirm('提示',info,function (r){
                if(r){
                    $.ajax({
                        sync: true,
                        type: "POST",
                        dataType: "json",
                        url: "XsubmitXdkdbwx",
                        data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"gteegoodscodeParam" : gteegoodscodeParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                        contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                        success: function (data) {
                            if (data == 0){
                                $.messager.alert('操作提示','提交成功','info');
                                $("#dg1").datagrid("reload");
                            }else {
                                $.messager.alert('操作提示','提交失败','info');
                            }
                        },
                        error: function (err) {
                            $.messager.alert('操作提示','提交失败','error');
                        }
                    });
                    $("#dg1").datagrid("reload");
                }
            });
        }
    });
}

















//导入Excel相关
function daoru(){
    document.getElementById("importbtn").disabled = "disabled";
    $('#excelfile').val('');
    $('#importExcel').dialog('open').dialog('center');
    $('#importExcel').dialog({modal:true});
}
//导入选择文件按钮
function inexcel(){
    var file = document.getElementById("excelfile").files[0];
    if(file == null){
        document.getElementById("importbtn").disabled = "disabled";
    }else{
        var fileName = file.name;
        var fileType = fileName.substring(fileName.lastIndexOf('.'),
            fileName.length);
        if (fileType == '.xls' || fileType == '.xlsx'){
            if (file) {
                document.getElementById("importbtn").disabled = "";
            }
        } else {
            $.messager.alert('提示', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！","error");
            document.getElementById("importbtn").disabled = "disabled";
        }
    }
}
//导入提交按钮
function excel(){
    $.messager.progress({
        title: '请稍等',
        msg: '数据正在导入中......'
    });
    $.ajaxFileUpload({
        type: "post",
        url: 'Xdaorudkdbwx',
        fileElementId: 'excelfile',
        secureuri: false,
        dataType: 'json',
        success: function (data) {
            const that = data.msg;
            $.messager.progress('close');
            if (that == "导入成功") {
                $.messager.alert('提示', "导入成功", 'info');
                $("#dg1").datagrid('reload');
                $('#importExcel').dialog('close');
            } else if (that.startsWith("导入模板不正确")) {
                $.messager.alert('提示', that, 'error');
            } else {
            	$.messager.alert('提示', that, 'error');
            }
        },
        error: function () {
            $.messager.progress('close');
            $.messager.alert('提示', '导入文件发生未知错误，请重新刷新', 'error');
        }
    });
}

//导出
function daochu(){
	var rows = $("#dg1").datagrid('getSelections'); //获取选中行对象
	if(rows.length > 0){
		$.messager.confirm('操作提示','确认将选中的'+rows.length+'条数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
			    var exportExcelUrl = "Xdaochudkdbwx?zuhagntai=dtj&selectid="+selectid;
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				window.location.href = "Xdaochudkdbwx?zuhagntai=dtj&selectid="+selectid;
			}
		});
	}else{
		$.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
				var exportExcelUrl = "Xdaochudkdbwx";
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				window.location.href = "Xdaochudkdbwx?zuhagntai=dtj";
			}
		});	
	}
}
/**
 * 导出excel（带进度条）
 * @param exportExcelUrl
 * @param scanTime 检测是否导出完毕请求间隔 单位毫秒
 * @param interval 进度条更新间隔（每次更新进度10%）  单位毫秒  导出时间越长 请设置越大 200 对应2秒导出时间
 */
function exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval){
    window.location.href = exportExcelUrl;
     var timer = setInterval(function(){
         $.ajax({
                url: isExportUrl,
                type:'POST', //GET
        	    async:true,    //或false,是否异步
                success: function(data){
                    if(data == "success"){
                        $('#exportFile').progressbar('setValue','100');
                        clearInterval(timer);
                        $('#exportFileDialog').dialog('close');
                        $('#exportFile').progressbar('setValue','0');
                    }else{
                    	var value = $('#exportFile').progressbar('getValue');
                    	if(value <= 100){
                    		value += Math.floor(Math.random() * 10);
                        	$('#exportFile').progressbar('setValue', value);
                    	}else{
                        	$('#exportFile').progressbar('setValue', '0');
                    	}
                    }
                },
                error:function(e){
                    console.log(e.responseText);
                }
            }); 
          }, scanTime);
}

//导出模版
function daochumoban(){
	$.messager.confirm('操作提示','确认要导出Excel模版文件吗？',function(r){
		if(r){
			/* $('#exportFileDialog').dialog('open');
			$("#exportFileDialog").dialog({
				   closable: false
			});
			$('#exportFileDialog').dialog({modal:true});
			var scanTime = 2000; //请求间隔毫秒
			var interval = 2000;
			var isExportUrl = "exportJinDuTiao";
			var exportExcelUrl = "Xdaochumobandkdbwx";
			exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
			window.location.href = "Xdaochumobandkdbwx";
		}
	});
}

//前台校验
$.extend($.fn.datebox.defaults.rules, {
    dateFormat : {
        validator : function(value) {
            var reg = /^(\d{4})-(\d{2})-(\d{2})$/;
            return reg.test(value);
        },
        message : '日期格式必须满足:YYYY-MM-DD。'
    },
    normalDate : {
        validator : function(value) {
            var startTmp = new Date('1800-01-01');
            var endTmp = new Date('2100-12-31');
            var nowTmp = new Date(value);
            return nowTmp > startTmp && nowTmp < endTmp;
        },
        message : '评估基准日早于1800-01-01且晚于2100-12-31'
    },
    teshu:{
        validator: function (value) {
            var patrn = /[？?！!^]/;
            return !patrn.test(value);
        },
        message: '不能包含特殊符号和空格'
    },
    teshua:{
        validator: function (value) {
            var patrn = /[？?！!^]/;
            return !patrn.test(value);
        },
        message: '不能包含特殊符号和空格'
    },
    amount : {
        validator : function(value) {
            var reg = /^([0-9]\d{0,18}(\.\d{2})|0\.\d{2})$/;
            return reg.test(value);
        },
        message : '总长度不超过20位的，精度保留小数点后两位。'
    },
});
</script>
</body>
</html>
