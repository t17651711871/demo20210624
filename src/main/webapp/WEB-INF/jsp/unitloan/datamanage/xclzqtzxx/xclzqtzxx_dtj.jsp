<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">
    <title>存量债券投资信息</title>
    <%@include file="../../../common/head.jsp" %>
</head>
<body>


<!--表格工具栏-->
<div id="tb1" style="padding: 5px; height: auto;">
    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;
    <label>金融机构代码：</label><input id="financeorgcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>校验类型：</label>
    <select class='easyui-combobox' id='checkstatusParam' name='checkstatusParam' editable=false
            style='height: 23px; width:80px'>
        <option value=''>=请选择=</option>
        <option value='0'>未校验</option>
        <option value='1'>校验成功</option>
        <option value='2'>校验失败</option>
    </select>&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected('dtj')">查询</a>&nbsp;&nbsp;<br>


    <c:forEach var="operate" items="${sessionScope.vu.operateReportSet}">
        <c:choose>
            <c:when test="${operate.resValue=='ADD'}">
                <a class="easyui-linkbutton" iconCls="icon-add" onclick="add('新增存量债券投资信息')">新增</a>&nbsp;&nbsp;&nbsp;&nbsp;
            </c:when>
            <c:when test="${operate.resValue=='EDIT'}">
                <a class="easyui-linkbutton" iconCls="icon-edit" onclick="edit('修改存量债券投资信息')">修改</a>&nbsp;&nbsp;&nbsp;&nbsp;
            </c:when>
            <c:when test="${operate.resValue=='APPLYDELETE'}">
                <a class="easyui-linkbutton" iconCls="icon-remove" onclick="applyDelete('Xclzqtzxx')">申请删除</a>&nbsp;&nbsp;&nbsp;&nbsp;
            </c:when>
            <c:when test="${operate.resValue=='IMPORT'}">
                <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="openExcelSelectDialog()">导入</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </c:when>
            <c:when test="${operate.resValue=='EXPORT'}">
                <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="downloadExcel('Xclzqtzxx','0')">导出</a>&nbsp;&nbsp;&nbsp;
                <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="downloadExcelTemplate('Xclzqtzxx')">导出模板</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </c:when>
            <c:when test="${operate.resValue=='CHECKOUT'}">
                <a class="easyui-linkbutton" iconCls="icon-accept" onclick="check('Xclzqtzxx')">校验</a>&nbsp;&nbsp;&nbsp;
            </c:when>
            <c:when test="${operate.resValue=='SUBMIT'}">
                <a class="easyui-linkbutton" iconCls="icon-easyicon-net-16" onclick="submitit('Xclzqtzxx')">提交</a>&nbsp;
            </c:when>
            <c:otherwise>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</div>

<!--表格-->
<table id="dg1" style="height: 480px;" title="存量债券投资信息"></table>

<!-- 导入文件dialog -->
<div style="visibility: hidden;">
    <div id="importExcel" class="easyui-dialog" style="width: 600px; height: 150px;top: 100px;padding: 20px;"
         title="数据导入" data-options="modal:true,closed:true">
        文件导入:<input type="file" accept=".xls,.xlsx" name="excelfile" id="excelfile"
                    onchange="selectExcelOnchange()"/><br>
        <input type="button" id="importbtn" value="提交" style="width:50px;margin-top:20px;"
               onclick="excelImport('Xclzqtzxx')"/>
    </div>
</div>

<!--新增修改窗口-->
<div id="dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;"
     data-options="resizable:true,modal:true,closed:true">
    <div id="tbForAddDialog" style="padding-left: 100px;padding-top: 10px">
        <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="save('Xclzqtzxx')">确定</a>&nbsp;&nbsp;
        <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="cancel()">取消</a>
    </div>

    <!-- 信息录入 -->
    <div align="center" style="padding-top: 30px">
        <form id="formForAdd" method="post">
            <input id="id" class="backId" name="id" type="hidden">
            <table class='enterTable' id='info_enterTable_1'>
                <tr>
                    <td align='right'>
                        金融机构代码:
                    </td>
                    <td>
                        <input id='info_financeorgcode' name='financeorgcode' style='width:173px;'/>
                    </td>
                    <td align='right'>
                        内部机构号:
                    </td>
                    <td>
                        <input id='info_financeorginnum' name='financeorginnum' style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        债券代码:
                    </td>
                    <td>
                        <input id='info_zqdm' name='zqdm' style='width:173px;'/>
                    </td>
                    <td align='right'>
                        债券总托管机构:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_zqztgjg' name='zqztgjg' style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=2',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >

                        </select>
                    </td>
                </tr>

                <tr>
                    <td align='right'>
                        债券品种:
                    </td>
                    <td>
                        <%--<select class='easyui-combobox' id='info_zqpz' name='zqpz'   style='width:173px;'>
                            <option value='GB01'>GB01：凭证式国债</option>
                            <option value='GB02'>GB02：储蓄国债(电子式)</option>
                            <option value='GB03'>GB03：记账式国债</option>
                            <option value='GB041'>GB041：地方政府一般债券</option>
                            <option value='GB042'>GB042：地方政府专项债券</option>
                            <option value='CBN'>CBN：中央银行票据</option>
                            <option value='FB00'>FB00：政策性银行债券</option>
                            <option value='FB01'>FB01：商业银行普通债</option>
                            <option value='FB02'>FB02：商业银行次级债</option>
                            <option value='FB03'>FB03：商业银行混合资本债</option>
                            <option value='FB04'>FB04：商业银行二级资本债券</option>
                            <option value='FB05'>FB05：其他一级资本债券</option>
                            <option value='FB06'>FB06：金融机构短期融资券</option>
                            <option value='MTN'>MTN：中期票据（MTN）</option>
                            <option value='CP'>CP：短期融资券（CP）</option>
                            <option value='SCP'>SCP：超短期融资券（SCP）</option>
                            <option value='SMECN1'>SMECN1：中小企业集合票据（SMECN）</option>
                            <option value='SMECN2'>SMECN2：区域集优中小企业集合票据 （SMECNⅡ）</option>
                            <option value='PPN'>PPN：非公开定向债务融资工具（PPN）</option>
                            <option value='ABN'>ABN：非金融企业资产支持票据(ABN)</option>
                            <option value='PRB'>PRB：项目收益债券</option>
                            <option value='PRN'>PRN：非金融企业项目收益票据</option>
                            <option value='CB01'>CB01：企业债券</option>
                            <option value='CB02'>CB02：中小企业私募债</option>
                            <option value='CB03'>CB03：公司债券</option>
                            <option value='CB04'>CB04：可转换公司债券</option>
                            <option value='CB05'>CB05：分离交易的可转换公司债券</option>
                            <option value='CB06'>CB06：证券公司次级债</option>
                            <option value='CB07'>CB07：保险公司次级债</option>
                            <option value='CB08'>CB08：保险公司资本补充债券</option>
                            <option value='MDBB'>MDBB：国际开发机构人民币债券</option>
                            <option value='ABS01'>ABS01：信贷资产 ABS</option>
                            <option value='ABS02'>ABS02：企业资产 ABS</option>
                            <option value='TB99'>TB99：其他</option>
                        </select>--%>
                        <select class='easyui-combobox' id='info_zqpz' name='zqpz' style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=1',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >
                        </select></td>
                    <td align='right'>
                        债券信用级别:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_zqxyjg' name='zqxyjg' style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=3',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >
                        </select></td>
                </tr>


                <tr>
                    <td align='right'>
                        币种:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_bz' name='bz' style='width:173px;'>
                            <c:forEach items='${baseCurrencyList}' var='aa'>
                                <option value='${aa.currencycode}'>${aa.currencycode}-${aa.currencyname}</option>
                            </c:forEach>
                        </select></td>
                    <td align='right'>
                        债券余额:
                    </td>
                    <td>
                        <input id='info_zqye' name='zqye' style='width:173px;'/></td>
                </tr>
                <tr>
                    <td align='right'>
                        债券余额折人民币:
                    </td>
                    <td>
                        <input id='info_zqyezrmb' name='zqyezrmb' style='width:173px;'/></td>
                    <td align='right'>
                        债权债务登记日:
                    </td>
                    <td>
                        <input class='easyui-datebox' id='info_info_zqzwdj' name='zqzwdj' style='width:173px;'/>
                    </td>
                </tr>

                <tr>
                    <td align='right'>
                        起息日:
                    </td>
                    <td>
                        <input class='easyui-datebox' id='info_qxr' name='qxr' style='width:173px;'/>
                    </td>
                    <td align='right'>
                        兑付日期:
                    </td>
                    <td>
                        <input class='easyui-datebox' id='info_dfrq' name='dfrq' style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        票面利率:
                    </td>
                    <td>
                        <input id='info_pmll' name='pmll' style='width:173px;'/>
                    </td>
                    <td align='right'>
                        发行人证件代码:
                    </td>
                    <td>
                        <input id='info_fxrzjdm' name='fxrzjdm' style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        发行人地区代码:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_fxrdqdm' name='fxrdqdm' style='width:173px;'>
                            <c:forEach items='${baseCountryList}' var='aa'>
                                <option value='${aa.countrycode}'>${aa.countrycode}-${aa.countryname}</option>
                            </c:forEach>
                            <c:forEach items='${baseAreaList}' var='aa'>
                                <option value='${aa.areacode}'>${aa.areacode}-${aa.areaname}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td align='right'>
                        发行人行业 :
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_fxrhy' name='fxrhy' style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=14',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        发行人企业规模:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_fxrqygm' name='fxrqygm' style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=4',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >
                        </select>
                    </td>
                    <td align='right'>
                        发行人经济成分:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_fxrjjcf' name='fxrjjcf' style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=5',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        发行人国民经济部门 :
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_fxrgmjjbm' name='fxrgmjjbm'  style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=6',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >
                        </select>                    </td>
                    <td align='right'>
                        数据日期 :
                    </td>
                    <td>
                        <input class='easyui-datebox' id='info_sjrq' name='sjrq' style='width:173px;'/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<!--详情窗口-->
<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;"
     data-options="resizable:true,modal:true,closed:true">
    <!-- 信息录入 -->
    <div align="center" style="padding-top: 30px">
        <form id="info_formForAdd" method="post">
            <input id="info_id" class="backId" name="id" type="hidden">
            <table class='enterTable' id='info_enterTable'>
                <tr>
                    <td align='right'>
                        金融机构代码:
                    </td>
                    <td>
                        <input id='financeorgcode' name='financeorgcode' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        内部机构号:
                    </td>
                    <td>
                        <input id='financeorginnum' name='financeorginnum' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        债券代码:
                    </td>
                    <td>
                        <input id='zqdm' name='zqdm' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        债券总托管机构:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='zqztgjg' name='zqztgjg' disabled="disabled"
                                style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=2',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >
                        </select>
                    </td>
                </tr>

                <tr>
                    <td align='right'>
                        债券品种:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='zqpz' name='zqpz' disabled='disabled' style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=1',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >
                        </select>
                    </td>
                    <td align='right'>
                        债券信用级别:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='zqxyjg' name='zqxyjg' disabled="disabled"
                                style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=3',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >

                        </select>
                    </td>
                </tr>


                <tr>
                    <td align='right'>
                        币种:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='bz' name='bz' disabled="disabled" style='width:173px;'>
                            <c:forEach items='${baseCurrencyList}' var='aa'>
                                <option value='${aa.currencycode}'>${aa.currencycode}-${aa.currencyname}</option>
                            </c:forEach>
                        </select></td>
                    <td align='right'>
                        债券余额:
                    </td>
                    <td>
                        <input id='zqye' name='zqye' disabled="disabled" style='width:173px;'/></td>
                </tr>
                <tr>
                    <td align='right'>
                        债券余额折人民币:
                    </td>
                    <td>
                        <input id='zqyezrmb' name='zqyezrmb' disabled="disabled" style='width:173px;'/></td>
                    <td align='right'>
                        债权债务登记日:
                    </td>
                    <td>
                        <input class='easyui-datebox' id='info_zqzwdj' name='zqzwdj' disabled="disabled"
                               style='width:173px;'/>
                    </td>
                </tr>

                <tr>
                    <td align='right'>
                        起息日:
                    </td>
                    <td>
                        <input class='easyui-datebox' id='qxr' name='qxr' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        兑付日期:
                    </td>
                    <td>
                        <input class='easyui-datebox' id='dfrq' name='dfrq' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        票面利率:
                    </td>
                    <td>
                        <input id='pmll' name='pmll' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        发行人证件代码:
                    </td>
                    <td>
                        <input id='fxrzjdm' name='fxrzjdm' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        发行人地区代码:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='fxrdqdm' name='fxrdqdm' disabled="disabled"
                                style='width:173px;'>
                            <c:forEach items='${baseCountryList}' var='aa'>
                                <option value='${aa.countrycode}'>${aa.countrycode}-${aa.countryname}</option>
                            </c:forEach>
                            <c:forEach items='${baseAreaList}' var='aa'>
                                <option value='${aa.areacode}'>${aa.areacode}-${aa.areaname}</option>
                            </c:forEach>

                        </select>
                    </td>
                    <td align='right'>
                        发行人行业 :
                    </td>
                    <td>
                        <select class='easyui-combobox' id='fxrhy' name='fxrhy' disabled="disabled"
                                style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=14',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        发行人企业规模:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='fxrqygm' name='fxrqygm' disabled="disabled"
                                style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=4',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >
                        </select>
                    </td>
                    <td align='right'>
                        发行人经济成分:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='fxrjjcf' name='fxrjjcf' disabled="disabled"
                                style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=5',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        发行人国民经济部门 :
                    </td>
                    <td>
                        <select class='easyui-combobox' id='fxrgmjjbm'  name='fxrgmjjbm' disabled='disabled' style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=6',
				                method: 'get',
				                valueField:'code',
                                textField:'text'"
                        >
                        </select>                </td>
                    <td align='right'>
                        数据日期 :
                    </td>
                    <td>
                        <input class='easyui-datebox' id='sjrq' name='sjrq' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script type="text/javascript">
    var tablelazyload = null;
    var baseCurrencys = ${baseCurrencys};
    var baseCountrys = ${baseCountrys};
    var baseAreas = ${baseAreas};
    var baseAindustrys = ${baseAindustrys};
    $(function () {
        $('#dialog1').dialog({
            onClose: function () {
                $('.validatebox-tip').remove();
            }
        });
        $("#dg1").datagrid({
            loadMsg: '数据加载中,请稍后...',
            method: 'get',
            url: 'getXclzqtzxxData?datastatus=' + '${datastatus}',
            checkOnSelect: true,
            autoRowHeight: false,
            pagination: true,
            rownumbers: true,
            toolbar: '#tb1',
            fitColumns: false,
            pageSize: 20,
            pageList: [15, 20, 30, 50],
            columns: [[
                {field: 'ck', checkbox: true},
                {field: 'id', title: '编号', width: 80, align: 'center', hidden: true},
                {field: 'operationname', title: '操作名', width: 150, align: 'center'},
                {field: 'nopassreason', title: '审核不通过原因', width: 150, align: 'center'},
                {
                    field: 'checkstatus', title: '校验结果', width: 150, align: 'center', formatter: function (value) {
                        if (value == '0') {
                            return "未校验";
                        } else if (value == '1') {
                            return "校验成功";
                        } else if (value == '2') {
                            return '<span style="color:red;">' + '校验失败' + '</span>';
                        }
                        return value;
                    }
                },
                {field: 'financeorgcode', title: '金融机构代码', width: 150, align: 'center'},
                {field: 'financeorginnum', title: '内部机构号', width: 150, align: 'center'},
                {field: 'zqdm', title: '债券代码 ', width: 150, align: 'center'},
                {field: 'zqztgjg', title: '债券总托管机构', width: 150, align: 'center', formatter: function (value) {
                        return  ${zqztgjgData}[value];
                    }},
                {field: 'zqpz', title: '债券品种', width: 150, align: 'center', formatter: function (value) {
                        return ${zqpzData}[value];
                    }},
                {field: 'zqxyjg', title: '债券信用级别', width: 150, align: 'center', formatter: function (value) {
                        return  ${zqxydjData}[value];
                    }},
                {field: 'bz', title: '币种', width: 150, align: 'center', formatter: function (value) {
                        for (var i = 0; i < baseCurrencys.length; i++) {
                            if (baseCurrencys[i].currencycode == value) {
                                return baseCurrencys[i].currencyname;
                            }
                        }
                    }
                },
                {field: 'zqye', title: '债券余额', width: 150, align: 'center'},
                {field: 'zqyezrmb', title: '债券余额折人民币', width: 150, align: 'center'},
                {field: 'zqzwdj', title: '债权债务登记日', width: 150, align: 'center'},
                {field: 'qxr', title: '起息日', width: 150, align: 'center'},
                {field: 'dfrq', title: '兑付日期', width: 150, align: 'center'},
                {field: 'pmll', title: '票面利率', width: 150, align: 'center'},
                {field: 'fxrzjdm', title: '发行人证件代码', width: 150, align: 'center'},
                {field: 'fxrdqdm', title: '发行人地区代码', width: 150, align: 'center', formatter: function (value) {
                        for (var i = 0; i < baseCountrys.length; i++) {
                            if (baseCountrys[i].countrycode == value) {
                                return baseCountrys[i].countryname;
                            }
                        }
                        for (var i = 0; i < baseAreas.length; i++) {
                            if (baseAreas[i].areacode == value) {
                                return baseAreas[i].areaname;
                            }
                        }
                    }
                },
                {field: 'fxrhy', title: '发行人行业', width: 150, align: 'center', formatter: function (value) {
                        return  ${fxrhyData}[value];
                    }},
                {field: 'fxrqygm', title: '发行人企业规模', width: 150, align: 'center', formatter: function (value) {
                        return  ${zqqygmData}[value];
                    }},
                {field: 'fxrjjcf', title: '发行人经济成分', width: 150, align: 'center', formatter: function (value) {
                        return  ${zqfxrjjcfData}[value];
                    }},
                {field: 'fxrgmjjbm', title: '发行人国民经济部门', width: 150, align: 'center', formatter: function (value) {
                        return  ${zqfxrjjbmData}[value];
                    }},
                {field: 'sjrq', title: '数据日期', width: 150, align: 'center'},
                {field: 'operator', title: '操作人', align: 'center'},
                {field: 'operationtime', title: '操作时间', align: 'center'}
            ]],
            onDblClickRow: function (rowIndex, rowData) {
                $('#info_formForAdd').form('clear');
                $('#info_dialog1').dialog('open').dialog('center').dialog('setTitle', '存量债券投资信息详情');
                $('#info_formForAdd').form('load', rowData);
            }
        });
        $("#formForAdd").append(tablelazyload);
        $.parser.parse($("#enterTable").parent());
        $(".combo").click(function (e) {
            if (e.target.className == 'combo-text validatebox-text' || e.target.className == 'combo-text validatebox-text validatebox-invalid') {
                if (!$(this).prev().prop("disabled")) {
                    if ($(this).prev().combobox("panel").is(":visible")) {
                        $(this).prev().combobox("hidePanel");
                    } else {
                        $(this).prev().combobox("showPanel");
                    }
                }
            }
        });
    })

    // 查询
    function searchOnSelected(value) {
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        // var contractcodeParam = $("#contractcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        // var receiptcodeParam = $("#receiptcodeParam").val().trim();
        // var brroweridnumParam = $("#brroweridnumParam").val().trim();
        // var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");

        var operationnameParam = '';
        if (value == 'dsh') {
            operationnameParam = $("#operationnameParam").combobox("getValue");
        }
        //$("#dg1").datagrid("load", {});
        $("#dg1").datagrid("load", {
            "financeorgcodeParam": financeorgcodeParam,
            "checkstatusParam": checkstatusParam,
            "operationnameParam": operationnameParam
        });

    }

</script>
</body>
</html>
