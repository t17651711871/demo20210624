<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%@ page import="com.geping.etl.common.util.VariableUtils" %>
<%@ page import="com.geping.etl.common.entity.Sys_Auth_Role_Resource" %>
<%@ page import="com.geping.etl.common.entity.Sys_UserAndOrgDepartment" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    VariableUtils vu = (VariableUtils)request.getSession().getAttribute("vu");
    Set<Sys_Auth_Role_Resource> operateSet = vu.getOperateReportSet();
    String status = request.getParameter("status");
    String shifoushenheziji = (String) session.getAttribute("shifoushenheziji");
    String role = vu.getRole();
    Sys_UserAndOrgDepartment sys_User = ((Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User"));
    String handlename = sys_User.getLoginid();
    String isusedelete = (String) request.getSession().getAttribute("isusedelete");
    String isuseupdate = (String) request.getSession().getAttribute("isuseupdate");
    List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
    List<String> baseAreas = new ArrayList<>();
    for (BaseArea baseArea : baseAreaList) {
        baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
    }
    List<BaseCountry> baseCountryList = (List<BaseCountry>) request.getAttribute("baseCountryList");

    List<String>  baseCountrys = new ArrayList<>();
    for (BaseCountry baseCountry : baseCountryList) {
        baseCountrys.add("'"+baseCountry.getCountrycode()+ "-" +baseCountry.getCountryname()+"'");
    }
    List<String>  baseAreaAndCountrys = new ArrayList<>();
    baseAreaAndCountrys.addAll(baseAreas);
    baseAreaAndCountrys.addAll(baseCountrys);

    List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
    List<String> baseCurrencys = new ArrayList<>();
    for (BaseCurrency baseCurrency : baseCurrencyList) {
        baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>存量个人贷款基础数据信息</title>
    <%@include file="../../../common/head.jsp"%>
</head>
<body>

<table id="dg1" style="height: 480px;" title="存量个人贷款基础数据信息列表"></table>

<!--表格工具栏-->
<div id="tb1" style="padding: 5px; height: auto;">
    <%if("dtj".equals(status) || "dsh".equals(status)){%>
    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return" onclick="fanhui()">返回</a>&nbsp;&nbsp;
    <%}else if ("dsb".equals(status)){%>
    <a class="easyui-linkbutton" href="VGenerateMessageUi" iconCls="icon-return" onclick="fanhui()">返回</a>&nbsp;&nbsp;
    <%}%>&nbsp;&nbsp;
    <label>金融机构代码：</label><input id="financeorgcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>贷款合同编码：</label><input id="contractcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>贷款借据编码：</label><input id="receiptcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>借款人证件代码：</label><input id="brroweridnumParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>借款人证件类型：</label>
    <select class='easyui-combobox'  id="isfarmerloanParam" name="isfarmerloanParam" editable=false style="height: 23px; width:130px">
        <option value=''>=请选择=</option>
        <option value='A01'>A01-统一社会信用代码</option>
        <option value='A02'>A02-组织机构代码</option>
        <option value='A03'>A03-其他</option>
        <option value='B01'>B01-身份证</option>
        <option value='B02'>B02-户口簿</option>
        <option value='B03'>B03-护照</option>
        <option value='B04'>B04-军官证</option>
        <option value='B05'>B05-士兵证</option>
        <option value='B06'>B06-港澳居民来往内地通行证</option>
        <option value='B07'>B07-台湾同胞来往内地通行证</option>
        <option value='B08'>B08-临时身份证</option>
        <option value='B09'>B09-外国人居留证</option>
        <option value='B10'>B10-警官证</option>
        <option value='B11'>B11-外国人永久居留身份证</option>
        <option value='B12'>B12-港澳台居民居住证</option>
        <option value='B99'>B99-其他证件</option>
    </select>
    &nbsp;
    <label>校验类型：</label>
    <select class='easyui-combobox'  id='checkstatusParam' name='checkstatusParam' editable=false style='height: 23px; width:80px' >
        <option value=''>=请选择=</option>
        <option value='0'>未校验</option>
        <option value='1'>校验成功</option>
        <option value='2'>校验失败</option>
    </select>&nbsp;&nbsp;
    <%if("dtj".equals(status)){%>
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected('dtj')">查询</a>&nbsp;&nbsp;<br>
    <%}else if ("dsh".equals(status)){%>
    <label>操作名：</label>
    <select class='easyui-combobox' id='operationnameParam' name='operationnameParam' editable=false style='height: 23px; width:80px' >
        <option value=''>=请选择=</option>
        <option value='申请删除'>申请删除</option>
    </select>&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected('dsh')">查询</a>&nbsp;&nbsp;<br>
    <%}%>
    <%if("dtj".equals(status)){
        for(Sys_Auth_Role_Resource sarr : operateSet){
            if(sarr.getResValue().equals("ADD")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-add" onclick="add()">新增</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("EDIT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-edit" onclick="edit()">修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("APPLYDELETE")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-remove" onclick="applyDelete()">申请删除</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("IMPORT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="importExcel()">导入</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("EXPORT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(false)">导出</a>&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(true)">导出模板</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("CHECKOUT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-accept" onclick="check()">校验</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("SUBMIT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-easyicon-net-16" onclick="submitit()">提交</a>
    <%
            }
        }
    }else if("dsh".equals(status)){
        for(Sys_Auth_Role_Resource sarr : operateSet){
            if(sarr.getResValue().equals("EDIT") && "yes".equals(isuseupdate)){
    %>
    <%--<a class="easyui-linkbutton" iconCls="icon-edit" onclick="applyEdit()" >申请修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
    <%
    }else if(sarr.getResValue().equals("DELETE") && "yes".equals(isusedelete)){
    %>
    <%--<a class="easyui-linkbutton" data-options="iconCls:'icon-remove'" onclick="applyDelete()" >申请删除</a>&nbsp;--%>
    <%
    }else if(sarr.getResValue().equals("CHECK")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-ok" onclick="agreeApply()" >同意申请删除</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-cross" onclick="noAgreeApply()" >拒绝申请删除</a>&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-application-get" onclick="agreeAudit()" >审核通过</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-cross" onclick="noAgreeAudit()" >审核不通过</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(false)">导出</a>&nbsp;&nbsp;&nbsp;
    <%
                }
            }
        }%>
</div>

<!-- 导入文件dialog -->
<div style="visibility: hidden;">
    <div id="importExcel" class="easyui-dialog" style="width: 600px; height: 150px;top: 100px;padding: 20px;" title="数据导入" data-options="modal:true,closed:true">
        文件导入:<input type="file" accept=".xls,.xlsx" name="excelfile" id="excelfile" onchange="inexcel()"/><br>
        <input type="button" id="importbtn" value="提交" style="width:50px;margin-top:20px;" onclick="excel()"/>
    </div>
</div>

<!--新增修改窗口-->
<div id="dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div id="tbForAddDialog" style="padding-left: 100px;padding-top: 10px">
        <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="save()">确定</a>&nbsp;&nbsp;
        <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="cancel()">取消</a>
    </div>

    <!-- 信息录入 -->
    <div align="center" style="padding-top: 30px">
        <form id="formForAdd" method="post">
            <input id="id" class="backId" name="id" type="hidden">
        </form>
    </div>
</div>

<!--详情窗口-->
<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <!-- 信息录入 -->
    <div align="center" style="padding-top: 30px">
        <form id="info_formForAdd" method="post">
            <input id="info_id" class="backId" name="id" type="hidden">
            <table class='enterTable' id='info_enterTable'>
                <tr>
                    <td align='right'>
                        数据日期:
                    </td>
                    <td>
                        <input id='info_sjrq' name='sjrq' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        金融机构代码:
                    </td>
                    <td>
                        <input id='info_financeorgcode' name='financeorgcode' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        内部机构号:
                    </td>
                    <td>
                        <input id='info_financeorginnum' name='financeorginnum' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        金融机构地区代码:
                    </td>
                    <td>
                        <input id='info_financeorgareacode' name='financeorgareacode' disabled="disabled" style=' height: 23px; width:173px'>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        借款人证件代码:
                    </td>
                    <td>
                        <input id='info_brroweridnum' name='brroweridnum' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        借款人地区代码:
                    </td>
                    <td>
                        <input id='info_brrowerareacode' name='brrowerareacode' disabled="disabled" style='height: 23px; width:173px'>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款借据编码:
                    </td>
                    <td>
                        <input id='info_receiptcode' name='receiptcode' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        贷款合同编码:
                    </td>
                    <td>
                        <input id='info_contractcode' name='contractcode' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款产品类别:
                    </td>
                    <td>
                        <select id='info_productcetegory' name='productcetegory' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='F0211'>F0211-个人住房贷款</option>
                            <option value='F0212'>F0212-个人汽车消费贷款</option>
                            <option value='F02131'>F02131-国家助学贷款</option>
                            <option value='F02132'>F02132-一般商业性助学贷款</option>
                            <option value='F0219'>F021-其他消费贷款</option>
                            <option value='F022'>F022-经营贷款</option>
                            <option value='F023'>F023-固定资产贷款</option>
                            <option value='F03'>F03-拆借</option>
                            <option value='F041'>F041-账户透支</option>
                            <option value='F042'>F402-货记卡透支</option>
                            <option value='F043'>F403-准货记卡透支</option>
                            <option value='F061'>F061-债券回购/返售</option>
                            <option value='F062'>F062-票据回购/返售</option>
                            <option value='F063'>F063-贷款回购/返售</option>
                            <option value='F064'>F064-股票及其他股权回购/返售</option>
                            <option value='F065'>F065-黄金回购/返售</option>
                            <option value='F069'>F069-其他资产回购/返售</option>
                            <option value='F081'>F081-国际贸易融资</option>
                            <option value='F082'>F082-国内贸易融资</option>
                            <option value='F09'>F09-融资租赁</option>
                            <option value='F12'>F12-并购贷款</option>
                            <option value='F13'>F13-其他债券投资</option>
                        </select>
                    </td>
                    <td align='right'>
                        借款人证件类型:
                    </td>
                    <td>
                        <select id='info_isfarmerloan' name='isfarmerloan' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='A01'>A01-统一社会信用代码</option>
                            <option value='A02'>A02-组织机构代码</option>
                            <option value='A03'>A03-其他</option>
                            <option value='B01'>B01-身份证</option>
                            <option value='B02'>B02-户口簿</option>
                            <option value='B03'>B03-护照</option>
                            <option value='B04'>B04-军官证</option>
                            <option value='B05'>B05-士兵证</option>
                            <option value='B06'>B06-港澳居民来往内地通行证</option>
                            <option value='B07'>B07-台湾同胞来往内地通行证</option>
                            <option value='B08'>B08-临时身份证</option>
                            <option value='B09'>B09-外国人居留证</option>
                            <option value='B10'>B10-警官证</option>
                            <option value='B11'>B11-外国人永久居留身份证</option>
                            <option value='B12'>B012-港澳台居民居住证</option>
                            <option value='B99'>B99-其他证件</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款发放日期:
                    </td>
                    <td>
                        <input id='info_loanstartdate' name='loanstartdate' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        贷款到期日期:
                    </td>
                    <td>
                        <input id='info_loanenddate' name='loanenddate'   disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款展期到期日期:
                    </td>
                    <td>
                        <input id='info_extensiondate' name='extensiondate' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        币种:
                    </td>
                    <td>
                        <select id='info_currency' name='currency' disabled="disabled" style='height: 23px; width:173px'>
                            <c:forEach items='${baseCurrencyList}' var='aa'>
                                <option value='${aa.currencycode}'>${aa.currencycode}-${aa.currencyname}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款余额:
                    </td>
                    <td>
                        <input id='info_receiptbalance' name='receiptbalance' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        贷款余额折人民币:
                    </td>
                    <td>
                        <input id='info_receiptcnybalance' name='receiptcnybalance' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        利率是否固定:
                    </td>
                    <td>
                        <select id='info_interestisfixed' name='interestisfixed' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='RF01'>RF01-固定利率</option>
                            <option value='RF02'>RF02-浮动利率</option>
                        </select>
                    </td>
                    <td align='right'>
                        利率水平:
                    </td>
                    <td>
                        <input id='info_interestislevel' name='interestislevel' disabled="disabled"  data-options="required:true,precision:2" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款定价基准类型:
                    </td>
                    <td>
                        <select id='info_fixpricetype' name='fixpricetype' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='TR01'>TR01-上海银行间同业拆放利率</option>
                            <option value='TR02'>TR02-伦敦银行间同业拆放利率</option>
                            <option value='TR03'>TR03-香港银行间同业拆放利率</option>
                            <option value='TR04'>TR04-欧洲银行间同业拆放利率</option>
                            <option value='TR05'>TR05-贷款市场报价利率</option>
                            <option value='TR06'>TR06-中国国债收益率</option>
                            <option value='TR07'>TR07-人名币存款基准利率</option>
                            <option value='TR08'>TR08-人名币贷款基准利率</option>
                            <option value='TR99'>TR99-其他</option>
                        </select>
                    </td>
                    <td align='right'>
                        基准利率:
                    </td>
                    <td>
                        <input id='info_baseinterest' name='baseinterest' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款财政扶持方式:
                    </td>
                    <td>
                        <select id='info_loanfinancesupport' name='loanfinancesupport' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='A0101'>A0101-中央政府全额贴息</option>
                            <option value='A0102'>A0102-中央政府部分贴息</option>
                            <option value='A0201'>A0201-地方政府全额贴息</option>
                            <option value='A0202'>A0202-地方政府部分贴息</option>
                            <option value='B'>B-税前提取准备</option>
                            <option value='C'>C-联合贴息</option>
                            <option value='Z'>Z-其他</option>
                        </select>
                    </td>
                    <td align='right'>
                        贷款利率重新定价日:
                    </td>
                    <td>
                        <input id='info_loaninterestrepricedate' name='loaninterestrepricedate' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款担保方式:
                    </td>
                    <td>
                        <select id='info_guaranteemethod' name='guaranteemethod' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='A'>A-质押贷款</option>
                            <option value='B01'>B01-房地产抵押贷款</option>
                            <option value='B99'>B99-其他抵押贷款</option>
                            <option value='C01'>C01-联保贷款</option>
                            <option value='C99'>C99-其他保证贷款</option>
                            <option value='D'>D-信用/免担保贷款</option>
                            <option value='E'>E-组合担保</option>
                            <option value='Z'>Z-其他</option>
                        </select>
                    </td>
                    <td align='right'>
                        贷款质量:
                    </td>
                    <td>
                        <select id='info_loanquality' name='loanquality' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='FQ01'>FQ01-正常类贷款</option>
                            <option value='FQ02'>FQ02-关注类贷款</option>
                            <option value='FQ03'>FQ03-次级类贷款</option>
                            <option value='FQ04'>FQ04-可疑类贷款</option>
                            <option value='FQ05'>FQ05-损失类贷款</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款状态:
                    </td>
                    <td>
                        <select id='info_loanstatus' name='loanstatus' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='LS01'>LS01-正常</option>
                            <option value='LS02'>LS02-展期</option>
                            <option value='LS03'>LS03-逾期</option>
                            <option value='LS04'>LS04-缩期</option>
                        </select>
                    </td>
                    <td align='right'>
                        逾期类型:
                    </td>
                    <td>
                        <select id='info_overduetype' name='overduetype' disabled="disabled" style='height: 23px; width:173px' >
                            <option value='01'>01-本金逾期</option>
                            <option value='02'>02-利息逾期</option>
                            <option value='03'>03-本金利息逾期</option>
                        </select>
                    </td>
                </tr>
                <tr>

                </tr>
                <tr>
                    <td align='right'>
                        是否首次贷款:
                    </td>
                    <td>
                        <select id='info_isplatformloan' name='isplatformloan' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='1'>1-是</option>
                            <option value='0'>0-否</option>
                        </select>
                    </td>
                    <td align='right'>
                        贷款用途:
                    </td>
                    <td>
                        <input id='info_issupportliveloan' name='issupportliveloan' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<script type="text/javascript">
    var tablelazyload = "<table class='enterTable' id='enterTable'>"
        +" <tr>"
        +"        <td align='right'>"
            +"数据日期"
        +"</td>"
        +"    <td>"
        +"    <input id='sjrq' name='sjrq' editable=true style='width:173px;' class='easyui-datebox' data-options='validType:[\"normalDate\",\"dateFormat\"]'/>"
        +"        </td>"
        +"        <td align='right'>"
        +"        借款人证件类型:"
        +"</td>"
        +"    <td>"
        +"    <select class='easyui-combobox' id='isfarmerloan' name='isfarmerloan' editable=false style='height: 23px; width:173px'>"
        +"        <option value='A01'>A01-统一社会信用代码</option>"
        +"        <option value='A02'>A02-组织机构代码</option>"
        +"        <option value='A03'>A03-其他</option>"
        +"        <option value='B01'>B01-身份证</option>"
        +"        <option value='B02'>B02-户口簿</option>"
        +"        <option value='B03'>B03-护照</option>"
        +"        <option value='B04'>B04-军官证</option>"
        +"        <option value='B05'>B05-士兵证</option>"
        +"        <option value='B06'>B06-港澳居民来往内地通行证</option>"
        +"        <option value='B07'>B07-台湾同胞来往内地通行证</option>"
        +"        <option value='B08'>B08-临时身份证</option>"
        +"        <option value='B09'>B09-外国人居留证</option>"
        +"        <option value='B10'>B010-警官证</option>"
        +"        <option value='B11'>B011-外国人永久居留身份证</option>"
        +"        <option value='B12'>B012-港澳台居民居住证</option>"
        +"        <option value='B99'>B99-其他证件</option>"
        +"        </select>"
        +"        </td>"
        +"        </tr>"
        +"<tr> <td align='right'>"
        +"    金融机构代码:</td>"
        +"    <td>"
        +"    <input id='financeorgcode' name='financeorgcode' style='width:173px;' class='easyui-validatebox' maxlength='18' data-options='validType:[\"teshu\"]'/>"
        +"        </td>"
        +"        <td align='right'>"
        +"        内部机构号:"
        +"</td>"
        +"    <td>"
        +"    <input id='financeorginnum' name='financeorginnum' style='width:173px;' class='easyui-validatebox' maxlength='60' data-options='validType:[\"teshu\"]'/>"
        +"        </td>"
        +"        </tr>"
        +"        <tr>"
        +"        <td align='right'>"
        +"        金融机构地区代码:"
        +"</td>"
        +"    <td>"
        +"    <select class='easyui-combotree' id='financeorgareacode' name='financeorgareacode' style='height: 23px; width:173px' data-options=\"url:'XgetAdmindivideJson2',valueField:'id',textField:'text',editable:true,value:''\">"
        +"        </select>"
        +"        </td>"
        +"        <td align='right'>"
        +"        借款人证件代码:"
        +"</td>"
        +"    <td>"
        +"    <input id='brroweridnum' name='brroweridnum' style='width:173px;' class='easyui-validatebox' data-options='validType:[\"teshu\"]'/>"
        +"        </td>"
        +"        </tr>"
        +"        <tr>"
        +"        <td align='right'>"
        +"        借款人地区代码:"
        +"</td>"
        +"    <td>"
        +"    <select class='easyui-combotree' id='brrowerareacode' name='brrowerareacode' style='height: 23px; width:173px' data-options=\"url:'XgetAdmindivideJson',valueField:'id',textField:'text',editable:true,value:''\">"
        +"        </select>"
        +"        </td>"
        +"    <td align='right'>"
        +"        贷款借据编码:"
        +"</td>"
        +"    <td>"
        +"    <input id='receiptcode' name='receiptcode' style='width:173px;' class='easyui-validatebox' maxlength='100' data-options='validType:[\"teshu\"]'/>"
        +"        </td>"
        +"        </tr>"
        +"    <tr>"
        +"        <td align='right'>"
        +"        贷款合同编码:"
        +"</td>"
        +"    <td>"
        +"    <input id='contractcode' name='contractcode' style='width:173px;' class='easyui-validatebox' maxlength='100' data-options='validType:[\"teshua\"]'/>"
        +"        </td>"
        +"        <td align='right'>"
        +"        贷款产品类别:"
        +"</td>"
        +"    <td>"
        +"    <select class='easyui-combobox' id='productcetegory' name='productcetegory' editable=false style='height: 23px; width:173px' >"
        +"<option value='F0211'>F0211-个人住房贷款</option>"
        +"<option value='F0212'>F0212-个人汽车消费贷款</option>"
        +"<option value='F02131'>F02131-国家助学贷款</option>"
        +"<option value='F02132'>F02132-一般商业性助学贷款</option>"
        +"<option value='F0219'>F021-其他消费贷款</option>"
        +"<option value='F022'>F022-经营贷款</option>"
        +"<option value='F023'>F023-固定资产贷款</option>"
        +"<option value='F03'>F03-拆借</option>"
        +"<option value='F041'>F041-账户透支</option>"
        +"<option value='F042'>F402-货记卡透支</option>"
        +"<option value='F043'>F403-准货记卡透支</option>"
        +"<option value='F061'>F061-债券回购/返售</option>"
        +"<option value='F062'>F062-票据回购/返售</option>"
        +"<option value='F063'>F063-贷款回购/返售</option>"
        +"<option value='F064'>F064-股票及其他股权回购/返售</option>"
        +"<option value='F065'>F065-黄金回购/返售</option>"
        +"<option value='F069'>F069-其他资产回购/返售</option>"
        +"<option value='F081'>F081-国际贸易融资</option>"
        +"<option value='F082'>F082-国内贸易融资</option>"
        +"<option value='F09'>F09-融资租赁</option>"
        +"<option value='F12'>F12-并购贷款</option>"
        +"<option value='F13'>F13-其他债权投资</option>"
        +"        </select>"
        +"        </td>"
        +"        </tr>"
        +"        <tr>"
        +"        <td align='right'>"
        +"        贷款发放日期:"
        +"</td>"
        +"    <td>"
        +"    <input id='loanstartdate' name='loanstartdate' editable=true style='width:173px;' class='easyui-datebox' data-options='validType:[\"normalDate\",\"startcompareToExten\",\"compareToEnd\"]'/>"
        +"        </td>"
        +"        <td align='right'>"
        +"        贷款到期日期:"
        +"</td>"
        +"    <td>"
        +"    <input id='loanenddate' name='loanenddate' editable=true style='width:173px;' class='easyui-datebox' data-options='validType:[\"normalDate\",\"compareToExten\",\"compareToExtenLS\",\"dateFormat\"]'/>"
        +"        </td>"
        +"        </tr>"
        +"        <tr>"
        +"        <td align='right'>"
        +"        贷款展期到期日期:"
        +"</td>"
        +"    <td>"
        +"    <input id='extensiondate' name='extensiondate' editable=true style='width:173px;' class='easyui-datebox' data-options='validType:[\"normalDate\",\"dateFormat\"]'/>"
        +"        </td>"
        +"        <td align='right'>"
        +"        币种:"
        +"</td>"
        +"    <td>"
        +"    <select class='easyui-combobox' id='currency' name='currency' style='height: 23px; width:173px' >"
        +"        <c:forEach items='${baseCurrencyList}' var='aa'>"
        +"        <option value='${aa.currencycode}'>${aa.currencycode}-${aa.currencyname}</option>"
        +"        </c:forEach>"
        +"        </select>"
        +"        </td>"
        +"        </tr>"
        +"        <tr>"
        +"        <td align='right'>"
        +"        贷款余额:"
        +"</td>"
        +"    <td>"
        +"    <input id='receiptbalance' name='receiptbalance' style='width:173px;' class='easyui-validatebox easyui-numberbox' data-options='precision:2,validType:[\"amount\"]'/>"
        +"        </td>"
        +"        <td align='right'>"
        +"        贷款余额折人民币:"
        +"</td>"
        +"    <td>"
        +"    <input id='receiptcnybalance' name='receiptcnybalance' style='width:173px;' class='easyui-validatebox easyui-numberbox' data-options='precision:2,validType:[\"amount\"]'/>"
        +"        </td>"
        +"        </tr>"
        +"        <tr>"
        +"        <td align='right'>"
        +"        利率是否固定:"
        +"</td>"
        +"    <td>"
        +"    <select class='easyui-combobox' id='interestisfixed' name='interestisfixed' editable=false style='height: 23px; width:173px' >"
        +"        <option value='RF01'>RF01-固定利率</option>"
        +"        <option value='RF02'>RF02-浮动利率</option>"
        +"        </select>"
        +"        </td>"
        +"        <td align='right'>"
        +"        利率水平:"
        +"</td>"
        +"    <td>"
        +"    <input id='interestislevel' name='interestislevel' style='width:173px;' class='easyui-validatebox easyui-numberbox' data-options='precision:5,validType:[\"ratelevel\"]' />"
        +"        </td>"
        +"        </tr>"
        +"        <tr>"
        +"        <td align='right'>"
        +"        贷款定价基准类型:"
        +"</td>"
        +"    <td>"
        +"    <select class='easyui-combobox' id='fixpricetype' name='fixpricetype' editable=false style='height: 23px; width:173px' >"
        +"        <option value='TR01'>TR01-上海银行间同业拆放利率</option>"
        +"        <option value='TR02'>TR02-伦敦银行间同业拆放利率</option>"
        +"        <option value='TR03'>TR03-香港银行间同业拆放利率</option>"
        +"        <option value='TR04'>TR04-欧洲银行间同业拆放利率</option>"
        +"        <option value='TR05'>TR05-贷款市场报价利率</option>"
        +"        <option value='TR06'>TR06-中国国债收益率</option>"
        +"        <option value='TR07'>TR07-人名币存款基准利率</option>"
        +"        <option value='TR08'>TR08-人名币贷款基准利率</option>"
        +"        <option value='TR99'>TR99-其他</option>"
        +"        </select>"
        +"        </td>"
        +"        <td align='right'>"
        +"        基准利率:"
        +"</td>"
        +"    <td>"
        +"    <input id='baseinterest' name='baseinterest' style='width:173px;' class='easyui-validatebox easyui-numberbox' data-options='precision:5,validType:[\"ratelevel\"]'/>"
        +"        </td>"
        +"        </tr>"
        +"        <tr>"
        +"        <td align='right'>"
        +"        贷款财政扶持方式:"
        +"</td>"
        +"    <td>"
        +"    <select class='easyui-combobox' id='loanfinancesupport' name='loanfinancesupport' editable=false style='height: 23px; width:173px'>"
        +"        <option value='A0101'>A0101-中央政府全额贴息</option>"
        +"        <option value='A0102'>A0102-中央政府部分贴息</option>"
        +"        <option value='A0201'>A0201-地方政府全额贴息</option>"
        +"        <option value='A0202'>A0202-地方政府部分贴息</option>"
        +"        <option value='B'>B-税前提取准备</option>"
        +"        <option value='C'>C-联合贴息</option>"
        +"        <option value='Z'>Z-其他</option>"
        +"        </select>"
        +"        </td>"
        +"        <td align='right'>"
        +"        贷款利率重新定价日:"
        +"</td>"
        +"    <td>"
        +"    <input id='loaninterestrepricedate' name='loaninterestrepricedate' style='width:173px;' class='easyui-datebox' data-options='validType:[\"normalDate\",\"aloaninterestrepricedate\",\"eloaninterestrepricedate\",\"bloaninterestrepricedate\",\"cloaninterestrepricedate\",\"dloaninterestrepricedate\"]'/>"
        +"        </td>"
        +"        </tr>"
        +"        <tr>"
        +"        <td align='right'>"
        +"        贷款担保方式:"
        +"</td>"
        +"    <td>"
        +"    <select class='easyui-combobox' id='guaranteemethod' name='guaranteemethod' editable=false style='height: 23px; width:173px' >"
        +"        <option value='A'>A-质押贷款</option>"
        +"        <option value='B01'>B01-房地产抵押贷款</option>"
        +"        <option value='B99'>B99-其他抵押贷款</option>"
        +"        <option value='C01'>C01-联保贷款</option>"
        +"        <option value='C99'>C99-其他保证贷款</option>"
        +"        <option value='D'>D-信用/免担保贷款</option>"
        +"        <option value='E'>E-组合担保</option>"
        +"        <option value='Z'>Z-其他</option>"
        +"        </select>"
        +"        </td>"
        +"        <td align='right'>"
        +"        贷款质量:"
        +"</td>"
        +"    <td>"
        +"    <select class='easyui-combobox' id='loanquality' name='loanquality' editable=false style='height: 23px; width:173px'>"
        +"        <option value='FQ01'>FQ01-正常类贷款</option>"
        +"        <option value='FQ02'>FQ02-关注类贷款</option>"
        +"        <option value='FQ03'>FQ03-次级类贷款</option>"
        +"        <option value='FQ04'>FQ04-可疑类贷款</option>"
        +"        <option value='FQ05'>FQ05-损失类贷款</option>"
        +"        </select>"
        +"        </td>"
        +"        </tr>"
        +"        <tr>"
        +"        <td align='right'>"
        +"        贷款状态:"
        +"</td>"
        +"    <td>"
        +"    <select class='easyui-combobox' id='loanstatus' name='loanstatus' editable=false style='height: 23px; width:173px' >"
        +"        <option value='LS01'>LS01-正常</option>"
        +"        <option value='LS02'>LS02-展期</option>"
        +"        <option value='LS03'>LS03-逾期</option>"
        +"        <option value='LS04'>LS04-缩期</option>"
        +"        </select>"
        +"        </td>"
        +"        <td align='right'>"
        +"        逾期类型:"
        +"</td>"
        +"    <td>"
        +"    <select class='easyui-combobox' id='overduetype' name='overduetype' style='height: 23px; width:173px' >"
        +"        <option value='01'>01-本金逾期</option>"
        +"        <option value='02'>02-利息逾期</option>"
        +"        <option value='03'>03-本金利息逾期</option>"
        +"        </select>"
        +"        </td>"
        +"        </tr>"
        +"        <tr>"
        +"        <td align='right'>"
        +"        贷款用途:"
        +"</td>"
        +"    <td>"
        +"    <input id='issupportliveloan' name='issupportliveloan' style='width:173px;' class='easyui-validatebox' maxlength='1000'/>"
        +"        </td>"
        +"<td align='right'>"
        +" 是否首次贷款:"
        +"</td>"
        +" <td>"
        +"    <select class='easyui-combobox' id='isplatformloan' name='isplatformloan' editable=false style='height: 23px; width:173px'>"
        +"        <option value='1'>1-是</option>"
        +"        <option value='0'>0-否</option>"
        +"        </select>"
        +"        </td>"
        +"        </tr>"
        +"</table>"
    var index = 5;
    var baseAreas = <%=baseAreas%>;
    var baseAreaAndCountrys = <%=baseAreaAndCountrys%>;
    var baseCurrencys = <%=baseCurrencys%>;
    var handlename = '<%= handlename%>';
    var role = '<%= role%>';
    var shifoushenheziji = '<%= shifoushenheziji%>';
    $(function(){
        $('#dialog1').dialog({
            onClose: function () {
                $('.validatebox-tip').remove();
            }
        });
        $("#dg1").datagrid({
            loadMsg:'数据加载中,请稍后...',
            method:'post',
            url:'XGetXclgrdkxxData?datastatus='+'${datastatus}',
            checkOnSelect:true,
            autoRowHeight:false,
            pagination:true,
            rownumbers:true,
            toolbar:'#tb1',
            fitColumns:false,
            pageSize:20,
            pageList:[15,20,30,50],
            columns:[[
                {field:'ck',checkbox:true},
                {field:'id', title: '编号', width: 80, align: 'center', hidden: true},
                {field:'operationname',title:'操作名',width:150,align:'center'},
                {field:'nopassreason',title:'审核不通过原因',width:150,align:'center'},
                {field:'checkstatus',title:'校验结果',width:150,align:'center',formatter:function (value) {
                        if (value=='0'){
                            return "未校验";
                        }else if(value=='1'){
                            return "校验成功";
                        }else if (value=='2'){
                            return '<span style="color:red;">' + '校验失败' + '</span>';
                        }
                        return value;
                    }},
                {field:'financeorgcode',title:'金融机构代码',width:150,align:'center'},
                {field:'financeorginnum',title:'内部机构号',width:150,align:'center'},
                {field:'financeorgareacode',title:'金融机构地区代码',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseAreas.length;i++){
                            if (baseAreas[i].substring(0,6)==value){
                                return baseAreas[i];
                            }
                        }
                    }},
                {field:'isfarmerloan',title:'借款人证件类型',width:150,align:'center',formatter:function (value) {
                        if (value=='A01'){
                            return "A01-统一社会信用代码";
                        }else if(value=='A02'){
                            return "A02-组织机构代码";
                        }else if (value=='A03'){
                            return "A03-其他";
                        }else if (value =='B01'){
                            return "B01-身份证";
                        }else if (value =='B02'){
                            return "B02-户口簿";
                        }else if (value =='B03'){
                            return "B03-护照";
                        }else if (value =='B04'){
                            return "B04-军官证";
                        }else if (value =='B05'){
                            return "B05-士兵证";
                        }else if (value =='B06'){
                            return "B06-港澳居民来往内地通行证";
                        }else if (value =='B07'){
                            return "B07-台湾同胞来往内地通行证";
                        }else if (value =='B08'){
                            return "B08-临时身份证";
                        }else if (value =='B09'){
                            return "B09-外国人居留证";
                        }else if (value =='B10'){
                            return "B10-警官证";
                        }else if (value =='B11'){
                            return "B11-外国人永久居留身份证";
                        } else if (value =='B12'){
                            return "B12-港澳台居民居住证";
                        } else if (value =='B99'){
                            return "B99-其他证件";
                        }
                        return value;
                    }},
                {field:'brroweridnum',title:'借款人证件代码',width:150,align:'center'},
                {field:'brrowerareacode',title:'借款人地区代码',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseAreaAndCountrys.length;i++){
                            if (baseAreaAndCountrys[i].substring(0,6)==value){
                                return baseAreaAndCountrys[i];
                            }
                        }
                    }},
                {field:'receiptcode',title:'贷款借据编码',width:150,align:'center'},
                {field:'contractcode',title:'贷款合同编码',width:150,align:'center'},
                {field:'productcetegory',title:'贷款产品类别',width:150,align:'center',formatter:function (value) {
                        if(value=='F0211'){
                            return "F0211-个人住房贷款";
                        }else if(value=="F0212"){
                            return "F0212-个人汽车消费贷款";
                        }else if(value=="F02131"){
                            return "F02131-国家助学贷款";
                        }else if(value=="F02132"){
                            return "F02132-一般商业性助学贷款";
                        }else if(value=="F0219"){
                            return "F0219-其他消费贷款";
                        } else if(value=='F022'){
                            return "F022-经营贷款";
                        }else if (value=='F023'){
                            return "F023-固定资产贷款";
                        }else if (value=='F03'){
                            return "F03-拆借";
                        }else if(value=='F041'){
                            return "F041-账户透支";
                        }else if(value=="F042"){
                            return "F402-货记卡透支";
                        }else if(value=="F043"){
                           return "F403-准货记卡透支";
                       }else if(value=='F061'){
                            return "F061-债券回购/返售";
                        }else if(value=='F062'){
                            return "F062-票据回购/返售";
                        }else if(value=='F063'){
                            return "F063-贷款回购/返售";
                        }else if(value=='F064'){
                            return "F064-股票及其他股权回购/返售";
                        }else if(value=='F065'){
                            return "F065-黄金回购/返售";
                        }else if(value=='F069'){
                            return "F069-其他资产回购/返售";
                        }else if(value=='F081'){
                            return "F081-国际贸易融资";
                        }else if(value=='F082'){
                            return "F082-国内贸易融资";
                        }else if (value=='F09'){
                            return "F09-融资租赁";
                        }else if (value=='F12'){
                            return "F12-并购贷款";
                        }else if(value=='F13'){
                            return "F13-其他债券投资";
                        }
                        return value;
                    }},
                {field:'loanstartdate',title:'贷款发放日期',width:150,align:'center'},
                {field:'loanenddate',title:'贷款到期日期',width:150,align:'center'},
                {field:'extensiondate',title:'贷款展期到期日期',width:150,align:'center'},
                {field:'currency',title:'币种',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseCurrencys.length;i++){
                            if (baseCurrencys[i].substring(0,3)==value){
                                return baseCurrencys[i];
                            }
                        }
                    }},
                {field:'receiptbalance',title:'贷款余额',width:150,align:'center'},
                {field:'receiptcnybalance',title:'贷款余额折人民币',width:150,align:'center'},
                {field:'interestisfixed',title:'利率是否固定',width:150,align:'center',formatter:function (value) {
                        if (value =='RF01'){
                            return "RF01-固定利率";
                        }else if (value =='RF02-浮动利率'){
                            return "RF02-浮动利率";
                        }
                    }},
                {field:'interestislevel',title:'利率水平',width:150,align:'center'},
                {field:'fixpricetype',title:'贷款定价基准类型',width:150,align:'center',formatter:function (value) {
                        if (value=='TR01'){
                            return "TR01-上海银行间同业拆放利率";
                        }else if(value=='TR02'){
                            return "TR02-伦敦银行间同业拆放利率";
                        }else if (value=='TR03'){
                            return "TR03-香港银行间同业拆放利率";
                        }else if(value=='TR04'){
                            return "TR04-欧洲银行间同业拆放利率";
                        }else if (value=='TR05'){
                            return "TR05-贷款市场报价利率";
                        }else if (value=='TR06'){
                            return "TR06-中国国债收益率";
                        }else if(value=='TR07'){
                            return "TR07-人名币存款基准利率";
                        }else if (value=='TR08'){
                            return "TR08-人名币贷款基准利率";
                        }else if (value=='TR99'){
                            return "TR99-其他";
                        }
                        return value;
                    }},
                {field:'baseinterest',title:'基准利率',width:150,align:'center'},
                {field:'loanfinancesupport',title:'贷款财政扶持方式',width:150,align:'center',formatter:function (value) {
                         if (value=='A0101'){
                            return "A0101-中央政府全额贴息";
                        }else if(value=='A0102'){
                            return "A0102-中央政府部分贴息";
                        }else if (value=='A02'){
                            return "A02-地方政府贴息";
                        }else if (value=='A0201'){
                            return "A0201-地方政府全额贴息";
                        }else if(value=='A0202'){
                            return "A0202-地方政府部分贴息";
                        }else if (value=='B'){
                            return "B-税前提取准备";
                        }else if(value=='C'){
                            return "C-联合贴息";
                        }
                        else if (value=='Z'){
                            return "Z-其他";
                        }
                        return value;
                    }},
                {field:'loaninterestrepricedate',title:'贷款利率重新定价日',width:150,align:'center'},
                {field:'guaranteemethod',title:'贷款担保方式',width:150,align:'center',formatter:function (value) {
                        if (value=='A'){
                            return "A-质押贷款";
                        }else if (value=='B01'){
                            return "B01-房地产抵押贷款";
                        }else if(value=='B99'){
                            return "B99-其他抵押贷款";
                        }else if (value=='C01'){
                            return "C01-联保贷款";
                        }else if(value=='C99'){
                            return "C99-其他保证贷款";
                        }else if (value=='D'){
                            return "D-信用/免担保贷款";
                        }else if (value=='E'){
                            return "E-组合担保";
                        }else if (value=='Z'){
                            return "Z-其他";
                        }
                        return value;
                    }},
                {field:'loanquality',title:'贷款质量',width:150,align:'center',formatter:function (value) {
                        if (value=='FQ01'){
                            return "FQ01-正常类贷款";
                        }else if(value=='FQ02'){
                            return "FQ02-关注类贷款";
                        }else if (value=='FQ03'){
                            return "FQ03-次级类贷款";
                        }else if(value=='FQ04'){
                            return "FQ04-可疑类贷款";
                        }else if (value=='FQ05'){
                            return "FQ05-损失类贷款";
                        }
                        return value;
                    }},
                {field:'loanstatus',title:'贷款状态',width:150,align:'center',formatter:function (value) {
                        if (value=='LS01'){
                            return "LS01-正常";
                        }else if(value=='LS02'){
                            return "LS02-展期";
                        }else if (value=='LS03'){
                            return "LS03-逾期";
                        }else if(value=='LS04'){
                            return "LS04-缩期";
                        }
                        return value;
                    }},
                {field:'overduetype',title:'逾期类型',width:150,align:'center',formatter:function (value) {
                        if (value=='01'){
                            return "01-本金逾期";
                        }else if(value=='02'){
                            return "02-利息逾期";
                        }else if (value=='03'){
                            return "03-本金利息逾期";
                        }
                        return value;
                    }},
                {field:'isplatformloan',title:'是否首次贷款',width:150,align:'center',formatter:function (value) {
                        if (value=='1'){
                            return "1-是";
                        }else if(value=='0'){
                            return "0-否";
                        }
                        return value;
                    }},
                {field:'issupportliveloan',title:'贷款用途',width:150,align:'center'},
                {field: 'sjrq',title: '数据日期',width:150,align: 'center'},
                {field: 'operator', title: '操作人',  align: 'center'},
                {field: 'operationtime', title: '操作时间',  align: 'center'}
            ]],
            onDblClickRow: function (rowIndex,rowData) {
                //$("#tbForAddDialog").hide();//隐藏div
                //disableOcx();
                $('#info_formForAdd').form('clear');
                $('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','存量个人贷款信息详情');
                $('#info_formForAdd').form('load',rowData);
            }
        });
        $("#formForAdd").append(tablelazyload);
        $.parser.parse($("#enterTable").parent());
        $(".combo").click(function (e) {
            if (e.target.className == 'combo-text validatebox-text' || e.target.className == 'combo-text validatebox-text validatebox-invalid'){
                if (!$(this).prev().prop("disabled")){
                    if ($(this).prev().combobox("panel").is(":visible")) {
                        $(this).prev().combobox("hidePanel");
                    } else {
                        $(this).prev().combobox("showPanel");
                    }
                }
            }
        });

        $("#currency").combobox({
            onChange: function (n) {
                if (n != 'CNY'){
                    $('#receiptcnybalance').validatebox({required: true});
                }else {
                    $('#receiptcnybalance').val('');
                    $('#receiptcnybalance').validatebox({required: false});
                }
            }
        });
         $("#loanstatus").combobox({
            onChange: function (newValue) {
                 if (newValue == 'LS01'){
                    $("#overduetype").combobox('setValue',"");

                 }

            }
         });

    })

    function disableOcx() {
        var form = document.forms[0];
        for ( var i = 0; i < form.length; i++) {
            var element = form.elements[i];
            element.disabled = "true";
        }
    }

    function noDisable() {
        var form = document.forms[0];
        for ( var i = 0; i < form.length; i++) {
            var element = form.elements[i];
            element.disabled = false;
        }
    }

    // 查询
    function searchOnSelected(value){
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var receiptcodeParam = $("#receiptcodeParam").val().trim();
        var brroweridnumParam = $("#brroweridnumParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");

        var operationnameParam = '';
        if (value == 'dsh'){
            operationnameParam = $("#operationnameParam").combobox("getValue");
        }
        $("#dg1").datagrid("load", {"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"receiptcodeParam" : receiptcodeParam,"brroweridnumParam":brroweridnumParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam});
    }

    // 新增
    function add(){
        resetForAdd();

        $('#dialog1').dialog('open').dialog('center').dialog('setTitle','新增个人存量贷款信息');
        keydownsearch();
    }

    // 修改
    function edit(){
        var rows = $('#dg1').datagrid('getSelections');
        if(rows.length == 1){
            //$("#formForAdd").form('reset');
            resetForAdd();
            $("#formForAdd").form("load", rows[0]);
            $('#dialog1').dialog('open').dialog('center').dialog('setTitle','修改存量个人贷款信息');
            keydownsearch();
        }else{
            $.messager.alert("提示", "请选择一条数据进行修改!", "info");
        }
    }

    // 保存
    function save() {
        if($("#formForAdd").form('validate')){
            $.ajax({
                type: "POST",//为post请求
                url: "XssaveXclgrdkxx",//这是我在后台接受数据的文件名
                data: $('#formForAdd').serialize(),//将该表单序列化
                dataType: "json",
                async: false,
                success: function (res) {
                    if (res.status == "1") {
                        $.messager.alert('提示', '提交成功!', 'info');
                        $("#dialog1").dialog("close");
                        $("#dg1").datagrid("reload");
                    } else {
                        $.messager.alert('提示', '提交失败!', 'info');
                    }
                },
                error: function (err) {//请求失败之后的操作

                }
            });
        }
    }

    // 重置
    function resetForAdd(){
        //$("#tbForAddDialog").show();//显示div
        //noDisable();
        //客户号码onblur事件
        $('#customernum').bind('blur',function(){
            var customernum = $('#customernum').val();
            if (customernum.trim().length == 0){
                return;
            }
            $.ajax({
                sync: true,
                type: "POST",
                dataType: "json",
                url: "XgetInfoByCustomernum",
                data:{"customernum" : customernum},
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (data) {
                    if (data == null){
                        return;
                    };
                    if (data.customerfinorgcode != null){
                        $("#financeorgcode").val(data.customerfinorgcode);
                    }else {
                        $("#financeorgcode").val("");
                    }
                    if (data.customerfinorginside != null){
                        $("#financeorginnum").val(data.customerfinorginside);
                    }else {
                        $("#financeorginnum").val("");
                        //$("#financeorginnum").attr("readOnly",false);
                    }
                    if (data.customerfinorgarea != null){
                        $("#financeorgareacode").combotree('setValue',data.customerfinorgarea);
                        //$("#financeorgareacode").combobox('disable');
                    }else {
                        $("#financeorgareacode").combotree('setValue',"");
                    }
                    if (data.customercode != null){
                        $("#brroweridnum").val(data.customercode);
                    }else {
                        $("#brroweridnum").val("");
                    }
                    if (data.industry != null){
                        $("#brrowerindustry").combobox('setValue',data.industry);
                    }else {
                        $("#brrowerindustry").combobox('setValue',"");
                    }
                    if (data.regareacode != null){
                        $("#brrowerareacode").combotree('setValue',data.regareacode);
                    }else {
                        $("#brrowerareacode").combotree('setValue',"");
                    }
                    if (data.entpczjjcf != null){
                        $("#inverstoreconomy").combobox('setValue',data.entpczjjcf);
                    }else {
                        $("#inverstoreconomy").combobox('setValue',"");
                    }
                    if (data.entpmode != null){
                        $("#enterprisescale").combobox('setValue',data.entpmode);
                    }else {
                        $("#enterprisescale").combobox('setValue',"");
                    }
                    if (data.regamtcreny != null){
                        $("#isfarmerloan").combobox('setValue',data.regamtcreny);
                    }else {
                        $("#isfarmerloan").combobox('setValue',"");
                    }
                    if (data.actamtcreny != null){
                        $("#isgreenloan").combobox('setValue',data.actamtcreny);
                    }else {
                        $("#isgreenloan").combobox('setValue',"");
                    }
                },
                error: function (err) {
                    $.messager.alert('操作提示','系统错误','error');
                }
            });
        });
        $("#financeorgcode").attr("readOnly",false);
        $("#financeorginnum").attr("readOnly",false);
        $("#brroweridnum").attr("readOnly",false);
        $("#brrowerindustry").combobox({disabled: false});
        $("#inverstoreconomy").combobox({disabled: false});
        $("#enterprisescale").combobox({disabled: false});
        $('#formForAdd').form('clear');
    }

    // 取消
    function cancel(){
        $('#dialog1').dialog('close');
    }

    // 删除
    function deleteit(){
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        if(rows.length > 0){
            info = '是否删除所选中的数据？';
        }else {
            info = '您没有选择数据，是否删除所有数据？';
        }
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();
        var receiptcodeParam = $("#receiptcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var brroweridnumParam = $("#brroweridnumParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");

        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XDeleteXcldkxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"receiptcodeParam" : receiptcodeParam,"brroweridnumParam":brroweridnumParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','删除成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','删除失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','删除失败','error');
                    }
                });
                $("#dg1").datagrid("reload");
            }
        });
    }

    // 导入Excel相关
    function importExcel(){
        document.getElementById("importbtn").disabled = "disabled";
        $('#excelfile').val('');
        $('#importExcel').dialog('open').dialog('center');
    }

    function inexcel(){
        var file = document.getElementById("excelfile").files[0];
        if(file == null){
            document.getElementById("importbtn").disabled = "disabled";
        }else{
            var fileName = file.name;
            var fileType = fileName.substring(fileName.lastIndexOf('.'),
                fileName.length);
            if (fileType == '.xls' || fileType == '.xlsx'){
                if (file) {
                    document.getElementById("importbtn").disabled = "";
                }
            } else {
                $.messager.alert('提示', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！","error");
                document.getElementById("importbtn").disabled = "disabled";
            }
        }
    }

    function excel(){
        $.messager.progress({
            title: '请稍等',
            msg: '数据正在导入中......'
        });
        $.ajaxFileUpload({
            type: "post",
            url: 'XimmportExcelFour',
            fileElementId: 'excelfile',
            secureuri: false,
            dataType: 'json',
            success: function (data) {
                const that = data.msg;
                $.messager.progress('close');
                if (that == "导入成功") {
                    $.messager.alert('提示', "导入成功", 'info');
                    $("#dg1").datagrid('reload');
                    $('#importExcel').dialog('close');
                }else if (that == "补录表中有相同客户号码的数据"){
                    $.messager.alert('提示', "补录表中有相同客户号码的数据", 'info');
                    $("#dg1").datagrid('reload');
                    $('#importExcel').dialog('close');
                } else if (that.startsWith("导入模板不正确")) {
                    $.messager.alert('提示', that, 'error');
                } else {
                    $.messager.show({
                        title: '导入反馈',
                        msg: "<div style='overflow-y:scroll;height:100%'>" + escape2Html(that) + "</div>",
                        timeout: 0,
                        showType: 'show',
                        width: 600,
                        height: 700,
                        style: {
                            right: '100',
                            top: document.body.scrollTop + document.documentElement.scrollTop
                        }
                    });
                }
            },
            error: function () {
                $.messager.progress('close');
                $.messager.alert('提示', '导入文件错误，请重新导入', 'error');
            }
        });
    }

    // 导出
    function showOut(model){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        var info = '';
        if (model){
            id = '00000000';
            info = '是否导出模板？';
        }else {
            for( var dataIndex in row){
                id = id + row[dataIndex].id + ",";
            }
            if(id != ''){
                info = '是否将选中的数据导出到Excel表中？';
            }else {
                info = '是否将全部数据导出到Excel表中？'
            }
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();
        var receiptcodeParam = $("#receiptcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var brroweridnumParam = $("#brroweridnumParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        $.messager.confirm('操作提示', info, function (r) {
            if (r) {
                window.location.href = "XEExportXclgrdkxx?financeorgcodeParam="+financeorgcodeParam+"&contractcodeParam="+contractcodeParam+"&receiptcodeParam="+receiptcodeParam+"&brroweridnumParam="+brroweridnumParam+"&isfarmerloanParam="+isfarmerloanParam+"&checkstatusParam="+checkstatusParam +"&id="+id + "&datastatus="+'${datastatus}';
            }
        });
    }

    // 校验
  function check(){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        for( var dataIndex in row){
            id = id + row[dataIndex].id + ",";
            if (row[dataIndex].checkstatus != 0){
                $.messager.alert("提示", "只能校验未校验的数据", "info");
                return;
            }
        }
        var info = '';
        if(id != ''){
            info = '是否校验选中的数据？';
        }else {
            info = '是否校验所有数据？'
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();
        var receiptcodeParam = $("#receiptcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var brroweridnumParam = $("#brroweridnumParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");

        $.messager.confirm('提示',info,function (r) {
            if (r) {
                $.messager.progress({
                    title: '请稍等',
                    msg: '正在校验数据中......',
                   /* timeout: 100000,*/
                });

                $.ajax({
                    url: "XCheckDataFour",
                    dataType: "json",
                    async: true,
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"receiptcodeParam" : receiptcodeParam,"brroweridnumParam":brroweridnumParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam},
                    type: "POST",
                    success: function (data) {
                        $("#dg1").datagrid("reload");
                        $.messager.progress('close');
                        if (data.msg == "1") {
                            $.messager.alert('提示', '校验成功！', 'info');
                        }else if (data.msg == "-1") {
                            $.messager.alert('提示', '没有可校验的数据！', 'info');
                        } else {
                            $.messager.alert('提示', '校验完成,将自动下载校验结果', 'info', function (r) {
                                window.location.href = "XDownLoadCheckFour";
                            });
                        }
                    },
                    error: function () {
                        $.messager.progress('close');
                        $.messager.alert("提示", "校验出错，请重新校验！", "error");
                    }
                });
            }
        })
    }





    // 提交
    function submitit(){
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            for(var i = 0;i < rows.length;i++){
                if(rows[i].checkstatus != '1'){
                    $.messager.alert("提示", "只能提交校验成功的数据", "info");
                    return;
                }
                id = id + rows[i].id + ',';
            }
            info = '是否提交所选中的数据？';
        }else {
            info = '您没有选择数据，是否提交所有数据？';
        }
        $.messager.confirm('提示',info,function (r){
            if(r){
                var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
                var contractcodeParam = $("#contractcodeParam").val().trim();
                var receiptcodeParam = $("#receiptcodeParam").val().trim();
                var checkstatusParam = $("#checkstatusParam").combobox("getValue");
                var brroweridnumParam = $("#brroweridnumParam").val().trim();
                var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
                $.messager.confirm('提示',info,function (r){
                    if(r){
                        $.messager.progress({
                            title: '请稍等',
                            msg: '正在提交数据中......',
                        });
                        $.ajax({
                            sync: true,
                            type: "POST",
                            dataType: "json",
                            url: "XsubmitXclgrdkxx",
                            data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"receiptcodeParam" : receiptcodeParam,"brroweridnumParam":brroweridnumParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                            success: function (data) {
                                if (data == 0){
                                    $.messager.progress('close');
                                    $.messager.alert('操作提示','提交成功','info');
                                    $("#dg1").datagrid("reload");
                                }else {
                                    $.messager.progress('close');
                                    $.messager.alert('操作提示','提交失败','info');
                                }
                            },
                            error: function (err) {
                                $.messager.progress('close');
                                $.messager.alert('操作提示','提交失败','error');
                            }
                        });
                        $("#dg1").datagrid("reload");
                    }
                });
            }
        });
    }

    //申请删除dsh
    function applyDelete() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            info = '是否申请删除所选中的数据？';
        }else {
            info = '是否申请删除所有数据？';
        }
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();
        var receiptcodeParam = $("#receiptcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var brroweridnumParam = $("#brroweridnumParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            $.messager.progress({
                title: '请稍等',
                msg: '数据正在申请删除中......'
            });
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XApplyDeleteXclgrdkxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"receiptcodeParam" : receiptcodeParam,"brroweridnumParam":brroweridnumParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.progress('close');
                            $.messager.alert('操作提示','申请删除成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.progress('close');
                            $.messager.alert('操作提示','申请删除失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.progress('close');
                        $.messager.alert('操作提示','申请删除失败','error');
                    }
                });
            }
        });
    }

    //申请修改
    function applyEdit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            info = '是否申请修改所选中的数据？';
        }else {
            info = '是否申请修改所有数据？';
        }
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();
        var receiptcodeParam = $("#receiptcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var brroweridnumParam = $("#brroweridnumParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XApplyEditXclgrdkxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"receiptcodeParam" : receiptcodeParam,"brroweridnumParam":brroweridnumParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','申请修改成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','申请修改失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','申请修改失败','error');
                    }
                });
            }
        });
    }

    /*同意申请*/
    function agreeApply() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
                return;
            }
            if (rows[i].operationname != '申请删除'){
                $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否同意所选中的申请？';
        }else {
            info = '是否同意所有的申请？';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();
        var receiptcodeParam = $("#receiptcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var brroweridnumParam = $("#brroweridnumParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.messager.progress({
                    title: '请稍等',
                    msg: '数据正在同意申请中......'
                });
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XAgreeApplyXclgrdkxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"receiptcodeParam" : receiptcodeParam,"brroweridnumParam":brroweridnumParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.progress('close');
                            $.messager.alert('操作提示','同意成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.progress('close');
                            $.messager.alert('操作提示','同意失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.progress('close');
                        $.messager.alert('操作提示','同意失败','error');
                    }
                });
            }
        });
    }

    /*拒绝申请*/
    function noAgreeApply() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
                return;
            }
            if (rows[i].operationname != '申请删除'){
                $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否拒绝所选中的申请？';
        }else {
            info = '是否拒绝所有的申请？';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();
        var receiptcodeParam = $("#receiptcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var brroweridnumParam = $("#brroweridnumParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.messager.progress({
                    title: '请稍等',
                    msg: '数据正在拒绝申请中......'
                });
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XNoAgreeApplyXclgrdkxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"receiptcodeParam" : receiptcodeParam,"brroweridnumParam":brroweridnumParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.progress('close');
                            $.messager.alert('操作提示','拒绝成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.progress('close');
                            $.messager.alert('操作提示','拒绝失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.progress('close');
                        $.messager.alert('操作提示','拒绝意失败','error');
                    }
                });
            }
        });
    }

    /*审核通过*/
    function agreeAudit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
                return;
            }
            if (rows[i].checkstatus != 1){
                $.messager.alert("提示","请选择校验通过的数据！","error");
                return;
            }
            if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
            }else {
                $.messager.alert("提示","请选择操作名为空的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否审核通过所选中的数据？';
        }else {
            info = '是否审核通过所有数据？';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();
        var receiptcodeParam = $("#receiptcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var brroweridnumParam = $("#brroweridnumParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.messager.progress({
                    title: '请稍等',
                    msg: '数据正在通过中......'
                });
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XAgreeAuditXclgrdkxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"receiptcodeParam" : receiptcodeParam,"brroweridnumParam":brroweridnumParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.progress('close');
                            $.messager.alert('操作提示','审核通过成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.progress('close');
                            $.messager.alert('操作提示','审核通过失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.progress('close');
                        $.messager.alert('操作提示','审核通过失败','error');
                    }
                });
            }
        });
    }

    /*审核不通过*/
    function noAgreeAudit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
                return;
            }
            if (rows[i].checkstatus != 1){
                $.messager.alert("提示","请选择校验通过的数据！","error");
                return;
            }
            if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
            }else {
                $.messager.alert("提示","请选择操作名为空的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否审核不通过所选中的数据？';
        }else {
            info = '是否审核不通过所有数据？';
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();
        var receiptcodeParam = $("#receiptcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var brroweridnumParam = $("#brroweridnumParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.prompt('提示',info,function (r){
            if(r){
                $.messager.progress({
                    title: '请稍等',
                    msg: '数据正在不通过中......'
                });
                console.info("12")
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XNoAgreeAuditXclgrdkxx",
                    data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"receiptcodeParam" : receiptcodeParam,"brroweridnumParam":brroweridnumParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam,"reason":r},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.progress('close');
                            $.messager.alert('操作提示','审核不通过成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.progress('close');
                            $.messager.alert('操作提示','审核不通过失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.progress('close');
                        $.messager.alert('操作提示','审核不通过失败','error');
                    }
                });
            }else {
                $.messager.alert('操作提示','审核不通过原因必填','info');
            }
        });
    }

    //前台校验
    $.extend($.fn.datebox.defaults.rules, {
        normalDate : {
            validator : function(value) {
                var startTmp = new Date('1800-01-01');
                var endTmp = new Date('2100-12-31');
                var nowTmp = new Date(value);
                return nowTmp > startTmp && nowTmp < endTmp;
            },
            message : '日期早于1800-01-01且晚于2100-12-31'
        },
        dateFormat : {
            validator : function(value) {
                var reg = /^(\d{4})-(\d{2})-(\d{2})$/;
                return reg.test(value);
            },
            message : '日期格式必须满足:YYYY-MM-DD。'
        },



        // compareToExten : {
        //     validator : function(value) {
        //         var extensiondate = $('#extensiondate').combobox('getValue');
        //         var loanstatus = $('#loanstatus').combobox('getValue');
        //         if (extensiondate == null || extensiondate ==''){
        //             return true;
        //         }else if (loanstatus == 'LS04' && loanstatus!=''&& loanstatus!=null) {
        //             var startTmp = new Date(extensiondate);
        //             var nowTmp = new Date(value);
        //             return nowTmp >= startTmp;
        //         }
        //     },
        //     message : '贷款状态为LS04-缩期时，贷款到期日期应大于等于贷款展期到期日期'
        // },
        //
        // compareToExtenA : {
        //     validator : function(value) {
        //         var extensiondate = $('#extensiondate').combobox('getValue');
        //         var loanstatus = $('#loanstatus').combobox('getValue');
        //         if (extensiondate == null || extensiondate ==''){
        //             return true;
        //         }else if (loanstatus != 'LS04') {
        //             var startTmp = new Date(extensiondate);
        //             var nowTmp = new Date(value);
        //             return nowTmp <= startTmp;
        //         }
        //     },
        //     message : '贷款到期日期应小于等于贷款展期到期日期'
        // },


        compareToExten : {
            validator : function(value) {
                var extensiondate = $('#extensiondate').combobox('getValue');
                var LS04 = $('#loanstatus').combobox('getValue');
                if (extensiondate == null || extensiondate ==''){
                    return true;
                }
                if(LS04 != "LS04"){
                    var startTmp = new Date(extensiondate);
                    var nowTmp = new Date(value);
                    return nowTmp <= startTmp;
                }
                return true;
            },
            message : '贷款到期日期应小于等于贷款展期到期日期'
        },
        compareToExtenLS : {
            validator : function(value) {
                var extensiondate = $('#extensiondate').combobox('getValue');
                var LS04 = $('#loanstatus').combobox('getValue');
                if (extensiondate == null || extensiondate ==''){
                    return true;
                }
                if(LS04 == "LS04"){
                    var startTmp = new Date(extensiondate);
                    var nowTmp = new Date(value);
                    return nowTmp > startTmp;
                }
                return true;
            },
            message : '贷款状态为LS04-缩期时，贷款到期日期应大于贷款展期到期日期'
        },


        startcompareToExten : {
            validator : function(value) {
                var extensiondate = $('#extensiondate').combobox('getValue');
                if (extensiondate == null || extensiondate ==''){
                    return true;
                }
                var startTmp = new Date(extensiondate);
                var nowTmp = new Date(value);
                return nowTmp <= startTmp;
            },
            message : '贷款发放日期应小于等于贷款展期到期日期'
        },
        compareToEnd : {
            validator : function(value) {
                var loanenddate = $('#loanenddate').combobox('getValue');
                if (loanenddate == null || loanenddate ==''){
                    return true;
                }
                var startTmp = new Date(loanenddate);
                var nowTmp = new Date(value);
                return nowTmp <= startTmp;
            },
            message : '贷款发放日期应小于等于贷款到期日期'
        },
        teshu:{
            validator: function (value) {
                var patrn = /[？?！!^]/;
                return !patrn.test(value);
            },
            message: '不能包含特殊符号和空格'
        },
        teshua:{
            validator: function (value) {
                var patrn = /[？?！!^]/;
                return !patrn.test(value);
            },
            message: '不能包含特殊符号和空格'
        },

        amount : {
            validator : function(value) {
                var reg = /^([0-9]\d{0,18}(\.\d{2})?|0\.\d{2})$/;
                return reg.test(value);
            },
            message : '总长度不超过20位的，精度保留小数点后两位。'
        },


        baserate : {
            validator : function(value) {
                if (value.indexOf("%") == -1 && value.indexOf("‰") == -1){
                    return true
                }
                return false;
            },
            message : '基准利率不能包含‰或%'
        },
        ratelevel : {
            validator : function(value) {
                var reg = /^([0-9]\d{0,3}(\.\d{5})?|0\.\d{5})$/;
                return reg.test(value);
            },
            message : '总长度不能超过10位，小数位应保留5位'
        },


    });
    function keydownsearch(){
        //combobox可编辑，自定义模糊查询
        $.fn.combobox.defaults.editable = true;
        $.fn.combobox.defaults.filter = function(q, row){
            var opts = $(this).combobox('options');
            return row[opts.textField].indexOf(q) >= 0;
        };
    }
    //返回
    function fanhui(){
        $.messager.progress({
            title: '请稍等',
            msg: '数据正在加载中......'
        });
    }
</script>
</body>
</html>
