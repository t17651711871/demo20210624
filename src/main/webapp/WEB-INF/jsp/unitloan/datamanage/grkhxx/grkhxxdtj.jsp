<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountryTwo" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseEducation" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseNation" %>
<%@ page import="com.geping.etl.common.util.VariableUtils" %>
<%@ page import="com.geping.etl.common.entity.Sys_Auth_Role_Resource" %>
<%@ page import="com.geping.etl.common.entity.Sys_UserAndOrgDepartment" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    VariableUtils vu = (VariableUtils)request.getSession().getAttribute("vu");
    Set<Sys_Auth_Role_Resource> operateSet = vu.getOperateReportSet();
    String status = request.getParameter("status");
    String shifoushenheziji = (String) session.getAttribute("shifoushenheziji");
    String role = vu.getRole();
    Sys_UserAndOrgDepartment sys_User = ((Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User"));
    String handlename = sys_User.getLoginid();
    String isusedelete = (String) request.getSession().getAttribute("isusedelete");
    String isuseupdate = (String) request.getSession().getAttribute("isuseupdate");
    List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
    List<BaseCountry> baseCountryList = (List<BaseCountry>) request.getAttribute("baseCountryList");
    List<String> baseAreas = new ArrayList<>();
    for (BaseArea baseArea : baseAreaList) {
        baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
    }
    for (BaseCountry baseCountry : baseCountryList) {
        baseAreas.add("'"+baseCountry.getCountrycode()+ "-" +baseCountry.getCountryname()+"'");
    }
    List<BaseAindustry> baseAindustryList = (List<BaseAindustry>) request.getAttribute("baseAindustryList");
    List<String> baseAindustrys = new ArrayList<>();
    for (BaseAindustry baseAindustry : baseAindustryList) {
        baseAindustrys.add("'"+baseAindustry.getAindustrycode()+ "-" +baseAindustry.getAindustryname()+"'");
    }
    List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
    List<String> baseCurrencys = new ArrayList<>();
    for (BaseCurrency baseCurrency : baseCurrencyList) {
        baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
    }    
    List<BaseCountryTwo> baseCountryTwoList = (List<BaseCountryTwo>) request.getAttribute("baseCountryTwoList");
    List<String> baseCountryTwos = new ArrayList<>();
    for (BaseCountryTwo baseCountryTwo : baseCountryTwoList) {
    	baseCountryTwos.add("'"+baseCountryTwo.getCountrytwocode()+ "-" +baseCountryTwo.getCountrytwoname()+"'");
    }
    List<BaseEducation> baseEducationList = (List<BaseEducation>) request.getAttribute("baseEducationList");
    List<String> baseEducations = new ArrayList<>();
    for (BaseEducation baseEducation : baseEducationList) {
    	baseEducations.add("'"+baseEducation.getEducationcode()+ "-" +baseEducation.getEducationname()+"'");
    }
    List<BaseNation> baseNationList = (List<BaseNation>) request.getAttribute("baseNationList");
    List<String> baseNations = new ArrayList<>();
    for (BaseNation baseNation : baseNationList) {
    	baseNations.add("'"+baseNation.getNationcode()+ "-" +baseNation.getNationname()+"'");
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>个人客户基础信息</title>
    <%@include file="../../../common/head.jsp"%>
</head>
<body>

<table id="dg1" style="height: 480px;" title="个人客户基础信息数据列表"></table>

<!--表格工具栏-->
<div id="tb1" style="padding: 5px; height: auto;">
    <%if("dtj".equals(status) || "dsh".equals(status)){%>
    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return" onclick="fanhui()">返回</a>&nbsp;&nbsp;
    <%}else if ("dsb".equals(status)){%>
    <a class="easyui-linkbutton" href="VGenerateMessageUi" iconCls="icon-return" onclick="fanhui()">返回</a>&nbsp;&nbsp;
    <%}%>&nbsp;&nbsp;
    <label>金融机构代码：</label><input id="finorgcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>客户证件类型：</label><select class='easyui-combobox' id='regamtcrenyParam' style='height: 23px; width:100px'>
    						<option value=''>=请选择=</option>
                            <option value='A01'>A01-统一社会信用代码</option>
                            <option value='A02'>A02-组织机构代码</option>
                            <option value='A03'>A03-其他</option>
                            <option value='B01'>B01-身份证</option>
					        <option value='B02'>B02-户口簿</option>
					        <option value='B03'>B03-护照</option>
					        <option value='B04'>B04-军官证</option>
					        <option value='B05'>B05-士兵证</option>
					        <option value='B06'>B06-港澳居民来往内地通行证</option>
					        <option value='B07'>B07-台湾同胞来往内地通行证</option>
					        <option value='B08'>B08-临时身份证</option>
					        <option value='B09'>B09-外国人居留证</option>
					        <option value='B10'>B10-警官证</option>
					        <option value='B11'>B11-外国人永久居留身份证</option>
					        <option value='B12'>B12-港澳台居民居住证</option>
					        <option value='B99'>B99-其他证件</option>
                        </select>
    <label>客户证件代码：</label><input id="customercodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>校验类型：</label>
    <select class='easyui-combobox' id='checkstatusParam' name='checkstatusParam' editable=false style='height: 23px; width:80px' >
        <option value=''>=请选择=</option>
        <option value='0'>未校验</option>
        <option value='1'>校验成功</option>
        <option value='2'>校验失败</option>
    </select>&nbsp;&nbsp;
    <%if("dtj".equals(status)){%>
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected('dtj')">查询</a>&nbsp;&nbsp;<br>
    <%}else if ("dsh".equals(status)){%>
    <label>操作名：</label>
    <select class='easyui-combobox' id='operationnameParam' name='operationnameParam' editable=false style='height: 23px; width:80px' >
        <option value=''>=请选择=</option>
        <option value='申请删除'>申请删除</option>
    </select>&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected('dsh')">查询</a>&nbsp;&nbsp;<br>
    <%}%>
    <%if("dtj".equals(status)){
        for(Sys_Auth_Role_Resource sarr : operateSet){
            if(sarr.getResValue().equals("ADD")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-add" onclick="add()">新增</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("EDIT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-edit" onclick="edit()">修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("APPLYDELETE")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-remove" onclick="applyDelete()">申请删除</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("IMPORT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="importExcel()">导入</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("EXPORT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(false)">导出</a>&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(true)">导出模板</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("CHECKOUT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-accept" onclick="check()">校验</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("SUBMIT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-easyicon-net-16" onclick="submitit()">提交</a>
    <%
            }
        }
    }else if("dsh".equals(status)){
        for(Sys_Auth_Role_Resource sarr : operateSet){
            if(sarr.getResValue().equals("EDIT") && "yes".equals(isuseupdate)){
    %>
    <%--<a class="easyui-linkbutton" iconCls="icon-edit" onclick="applyEdit()" >申请修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
    <%
    }else if(sarr.getResValue().equals("DELETE") && "yes".equals(isusedelete)){
    %>
    <%--<a class="easyui-linkbutton" data-options="iconCls:'icon-remove'" onclick="applyDelete()" >申请删除</a>&nbsp;--%>
    <%
    }else if(sarr.getResValue().equals("CHECK")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-ok" onclick="agreeApply()" >同意申请删除</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-cross" onclick="noAgreeApply()" >拒绝申请删除</a>&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-application-get" onclick="agreeAudit()" >审核通过</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-cross" onclick="noAgreeAudit()" >审核不通过</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(false)">导出</a>&nbsp;&nbsp;&nbsp;
    <%
                }
            }
        }%>
</div>

<!-- 导入文件dialog -->
<div style="visibility: hidden;">
    <div id="importExcel" class="easyui-dialog" style="width: 600px; height: 150px;top: 100px;padding: 20px;" title="数据导入" data-options="modal:true,closed:true">
        文件导入:<input type="file" name="excelfile" id="excelfile" onchange="inexcel()"/><br>
        <input type="button" id="importbtn" value="提交" style="width:50px;margin-top:20px;" onclick="excel()"/>
    </div>
</div>

<!--新增实际控制人证件类型窗口-->
<div id="dialogactctrlidtype" class="easyui-dialog" style="width:450px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div id="tbForAddDialogactctrlidtype">
        <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveactctrlidtype()">确定</a>&nbsp;&nbsp;
        <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="cancelactctrlidtype()">取消</a>
    </div><br>
    <div align="center">
        <textarea id="areaactctrlidtype" style="width: 360px;height: 40px" disabled="disabled"></textarea>
    </div><br>
    <!-- 信息录入 -->
    <div style="position:absolute; height:240px; margin-left: 45px; overflow:auto"; align="center">
        <table align="center">
            <tr>
                <td align="left">A01-统一社会信用代码</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('A01')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('A01')"></a></td>
            </tr>
            <tr>
                <td align="left">A02-组织机构代码</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('A02')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('A02')"></a></td>
            </tr>
            <tr>
                <td align="left">A03-其他</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('A03')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('A03')"></a></td>
            </tr>
            <tr>
                <td align="left">B01-身份证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B01')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B01')"></a></td>
            </tr>
            <tr>
                <td align="left">B02-户口簿</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B02')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B02')"></a></td>
            </tr>
            <tr>
                <td align="left">B03-护照</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B03')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B03')"></a></td>
            </tr>
            <tr>
                <td align="left">B04-军官证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B04')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B04')"></a></td>
            </tr>
            <tr>
                <td align="left">B05-士兵证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B05')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B05')"></a></td>
            </tr>
            <tr>
                <td align="left">B06-港澳居民来往内地通行证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B06')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B06')"></a></td>
            </tr>
            <tr>
                <td align="left">B07-台湾同胞来往内地通行证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B07')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B07')"></a></td>
            </tr>
            <tr>
                <td align="left">B08-临时身份证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B08')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B08')"></a></td>
            </tr>
            <tr>
                <td align="left">B09-外国人居留证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B09')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B09')"></a></td>
            </tr>
            <tr>
                <td align="left">B10-警官证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B10')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B10')"></a></td>
            </tr>
            <tr>
                <td align="left">B11-外国人永久拘留省份证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B11')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B11')"></a></td>
            </tr>
            <tr>
                <td align="left">B12港澳台居民居住证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B12')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B12')"></a></td>
            </tr>
            <tr>
                <td align="left">B99-其他证件</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addactctrlidtypeCode('B99')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deleteactctrlidtypeCode('B99')"></a></td>
            </tr>
        </table>
    </div>
</div>

<!--新增修改窗口-->
<div id="dialog1" class="easyui-dialog" style="width:800px;height:450px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div id="tbForAddDialog" style="padding-left: 100px;padding-top: 10px">
        <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="save()">确定</a>&nbsp;&nbsp;
        <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="cancel()">取消</a>
    </div>

    <!-- 信息录入 -->
    <div align="center" style="padding-top: 30px">
        <form id="formForAdd" method="post">
            <input id="id" class="backId" name="id" type="hidden">

        </form>
    </div>
</div>

<!--详情窗口-->
<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div align="center" style="padding-top: 30px">
        <form id="info_formForAdd" method="post">
            <input id="info_id" class="backId" name="id" type="hidden">
            <table class='enterTable' id='info_enterTable'>
                <tr>
                    <td align='right'>
                        金融机构代码:
                    </td>
                    <td>
                        <input id='info_finorgcode' name='finorgcode' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        客户证件类型:
                    </td>
                    <td>
                        <input id='info_regamtcreny' name='regamtcreny' style='width:173px;' maxlength='200' disabled="disabled"/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        客户证件代码:
                    </td>
                    <td>
                        <input id='info_customercode' name='customercode' style='width:173px;' disabled="disabled"/>
                    </td>
                    <td align='right'>
                        国籍:
                    </td>
                    <td>
                        <select id='info_country' name='country' style='width:173px;' disabled="disabled">
                           <c:forEach items='${baseCountryTwoList}' var='aa'>                                                                                                        
                                <option value='${aa.countrytwocode}'>${aa.countrytwocode}-${aa.countrytwoname}</option>                                                                 
                           </c:forEach>                                        
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                       民族:
                    </td>
                    <td>
                        <select id='info_nation' name='nation' style='width:173px;' disabled="disabled">
                        <c:forEach items='${baseNationList}' var='aa'>                                                                                                        
                                <option value='${aa.nationcode}'>${aa.nationcode}-${aa.nationname}</option>                                                                 
                           </c:forEach>
                        </select>
                    </td>
                    <td align='right'>
                        性别:
                    </td>
                    <td>
                        <select id='info_sex' name='sex' style='width:173px;' disabled="disabled">
                        	<option value='01'>01-男性</option>				
        					<option value='02'>02-女性</option>
        					<option value='03'>03-其他性别</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        最高学历:
                    </td>
                    <td>
                        <select id='info_education' name='education' style='height: 23px; width:173px' disabled="disabled">
                            <c:forEach items='${baseEducationList}' var='aa'>                                                                                                        
                                <option value='${aa.educationcode}'>${aa.educationcode}-${aa.educationname}</option>                                                                 
                           </c:forEach>
                        </select>
                    </td>
                    <td align='right'>
                        出生日期:
                    </td>
                    <td>
                        <input id='info_birthday' name='birthday' style='width:173px;' disabled="disabled"/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        地区代码:
                    </td>
                    <td>
                        <input id='info_regareacode' name='regareacode' style='height: 23px; width:173px' disabled="disabled"/>
                    </td>
                    <td align='right'>
                        个人年收入:
                    </td>
                    <td>
                        <input id='info_grincome' name='grincome' style='width:173px;' disabled="disabled"/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        家庭年收入:
                    </td>
                    <td>
                        <input id='info_familyincome' name='familyincome' style='width:173px;' disabled="disabled"/>
                    </td>
                    <td align='right'>
                        婚姻情况:
                    </td>
                    <td>
                        <select id='info_marriage' name='marriage' disabled="disabled" style='height: 23px; width:173px' >
                                <option value='M01'>M01-未婚</option>				
        						<option value='M02'>M02-已婚</option>
        						<option value='M06'>M06-丧偶</option>
        						<option value='M07'>M07-离异</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        是否关联方:
                    </td>
                    <td>
                        <select id='info_isrelation' name='isrelation' disabled="disabled" style='height: 23px; width:173px' >
                            <option value='1'>1-是</option>
                            <option value='0'>0-否</option>
                        </select>
                    </td>
                    <td align='right'>
                        授信额度:
                    </td>
                    <td>
                        <input id='info_creditamt' name='creditamt' style='width:173px;' disabled="disabled"/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        已用额度:
                    </td>
                    <td>
                        <input id='info_usedamt' name='usedamt' style='width:173px;' disabled="disabled"/>
                    </td>
                    <td align='right'>
                        个人客户身份标识:
                    </td>
                    <td>
                        <input id='info_grkhsfbs' name='grkhsfbs' style='height: 23px; width:173px' disabled="disabled">
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                       个体工商户营业执照代码:
                    </td>
                    <td>
                        <input id='info_gtgshyyzzdm' name='gtgshyyzzdm' style='width:173px;' disabled="disabled"/>
                    </td>
                    <td align='right'>
                        小微企业社会统一信用代码:
                    </td>
                    <td>
                        <input id='info_xwqyshtyxydm' name='xwqyshtyxydm' style='width:173px;' disabled="disabled"/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        客户信用级别总等级数:
                    </td>
                    <td>
                        <input id='info_workareacode' name='workareacode' style='width:173px;' disabled="disabled"/>
                    </td>
                    <td align='right'>
                        客户信用评级:
                    </td>
                    <td>
                        <input id='info_actctrltype' name='actctrltype' style='width:173px;' disabled="disabled"/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        数据日期:
                    </td>
                    <td>
                        <input id='info_sjrq' name='sjrq' style='width:173px;' disabled="disabled"/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<script type="text/javascript">
    var index = 5;
    var baseAreas = <%=baseAreas%>;
    var baseAindustrys = <%=baseAindustrys%>;
    var baseCurrencys = <%=baseCurrencys%>;
    var baseCountryTwos = <%=baseCountryTwos%>;
    var baseEducations = <%=baseEducations%>;
    var baseNations = <%=baseNations%>;
    var handlename = '<%= handlename%>';
    var role = '<%= role%>';
    var shifoushenheziji = '<%= shifoushenheziji%>';
    var tablelazyload =
			"<table class='enterTable' id='enterTable'>                                                                                                                               "
            +"    <tr>                                                                                                                                                                 "
            +"        <td align='right'>                                                                                                                                               "
            +"            金融机构代码:                                                                                                                                                "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <input id='finorgcode' name='finorgcode' style='width:173px;' class='easyui-validatebox' maxlength='18' data-options='validType:[\"teshu\"]'/>                                      "
            +"        </td>                                                                                                                                                            "
            +"        <td align='right'>                                                                                                                                               "
            +"            客户证件类型:                                                                                                                                                "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <select class='easyui-combobox' id='regamtcreny' name='regamtcreny' editable=false style='height: 23px; width:173px' >           "
            +"                	<option value='A01'>A01-统一社会信用代码</option>                                                                                                         "
            +"                	<option value='A02'>A02-组织机构代码</option>                                                                    "
            +"                	<option value='A03'>A03-其他</option>                                                                                                                                             "
            +"        			<option value='B01'>B01-身份证</option>"
            +"        			<option value='B02'>B02-户口簿</option>"
            +"        			<option value='B03'>B03-护照</option>"
            +"        			<option value='B04'>B04-军官证</option>"
            +"        			<option value='B05'>B05-士兵证</option>"
            +"        			<option value='B06'>B06-港澳居民来往内地通行证</option>"
            +"        			<option value='B07'>B07-台湾同胞来往内地通行证</option>"
            +"        			<option value='B08'>B08-临时身份证</option>"
            +"        			<option value='B09'>B09-外国人居留证</option>"
            +"        			<option value='B10'>B10-警官证</option>"
            +"        			<option value='B11'>B11-外国人永久居留身份证</option>"
            +"        			<option value='B12'>B12-港澳台居民居住证</option>"
            +"        			<option value='B99'>B99-其他证件</option>"
            +"            </select>                                                                                                                                                    "
            +"        </td>                                                                                                                                                            "
            +"    </tr>                                                                                                                                                                "
            +"    <tr>                                                                                                                                                                 "
            +"        <td align='right'>                                                                                                                                               "
            +"            客户证件代码:                                                                                                                                                    "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <input id='customercode' name='customercode' style='width:173px;' class='easyui-validatebox' maxlength='60' data-options='validType:[\"teshua\"]'/>                                  "
            +"        </td>                                                                                                                                                            "
            +"        <td align='right'>                                                                                                                                               "
            +"            国籍:                                                                                                                                                "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <select id='country' name='country' style='width:173px;' class='easyui-combobox' maxlength='60' >                                      "
            +"                <c:forEach items='${baseCountryTwoList}' var='aa'>                                                                                                        "
            +"                    <option value='${aa.countrytwocode}'>${aa.countrytwocode}-${aa.countrytwoname}</option>                                                                 "
            +"                </c:forEach>                                                                                                                                             "
            +"            </select>                                                                                                                                                    "
            +"        </td>                                                                                                                                                            "
            +"    </tr>                                                                                                                                                                "
            +"    <tr>                                                                                                                                                                 "
            +"        <td align='right'>                                                                                                                                               "
            +"            民族:                                                                                                                                          "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <select id='nation' name='nation' style='width:173px;' class='easyui-combobox' maxlength='200' >                        "
            +"                <c:forEach items='${baseNationList}' var='aa'>                                                                                                        "
            +"                    <option value='${aa.nationcode}'>${aa.nationcode}-${aa.nationname}</option>                                                                 "
            +"                </c:forEach>                                                                                                                                             "
            +"            </select>                                                                                                                                                    "
            +"        </td>                                                                                                                                                            "
            +"        <td align='right'>                                                                                                                                               "
            +"            性别:                                                                                                                                                    "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <select id='sex' name='sex' style='width:173px;' editable=false class='easyui-combobox' >                                              "
            +"					<option value='01'>01-男性</option>"
            +"					<option value='02'>02-女性</option>"
            +"					<option value='03'>03-其他性别</option>"
            +"            </select>                                                                                                                                                    "
            +"        </td>                                                                                                                                                            "
            +"    </tr>                                                                                                                                                                "
            +"    <tr>                                                                                                                                                                 "          
            +"        <td align='right'>                                                                                                                                               "
            +"            最高学历:                                                                                                                                                    "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <select id='education' name='education' style='width:173px;' class='easyui-combobox' >                                              "
            +"                <c:forEach items='${baseEducationList}' var='aa'>                                                                                                        "
            +"                    <option value='${aa.educationcode}'>${aa.educationcode}-${aa.educationname}</option>                                                                 "
            +"                </c:forEach>"
            +"            </select>"
            +"        </td>                                                                                                                                                            "
            +"        <td align='right'>                                                                                                                                               "
            +"            出生日期:                                                                                                                                                "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <input class='easyui-datebox' id='birthday' name='birthday' editable=false style='height: 23px; width:173px' />           "
            +"        </td>                                                                                                                                                            "
            +"    </tr>                                                                                                                                                                "
            +"    <tr>                                                                                                                                                                 "
            +"        <td align='right'>                                                                                                                                               "
            +"            地区代码:                                                                                                                                                      "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <select class='easyui-combotree' id='regareacode' name='regareacode' style='height: 23px; width:173px'  data-options=\"url:'XgetAdmindivideJson',valueField:'id',textField:'text',editable:false,value:''\">          "
            +"            </select>                                                                                                                                                    "
            +"        </td>                                                                                                                                                            "
            +"        <td align='right'>                                                                                                                                               "
            +"            个人年收入:                                                                                                                                                      "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <input id='grincome' name='grincome' style='width:173px;' class='easyui-validatebox easyui-numberbox' data-options='precision:2,validType:[\"amount\"]'/>                                              "
            +"        </td>                                                                                                                                                            "
            +"    </tr>                                                                                                                                                                "
            +"    <tr>                                                                                                                                                                 "
            +"        <td align='right'>                                                                                                                                               "
            +"            家庭年收入:                                                                                                                                                "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <input class='easyui-validatebox easyui-numberbox' id='familyincome' name='familyincome' data-options='precision:2,validType:[\"amount\"]' style='height: 23px; width:173px' />                                         "
            +"        </td>                                                                                                                                                            "
            +"        <td align='right'>                                                                                                                                               "
            +"            婚姻情况:                                                                                                                                        "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <select id='marriage' name='marriage' style='width:173px;' editable=false class='easyui-combobox' >                                             "
        	+"					<option value='M01'>M01-未婚</option>"				
        	+"					<option value='M02'>M02-已婚</option>"
        	+"					<option value='M06'>M06-丧偶</option>"
        	+"					<option value='M07'>M07-离异</option>"
        	+"			  </select>"				
            +"        </td>                                                                                                                                                            "
            +"    </tr>                                                                                                                                                                "
            +"    <tr>                                                                                                                                                                 "
            +"        <td align='right'>                                                                                                                                               "
            +"            是否关联方:                                                                                                                                                  "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <select class='easyui-combobox' id='isrelation' name='isrelation' editable=false  style='height: 23px; width:173px' >                                         "
            +"                <option value='1'>1-是</option>                                                                                                                          "
            +"                <option value='0'>0-否</option>                                                                                                                          "
            +"            </select>                                                                                                                                                    "
            +"        </td>                                                                                                                                                            "
            +"        <td align='right'>                                                                                                                                               "
            +"        授信额度:                                                                                                                                                        "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <input id='creditamt' name='creditamt' style='width:173px;' class='easyui-validatebox easyui-numberbox' data-options='precision:2,validType:[\"amount\"]'/>                                            "
            +"        </td>                                                                                                                                                            "
            +"    </tr>                                                                                                                                                                "
            +"    <tr>                                                                                                                                                                 "
            +"        <td align='right'>                                                                                                                                               "
            +"            已用额度:                                                                                                                                                    "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <input id='usedamt' name='usedamt' style='width:173px;' class='easyui-validatebox easyui-numberbox' data-options='precision:2,validType:[\"amount\"]'/>                                                "
            +"        </td>                                                                                                                                                            "
            +"        <td align='right'>                                                                                                                                               "
            +"            个人客户身份标识:                                                                                                                                                    "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <select id='grkhsfbs' name='grkhsfbs' style='width:173px;' editable=false class='easyui-combobox' >                                          "
            +"					<option value='1'>1-农户</option>"
            +"					<option value='2'>2-个体工商户</option>"
            +"					<option value='3'>3-小微企业主</option>"
            +"					<option value='9'>9-其他</option>"
            +"			  </select>"	
            +"        </td>                                                                                                                                                            "
            +"    </tr>                                                                                                                                                                "
            +"    <tr>                                                                                                                                                                 "
            +"        <td align='right'>                                                                                                                                               "
            +"            个体工商户营业执照代码:                                                                                                                                          "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <input id='gtgshyyzzdm' name='gtgshyyzzdm' style='width:173px;' class='easyui-validatebox'/>                                          "
            +"        </td>                                                                                                                                                            "
            +"        <td align='right'>                                                                                                                                               "
            +"            小微企业社会统一信用代码:                                                                                                                                                    "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <input class='easyui-validatebox' id='xwqyshtyxydm' name='xwqyshtyxydm'  style='height: 23px; width:173px' />                                                                 "
            +"        </td>                                                                                                                                                            "
            +"    </tr>                                                                                                                                                                "
            +"    <tr>                                                                                                                                                                 "
            +"        <td align='right'>                                                                                                                                               "
            +"            客户信用级别总等级数:                                                                                                                                          "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <input id='workareacode' name='workareacode' style='width:173px;' class='easyui-validatebox'/>                                          "
            +"        </td>                                                                                                                                                            "
            +"        <td align='right'>                                                                                                                                               "
            +"            客户信用评级:                                                                                                                                                    "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <input class='easyui-validatebox' id='actctrltype' name='actctrltype' style='height: 23px; width:173px' />                 "
            +"        </td>                                                                                                                                                            "
            +"    </tr>                                                                                                                                                                "
            +"    <tr>                                                                                                                                                                 "
            +"        <td align='right'>                                                                                                                                               "
            +"            数据日期:                                                                                                                                        "
            +"        </td>                                                                                                                                                            "
            +"        <td>                                                                                                                                                             "
            +"            <input id='sjrq' name='sjrq' style='width:173px;' class='easyui-datebox' data-options='validType:[\"dateFormat\",\"normalDate\"]'/>                                             "
            +"        </td>"
            +"    </tr>                                                                                                                                                                "
            +"</table>                                                                                                                                                                 "
    $(function(){
        $('#dialog1').dialog({
            onClose: function () {
                $('.validatebox-tip').remove();
            }
        });
        $("#dg1").datagrid({
            loadMsg:'数据加载中,请稍后...',
            method:'post',
            url:'XGetXgrkhxxData?datastatus='+'${datastatus}',
            checkOnSelect: true,
            autoRowHeight: false,
            pagination: true,
            rownumbers: true,
            toolbar: '#tb1',
            fitColumns: false,
            pageSize:20,
            pageList:[15,20,30,50],
            columns:[[
                {field: 'ck', checkbox: true},
                {field: 'id', title: '编号', width: 80, align: 'center', hidden: true},
                {field: 'operationname', title: '操作名', width: 150, align: 'center'},
                {field: 'nopassreason', title: '审核不通过原因', width: 150, align: 'center'},
                {field: 'checkstatus', title: '校验结果', width: 150, align: 'center', formatter: function (value) {
                        if (value == '0') {
                            return "未校验";
                        } else if (value == '1') {
                            return "校验成功";
                        } else if (value == '2') {
                            return '<span style="color:red;">' + '校验失败' + '</span>';
                        }
                        return value;
                    }
                },
                {field: 'finorgcode', title: '金融机构代码', width: 100, align: 'center'},
                {field: 'regamtcreny', title: '客户证件类型', width: 100, align: 'center',formatter:function (value) {
                    if (value=='A01'){
                        return "A01-统一社会信用代码";
                    }else if(value=='A02'){
                        return "A02-组织机构代码";
                    }else if (value=='A03'){
                        return "A03-其他";
                    }else if (value =='B01'){
                        return "B01-身份证";
                    }else if (value =='B02'){
                        return "B02-户口簿";
                    }else if (value =='B03'){
                        return "B03-护照";
                    }else if (value =='B04'){
                        return "B04-军官证";
                    }else if (value =='B05'){
                        return "B05-士兵证";
                    }else if (value =='B06'){
                        return "B06-港澳居民来往内地通行证";
                    }else if (value =='B07'){
                        return "B07-台湾同胞来往内地通行证";
                    }else if (value =='B08'){
                        return "B08-临时身份证";
                    }else if (value =='B09'){
                        return "B09-外国人居留证";
                    }else if (value =='B10'){
                        return "B10-警官证";
                    }else if (value =='B11'){
                        return "B11-外国人永久居留身份证";
                    } else if (value =='B12'){
                        return "B12-港澳台居民居住证";
                    } else if (value =='B99'){
                        return "B99-其他证件";
                    }
                    return value;
                }},
                {field: 'customercode', title: '客户证件代码', width: 100, align: 'center'},
                {field: 'country', title: '国籍', width: 100, align: 'center',formatter:function (value) {
                	for (var i = 0;i <  baseCountryTwos.length;i++){
                        if (baseCountryTwos[i].substring(0,2)==value){
                            return baseCountryTwos[i];
                        }
                    }
                    }
                },
                {field: 'nation', title: '民族', width: 100, align: 'center',formatter:function (value) {
                	for (var i = 0;i <  baseNations.length;i++){
                        if (baseNations[i].substring(0,2)==value){
                            return baseNations[i];
                        }
                    }
                    }
                },
                {field: 'sex', title: '性别', width: 100, align: 'center', formatter: function (value) {
                	if(value == '01'){
                		return "01-男性";
                	}else if(value == '02'){
                		return "02-女性";
                	}else if(value == '03'){
                		return "03-其他性别";
                	}else{
                		return value;
                	}
                }
                },
                {field: 'education', title: '最高学历', width: 100, align: 'center',formatter:function (value) {
                	for (var i = 0;i <  baseEducations.length;i++){
                        if (baseEducations[i].substring(0,2)==value){
                            return baseEducations[i];
                        }
                    }
                    }
                },
                {field: 'birthday', title: '出生日期', width: 100, align: 'center'},
                {field: 'regareacode', title: '地区代码', width: 100, align: 'center',formatter:function (value) {
                	for (var i = 0;i <  baseAreas.length;i++){
                        if (baseAreas[i].substring(0,6)==value){
                            return baseAreas[i];
                        }
                    }
                    }},
                {field: 'grincome', title: '个人年收入', width: 100, align: 'center'},
                {field: 'familyincome', title: '家庭年收入', width: 100, align: 'center'},
                {field: 'marriage', title: '婚姻情况', width: 100, align: 'center', formatter: function (value) {
                	if(value == 'M01'){
                		return "M01-未婚";
                	}else if(value == 'M02'){
                		return "M02-已婚";
                	}else if(value == 'M03'){
                		return "M03-丧偶";
                	}else if(value == 'M07'){
                		return "M07-离异";
                	}
                }
                },
                 {field: 'isrelation', title: '是否关联方', width: 100, align: 'center',formatter:function (value) {
                     if (value=='1'){
                         return "1-是";
                     }else if(value=='0'){
                         return "0-否";
                     }
                     return value;
                 }},
                {field: 'creditamt', title: '授信额度', width: 100, align: 'center'},
                {field: 'usedamt', title: '已用额度', width: 100, align: 'center'},
				{field: 'grkhsfbs', title: '个人客户身份标识', width: 100, align: 'center', formatter: function (value) {
                	if(value == '1'){
                		return "1-农户";
                	}else if(value == '2'){
                		return "2-个体工商户";
                	}else if(value == '3'){
                		return "3-小微企业主";
                	}else if(value == '9'){
                		return "9-其他";
                	}else{
                		return value;
                	}
				}
                },
                {field: 'gtgshyyzzdm', title: '个体工商户营业执照代码', width: 100, align: 'center'},
                {field: 'xwqyshtyxydm', title: '小微企业社会统一信用代码', width: 100, align: 'center'},
                {field: 'workareacode', title: '客户信用级别总等级数', width: 100, align: 'center'},
                {field: 'actctrltype', title: '客户信用评级', width: 100, align: 'center'},
                {field: 'sjrq', title: '数据日期', width: 100, align: 'center'},
                {field: 'operator', title: '操作人',  align: 'center'},
                {field: 'operationtime', title: '操作时间',  align: 'center'}
            ]],
            onDblClickRow: function (rowIndex,rowData) {
                //$("#tbForAddDialog").hide();//隐藏div
                //disableOcx();
                $('#info_formForAdd').form('clear');
                $('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','个人客户基础信息详情');
                $('#info_formForAdd').form('load',rowData);
            }
        });
        $("#formForAdd").append(tablelazyload);
        $.parser.parse($("#enterTable").parent());
        $(".combo").click(function (e) {
            if (e.target.className == 'combo-text validatebox-text' || e.target.className == 'combo-text validatebox-text validatebox-invalid'){
                if ($(this).prev().combobox("panel").is(":visible")) {
                    $(this).prev().combobox("hidePanel");
                } else {
                    $(this).prev().combobox("showPanel");
                }
            }
        });
    })

    function disableOcx() {
        var form = document.forms[0];
        for ( var i = 0; i < form.length; i++) {
            var element = form.elements[i];
            element.disabled = "true";
        }
    }

    function noDisable() {
        var form = document.forms[0];
        for ( var i = 0; i < form.length; i++) {
            var element = form.elements[i];
            element.disabled = false;
        }
    }

    //动态查询
    function searchOnSelected(value) {
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var customercodeParam = $("#customercodeParam").val().trim();
        var regamtcrenyParam = $("#regamtcrenyParam").combobox("getValue");
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = '';
        if (value == 'dsh'){
            operationnameParam = $("#operationnameParam").combobox("getValue");
        }
        $("#dg1").datagrid("load", {"finorgcodeParam" : finorgcodeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam,"customercodeParam":customercodeParam,"regamtcrenyParam":regamtcrenyParam});
    }

    // 新增实际控制人证件类型
    function addactctrlidtype(){
        var actctrlidtype = $('#actctrlidtype').val();
        $('#areaactctrlidtype').val(actctrlidtype);
        $('#dialogactctrlidtype').dialog('open').dialog('center').dialog('setTitle','实际控制人证件类型');
    }

    //新增按钮实际控制人证件类型
    function addactctrlidtypeCode(code){
        var areaactctrlidtype = $('#areaactctrlidtype').val();
        if (areaactctrlidtype.length < 60){
            if (areaactctrlidtype == null || areaactctrlidtype == ''){
                $('#areaactctrlidtype').val(code);
            }else {
                $('#areaactctrlidtype').val(areaactctrlidtype + "," + code);
            }
        }else {
            $.messager.alert('提示', '实际控制人证件类型字符长度不能超过60', 'info');
        }
    }

    //删除按钮实际控制人证件类型
    function deleteactctrlidtypeCode(code){
        var areaactctrlidtype = $('#areaactctrlidtype').val();
        if (areaactctrlidtype != null && areaactctrlidtype != ''){
            var areaactctrlidtypes = areaactctrlidtype.split(",")
            for (var i = areaactctrlidtypes.length-1;i>=0;i--){
                if (areaactctrlidtypes[i] == code){
                    areaactctrlidtypes.splice(i,1);
                    break;
                }
            }
            var areaactctrlidtypeafter = "";
            for (var i = 0;i<areaactctrlidtypes.length;i++){
                areaactctrlidtypeafter = areaactctrlidtypeafter + areaactctrlidtypes[i] + ",";
            }
            $('#areaactctrlidtype').val(areaactctrlidtypeafter.substring(0,areaactctrlidtypeafter.length-1));
        }
    }

    // 保存实际控制人证件类型
    function saveactctrlidtype(){
        var areaactctrlidtype = $('#areaactctrlidtype').val();
        $('#actctrlidtype').val(areaactctrlidtype);
        $('#dialogactctrlidtype').dialog('close');
    }

    // 取消实际控制人证件类型
    function cancelactctrlidtype(){
        $('#dialogactctrlidtype').dialog('close');
    }

    // 新增
    function add(){
        resetForAdd();
        $('#dialog1').dialog('open').dialog('center').dialog('setTitle','新增个人客户基础信息');
    }

    // 修改
    function edit(){
        var rows = $('#dg1').datagrid('getSelections');
        if(rows.length == 1){
            resetForAdd();
            $("#formForAdd").form("load", rows[0]);
            $('#dialog1').dialog('open').dialog('center').dialog('setTitle','修改个人客户基础信息');
        }else{
            $.messager.alert("提示", "请选择一条数据进行修改!", "info");
        }
    }

    // 保存
    function save() {
        if($("#formForAdd").form('validate')){
            $.ajax({
                type: "POST",//为post请求
                url: "XssaveXgrkhxx",//这是我在后台接受数据的文件名
                data: $('#formForAdd').serialize(),//将该表单序列化
                dataType: "json",
                async: false,
                success: function (res) {
                    if (res.status == "1") {
                        $.messager.alert('提示', '提交成功!', 'info');
                        $("#dialog1").dialog("close");
                        $("#dg1").datagrid("reload");
                    } else {
                        $.messager.alert('提示', '提交失败!', 'info');
                    }
                },
                error: function (err) {//请求失败之后的操作

                }
            });
        }
    }

    // 重置
    function resetForAdd(){
        //$("#tbForAddDialog").show();//显示div
        //noDisable();
        $('#formForAdd').form('clear');
    }

    // 取消
    function cancel(){
        $('#dialog1').dialog('close');
    }

    // 删除
    function deleteit(){
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        if(rows.length > 0){
            info = '是否删除所选中的数据？';
        }else {
            info = '您没有选择数据，是否删除所有数据？';
        }
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var customernameParam = $("#customernameParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XDeleteXftykhx",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"customernameParam" : customernameParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','删除成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','删除失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','删除失败','error');
                    }
                });
                $("#dg1").datagrid("reload");
            }
        });
    }

    // 导入Excel相关
    function importExcel(){
        document.getElementById("importbtn").disabled = "disabled";
        $('#excelfile').val('');
        $('#importExcel').dialog('open').dialog('center');
    }

    function inexcel(){
        var file = document.getElementById("excelfile").files[0];
        if(file == null){
            document.getElementById("importbtn").disabled = "disabled";
        }else{
            var fileName = file.name;
            var fileType = fileName.substring(fileName.lastIndexOf('.'),
                fileName.length);
            if (fileType == '.xls' || fileType == '.xlsx'){
                if (file) {
                    document.getElementById("importbtn").disabled = "";
                }
            } else {
                $.messager.alert('提示', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！","error");
                document.getElementById("importbtn").disabled = "disabled";
            }
        }
    }

    function excel(){
        $.messager.progress({
            title: '请稍等',
            msg: '数据正在导入中......'
        });
        $.ajaxFileUpload({
            type: "post",
            url: 'XimmportExcelXgrkhxx',
            fileElementId: 'excelfile',
            secureuri: false,
            dataType: 'json',
            success: function (data) {
                const that = data.msg;
                $.messager.progress('close');
                if (that == "导入成功") {
                    $.messager.alert('提示', "导入成功", 'info');
                    $("#dg1").datagrid('reload');
                    $('#importExcel').dialog('close');
                } else if (that.startsWith("导入模板不正确")) {
                    $.messager.alert('提示', that, 'error');
                } else {
                    $.messager.show({
                        title: '导入反馈',
                        msg: "<div style='overflow-y:scroll;height:100%'>" + escape2Html(that) + "</div>",
                        timeout: 0,
                        showType: 'show',
                        width: 600,
                        height: 700,
                        style: {
                            right: '100',
                            top: document.body.scrollTop + document.documentElement.scrollTop
                        }
                    });
                }
            },
            error: function () {
                $.messager.progress('close');
                $.messager.alert('提示', '导入文件错误，请重新导入', 'error');
            }
        });
    }

    // 导出
    function showOut(model){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        var info = '';
        if (model){
            id = '00000000';
            info = '是否导出模板？';
        }else {
            for( var dataIndex in row){
                id = id + row[dataIndex].id + ",";
            }
            if(id != ''){
                info = '是否将选中的数据导出到Excel表中？';
            }else {
                info = '是否将全部数据导出到Excel表中？'
            }
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('操作提示', info, function (r) {
            if (r) {
                window.location.href = "XEExportXgrkhxx?finorgcodeParam="+finorgcodeParam+"&checkstatusParam="+checkstatusParam+"&id="+id + "&datastatus="+'${datastatus}';
            }
        });
    }

    // 校验
    function check(){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        for( var dataIndex in row){
            id = id + row[dataIndex].id + ",";
            if (row[dataIndex].checkstatus != 0){
                $.messager.alert("提示", "只能校验未校验的数据", "info");
                return;
            }
        }
        var info = '';
        if(id != ''){
            info = '是否校验选中的数据？';
        }else {
            info = '是否校验所有数据？'
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r) {
            if (r) {
                $.messager.progress({
                    title: '请稍等',
                    msg: '正在校验数据中......'
                });
                $.ajax({
                    url: "XCheckDataXgrkhxx",
                    dataType: "json",
                    async: true,
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"checkstatusParam" : checkstatusParam},
                    type: "POST",
                    success: function (data) {
                    	$.messager.progress('close');
                        $("#dg1").datagrid("reload");
                        if (data.msg == "1") {
                            $.messager.alert('提示', '校验成功！', 'info');
                        }else if (data.msg == "-1") {
                            $.messager.alert('提示', '没有可校验的数据！', 'info');
                        } else {
                            $.messager.alert('提示', '校验完成,将自动下载校验结果', 'info', function (r) {
                                window.location.href = "XDownLoadCheckXgrkhxx";
                            });
                        }
                    },
                    error: function () {
                        $.messager.progress('close');
                        $.messager.alert("提示", "校验出错，请重新校验！", "error");
                    }
                });               
            }
        })
    }

    // 提交
    function submitit(){
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            for(var i = 0;i < rows.length;i++){
                if(rows[i].checkstatus != '1'){
                    $.messager.alert("提示", "只能提交校验成功的数据", "info");
                    return;
                }
                id = id + rows[i].id + ',';
            }
            info = '是否提交所选中的数据？';
        }else {
            info = '您没有选择数据，是否提交所有数据？';
        }
        $.messager.confirm('提示',info,function (r){
            if(r){
                var finorgcodeParam = $("#finorgcodeParam").val().trim();
                var checkstatusParam = $("#checkstatusParam").combobox("getValue");
                $.messager.confirm('提示',info,function (r){
                    if(r){
                    	$.messager.progress({
                            title: '请稍等',
                            msg: '正在提交数据中......'
                        });
                        $.ajax({
                            sync: true,
                            type: "POST",
                            dataType: "json",
                            url: "XsubmitXgrkhxx",
                            data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                            success: function (data) {
                            	$.messager.progress('close');
                                if (data == 0){
                                    $.messager.alert('操作提示','提交成功','info');
                                    $("#dg1").datagrid("reload");
                                }else {
                                    $.messager.alert('操作提示','提交失败','info');
                                }
                            },
                            error: function (err) {
                                $.messager.alert('操作提示','提交失败','error');
                            }
                        });
                        $("#dg1").datagrid("reload");
                    }
                });
            }
        });
    }

    //申请删除dsh
    function applyDelete() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            info = '是否申请删除所选中的数据？';
        }else {
            info = '是否申请删除所有数据？';
        }
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
            	$.messager.progress({
                    title: '请稍等',
                    msg: '正在申请删除数据中......'
                });
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XApplyDeleteXgrkhxx",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                    	$.messager.progress('close');
                        if (data == 0){
                            $.messager.alert('操作提示','申请删除成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','申请删除失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','申请删除失败','error');
                    }
                });
            }
        });
    }

    //申请修改
    function applyEdit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            info = '是否申请修改所选中的数据？';
        }else {
            info = '是否申请修改所有数据？';
        }
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
            	$.messager.progress({
                    title: '请稍等',
                    msg: '正在申请修改数据中......'
                });
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XApplyEditXgrkhxx",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                    	$.messager.progress('close');
                        if (data == 0){
                            $.messager.alert('操作提示','申请修改成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','申请修改失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','申请修改失败','error');
                    }
                });
            }
        });
    }

    /*同意申请*/
    function agreeApply() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
                return;
            }
            if (rows[i].operationname != '申请删除'){
                $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否同意所选中的申请？';
        }else {
            info = '是否同意所有的申请？';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
            	$.messager.progress({
                    title: '请稍等',
                    msg: '正在同意申请数据中......'
                });
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XAgreeApplyXgrkhxx",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                    	$.messager.progress('close');
                        if (data == 0){
                            $.messager.alert('操作提示','同意成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','同意失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','同意失败','error');
                    }
                });
            }
        });
    }

    /*拒绝申请*/
    function noAgreeApply() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
                return;
            }
            if (rows[i].operationname != '申请删除'){
                $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否拒绝所选中的申请？';
        }else {
            info = '是否拒绝所有的申请？';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
            	$.messager.progress({
                    title: '请稍等',
                    msg: '正在拒绝申请数据中......'
                });
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XNoAgreeApplyXgrkhxx",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                    	$.messager.progress('close');
                        if (data == 0){
                            $.messager.alert('操作提示','拒绝成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','拒绝失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','拒绝意失败','error');
                    }
                });
            }
        });
    }

    /*审核通过*/
    function agreeAudit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
                return;
            }
            if (rows[i].checkstatus != 1){
                $.messager.alert("提示","请选择校验通过的数据！","error");
                return;
            }
            if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
            }else {
                $.messager.alert("提示","请选择操作名为空的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否审核通过所选中的数据？';
        }else {
            info = '是否审核通过所有数据？';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
            	$.messager.progress({
                    title: '请稍等',
                    msg: '正在审核通过数据中......'
                });
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XAgreeAuditXgrkhxx",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                    	$.messager.progress('close');
                        if (data == 0){
                            $.messager.alert('操作提示','审核通过成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','审核通过失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','审核通过失败','error');
                    }
                });
            }
        });
    }

    /*审核不通过*/
    function noAgreeAudit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
                return;
            }
            if (rows[i].checkstatus != 1){
                $.messager.alert("提示","请选择校验通过的数据！","error");
                return;
            }
            if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
            }else {
                $.messager.alert("提示","请选择操作名为空的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否审核不通过所选中的数据？';
        }else {
            info = '是否审核不通过所有数据？';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.prompt('提示',info,function (r){
            if(r){
            	$.messager.progress({
                    title: '请稍等',
                    msg: '正在审核不通过数据中......'
                });
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XNoAgreeAuditXgrkhxx",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam,"reason":r},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                    	$.messager.progress('close');
                        if (data == 0){
                            $.messager.alert('操作提示','审核不通过成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','审核不通过失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','审核不通过失败','error');
                    }
                });
            }else {
                $.messager.alert('操作提示','审核不通过原因必填','info');
            }
        });
    }
    
  	//返回
    function fanhui(param){
    	$.messager.progress({
            title: '请稍等',
            msg: '数据正在加载中......'
        });
    }

    //前台校验
    $.extend($.fn.validatebox.defaults.rules, {
    	dateFormat : {
    		validator : function(value){
    			var patrn = /^\d{4}-\d{2}-\d{2}$/;
                return patrn.test(value);
    		},
    		message : '日期格式为：YYYY-MM-DD'
    	},
        normalDate : {
            validator : function(value) {
                var startTmp = new Date('1800-01-01');
                var endTmp = new Date('2100-12-31');
                var nowTmp = new Date(value);
                return nowTmp > startTmp && nowTmp < endTmp;
            },
            message : '日期早于1800-01-01且晚于2100-12-31'
        },
        compareToEnd : {
            validator : function(value) {
                var gteeenddate = $('#gteeenddate').combobox('getValue');
                if (gteeenddate == null || gteeenddate == ''){
                    return true;
                }
                var startTmp = new Date(gteeenddate);
                var nowTmp = new Date(value);
                return nowTmp <= startTmp;
            },
            message : '担保合同签订日期应小于等于担保合同到期日期'
        },
        teshu:{
            validator: function (value) {
                var patrn = /[？?！!^]/;
                return !patrn.test(value);
            },
            message: '不能包含特殊符'
        },
        teshua:{
            validator: function (value) {
            	if(value != null){
            		var patrn = /[？?！!^]/;
                    return !patrn.test(value);
            	}                
            },
            message: '不能包含特殊符号'
        },
        teshub:{
            validator: function (value) {
                var patrn = /[？?！!^]/;
                return !patrn.test(value);
            },
            message: '不能包含特殊符号'
        },
        teshuc:{
            validator: function (value) {
                var patrn = /[？?！!^]/;
                return !patrn.test(value);
            },
            message: '不能包含特殊符号'
        },
        amount : {
            validator : function(value) {
            	if(value != null){
            		var reg = /^([0-9]\d{0,18}(\.\d{2})|0\.\d{2})$/;
                    return reg.test(value);
            	}
                
            },
            message : '总长度不超过20位的，精度保留小数点后两位。'
        },
        personnum : {
            validator : function(value) {
                var reg = /^([1-9]\d*|[0]{1,1})$/;
                return reg.test(value);
            },
            message : '从业人员数应为正整数'
        }
    });
</script>
</body>
</html>
