<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ page import="com.geping.etl.common.util.VariableUtils" %>
<%@ page import="com.geping.etl.common.entity.Sys_Auth_Role_Resource" %>
<%@ page import="com.geping.etl.common.entity.Sys_UserAndOrgDepartment" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    VariableUtils vu = (VariableUtils)request.getSession().getAttribute("vu");
    Set<Sys_Auth_Role_Resource> operateSet = vu.getOperateReportSet();
    String status = request.getParameter("status");
    String shifoushenheziji = (String) session.getAttribute("shifoushenheziji");
    String role = vu.getRole();
    Sys_UserAndOrgDepartment sys_User = ((Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User"));
    String handlename = sys_User.getLoginid();
    String isusedelete = (String) request.getSession().getAttribute("isusedelete");
    String isuseupdate = (String) request.getSession().getAttribute("isuseupdate");
    List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
    List<String> baseCurrencys = new ArrayList<>();
    for (BaseCurrency baseCurrency : baseCurrencyList) {
        baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
    }
    List<BaseAindustry> baseAindustryList = (List<BaseAindustry>) request.getAttribute("baseAindustryList");
    List<String> baseAindustrys = new ArrayList<>();
    for (BaseAindustry baseAindustry : baseAindustryList) {
        baseAindustrys.add("'"+baseAindustry.getAindustrycode()+ "-" +baseAindustry.getAindustryname()+"'");
    }
    List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
    List<BaseCountry> baseCountryList = (List<BaseCountry>) request.getAttribute("baseCountryList");
    List<String> baseAreas = new ArrayList<>();
    for (BaseArea baseArea : baseAreaList) {
        baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
    }
    for (BaseCountry baseCountry : baseCountryList) {
        baseAreas.add("'"+baseCountry.getCountrycode()+ "-" +baseCountry.getCountryname()+"'");
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">
    <%@include file="../../../common/head.jsp" %>
    <title>单位贷款担保合同信息提交</title>
</head>
<body>
<table id="dg1" style="height: 480px;" title="单位贷款担保合同信息列表"></table>

<!--表格工具栏-->
<div id="tb1" style="padding: 5px; height: auto;">
    <%if("dtj".equals(status) || "dsh".equals(status)){%>
    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return" onclick="fanhui()">返回</a>&nbsp;&nbsp;
    <%}else if ("dsb".equals(status)){%>
    <a class="easyui-linkbutton" href="VGenerateMessageUi" iconCls="icon-return" onclick="fanhui()">返回</a>&nbsp;&nbsp;
    <%}%>&nbsp;&nbsp;
    <label>担保合同编码：</label><input id="gteecontractcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>被担保合同编码：</label><input id="loancontractcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>校验类型：</label>
    <select class='easyui-combobox' id='checkstatusParam' name='checkstatusParam' editable=false style='height: 23px; width:80px' >
        <option value=''>=请选择=</option>
        <option value='0'>未校验</option>
        <option value='1'>校验成功</option>
        <option value='2'>校验失败</option>
    </select>&nbsp;&nbsp;
    <%if("dtj".equals(status)){%>
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected('dtj')">查询</a>&nbsp;&nbsp;<br>
    <%}else if ("dsh".equals(status)){%>
    <label>操作名：</label>
    <select class='easyui-combobox' id='operationnameParam' name='operationnameParam' editable=false style='height: 23px; width:80px' >
        <option value=''>=请选择=</option>
        <option value='申请删除'>申请删除</option>
    </select>&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected('dsh')">查询</a>&nbsp;&nbsp;<br>
    <%}%>
    <%if("dtj".equals(status)){
        for(Sys_Auth_Role_Resource sarr : operateSet){
            if(sarr.getResValue().equals("ADD")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-add" onclick="add()">新增</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("EDIT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-edit" onclick="edit()">修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("APPLYDELETE")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-remove" onclick="applyDelete()">申请删除</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("IMPORT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="importExcel()">导入</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("EXPORT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(false)">导出</a>&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(true)">导出模板</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("CHECKOUT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-accept" onclick="check()">校验</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("SUBMIT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-easyicon-net-16" onclick="submitit()">提交</a>
    <%
            }
        }
    }else if("dsh".equals(status)){
        for(Sys_Auth_Role_Resource sarr : operateSet){
            if(sarr.getResValue().equals("EDIT") && "yes".equals(isuseupdate)){
    %>
    <%--<a class="easyui-linkbutton" iconCls="icon-edit" onclick="applyEdit()" >申请修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
    <%
    }else if(sarr.getResValue().equals("DELETE") && "yes".equals(isusedelete)){
    %>
    <%--<a class="easyui-linkbutton" data-options="iconCls:'icon-remove'" onclick="applyDelete()" >申请删除</a>&nbsp;--%>
    <%
    }else if(sarr.getResValue().equals("CHECK")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-ok" onclick="agreeApply()" >同意申请删除</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-cross" onclick="noAgreeApply()" >拒绝申请删除</a>&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-application-get" onclick="agreeAudit()" >审核通过</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-cross" onclick="noAgreeAudit()" >审核不通过</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(false)">导出</a>&nbsp;&nbsp;&nbsp;
    <%
                }
            }
        }%>
</div>


<!-- 导入文件dialog -->
<div style="visibility: hidden;">
    <div id="importExcel" class="easyui-dialog" style="width: 600px; height: 150px;top: 100px;padding: 20px;" title="数据导入" data-options="modal:true,closed:true">
        文件导入:<input type="file" accept=".xls,.xlsx" name="excelfile" id="excelfile" onchange="inexcel()"/><br>
        <input type="button" id="importbtn" value="提交" style="width:50px;margin-top:20px;" onclick="excel()"/>
    </div>
</div>

<div id="dialog1" class="easyui-dialog" style="width:840px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
        <div id="tbForAddDialog" style="padding-left: 30px;padding-top: 10px">
            <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="save()">确定</a>&nbsp;&nbsp;
            <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="cancel()">取消</a>
        </div>

        <form id="formForAdd" method="post">
            <input id="id" class="backId" name="id" type="hidden">
            <div style="padding-top: 20px">
                <table style="padding-left: 30px">
                    <tr>
                        <td>担保合同编码:</td>
                        <td><input class="easyui-validatebox" type="text" id="gteecontractcode" name="gteecontractcode" maxlength="100" data-options="required:true,validType:['teshua']" style="height: 23px; width:173px" /></td>
                        <td>被担保合同编码:</td>
                        <td><input class="easyui-validatebox" type="text" id="loancontractcode" name="loancontractcode" maxlength="100" data-options="required:true,validType:['teshua']" style="height: 23px; width:173px"/></td>
                    </tr>

                    <tr>
                        <td>担保合同类型:</td>
                        <td>
                            <select class="easyui-combobox" id="gteecontracttype" name="gteecontracttype" required="true" editable=false style="height: 23px; width:173px" >
                                <option value="01">01-一般担保合同</option>
                                <option value="02">02-最高额担保合同</option>
                            </select>
                        </td>
                        <td>担保合同起始日期:</td>
                        <td><input class="easyui-datebox" required="true" data-options="validType:['dateFormat','normalDate','compareToEnd']" id="gteestartdate" name="gteestartdate" style="height: 23px; width:173px"/></td>
                    </tr>

                    <tr>
                        <td>担保合同到期日期:</td>
                        <td><input class="easyui-datebox" required="true" id="gteeenddate" name="gteeenddate" data-options="validType:['dateFormat','normalDate']" style="height: 23px; width:173px"/></td>
                        <td>币种:</td>
                        <td><input id='gteecurrency' name='gteecurrency' style='width:173px;' readonly='true' class='easyui-validatebox' required="true"><a class='easyui-linkbutton' iconCls='icon-add' onclick='addgteecurrency()'></a>
                            <%--<select class="easyui-combobox" required="true" id="gteecurrency" name="gteecurrency" editable=false data-options="required:true,valueField:'id', textField:'text'" style="height: 23px; width:173px" >
                                <c:forEach items="${baseCurrencyList}" var="aa">
                                    <option value="${aa.currencycode}">${aa.currencycode}-${aa.currencyname}</option>
                                </c:forEach>
                            </select>--%>
                        </td>
                    </tr>

                    <tr>
                        <td>担保合同金额:</td>
                        <td><input class="easyui-validatebox" type="text" id="gteeamount" name="gteeamount" data-options="required:true,validType:['maoreamount']" style="height: 23px; width:173px"/></td>
                        <td>担保合同金额折人民币:</td>
                        <td><input class="easyui-validatebox" type="text" id="gteecnyamount" name="gteecnyamount" data-options="required:true,validType:['amount']" style="height: 23px; width:173px"/></td>
                    </tr>

                    <tr>
                        <td>担保人证件类型:</td>
                        <td><input id='gteeidtype' name='gteeidtype' style='width:173px;' readonly='true' class='easyui-validatebox'/><a class='easyui-linkbutton' iconCls='icon-add' onclick='addgteeidtype()'></a>
                            <%--<select class="easyui-combobox" id="gteeidtype" name="gteeidtype" required="true" editable=false style="height: 23px; width:173px" >
                                <option value="A01">A01-统一社会信用代码</option>
                                <option value="A02">A02-组织机构代码</option>
                                <option value="A03">A03-其他</option>
                                <option value="B01">B01-身份证</option>
                                <option value="B02">B02-户口簿</option>
                                <option value="B03">B03-护照</option>
                                <option value="B04">B04-军官证</option>
                                <option value="B05">B05-士兵证</option>
                                <option value="B06">B06-港澳居民来往内地通行证</option>
                                <option value="B07">B07-台湾同胞来往内地通行证</option>
                                <option value="B08">B08-临时身份证</option>
                                <option value="B09">B09-外国人居留证</option>
                                <option value="B10">B10-警官证</option>
                                <option value="B11">B11-个体工商户营业执照</option>
                                <option value="B12">B12-外国人永久拘留省份证</option>
                                <option value="B13">B13-港澳台居民居住证</option>
                                <option value="B99">B99-其他证件</option>
                            </select>--%>
                        </td><td>担保人证件代码:</td>
                        <td><input class="easyui-validatebox" maxlength='1000' data-options="validType:['teshua']" type="text" id="gteeidnum" name="gteeidnum" style="height: 23px; width:173px"/></td>
                    </tr>

                    <tr>
                        <td>金融机构代码:</td>
                        <td><input class="easyui-validatebox" type="text" id="financeorgcode" name="financeorgcode" style="height: 23px; width:173px" data-options="required:true,validType:['teshu']"/></td>
                        <td>内部机构号:</td>
                        <td><input class="easyui-validatebox" type="text" id="financeorginnum" name="financeorginnum" style="height: 23px; width:173px" data-options="required:true,validType:['teshua']"/></td>
                    </tr>

                    <tr>
                        <td>交易类型:</td>
                        <td><input id='transactiontype' name='transactiontype' style='width:173px;' readonly='true' class='easyui-validatebox' required="true"/><a class='easyui-linkbutton' iconCls='icon-add' onclick='addtransactiontype()'></a>
                            <%--<select class="easyui-combobox" id="transactiontype" name="transactiontype" editable=false style="height: 23px; width:173px" >
                            <option value="01">01-回购类交易</option>
                            <option value="02">02-信贷交易</option>
                            <option value="03">03-其他交易</option>
                        </select>--%></td>
                        <td>抵质押率:</td>
                        <td><input class="easyui-validatebox" type="text" id="pledgerate" name="pledgerate" style="height: 23px; width:173px"  data-options="validType:['pledgerate']"/></td>
                    </tr>

                    <tr>
                        <td>担保人国民经济部门:</td>
                        <td><select class="easyui-combobox" id="isgreenloan" name="isgreenloan" editable=false style="height: 23px; width:173px" >
                            <option value='A'>A-广义政府</option>
                            <option value='A01'>A01-中央政府</option>
                            <option value='A02'>A02-地方政府</option>
                            <option value='A03'>A03-社会保障基金</option>
                            <option value='A04'>A04-机关团体</option>
                            <option value='A05'>A05-部队</option>
                            <option value='A06'>A06-住房公积金</option>
                            <option value='A99'>A99-其他</option>
                            <option value='B'>B-金融机构部门</option>
                            <option value='B01'>B01-货币当局</option>
                            <option value='B02'>B02-监管当局</option>
                            <option value='B03'>B03-银行业存款类金融机构</option>
                            <option value='B04'>B04-银行业非存款类金融机构</option>
                            <option value='B05'>B05-证券业金融机构</option>
                            <option value='B06'>B06-保险业金融机构</option>
                            <option value='B07'>B07-交易及结算类金融机构</option>
                            <option value='B08'>B08-金融控股公司</option>
                            <option value='B09'>B09-特定目的载体</option>
                            <option value='B99'>B99-其他</option>
                            <option value='C'>C-非金融企业部门</option>
                            <option value='C01'>C01-公司</option>
                            <option value='C02'>C02-非公司企业</option>
                            <option value='C99'>C99-其他非金融企业部门</option>
                            <option value='D'>D-住户部门</option>
                            <option value='D01'>D01-住户</option>
                            <option value='D02'>D02-为住户服务的非营力机构</option>
                            <option value='E'>E-非居民部门</option>
                            <option value='E01'>E01-国际组织</option>
                            <option value='E02'>E02-外国政府</option>
                            <option value='E03'>E03-境外金融机构</option>
                            <option value='E04'>E04-境外非金融企业</option>
                            <option value='E05'>E05-外国居民</option>
                        </select></td>
                        <td>担保人行业:</td>
                        <td><select class="easyui-combobox" id="brrowerindustry" name="brrowerindustry" editable=false style="height: 23px; width:173px" >
                            <c:forEach items='${baseAindustryList}' var='aa'>
                                <option value='${aa.aindustrycode}'>${aa.aindustrycode}-${aa.aindustryname}</option>
                            </c:forEach>
                        </select></td>
                    </tr>

                    <tr>
                    <!-- 
                        <td>担保人地区代码:</td>
                        <td><select class="easyui-combobox" id="brrowerareacode" name="brrowerareacode" required="true" style="height: 23px; width:173px" >
                            <c:forEach items='${baseAreaList}' var='aa'>
                                <option value='${aa.areacode}'>${aa.areacode}-${aa.areaname}</option>
                            </c:forEach>
                            <c:forEach items='${baseCountryList}' var='aa'>
                                <option value='${aa.countrycode}'>${aa.countrycode}-${aa.countryname}</option>
                            </c:forEach>
                        </select></td>
                         -->
                         <td>担保人地区代码:</td>
                        <td><input class="easyui-combotree" id="brrowerareacode" name="brrowerareacode" style="height: 23px; width:173px" data-options="url:'XgetAdmindivideJson',valueField:'id',textField:'text',editable:false,value:''" >
                            </td>
                        <td>担保人企业规模:</td>
                        <td><select class="easyui-combobox" id="enterprisescale" name="enterprisescale" style="height: 23px; width:173px" >
                            <option value='CS01'>CS01-大型</option>
                            <option value='CS02'>CS02-中型</option>
                            <option value='CS03'>CS03-小型</option>
                            <option value='CS04'>CS04-微型</option>
                            <option value='CS05'>CS05-其他（非企业类单位）</option>
                        </select></td>
                    </tr>
                    <tr>
                    <td>数据日期:</td>
                    <td><input class="easyui-datebox" required="true" id="sjrq" name="sjrq" data-options="validType:['dateFormat','normalDate']" style="height: 23px; width:173px"/></td>
                    </tr>
                </table>
            </div>
        </form>
    </div>

<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">

    <form id="info_formForAdd" method="post">
        <input id="info_id" class="backId" name="id" type="hidden">
        <div style="padding-top: 20px">
            <table style="padding-left: 30px">
                <tr>
                    <td>担保合同编码:</td>
                    <td><input type="text" id="info_gteecontractcode" name="gteecontractcode" disabled="disabled" style="height: 23px; width:173px" /></td>
                    <td>被担保合同编码:</td>
                    <td><input type="text" id="info_loancontractcode" name="loancontractcode" disabled="disabled" style="height: 23px; width:173px"/></td>
                </tr>

                <tr>
                    <td>担保合同类型:</td>
                    <td>
                        <select id="info_gteecontracttype" name="gteecontracttype" disabled="disabled" style="height: 23px; width:173px" >
                            <option value="01">01-一般担保合同</option>
                            <option value="02">02-最高额担保合同</option>
                        </select>
                    </td>
                    <td>担保合同起始日期:</td>
                    <td><input disabled="disabled" id="info_gteestartdate" name="gteestartdate" style="height: 23px; width:173px"/></td>
                </tr>

                <tr>
                    <td>担保合同到期日期:</td>
                    <td><input disabled="disabled" id="info_gteeenddate" name="gteeenddate" style="height: 23px; width:173px"/></td>
                    <td>币种:</td>
                    <td><input disabled="disabled" type="text" id="info_gteecurrency" name="gteecurrency" style="height: 23px; width:173px"/>
                        <%--<select required="true" id="info_gteecurrency" name="gteecurrency" disabled="disabled" style="height: 23px; width:173px" >
                            <c:forEach items="${baseCurrencyList}" var="aa">
                                <option value="${aa.currencycode}">${aa.currencycode}-${aa.currencyname}</option>
                            </c:forEach>
                        </select>--%>
                    </td>
                </tr>

                <tr>
                    <td>担保合同金额:</td>
                    <td><input type="text" id="info_gteeamount" name="gteeamount" disabled="disabled" style="height: 23px; width:173px"/></td>
                    <td>担保合同金额折人民币:</td>
                    <td><input type="text" id="info_gteecnyamount" name="gteecnyamount" disabled="disabled" style="height: 23px; width:173px"/></td>
                </tr>

                <tr>
                    <td>担保人证件类型:</td>
                    <td><input disabled="disabled" type="text" id="info_gteeidtype" name="gteeidtype" style="height: 23px; width:173px"/>
                        <%--<select id="info_gteeidtype" name="gteeidtype" disabled="disabled" style="height: 23px; width:173px" >
                            <option value="A01">A01-统一社会信用代码</option>
                            <option value="A02">A02-组织机构代码</option>
                            <option value="A03">A03-其他</option>
                            <option value="B01">B01-身份证</option>
                            <option value="B02">B02-户口簿</option>
                            <option value="B03">B03-护照</option>
                            <option value="B04">B04-军官证</option>
                            <option value="B05">B05-士兵证</option>
                            <option value="B06">B06-港澳居民来往内地通行证</option>
                            <option value="B07">B07-台湾同胞来往内地通行证</option>
                            <option value="B08">B08-临时身份证</option>
                            <option value="B09">B09-外国人居留证</option>
                            <option value="B10">B10-警官证</option>
                            <option value="B11">B11-个体工商户营业执照</option>
                            <option value="B12">B12-外国人永久拘留省份证</option>
                            <option value="B13">B13-港澳台居民居住证</option>
                            <option value="B99">B99-其他证件</option>
                        </select>--%>
                    </td>
                    <td>担保人证件代码:</td>
                    <td><input disabled="disabled" type="text" id="info_gteeidnum" name="gteeidnum" style="height: 23px; width:173px"/></td>
                </tr>

                <tr>
                    <td>金融机构代码:</td>
                    <td><input disabled="disabled" type="text" id="info_financeorgcode" name="financeorgcode"  style="height: 23px; width:173px"/></td>
                    <td>内部机构号:</td>
                    <td><input disabled="disabled" type="text" id="info_financeorginnum" name="financeorginnum" style="height: 23px; width:173px"/></td>
                </tr>

                <tr>
                    <td>交易类型:</td>
                    <td><input disabled="disabled" type="text" id="info_transactiontype" name="transactiontype" style="height: 23px; width:173px"/>
                        <%--<select disabled="disabled" id="info_transactiontype" name="transactiontype" style="height: 23px; width:173px" >
                        <option value="01">01-回购类交易</option>
                        <option value="02">02-信贷交易</option>
                        <option value="03">03-其他交易</option>
                    </select>--%></td>
                    <td>抵质押率:</td>
                    <td><input disabled="disabled" type="text" id="info_pledgerate" name="pledgerate" style="height: 23px; width:173px"/></td>
                </tr>

                <tr>
                    <td>担保人国民经济部门:</td>
                    <td><select disabled="disabled" id="info_isgreenloan" name="isgreenloan" style="height: 23px; width:173px" >
                        <option value='A'>A-广义政府</option>
                        <option value='A01'>A01-中央政府</option>
                        <option value='A02'>A02-地方政府</option>
                        <option value='A03'>A03-社会保障基金</option>
                        <option value='A04'>A04-机关团体</option>
                        <option value='A05'>A05-部队</option>
                        <option value='A06'>A06-住房公积金</option>
                        <option value='A99'>A99-其他</option>
                        <option value='B'>B-金融机构部门</option>
                        <option value='B01'>B01-货币当局</option>
                        <option value='B02'>B02-监管当局</option>
                        <option value='B03'>B03-银行业存款类金融机构</option>
                        <option value='B04'>B04-银行业非存款类金融机构</option>
                        <option value='B05'>B05-证券业金融机构</option>
                        <option value='B06'>B06-保险业金融机构</option>
                        <option value='B07'>B07-交易及结算类金融机构</option>
                        <option value='B08'>B08-金融控股公司</option>
                        <option value='B09'>B09-特定目的载体</option>
                        <option value='B99'>B99-其他</option>
                        <option value='C'>C-非金融企业部门</option>
                        <option value='C01'>C01-公司</option>
                        <option value='C02'>C02-非公司企业</option>
                        <option value='C99'>C99-其他非金融企业部门</option>
                        <option value='D'>D-住户部门</option>
                        <option value='D01'>D01-住户</option>
                        <option value='D02'>D02-为住户服务的非营力机构</option>
                        <option value='E'>E-非居民部门</option>
                        <option value='E01'>E01-国际组织</option>
                        <option value='E02'>E02-外国政府</option>
                        <option value='E03'>E03-境外金融机构</option>
                        <option value='E04'>E04-境外非金融企业</option>
                        <option value='E05'>E05-外国居民</option>
                    </select></td>
                    <td>担保人行业:</td>
                    <td><select disabled="disabled" id="info_brrowerindustry" name="brrowerindustry" style="height: 23px; width:173px" >
                        <c:forEach items='${baseAindustryList}' var='aa'>
                            <option value='${aa.aindustrycode}'>${aa.aindustrycode}-${aa.aindustryname}</option>
                        </c:forEach>
                    </select></td>
                </tr>

                <tr>
                    <td>担保人地区代码:</td>
                    <td>
                    <input disabled="disabled" id="info_brrowerareacode" name="brrowerareacode" style="height: 23px; width:173px" >
                    </td>
                    <td>担保人企业规模:</td>
                    <td><select disabled="disabled" id="info_enterprisescale" name="enterprisescale" style="height: 23px; width:173px" >
                        <option value='CS01'>CS01-大型</option>
                        <option value='CS02'>CS02-中型</option>
                        <option value='CS03'>CS03-小型</option>
                        <option value='CS04'>CS04-微型</option>
                        <option value='CS05'>CS05-其他（非企业类单位）</option>
                    </select></td>
                </tr>
                <tr>
                    <td>数据日期:</td>
                    <td><input disabled="disabled" id="info_sjrq" name="sjrq" style="height: 23px; width:173px"/></td>
                    </tr>
            </table>
        </div>
    </form>
</div>

<!--新增交易类型窗口-->
<div id="dialogtransactiontype" class="easyui-dialog" style="width:450px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div id="tbForAddDialogtransactiontype">
        <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="savetransactiontype()">确定</a>&nbsp;&nbsp;
        <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="canceltransactiontype()">取消</a>
    </div><br>
    <div align="center">
        <textarea id="areatransactiontype" style="width: 360px;height: 40px" disabled="disabled"></textarea>
    </div><br>
    <!-- 信息录入 -->
    <div align="center">
        <table style="width: 380px">
            <tr>
                <td align="left">01-回购类交易</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addtransactiontypeCode('01')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletetransactiontypeCode('01')"></a></td>
            </tr>
            <tr>
                <td align="left">02-信贷交易</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addtransactiontypeCode('02')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletetransactiontypeCode('02')"></a></td>
            </tr>
            <tr>
                <td align="left">03-其他交易</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addtransactiontypeCode('03')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletetransactiontypeCode('03')"></a></td>
            </tr>
        </table>
    </div>
</div>

<!--新增币种窗口-->
<div id="dialoggteecurrency" class="easyui-dialog" style="width:450px;height:200px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div id="tbForAddDialoggteecurrency">
        <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="savegteecurrency()">确定</a>&nbsp;&nbsp;
        <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="cancelgteecurrency()">取消</a>
    </div><br>
    <div align="center">
        <textarea id="areagteecurrency" style="width: 360px;height: 40px" disabled="disabled"></textarea>
    </div><br>
    <!-- 信息录入 -->
    <div align="center">
        <table style="width: 380px">
            <tr>
                <td align="left">
                    <select editable=false id="gteecurrencyCode" style="height: 23px; width:173px" >
                        <c:forEach items="${baseCurrencyList}" var="aa">
                            <option value="${aa.currencycode}">${aa.currencycode}-${aa.currencyname}</option>
                        </c:forEach>
                    </select>
                </td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteecurrencyCode()"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteecurrencyCode()"></a></td>
            </tr>
        </table>
    </div>
</div>

<!--新增担保人证件类型窗口-->
<div id="dialoggteeidtype" class="easyui-dialog" style="width:450px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div id="tbForAddDialoggteeidtype">
        <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="savegteeidtype()">确定</a>&nbsp;&nbsp;
        <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="cancelgteeidtype()">取消</a>
    </div><br>
    <div align="center">
        <textarea id="areagteeidtype" style="width: 360px;height: 40px" disabled="disabled"></textarea>
    </div><br>
    <!-- 信息录入 -->
    <div style="position:absolute; height:240px; margin-left: 45px; overflow:auto"; align="center">
        <table align="center">
            <tr>
                <td align="left">A01-统一社会信用代码</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('A01')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('A01')"></a></td>
            </tr>
            <tr>
                <td align="left">A02-组织机构代码</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('A02')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('A02')"></a></td>
            </tr>
            <tr>
                <td align="left">A03-资管产品统计编码</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('A03')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('A03')"></a></td>
            </tr>
            <tr>
                <td align="left">A04-资管产品登记备案编码</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('A04')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('A04')"></a></td>
            </tr>
            <tr>
                <td align="left">A99-其他</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('A99')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('A99')"></a></td>
            </tr>
            <tr>
                <td align="left">B01-身份证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('B01')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('B01')"></a></td>
            </tr>
            <tr>
                <td align="left">B02-户口簿</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('B02')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('B02')"></a></td>
            </tr>
            <tr>
                <td align="left">B03-护照</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('B03')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('B03')"></a></td>
            </tr>
            <tr>
                <td align="left">B04-军官证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('B04')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('B04')"></a></td>
            </tr>
            <tr>
                <td align="left">B05-士兵证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('B05')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('B05')"></a></td>
            </tr>
            <tr>
                <td align="left">B06-港澳居民来往内地通行证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('B06')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('B06')"></a></td>
            </tr>
            <tr>
                <td align="left">B07-台湾同胞来往内地通行证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('B07')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('B07')"></a></td>
            </tr>
            <tr>
                <td align="left">B08-临时身份证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('B08')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('B08')"></a></td>
            </tr>
            <tr>
                <td align="left">B09-外国人居留证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('B09')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('B09')"></a></td>
            </tr>
            <tr>
                <td align="left">B10-警官证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('B10')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('B10')"></a></td>
            </tr>
            <tr>
                <td align="left">B11-外国人永久拘留省份证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('B11')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('B11')"></a></td>
            </tr>
            <tr>
                <td align="left">B12-港澳台居民居住证</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('B12')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('B12')"></a></td>
            </tr>
            <tr>
                <td align="left">B99-其他证件</td>
                <td align="right"><a class="easyui-linkbutton" iconCls="icon-add" onclick="addgteeidtypeCode('B99')"></a>&nbsp;<a class="easyui-linkbutton" iconCls="icon-remove" onclick="deletegteeidtypeCode('B99')"></a></td>
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript">
    var baseCurrencys = <%=baseCurrencys%>;
    var baseAindustrys = <%=baseAindustrys%>;
    var baseAreas = <%=baseAreas%>;
    var handlename = '<%= handlename%>';
    var role = '<%= role%>';
    var shifoushenheziji = '<%= shifoushenheziji%>';
    $(function () {
        $('#dialog1').dialog({
            onClose: function () {
                $('.validatebox-tip').remove();
            }
        });
        $('#dialog1').dialog('close');
        $("#importFileDialog").dialog("close");

        $("#dg1").datagrid({
            method: 'post',
            url:'XGetXdkdbhtData?datastatus='+'${datastatus}',
            loadMsg: '数据加载中,请稍后...',
            checkOnSelect:true,
            autoRowHeight:false,
            pagination:true,
            rownumbers:true,
            toolbar:'#tb1',
            fitColumns:false,
            pageSize: 20,
            pageList: [15, 20, 30, 50],
            columns: [[
                {field: 'ck', checkbox: true},
                {field: 'id', title: '编号', width: 80, align: 'center', hidden: true},
                {field: 'operationname', title: '操作名', width: 100, align: 'center'},
                {field: 'nopassreason', title: '审核不通过原因', width: 100, align: 'center'},
                {
                    field: 'checkstatus',
                    title: '校验结果',
                    width: 100,
                    align: 'center',
                    formatter: function (value, row, index) {
                        if (value == '0') {
                            return "未校验";
                        } else if (value == '1') {
                            return "校验成功";
                        } else if (value == '2') {
                            return '<span style="color:red;">' + '校验失败' + '</span>';
                        }
                        return value;
                    }
                },
                {field: 'gteecontractcode', title: '担保合同编码', width: 100, align: 'center'},
                {field: 'loancontractcode', title: '被担保合同编码', width: 100, align: 'center'},
                {field: 'gteecontracttype', title: '担保合同类型', width: 100, align: 'center',formatter:function (value) {
                        if (value=='01'){
                            return "01-一般担保合同";
                        }else if(value=='02'){
                            return "02-最高额担保合同";
                        }
                        return value;
                    }},
                {field: 'gteestartdate', title: '担保合同起始日期', width: 100, align: 'center'},
                {field: 'gteeenddate', title: '担保合同到期日期', width: 100, align: 'center'},
                {field: 'gteecurrency', title: '币种', width: 100, align: 'center',formatter:function (value) {
                        for (var i = 0;i <  baseCurrencys.length;i++){
                            if (baseCurrencys[i].substring(0,3)==value){
                                return baseCurrencys[i];
                            }
                        }
                    }},
                {field: 'gteeamount', title: '担保合同金额', width: 100, align: 'center'},
                {field: 'gteecnyamount', title: '担保合同金额折人民币', width: 100, align: 'center'},
                {field: 'gteeidtype', title: '担保人证件类型', width: 100, align: 'center',formatter:function (value) {
                        if (value=='A01'){
                            return "A01-统一社会信用代码";
                        }else if(value=='A02'){
                            return "A02-组织机构代码";
                        }else if (value=='A03'){
                            return "A03-其他";
                        }else if (value=='A03'){
                            return "A03-资管产品统计编码";
                        }else if (value=='A04'){
                            return "A04-资管产品登记备案编码";
                        }else if (value=='A99'){
                            return "A99-其他";
                        }else if(value=='B01'){
                            return "B01-身份证";
                        }else if (value=='B02'){
                            return "B02-户口簿";
                        }else if (value=='B03'){
                            return "B03-护照";
                        }else if(value=='B04'){
                            return "B04-军官证";
                        }else if (value=='B05'){
                            return "B05-士兵证";
                        }else if (value=='B06'){
                            return "B06-港澳居民来往内地通行证";
                        }else if(value=='B07'){
                            return "B07-台湾同胞来往内地通行证";
                        }else if (value=='B08'){
                            return "B08-临时身份证";
                        }else if(value=='B09'){
                            return "B09-外国人居留证";
                        }else if (value=='B10'){
                            return "B10-警官证";
                        }else if(value=='B11'){
                            return "B11-外国人永久拘留省份证";
                        }else if (value=='B12'){
                            return "B12-港澳台居民居住证";
                        }else if (value=='B99'){
                            return "B99-其他证件";
                        }
                        return value;
                    }},
                {field: 'gteeidnum', title: '担保人证件代码', width: 100, align: 'center'},
                {field: 'financeorgcode', title: '金融机构代码', width: 100, align: 'center'},
                {field: 'financeorginnum', title: '内部机构号', width: 100, align: 'center'},
                {field: 'transactiontype', title: '交易类型', width: 100, align: 'center',formatter:function (value) {
                        if (value=='01'){
                            return "01-回购类交易";
                        }else if(value=='02'){
                            return "02-信贷交易";
                        }else if (value=='03'){
                            return "03-其他交易";
                        }
                        return value;
                    }},
                {field: 'pledgerate', title: '抵质押率', width: 100, align: 'center'},
                {field: 'isgreenloan', title: '担保人国民经济部门', width: 100, align: 'center',formatter:function (value) {
                        if (value=='A'){
                            return "A-广义政府";
                        }else if (value=='A01'){
                            return "A01-中央政府";
                        }else if(value=='A02'){
                            return "A02-地方政府";
                        }else if (value=='A03'){
                            return "A03-社会保障基金";
                        }else if (value=='A04'){
                            return "A04-机关团体";
                        }else if(value=='A05'){
                            return "A05-部队";
                        }else if (value=='A06'){
                            return "A06-住房公积金";
                        }else if (value=='A99'){
                            return "A99-其他";
                        }else if(value=='B'){
                            return "B-金融机构部门";
                        }else if(value=='B01'){
                            return "B01-货币当局";
                        }else if (value=='B02'){
                            return "B02-监管当局";
                        }else if (value=='B03'){
                            return "B03-银行业存款类金融机构";
                        }else if(value=='B04'){
                            return "B04-银行业非存款类金融机构";
                        }else if (value=='B05'){
                            return "B05-证券业金融机构";
                        }else if (value=='B06'){
                            return "B06-保险业金融机构";
                        }else if(value=='B07'){
                            return "B07-交易及结算类金融机构";
                        }else if (value=='B08'){
                            return "B08-金融控股公司";
                        }else if(value=='B09'){
                            return "B09-特定目的载体";
                        }else if (value=='B99'){
                            return "B99-其他";
                        }else if(value=='C'){
                            return "C-非金融企业部门";
                        }else if(value=='C01'){
                            return "C01-公司";
                        }else if (value=='C02'){
                            return "C02-非公司企业";
                        }else if (value=='C99'){
                            return "C99-其他非金融企业部门";
                        }else if(value=='D'){
                            return "D-住户部门";
                        }else if(value=='D01'){
                            return "D01-住户";
                        }else if (value=='D02'){
                            return "D02-为住户服务的非营力机构";
                        }else if(value=='E'){
                            return "E-非居民部门";
                        }else if(value=='E01'){
                            return "E01-国际组织";
                        }else if (value=='E02'){
                            return "E02-外国政府";
                        }else if (value=='E03'){
                            return "E03-境外金融机构";
                        }else if(value=='E04'){
                            return "E04-境外非金融企业";
                        }else if (value=='E05'){
                            return "E05-外国居民";
                        }
                        return value;
                    }},
                {field: 'brrowerindustry', title: '担保人行业', width: 100, align: 'center',formatter:function (value) {
                        for (var i = 0;i <  baseAindustrys.length;i++){
                            if (baseAindustrys[i].substring(0,3)==value){
                                return baseAindustrys[i];
                            }
                        }
                    }},
                {field: 'brrowerareacode', title: '担保人地区代码', width: 100, align: 'center',formatter:function (value) {
                        for (var i = 0;i <  baseAreas.length;i++){
                            if (baseAreas[i].substring(0,6)==value){
                                return baseAreas[i];
                            }
                        }
                    }},
                {field: 'enterprisescale', title: '担保人企业规模', width: 100, align: 'center',formatter:function (value) {
                        if (value=='CS01'){
                            return "CS01-大型";
                        }else if(value=='CS02'){
                            return "CS02-中型";
                        }else if (value=='CS03'){
                            return "CS03-小型";
                        }else if(value=='CS04'){
                            return "CS04-微型";
                        }else if (value=='CS05'){
                            return "CS05-其他（非企业类单位）";
                        }
                        return value;
                    }},
                {field: 'sjrq', title: '数据日期',  align: 'center'},
                {field: 'operator', title: '操作人',  align: 'center'},
                {field: 'operationtime', title: '操作时间',  align: 'center'}
            ]],
            onDblClickRow: function (rowIndex,rowData) {
                //$("#tbForAddDialog").hide();//隐藏div
                //disableOcx();
                $('#info_formForAdd').form('clear');
                $('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','单位贷款担保合同信息详情');
                $('#info_formForAdd').form('load',rowData);
            }
        });
    })

    function disableOcx() {
        var form = document.forms[0];
        for ( var i = 0; i < form.length; i++) {
            var element = form.elements[i];
            element.disabled = "true";
        }
    }

    function noDisable() {
        var form = document.forms[0];
        for ( var i = 0; i < form.length; i++) {
            var element = form.elements[i];
            element.disabled = false;
        }
    }
    //动态查询
    function searchOnSelected(value) {
        var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = '';
        if (value == 'dsh'){
            operationnameParam = $("#operationnameParam").combobox("getValue");
        }
        $("#dg1").datagrid("load", {"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam});
    }

    $(".combo").click(function (e) {
        if (e.target.className == 'combo-text validatebox-text' || e.target.className == 'combo-text validatebox-text validatebox-invalid'){
            if ($(this).prev().combobox("panel").is(":visible")) {
                $(this).prev().combobox("hidePanel");
            } else {
                $(this).prev().combobox("showPanel");
            }
        }
    });

    // 新增
    function add(){
        resetForAdd();
        $('#dialog1').dialog('open').dialog('center').dialog('setTitle','新增贷款担保合同信息');
    }

    // 修改
    function edit(){
        var rows = $('#dg1').datagrid('getSelections');
        if(rows.length == 1){
            //$("#formForAdd").form('reset');
            resetForAdd();
            $("#formForAdd").form("load", rows[0]);
            $('#dialog1').dialog('open').dialog('center').dialog('setTitle','修改贷款担保合同信息');
        }else{
            $.messager.alert("提示", "请选择一条数据进行修改!", "info");
        }
    }

    // 保存
    function save() {
        if($("#formForAdd").form('validate')){
            $.ajax({
                type: "POST",//为post请求
                url: "XsaveXdkdbht",//这是我在后台接受数据的文件名
                data: $('#formForAdd').serialize(),//将该表单序列化
                dataType: "json",
                async: false,
                success: function (res) {
                    if (res.status == "1") {
                        $.messager.alert('提示', '提交成功!', 'info');
                        $("#dialog1").dialog("close");
                        $("#dg1").datagrid("reload");
                    } else {
                        $.messager.alert('提示', '提交失败!', 'info');
                    }
                },
                error: function (err) {//请求失败之后的操作

                }
            });
        }
    }

    // 重置
    function resetForAdd(){
        //$("#tbForAddDialog").show();//显示div
        //noDisable();
        $('#formForAdd').form('clear');
    }

    // 取消
    function cancel(){
        $('#dialog1').dialog('close');
    }

    // 删除
    function deleteit(){
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        if(rows.length > 0){
            info = '是否删除所选中的数据？';
        }else {
            info = '您没有选择数据，是否删除所有数据？';
        }
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XDeleteXdkdbht",
                    data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','删除成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','删除失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','删除失败','error');
                    }
                });
                $("#dg1").datagrid("reload");
            }
        });
    }

    // 导入Excel相关
    function importExcel(){
        document.getElementById("importbtn").disabled = "disabled";
        $('#excelfile').val('');
        $('#importExcel').dialog('open').dialog('center');
    }

    function inexcel(){
        var file = document.getElementById("excelfile").files[0];
        if(file == null){
            document.getElementById("importbtn").disabled = "disabled";
        }else{
            var fileName = file.name;
            var fileType = fileName.substring(fileName.lastIndexOf('.'),
                fileName.length);
            if (fileType == '.xls' || fileType == '.xlsx'){
                if (file) {
                    document.getElementById("importbtn").disabled = "";
                }
            } else {
                $.messager.alert('提示', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！","error");
                document.getElementById("importbtn").disabled = "disabled";
            }
        }
    }

    function excel(){
        $.messager.progress({
            title: '请稍等',
            msg: '数据正在导入中......'
        });
        $.ajaxFileUpload({
            type: "post",
            url: 'XimmportExcelThree',
            fileElementId: 'excelfile',
            secureuri: false,
            dataType: 'json',
            success: function (data) {
                const that = data.msg;
                $.messager.progress('close');
                if (that == "导入成功") {
                    $.messager.alert('提示', "导入成功", 'info');
                    $("#dg1").datagrid('reload');
                    $('#importExcel').dialog('close');
                } else if (that.startsWith("导入模板不正确")) {
                    $.messager.alert('提示', that, 'error');
                } else {
                    $.messager.show({
                        title: '导入反馈',
                        msg: "<div style='overflow-y:scroll;height:100%'>" + escape2Html(that) + "</div>",
                        timeout: 0,
                        showType: 'show',
                        width: 600,
                        height: 700,
                        style: {
                            right: '100',
                            top: document.body.scrollTop + document.documentElement.scrollTop
                        }
                    });
                }
            },
            error: function () {
                $.messager.progress('close');
                $.messager.alert('提示', '导入文件错误，请重新导入', 'error');
            }
        });
    }

    // 导出
    function showOut(model){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        var info = '';
        if (model){
            id = '00000000';
            info = '是否导出模板？';
        }else {
            for( var dataIndex in row){
                id = id + row[dataIndex].id + ",";
            }
            if(id != ''){
                info = '是否将选中的数据导出到Excel表中？';
            }else {
                info = '是否将全部数据导出到Excel表中？'
            }
        }
        var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('操作提示', info, function (r) {
            if (r) {
                window.location.href = "XExportXdkdbht?gteecontractcodeParam="+gteecontractcodeParam+"&loancontractcodeParam="+loancontractcodeParam+"&checkstatusParam="+checkstatusParam+"&id="+id + "&datastatus="+'${datastatus}';
            }
        });
    }

    // 校验
    function check(){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        for( var dataIndex in row){
            id = id + row[dataIndex].id + ",";
            if (row[dataIndex].checkstatus != 0){
                $.messager.alert("提示", "只能校验未校验的数据", "info");
                return;
            }
        }
        var info = '';
        if(id != ''){
            info = '是否校验选中的数据？';
        }else {
            info = '是否校验所有数据？'
        }
        var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r) {
            if (r) {
                $.messager.progress({
                    title: '请稍等',
                    msg: '正在校验数据中......'
                });
                $.ajax({
                    url: "XCheckDataThree",
                    dataType: "json",
                    async: true,
                    data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"checkstatusParam" : checkstatusParam},
                    type: "POST",
                    success: function (data) {
                    	$.messager.progress('close');
                        $("#dg1").datagrid("reload");
                        if (data.msg == "1") {
                            $.messager.alert('提示', '校验成功！', 'info');
                        }else if (data.msg == "-1") {
                            $.messager.alert('提示', '没有可校验的数据！', 'info');
                        } else {
                            $.messager.alert('提示', '校验完成,将自动下载校验结果', 'info', function (r) {
                                window.location.href = "XDownLoadCheckThree";
                            });
                        }
                    },
                    error: function () {
                        $.messager.progress('close');
                        $.messager.alert("提示", "校验出错，请重新校验！", "error");
                    }
                });               
            }
        })
    }

    // 提交
    function submitit(){
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            for(var i = 0;i < rows.length;i++){
                if(rows[i].checkstatus != '1'){
                    $.messager.alert("提示", "只能提交校验成功的数据", "info");
                    return;
                }
                id = id + rows[i].id + ',';
            }
            info = '是否提交所选中的数据？';
        }else {
            info = '您没有选择数据，是否提交所有数据？';
        }
        $.messager.confirm('提示',info,function (r){
            if(r){
                var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
                var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
                var checkstatusParam = $("#checkstatusParam").combobox("getValue");
                $.messager.confirm('提示',info,function (r){
                    if(r){
                        $.ajax({
                            sync: true,
                            type: "POST",
                            dataType: "json",
                            url: "XsubmitXdkdbht",
                            data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                            success: function (data) {
                                if (data == 0){
                                    $.messager.alert('操作提示','提交成功','info');
                                    $("#dg1").datagrid("reload");
                                }else {
                                    $.messager.alert('操作提示','提交失败','info');
                                }
                            },
                            error: function (err) {
                                $.messager.alert('操作提示','提交失败','error');
                            }
                        });
                        $("#dg1").datagrid("reload");
                    }
                });
            }
        });
    }

    //申请删除dsh
    function applyDelete() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            info = '是否申请删除所选中的数据？';
        }else {
            info = '是否申请删除所有数据？';
        }
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XApplyDeleteXdkdbht",
                    data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','申请删除成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','申请删除失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','申请删除失败','error');
                    }
                });
            }
        });
    }

    //申请修改
    function applyEdit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            info = '是否申请修改所选中的数据？';
        }else {
            info = '是否申请修改所有数据？';
        }
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XApplyEditXdkdbht",
                    data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','申请修改成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','申请修改失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','申请修改失败','error');
                    }
                });
            }
        });
    }

    /*同意申请*/
    function agreeApply() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
                return;
            }
            if (rows[i].operationname != '申请删除'){
                $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否同意所选中的申请？';
        }else {
            info = '是否同意所有的申请？';
        }
        var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XAgreeApplyXdkdbht",
                    data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','同意成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','同意失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','同意失败','error');
                    }
                });
            }
        });
    }

    /*拒绝申请*/
    function noAgreeApply() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
                return;
            }
            if (rows[i].operationname != '申请删除'){
                $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否拒绝所选中的申请？';
        }else {
            info = '是否拒绝所有的申请？';
        }
        var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XNoAgreeApplyXdkdbht",
                    data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','拒绝成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','拒绝失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','拒绝意失败','error');
                    }
                });
            }
        });
    }

    /*审核通过*/
    function agreeAudit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
                return;
            }
            if (rows[i].checkstatus != 1){
                $.messager.alert("提示","请选择校验通过的数据！","error");
                return;
            }
            if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
            }else {
                $.messager.alert("提示","请选择操作名为空的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否审核通过所选中的数据？';
        }else {
            info = '是否审核通过所有数据？';
        }
        var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XAgreeAuditXdkdbht",
                    data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','审核通过成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','审核通过失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','审核通过失败','error');
                    }
                });
            }
        });
    }

    /*审核不通过*/
    function noAgreeAudit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
                return;
            }
            if (rows[i].checkstatus != 1){
                $.messager.alert("提示","请选择校验通过的数据！","error");
                return;
            }
            if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
            }else {
                $.messager.alert("提示","请选择操作名为空的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否审核不通过所选中的数据？';
        }else {
            info = '是否审核不通过所有数据？';
        }
        var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.prompt('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XNoAgreeAuditXdkdbht",
                    data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam,"reason":r},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','审核不通过成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','审核不通过失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','审核不通过失败','error');
                    }
                });
            }else {
                $.messager.alert('操作提示','审核不通过原因必填','info');
            }
        });
    }

    // 新增交易类型
    function addtransactiontype(){
        var transactiontype = $('#transactiontype').val();
        $('#areatransactiontype').val(transactiontype);
        $('#dialogtransactiontype').dialog('open').dialog('center').dialog('setTitle','交易类型');
    }

    // 保存交易类型
    function savetransactiontype(){
        var areatransactiontype = $('#areatransactiontype').val();
        $('#transactiontype').val(areatransactiontype);
        $('#dialogtransactiontype').dialog('close');
    }

    // 取消交易类型
    function canceltransactiontype(){
        $('#dialogtransactiontype').dialog('close');
    }

    //新增按钮交易类型
    function addtransactiontypeCode(code){
        var areatransactiontype = $('#areatransactiontype').val();
        if (areatransactiontype == null || areatransactiontype == ''){
            $('#areatransactiontype').val(code);
        }else {
            $('#areatransactiontype').val(areatransactiontype + "," + code);
        }
    }

    //删除按钮交易类型
    function deletetransactiontypeCode(code){
        var areatransactiontype = $('#areatransactiontype').val();
        if (areatransactiontype != null && areatransactiontype != ''){
            var areatransactiontypes = areatransactiontype.split(",")
            for (var i = areatransactiontypes.length-1;i>=0;i--){
                if (areatransactiontypes[i] == code){
                    areatransactiontypes.splice(i,1);
                    break;
                }
            }
            var areatransactiontypeafter = "";
            for (var i = 0;i<areatransactiontypes.length;i++){
                areatransactiontypeafter = areatransactiontypeafter + areatransactiontypes[i] + ",";
            }
            $('#areatransactiontype').val(areatransactiontypeafter.substring(0,areatransactiontypeafter.length-1));
        }
    }

    // 新增币种
    function addgteecurrency(){
        var gteecurrency = $('#gteecurrency').val();
        $('#areagteecurrency').val(gteecurrency);
        $('#dialoggteecurrency').dialog('open').dialog('center').dialog('setTitle','币种');
    }

    // 保存币种
    function savegteecurrency(){
        var areagteecurrency = $('#areagteecurrency').val();
        $('#gteecurrency').val(areagteecurrency);
        $('#dialoggteecurrency').dialog('close');
    }

    // 取消币种
    function cancelgteecurrency(){
        $('#dialoggteecurrency').dialog('close');
    }

    //新增按钮币种
    function addgteecurrencyCode(){
        var code = $('#gteecurrencyCode').val();
        var areagteecurrency = $('#areagteecurrency').val();
        if (areagteecurrency == null || areagteecurrency == ''){
            $('#areagteecurrency').val(code);
        }else {
            $('#areagteecurrency').val(areagteecurrency + "," + code);
        }
    }

    //删除按钮币种
    function deletegteecurrencyCode(){
        var code = $('#gteecurrencyCode').val();
        var areagteecurrency = $('#areagteecurrency').val();
        if (areagteecurrency != null && areagteecurrency != ''){
            var areagteecurrencys = areagteecurrency.split(",")
            for (var i = areagteecurrencys.length-1;i>=0;i--){
                if (areagteecurrencys[i] == code){
                    areagteecurrencys.splice(i,1);
                    break;
                }
            }
            var areagteecurrencyafter = "";
            for (var i = 0;i<areagteecurrencys.length;i++){
                areagteecurrencyafter = areagteecurrencyafter + areagteecurrencys[i] + ",";
            }
            $('#areagteecurrency').val(areagteecurrencyafter.substring(0,areagteecurrencyafter.length-1));
        }
    }

    // 新增担保人证件类型
    function addgteeidtype(){
        var gteeidtype = $('#gteeidtype').val();
        $('#areagteeidtype').val(gteeidtype);
        $('#dialoggteeidtype').dialog('open').dialog('center').dialog('setTitle','担保人证件类型');
    }

    // 保存担保人证件类型
    function savegteeidtype(){
        var areagteeidtype = $('#areagteeidtype').val();
        $('#gteeidtype').val(areagteeidtype);
        $('#dialoggteeidtype').dialog('close');
    }

    // 取消担保人证件类型
    function cancelgteeidtype(){
        $('#dialoggteeidtype').dialog('close');
    }

    //新增按钮担保人证件类型
    function addgteeidtypeCode(code){
        var areagteeidtype = $('#areagteeidtype').val();
        if (areagteeidtype == null || areagteeidtype == ''){
            $('#areagteeidtype').val(code);
        }else {
            $('#areagteeidtype').val(areagteeidtype + "," + code);
        }
    }

    //删除按钮担保人证件类型
    function deletegteeidtypeCode(code){
        var areagteeidtype = $('#areagteeidtype').val();
        if (areagteeidtype != null && areagteeidtype != ''){
            var areagteeidtypes = areagteeidtype.split(",")
            for (var i = areagteeidtypes.length-1;i>=0;i--){
                if (areagteeidtypes[i] == code){
                    areagteeidtypes.splice(i,1);
                    break;
                }
            }
            var areagteeidtypeafter = "";
            for (var i = 0;i<areagteeidtypes.length;i++){
                areagteeidtypeafter = areagteeidtypeafter + areagteeidtypes[i] + ",";
            }
            $('#areagteeidtype').val(areagteeidtypeafter.substring(0,areagteeidtypeafter.length-1));
        }
    }

    /*$("#gteecurrency").combobox({
        onChange: function (n) {
            if (n != 'CNY'){
                $('#gteecnyamount').validatebox({required: true});
            }else {
                $('#gteecnyamount').val('');
                $('#gteecnyamount').validatebox({required: false});
            }
        }
    });*/

    //前台校验
    $.extend($.fn.datebox.defaults.rules, {
    	dateFormat : {
    		validator : function(value){
    			var patrn = /^\d{4}-\d{2}-\d{2}$/;
                return patrn.test(value);
    		},
    		message : '日期格式为：YYYY-MM-DD'
    	},
        normalDate : {
            validator : function(value) {
                var startTmp = new Date('1800-01-01');
                var endTmp = new Date('2100-12-31');
                var nowTmp = new Date(value);
                return nowTmp > startTmp && nowTmp < endTmp;
            },
            message : '日期早于1800-01-01且晚于2100-12-31'
        },
        compareToEnd : {
            validator : function(value) {
                var gteeenddate = $('#gteeenddate').combobox('getValue');
                if (gteeenddate == null || gteeenddate == ''){
                    return true;
                }
                var startTmp = new Date(gteeenddate);
                var nowTmp = new Date(value);
                return nowTmp <= startTmp;
            },
            message : '担保合同起始日期应小于等于担保合同到期日期'
        },
        teshu:{
            validator: function (value) {
                var patrn = /[？?！!^]/;
                return !patrn.test(value);
            },
            message: '不能包含特殊符号'
        },
        teshua:{
            validator: function (value) {
            	if(value != null){
            		var patrn = /[？?！!^]/;
                    return !patrn.test(value);
            	}
            },
            message: '不能包含特殊符号'
        },
        amount : {
            validator : function(value) {
                var reg = /^([0-9]\d{0,18}(\.\d{2})|0\.\d{2})$/;
                return reg.test(value);
            },
            message : '总长度不超过20位的，精度保留小数点后两位。'
        },
        pledgerate : {
            validator : function(value) {
                if (value.indexOf("%") == -1 && value.indexOf("‰") == -1){
                    return true
                }
                return false;
            },
            message : '抵质押率不能包含‰或%'
        },
        maoreamount : {
            validator : function(value) {
                var values = value.split(",");
                for (var i=0;i<values.length;i++){
                    var reg = /^([0-9]\d{0,18}(\.\d{2})|0\.\d{2})$/;
                    return reg.test(values[i]);
                }
            },
            message : '单个金额长度不超过20位的，精度保留小数点后两位。'
        }
    });

    //返回
    function fanhui(){
    	$.messager.progress({
            title: '请稍等',
            msg: '数据正在加载中......'
        });
    }
    
    
    /*$("#gteecurrency").combobox("textbox").bind("keydown",function(event) {
        var baseCurrencyList= ${currencyList};
        var data = [];
        for (var i = 0;i<baseCurrencyList.length;i++){
            if (baseCurrencyList[i].currencycode.substring(0,1)==event.originalEvent.key.toUpperCase() || event.originalEvent.key==' '){
                data.push({id:baseCurrencyList[i].currencycode,text:baseCurrencyList[i].currencycode +"-"+baseCurrencyList[i].currencyname});
            }
        }
        $("#gteecurrency").combobox("loadData", data);
    })*/

</script>
</body>
</html>
