<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%@ page import="com.geping.etl.common.util.VariableUtils" %>
<%@ page import="com.geping.etl.common.entity.Sys_Auth_Role_Resource" %>
<%@ page import="com.geping.etl.common.entity.Sys_UserAndOrgDepartment" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    VariableUtils vu = (VariableUtils)request.getSession().getAttribute("vu");
    Set<Sys_Auth_Role_Resource> operateSet = vu.getOperateReportSet();
    String status = request.getParameter("status");
    String shifoushenheziji = (String) session.getAttribute("shifoushenheziji");
    String role = vu.getRole();
    Sys_UserAndOrgDepartment sys_User = ((Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User"));
    String handlename = sys_User.getLoginid();
    String isusedelete = (String) request.getSession().getAttribute("isusedelete");
    String isuseupdate = (String) request.getSession().getAttribute("isuseupdate");
    List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
    List<String> baseAreas = new ArrayList<>();
    for (BaseArea baseArea : baseAreaList) {
        baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
    }
    List<BaseCountry> baseCountryList = (List<BaseCountry>) request.getAttribute("baseCountryList");
    List<String>  baseCountrys = new ArrayList<>();
    for (BaseCountry baseCountry : baseCountryList) {
    	baseCountrys.add("'"+baseCountry.getCountrycode()+ "-" +baseCountry.getCountryname()+"'");
    }
    List<String>  baseAreaAndCountrys = new ArrayList<>();
    baseAreaAndCountrys.addAll(baseAreas);
    baseAreaAndCountrys.addAll(baseCountrys);
    List<BaseAindustry> baseAindustryList = (List<BaseAindustry>) request.getAttribute("baseAindustryList");
    List<String> baseAindustrys = new ArrayList<>();
    for (BaseAindustry baseAindustry : baseAindustryList) {
        baseAindustrys.add("'"+baseAindustry.getAindustrycode()+ "-" +baseAindustry.getAindustryname()+"'");
    }
    List<BaseBindustry> baseBindustryList = (List<BaseBindustry>) request.getAttribute("baseBindustryList");
    List<String> baseBindustrys = new ArrayList<>();
    for (BaseBindustry baseBindustry : baseBindustryList) {
        baseBindustrys.add("'"+baseBindustry.getBindustrycode()+ "-" +baseBindustry.getBindustryname()+"'");
    }
    List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
    List<String> baseCurrencys = new ArrayList<>();
    for (BaseCurrency baseCurrency : baseCurrencyList) {
        baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>委托贷款发生额信息</title>
    <%@include file="../../../common/head.jsp"%>
</head>
<body>

<table id="dg1" style="height: 480px;" title="委托贷款发生额信息列表"></table>

<!--表格工具栏-->
<div id="tb1" style="padding: 5px; height: auto;">
    <%if("dtj".equals(status) || "dsh".equals(status)){%>
    <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;
    <%}else if ("dsb".equals(status)){%>
    <a class="easyui-linkbutton" href="VGenerateMessageUi" iconCls="icon-return">返回</a>&nbsp;&nbsp;
    <%}%>&nbsp;&nbsp;
    <label>金融机构代码：</label><input id="finorgcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>委托贷款合同编码：</label><input id="loancontractcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>委托贷款借据编码：</label><input id="loanbrowcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>借款人证件代码：</label><input id="browidcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>借款人证件类型：</label>
    <select class='easyui-combobox'  id="isfarmerloanParam" name="isfarmerloanParam" editable=false style="height: 23px; width:130px">
        <option value=''>=请选择=</option>
        <option value='A01'>A01-统一社会信用代码</option>
        <option value='A02'>A02-组织机构代码</option>
        <option value='A03'>A03-其他</option>
        <option value='B01'>B01-身份证</option>
        <option value='B02'>B02-户口簿</option>
        <option value='B03'>B03-护照</option>
        <option value='B04'>B04-军官证</option>
        <option value='B05'>B05-士兵证</option>
        <option value='B06'>B06-港澳居民来往内地通行证</option>
        <option value='B07'>B07-台湾同胞来往内地通行证</option>
        <option value='B08'>B08-临时身份证</option>
        <option value='B09'>B09-外国人居留证</option>
        <option value='B10'>B10-警官证</option>
        <option value='B11'>B11-外国人永久居留身份证</option>
        <option value='B12'>B12-港澳台居民居住证</option>
        <option value='B99'>B99-其他证件</option>
    </select>
    &nbsp;
    <label>校验类型：</label>
    <select class='easyui-combobox' id='checkstatusParam' name='checkstatusParam' editable=false style='height: 23px; width:80px' >
        <option value=''>=请选择=</option>
        <option value='0'>未校验</option>
        <option value='1'>校验成功</option>
        <option value='2'>校验失败</option>
    </select>&nbsp;&nbsp;
    <%if("dtj".equals(status)){%>
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected('dtj')">查询</a>&nbsp;&nbsp;<br>
    <%}else if ("dsh".equals(status)){%>
    <label>操作名：</label>
    <select class='easyui-combobox' id='operationnameParam' name='operationnameParam' editable=false style='height: 23px; width:80px' >
        <option value=''>=请选择=</option>
        <option value='申请删除'>申请删除</option>
    </select>&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected('dsh')">查询</a>&nbsp;&nbsp;<br>
    <%}%>
    <%if("dtj".equals(status)){
        for(Sys_Auth_Role_Resource sarr : operateSet){
            if(sarr.getResValue().equals("ADD")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-add" onclick="add()">新增</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("EDIT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-edit" onclick="edit()">修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("APPLYDELETE")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-remove" onclick="applyDelete()">申请删除</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("IMPORT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="importExcel()">导入</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("EXPORT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(false)">导出</a>&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(true)">导出模板</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("CHECKOUT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-accept" onclick="check()">校验</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("SUBMIT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-easyicon-net-16" onclick="submitit()">提交</a>
    <%
            }
        }
    }else if("dsh".equals(status)){
        for(Sys_Auth_Role_Resource sarr : operateSet){
            if(sarr.getResValue().equals("EDIT") && "yes".equals(isuseupdate)){
    %>
    <%--<a class="easyui-linkbutton" iconCls="icon-edit" onclick="applyEdit()" >申请修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
    <%
    }else if(sarr.getResValue().equals("DELETE") && "yes".equals(isusedelete)){
    %>
    <%--<a class="easyui-linkbutton" data-options="iconCls:'icon-remove'" onclick="applyDelete()" >申请删除</a>&nbsp;--%>
    <%
    }else if(sarr.getResValue().equals("EXPORT")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut(false)">导出</a>&nbsp;&nbsp;&nbsp;
    <%
    }else if(sarr.getResValue().equals("CHECK")){
    %>
    <a class="easyui-linkbutton" iconCls="icon-ok" onclick="agreeApply()" >同意申请删除</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-cross" onclick="noAgreeApply()" >拒绝申请删除</a>&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-application-get" onclick="agreeAudit()" >审核通过</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-cross" onclick="noAgreeAudit()" >审核不通过</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <%
                }
            }
        }%>
</div>

<!-- 导入文件dialog -->
<div style="visibility: hidden;">
    <div id="importExcel" class="easyui-dialog" style="width: 600px; height: 150px;top: 100px;padding: 20px;" title="数据导入" data-options="modal:true,closed:true">
        文件导入:<input type="file" accept=".xls,.xlsx" name="excelfile" id="excelfile" onchange="inexcel()"/><br>
        <input type="button" id="importbtn" value="提交" style="width:50px;margin-top:20px;" onclick="excel()"/>
    </div>
</div>

<!--新增修改窗口-->
<div id="dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div id="tbForAddDialog" style="padding-left: 100px;padding-top: 10px">
        <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="save()">确定</a>&nbsp;&nbsp;
        <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="cancel()">取消</a>
    </div>

    <!-- 信息录入 -->
    <div align="center" style="padding-top: 30px">
        <form id="formForAdd" method="post">
            <input id="id" class="backId" name="id" type="hidden">

        </form>
    </div>
</div>

<!--详情窗口-->
<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div align="center" style="padding-top: 30px">
        <form id="info_formForAdd" method="post">
            <input id="info_id" class="backId" name="id" type="hidden">
            <table class='enterTable' id='info_enterTable'>
            	<tr>
                    <td align='right'>
                        金融机构代码:
                    </td>
                    <td>
                        <input id='info_finorgcode' name='finorgcode' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        内部机构号:
                    </td>
                    <td>
                        <input id='info_finorgincode' name='finorgincode' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
            	<tr>
                    <td align='right'>
                        金融机构地区代码:
                    </td>
                    <td>
                        <input id='info_finorgareacode' name='finorgareacode' disabled="disabled" style='height: 23px; width:173px'>

                    </td>
                    <td align='right'>
                        借款人证件类型:
                    </td>
                    <td>
                        <select id='info_isfarmerloan' name='isfarmerloan' disabled="disabled" style='height: 23px; width:173px'>
                            <option value="A01">A01-统一社会信用代码</option>
                            <option value="A02">A02-组织机构代码</option>
                            <option value="A03">A03-其他</option>
                            <option value='B01'>B01-身份证</option>
					        <option value='B02'>B02-户口簿</option>
					        <option value='B03'>B03-护照</option>
					        <option value='B04'>B04-军官证</option>
					        <option value='B05'>B05-士兵证</option>
					        <option value='B06'>B06-港澳居民来往内地通行证</option>
					        <option value='B07'>B07-台湾同胞来往内地通行证</option>
					        <option value='B08'>B08-临时身份证</option>
					        <option value='B09'>B09-外国人居留证</option>
					        <option value='B10'>B10-警官证</option>
					        <option value='B11'>B11-外国人永久居留身份证</option>
					        <option value='B12'>B12-港澳台居民居住证</option>
					        <option value='B99'>B99-其他证件</option>
                        </select>
                    </td>
                </tr>
            	<tr>
            	<td align='right'>
                        借款人证件代码:
                    </td>
                    <td>
                        <input id='info_browidcode' name='browidcode' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        借款人国民经济部门:
                    </td>
                    <td>
                        <select id='info_isgreenloan' name='isgreenloan' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='A01'>A01-中央政府</option>
                            <option value='A02'>A02-地方政府</option>
                            <option value='A03'>A03-社会保障基金</option>
                            <option value='A04'>A04-机关团体</option>
                            <option value='A05'>A05-部队</option>
                            <option value='A06'>A06-住房公积金</option>
                            <option value='A99'>A99-其他</option>
                            <option value='B01'>B01-货币当局</option>
                            <option value='B02'>B02-监管当局</option>
                            <option value='B03'>B03-银行业存款类金融机构</option>
                            <option value='B04'>B04-银行业非存款类金融机构</option>
                            <option value='B05'>B05-证券业金融机构</option>
                            <option value='B06'>B06-保险业金融机构</option>
                            <option value='B07'>B07-交易及结算类金融机构</option>
                            <option value='B08'>B08-金融控股公司</option>
                            <option value='B09'>B09-特定目的载体</option>
                            <option value='B99'>B99-其他</option>
                            <option value='C01'>C01-公司</option>
                            <option value='C02'>C02-非公司企业</option>
                            <option value='C99'>C99-其他非金融企业部门</option>
                            <option value='D01'>D01-住户</option>
                            <option value='D02'>D02-为住户服务的非营力机构</option>
                            <option value='E01'>E01-国际组织</option>
                            <option value='E02'>E02-外国政府</option>
                            <option value='E03'>E03-境外金融机构</option>
                            <option value='E04'>E04-境外非金融企业</option>
                            <option value='E05'>E05-外国居民</option>
                        </select>
                    </td>
            	</tr>
            	<tr>
            	<td align='right'>
                        借款人行业:
                    </td>
                    <td>
                        <select id='info_browinds' name='browinds' disabled="disabled" style='height: 23px; width:173px'>
                            <c:forEach items='${baseAindustryList}' var='aa'>
                                <option value='${aa.aindustrycode}'>${aa.aindustrycode}-${aa.aindustryname}</option>
                            </c:forEach>
                        </select>
                    </td>
            	<td align='right'>
                        借款人地区代码:
                    </td>
                    <td>
                        <input id='info_browareacode' name='browareacode' disabled="disabled" style='height: 23px; width:173px'/>
                            <%--<c:forEach items='${baseAreaAndCountryList}' var='aa'>--%>
                                <%--<option value='${aa.areacode}'>${aa.areacode}-${aa.areaname}</option>--%>
                            <%--</c:forEach>--%>

                    </td>
            	</tr>
            	<tr>
                    <td align='right'>
                        借款人经济成分:
                    </td>
                    <td>
                        <select id='info_entpczjjcf' name='entpczjjcf' disabled="disabled" style='height: 23px; width:173px' >
                            <option value='A01'>A01-国有控股</option>
                            <option value='A0101'>A0101-国有相对控股</option>
                            <option value='A0102'>A0102-国有绝对控股</option>
                            <option value='A02'>A02-集体控股</option>
                            <option value='A0201'>A0201-集体相对控股</option>
                            <option value='A0202'>A0202-集体绝对控股</option>
                            <option value='B01'>B01-私人控股</option>
                            <option value='B0101'>B0101-私人相对控股</option>
                            <option value='B0102'>B0102-私人绝对控股</option>
                            <option value='B02'>B02-港澳台控股</option>
                            <option value='B0201'>B0201-港澳台相对控股</option>
                            <option value='B0202'>B0202-港澳台绝对控股</option>
                            <option value='B03'>B03-外商控股</option>
                            <option value='B0301'>B0301-外商相对控股</option>
                            <option value='B0302'>B0302-外商绝对控股</option>
                        </select>
                    </td>
                    <td align='right'>
                        借款人企业规模:
                    </td>
                    <td>
                        <select id='info_entpmode' name='entpmode' disabled="disabled" style='height: 23px; width:173px' >
                            <option value='CS01'>CS01-大型</option>
                            <option value='CS02'>CS02-中型</option>
                            <option value='CS03'>CS03-小型</option>
                            <option value='CS04'>CS04-微型</option>
                            <option value='CS05'>CS05-其他（非企业类单位）</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        委托贷款借据编码:
                    </td>
                    <td>
                        <input id='info_loanbrowcode' name='loanbrowcode' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                       委托 贷款合同编码:
                    </td>
                    <td>
                        <input id='info_loancontractcode' name='loancontractcode' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                 <td align='right'>
                        贷款实际投向:
                    </td>
                    <td>
                        <select id='info_loanactdect' name='loanactdect' disabled="disabled" style='height: 23px; width:173px'>
                            <c:forEach items='${baseBindustryList}' var='aa'>
                                <option value='${aa.bindustrycode}'>${aa.bindustrycode}-${aa.bindustryname}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td align='right'>
                        委托贷款发放日期:
                    </td>
                    <td>
                        <input id='info_loanstartdate' name='loanstartdate' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                <td align='right'>
                        委托贷款到期日期:
                    </td>
                    <td>
                        <input id='info_loanenddate' name='loanenddate' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        交易流水号:
                    </td>
                    <td>
                        <input id='info_transactionnum' name='transactionnum' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
            	<tr>
            	<td align='right'>
                        币种:
                    </td>
                    <td>
                        <select id='info_loancurrency' name='loancurrency' disabled="disabled" style='height: 23px; width:173px'>
                            <c:forEach items='${baseCurrencyList}' var='aa'>
                                <option value='${aa.currencycode}'>${aa.currencycode}-${aa.currencyname}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td align='right'>
                        委托贷款发生金额:
                    </td>
                    <td>
                        <input id='info_loanamt' name='loanamt' disabled="disabled" style='width:173px;'/>
                    </td>
            	</tr>
            	<tr>
            	<td align='right'>
                        委托贷款发生金额折人民币:
                    </td>
                    <td>
                        <input id='info_loancnyamt' name='loancnyamt' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        利率是否固定:
                    </td>
                    <td>
                        <select id='info_rateisfix' name='rateisfix' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='RF01'>RF01-固定利率</option>
                            <option value='RF02'>RF02-浮动利率</option>
                        </select>
                    </td>
            	</tr>
            	<tr>
            	<td align='right'>
                        利率水平:
                    </td>
                    <td>
                        <input id='info_ratelevel' name='ratelevel' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        手续费金额折人民币:
                    </td>
                    <td>
                        <input id='info_sxfcny' name='sxfcny' disabled="disabled" style='width:173px;'/>
                    </td>
            	</tr>
            	<tr>
            	<td align='right'>
                        贷款担保方式:
                    </td>
                    <td>
                        <select id='info_gteemethod' name='gteemethod' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='A'>A-质押贷款</option>
                            <option value='B01'>B01-房地产抵押贷款</option>
                            <option value='B99'>B99-其他抵押贷款</option>
                            <option value='C01'>C01-联保贷款</option>
                            <option value='C99'>C99-其他保证贷款</option>
                            <option value='D'>D-信用/免担保贷款</option>
                            <option value='E'>E-组合担保</option>
                            <option value='Z'>Z-其他</option>
                        </select>
                    </td>
                    <td align='right'>
                        委托人国民经济部门:
                    </td>
                    <td>
                        <select id='info_wtrgmjjbm' name='wtrgmjjbm' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='A01'>A01-中央政府</option>
                            <option value='A02'>A02-地方政府</option>
                            <option value='A03'>A03-社会保障基金</option>
                            <option value='A04'>A04-机关团体</option>
                            <option value='A05'>A05-部队</option>
                            <option value='A06'>A06-住房公积金</option>
                            <option value='A99'>A99-其他</option>
                            <option value='B01'>B01-货币当局</option>
                            <option value='B02'>B02-监管当局</option>
                            <option value='B03'>B03-银行业存款类金融机构</option>
                            <option value='B04'>B04-银行业非存款类金融机构</option>
                            <option value='B05'>B05-证券业金融机构</option>
                            <option value='B06'>B06-保险业金融机构</option>
                            <option value='B07'>B07-交易及结算类金融机构</option>
                            <option value='B08'>B08-金融控股公司</option>
                            <option value='B09'>B09-特定目的载体</option>
                            <option value='B99'>B99-其他</option>
                            <option value='C01'>C01-公司</option>
                            <option value='C02'>C02-非公司企业</option>
                            <option value='C99'>C99-其他非金融企业部门</option>
                            <option value='D01'>D01-住户</option>
                            <option value='D02'>D02-为住户服务的非营力机构</option>
                            <option value='E01'>E01-国际组织</option>
                            <option value='E02'>E02-外国政府</option>
                            <option value='E03'>E03-境外金融机构</option>
                            <option value='E04'>E04-境外非金融企业</option>
                            <option value='E05'>E05-外国居民</option>
                        </select>
                    </td>
            	</tr>
            	<tr>
            	<td align='right'>
                        委托人证件类型:
                    </td>
                    <td>
                        <select id='info_wtrtype' name='wtrtype' disabled="disabled" style='height: 23px; width:173px'>
                            <option value="A01">A01-统一社会信用代码</option>
                            <option value="A02">A02-组织机构代码</option>
                            <option value="A03">A03-其他</option>
                            <option value='B01'>B01-身份证</option>
					        <option value='B02'>B02-户口簿</option>
					        <option value='B03'>B03-护照</option>
					        <option value='B04'>B04-军官证</option>
					        <option value='B05'>B05-士兵证</option>
					        <option value='B06'>B06-港澳居民来往内地通行证</option>
					        <option value='B07'>B07-台湾同胞来往内地通行证</option>
					        <option value='B08'>B08-临时身份证</option>
					        <option value='B09'>B09-外国人居留证</option>
					        <option value='B10'>B10-警官证</option>
					        <option value='B11'>B11-外国人永久居留身份证</option>
					        <option value='B12'>B12-港澳台居民居住证</option>
					        <option value='B99'>B99-其他证件</option>
                        </select>
                    </td>
                    <td align='right'>
                        委托人证件代码:
                    </td>
                    <td>
                        <input id='info_wtrcode' name='wtrcode' disabled="disabled" style='width:173px;'/>
                    </td>
            	</tr>
            	<tr>
                    <td align='right'>
                        委托人行业:
                    </td>
                    <td>
                        <select id='info_wtrhy' name='wtrhy' disabled="disabled" style='height: 23px; width:173px'>
                            <c:forEach items='${baseAindustryList}' var='aa'>
                                <option value='${aa.aindustrycode}'>${aa.aindustrycode}-${aa.aindustryname}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td align='right'>
                        委托人地区代码:
                    </td>
                    <td>
                        <input id='info_wtrdqdm' name='wtrdqdm' disabled="disabled" style='height: 23px; width:173px'/>
                            <%--<c:forEach items='${baseAreaAndCountryList}' var='aa'>--%>
                                <%--<option value='${aa.areacode}'>${aa.areacode}-${aa.areaname}</option>--%>
                            <%--</c:forEach>--%>

                    </td>
                </tr>
            <tr>
                    <td align='right'>
                        委托人经济成分:
                    </td>
                    <td>
                        <select id='info_wtrjjcf' name='wtrjjcf' disabled="disabled" style='height: 23px; width:173px' >
                            <option value='A01'>A01-国有控股</option>
                            <option value='A0101'>A0101-国有相对控股</option>
                            <option value='A0102'>A0102-国有绝对控股</option>
                            <option value='A02'>A02-集体控股</option>
                            <option value='A0201'>A0201-集体相对控股</option>
                            <option value='A0202'>A0202-集体绝对控股</option>
                            <option value='B01'>B01-私人控股</option>
                            <option value='B0101'>B0101-私人相对控股</option>
                            <option value='B0102'>B0102-私人绝对控股</option>
                            <option value='B02'>B02-港澳台控股</option>
                            <option value='B0201'>B0201-港澳台相对控股</option>
                            <option value='B0202'>B0202-港澳台绝对控股</option>
                            <option value='B03'>B03-外商控股</option>
                            <option value='B0301'>B0301-外商相对控股</option>
                            <option value='B0302'>B0302-外商绝对控股</option>
                        </select>
                    </td>
                    <td align='right'>
                        委托人企业规模:
                    </td>
                    <td>
                        <select id='info_wtrqygm' name='wtrqygm' disabled="disabled" style='height: 23px; width:173px' >
                            <option value='CS01'>CS01-大型</option>
                            <option value='CS02'>CS02-中型</option>
                            <option value='CS03'>CS03-小型</option>
                            <option value='CS04'>CS04-微型</option>
                            <option value='CS05'>CS05-其他（非企业类单位）</option>
                        </select>
                    </td>
                </tr>
            	<tr>
            	<td align='right'>
                        发放/收回标识:
                    </td>
                    <td>
                        <select id='info_givetakeid' name='givetakeid' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='0'>0-收回</option>
                            <option value='1'>1-发放</option>
                        </select>
                    </td>
                    <td align='right'>
                        贷款用途:
                    </td>
                    <td>
                        <input id='info_issupportliveloan' name='issupportliveloan' disabled="disabled" style='width:173px;'/>
                    </td>
            	</tr>
            	<tr>
            	<td align='right'>
                        数据日期:
                    </td>
                    <td>
                        <input id='info_sjrq' name='sjrq' disabled="disabled" style='width:173px;'/>
                    </td>
            	</tr>
            </table>
        </form>
    </div>
</div>

<script type="text/javascript">
    var index = 5;
    var baseAreaAndCountrys = <%=baseAreaAndCountrys%>;
    var baseAreas = <%=baseAreas%>;
    var baseAindustrys = <%=baseAindustrys%>;
    var baseBindustrys = <%=baseBindustrys%>;
    var baseCurrencys = <%=baseCurrencys%>;
    var handlename = '<%= handlename%>';
    var role = '<%= role%>';
    var shifoushenheziji = '<%= shifoushenheziji%>';
    var tablelazyload = "<table class='enterTable' id='enterTable'>"
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        金融机构代码:                                                                                                                                                      "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='finorgcode' name='finorgcode' style='width:173px;' class='easyui-validatebox' maxlength='18' data-options='validType:[\"teshuone\"]'/>                                            "
        +"    </td>                                                                                                                                                                  "
        +"    <td align='right'>                                                                                                                                                     "
        +"        内部机构号:                                                                                                                                                "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='finorgincode' name='finorgincode' style='width:173px;' class='easyui-validatebox' maxlength='30' data-options='validType:[\"teshutwo\"]'/>                                        "
        +"    </td>                                                                                                                                                                  "
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        金融机构地区代码:                                                                                                                                                  "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combotree' id='finorgareacode' name='finorgareacode' style='height: 23px; width:173px' data-options=\"url:'XgetAdmindivideJson2',valueField:'id',textField:'text',editable:true,value:''\">           "
        +"                                                                                                                              "
        +"                                                                                                    "
        +"                                                                                                                                                        "
        +"        </select>                                                                                                                                                          "
        +"    </td>                                                                                                                                                                  "
        +"    <td align='right'>                                                                                                                                                     "
        +"        借款人证件类型:                                                                                                                                                      "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='isfarmerloan' name='isfarmerloan' editable=true style='height: 23px; width:173px' >                                             "
        +"        <option value='A01'>A01-统一社会信用代码</option>"
        +"        <option value='A02'>A02-组织机构代码</option>"
        +"        <option value='A03'>A03-其他</option>"
        +"        <option value='B01'>B01-身份证</option>"
        +"        <option value='B02'>B02-户口簿</option>"
        +"        <option value='B03'>B03-护照</option>"
        +"        <option value='B04'>B04-军官证</option>"
        +"        <option value='B05'>B05-士兵证</option>"
        +"        <option value='B06'>B06-港澳居民来往内地通行证</option>"
        +"        <option value='B07'>B07-台湾同胞来往内地通行证</option>"
        +"        <option value='B08'>B08-临时身份证</option>"
        +"        <option value='B09'>B09-外国人居留证</option>"
        +"        <option value='B10'>B10-警官证</option>"
        +"        <option value='B11'>B11-外国人永久居留身份证</option>"
        +"        <option value='B12'>B12-港澳台居民居住证</option>"
        +"        <option value='B99'>B99-其他证件</option>"
        +"        </select>                                                                                                                                                          "
        +"    </td>                                                                                                                                                                  "
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        借款人证件代码:                                                                                                                                                    "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='browidcode' name='browidcode' style='width:173px;' class='easyui-validatebox' maxlength='60' data-options='validType:[\"teshutwo\"]'/>                                            "
        +"    </td>"                                                                                                                                                                  
        +"    <td align='right'>                                                                                                                                                     "
        +"        借款人国民经济部门:                                                                                                                                                      "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='isgreenloan' name='isgreenloan' editable=true style='height: 23px; width:173px' >               "
        +"            <option value='A01'>A01-中央政府</option>"
		+"	        <option value='A02'>A02-地方政府</option>"
		+"	        <option value='A03'>A03-社会保障基金</option>"
		+"	        <option value='A04'>A04-机关团体</option>"
		+"	        <option value='A05'>A05-部队</option>"
		+"	        <option value='A06'>A06-住房公积金</option>"
		+"	        <option value='A99'>A99-其他</option>"
		+"	        <option value='B01'>B01-货币当局</option>"
		+"	        <option value='B02'>B02-监管当局</option>"
		+"	        <option value='B03'>B03-银行业存款类金融机构</option>"
		+"	        <option value='B04'>B04-银行业非存款类金融机构</option>"
		+"	        <option value='B05'>B05-证券业金融机构</option>"
		+"	        <option value='B06'>B06-保险业金融机构</option>"
		+"	        <option value='B07'>B07-交易及结算类金融机构</option>"
		+"	        <option value='B08'>B08-金融控股公司</option>"
		+"	        <option value='B09'>B09-特定目的载体</option>"
		+"	        <option value='B99'>B99-其他</option>"
		+"	        <option value='C01'>C01-公司</option>"
		+"	        <option value='C02'>C02-非公司企业</option>"
		+"	        <option value='C99'>C99-其他非金融企业部门</option>"
		+"	        <option value='D01'>D01-住户</option>"
		+"	        <option value='D02'>D02-为住户服务的非营力机构</option>"
		+"	        <option value='E01'>E01-国际组织</option>"
		+"	        <option value='E02'>E02-外国政府</option>"
		+"	        <option value='E03'>E03-境外金融机构</option>"
		+"	        <option value='E04'>E04-境外非金融企业</option>"
		+"	        <option value='E05'>E05-外国居民</option>                                                                                                                    "
        +"        </select>                                                                                                                                                        "
        +"    </td>"                                                                                                                                                                 
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        借款人行业:                                                                                                                                                        "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='browinds' name='browinds' style='height: 23px; width:173px' >                       "
        +"            <c:forEach items='${baseAindustryList}' var='aa'>                                                                                                              "
        +"                <option value='${aa.aindustrycode}'>${aa.aindustrycode}-${aa.aindustryname}</option>                                                                       "
        +"            </c:forEach>                                                                                                                                                   "
        +"        </select>                                                                                                                                                          "
        +"    </td>                                                                                                                                                                  "
        +"    <td align='right'>                                                                                                                                                     "
        +"        借款人地区代码:                                                                                                                                                    "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combotree' id='browareacode' name='browareacode' style='height: 23px; width:173px' data-options=\"url:'XgetAdmindivideJson',valueField:'id',textField:'text',editable:true,value:''\"> "
        +"        </select>                                                                                                                                                          "
        +"    </td>"                                                                                                                                                                  
        +"	</tr>                                                                                                                                                                      "
        +"	<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        借款人经济成分:                                                                                                                                                "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='entpczjjcf' name='entpczjjcf' editable=true style='height: 23px; width:173px' >                                               "
        +"        	  <option value=''>--请选择--</option>"
        +"            <option value='A01'>A01-国有控股</option>                                                                                                                        "
        +"            <option value='A0101'>A0101-国有相对控股</option>                                                                                                                "
        +"            <option value='A0102'>A0102-国有绝对控股</option>                                                                                                                "
        +"            <option value='A02'>A02-集体控股</option>                                                                                                                        "
        +"            <option value='A0201'>A0201-集体相对控股</option>                                                                                                                "
        +"            <option value='A0202'>A0202-集体绝对控股</option>                                                                                                                "
        +"            <option value='B01'>B01-私人控股</option>                                                                                                                        "
        +"            <option value='B0101'>B0101-私人相对控股</option>                                                                                                                "
        +"            <option value='B0102'>B0102-私人绝对控股</option>                                                                                                                "
        +"            <option value='B02'>B02-港澳台控股</option>                                                                                                                      "
        +"            <option value='B0201'>B0201-港澳台相对控股</option>                                                                                                              "
        +"            <option value='B0202'>B0202-港澳台绝对控股</option>                                                                                                              "
        +"            <option value='B03'>B03-外商控股</option>                                                                                                                        "
        +"            <option value='B0301'>B0301-外商相对控股</option>                                                                                                                "
        +"            <option value='B0302'>B0302-外商绝对控股</option>                                                                                                              "
        +"        </select>                                                                                                                                                          "
        +"    </td>                                                                                                                                                                  "
        +"    <td align='right'>                                                                                                                                                     "
        +"        借款人企业规模:                                                                                                                                                          "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='entpmode' name='entpmode' editable=true style='height: 23px; width:173px' >                                                   "
        +"        	  <option value=''>--请选择--</option>"
        +"            <option value='CS01'>CS01-大型</option>                                                                                                                        "
        +"            <option value='CS02'>CS02-中型</option>                                                                                                                        "
        +"            <option value='CS03'>CS03-小型</option>                                                                                                                        "
        +"            <option value='CS04'>CS04-微型</option>                                                                                                                        "
        +"            <option value='CS05'>CS05-其他（非企业类单位）</option>                                                                                                        "
        +"        </select>                                                                                                                                                          "
        +"	  </td>"                                                                                                                                                                  
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        委托贷款借据编码:                                                                                                                                                      "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='loanbrowcode' name='loanbrowcode' style='width:173px;' class='easyui-validatebox' maxlength='100' data-options='validType:[\"teshutwo\"]'/>                                        "
        +"    </td>                                                                                                                                                                  "
        +"    <td align='right'>                                                                                                                                                     "
        +"        委托贷款合同编码:                                                                                                                                                      "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='loancontractcode' name='loancontractcode' style='width:173px;' class='easyui-validatebox' maxlength='100' data-options='validType:[\"teshutwo\"]'/>                                "
        +"    </td>"                                                                                                                                                                  
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        贷款实际投向:                                                                                                                                                      "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='loanactdect' name='loanactdect' style='height: 23px; width:173px'>                 "
        +"        	  <option value=''>--请选择--</option>"
        +"            <c:forEach items='${baseBindustryList}' var='aa'>                                                                                                              "
        +"                <option value='${aa.bindustrycode}'>${aa.bindustrycode}-${aa.bindustryname}</option>                                                                       "
        +"            </c:forEach>                                                                                                                                                   "
        +"        </select>                                                                                                                                                          "
        +"    </td>"                                                                                                                                                                  
        +"    <td align='right'>                                                                                                                                                     "
        +"        委托贷款发放日期:                                                                                                                                                      "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='loanstartdate' name='loanstartdate' style='width:173px;' class='easyui-datebox' data-options='validType:[\"dateFormat\",\"normalDate\"]'/>                                         "
        +"    </td>"                                                                                                                                                                  
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        贷款到期日期:                                                                                                                                                      "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='loanenddate' name='loanenddate' style='width:173px;' class='easyui-datebox' data-options='validType:[\"dateFormat\",\"normalDate\",\"compareToEnd\"]'/>                                             "
        +"    </td>"                                                                                                                                                                  
        +"    <td align='right'>                                                                                                                                                     "
        +"        交易流水号:                                                                                                                                            "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='transactionnum' name='transactionnum' style='width:173px;' class='easyui-validatebox' maxlength='60' data-options='validType:[\"teshutwo\"]'/>                             "
        +"    </td>"                                                                                                                                                                  
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        币种:                                                                                                                                                          "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='loancurrency' name='loancurrency' style='height: 23px; width:173px' >               "
        +"            <c:forEach items='${baseCurrencyList}' var='aa'>                                                                                                               "
        +"                <option value='${aa.currencycode}'>${aa.currencycode}-${aa.currencyname}</option>                                                                          "
        +"            </c:forEach>                                                                                                                                                   "
        +"        </select>                                                                                                                                                          "
        +"    </td>"                                                                                                                                                                 
        +"    <td align='right'>                                                                                                                                                     "
        +"        委托贷款发生金额:                                                                                                                                                      "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='loanamt' name='loanamt' style='width:173px;' class='easyui-validatebox' data-options='precision:2,validType:[\"amount\"]'/>                                                  "
        +"    </td>"                                                                                                                                                                  
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        委托贷款发生金额折人民币:                                                                                                                                              "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='loancnyamt' name='loancnyamt' style='width:173px;' class='easyui-validatebox' data-options='precision:2,validType:[\"amount\"]'/>                                           "
        +"    </td>"                                                                                                                                                                  
        +"    <td align='right'>                                                                                                                                                     "
        +"        利率是否固定:                                                                                                                                                      "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='rateisfix' name='rateisfix' editable=true style='height: 23px; width:173px' >                     "
        +"            <option value='RF01'>RF01-固定利率</option>                                                                                                                    "
        +"            <option value='RF02'>RF02-浮动利率</option>                                                                                                                    "
        +"        </select>                                                                                                                                                          "
        +"    </td>"                                                                                                                                                                  
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        利率水平:                                                                                                                                                          "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='ratelevel' name='ratelevel' style='width:173px;' class='easyui-validatebox' data-options='precision:5,validType:[\"ratelevel\",\"baserate\"]'/>                                              "
        +"    </td>"                                                                                                                                                                  
        +"    <td align='right'>                                                                                                                                                     "
        +"        手续费金额折人民币:                                                                                                                                                          "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='sxfcny' name='sxfcny' style='width:173px;' class='easyui-validatebox' data-options='precision:2,validType:[\"amount\"]'/>                                                       "
        +"    </td>                                                                                                                                                                  "
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        贷款担保方式:                                                                                                                                                      "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='gteemethod' name='gteemethod' editable=true style='height: 23px; width:173px'>                   "
        +"        	  <option value=''>--请选择--</option>"
        +"            <option value='A'>A-质押贷款</option>                                                                                                                          "
        +"            <option value='B01'>B01-房地产抵押贷款</option>                                                                                                                "
        +"            <option value='B99'>B99-其他抵押贷款</option>                                                                                                                  "
        +"            <option value='C01'>C01-联保贷款</option>                                                                                                                      "
        +"            <option value='C99'>C99-其他保证贷款</option>                                                                                                                  "
        +"            <option value='D'>D-信用/免担保贷款</option>                                                                                                                   "
        +"            <option value='E'>E-组合担保</option>                                                                                                                          "
        +"            <option value='Z'>Z-其他</option>                                                                                                                              "
        +"        </select>                                                                                                                                                          "
        +"    </td>"                                                                                                                                                                  
        +"    <td align='right'>                                                                                                                                                     "
        +"        委托人国民经济部门:                                                                                                                                                      "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='wtrgmjjbm' name='wtrgmjjbm' editable=true style='height: 23px; width:173px' >               "
        +"            <option value='A01'>A01-中央政府</option>"
		+"	        <option value='A02'>A02-地方政府</option>"
		+"	        <option value='A03'>A03-社会保障基金</option>"
		+"	        <option value='A04'>A04-机关团体</option>"
		+"	        <option value='A05'>A05-部队</option>"
		+"	        <option value='A06'>A06-住房公积金</option>"
		+"	        <option value='A99'>A99-其他</option>"
		+"	        <option value='B01'>B01-货币当局</option>"
		+"	        <option value='B02'>B02-监管当局</option>"
		+"	        <option value='B03'>B03-银行业存款类金融机构</option>"
		+"	        <option value='B04'>B04-银行业非存款类金融机构</option>"
		+"	        <option value='B05'>B05-证券业金融机构</option>"
		+"	        <option value='B06'>B06-保险业金融机构</option>"
		+"	        <option value='B07'>B07-交易及结算类金融机构</option>"
		+"	        <option value='B08'>B08-金融控股公司</option>"
		+"	        <option value='B09'>B09-特定目的载体</option>"
		+"	        <option value='B99'>B99-其他</option>"
		+"	        <option value='C01'>C01-公司</option>"
		+"	        <option value='C02'>C02-非公司企业</option>"
		+"	        <option value='C99'>C99-其他非金融企业部门</option>"
		+"	        <option value='D01'>D01-住户</option>"
		+"	        <option value='D02'>D02-为住户服务的非营力机构</option>"
		+"	        <option value='E01'>E01-国际组织</option>"
		+"	        <option value='E02'>E02-外国政府</option>"
		+"	        <option value='E03'>E03-境外金融机构</option>"
		+"	        <option value='E04'>E04-境外非金融企业</option>"
		+"	        <option value='E05'>E05-外国居民</option>                                                                                                                    "
        +"        </select>                                                                                                                                                        "
        +"    </td>"                                                                                                                                                                  
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        委托人证件类型:                                                                                                                                                      "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='wtrtype' name='wtrtype' editable=true style='height: 23px; width:173px' >                                             "
        +"        <option value='A01'>A01-统一社会信用代码</option>"
        +"        <option value='A02'>A02-组织机构代码</option>"
        +"        <option value='A03'>A03-其他</option>"
        +"        <option value='B01'>B01-身份证</option>"
        +"        <option value='B02'>B02-户口簿</option>"
        +"        <option value='B03'>B03-护照</option>"
        +"        <option value='B04'>B04-军官证</option>"
        +"        <option value='B05'>B05-士兵证</option>"
        +"        <option value='B06'>B06-港澳居民来往内地通行证</option>"
        +"        <option value='B07'>B07-台湾同胞来往内地通行证</option>"
        +"        <option value='B08'>B08-临时身份证</option>"
        +"        <option value='B09'>B09-外国人居留证</option>"
        +"        <option value='B10'>B10-警官证</option>"
        +"        <option value='B11'>B11-外国人永久居留身份证</option>"
        +"        <option value='B12'>B12-港澳台居民居住证</option>"
        +"        <option value='B99'>B99-其他证件</option>"
        +"        </select>                                                                                                                                                          "
        +"    </td>"                                                                                                                                                                  
        +"    <td align='right'>                                                                                                                                                     "
        +"        委托人证件代码:                                                                                                                                                    "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='wtrcode' name='wtrcode' style='width:173px;' class='easyui-validatebox' maxlength='60' data-options='validType:[\"teshutwo\"]'/>                                            "
        +"    </td>                                                                                                                                                                  "
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        委托人行业:                                                                                                                                                        "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='wtrhy' name='wtrhy' style='height: 23px; width:173px' >                       "
        +"            <c:forEach items='${baseAindustryList}' var='aa'>                                                                                                              "
        +"                <option value='${aa.aindustrycode}'>${aa.aindustrycode}-${aa.aindustryname}</option>                                                                       "
        +"            </c:forEach>                                                                                                                                                   "
        +"        </select>                                                                                                                                                          "
        +"    </td>"                                                                                                                                                                  
        +"    <td align='right'>                                                                                                                                                     "
        +"        委托人地区代码:                                                                                                                                                    "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combotree' id='wtrdqdm' name='wtrdqdm' style='height: 23px; width:173px' data-options=\"url:'XgetAdmindivideJson',valueField:'id',textField:'text',editable:true,value:''\"> "
        +"        </select>                                                                                                                                                          "
        +"    </td>"                                                                                                                                                                  
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        委托人经济成分:                                                                                                                                                "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='wtrjjcf' name='wtrjjcf' editable=true style='height: 23px; width:173px' >                                               "
        +"        	  <option value=''>--请选择--</option>"
        +"            <option value='A01'>A01-国有控股</option>                                                                                                                        "
        +"            <option value='A0101'>A0101-国有相对控股</option>                                                                                                                "
        +"            <option value='A0102'>A0102-国有绝对控股</option>                                                                                                                "
        +"            <option value='A02'>A02-集体控股</option>                                                                                                                        "
        +"            <option value='A0201'>A0201-集体相对控股</option>                                                                                                                "
        +"            <option value='A0202'>A0202-集体绝对控股</option>                                                                                                                "
        +"            <option value='B01'>B01-私人控股</option>                                                                                                                        "
        +"            <option value='B0101'>B0101-私人相对控股</option>                                                                                                                "
        +"            <option value='B0102'>B0102-私人绝对控股</option>                                                                                                                "
        +"            <option value='B02'>B02-港澳台控股</option>                                                                                                                      "
        +"            <option value='B0201'>B0201-港澳台相对控股</option>                                                                                                              "
        +"            <option value='B0202'>B0202-港澳台绝对控股</option>                                                                                                              "
        +"            <option value='B03'>B03-外商控股</option>                                                                                                                        "
        +"            <option value='B0301'>B0301-外商相对控股</option>                                                                                                                "
        +"            <option value='B0302'>B0302-外商绝对控股</option>                                                                                                              "
        +"        </select>                                                                                                                                                          "
        +"    </td>"                                                                                                                                                                 
        +"    <td align='right'>                                                                                                                                                     "
        +"        委托人企业规模:                                                                                                                                                          "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='wtrqygm' name='wtrqygm' editable=true style='height: 23px; width:173px' >                                                   "
        +"        	  <option value=''>--请选择--</option>"
        +"            <option value='CS01'>CS01-大型</option>                                                                                                                        "
        +"            <option value='CS02'>CS02-中型</option>                                                                                                                        "
        +"            <option value='CS03'>CS03-小型</option>                                                                                                                        "
        +"            <option value='CS04'>CS04-微型</option>                                                                                                                        "
        +"            <option value='CS05'>CS05-其他（非企业类单位）</option>                                                                                                        "
        +"        </select>                                                                                                                                                          "
        +"	  </td>"                                                                                                                                                                  
        +"</tr>                                                                                                                                                                      "
        +"                                                                                                                                                                           "
        +"<tr>                                                                                                                                                                       "
        +"    <td align='right'>                                                                                                                                                     "
        +"        发放/收回标识:                                                                                                                                                     "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <select class='easyui-combobox' id='givetakeid' name='givetakeid' editable=true style='height: 23px; width:173px' >                   "
        +"            <option value='0'>0-收回</option>                                                                                                                              "
        +"            <option value='1'>1-发放</option>                                                                                                                              "
        +"        </select>                                                                                                                                                          "
        +"    </td>                                                                                                                                                                  "
        +"    <td align='right'>                                                                                                                                                     "
        +"        贷款用途:                                                                                                                                            "
        +"    </td>                                                                                                                                                                  "
        +"    <td>                                                                                                                                                                   "
        +"        <input id='issupportliveloan' name='issupportliveloan' style='width:173px;' class='easyui-validatebox' maxlength='1000' data-options='validType:[\"teshufive\"]'/>                             "
        +"    </td>"                                                                                                                                                                 
        +"</tr>                                                                                                                                                                      "
        +"<tr>                                                                                                                                                                       "
        +"        <td align='right'>"
        +"        数据日期:"
        +"</td>"
        +"    <td>"
        +"    <input id='sjrq' name='sjrq' style='width:173px;' class='easyui-datebox' data-options='validType:[\"dateFormat\",\"normalDate\"]'/>"
        +"        </td>"                                                                                                                                                             
        +"</tr>                                                                                                                                                                      "
        +"</table>"
    $(function(){
        $('#dialog1').dialog({
            onClose: function () {
                $('.validatebox-tip').remove();
            }
        });
        $("#dg1").datagrid({
            loadMsg:'数据加载中,请稍后...',
            method:'post',
            url:'XGetXwtdkfseData?datastatus='+'${datastatus}',
            checkOnSelect:true,
            autoRowHeight:false,
            pagination:true,
            rownumbers:true,
            toolbar:'#tb1',
            fitColumns:false,
            pageSize:20,
            pageList:[15,20,30,50],
            columns:[[
            	{field:'ck',checkbox:true},
                {field:'id', title: '编号', width: 80, align: 'center', hidden: true},
                {field:'operationname',title:'操作名',width:150,align:'center'},
                {field:'nopassreason',title:'审核不通过原因',width:150,align:'center'},
                {field:'checkstatus',title:'校验结果',width:150,align:'center',formatter:function (value) {
                        if (value=='0'){
                            return "未校验";
                        }else if(value=='1'){
                            return "校验成功";
                        }else if (value=='2'){
                            return '<span style="color:red;">' + '校验失败' + '</span>';
                        }
                        return value;
                    }},
                {field:'finorgcode',title:'金融机构代码',width:150,align:'center'},
                {field:'finorgincode',title:'内部机构号',width:150,align:'center'},
                {field:'finorgareacode',title:'金融机构地区代码',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseAreas.length;i++){
                            if (baseAreas[i].substring(0,6)==value){
                                return baseAreas[i];
                            }
                        }
                    }},
                {field:'isfarmerloan',title:'借款人证件类型',width:150,align:'center',formatter:function (value) {
                    if (value=='A01'){
                        return "A01-统一社会信用代码";
                    }else if(value=='A02'){
                        return "A02-组织机构代码";
                    }else if (value=='A03'){
                        return "A03-其他";
                    }else if (value =='B01'){
                        return "B01-身份证";
                    }else if (value =='B02'){
                        return "B02-户口簿";
                    }else if (value =='B03'){
                        return "B03-护照";
                    }else if (value =='B04'){
                        return "B04-军官证";
                    }else if (value =='B05'){
                        return "B05-士兵证";
                    }else if (value =='B06'){
                        return "B06-港澳居民来往内地通行证";
                    }else if (value =='B07'){
                        return "B07-台湾同胞来往内地通行证";
                    }else if (value =='B08'){
                        return "B08-临时身份证";
                    }else if (value =='B09'){
                        return "B09-外国人居留证";
                    }else if (value =='B010'){
                        return "B10-警官证";
                    }else if (value =='B011'){
                        return "B11-外国人永久居留身份证";
                    } else if (value =='B012'){
                        return "B12-港澳台居民居住证";
                    } else if (value =='B99'){
                        return "B99-其他证件";
                    }
                    return value;
                }},
                {field:'browidcode',title:'借款人证件代码',width:150,align:'center'},
                {field:'isgreenloan',title:'借款人国民经济部门',width:150,align:'center',formatter:function (value) {
                	if (value=='A'){
                		  return "A-广义政府";
                		}else if (value=='A01'){
                		  return "A01-中央政府";
                		}else if (value=='A02'){
                		  return "A02-地方政府";
                		}else if (value=='A03'){
                		  return "A03-社会保障基金";
                		}else if (value=='A04'){
                		  return "A04-机关团体";
                		}else if (value=='A05'){
                		  return "A05-部队";
                		}else if (value=='A06'){
                		  return "A06-住房公积金";
                		}else if (value=='A99'){
                		  return "A99-其他";
                		}else if (value=='B'){
                		  return "B-金融机构部门";
                		}else if (value=='B01'){
                		  return "B01-货币当局";
                		}else if (value=='B02'){
                		  return "B02-监管当局";
                		}else if (value=='B03'){
                		  return "B03-银行业存款类金融机构";
                		}else if (value=='B04'){
                		  return "B04-银行业非存款类金融机构";
                		}else if (value=='B05'){
                		  return "B05-证券业金融机构";
                		}else if (value=='B06'){
                		  return "B06-保险业金融机构";
                		}else if (value=='B07'){
                		  return "B07-交易及结算类金融机构";
                		}else if (value=='B08'){
                		  return "B08-金融控股公司";
                		}else if (value=='B09'){
                		  return "B09-特定目的载体";
                		}else if (value=='B99'){
                		  return "B99-其他";
                		}else if (value=='C'){
                		  return "C-非金融企业部门";
                		}else if (value=='C01'){
                		  return "C01-公司";
                		}else if (value=='C02'){
                		  return "C02-非公司企业";
                		}else if (value=='C99'){
                		  return "C99-其他非金融企业部门";
                		}else if (value=='D'){
                		  return "D-住户部门";
                		}else if (value=='D01'){
                		  return "D01-住户";
                		}else if (value=='D02'){
                		  return "D02-为住户服务的非营利机构";
                		}else if (value=='E'){
                		  return "E-非居民部门";
                		}else if (value=='E01'){
                		  return "E01-国际组织";
                		}else if (value=='E02'){
                		  return "E02-外国政府";
                		}else if (value=='E03'){
                		  return "E03-境外金融机构";
                		}else if (value=='E04'){
                		  return "E04-境外非金融企业";
                		}else if (value=='E05'){
                		  return "E05-外国居民";
                    }
                    return value;
                }},
                {field:'browinds',title:'借款人行业',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseAindustrys.length;i++){
                            if (baseAindustrys[i].substring(0,3)==value){
                                return baseAindustrys[i];
                            }
                        }
                    }},
                {field:'browareacode',title:'借款人地区代码',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseAreaAndCountrys.length;i++){
                            if (baseAreaAndCountrys[i].substring(0,6)==value){
                                return baseAreaAndCountrys[i];
                            }
                        }
                    }},
                {field:'entpczjjcf',title:'借款人经济成分',width:150,align:'center',formatter:function (value) {
                        if (value=='A01'){
                            return "A01-国有控股";
                        }else if(value=='A0101'){
                            return "A0101-国有相对控股";
                        }else if (value=='A0102'){
                            return "A0102-国有绝对控股";
                        }else if(value=='A02'){
                            return "A02-集体控股";
                        }else if (value=='A0201'){
                            return "A0201-集体相对控股";
                        }else if (value=='A0202'){
                            return "A0202-集体绝对控股";
                        }else if(value=='B01'){
                            return "B01-私人控股";
                        }else if (value=='B0101'){
                            return "B0101-私人相对控股";
                        }else if (value=='B0102'){
                            return "B0102-私人绝对控股";
                        }else if(value=='B02'){
                            return "B02-港澳台控股";
                        }else if (value=='B0201'){
                            return "B0201-港澳台相对控股";
                        }else if (value=='B0202'){
                            return "B0202-港澳台绝对控股";
                        }else if(value=='B03'){
                            return "B03-外商控股";
                        }else if (value=='B0301'){
                            return "B0301-外商相对控股";
                        }else if (value=='B0302'){
                            return "B0302-外商绝对控股";
                        }
                        return value;
                    }},
                {field:'entpmode',title:'借款人企业规模',width:150,align:'center',formatter:function (value) {
                        if (value=='CS01'){
                            return "CS01-大型";
                        }else if(value=='CS02'){
                            return "CS02-中型";
                        }else if (value=='CS03'){
                            return "CS03-小型";
                        }else if(value=='CS04'){
                            return "CS04-微型";
                        }else if (value=='CS05'){
                            return "CS05-其他（非企业类单位）";
                        }
                        return value;
                    }},
                {field:'loanbrowcode',title:'委托贷款借据编码',width:150,align:'center'},
                {field:'loancontractcode',title:'委托贷款合同编码',width:150,align:'center'},
                {field:'loanactdect',title:'贷款实际投向',width:150,align:'center',formatter:function (value) {
                    for (var i = 0;i <  baseBindustrys.length;i++){
                        if (baseBindustrys[i].substring(0,4)==value){
                            return baseBindustrys[i];
                        }
                    }
                }},
                {field:'loanstartdate',title:'委托贷款发放日期',width:150,align:'center'},
                {field:'loanenddate',title:'委托贷款到期日期',width:150,align:'center'},
                {field:'transactionnum',title:'交易流水号',width:150,align:'center'},
                {field:'loancurrency',title:'币种',width:150,align:'center',formatter:function (value) {
                    for (var i = 0;i <  baseCurrencys.length;i++){
                        if (baseCurrencys[i].substring(0,3)==value){
                            return baseCurrencys[i];
                        }
                    }
                }},
            {field:'loanamt',title:'委托贷款发生金额',width:150,align:'center'},
            {field:'loancnyamt',title:'委托贷款发生金额折人民币',width:150,align:'center'},
            {field:'rateisfix',title:'利率是否固定',width:150,align:'center',formatter:function (value) {
           		if (value=='RF01'){
                	return "RF01-固定利率";
            	}else if(value=='RF02'){
                	return "RF02-浮动利率";
            	}
           		return value;
        	}},
        	{field:'ratelevel',title:'利率水平',width:150,align:'center'},
        	{field:'sxfcny',title:'手续费金额折人民币',width:150,align:'center'},
        	{field:'gteemethod',title:'贷款担保方式',width:150,align:'center',formatter:function (value) {
                if (value=='A'){
                    return "A-质押贷款";
                }else if (value=='B01'){
                    return "B01-房地产抵押贷款";
                }else if(value=='B99'){
                    return "B99-其他抵押贷款";
                }else if (value=='C01'){
                    return "C01-联保贷款";
                }else if(value=='C99'){
                    return "C99-其他保证贷款";
                }else if (value=='D'){
                    return "D-信用/免担保贷款";
                }else if (value=='E'){
                    return "E-组合担保";
                }else if (value=='Z'){
                    return "Z-其他";
                }
                return value;
            }},
            {field:'wtrgmjjbm',title:'委托人国民经济部门',width:150,align:'center',formatter:function (value) {
            	if (value=='A'){
          		  return "A-广义政府";
          		}else if (value=='A01'){
          		  return "A01-中央政府";
          		}else if (value=='A02'){
          		  return "A02-地方政府";
          		}else if (value=='A03'){
          		  return "A03-社会保障基金";
          		}else if (value=='A04'){
          		  return "A04-机关团体";
          		}else if (value=='A05'){
          		  return "A05-部队";
          		}else if (value=='A06'){
          		  return "A06-住房公积金";
          		}else if (value=='A99'){
          		  return "A99-其他";
          		}else if (value=='B'){
          		  return "B-金融机构部门";
          		}else if (value=='B01'){
          		  return "B01-货币当局";
          		}else if (value=='B02'){
          		  return "B02-监管当局";
          		}else if (value=='B03'){
          		  return "B03-银行业存款类金融机构";
          		}else if (value=='B04'){
          		  return "B04-银行业非存款类金融机构";
          		}else if (value=='B05'){
          		  return "B05-证券业金融机构";
          		}else if (value=='B06'){
          		  return "B06-保险业金融机构";
          		}else if (value=='B07'){
          		  return "B07-交易及结算类金融机构";
          		}else if (value=='B08'){
          		  return "B08-金融控股公司";
          		}else if (value=='B09'){
          		  return "B09-特定目的载体";
          		}else if (value=='B99'){
          		  return "B99-其他";
          		}else if (value=='C'){
          		  return "C-非金融企业部门";
          		}else if (value=='C01'){
          		  return "C01-公司";
          		}else if (value=='C02'){
          		  return "C02-非公司企业";
          		}else if (value=='C99'){
          		  return "C99-其他非金融企业部门";
          		}else if (value=='D'){
          		  return "D-住户部门";
          		}else if (value=='D01'){
          		  return "D01-住户";
          		}else if (value=='D02'){
          		  return "D02-为住户服务的非营利机构";
          		}else if (value=='E'){
          		  return "E-非居民部门";
          		}else if (value=='E01'){
          		  return "E01-国际组织";
          		}else if (value=='E02'){
          		  return "E02-外国政府";
          		}else if (value=='E03'){
          		  return "E03-境外金融机构";
          		}else if (value=='E04'){
          		  return "E04-境外非金融企业";
          		}else if (value=='E05'){
          		  return "E05-外国居民";
              }
                return value;
            }},
            {field:'wtrtype',title:'委托人证件类型',width:150,align:'center',formatter:function (value) {
                if (value=='A01'){
                    return "A01-统一社会信用代码";
                }else if(value=='A02'){
                    return "A02-组织机构代码";
                }else if (value=='A03'){
                    return "A03-其他";
                }else if (value =='B01'){
                    return "B01-身份证";
                }else if (value =='B02'){
                    return "B02-户口簿";
                }else if (value =='B03'){
                    return "B03-护照";
                }else if (value =='B04'){
                    return "B04-军官证";
                }else if (value =='B05'){
                    return "B05-士兵证";
                }else if (value =='B06'){
                    return "B06-港澳居民来往内地通行证";
                }else if (value =='B07'){
                    return "B07-台湾同胞来往内地通行证";
                }else if (value =='B08'){
                    return "B08-临时身份证";
                }else if (value =='B09'){
                    return "B09-外国人居留证";
                }else if (value =='B010'){
                    return "B10-警官证";
                }else if (value =='B011'){
                    return "B11-外国人永久居留身份证";
                } else if (value =='B012'){
                    return "B12-港澳台居民居住证";
                } else if (value =='B99'){
                    return "B99-其他证件";
                }
                return value;
            }},
            {field:'wtrcode',title:'委托人证件代码',width:150,align:'center'},
            {field:'wtrhy',title:'委托人行业',width:150,align:'center',formatter:function (value) {
                for (var i = 0;i <  baseAindustrys.length;i++){
                    if (baseAindustrys[i].substring(0,3)==value){
                        return baseAindustrys[i];
                    }
                }
            }},
        	{field:'wtrdqdm',title:'委托人地区代码',width:150,align:'center',formatter:function (value) {
                for (var i = 0;i <  baseAreaAndCountrys.length;i++){
                    if (baseAreaAndCountrys[i].substring(0,6)==value){
                        return baseAreaAndCountrys[i];
                    }
                }
            }},
            {field:'wtrjjcf',title:'委托人经济成分',width:150,align:'center',formatter:function (value) {
                if (value=='A01'){
                    return "A01-国有控股";
                }else if(value=='A0101'){
                    return "A0101-国有相对控股";
                }else if (value=='A0102'){
                    return "A0102-国有绝对控股";
                }else if(value=='A02'){
                    return "A02-集体控股";
                }else if (value=='A0201'){
                    return "A0201-集体相对控股";
                }else if (value=='A0202'){
                    return "A0202-集体绝对控股";
                }else if(value=='B01'){
                    return "B01-私人控股";
                }else if (value=='B0101'){
                    return "B0101-私人相对控股";
                }else if (value=='B0102'){
                    return "B0102-私人绝对控股";
                }else if(value=='B02'){
                    return "B02-港澳台控股";
                }else if (value=='B0201'){
                    return "B0201-港澳台相对控股";
                }else if (value=='B0202'){
                    return "B0202-港澳台绝对控股";
                }else if(value=='B03'){
                    return "B03-外商控股";
                }else if (value=='B0301'){
                    return "B0301-外商相对控股";
                }else if (value=='B0302'){
                    return "B0302-外商绝对控股";
                }
                return value;
            }},
        {field:'wtrqygm',title:'委托人企业规模',width:150,align:'center',formatter:function (value) {
                if (value=='CS01'){
                    return "CS01-大型";
                }else if(value=='CS02'){
                    return "CS02-中型";
                }else if (value=='CS03'){
                    return "CS03-小型";
                }else if(value=='CS04'){
                    return "CS04-微型";
                }else if (value=='CS05'){
                    return "CS05-其他（非企业类单位）";
                }
                return value;
            }},
            {field:'givetakeid',title:'发放/收回标识',width:150,align:'center',formatter:function (value) {
                if (value=='1'){
                    return "1-发放";
                }else if(value=='0'){
                    return "0-收回";
                }
                return value;
            }},
            {field:'issupportliveloan',title:'贷款用途',width:150,align:'center'},
            {field:'sjrq',title:'数据日期',width:150,align:'center'},
                {field: 'operator', title: '操作人',  align: 'center'},
                {field: 'operationtime', title: '操作时间',  align: 'center'}
    	    ]],
            onDblClickRow: function (rowIndex,rowData) {
                //$("#tbForAddDialog").hide();//隐藏div
                //disableOcx();
                $('#info_formForAdd').form('clear');
                $('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','委托贷款发生额信息详情');
                $('#info_formForAdd').form('load',rowData);
            }
        });
        $("#formForAdd").append(tablelazyload);
        $.parser.parse($("#enterTable").parent());
        $(".combo").click(function (e) {
            if (e.target.className == 'combo-text validatebox-text' || e.target.className == 'combo-text validatebox-text validatebox-invalid'){
                if ($(this).prev().combobox("panel").is(":visible")) {
                    if (!$(this).prev().prop("disabled")){
                        $(this).prev().combobox("hidePanel");
                    }
                } else {
                    if (!$(this).prev().prop("disabled")){
                        $(this).prev().combobox("showPanel");
                    }
                }
            }
        });

        /*
        $("#loanprocode").combobox({
            onChange: function (n) {
                if (n != 'F04' && n != 'F05'){
                    $('#loanenddate').datebox({required: true});
                    $('#loanstartdate').datebox({required: true});
                    $('#extensiondate').datebox({required: true});
                }else {
                    $('#loanenddate').val('');
                    $('#loanenddate').datebox({required: false});
                    $('#loanstartdate').val('');
                    $('#loanstartdate').datebox({required: false});
                    $('#extensiondate').val('');
                    $('#extensiondate').datebox({required: false});
                }
            }
        });
        $("#loancurrency").combobox({
            onChange: function (n) {
                if (n != 'CNY'){
                    $('#loancnyamt').validatebox({required: true});
                }else {
                    $('#loancnyamt').val('');
                    $('#loancnyamt').validatebox({required: false});
                }
            }
        });
        */
        /*$("#loanstatus").combobox({
            onChange: function (n) {
                if (n == 'FS03'){
                    $('#receiptcnybalance').datebox({required: true});
                }else {
                    $('#overduetype').val('');
                    $('#overduetype').datebox({required: false});
                }
            }
        });*/
    })

    function disableOcx() {
        var form = document.forms[0];
        for ( var i = 0; i < form.length; i++) {
            var element = form.elements[i];
            element.disabled = "true";
        }
    }

    function noDisable() {
        var form = document.forms[0];
        for ( var i = 0; i < form.length; i++) {
            var element = form.elements[i];
            element.disabled = false;
        }
    }

    // 查询
    function searchOnSelected(value){
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var loanbrowcodeParam = $("#loanbrowcodeParam").val().trim();

        var browidcodeParam = $("#browidcodeParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = '';
        if (value == 'dsh'){
            operationnameParam = $("#operationnameParam").combobox("getValue");
        }
        $("#dg1").datagrid("load", {"finorgcodeParam" : finorgcodeParam,"loancontractcodeParam" :loancontractcodeParam,"loanbrowcodeParam":loanbrowcodeParam,"browidcodeParam":browidcodeParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam});
    }

    // 新增
    function add(){
        resetForAdd();
        $('#dialog1').dialog('open').dialog('center').dialog('setTitle','新增委托贷款发生额信息');
        keydownsearch();
    }

    // 修改
    function edit(){
        var rows = $('#dg1').datagrid('getSelections');
        if(rows.length == 1){
            resetForAdd();
            $("#formForAdd").form("load", rows[0]);
            $('#dialog1').dialog('open').dialog('center').dialog('setTitle','修改委托贷款发生额信息');
            keydownsearch();
        }else{
            $.messager.alert("提示", "请选择一条数据进行修改!", "info");
        }
    }

    // 保存
    function save() {
        if($("#formForAdd").form('validate')){
            $.ajax({
                type: "POST",//为post请求
                url: "XsaveXwtdkfse",//这是我在后台接受数据的文件名
                data: $('#formForAdd').serialize(),//将该表单序列化
                dataType: "json",
                async: false,
                success: function (res) {
                    if (res.status == "1") {
                        $.messager.alert('提示', '提交成功!', 'info');
                        $("#dialog1").dialog("close");
                        $("#dg1").datagrid("reload");
                    } else {
                        $.messager.alert('提示', '提交失败!', 'info');
                    }
                },
                error: function (err) {//请求失败之后的操作

                }
            });
        }
    }

    // 重置
    function resetForAdd(){
        $("#tbForAddDialog").show();//显示div
        noDisable();
        $("#finorgcode").attr("readOnly",false);
        $("#finorgincode").attr("readOnly",false);
        $("#browidcode").attr("readOnly",false);
        $("#brroweridnum").attr("readOnly",false);
        $("#browinds").combobox({disabled: false});
        // $("#browareacode").combobox({disabled: false});
        $("#entpczjjcf").combobox({disabled: false});
        $("#entpmode").combobox({disabled: false});
        $('#formForAdd').form('clear');
    }

    // 取消
    function cancel(){
        $('#dialog1').dialog('close');
    }

    // 删除
    function deleteit(){
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        if(rows.length > 0){
            info = '是否删除所选中的数据？';
        }else {
            info = '您没有选择数据，是否删除所有数据？';
        }
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var loanbrowcodeParam = $("#loanbrowcodeParam").val().trim();

        var browidcodeParam = $("#browidcodeParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XDeleteXwtdkfse",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"loancontractcodeParam" : loancontractcodeParam,"loanbrowcodeParam":loanbrowcodeParam,"browidcodeParam":browidcodeParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','删除成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','删除失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','删除失败','error');
                    }
                });
                $("#dg1").datagrid("reload");
            }
        });
    }

    // 导入Excel相关
    function importExcel(){
        document.getElementById("importbtn").disabled = "disabled";
        $('#excelfile').val('');
        $('#importExcel').dialog('open').dialog('center');
    }

    function inexcel(){
        var file = document.getElementById("excelfile").files[0];
        if(file == null){
            document.getElementById("importbtn").disabled = "disabled";
        }else{
            var fileName = file.name;
            var fileType = fileName.substring(fileName.lastIndexOf('.'),
                fileName.length);
            if (fileType == '.xls' || fileType == '.xlsx'){
                if (file) {
                    document.getElementById("importbtn").disabled = "";
                }
            } else {
                $.messager.alert('提示', "导入文件应该是.xls或者.xlsx为后缀,而不是" + fileType + ",请重新选择文件！","error");
                document.getElementById("importbtn").disabled = "disabled";
            }
        }
    }

    function excel(){
        $.messager.progress({
            title: '请稍等',
            msg: '数据正在导入中......'
        });
        $.ajaxFileUpload({
            type: "post",
            url: 'XimmportExcelwtdkfse',
            fileElementId: 'excelfile',
            secureuri: false,
            dataType: 'json',
            success: function (data) {
                const that = data.msg;
                $.messager.progress('close');
                if (that == "导入成功") {
                    $.messager.alert('提示', "导入成功", 'info');
                    $("#dg1").datagrid('reload');
                    $('#importExcel').dialog('close');
                } else if (that.startsWith("导入模板不正确")) {
                    $.messager.alert('提示', that, 'error');
                } else {
                    $.messager.show({
                        title: '导入反馈',
                        msg: "<div style='overflow-y:scroll;height:100%'>" + escape2Html(that) + "</div>",
                        timeout: 0,
                        showType: 'show',
                        width: 600,
                        height: 700,
                        style: {
                            right: '100',
                            top: document.body.scrollTop + document.documentElement.scrollTop
                        }
                    });
                }
            },
            error: function () {
                $.messager.progress('close');
                $.messager.alert('提示', '导入文件错误，请重新导入', 'error');
            }
        });
    }

    // 导出
    function showOut(model){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        var info = '';
        if (model){
            id = '00000000';
            info = '是否导出模板？';
        }else {
            for( var dataIndex in row){
                id = id + row[dataIndex].id + ",";
            }
            if(id != ''){
                info = '是否将选中的数据导出到Excel表中？';
            }else {
                info = '是否将全部数据导出到Excel表中？'
            }
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var loanbrowcodeParam = $("#loanbrowcodeParam").val().trim();
        var browidcodeParam = $("#browidcodeParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");

        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('操作提示', info, function (r) {
            if (r) {
                window.location.href = "XExportXwtdkfse?finorgcodeParam="+finorgcodeParam+"&loancontractcodeParam="+loancontractcodeParam+"&loanbrowcodeParam="+loanbrowcodeParam+"&browidcodeParam="+browidcodeParam+"&isfarmerloanParam="+isfarmerloanParam+"&checkstatusParam="+checkstatusParam+"&id="+id + "&datastatus="+'${datastatus}';
            }
        });
    }

    // 校验
    function check(){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        for( var dataIndex in row){
            id = id + row[dataIndex].id + ",";
            if (row[dataIndex].checkstatus != 0){
                $.messager.alert("提示", "只能校验未校验的数据", "info");
                return;
            }
        }
        var info = '';
        if(id != ''){
            info = '是否校验选中的数据？';
        }else {
            info = '是否校验所有数据？'
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var loanbrowcodeParam = $("#loanbrowcodeParam").val().trim();
        var browidcodeParam = $("#browidcodeParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");

        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r) {
            if (r) {
                $.messager.progress({
                    title: '请稍等',
                    msg: '正在校验数据中......',
                    timeout: 100000,
                });
                $.ajax({
                    url: "XCheckDataXwtdkfse",
                    dataType: "json",
                    async: true,
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"loancontractcodeParam" : loancontractcodeParam,"loanbrowcodeParam":loanbrowcodeParam,"browidcodeParam":browidcodeParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam},
                    type: "POST",
                    success: function (data) {
                        $("#dg1").datagrid("reload");
                        $.messager.progress('close');
                        if (data.msg == "1") {
                            $.messager.alert('提示', '校验成功！', 'info');
                        }else if (data.msg == "-1") {
                            $.messager.alert('提示', '没有可校验的数据！', 'info');
                        } else {
                            $.messager.alert('提示', '校验完成,将自动下载校验结果', 'info', function (r) {
                                window.location.href = "XDownLoadCheckXwtdkfse";
                            });
                        }
                    },
                    error: function () {
                        $.messager.progress('close');
                        $.messager.alert("提示", "校验出错，请重新校验！", "error");
                    }
                });
            }
        })
    }

    // 提交
    function submitit(){
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            for(var i = 0;i < rows.length;i++){
                if(rows[i].checkstatus != '1'){
                    $.messager.alert("提示", "只能提交校验成功的数据", "info");
                    return;
                }
                id = id + rows[i].id + ',';
            }
            info = '是否提交所选中的数据？';
        }else {
            info = '您没有选择数据，是否提交所有数据？';
        }
        $.messager.confirm('提示',info,function (r){
            if(r){
                var finorgcodeParam = $("#finorgcodeParam").val().trim();
                var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
                var loanbrowcodeParam = $("#loanbrowcodeParam").val().trim();
                var browidcodeParam = $("#browidcodeParam").val().trim();
                var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");

                var checkstatusParam = $("#checkstatusParam").combobox("getValue");
                $.messager.confirm('提示',info,function (r){
                    if(r){
                        $.ajax({
                            sync: true,
                            type: "POST",
                            dataType: "json",
                            url: "XsubmitXwtdkfse",
                            data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"loancontractcodeParam" : loancontractcodeParam,"loanbrowcodeParam":loanbrowcodeParam,"browidcodeParam":browidcodeParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                            success: function (data) {
                                if (data == 0){
                                    $.messager.alert('操作提示','提交成功','info');
                                    $("#dg1").datagrid("reload");
                                }else {
                                    $.messager.alert('操作提示','提交失败','info');
                                }
                            },
                            error: function (err) {
                                $.messager.alert('操作提示','提交失败','error');
                            }
                        });
                        $("#dg1").datagrid("reload");
                    }
                });
            }
        });
    }

    //申请删除dsh
    function applyDelete() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            info = '是否申请删除所选中的数据？';
        }else {
            info = '是否申请删除所有数据？';
        }
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var loanbrowcodeParam = $("#loanbrowcodeParam").val().trim();

        var browidcodeParam = $("#browidcodeParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XApplyDeleteXwtdkfse",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"loancontractcodeParam" : loancontractcodeParam,"loanbrowcodeParam":loanbrowcodeParam,"browidcodeParam":browidcodeParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','申请删除成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','申请删除失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','申请删除失败','error');
                    }
                });
            }
        });
    }

    //申请修改
    function applyEdit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        if(rows.length > 0){
            info = '是否申请修改所选中的数据？';
        }else {
            info = '是否申请修改所有数据？';
        }
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var loanbrowcodeParam = $("#loanbrowcodeParam").val().trim();

        var browidcodeParam = $("#browidcodeParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XApplyEditXwtdkfse",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"loancontractcodeParam" : loancontractcodeParam,"loanbrowcodeParam":loanbrowcodeParam,"browidcodeParam":browidcodeParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"datastatus":'${datastatus}'},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','申请修改成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','申请修改失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','申请修改失败','error');
                    }
                });
            }
        });
    }

    /*同意申请*/
    function agreeApply() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
                return;
            }
            if (rows[i].operationname != '申请删除'){
                $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否同意所选中的申请？';
        }else {
            info = '是否同意所有的申请？';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var loanbrowcodeParam = $("#loanbrowcodeParam").val().trim();
        var browidcodeParam = $("#browidcodeParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XAgreeApplyXwtdkfse",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"loancontractcodeParam" : loancontractcodeParam,"loanbrowcodeParam":loanbrowcodeParam,"browidcodeParam":browidcodeParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','同意成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','同意失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','同意失败','error');
                    }
                });
            }
        });
    }

    /*拒绝申请*/
    function noAgreeApply() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
                return;
            }
            if (rows[i].operationname != '申请删除'){
                $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否拒绝所选中的申请？';
        }else {
            info = '是否拒绝所有的申请？';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var loanbrowcodeParam = $("#loanbrowcodeParam").val().trim();
        var browidcodeParam = $("#browidcodeParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");

        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XNoAgreeApplyXwtdkfse",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"loancontractcodeParam" : loancontractcodeParam,"loanbrowcodeParam":loanbrowcodeParam,"browidcodeParam":browidcodeParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','拒绝成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','拒绝失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','拒绝意失败','error');
                    }
                });
            }
        });
    }

    /*审核通过*/
    function agreeAudit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
                return;
            }
            if (rows[i].checkstatus != 1){
                $.messager.alert("提示","请选择校验通过的数据！","error");
                return;
            }
            if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
            }else {
                $.messager.alert("提示","请选择操作名为空的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否审核通过所选中的数据？';
        }else {
            info = '是否审核通过所有数据？';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var loanbrowcodeParam = $("#loanbrowcodeParam").val().trim();
        var browidcodeParam = $("#browidcodeParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.confirm('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XAgreeAuditXwtdkfse",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"loancontractcodeParam" : loancontractcodeParam,"loanbrowcodeParam":loanbrowcodeParam,"browidcodeParam":browidcodeParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','审核通过成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','审核通过失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','审核通过失败','error');
                    }
                });
            }
        });
    }

    /*审核不通过*/
    function noAgreeAudit() {
        var rows = $('#dg1').datagrid('getSelections');
        var info = '';
        var id = '';
        for (var i = 0;i < rows.length;i++){
            id = id + rows[i].id + ',';
            if(handlename == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
                $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
                return;
            }
            if (rows[i].checkstatus != 1){
                $.messager.alert("提示","请选择校验通过的数据！","error");
                return;
            }
            if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
            }else {
                $.messager.alert("提示","请选择操作名为空的数据！","error");
                return;
            }
        }
        if(rows.length > 0){
            info = '是否审核不通过所选中的数据？';
        }else {
            info = '是否审核不通过所有数据？';
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
        var loanbrowcodeParam = $("#loanbrowcodeParam").val().trim();
        var browidcodeParam = $("#browidcodeParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var checkstatusParam = $("#checkstatusParam").combobox("getValue");
        var operationnameParam = $("#operationnameParam").combobox("getValue");
        $.messager.prompt('提示',info,function (r){
            if(r){
                $.ajax({
                    sync: true,
                    type: "POST",
                    dataType: "json",
                    url: "XNoAgreeAuditXwtdkfse",
                    data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"loancontractcodeParam" : loancontractcodeParam,"loanbrowcodeParam":loanbrowcodeParam,"browidcodeParam":browidcodeParam,"isfarmerloanParam":isfarmerloanParam,"checkstatusParam" : checkstatusParam,"operationnameParam" : operationnameParam,"reason":r},
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function (data) {
                        if (data == 0){
                            $.messager.alert('操作提示','审核不通过成功','info');
                            $("#dg1").datagrid("reload");
                        }else {
                            $.messager.alert('操作提示','审核不通过失败','info');
                        }
                    },
                    error: function (err) {
                        $.messager.alert('操作提示','审核不通过失败','error');
                    }
                });
            }else {
                $.messager.alert('操作提示','审核不通过原因必填','info');
            }
        });
    }

    //前台校验
    $.extend($.fn.validatebox.defaults.rules, {
    	dateFormat : {
            validator : function(value) {
            	var reg = /^(\d{4})-(\d{2})-(\d{2})$/;
                return reg.test(value);
            },
            message : '日期格式必须满足:YYYY-MM-DD。'
        },
        normalDate : {
            validator : function(value) {
                var startTmp = new Date('1800-01-01');
                var endTmp = new Date('2100-12-31');
                var nowTmp = new Date(value);
                return nowTmp > startTmp && nowTmp < endTmp;
            },
            message : '贷款到期日期早于1800-01-01且晚于2100-12-31'
        },
        compareToEnd : {
            validator : function(value) {
            	var loanstartdate = $('#loanstartdate').datebox('getValue');
                if (loanstartdate == null || loanstartdate == ''){
                    return true;
                }
                var startTmp = new Date(loanstartdate);
                var nowTmp = new Date(value);
                return nowTmp >= startTmp;
            },
            message : '贷款发放日期应小于等于贷款到期日期'
        },
        compareToStart : {
            validator : function(value) {
            	if(value != null){
            		var loanstartdate = $('#loanstartdate').datebox('getValue');
                    if (loanstartdate == null || loanstartdate == ''){
                        return true;
                    }
                    var startTmp = new Date(loanstartdate);
                    var nowTmp = new Date(value);
                    return nowTmp >= startTmp;
            	}else{
            		return true;
            	}
            },
            message : '当贷款实际终止日期不为空时，贷款实际终止日期应大于等于贷款发放日期'
        },
        compareToStartone : {
            validator : function(value) {
                var loanstartdate = $('#loanstartdate').datebox('getValue');
                if (loanstartdate == null || loanstartdate == ''){
                    return true;
                }
                var startTmp = new Date(loanstartdate);
                var nowTmp = new Date(value);
                return nowTmp >= startTmp;
            },
            message : '贷款利率重新定价日应大于等于贷款发放日期'
        },
        teshuone:{
            validator: function (value) {
                var patrn = /[？?！!^]/;
                return !patrn.test(value);
            },
            message: '不能包含特殊符号和空格'
        },
        teshutwo:{
            validator: function (value) {
                var patrn = /[？?！!^]/;
                return !patrn.test(value);
            },
            message: '不能包含特殊符号和空格'
        },
        teshufive:{
            validator: function (value) {
                var patrn = /[？?！!^]/;
                return !patrn.test(value);
            },
            message: '不能包含特殊符号'
        },
        amount : {
            validator : function(value) {
                var reg = /^([0-9]\d{0,16}(\.\d{2})|0\.\d{2})$/;
                return reg.test(value);
            },
            message : '总长度不超过20位的，精度保留小数点后两位。'
        },
        baserate : {
            validator : function(value) {
                if (value.indexOf("%") == -1 && value.indexOf("‰") == -1){
                    return true
                }
                return false;
            },
            message : '基准利率不能包含‰或%'
        },
        ratelevel : {
            validator : function(value) {
                var reg = /^([0-9]\d{0,3}(\.\d{5})|0\.\d{5})$/;
                return reg.test(value);
            },
            message : '利率水平，总长度不能超过10位，小数位应保留5位'
        }
    });
    
    function keydownsearch(){
	    //combobox可编辑，自定义模糊查询
	    $.fn.combobox.defaults.editable = true;
	    $.fn.combobox.defaults.filter = function(q, row){
	        var opts = $(this).combobox('options');
	        return row[opts.textField].indexOf(q) >= 0;
	    };
    }
</script>
</body>
</html>
