<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">
    <title>股权投资发生额信息</title>
    <%@include file="../../common/head.jsp"%>
</head>
<body>

<table id="dg1" style="height: 480px;" title="股权投资发生额信息列表"></table>
<!-- 日期选择框 -->
<div id="datedialog" class="easyui-dialog" title="日期选择" style="width:400px;height:200px;padding-left:30px;padding-top:50px" data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="dateform" action="">
        请选择报文日期：<input id="dateselect" type="text" class="easyui-datebox" required="required"><br/><br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-ok" onclick="dataBack('XgqtzfsexxH')">确定</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#datedialog').window('close')">取消</a>
    </form>
</div>

<!--表格工具栏-->
<div id="tb1" style="padding: 5px; height: auto;">
    <a class="easyui-linkbutton" href="XHistoryDataUi" iconCls="icon-return">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <label>金融机构代码：</label><input id="financeorgcodeParam" type="text" style="height: 23px; width:160px"/>
    <label>数据日期：</label><input id="operationtimeParam" class='easyui-datebox' style="height: 23px; width:160px"/>&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;<br>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="hisDownloadExcel('XgqtzfsexxH','3')">导出</a>&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-edit" onclick="javascript:$('#datedialog').window('open')">数据打回</a>
</div>


<!--详情窗口-->
<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <!-- 信息录入 -->
    <div align="center" style="padding-top: 30px">
        <form id="info_formForAdd" method="post">
            <input id="info_id" class="backId" name="id" type="hidden">
            <table class='enterTable' id='info_enterTable'>
                <tr> <td align='right'>
                    金融机构代码:</td>
                    <td>
                        <input id='financeorgcode' name='financeorgcode' disabled='disabled' style='width:173px;' />
                    </td>
                    <td align='right'>
                        内部机构号:
                    </td>
                    <td>
                        <input id='financeorginnum' name='financeorginnum'  disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
                <tr> <td align='right'>
                    凭证编码:</td>
                    <td>
                        <input id='pzbm' name='pzbm' disabled='disabled' style='width:173px;' />
                    </td>
                    <td align='right'>
                        股权类型:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='gqlx' name='gqlx'  disabled='disabled' style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=9',
				                method: 'get',
				                valueField:'code',
                                textField:'text'">
                        </select>
                    </td>
                </tr>
                <tr> <td align='right'>
                    机构类型:</td>
                    <td>
                        <select class='easyui-combobox' id='jglx' name='jglx' disabled='disabled' style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=10',
				                method: 'get',
				                valueField:'code',
                                textField:'text'">
                        </select>
                    </td>
                    <td align='right'>
                        机构证件代码:
                    </td>
                    <td>
                        <input id='jgzjdm' name='jgzjdm' disabled='disabled'  style='width:173px;'/>
                    </td>
                </tr>
                <tr> <td align='right'>
                    地区代码:</td>
                    <td>
                        <select class='easyui-combobox' id='dqdm' name='dqdm' disabled="disabled"  style='width:173px;'>
                            <c:forEach items='${baseCountryList}' var='aa'>
                                <option value='${aa.countrycode}'>${aa.countrycode}-${aa.countryname}</option>
                            </c:forEach>
                            <c:forEach items='${baseAreaList}' var='aa'>
                                <option value='${aa.areacode}'>${aa.areacode}-${aa.areaname}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td align='right'>
                        交易日期 :
                    </td>
                    <td>
                        <input class='easyui-datebox' id='jyrq' name='jyrq' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
                <tr> <td align='right'>
                    币种:</td>
                    <td>
                        <select class='easyui-combobox' id='bz' name='bz'  disabled='disabled' style='width:173px;'>
                            <c:forEach items='${baseCurrencyList}' var='aa'>
                                <option value='${aa.currencycode}'>${aa.currencycode}-${aa.currencyname}</option>
                            </c:forEach>
                        </select>                    </td>
                    <td align='right'>
                        交易金额:
                    </td>
                    <td>
                        <input id='jyje' name='jyje' disabled='disabled'  style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        交易金额折人民币:
                    </td>
                    <td>
                        <input id='jyjezrmb' name='jyjezrmb' disabled='disabled'  style='width:173px;'/>
                    </td>
                    <td align='right'>
                        交易方向:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='jyfx' name='jyfx' disabled='disabled' style='width:173px;'
                                data-options="
				                url: 'queryDataDictionary?type=11',
				                method: 'get',
				                valueField:'code',
                                textField:'text'">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        数据日期 :
                    </td>
                    <td>
                        <input class='easyui-datebox' id='sjrq' name='sjrq' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script type="text/javascript">
    var tablelazyload = null;
    var baseCurrencys = ${baseCurrencys};
    var baseCountrys = ${baseCountrys};
    var baseAreas = ${baseAreas};
    var baseAindustrys = ${baseAindustrys};
    $(function(){
        $('#datedialog').window('close');
        $("#dg1").datagrid({
            loadMsg:'数据加载中,请稍后...',
            method:'POST',
            url:'XGetHistory'+"XgqtzfsexxH",
            checkOnSelect:true,
            autoRowHeight:false,
            pagination:true,
            rownumbers:true,
            toolbar:'#tb1',
            fitColumns:false,
            pageSize:20,
            pageList:[15,20,30,50],
            columns:[[
                {field:'ck',checkbox:true},
                {field:'id', title: '编号', width: 80, align: 'center', hidden: true},
                {field:'operationname',title:'操作名',width:150,align:'center'},
                {field:'nopassreason',title:'审核不通过原因',width:150,align:'center'},
                {field:'checkstatus',title:'校验结果',width:150,align:'center',formatter:function (value) {
                        if (value=='0'){
                            return "未校验";
                        }else if(value=='1'){
                            return "校验成功";
                        }else if (value=='2'){
                            return '<span style="color:red;">' + '校验失败' + '</span>';
                        }
                        return value;
                    }},
                {field:'financeorgcode',title:'金融机构代码',width:150,align:'center'},
                {field:'financeorginnum',title:'内部机构号',width:150,align:'center'},

                {field:'pzbm',title:'凭证编码',width:150,align:'center'},
                {field:'gqlx',title:'股权类型',width:150,align:'center',formatter:function (value) {
                        return  ${gqlxData}[value];
                    }},
                {field:'jglx',title:'机构类型',width:150,align:'center',formatter:function (value) {
                        return  ${jglxData}[value];
                    }},
                {field:'jgzjdm',title:'机构证件代码',width:150,align:'center'},
                {field:'dqdm',title:'地区代码',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseCountrys.length;i++){
                            if (baseCountrys[i].countrycode == value){
                                return baseCountrys[i].countryname;
                            }
                        }
                        for (var i = 0;i <  baseAreas.length;i++){
                            if (baseAreas[i].areacode == value){
                                return baseAreas[i].areaname;
                            }
                        }
                    }},
                {field:'jyrq',title:'交易日期',width:150,align:'center'},
                {field: 'bz', title: '币种', width: 150, align: 'center', formatter: function (value) {
                        for (var i = 0; i < baseCurrencys.length; i++) {
                            if (baseCurrencys[i].currencycode == value) {
                                return baseCurrencys[i].currencyname;
                            }
                        }
                    }
                },
                {field:'jyje',title:'交易金额',width:150,align:'center'},
                {field:'jyjezrmb',title:'交易金额折人民币',width:150,align:'center'},
                {field:'jyfx',title:'交易方向',width:150,align:'center',formatter:function (value) {
                        return  ${jyfxData}[value];
                    }},

                {field:'sjrq',title:'数据日期',width:150,align:'center'},
                {field: 'operator', title: '操作人',  align: 'center'},
                {field: 'operationtime', title: '操作时间',  align: 'center'}
            ]],
            onDblClickRow: function (rowIndex,rowData) {
                $('#info_formForAdd').form('clear');
                $('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','存量贷款信息详情');
                $('#info_formForAdd').form('load',rowData);
            }
        });
    });
    //查询
    function searchOnSelected(){
        //refreshDatagrid({});
        //refreshDatagrid({});
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var operationtimeParam = $("#operationtimeParam").datebox("getValue");
        $("#dg1").datagrid("load", {"financeorgcodeParam" : financeorgcodeParam,"operationtimeParam" : operationtimeParam});
    }
</script>
</body>
</html>
