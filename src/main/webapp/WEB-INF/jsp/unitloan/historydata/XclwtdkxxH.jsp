<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String operationtimeParam = (String) session.getAttribute("operationtimeParam");
    if (StringUtils.isBlank(operationtimeParam)){
        operationtimeParam = "";
    }
    List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
    List<String> baseAreas = new ArrayList<>();
    for (BaseArea baseArea : baseAreaList) {
        baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
    }

    List<BaseCountry> baseCountryList = (List<BaseCountry>) request.getAttribute("baseCountryList");
    List<String>  baseCountrys = new ArrayList<>();
    for (BaseCountry baseCountry : baseCountryList) {
        baseCountrys.add("'"+baseCountry.getCountrycode()+ "-" +baseCountry.getCountryname()+"'");
    }
    List<String>  baseAreaAndCountrys = new ArrayList<>();
    baseAreaAndCountrys.addAll(baseAreas);
    baseAreaAndCountrys.addAll(baseCountrys);
    List<BaseAindustry> baseAindustryList = (List<BaseAindustry>) request.getAttribute("baseAindustryList");
    List<String> baseAindustrys = new ArrayList<>();
    for (BaseAindustry baseAindustry : baseAindustryList) {
        baseAindustrys.add("'"+baseAindustry.getAindustrycode()+ "-" +baseAindustry.getAindustryname()+"'");
    }
    List<BaseBindustry> baseBindustryList = (List<BaseBindustry>) request.getAttribute("baseBindustryList");
    List<String> baseBindustrys = new ArrayList<>();
    for (BaseBindustry baseBindustry : baseBindustryList) {
        baseBindustrys.add("'"+baseBindustry.getBindustrycode()+ "-" +baseBindustry.getBindustryname()+"'");
    }
    List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
    List<String> baseCurrencys = new ArrayList<>();
    for (BaseCurrency baseCurrency : baseCurrencyList) {
        baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>存量委托贷款基础数据信息</title>
    <%@include file="../../common/head.jsp"%>
</head>
<body>

<table id="dg1" style="height: 480px;" title="存量委托贷款基础数据信息列表"></table>
<!-- 日期选择框 -->
<div id="datedialog" class="easyui-dialog" title="日期选择" style="width:400px;height:200px;padding-left:30px;padding-top:50px" data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="dateform" action="">
        请选择报文日期：<input id="dateselect" type="text" class="easyui-datebox" required="required"><br/><br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-ok" onclick="dataBack()">确定</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#datedialog').window('close')">取消</a>
    </form>
</div>

<!--表格工具栏-->
<div id="tb1" style="padding: 5px; height: auto;">
    <a class="easyui-linkbutton" href="XHistoryDataUi" iconCls="icon-return">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <label>金融机构代码：</label><input id="financeorgcodeParam" type="text" style="height: 23px; width:160px"/>&nbsp;
    <label>委托贷款合同编码：</label><input id="contractcodeParam" type="text" style="height: 23px; width:160px"/>&nbsp;
    <label>委托贷款借据编码：</label><input id="receiptcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>借款人证件代码：</label><input id="brroweridnumParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>借款人证件类型：</label>
    <select class='easyui-combobox'  id="isfarmerloanParam" name="isfarmerloanParam" editable=false style="height: 23px; width:130px">
        <option value=''>=请选择=</option>
        <option value='A01'>A01-统一社会信用代码</option>
        <option value='A02'>A02-组织机构代码</option>
        <option value='A03'>A03-其他</option>
        <option value='B01'>B01-身份证</option>
        <option value='B02'>B02-户口簿</option>
        <option value='B03'>B03-护照</option>
        <option value='B04'>B04-军官证</option>
        <option value='B05'>B05-士兵证</option>
        <option value='B06'>B06-港澳居民来往内地通行证</option>
        <option value='B07'>B07-台湾同胞来往内地通行证</option>
        <option value='B08'>B08-临时身份证</option>
        <option value='B09'>B09-外国人居留证</option>
        <option value='B10'>B10-警官证</option>
        <option value='B11'>B11-外国人永久居留身份证</option>
        <option value='B12'>B12-港澳台居民居住证</option>
        <option value='B99'>B99-其他证件</option>
    </select>
    &nbsp;
    <label>数据日期：</label><input id="operationtimeParam" class='easyui-datebox' style="height: 23px; width:160px"/>&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;<br>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut()">导出</a>&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-edit" onclick="javascript:$('#datedialog').window('open')">数据打回</a>
</div>

<!--详情窗口-->
<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <!-- 信息录入 -->
    <div align="center" style="padding-top: 30px">
        <form id="info_formForAdd" method="post">
            <input id="info_id" class="backId" name="id" type="hidden">
            <table class='enterTable' id='info_enterTable'>
                <tr>
                    <td align='right'>
                        数据日期:
                    </td>
                    <td>
                        <input id='info_sjrq' name='sjrq' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr> <td align='right'>
                    金融机构代码:</td>
                    <td>
                        <input id='info_financeorgcode' name='financeorgcode' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        内部机构号:
                    </td>
                    <td>
                        <input id='info_financeorginnum' name='financeorginnum' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        金融机构地区代码:
                    </td>
                    <td>
                        <input id='info_financeorgareacode' name='financeorgareacode' disabled="disabled" style=' height: 23px; width:173px'>
                    </td>
                    <td align='right'>
                        借款人证件代码:
                    </td>
                    <td>
                        <input id='info_brroweridnum' name='brroweridnum' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>

                <tr>
                    <td align='right'>
                        借款人证件类型:
                    </td>
                    <td>
                        <select id='info_isfarmerloan' name='isfarmerloan' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='A01'>A01-统一社会信用代码</option>"
                            <option value='A02'>A02-组织机构代码</option>"
                            <option value='A03'>A03-资管产品统计编码</option>"
                            <option value='A04'>A04-资管产品登记备案编码</option>"
                            <option value='A99'>A99-其他</option>"
                            <option value='B01'>B01-身份证</option>"
                            <option value='B02'>B02-户口簿</option>"
                            <option value='B03'>B03-护照</option>"
                            <option value='B04'>B04-军官证</option>"
                            <option value='B05'>B05-士兵证</option>"
                            <option value='B06'>B06-港澳居民来往内地通行证</option>"
                            <option value='B07'>B07-台湾同胞来往内地通行证</option>"
                            <option value='B08'>B08-临时身份证</option>"
                            <option value='B09'>B09-外国人居留证</option>"
                            <option value='B10'>B10-警官证</option>"
                            <option value='B11'>B11-外国人永久居留身份证</option>"
                            <option value='B12'>B12-港澳台居民居住证</option>"
                            <option value='B99'>B99-其他证件</option>"
                        </select>
                    </td>
                    <td align='right'>
                        借款人国民经济部门:
                    </td>
                    <td>
                        <select id='info_isgreenloan' name='isgreenloan' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='A01'>A01-中央政府</option>
                            <option value='A02'>A02-地方政府</option>
                            <option value='A03'>A03-社会保障基金</option>
                            <option value='A04'>A04-机关团体</option>
                            <option value='A05'>A05-部队</option>
                            <option value='A06'>A06-住房公积金</option>
                            <option value='A99'>A99-其他</option>
                            <option value='B01'>B01-货币当局</option>
                            <option value='B02'>B02-监管当局</option>
                            <option value='B03'>B03-银行业存款类金融机构</option>
                            <option value='B04'>B04-银行业非存款类金融机构</option>
                            <option value='B05'>B05-证券业金融机构</option>
                            <option value='B06'>B06-保险业金融机构</option>
                            <option value='B07'>B07-交易及结算类金融机构</option>
                            <option value='B08'>B08-金融控股公司</option>
                            <option value='B09'>B09-特定目的载体</option>
                            <option value='B99'>B99-其他</option>
                            <option value='C01'>C01-公司</option>
                            <option value='C02'>C02-非公司企业</option>
                            <option value='C99'>C99-其他非金融企业部门</option>
                            <option value='D01'>D01-住户</option>
                            <option value='D02'>D02-为住户服务的非营力机构</option>
                            <option value='E01'>E01-国际组织</option>
                            <option value='E02'>E02-外国政府</option>
                            <option value='E03'>E03-境外金融机构</option>
                            <option value='E04'>E04-境外非金融企业</option>
                            <option value='E05'>E05-外国居民</option>
                        </select>
                    </td>
                </tr>


                <tr>
                    <td align='right'>
                        借款人行业:
                    </td>
                    <td>
                        <select id='info_brrowerindustry' name='brrowerindustry' disabled="disabled" style='height: 23px; width:173px'>
                            <c:forEach items='${baseAindustryList}' var='aa'>
                                <option value='${aa.aindustrycode}'>${aa.aindustrycode}-${aa.aindustryname}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td align='right'>
                        借款人地区代码:
                    </td>
                    <td>
                        <input id='info_brrowerareacode' name='brrowerareacode' disabled="disabled" style='height: 23px; width:173px'>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        借款人经济成分:
                    </td>
                    <td>
                        <select id='info_inverstoreconomy' name='inverstoreconomy' disabled="disabled" style='height: 23px; width:173px' >
                            <option value='A01'>A01:国有控股</option>
                            <option value='A0101'>A0101:国有相对控股</option>
                            <option value='A0102'>A0102:国有绝对控股</option>
                            <option value='A02'>A02:集体控股</option>
                            <option value='A0201'>A0201:集体相对控股</option>
                            <option value='A0202'>A0202:集体绝对控股</option>
                            <option value='B01'>B01:私人控股</option>
                            <option value='B0101'>B0101:私人相对控股</option>
                            <option value='B0102'>B0102:私人绝对控股</option>
                            <option value='B02'>B02:港澳台控股</option>
                            <option value='B0201'>B0201:港澳台相对控股</option>
                            <option value='B0202'>B0202:港澳台绝对控股</option>
                            <option value='B03'>B03:外商控股</option>
                            <option value='B0301'>B0301:外商相对控股</option>
                            <option value='B0302'>B0302:外商绝对控股</option>
                        </select>
                    </td>
                    <td align='right'>
                        借款人企业规模:
                    </td>
                    <td>
                        <select id='info_enterprisescale' name='enterprisescale' disabled="disabled" style='height: 23px; width:173px' >
                            <option value='CS01'>CS01-大型</option>
                            <option value='CS02'>CS02-中型</option>
                            <option value='CS03'>CS03-小型</option>
                            <option value='CS04'>CS04-微型</option>
                            <option value='CS05'>CS05-其他（非企业类单位）</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        委托贷款借据编码:
                    </td>
                    <td>
                        <input id='info_receiptcode' name='receiptcode' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        委托贷款合同编码:
                    </td>
                    <td>
                        <input id='info_contractcode' name='contractcode' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款实际投向:
                    </td>
                    <td>
                        <select id='info_loanactualdirection' name='loanactualdirection' disabled="disabled" style='height: 23px; width:173px'>
                            <c:forEach items='${baseBindustryList}' var='aa'>
                                <option value='${aa.bindustrycode}'>${aa.bindustrycode}-${aa.bindustryname}</option>
                            </c:forEach>
                        </select>
                    </td>

                    <td align='right'>
                        贷款用途:
                    </td>
                    <td>
                        <input id='info_issupportliveloan' name='issupportliveloan' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        委托贷款发放日期:
                    </td>
                    <td>
                        <input id='info_loanstartdate' name='loanstartdate' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        委托贷款到期日期:
                    </td>
                    <td>
                        <input id='info_loanenddate' name='loanenddate'   disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        委托贷款展期到期日期:
                    </td>
                    <td>
                        <input id='info_extensiondate' name='extensiondate' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        币种:
                    </td>
                    <td>
                        <select id='info_currency' name='currency' disabled="disabled" style='height: 23px; width:173px'>
                            <c:forEach items='${baseCurrencyList}' var='aa'>
                                <option value='${aa.currencycode}'>${aa.currencycode}-${aa.currencyname}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        委托贷款余额:
                    </td>
                    <td>
                        <input id='info_receiptbalance' name='receiptbalance' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        委托贷款余额折人民币:
                    </td>
                    <td>
                        <input id='info_receiptcnybalance' name='receiptcnybalance' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        利率是否固定:
                    </td>
                    <td>
                        <select id='info_interestisfixed' name='interestisfixed' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='RF01'>RF01-固定利率</option>
                            <option value='RF02'>RF02-浮动利率</option>
                        </select>
                    </td>
                    <td align='right'>
                        利率水平:
                    </td>
                    <td>
                        <input id='info_interestislevel' name='interestislevel' disabled="disabled"  data-options="required:true,precision:2" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        手续费金额折人民币:
                    </td>
                    <td>
                        <input id='info_procedurebalance' name='procedurebalance' disabled="disabled" style='width:173px;'/>
                    </td>

                    <td align='right'>
                        贷款担保方式:
                    </td>
                    <td>
                        <select id='info_guaranteemethod' name='guaranteemethod' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='A'>A-质押贷款</option>

                            <option value='B01'>B01-房地产抵押贷款</option>
                            <option value='B99'>B99-其他抵押贷款</option>

                            <option value='C01'>C01-联保贷款</option>
                            <option value='C99'>C99-其他保证贷款</option>
                            <option value='D'>D-信用/免担保贷款</option>
                            <option value='E'>E-组合担保</option>
                            <option value='Z'>Z-其他</option>
                        </select>
                    </td>
                </tr>


                <tr>
                    <td align='right'>
                        贷款质量:
                    </td>
                    <td>
                        <select id='info_loanquality' name='loanquality' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='FQ01'>FQ01-正常类贷款</option>
                            <option value='FQ02'>FQ02-关注类贷款</option>
                            <option value='FQ03'>FQ03-次级类贷款</option>
                            <option value='FQ04'>FQ04-可疑类贷款</option>
                            <option value='FQ05'>FQ05-损失类贷款</option>
                        </select>
                    </td>

                    <td align='right'>
                        贷款状态:
                    </td>
                    <td>
                        <select id='info_loanstatus' name='loanstatus' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='LS01'>LS01-正常</option>
                            <option value='LS02'>LS02-展期</option>
                            <option value='LS03'>LS03-逾期</option>
                            <option value='LS04'>LS04-缩期</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td align='right'>
                        委托人国民经济部门:
                    </td>
                    <td>
                        <select id='info_bailorgreenloan' name='bailorgreenloan' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='A01'>A01-中央政府</option>
                            <option value='A02'>A02-地方政府</option>
                            <option value='A03'>A03-社会保障基金</option>
                            <option value='A04'>A04-机关团体</option>
                            <option value='A05'>A05-部队</option>
                            <option value='A06'>A06-住房公积金</option>
                            <option value='A99'>A99-其他</option>
                            <option value='B01'>B01-货币当局</option>
                            <option value='B02'>B02-监管当局</option>
                            <option value='B03'>B03-银行业存款类金融机构</option>
                            <option value='B04'>B04-银行业非存款类金融机构</option>
                            <option value='B05'>B05-证券业金融机构</option>
                            <option value='B06'>B06-保险业金融机构</option>
                            <option value='B07'>B07-交易及结算类金融机构</option>
                            <option value='B08'>B08-金融控股公司</option>
                            <option value='B09'>B09-特定目的载体</option>
                            <option value='B99'>B99-其他</option>
                            <option value='C01'>C01-公司</option>
                            <option value='C02'>C02-非公司企业</option>
                            <option value='C99'>C99-其他非金融企业部门</option>
                            <option value='D01'>D01-住户</option>
                            <option value='D02'>D02-为住户服务的非营力机构</option>
                            <option value='E01'>E01-国际组织</option>
                            <option value='E02'>E02-外国政府</option>
                            <option value='E03'>E03-境外金融机构</option>
                            <option value='E04'>E04-境外非金融企业</option>
                            <option value='E05'>E05-外国居民</option>
                        </select>
                    </td>

                    <td align='right'>
                        委托人证件类型:
                    </td>
                    <td>
                        <select id='info_bailorgfarmerloan' name='bailorgfarmerloan' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='A01'>A01-统一社会信用代码</option>"
                            <option value='A02'>A02-组织机构代码</option>"
                            <option value='A03'>A03-资管产品统计编码</option>"
                            <option value='A04'>A04-资管产品登记备案编码</option>"
                            <option value='A99'>A99-其他</option>"
                            <option value='B01'>B01-身份证</option>"
                            <option value='B02'>B02-户口簿</option>"
                            <option value='B03'>B03-护照</option>"
                            <option value='B04'>B04-军官证</option>"
                            <option value='B05'>B05-士兵证</option>"
                            <option value='B06'>B06-港澳居民来往内地通行证</option>"
                            <option value='B07'>B07-台湾同胞来往内地通行证</option>"
                            <option value='B08'>B08-临时身份证</option>"
                            <option value='B09'>B09-外国人居留证</option>"
                            <option value='B10'>B10-警官证</option>"
                            <option value='B11'>B11-外国人永久居留身份证</option>"
                            <option value='B12'>B12-港澳台居民居住证</option>"
                            <option value='B99'>B99-其他证件</option>"
                        </select>
                    </td>
                </tr>

                <tr>
                    <td align='right'>
                        委托人证件代码:
                    </td>
                    <td>
                        <input id='info_bailorgidnum' name='bailorgidnum' disabled="disabled" style='width:173px;'/>
                    </td>

                    <td align='right'>
                        委托人行业:
                    </td>
                    <td>
                        <select id='info_bailorgindustry' name='bailorgindustry' disabled="disabled" style='height: 23px; width:173px'>
                            <c:forEach items='${baseAindustryList}' var='aa'>
                                <option value='${aa.aindustrycode}'>${aa.aindustrycode}-${aa.aindustryname}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td align='right'>
                        委托人地区代码:
                    </td>
                    <td>
                        <input id='info_bailorgareacode' name='bailorgareacode' disabled="disabled" style='height: 23px; width:173px'>
                    </td>
                    <td align='right'>
                        委托人经济成分:
                    </td>
                    <td>
                        <select id='info_bailorgeconomy' name='bailorgeconomy' disabled="disabled" style='height: 23px; width:173px' >
                            <option value='A01'>A01:国有控股</option>
                            <option value='A0101'>A0101:国有相对控股</option>
                            <option value='A0102'>A0102:国有绝对控股</option>
                            <option value='A02'>A02:集体控股</option>
                            <option value='A0201'>A0201:集体相对控股</option>
                            <option value='A0202'>A0202:集体绝对控股</option>
                            <option value='B01'>B01:私人控股</option>
                            <option value='B0101'>B0101:私人相对控股</option>
                            <option value='B0102'>B0102:私人绝对控股</option>
                            <option value='B02'>B02:港澳台控股</option>
                            <option value='B0201'>B0201:港澳台相对控股</option>
                            <option value='B0202'>B0202:港澳台绝对控股</option>
                            <option value='B03'>B03:外商控股</option>
                            <option value='B0301'>B0301:外商相对控股</option>
                            <option value='B0302'>B0302:外商绝对控股</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td align='right'>
                        委托人企业规模:
                    </td>
                    <td>
                        <select id='info_bailorgenterprise' name='bailorgenterprise' disabled="disabled" style='height: 23px; width:173px' >
                            <option value='CS01'>CS01-大型</option>
                            <option value='CS02'>CS02-中型</option>
                            <option value='CS03'>CS03-小型</option>
                            <option value='CS04'>CS04-微型</option>
                            <option value='CS05'>CS05-其他（非企业类单位）</option>
                        </select>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script type="text/javascript">
    var index = 5;
    var baseAreas = <%=baseAreas%>;
    var baseAindustrys = <%=baseAindustrys%>;
    var baseBindustrys = <%=baseBindustrys%>;
    var baseAreaAndCountrys = <%=baseAreaAndCountrys%>;
    var baseCurrencys = <%=baseCurrencys%>;
    var operationtimeParam = '<%=operationtimeParam%>';
    $(function(){
        $('#datedialog').window('close');
        $("#dg1").datagrid({
            loadMsg:'数据加载中,请稍后...',
            method:'post',
            url:'XGetHistoryXclwtdkxxH?operationtimeParam='+operationtimeParam,
            checkOnSelect:true,
            autoRowHeight:false,
            pagination:true,
            rownumbers:true,
            toolbar:'#tb1',
            fitColumns:false,
            pageSize:20,
            pageList:[15,20,30,50],
            columns:[[
                {field:'ck',checkbox:true},
                {field:'id', title: '编号', width: 80, align: 'center', hidden: true},
                {field:'financeorgcode',title:'金融机构代码',width:150,align:'center'},
                {field:'financeorginnum',title:'内部机构号',width:150,align:'center'},
                {field:'financeorgareacode',title:'金融机构地区代码',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseAreas.length;i++){
                            if (baseAreas[i].substring(0,6)==value){
                                return baseAreas[i];
                            }
                        }
                    }},
                {field:'isfarmerloan',title:'借款人证件类型',width:150,align:'center',formatter:function (value) {
                        if (value=='A01'){
                            return "A01-统一社会信用代码";
                        }else if(value=='A02'){
                            return "A02-组织机构代码";
                        }else if (value=='A03'){
                            return "A03-资管产品统计编码";
                        }else if (value=='A04'){
                            return "A04-资管产品登记备案编码";
                        }else if (value=='A99'){
                            return "A99-其他";
                        }else if (value=='B01'){
                            return "B01-身份证";
                        }else if (value=='B02'){
                            return "B02-户口簿";
                        }else if (value=='B03'){
                            return "B03-护照";
                        }else if (value=='B04'){
                            return "B04-军官证";
                        }else if (value=='B05'){
                            return "B05-士兵证";
                        }else if (value=='B06'){
                            return "B06-港澳居民来往内地通行证";
                        }else if (value=='B07'){
                            return "B07-台湾同胞来往内地通行证";
                        }else if (value=='B08'){
                            return "B08-临时身份证";
                        }else if (value=='B09'){
                            return "B09-外国人居留证";
                        }else if (value=='B10'){
                            return "B10-警官证";
                        }else if (value=='B11'){
                            return "B11-外国人永久居留身份证";
                        }else if (value=='B12'){
                            return "B12-港澳台居民居住证";
                        }else if (value=='B99'){
                            return "B99-其他证件";
                        }
                        return value;
                    }},
                {field:'brroweridnum',title:'借款人证件代码',width:150,align:'center'},
                {field:'isgreenloan',title:'借款人国民经济部门',width:150,align:'center',formatter:function (value) {
                        if (value == 'A01') {
                            return "A01-中央政府";
                        } else if (value == 'A02') {
                            return "A02-地方政府";
                        } else if (value == 'A03') {
                            return "A03-社会保障基金";
                        } else if (value == 'A04') {
                            return "A04-机关团体";
                        } else if (value == 'A05') {
                            return "A05-部队";
                        } else if (value == 'A06') {
                            return "A06-住房公积金";
                        } else if (value == 'A99') {
                            return "A99-其他";
                        } else if (value == 'B01') {
                            return "B01-货币当局";
                        } else if (value == 'B02') {
                            return "B02-监管当局";
                        } else if (value == 'B03') {
                            return "B03-银行业存款类金融机构";
                        } else if (value == 'B04') {
                            return "B04-银行业非存款类金融机构";
                        } else if (value == 'B05') {
                            return "B05-证券业金融机构";
                        } else if (value == 'B06') {
                            return "B06-保险业金融机构";
                        } else if (value == 'B07') {
                            return "B07-交易及结算类金融机构";
                        } else if (value == 'B08') {
                            return "B08-金融控股公司";
                        } else if (value == 'B09') {
                            return "B09-特定目的载体";
                        } else if (value == 'B99') {
                            return "B99-其他";
                        } else if (value == 'C01') {
                            return "C01-公司";
                        } else if (value == 'C02') {
                            return "C02-非公司企业";
                        } else if (value == 'C99') {
                            return "C99-其他非金融企业部门";
                        } else if (value == 'D01') {
                            return "D01-住户";
                        } else if (value == 'D02') {
                            return "D02-为住户服务的非营力机构";
                        } else if (value == 'E01') {
                            return "E01-国际组织";
                        } else if (value == 'E02') {
                            return "E02-外国政府";
                        } else if (value == 'E03') {
                            return "E03-境外金融机构";
                        } else if (value == 'E04') {
                            return "E04-境外非金融企业";
                        } else if (value == 'E05') {
                            return "E05-外国居民";
                        }
                        return value;
                    }},
                {field:'brrowerindustry',title:'借款人行业',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseAindustrys.length;i++){
                            if (baseAindustrys[i].substring(0,3)==value){
                                return baseAindustrys[i];
                            }
                        }
                    }},
                {field:'brrowerareacode',title:'借款人地区代码',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseAreaAndCountrys.length;i++){
                            if (baseAreaAndCountrys[i].substring(0,6)==value){
                                return baseAreaAndCountrys[i];
                            }
                        }
                    }},
                {field:'inverstoreconomy',title:'借款人经济成分',width:150,align:'center',formatter:function (value) {
                        if (value=='A01'){
                            return "A01:国有控股";
                        }else if(value=='A0101'){
                            return "A0101:国有相对控股";
                        }else if (value=='A0102'){
                            return "A0102:国有绝对控股";
                        }else if(value=='A02'){
                            return "A02:集体控股";
                        }else if (value=='A0201'){
                            return "A0201:集体相对控股";
                        }else if (value=='A0202'){
                            return "A0202:集体绝对控股";
                        }else if(value=='B01'){
                            return "B01:私人控股";
                        }else if (value=='B0101'){
                            return "B0101:私人相对控股";
                        }else if (value=='B0102'){
                            return "B0102:私人绝对控股";
                        }else if(value=='B02'){
                            return "B02:港澳台控股";
                        }else if (value=='B0201'){
                            return "B0201:港澳台相对控股";
                        }else if (value=='B0202'){
                            return "B0202:港澳台绝对控股";
                        }else if(value=='B03'){
                            return "B03:外商控股";
                        }else if (value=='B0301'){
                            return "B0301:外商相对控股";
                        }else if (value=='B0302'){
                            return "B0302:外商绝对控股";
                        }
                        return value;
                    }},
                {field:'enterprisescale',title:'借款人企业规模',width:150,align:'center',formatter:function (value) {
                        if (value=='CS01'){
                            return "CS01-大型";
                        }else if(value=='CS02'){
                            return "CS02-中型";
                        }else if (value=='CS03'){
                            return "CS03-小型";
                        }else if(value=='CS04'){
                            return "CS04-微型";
                        }else if (value=='CS05'){
                            return "CS05-其他（非企业类单位）";
                        }
                        return value;
                    }},

                {field:'receiptcode',title:'委托贷款借据编码',width:150,align:'center'},
                {field:'contractcode',title:'委托贷款合同编码',width:150,align:'center'},
                {field:'loanactualdirection',title:'贷款实际投向',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseBindustrys.length;i++){
                            if (baseBindustrys[i].substring(0,4)==value){
                                return baseBindustrys[i];
                            }
                        }
                    }},
                {field:'loanstartdate',title:'委托贷款发放日期',width:150,align:'center'},
                {field:'loanenddate',title:'委托贷款到期日期',width:150,align:'center'},
                {field:'extensiondate',title:'委托贷款展期到期日期',width:150,align:'center'},
                {field:'currency',title:'币种',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseCurrencys.length;i++){
                            if (baseCurrencys[i].substring(0,3)==value){
                                return baseCurrencys[i];
                            }
                        }
                    }},
                {field:'receiptbalance',title:'委托贷款余额',width:150,align:'center'},
                {field:'receiptcnybalance',title:'委托贷款余额折人民币',width:150,align:'center'},
                {field:'interestisfixed',title:'利率是否固定',width:150,align:'center'},
                {field:'interestislevel',title:'利率水平',width:150,align:'center'},
                {field:'baseinterest',title:'基准利率',width:150,align:'center'},
                {field:'procedurebalance',title:'手续费金额折人民币',width:150,align:'center'},
                {field:'guaranteemethod',title:'贷款担保方式',width:150,align:'center',formatter:function (value) {
                        if (value=='A'){
                            return "质押贷款";
                        }else if(value=='B'){
                            return "抵押贷款";
                        }else if (value=='B01'){
                            return "房地产抵押贷款";
                        }else if (value=='B99'){
                            return "其他抵押贷款";
                        }else if(value=='C'){
                            return "保证贷款";
                        }else if (value=='C01'){
                            return "联保贷款";
                        }else if(value=='C99'){
                            return "其他保证贷款";
                        }else if(value=='D'){
                            return "信用/免担保贷款";
                        }else if(value=='E'){
                            return "组合担保";
                        }
                        else if (value=='Z'){
                            return "Z-其他";
                        }
                        return value;
                    }},
                {field:'loanquality',title:'贷款质量',width:150,align:'center',formatter:function (value) {
                        if (value=='FQ01'){
                            return "FQ01-正常类贷款";
                        }else if(value=='FQ02'){
                            return "FQ02-关注类贷款";
                        }else if (value=='FQ03'){
                            return "FQ03-次级类贷款";
                        }else if(value=='FQ04'){
                            return "FQ04-可疑类贷款";
                        }else if (value=='FQ05'){
                            return "FQ05-损失类贷款";
                        }
                        return value;
                    }},
                {field:'loanstatus',title:'贷款状态',width:150,align:'center',formatter:function (value) {
                        if (value=='LS01'){
                            return "LS01-正常";
                        }else if(value=='LS02'){
                            return "LS02-展期";
                        }else if (value=='LS03'){
                            return "LS03-逾期";
                        }else if(value=='LS04'){
                            return "LS04-缩期";
                        }
                        return value;
                    }},

                {field:'bailorgreenloan',title:'委托人国民经济部门',width:150,align:'center',formatter:function (value) {
                        if (value == 'A01') {
                            return "A01-中央政府";
                        } else if (value == 'A02') {
                            return "A02-地方政府";
                        } else if (value == 'A03') {
                            return "A03-社会保障基金";
                        } else if (value == 'A04') {
                            return "A04-机关团体";
                        } else if (value == 'A05') {
                            return "A05-部队";
                        } else if (value == 'A06') {
                            return "A06-住房公积金";
                        } else if (value == 'A99') {
                            return "A99-其他";
                        } else if (value == 'B01') {
                            return "B01-货币当局";
                        } else if (value == 'B02') {
                            return "B02-监管当局";
                        } else if (value == 'B03') {
                            return "B03-银行业存款类金融机构";
                        } else if (value == 'B04') {
                            return "B04-银行业非存款类金融机构";
                        } else if (value == 'B05') {
                            return "B05-证券业金融机构";
                        } else if (value == 'B06') {
                            return "B06-保险业金融机构";
                        } else if (value == 'B07') {
                            return "B07-交易及结算类金融机构";
                        } else if (value == 'B08') {
                            return "B08-金融控股公司";
                        } else if (value == 'B09') {
                            return "B09-特定目的载体";
                        } else if (value == 'B99') {
                            return "B99-其他";
                        } else if (value == 'C01') {
                            return "C01-公司";
                        } else if (value == 'C02') {
                            return "C02-非公司企业";
                        } else if (value == 'C99') {
                            return "C99-其他非金融企业部门";
                        } else if (value == 'D01') {
                            return "D01-住户";
                        } else if (value == 'D02') {
                            return "D02-为住户服务的非营力机构";
                        } else if (value == 'E01') {
                            return "E01-国际组织";
                        } else if (value == 'E02') {
                            return "E02-外国政府";
                        } else if (value == 'E03') {
                            return "E03-境外金融机构";
                        } else if (value == 'E04') {
                            return "E04-境外非金融企业";
                        } else if (value == 'E05') {
                            return "E05-外国居民";
                        }
                        return value;
                    }},
                {field:'bailorgfarmerloan',title:'委托人证件类型',width:150,align:'center',formatter:function (value) {
                        if (value=='A01'){
                            return "A01-统一社会信用代码";
                        }else if(value=='A02'){
                            return "A02-组织机构代码";
                        }else if (value=='A03'){
                            return "A03-资管产品统计编码";
                        }else if (value=='A04'){
                            return "A04-资管产品登记备案编码";
                        }else if (value=='A99'){
                            return "A99-其他";
                        }else if (value=='B01'){
                            return "B01-身份证";
                        }else if (value=='B02'){
                            return "B02-户口簿";
                        }else if (value=='B03'){
                            return "B03-护照";
                        }else if (value=='B04'){
                            return "B04-军官证";
                        }else if (value=='B05'){
                            return "B05-士兵证";
                        }else if (value=='B06'){
                            return "B06-港澳居民来往内地通行证";
                        }else if (value=='B07'){
                            return "B07-台湾同胞来往内地通行证";
                        }else if (value=='B08'){
                            return "B08-临时身份证";
                        }else if (value=='B09'){
                            return "B09-外国人居留证";
                        }else if (value=='B10'){
                            return "B10-警官证";
                        }else if (value=='B11'){
                            return "B11-外国人永久居留身份证";
                        }else if (value=='B12'){
                            return "B12-港澳台居民居住证";
                        }else if (value=='B99'){
                            return "B99-其他证件";
                        }
                        return value;
                    }},
                {field:'bailorgidnum',title:'委托人证件代码',width:150,align:'center'},
                {field:'bailorgindustry',title:'委托人行业',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseAindustrys.length;i++){
                            if (baseAindustrys[i].substring(0,3)==value){
                                return baseAindustrys[i];
                            }
                        }
                    }},
                {field:'bailorgareacode',title:'委托人地区代码',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseAreaAndCountrys.length;i++){
                            if (baseAreaAndCountrys[i].substring(0,6)==value){
                                return baseAreaAndCountrys[i];
                            }
                        }
                    }},
                {field:'bailorgeconomy',title:'委托人经济成分',width:150,align:'center',formatter:function (value) {
                        if (value=='A01'){
                            return "A01:国有控股";
                        }else if(value=='A0101'){
                            return "A0101:国有相对控股";
                        }else if (value=='A0102'){
                            return "A0102:国有绝对控股";
                        }else if(value=='A02'){
                            return "A02:集体控股";
                        }else if (value=='A0201'){
                            return "A0201:集体相对控股";
                        }else if (value=='A0202'){
                            return "A0202:集体绝对控股";
                        }else if(value=='B01'){
                            return "B01:私人控股";
                        }else if (value=='B0101'){
                            return "B0101:私人相对控股";
                        }else if (value=='B0102'){
                            return "B0102:私人绝对控股";
                        }else if(value=='B02'){
                            return "B02:港澳台控股";
                        }else if (value=='B0201'){
                            return "B0201:港澳台相对控股";
                        }else if (value=='B0202'){
                            return "B0202:港澳台绝对控股";
                        }else if(value=='B03'){
                            return "B03:外商控股";
                        }else if (value=='B0301'){
                            return "B0301:外商相对控股";
                        }else if (value=='B0302'){
                            return "B0302:外商绝对控股";
                        }
                        return value;
                    }},
                {field:'bailorgenterprise',title:'委托人企业规模',width:150,align:'center',formatter:function (value) {
                        if (value=='CS01'){
                            return "CS01-大型";
                        }else if(value=='CS02'){
                            return "CS02-中型";
                        }else if (value=='CS03'){
                            return "CS03-小型";
                        }else if(value=='CS04'){
                            return "CS04-微型";
                        }else if (value=='CS05'){
                            return "CS05-其他（非企业类单位）";
                        }
                        return value;
                    }},
                {field:'issupportliveloan',title:'贷款用途',width:150,align:'center'},

                {field: 'sjrq',title: '数据日期',width: 150,align: 'center'},
                {field:'customernum',title:'客户号码',width:150,align:'center'},
                {field: 'operator', title: '操作人',  align: 'center'},
                {field: 'operationtime', title: '操作时间',  align: 'center'}
            ]],
            onDblClickRow: function (rowIndex, rowData) {
                $('#info_formForAdd').form('clear');
                $('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','存量贷款信息详情');
                $('#info_formForAdd').form('load',rowData);
            }
        });
        $('#operationtimeParam').datebox('setValue',operationtimeParam);
    })

    // 查询
    function searchOnSelected(){
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();
        var receiptcodeParam = $("#receiptcodeParam").val().trim();
        var brroweridnumParam = $("#brroweridnumParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var operationtimeParam = $("#operationtimeParam").datebox("getValue");
        $("#dg1").datagrid("load", {"financeorgcodeParam" : financeorgcodeParam,"receiptcodeParam" : receiptcodeParam,"brroweridnumParam" : brroweridnumParam,"isfarmerloanParam" : isfarmerloanParam,"contractcodeParam" : contractcodeParam,"sjrqParam" : operationtimeParam});
    }

    // 导出
    function showOut(){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        var info = '';
        for( var dataIndex in row){
            id = id + row[dataIndex].id + ",";
        }
        if(id != ''){
            info = '是否将选中的数据导出到Excel表中？';
        }else {
            info = '是否将全部数据导出到Excel表中？'
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();
        var receiptcodeParam = $("#receiptcodeParam").val().trim();
        var brroweridnumParam = $("#brroweridnumParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");
        var operationtimeParam = $("#operationtimeParam").combobox("getValue");
        $.messager.confirm('操作提示', info, function (r) {
            if (r) {
                window.location.href = "XExportXclwtdkxxH?financeorgcodeParam="+financeorgcodeParam+"&receiptcodeParam="+receiptcodeParam+"&brroweridnumParam="+brroweridnumParam+"&isfarmerloanParam="+isfarmerloanParam+"&contractcodeParam="+contractcodeParam+"&sjrqParam="+operationtimeParam +"&id="+id;
            }
        });
    }
    //数据打回
    function dataBack(){
        var v = $('#dateform').form('validate');
        if(v){
            var value = $('#dateselect').datebox('getValue');
            $('#datedialog').window('close');
            $.messager.confirm('提示','是否打回数据？',function (r){
                if(r){
                    $.messager.progress({
                        title: '请稍等',
                        msg: '数据处理中......'
                    });
                    $.ajax({
                        url: "XdatabackxclwtdkxxH",
                        type: "POST",
                        contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                        async: true,
                        data:{"date":value},
                        dataType:'json',
                        success: function (data) {
                            if(data != 0){
                                $.messager.progress('close');
                                $.messager.alert('操作提示', '数据打回成功！', 'info');
                                $("#dg1").datagrid("reload");
                            }else{
                                $.messager.progress('close');
                                $.messager.alert('操作提示', '没有数据可打回！', 'error');
                                $("#dg1").datagrid("reload");
                            }

                        },
                        error: function (error) {
                            $.messager.progress('close');
                            $.messager.alert('操作提示', '数据打回失败！', 'error');
                        }
                    })
                }
            });
        }

    }
</script>
</body>
</html>
