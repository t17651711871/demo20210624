<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String operationtimeParam = (String) session.getAttribute("operationtimeParam");
    if (StringUtils.isBlank(operationtimeParam)){
        operationtimeParam = "";
    }
    List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
    List<String> baseAreas = new ArrayList<>();
    for (BaseArea baseArea : baseAreaList) {
        baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
    }
    List<BaseCountry> baseCountryList = (List<BaseCountry>) request.getAttribute("baseCountryList");
    List<String>  baseCountrys = new ArrayList<>();
    for (BaseCountry baseCountry : baseCountryList) {
    	baseCountrys.add("'"+baseCountry.getCountrycode()+ "-" +baseCountry.getCountryname()+"'");
    }
    List<String>  baseAreaAndCountrys = new ArrayList<>();
    baseAreaAndCountrys.addAll(baseAreas);
    baseAreaAndCountrys.addAll(baseCountrys);
    List<BaseAindustry> baseAindustryList = (List<BaseAindustry>) request.getAttribute("baseAindustryList");
    List<String> baseAindustrys = new ArrayList<>();
    for (BaseAindustry baseAindustry : baseAindustryList) {
        baseAindustrys.add("'"+baseAindustry.getAindustrycode()+ "-" +baseAindustry.getAindustryname()+"'");
    }
    List<BaseBindustry> baseBindustryList = (List<BaseBindustry>) request.getAttribute("baseBindustryList");
    List<String> baseBindustrys = new ArrayList<>();
    for (BaseBindustry baseBindustry : baseBindustryList) {
        baseBindustrys.add("'"+baseBindustry.getBindustrycode()+ "-" +baseBindustry.getBindustryname()+"'");
    }
    List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
    List<String> baseCurrencys = new ArrayList<>();
    for (BaseCurrency baseCurrency : baseCurrencyList) {
        baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>同业客户基础信息</title>
    <%@include file="../../common/head.jsp"%>
</head>
<body>

<table id="dg1" style="height: 480px;" title="同业客户基础信息列表"></table>

<!-- 日期选择框 -->
<div id="datedialog" class="easyui-dialog" title="日期选择" style="width:400px;height:200px;padding-left:30px;padding-top:50px" data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="dateform" action="">
        请选择报文日期：<input id="dateselect" type="text" class="easyui-datebox" required="required"><br/><br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-ok" onclick="dataBack()">确定</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#datedialog').window('close')">取消</a>
    </form>
</div>

<!--表格工具栏-->
<div id="tb1" style="padding: 5px; height: auto;">
    <a class="easyui-linkbutton" href="XHistoryDataUi" iconCls="icon-return">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <label>金融机构代码：</label><input id="financeorgcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>客户名称：</label><input id="khnameParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>客户类别：</label>
    <select class='easyui-combobox'  id="khtypeParam" name="khtypeParam" editable=false style="height: 23px; width:130px">
        <option value=''>=请选择=</option>
        <option value='B0311'>B0311-中资大型银行</option>
		<option value='B0312'>B0312-中资中型银行</option>
		<option value='B03131'>B03131-小型城市商业银行</option>
		<option value='B03132'>B03132-农村商业银行</option>
		<option value='B03133'>B03133-农村合作银行</option>
		<option value='B03134'>B03134-村镇银行</option>
		<option value='B032'>B032-城市信用合作社</option>
		<option value='B033'>B033-农村信用社</option>
		<option value='B034'>B034-农村资金互助社</option>
		<option value='B035'>B035-财务公司</option>
		<option value='B036'>B036-外资银行</option>
		<option value='B041'>B041-信托公司</option>
		<option value='B042'>B042-金融资产管理公司</option>
		<option value='B043'>B043-金融租赁公司</option>
		<option value='B044'>B044-汽车金融公司</option>
		<option value='B045'>B045-贷款公司</option>
		<option value='B046'>B046-货币经纪公司</option>
		<option value='B047'>B047-消费金融公司</option>
		<option value='B051'>B051-证券公司</option>
		<option value='B052'>B052-证券投资基金公司</option>
		<option value='B053'>B053-期货公司</option>
		<option value='B054'>B054-投资咨询公司</option>
		<option value='B055'>B055-银行理财子公司</option>
		<option value='B056'>B056-证券子公司</option>
		<option value='B057'>B057-基金子公司</option>
		<option value='B058'>B058-期货子公司</option>
		<option value='B060'>B060-财产保险公司</option>
		<option value='B061'>B061-人身保险公司</option>
		<option value='B062'>B062-再保险公司</option>
		<option value='B063'>B063-保险资产管理公司</option>
		<option value='B064'>B064-保险经纪公司</option>
		<option value='B065'>B065-保险代理公司</option>
		<option value='B066'>B066-保险公估公司</option>
		<option value='B067'>B067-保险集团（控股）公司</option>
		<option value='B068'>B068-企业（职业）年金</option>
		<option value='B07'>B07-交易及结算类金融机构</option>
		<option value='B08'>B08-金融控股公司</option>
		<option value='B091'>B091-银行理财产品</option>
		<option value='B092'>B092-信托公司资管产品</option>
		<option value='B093'>B093-证券公司及其子公司资管产品</option>
		<option value='B094'>B094-基金公司及其子公司专户</option>
		<option value='B095'>B095-期货公司及其子公司资管产品</option>
		<option value='B096'>B096-公募基金</option>
		<option value='B097'>B097-私募机构私募基金</option>
		<option value='B098'>B098-保险资管产品</option>
		<option value='B099'>B099-金融资产投资公司资管产品</option>
		<option value='B10'>B10-其他金融机构</option>
		<option value='E012'>E012-境外金融机构</option>
    </select>&nbsp;
    <label>客户代码：</label><input id="khcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>数据日期：</label><input id="operationtimeParam" class='easyui-datebox' style="height: 23px; width:160px"/>&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;<br>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut()">导出</a>&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-edit" onclick="javascript:$('#datedialog').window('open')">数据打回</a>
</div>

<!--详情窗口-->
<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div align="center" style="padding-top: 30px">
        <form id="info_formForAdd" method="post">
            <input id="info_id" class="backId" name="id" type="hidden">
            <table class='enterTable' id='info_enterTable'>
                <tr> <td align='right'>
                    金融机构代码:</td>
                    <td>
                        <input id='info_financeorgcode' name='financeorgcode' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        客户名称:
                    </td>
                    <td>
                        <input id='info_khname' name='khname' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    客户代码:</td>
                    <td>
                        <input id='info_khcode' name='khcode' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        客户金融机构编码:
                    </td>
                    <td>
                        <input id='info_khjrjgbm' name='khjrjgbm' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    客户内部编码:</td>
                    <td>
                        <input id='info_khnbbm' name='khnbbm' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        基本存款账号:
                    </td>
                    <td>
                        <input id='info_jbckzh' name='jbckzh' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    基本账户开户行名称:</td>
                    <td>
                        <input id='info_jbzhkhhmc' name='jbzhkhhmc' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        注册地址:
                    </td>
                    <td>
                        <input id='info_zcdz' name='zcdz' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    地区代码:</td>
                    <td>
                        <input id='info_dqdm' name='dqdm' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        客户类别:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_khtype' name='khtype' disabled="disabled" style='width:173px;'>
                       		<option value='B0311'>B0311-中资大型银行</option>
							<option value='B0312'>B0312-中资中型银行</option>
							<option value='B03131'>B03131-小型城市商业银行</option>
							<option value='B03132'>B03132-农村商业银行</option>
							<option value='B03133'>B03133-农村合作银行</option>
							<option value='B03134'>B03134-村镇银行</option>
							<option value='B032'>B032-城市信用合作社</option>
							<option value='B033'>B033-农村信用社</option>
							<option value='B034'>B034-农村资金互助社</option>
							<option value='B035'>B035-财务公司</option>
							<option value='B036'>B036-外资银行</option>
							<option value='B041'>B041-信托公司</option>
							<option value='B042'>B042-金融资产管理公司</option>
							<option value='B043'>B043-金融租赁公司</option>
							<option value='B044'>B044-汽车金融公司</option>
							<option value='B045'>B045-贷款公司</option>
							<option value='B046'>B046-货币经纪公司</option>
							<option value='B047'>B047-消费金融公司</option>
							<option value='B051'>B051-证券公司</option>
							<option value='B052'>B052-证券投资基金公司</option>
							<option value='B053'>B053-期货公司</option>
							<option value='B054'>B054-投资咨询公司</option>
							<option value='B055'>B055-银行理财子公司</option>
							<option value='B056'>B056-证券子公司</option>
							<option value='B057'>B057-基金子公司</option>
							<option value='B058'>B058-期货子公司</option>
							<option value='B060'>B060-财产保险公司</option>
							<option value='B061'>B061-人身保险公司</option>
							<option value='B062'>B062-再保险公司</option>
							<option value='B063'>B063-保险资产管理公司</option>
							<option value='B064'>B064-保险经纪公司</option>
							<option value='B065'>B065-保险代理公司</option>
							<option value='B066'>B066-保险公估公司</option>
							<option value='B067'>B067-保险集团（控股）公司</option>
							<option value='B068'>B068-企业（职业）年金</option>
							<option value='B07'>B07-交易及结算类金融机构</option>
							<option value='B08'>B08-金融控股公司</option>
							<option value='B091'>B091-银行理财产品</option>
							<option value='B092'>B092-信托公司资管产品</option>
							<option value='B093'>B093-证券公司及其子公司资管产品</option>
							<option value='B094'>B094-基金公司及其子公司专户</option>
							<option value='B095'>B095-期货公司及其子公司资管产品</option>
							<option value='B096'>B096-公募基金</option>
							<option value='B097'>B097-私募机构私募基金</option>
							<option value='B098'>B098-保险资管产品</option>
							<option value='B099'>B099-金融资产投资公司资管产品</option>
							<option value='B10'>B10-其他金融机构</option>
							<option value='E012'>E012-境外金融机构</option>
                        </select>
                    </td>
                </tr>
<tr> <td align='right'>
                    成立日期:</td>
                    <td>
                        <input class='easyui-datebox' id='info_setupdate' name='setupdate' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        是否关联方:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_sfglf' name='sfglf' disabled="disabled" style='width:173px;'>
                        	<option value='1'>1-是</option>
                        	<option value='0'>0-否</option>
                        </select>
                    </td>
                </tr>
<tr> <td align='right'>
                    客户经济成分:</td>
                    <td>
                        <select class='easyui-combobox' id='info_khjjcf' name='khjjcf' disabled="disabled" style='width:173px;'>
                        	<option value='A'>A-公有控股经济</option>
							<option value='A01'>A01-国有控股</option>
							<option value='A0101'>A0101-国有相对控股</option>
							<option value='A0102'>A0102-国有绝对控股</option>
							<option value='A02'>A02-集体控股</option>
							<option value='A0201'>A0201-集体相对控股</option>
							<option value='A0202'>A0202-集体绝对控股</option>
							<option value='B'>B-非公有控股经济</option>
							<option value='B01'>B01-私人控股</option>
							<option value='B0101'>B0101-私人相对控股</option>
							<option value='B0102'>B0102-私人绝对控股</option>
							<option value='B02'>B02-港澳台控股</option>
							<option value='B0201'>B0201-港澳台相对控股</option>
							<option value='B0202'>B0202-港澳台绝对控股</option>
							<option value='B03'>B03-外商控股</option>
							<option value='B0301'>B0301-外商相对控股</option>
							<option value='B0302'>B0302-外商绝对控股</option>
                        </select>
                    </td>
     <td align='right'>
                        客户国民经济部门:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_khgmjjbm' name='khgmjjbm' disabled="disabled" style='width:173px;'>
                        	<option value='A'>A-广义政府</option>
							<option value='A01'>A01-中央政府</option>
							<option value='A02'>A02-地方政府</option>
							<option value='A03'>A03-社会保障基金</option>
							<option value='A04'>A04-机关团体</option>
							<option value='A05'>A05-部队</option>
							<option value='A06'>A06-住房公积金</option>
							<option value='A99'>A99-其他</option>
							<option value='B'>B-金融机构部门</option>
							<option value='B01'>B01-货币当局</option>
							<option value='B02'>B02-监管当局</option>
							<option value='B03'>B03-银行业存款类金融机构</option>
							<option value='B04'>B04-银行业非存款类金融机构</option>
							<option value='B05'>B05-证券业金融机构</option>
							<option value='B06'>B06-保险业金融机构</option>
							<option value='B07'>B07-交易及结算类金融机构</option>
							<option value='B08'>B08-金融控股公司</option>
							<option value='B09'>B09-特定目的载体</option>
							<option value='B99'>B99-其他</option>
							<option value='C'>C-非金融企业部门</option>
							<option value='C01'>C01-公司</option>
							<option value='C02'>C02-非公司企业</option>
							<option value='C99'>C99-其他非金融企业部门</option>
							<option value='D'>D-住户部门</option>
							<option value='D01'>D01-住户</option>
							<option value='D02'>D02-为住户服务的非营利机构</option>
							<option value='E'>E-非居民部门</option>
							<option value='E01'>E01-国际组织</option>
							<option value='E02'>E02-外国政府</option>
							<option value='E03'>E03-境外金融机构</option>
							<option value='E04'>E04-境外非金融企业</option>
							<option value='E05'>E05-外国居民</option>
                        </select>
                    </td>
                </tr>
<tr> <td align='right'>
                    客户信用级别总等级数:</td>
                    <td>
                        <input id='info_khxyjbzdjs' name='khxyjbzdjs' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        客户信用评级:
                    </td>
                    <td>
                        <input id='info_khxypj' name='khxypj' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    数据日期:</td>
                    <td>
                        <input class='easyui-datebox' id='info_sjrq' name='sjrq' disabled="disabled" style='width:173px;'/>
                    </td>
</tr>
            </table>
        </form>
    </div>
</div>

<script type="text/javascript">
    var index = 5;
    var baseAreaAndCountrys = <%=baseAreaAndCountrys%>;
    var baseAreas = <%=baseAreas%>;
    var baseAindustrys = <%=baseAindustrys%>;
    var baseBindustrys = <%=baseBindustrys%>;
    var baseCurrencys = <%=baseCurrencys%>;
    var operationtimeParam = '<%=operationtimeParam%>';
    $(function(){
        $('#datedialog').window('close');
        $("#dg1").datagrid({
            loadMsg:'数据加载中,请稍后...',
            method:'post',
            url:'XGetHistoryXtykhjcxxH?operationtimeParam='+operationtimeParam,
            checkOnSelect:true,
            autoRowHeight:false,
            pagination:true,
            rownumbers:true,
            toolbar:'#tb1',
            fitColumns:false,
            pageSize:20,
            pageList:[15,20,30,50],
            columns:[[
            	{field:'ck',checkbox:true},
                {field:'id', title: '编号', width: 80, align: 'center', hidden: true},
                {field:'financeorgcode',title:'金融机构代码',width:150,align:'center'},
                {field:'khname',title:'客户名称',width:150,align:'center'},
                {field:'khcode',title:'客户代码',width:150,align:'center'},
                {field:'khjrjgbm',title:'客户金融机构编码',width:150,align:'center'},
                {field:'khnbbm',title:'客户内部编码',width:150,align:'center'},
                {field:'jbckzh',title:'基本存款账号',width:150,align:'center'},
                {field:'jbzhkhhmc',title:'基本账户开户行名称',width:150,align:'center'},
                {field:'zcdz',title:'注册地址',width:150,align:'center'},
                {field:'dqdm',title:'地区代码',width:150,align:'center',formatter:function (value) {
                    for (var i = 0;i <  baseAreas.length;i++){
                        if (baseAreas[i].substring(0,6)==value){
                            return baseAreas[i];
                        }
                    }
                }},
                {field:'khtype',title:'客户类别',width:150,align:'center',formatter:function(value){
                	if (value=='B0311'){
                		  return "B0311-中资大型银行";
                		}else if (value=='B0312'){
                		  return "B0312-中资中型银行";
                		}else if (value=='B03131'){
                		  return "B03131-小型城市商业银行";
                		}else if (value=='B03132'){
                		  return "B03132-农村商业银行";
                		}else if (value=='B03133'){
                		  return "B03133-农村合作银行";
                		}else if (value=='B03134'){
                		  return "B03134-村镇银行";
                		}else if (value=='B032'){
                		  return "B032-城市信用合作社";
                		}else if (value=='B033'){
                		  return "B033-农村信用社";
                		}else if (value=='B034'){
                		  return "B034-农村资金互助社";
                		}else if (value=='B035'){
                		  return "B035-财务公司";
                		}else if (value=='B036'){
                		  return "B036-外资银行";
                		}else if (value=='B041'){
                		  return "B041-信托公司";
                		}else if (value=='B042'){
                		  return "B042-金融资产管理公司";
                		}else if (value=='B043'){
                		  return "B043-金融租赁公司";
                		}else if (value=='B044'){
                		  return "B044-汽车金融公司";
                		}else if (value=='B045'){
                		  return "B045-贷款公司";
                		}else if (value=='B046'){
                		  return "B046-货币经纪公司";
                		}else if (value=='B047'){
                		  return "B047-消费金融公司";
                		}else if (value=='B051'){
                		  return "B051-证券公司";
                		}else if (value=='B052'){
                		  return "B052-证券投资基金公司";
                		}else if (value=='B053'){
                		  return "B053-期货公司";
                		}else if (value=='B054'){
                		  return "B054-投资咨询公司";
                		}else if (value=='B055'){
                		  return "B055-银行理财子公司";
                		}else if (value=='B056'){
                		  return "B056-证券子公司";
                		}else if (value=='B057'){
                		  return "B057-基金子公司";
                		}else if (value=='B058'){
                		  return "B058-期货子公司";
                		}else if (value=='B060'){
                		  return "B060-财产保险公司";
                		}else if (value=='B061'){
                		  return "B061-人身保险公司";
                		}else if (value=='B062'){
                		  return "B062-再保险公司";
                		}else if (value=='B063'){
                		  return "B063-保险资产管理公司";
                		}else if (value=='B064'){
                		  return "B064-保险经纪公司";
                		}else if (value=='B065'){
                		  return "B065-保险代理公司";
                		}else if (value=='B066'){
                		  return "B066-保险公估公司";
                		}else if (value=='B067'){
                		  return "B067-保险集团（控股）公司";
                		}else if (value=='B068'){
                		  return "B068-企业（职业）年金";
                		}else if (value=='B07'){
                		  return "B07-交易及结算类金融机构";
                		}else if (value=='B08'){
                		  return "B08-金融控股公司";
                		}else if (value=='B091'){
                		  return "B091-银行理财产品";
                		}else if (value=='B092'){
                		  return "B092-信托公司资管产品";
                		}else if (value=='B093'){
                		  return "B093-证券公司及其子公司资管产品";
                		}else if (value=='B094'){
                		  return "B094-基金公司及其子公司专户";
                		}else if (value=='B095'){
                		  return "B095-期货公司及其子公司资管产品";
                		}else if (value=='B096'){
                		  return "B096-公募基金";
                		}else if (value=='B097'){
                		  return "B097-私募机构私募基金";
                		}else if (value=='B098'){
                		  return "B098-保险资管产品";
                		}else if (value=='B099'){
                		  return "B099-金融资产投资公司资管产品";
                		}else if (value=='B10'){
                		  return "B10-其他金融机构";
                		}else if (value=='E012'){
                		  return "E012-境外金融机构";
                		}
                }},
                {field:'setupdate',title:'成立日期',width:150,align:'center'},
                {field:'sfglf',title:'是否关联方',width:150,align:'center',formatter:function (value) {
                    if (value=='1'){
                        return "1-是";
                    }else if(value=='0'){
                        return "0-否";
                    }
                    return value;
                }},
                {field:'khjjcf',title:'客户经济成分',width:150,align:'center',formatter:function (value) {
                	if (value=='A'){
                		  return "A-公有控股经济";
                		}else if (value=='A01'){
                		  return "A01-国有控股";
                		}else if (value=='A0101'){
                		  return "A0101-国有相对控股";
                		}else if (value=='A0102'){
                		  return "A0102-国有绝对控股";
                		}else if (value=='A02'){
                		  return "A02-集体控股";
                		}else if (value=='A0201'){
                		  return "A0201-集体相对控股";
                		}else if (value=='A0202'){
                		  return "A0202-集体绝对控股";
                		}else if (value=='B'){
                		  return "B-非公有控股经济";
                		}else if (value=='B01'){
                		  return "B01-私人控股";
                		}else if (value=='B0101'){
                		  return "B0101-私人相对控股";
                		}else if (value=='B0102'){
                		  return "B0102-私人绝对控股";
                		}else if (value=='B02'){
                		  return "B02-港澳台控股";
                		}else if (value=='B0201'){
                		  return "B0201-港澳台相对控股";
                		}else if (value=='B0202'){
                		  return "B0202-港澳台绝对控股";
                		}else if (value=='B03'){
                		  return "B03-外商控股";
                		}else if (value=='B0301'){
                		  return "B0301-外商相对控股";
                		}else if (value=='B0302'){
                		  return "B0302-外商绝对控股";
                    }
                    return value;
                }},
                {field:'khgmjjbm',title:'客户国民经济部门',width:150,align:'center',formatter:function (value) {
                	if (value=='A'){
                		  return "A-广义政府";
                		}else if (value=='A01'){
                		  return "A01-中央政府";
                		}else if (value=='A02'){
                		  return "A02-地方政府";
                		}else if (value=='A03'){
                		  return "A03-社会保障基金";
                		}else if (value=='A04'){
                		  return "A04-机关团体";
                		}else if (value=='A05'){
                		  return "A05-部队";
                		}else if (value=='A06'){
                		  return "A06-住房公积金";
                		}else if (value=='A99'){
                		  return "A99-其他";
                		}else if (value=='B'){
                		  return "B-金融机构部门";
                		}else if (value=='B01'){
                		  return "B01-货币当局";
                		}else if (value=='B02'){
                		  return "B02-监管当局";
                		}else if (value=='B03'){
                		  return "B03-银行业存款类金融机构";
                		}else if (value=='B04'){
                		  return "B04-银行业非存款类金融机构";
                		}else if (value=='B05'){
                		  return "B05-证券业金融机构";
                		}else if (value=='B06'){
                		  return "B06-保险业金融机构";
                		}else if (value=='B07'){
                		  return "B07-交易及结算类金融机构";
                		}else if (value=='B08'){
                		  return "B08-金融控股公司";
                		}else if (value=='B09'){
                		  return "B09-特定目的载体";
                		}else if (value=='B99'){
                		  return "B99-其他";
                		}else if (value=='C'){
                		  return "C-非金融企业部门";
                		}else if (value=='C01'){
                		  return "C01-公司";
                		}else if (value=='C02'){
                		  return "C02-非公司企业";
                		}else if (value=='C99'){
                		  return "C99-其他非金融企业部门";
                		}else if (value=='D'){
                		  return "D-住户部门";
                		}else if (value=='D01'){
                		  return "D01-住户";
                		}else if (value=='D02'){
                		  return "D02-为住户服务的非营利机构";
                		}else if (value=='E'){
                		  return "E-非居民部门";
                		}else if (value=='E01'){
                		  return "E01-国际组织";
                		}else if (value=='E02'){
                		  return "E02-外国政府";
                		}else if (value=='E03'){
                		  return "E03-境外金融机构";
                		}else if (value=='E04'){
                		  return "E04-境外非金融企业";
                		}else if (value=='E05'){
                		  return "E05-外国居民";
                    }
                    return value;
                }},
                {field:'khxyjbzdjs',title:'客户信用级别总等级数',width:150,align:'center'},
                {field:'khxypj',title:'客户信用评级',width:150,align:'center'},
                {field:'sjrq',title:'数据日期',width:150,align:'center'},
                {field: 'operator', title: '操作人',  align: 'center'},
                {field: 'operationtime', title: '操作时间',  align: 'center'}
    	    ]],
            onDblClickRow: function (rowIndex, rowData) {
                $('#info_formForAdd').form('clear');
                $('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','同业客户基础信息详情');
                $('#info_formForAdd').form('load',rowData);
            }
        });
        $('#operationtimeParam').datebox('setValue',operationtimeParam);
    })

    // 查询
    function searchOnSelected(){
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var khnameParam = $("#khnameParam").val().trim();

        var khcodeParam = $("#khcodeParam").val().trim();
        var khtypeParam = $("#khtypeParam").combobox("getValue");

        var operationtimeParam = $("#operationtimeParam").datebox("getValue");
        $("#dg1").datagrid("load", {"financeorgcodeParam" : financeorgcodeParam,"khnameParam" : khnameParam,"khcodeParam" : khcodeParam,"khtypeParam" : khtypeParam,"sjrqParam" : operationtimeParam});
    }

    // 导出
    function showOut(){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        var info = '';
        for( var dataIndex in row){
            id = id + row[dataIndex].id + ",";
        }
        if(id != ''){
            info = '是否将选中的数据导出到Excel表中？';
        }else {
            info = '是否将全部数据导出到Excel表中？'
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var khnameParam = $("#khnameParam").val().trim();

        var khcodeParam = $("#khcodeParam").val().trim();
        var khtypeParam = $("#khtypeParam").combobox("getValue");

        var operationtimeParam = $("#operationtimeParam").datebox("getValue");
        $.messager.confirm('操作提示', info, function (r) {
            if (r) {
                window.location.href = "XExportXtykhjcxxH?financeorgcodeParam="+financeorgcodeParam+"&khnameParam="+khnameParam+"&khcodeParam="+khcodeParam+"&khtypeParam="+khtypeParam+"&sjrqParam="+operationtimeParam +"&id="+id;
            }
        });
    }

    //数据打回
    function dataBack(){
        var v = $('#dateform').form('validate');
        if(v){
            var value = $('#dateselect').datebox('getValue');
            $('#datedialog').window('close');
            $.messager.confirm('提示','是否打回数据？',function (r){
                if(r){
                    $.messager.progress({
                        title: '请稍等',
                        msg: '数据处理中......'
                    });
                    $.ajax({
                        url: "XdatabackxtykhjcxxH",
                        type: "POST",
                        contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                        async: true,
                        data:{"date":value},
                        dataType:'json',
                        success: function (data) {
                            if(data != 0){
                                $.messager.progress('close');
                                $.messager.alert('操作提示', '数据打回成功！', 'info');
                                $("#dg1").datagrid("reload");
                            }else{
                                $.messager.progress('close');
                                $.messager.alert('操作提示', '没有数据可打回！', 'error');
                                $("#dg1").datagrid("reload");
                            }

                        },
                        error: function (error) {
                            $.messager.progress('close');
                            $.messager.alert('操作提示', '数据打回失败！', 'error');
                        }
                    })
                }
            });
        }

    }
</script>
</body>
</html>
