<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String operationtimeParam = (String) session.getAttribute("operationtimeParam");
    if (StringUtils.isBlank(operationtimeParam)){
        operationtimeParam = "";
    }
    List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
    List<String> baseAreas = new ArrayList<>();
    for (BaseArea baseArea : baseAreaList) {
        baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
    }
    List<BaseCountry> baseCountryList = (List<BaseCountry>) request.getAttribute("baseCountryList");
    List<String>  baseCountrys = new ArrayList<>();
    for (BaseCountry baseCountry : baseCountryList) {
    	baseCountrys.add("'"+baseCountry.getCountrycode()+ "-" +baseCountry.getCountryname()+"'");
    }
    List<String>  baseAreaAndCountrys = new ArrayList<>();
    baseAreaAndCountrys.addAll(baseAreas);
    baseAreaAndCountrys.addAll(baseCountrys);
    List<BaseAindustry> baseAindustryList = (List<BaseAindustry>) request.getAttribute("baseAindustryList");
    List<String> baseAindustrys = new ArrayList<>();
    for (BaseAindustry baseAindustry : baseAindustryList) {
        baseAindustrys.add("'"+baseAindustry.getAindustrycode()+ "-" +baseAindustry.getAindustryname()+"'");
    }
    List<BaseBindustry> baseBindustryList = (List<BaseBindustry>) request.getAttribute("baseBindustryList");
    List<String> baseBindustrys = new ArrayList<>();
    for (BaseBindustry baseBindustry : baseBindustryList) {
        baseBindustrys.add("'"+baseBindustry.getBindustrycode()+ "-" +baseBindustry.getBindustryname()+"'");
    }
    List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
    List<String> baseCurrencys = new ArrayList<>();
    for (BaseCurrency baseCurrency : baseCurrencyList) {
        baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>同业存款发生额信息</title>
    <%@include file="../../common/head.jsp"%>
</head>
<body>

<table id="dg1" style="height: 480px;" title="同业存款发生额信息列表"></table>

<!-- 日期选择框 -->
<div id="datedialog" class="easyui-dialog" title="日期选择" style="width:400px;height:200px;padding-left:30px;padding-top:50px" data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="dateform" action="">
        请选择报文日期：<input id="dateselect" type="text" class="easyui-datebox" required="required"><br/><br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-ok" onclick="dataBack()">确定</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#datedialog').window('close')">取消</a>
    </form>
</div>

<!--表格工具栏-->
<div id="tb1" style="padding: 5px; height: auto;">
    <a class="easyui-linkbutton" href="XHistoryDataUi" iconCls="icon-return">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <label>金融机构代码：</label><input id="financeorgcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>合同编码：</label><input id="contractcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>交易对手证件类型：</label>
    <select class='easyui-combobox'  id="jydszjlxParam" name="jydszjlxParam" editable=false style="height: 23px; width:130px">
        <option value=''>=请选择=</option>
        <option value='A01'>A01-统一社会信用代码</option>
        <option value='A02'>A02-组织机构代码</option>
        <option value='C01'>C01-资管产品统计编码</option>
        <option value='C02'>C02-资管产品登记备案编码</option>
        <option value='L01'>L01-全球法人识别编码（LEI码）</option>
        <option value='Z99'>Z99-自定义码</option>
    </select>&nbsp;
    <label>交易对手代码：</label><input id="jydsdmParam" type="text" style="height: 23px; width:130px"/>
    &nbsp;
    <label>数据日期：</label><input id="operationtimeParam" class='easyui-datebox' style="height: 23px; width:160px"/>&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;<br>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut()">导出</a>&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-edit" onclick="javascript:$('#datedialog').window('open')">数据打回</a>
</div>

<!--详情窗口-->
<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div align="center" style="padding-top: 30px">
        <form id="info_formForAdd" method="post">
            <input id="info_id" class="backId" name="id" type="hidden">
            <table class='enterTable' id='info_enterTable'>
                <tr> <td align='right'>
                    金融机构代码:</td>
                    <td>
                        <input id='info_financeorgcode' name='financeorgcode' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        内部机构号:
                    </td>
                    <td>
                        <input id='info_financeorginnum' name='financeorginnum' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    业务类型:</td>
                    <td>
                        <select class='easyui-combobox' id='info_ywlx' name='ywlx' disabled='disabled' style='width:173px;'>
                        	<option value='T01'>T01-同业存放</option>
							<option value='T011'>T011-活期存放</option>
							<option value='T012'>T012-定期存放</option>
							<option value='T02'>T02-存放同业</option>
							<option value='T021'>T021-活期存放</option>
							<option value='T022'>T022-定期存放</option>
							<option value='T03'>T03-同业存单发行</option>
							<option value='T04'>T04-同业存单投资</option>
                    	</select>
                    </td>
     <td align='right'>
                        交易对手证件类型:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_jydszjlx' name='jydszjlx' disabled='disabled' style='width:173px;'>
                        	<option value='A01'>A01-统一社会信用代码</option>
                        	<option value='A02'>A02-组织机构代码</option>
                        	<option value='C01'>C01-资管产品统计编码</option>
                        	<option value='C02'>C02-资管产品登记备案编码</option>
                        	<option value='L01'>L01-全球法人识别编码（LEI码）</option>
                        	<option value='Z99'>Z99-自定义码</option>
                        </select>
                    </td>
                </tr>
<tr> <td align='right'>
                    交易对手代码:</td>
                    <td>
                        <input id='info_jydsdm' name='jydsdm' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        存款账户编码:
                    </td>
                    <td>
                        <input id='info_ckzhbm' name='ckzhbm' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    存款协议代码:</td>
                    <td>
                        <input id='info_ckxydm' name='ckxydm' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        协议起始日期:
                    </td>
                    <td>
                        <input class='easyui-datebox' id='info_startdate' name='startdate' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    协议到期日期:</td>
                    <td>
                        <input class='easyui-datebox' id='info_enddate' name='enddate' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        币种:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_currency' name='currency' disabled='disabled' style='width:173px;'>
                    		<c:forEach items='${baseCurrencyList}' var='aa'>
                                <option value='${aa.currencycode}'>${aa.currencycode}-${aa.currencyname}</option>
                            </c:forEach>
                            </select>
                    </td>
                </tr>
<tr> <td align='right'>
                    交易金额:</td>
                    <td>
                        <input id='info_receiptbalance' name='receiptbalance' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        交易金额折人民币:
                    </td>
                    <td>
                        <input id='info_receiptcnybalance' name='receiptcnybalance' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    交易日期:</td>
                    <td>
                        <input class='easyui-datebox' id='info_jyrq' name='jyrq' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        交易流水号:
                    </td>
                    <td>
                        <input id='info_jylsh' name='jylsh' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    利率水平:</td>
                    <td>
                        <input id='info_llsp' name='llsp' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        交易账户号:
                    </td>
                    <td>
                        <input id='info_jyzhh' name='jyzhh' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    交易账户开户行号:</td>
                    <td>
                        <input id='info_jyzhkhhh' name='jyzhkhhh' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        交易对手账户号:
                    </td>
                    <td>
                        <input id='info_jydszhh' name='jydszhh' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    交易方向:</td>
                    <td>
                        <select class='easyui-combobox' id='info_jyfx' name='jyfx' disabled='disabled' style='width:173px;'>
                        	<option value='1'>1-发生</option>
                        	<option value='0'>0-结清</option>
                        </select>
                    </td>
     <td align='right'>
                        数据日期:
                    </td>
                    <td>
                        <input class='easyui-datebox' id='info_sjrq' name='sjrq' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<script type="text/javascript">
    var index = 5;
    var baseAreaAndCountrys = <%=baseAreaAndCountrys%>;
    var baseAreas = <%=baseAreas%>;
    var baseAindustrys = <%=baseAindustrys%>;
    var baseBindustrys = <%=baseBindustrys%>;
    var baseCurrencys = <%=baseCurrencys%>;
    var operationtimeParam = '<%=operationtimeParam%>';
    $(function(){
        $('#datedialog').window('close');
        $("#dg1").datagrid({
            loadMsg:'数据加载中,请稍后...',
            method:'post',
            url:'XGetHistoryXtyckfsexxH?operationtimeParam='+operationtimeParam,
            checkOnSelect:true,
            autoRowHeight:false,
            pagination:true,
            rownumbers:true,
            toolbar:'#tb1',
            fitColumns:false,
            pageSize:20,
            pageList:[15,20,30,50],
            columns:[[
            	{field:'ck',checkbox:true},
                {field:'id', title: '编号', width: 80, align: 'center', hidden: true},
                {field:'financeorgcode',title:'金融机构代码',width:150,align:'center'},
                {field:'financeorginnum',title:'内部机构号',width:150,align:'center'},
                {field:'ywlx',title:'业务类型',width:150,align:'center',formatter:function (value) {
                	if (value=='T01'){
                		  return "T01-同业存放";
                		}else if (value=='T011'){
                		  return "T011-活期存放";
                		}else if (value=='T012'){
                		  return "T012-定期存放";
                		}else if (value=='T02'){
                		  return "T02-存放同业";
                		}else if (value=='T021'){
                		  return "T021-活期存放";
                		}else if (value=='T022'){
                		  return "T022-定期存放";
                		}else if (value=='T03'){
                		  return "T03-同业存单发行";
                		}else if (value=='T04'){
                		  return "T04-同业存单投资";
                    }
                    return value;
                }},
                {field:'jydszjlx',title:'交易对手证件类型',width:150,align:'center',formatter:function (value) {
                	if (value=='A01'){
                		  return "A01-统一社会信用代码";
                		}else if (value=='A02'){
                		  return "A02-组织机构代码";
                		}else if (value=='C01'){
                		  return "C01-资管产品统计编码";
                		}else if (value=='C02'){
                		  return "C02-资管产品登记备案编码";
                		}else if (value=='L01'){
                		  return "L01-全球法人识别编码（LEI码）";
                		}else if (value=='Z99'){
                		  return "Z99-自定义码";
                    }
                    return value;
                }},
                {field:'jydsdm',title:'交易对手代码',width:150,align:'center'},
                {field:'ckzhbm',title:'存款账户编码',width:150,align:'center'},
                {field:'ckxydm',title:'存款协议代码',width:150,align:'center'},
                {field:'startdate',title:'协议起始日期',width:150,align:'center'},
                {field:'enddate',title:'协议到期日期',width:150,align:'center'},
                {field:'currency',title:'币种',width:150,align:'center',formatter:function (value) {
                	for (var i = 0;i <  baseCurrencys.length;i++){
                        if (baseCurrencys[i].substring(0,3)==value){
                            return baseCurrencys[i];
                        }
                    }
                }},
                {field:'receiptbalance',title:'交易金额',width:150,align:'center'},
                {field:'receiptcnybalance',title:'交易金额折人民币',width:150,align:'center'},
                {field:'jyrq',title:'交易日期',width:150,align:'center'},
                {field:'jylsh',title:'交易流水号',width:150,align:'center'},
                {field:'llsp',title:'利率水平',width:150,align:'center'},
                {field:'jyzhh',title:'交易账户号',width:150,align:'center'},
                {field:'jyzhkhhh',title:'交易账户开户行号',width:150,align:'center'},
                {field:'jydszhh',title:'交易对手账户号',width:150,align:'center'},
                {field:'jyfx',title:'交易方向',width:150,align:'center',formatter:function(value){
                	if (value=='1'){
              		  return "1-发生";
              		}else if (value=='0'){
              		  return "0-结清";
              		}
                  return value;
                }},
                {field:'sjrq',title:'数据日期',width:150,align:'center'},
                {field: 'operator', title: '操作人',  align: 'center'},
                {field: 'operationtime', title: '操作时间',  align: 'center'}
    	    ]],
            onDblClickRow: function (rowIndex, rowData) {
                $('#info_formForAdd').form('clear');
                $('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','同业存款发生额信息详情');
                $('#info_formForAdd').form('load',rowData);
            }
        });
        $('#operationtimeParam').datebox('setValue',operationtimeParam);
    })

    // 查询
    function searchOnSelected(){
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();

        var jydsdmParam = $("#jydsdmParam").val().trim();
        var jydszjlxParam = $("#jydszjlxParam").combobox("getValue");

        var operationtimeParam = $("#operationtimeParam").datebox("getValue");
        $("#dg1").datagrid("load", {"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"jydsdmParam" : jydsdmParam,"jydszjlxParam" : jydszjlxParam,"sjrqParam" : operationtimeParam});
    }

    // 导出
    function showOut(){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        var info = '';
        for( var dataIndex in row){
            id = id + row[dataIndex].id + ",";
        }
        if(id != ''){
            info = '是否将选中的数据导出到Excel表中？';
        }else {
            info = '是否将全部数据导出到Excel表中？'
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();

        var jydsdmParam = $("#jydsdmParam").val().trim();
        var jydszjlxParam = $("#jydszjlxParam").combobox("getValue");

        var operationtimeParam = $("#operationtimeParam").datebox("getValue");
        $.messager.confirm('操作提示', info, function (r) {
            if (r) {
                window.location.href = "XExportXtyckfsexxH?financeorgcodeParam="+financeorgcodeParam+"&contractcodeParam="+contractcodeParam+"&jydsdmParam="+jydsdmParam+"&jydszjlxParam="+jydszjlxParam+"&sjrqParam="+operationtimeParam +"&id="+id;
            }
        });
    }

    //数据打回
    function dataBack(){
        var v = $('#dateform').form('validate');
        if(v){
            var value = $('#dateselect').datebox('getValue');
            $('#datedialog').window('close');
            $.messager.confirm('提示','是否打回数据？',function (r){
                if(r){
                    $.messager.progress({
                        title: '请稍等',
                        msg: '数据处理中......'
                    });
                    $.ajax({
                        url: "XdatabackxtyckfsexxH",
                        type: "POST",
                        contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                        async: true,
                        data:{"date":value},
                        dataType:'json',
                        success: function (data) {
                            if(data != 0){
                                $.messager.progress('close');
                                $.messager.alert('操作提示', '数据打回成功！', 'info');
                                $("#dg1").datagrid("reload");
                            }else{
                                $.messager.progress('close');
                                $.messager.alert('操作提示', '没有数据可打回！', 'error');
                                $("#dg1").datagrid("reload");
                            }

                        },
                        error: function (error) {
                            $.messager.progress('close');
                            $.messager.alert('操作提示', '数据打回失败！', 'error');
                        }
                    })
                }
            });
        }

    }
</script>
</body>
</html>
