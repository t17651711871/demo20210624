<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String operationtimeParam = (String) session.getAttribute("operationtimeParam");
    if (StringUtils.isBlank(operationtimeParam)){
        operationtimeParam = "";
    }
    List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
    List<String> baseAreas = new ArrayList<>();
    for (BaseArea baseArea : baseAreaList) {
        baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
    }
    List<BaseCountry> baseCountryList = (List<BaseCountry>) request.getAttribute("baseCountryList");
    List<String>  baseCountrys = new ArrayList<>();
    for (BaseCountry baseCountry : baseCountryList) {
    	baseCountrys.add("'"+baseCountry.getCountrycode()+ "-" +baseCountry.getCountryname()+"'");
    }
    List<String>  baseAreaAndCountrys = new ArrayList<>();
    baseAreaAndCountrys.addAll(baseAreas);
    baseAreaAndCountrys.addAll(baseCountrys);
    List<BaseAindustry> baseAindustryList = (List<BaseAindustry>) request.getAttribute("baseAindustryList");
    List<String> baseAindustrys = new ArrayList<>();
    for (BaseAindustry baseAindustry : baseAindustryList) {
        baseAindustrys.add("'"+baseAindustry.getAindustrycode()+ "-" +baseAindustry.getAindustryname()+"'");
    }
    List<BaseBindustry> baseBindustryList = (List<BaseBindustry>) request.getAttribute("baseBindustryList");
    List<String> baseBindustrys = new ArrayList<>();
    for (BaseBindustry baseBindustry : baseBindustryList) {
        baseBindustrys.add("'"+baseBindustry.getBindustrycode()+ "-" +baseBindustry.getBindustryname()+"'");
    }
    List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
    List<String> baseCurrencys = new ArrayList<>();
    for (BaseCurrency baseCurrency : baseCurrencyList) {
        baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>同业借贷发生额信息</title>
    <%@include file="../../common/head.jsp"%>
</head>
<body>

<table id="dg1" style="height: 480px;" title="同业借贷发生额信息列表"></table>

<!-- 日期选择框 -->
<div id="datedialog" class="easyui-dialog" title="日期选择" style="width:400px;height:200px;padding-left:30px;padding-top:50px" data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="dateform" action="">
        请选择报文日期：<input id="dateselect" type="text" class="easyui-datebox" required="required"><br/><br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-ok" onclick="dataBack()">确定</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#datedialog').window('close')">取消</a>
    </form>
</div>

<!--表格工具栏-->
<div id="tb1" style="padding: 5px; height: auto;">
    <a class="easyui-linkbutton" href="XHistoryDataUi" iconCls="icon-return">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <label>金融机构代码：</label><input id="financeorgcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>合同编码：</label><input id="contractcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>交易对手代码类别：</label>
    <select class='easyui-combobox'  id="jydsdmlbParam" name="jydsdmlbParam" editable=false style="height: 23px; width:130px">
        <option value=''>=请选择=</option>
        <option value='A01'>A01-统一社会信用代码</option>
        <option value='A02'>A02-组织机构代码</option>
        <option value='C01'>C01-资管产品统计编码</option>
        <option value='C02'>C02-资管产品登记备案编码</option>
        <option value='L01'>L01-全球法人识别编码（LEI码）</option>
        <option value='Z99'>Z99-自定义码</option>
    </select>&nbsp;
    <label>交易对手代码：</label><input id="jydsdmParam" type="text" style="height: 23px; width:130px"/>
    &nbsp;
    <label>数据日期：</label><input id="operationtimeParam" class='easyui-datebox' style="height: 23px; width:160px"/>&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;<br>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut()">导出</a>&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-edit" onclick="javascript:$('#datedialog').window('open')">数据打回</a>
</div>

<!--详情窗口-->
<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div align="center" style="padding-top: 30px">
        <form id="info_formForAdd" method="post">
            <input id="info_id" class="backId" name="id" type="hidden">
            <table class='enterTable' id='info_enterTable'>
                <tr> <td align='right'>
                    金融机构代码:</td>
                    <td>
                        <input id='info_financeorgcode' name='financeorgcode' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        内部机构号:
                    </td>
                    <td>
                        <input id='info_financeorginnum' name='financeorginnum' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    交易对手代码:</td>
                    <td>
                        <select class='easyui-combobox' id='info_jydsdm' name='jydsdm' disabled="disabled" style='width:173px;'>
                    		<option value='A01'>A01-统一社会信用代码</option>
                        	<option value='A02'>A02-组织机构代码</option>
                        	<option value='C01'>C01-资管产品统计编码</option>
                        	<option value='C02'>C02-资管产品登记备案编码</option>
                        	<option value='L01'>L01-全球法人识别编码（LEI码）</option>
                        	<option value='Z99'>Z99-自定义码</option>
                        </select>
                    </td>
     <td align='right'>
                        交易对手代码类别:
                    </td>
                    <td>
                        <input id='info_jydsdmlb' name='jydsdmlb' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    合同编码:</td>
                    <td>
                        <input id='info_contractcode' name='contractcode' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        交易流水号:
                    </td>
                    <td>
                        <input id='info_jylsh' name='jylsh' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    资产负债类型:</td>
                    <td>
                        <select class='easyui-combobox' id='info_zcfzlx' name='zcfzlx' disabled='disabled' style='width:173px;'>
                        	<option value='AL01'>AL01-资产</option>
                        	<option value='AL02'>AL02-负债</option>
                        </select>
                    </td>
     <td align='right'>
                        产品类别:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_productcetegory' name='productcetegory' disabled='disabled' style='width:173px;'>
                        	<option value='F03'>F03-拆放同业/同业拆借</option>
                        	<option value='F06'>F06-买入返售/卖出回购</option>
                        	<option value='F061'>F061-债券买入返售/卖出回购</option>
                        	<option value='F062'>F062-票据买入返售/卖出回购</option>
                        	<option value='F063'>F063-贷款买入返售/卖出回购</option>
                        	<option value='F064'>F064-股票及其他股权买入返售/卖出回购</option>
                        	<option value='F065'>F065-黄金买入返售/卖出回购</option>
                        	<option value='F069'>F069-其他资产买入返售/卖出回购</option>
                        </select>
                    </td>
                </tr>
<tr> <td align='right'>
                    合同起始日期:</td>
                    <td>
                        <input class='easyui-datebox' id='info_startdate' name='startdate' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        合同到期日期:
                    </td>
                    <td>
                        <input class='easyui-datebox' id='info_enddate' name='enddate' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    合同实际终止日期:</td>
                    <td>
                        <input class='easyui-datebox' id='info_finaldate' name='finaldate' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        币种:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_currency' name='currency' disabled='disabled' style='width:173px;'>
                    		<c:forEach items='${baseCurrencyList}' var='aa'>
                                <option value='${aa.currencycode}'>${aa.currencycode}-${aa.currencyname}</option>
                            </c:forEach>
                            </select>
                    </td>
                </tr>
<tr> <td align='right'>
                    发生金额:</td>
                    <td>
                        <input id='info_receiptbalance' name='receiptbalance' disabled="disabled" style='width:173px;'/>
                    </td>
     <td align='right'>
                        发生金额折人民币:
                    </td>
                    <td>
                        <input id='info_receiptcnybalance' name='receiptcnybalance' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    利率是否固定:</td>
                    <td>
                        <select class='easyui-combobox' id='info_interestisfixed' name='interestisfixed' disabled='disabled' style='width:173px;'>
                        	<option value='RF01'>RF01-固定利率</option>
                        	<option value='RF02'>RF02-浮动利率</option>
                        </select>
                    </td>
     <td align='right'>
                        利率水平:
                    </td>
                    <td>
                        <input id='info_interestislevel' name='interestislevel' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    定价基准类型:</td>
                    <td>
                        <select class='easyui-combobox' id='info_fixpricetype' name='fixpricetype' disabled='disabled' style='width:173px;'>
                        	<option value='TR01'>TR01-上海银行间同业拆放利率</option>
                            <option value='TR02'>TR02-伦敦银行间同业拆放利率</option>
                            <option value='TR03'>TR03-香港银行间同业拆放利率</option>
                            <option value='TR04'>TR04-欧洲银行间同业拆放利率</option>
                            <option value='TR05'>TR05-贷款市场报价利率</option>
                            <option value='TR06'>TR06-中国国债收益率</option>
                            <option value='TR07'>TR07-人名币存款基准利率</option>
                            <option value='TR08'>TR08-人名币贷款基准利率</option>
                            <option value='TR99'>TR99-其他</option>
                        </select>
                    </td>
     <td align='right'>
                        基准利率:
                    </td>
                    <td>
                        <input id='info_baseinterest' name='baseinterest' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    计息方式:</td>
                    <td>
                        <select class='easyui-combobox' id='info_jxfs' name='jxfs' disabled='disabled' style='width:173px;'>
                        	<option value='B01'>B01-按月结息</option>
                        	<option value='B02'>B02-按季结息</option>
                        	<option value='B03'>B03-按年结息</option>
                        	<option value='B04'>B04-不定期结息</option>
                        	<option value='B05'>B05-不计利息</option>
                        	<option value='B99'>B99-其他</option>
                        </select>
                    </td>
                    <td align='right'>
                   发生/结清标识:</td>
                    <td>
                        <input id='info_fsjqbs' name='fsjqbs' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                     <td align='right'>
                        数据日期:
                    </td>
                    <td>
                        <input class='easyui-datebox' id='info_sjrq' name='sjrq' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<script type="text/javascript">
    var index = 5;
    var baseAreaAndCountrys = <%=baseAreaAndCountrys%>;
    var baseAreas = <%=baseAreas%>;
    var baseAindustrys = <%=baseAindustrys%>;
    var baseBindustrys = <%=baseBindustrys%>;
    var baseCurrencys = <%=baseCurrencys%>;
    var operationtimeParam = '<%=operationtimeParam%>';
    $(function(){
        $('#datedialog').window('close');
        $("#dg1").datagrid({
            loadMsg:'数据加载中,请稍后...',
            method:'post',
            url:'XGetHistoryXtyjdfsexxH?operationtimeParam='+operationtimeParam,
            checkOnSelect:true,
            autoRowHeight:false,
            pagination:true,
            rownumbers:true,
            toolbar:'#tb1',
            fitColumns:false,
            pageSize:20,
            pageList:[15,20,30,50],
            columns:[[
            	{field:'ck',checkbox:true},
                {field:'id', title: '编号', width: 80, align: 'center', hidden: true},
                {field:'financeorgcode',title:'金融机构代码',width:150,align:'center'},
                {field:'financeorginnum',title:'内部机构号',width:150,align:'center'},
                {field:'jydsdm',title:'交易对手代码',width:150,align:'center'},
                {field:'jydsdmlb',title:'交易对手代码类别',width:150,align:'center',formatter:function (value) {
                	if (value=='A01'){
              		  return "A01-统一社会信用代码";
              		}else if (value=='A02'){
              		  return "A02-组织机构代码";
              		}else if (value=='C01'){
              		  return "C01-资管产品统计编码";
              		}else if (value=='C02'){
              		  return "C02-资管产品登记备案编码";
              		}else if (value=='L01'){
              		  return "L01-全球法人识别编码（LEI码）";
              		}else if (value=='Z99'){
              		  return "Z99-自定义码";
              		}
              }},
                {field:'contractcode',title:'合同编码',width:150,align:'center'},
                {field:'jylsh',title:'交易流水号',width:150,align:'center'},
                {field:'zcfzlx',title:'资产负债类型',width:150,align:'center',formatter:function(value){
                	if(value == 'AL01'){
                		return "AL01-资产";
                	}else if(value == 'AL02'){
                		return "AL02-负债";
                	}
                }},
                {field:'productcetegory',title:'产品类别',width:150,align:'center',formatter:function(value){
                	if (value=='F03'){
              		  return "F03-拆放同业/同业拆借";
              		}else if (value=='F06'){
              		  return "F06-买入返售/卖出回购";
              		}else if (value=='F061'){
              		  return "F061-债券买入返售/卖出回购";
              		}else if (value=='F062'){
              		  return "F062-票据买入返售/卖出回购";
              		}else if (value=='F063'){
              		  return "F063-贷款买入返售/卖出回购";
              		}else if (value=='F064'){
              		  return "F064-股票及其他股权买入返售/卖出回购";
              		}else if (value=='F065'){
              		  return "F065-黄金买入返售/卖出回购";
              		}else if (value=='F069'){
              		  return "F069-其他资产买入返售/卖出回购";
              		}
              }},
                {field:'startdate',title:'合同起始日期',width:150,align:'center'},
                {field:'enddate',title:'合同到期日期',width:150,align:'center'},
                {field:'finaldate',title:'合同实际终止日期',width:150,align:'center'},
                {field:'currency',title:'币种',width:150,align:'center',formatter:function (value) {
                    for (var i = 0;i <  baseCurrencys.length;i++){
                        if (baseCurrencys[i].substring(0,3)==value){
                            return baseCurrencys[i];
                        }
                    }
                }},
                {field:'receiptbalance',title:'发生金额',width:150,align:'center'},
                {field:'receiptcnybalance',title:'发生金额折人民币',width:150,align:'center'},
                {field:'interestisfixed',title:'利率是否固定',width:150,align:'center',formatter:function(value){
                	if(value == 'RF01'){
                		return "RF01-固定利率";
                	}else if(value == 'RF02'){
                		return "RF02-浮动利率";
                	}
                }},
                {field:'interestislevel',title:'利率水平',width:150,align:'center'},
                {field:'fixpricetype',title:'定价基准类型',width:150,align:'center',formatter:function(value){
                	if (value=='TR01'){
              		  return "TR01-上海银行间同业拆放利率";
              		}else if (value=='TR02'){
              		  return "TR02-伦敦银行间同业拆借利率";
              		}else if (value=='TR03'){
              		  return "TR03-香港银行间同业拆借利率";
              		}else if (value=='TR04'){
              		  return "TR04-欧洲银行间同业拆借利率";
              		}else if (value=='TR05'){
              		  return "TR05-贷款市场报价利率";
              		}else if (value=='TR06'){
              		  return "TR06-中国国债收益率";
              		}else if (value=='TR07'){
              		  return "TR07-存款基准利率";
              		}else if (value=='TR08'){
              		  return "TR08-贷款基准利率";
              		}else if (value=='TR99'){
              		  return "TR99-其他";
              		}
              }},
                {field:'baseinterest',title:'基准利率',width:150,align:'center'},
                {field:'jxfs',title:'计息方式',width:150,align:'center',formatter:function(value){
                	if (value=='B01'){
              		  return "B01-按月结息";
              		}else if (value=='B02'){
              		  return "B02-按季结息";
              		}else if (value=='B03'){
              		  return "B03-按年结息";
              		}else if (value=='B04'){
              		  return "B04-不定期结息";
              		}else if (value=='B05'){
              		  return "B05-不计利息";
              		}else if (value=='B99'){
              		  return "B99-其他";
            			}
            	}},
            	{field:'fsjqbs',title:'发生/结清标识',width:150,align:'center'},
                {field:'sjrq',title:'数据日期',width:150,align:'center'},
                {field: 'operator', title: '操作人',  align: 'center'},
                {field: 'operationtime', title: '操作时间',  align: 'center'}
    	    ]],
            onDblClickRow: function (rowIndex, rowData) {
                $('#info_formForAdd').form('clear');
                $('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','同业借贷发生额信息详情');
                $('#info_formForAdd').form('load',rowData);
            }
        });
        $('#operationtimeParam').datebox('setValue',operationtimeParam);
    })

    // 查询
    function searchOnSelected(){
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();

        var jydsdmParam = $("#jydsdmParam").val().trim();
        var jydsdmlbParam = $("#jydsdmlbParam").combobox("getValue");

        var operationtimeParam = $("#operationtimeParam").datebox("getValue");
        $("#dg1").datagrid("load", {"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"jydsdmParam" : jydsdmParam,"jydsdmlbParam" : jydsdmlbParam,"sjrqParam" : operationtimeParam});
    }

    // 导出
    function showOut(){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        var info = '';
        for( var dataIndex in row){
            id = id + row[dataIndex].id + ",";
        }
        if(id != ''){
            info = '是否将选中的数据导出到Excel表中？';
        }else {
            info = '是否将全部数据导出到Excel表中？'
        }
        var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
        var contractcodeParam = $("#contractcodeParam").val().trim();

        var jydsdmParam = $("#jydsdmParam").val().trim();
        var jydsdmlbParam = $("#jydsdmlbParam").combobox("getValue");

        var operationtimeParam = $("#operationtimeParam").datebox("getValue");
        $.messager.confirm('操作提示', info, function (r) {
            if (r) {
                window.location.href = "XExportXtyjdfsexxH?financeorgcodeParam="+financeorgcodeParam+"&contractcodeParam="+contractcodeParam+"&jydsdmParam="+jydsdmParam+"&jydsdmlbParam="+jydsdmlbParam+"&sjrqParam="+operationtimeParam +"&id="+id;
            }
        });
    }

    //数据打回
    function dataBack(){
        var v = $('#dateform').form('validate');
        if(v){
            var value = $('#dateselect').datebox('getValue');
            $('#datedialog').window('close');
            $.messager.confirm('提示','是否打回数据？',function (r){
                if(r){
                    $.messager.progress({
                        title: '请稍等',
                        msg: '数据处理中......'
                    });
                    $.ajax({
                        url: "XdatabackxtyjdfsexxH",
                        type: "POST",
                        contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                        async: true,
                        data:{"date":value},
                        dataType:'json',
                        success: function (data) {
                            if(data != 0){
                                $.messager.progress('close');
                                $.messager.alert('操作提示', '数据打回成功！', 'info');
                                $("#dg1").datagrid("reload");
                            }else{
                                $.messager.progress('close');
                                $.messager.alert('操作提示', '没有数据可打回！', 'error');
                                $("#dg1").datagrid("reload");
                            }

                        },
                        error: function (error) {
                            $.messager.progress('close');
                            $.messager.alert('操作提示', '数据打回失败！', 'error');
                        }
                    })
                }
            });
        }

    }
</script>
</body>
</html>
