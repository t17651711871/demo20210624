<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
    String operationtimeParam = (String) session.getAttribute("operationtimeParam");
    if (StringUtils.isBlank(operationtimeParam)){
        operationtimeParam = "";
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>金融机构（法人）基础信息</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
<!-- 日期选择框 -->
<div id="datedialog" class="easyui-dialog" title="日期选择" style="width:400px;height:200px;padding-left:30px;padding-top:50px" data-options="iconCls:'icon-save',resizable:true,modal:true">
   	<form id="dateform" action="">
   	<input type="hidden" id="tableName" value="">
   	请选择报文日期：<input id="dateselect" type="text" class="easyui-datebox" required="required" editable=false><br/><br/>
   	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-ok" onclick="dataBack()">确定</a>&nbsp;&nbsp;&nbsp;&nbsp;
   	<a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#datedialog').window('close')">取消</a>
   	</form>
</div>
<!--表格工具栏-->
	<div id="tb0" style="padding: 5px; height: auto;">
	    <a class="easyui-linkbutton" href="XHistoryDataUi" iconCls="icon-return" onclick="fanhui()">返回</a>
	</div>
<div id="jrjg_tabs" class="easyui-tabs" style="height:500px;">
	<div title="金融机构（法人）基础信息-基础情况统计表" style="padding:10px;">
		<table id="dg1" style="height: 480px;" title="金融机构（法人）基础信息-基础情况统计表"></table>  
		<!--表格工具栏-->
		<div id="tb1" style="padding: 5px; height: auto;">			
			<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochu_jc()">导出</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-edit" onclick="javascript:$('#datedialog').dialog('open').window('center')">数据打回</a>
		</div>   
	</div>
	<div title="金融机构（法人）基础信息-资产负债及风险统计表" style="padding:10px;">       
		<table id="dg2" style="height: 480px;" title="金融机构（法人）基础信息-资产负债及风险统计表"></table>
		<!--表格工具栏-->
		<div id="tb2" style="padding: 5px; height: auto;">
			<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochu_zc()">导出</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-edit" onclick="javascript:$('#datedialog').dialog('open').window('center')">数据打回</a>
		</div>
	</div>
	<div title="金融机构（法人）基础信息-利润及资本统计表" style="padding:10px;">
		<table id="dg3" style="height: 480px;" title="金融机构（法人）基础信息-利润及资本统计表"></table> 
		<!--表格工具栏-->
		<div id="tb3" style="padding: 5px; height: auto;">
			<a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="daochu_lr()">导出</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-edit" onclick="javascript:$('#datedialog').dialog('open').window('center')">数据打回</a>
		</div>        
	</div>
</div>	
	
<!-- 导入文件dialog -->
    <div style="visibility: hidden;">
        <div id="importFileDialog" class="easyui-dialog" style="width: 600px; height: 160px; padding-left: 30px" title="导入数据">
            <h2>请选择要导入的Excel文件</h2>
            <table id="importTable" border="0">
                <tr>
                    <td><input type="file" name="file_info" id="file_info" size="60" onchange="fileSelected()" />&nbsp;</td>
                </tr>
                <tr>
                    <td><input type="button" id="importId" value="提交" size="60" onclick="fileUp()" /></td>
                </tr>
            </table>
        </div>
    </div>
<!-- 导出进度条 -->
<div style="visibility: hidden;">
  <div id="exportFileDialog" class="easyui-dialog" style="width:550px;height:100px;padding-left: 10px;top:200px;" title="导出数据"> 
	<div align="center"><span id="daorutishi">请您稍等,数据正在导出中......</span></div>
	<div id="exportFile" class="easyui-progressbar" style="width:500px;heigth:50px;"></div>
  </div>
</div>    	

<!-- 金融机构（法人）基础信息-基础情况统计表 详情 -->	
<div style="visibility: hidden;">
    <div id="dialog_m1" class="easyui-dialog" title="金融机构（法人）基础信息-基础情况统计表"  data-options="iconCls:'icon-more'" style="width:900px;height:620px;">
          
         <form id="formForMore1" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>金融机构名称 :</td>
                 <td><input class="easyui-validatebox" id="finorgname_m" name="finorgname" style="height: 23px; width:230px" disabled="disabled"/></td>
                
                 <td>金融机构代码:</td>
                 <td><input class="easyui-validatebox" id="finorgcode_m" name="finorgcode" style="height: 23px; width:230px" disabled="disabled"/></td>
              </tr>
                
               <tr> 
                 <td>金融机构编码 :</td>
                 <td><input class="easyui-validatebox" id="finorgnum_m" name="finorgnum" style="height: 23px; width:230px" disabled="disabled"/></td>
			   
			   <td>机构类别 :</td>
			     <td>
					<select class="easyui-combobox" id="jglb" name="jglb" editable=false style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项">
						<option value="A01">A01:中国人民银行</option>
						<option value="A02">A02:国家外汇管理局</option>
						<option value="B01">B01:中国银行保险监督管理委员会</option>
						<option value="B02">B02:中国证券监督管理委员会</option>
						<option value="C01">C01:开发性金融机构及政策性银行</option>
						<option value="C02">C02:国有商业银行</option>
						<option value="C03">C03:股份制商业银行</option>
						<option value="C04">C04:城市商业银行</option>
						<option value="C05">C05:农村商业银行</option>
						<option value="C06">C06:农村合作银行</option>
						<option value="C07">C07:村镇银行</option>
						<option value="C08">C08:农村信用社</option>
						<option value="C09">C09:城市信用合作社</option>
						<option value="C10">C10:农村资金互助社</option>
						<option value="C11">C11:财务公司</option>
						<option value="C12">C12:外资银行</option>
						<option value="D01">D01:信托公司</option>
						<option value="D02">D02:金融资产管理公司</option>
						<option value="D03">D03:金融租赁公司</option>
						<option value="D04">D04:汽车金融公司</option>
						<option value="D05">D05:贷款公司</option>
						<option value="D06">D06:货币经纪公司</option>
						<option value="D07">D07:消费金融公司</option>
						<option value="D08">D08:其他</option>
						<option value="E">E:证券业金融机构</option>
						<option value="F">F:保险业金融机构</option>
						<option value="G">G:交易及结算类金融机构</option>
						<option value="H">H:金融控股公司</option>
						<option value="Z">Z:其他</option>
	                </select>
				 </td>
			   </tr>
			   
			   <tr>
			   	 <td>注册地址 :</td>
			     <td><input class="easyui-validatebox" id="regaddress_m" name="regaddress" style="height: 23px; width:230px" disabled="disabled"/></td>
			     
			     <td>地区代码 :</td>
			     <td>
			        <input id="regarea_m" name="regarea" style="height: 23px; width:230px" data-options="valueField:'id',textField:'text'" disabled="disabled"/>
			     </td>
               </tr> 
			   
			   <tr>
			   	 <td>注册资本 :</td>
                 <td><input class="easyui-validatebox" id="regamt_m" name="regamt" style="height: 23px; width:230px" disabled="disabled"/></td>
                 
			     <td>成立日期 :</td>
			     <td><input id="setupdate_m" name="setupdate" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
               
               <tr>
			     <td>联系人 :</td>
                 <td><input class="easyui-validatebox" id="lxr_m" name="lxr" style="height: 23px; width:230px" data-options="required:true" missingMessage="必填项"/></td>
                 
                 <td>联系电话:</td>
                 <td><input class="easyui-validatebox" id="phone_m" name="phone" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>
			     <td>经营状态:</td>
                 <td>
                 <select id="orgmanagestatus_m" name="orgmanagestatus" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:正常运营</option>
						<option value="02">02:停业（歇业）</option>
						<option value="03">03:筹建</option>
						<option value="04">04:当年关闭</option>
						<option value="05">05:当年破产</option>
						<option value="06">06:当年注销</option>
						<option value="07">07:当年吊销</option>
						<option value="99">99:其他</option>
	                </select>
                 </td>
               
			     <td>经济成分:</td>
                 <td>
                 <select id="orgstoreconomy_m" name="orgstoreconomy" style="height: 23px; width:230px" disabled="disabled">
						<option value="A01">A01:国有控股</option>
						<option value="A0101">A0101:国有相对控股</option>
						<option value="A0102">A0102:国有绝对控股</option>
						<option value="A02">A02:集体控股</option>
						<option value="A0201">A0201:集体相对控股</option>
						<option value="A0202">A0202:集体绝对控股</option>
						<option value="B01">B01:私人控股</option>
						<option value="B0101">B0101:私人相对控股</option>
						<option value="B0102">B0102:私人绝对控股</option>
						<option value="B02">B02:港澳台控股</option>
						<option value="B0201">B0201:港澳台相对控股</option>
						<option value="B0202">B0202:港澳台绝对控股</option>
						<option value="B03">B03:外商控股</option>
						<option value="B0301">B0301:外商相对控股</option>
						<option value="B0302">B0302:外商绝对控股</option>
	                </select>
                 </td>
               </tr>
			   
			   <tr>
			     <!-- <td>行业分类 :</td>
			     <td>
			        <input id="industrycetegory_m" name="industrycetegory" style="height: 23px; width:230px" data-options="valueField:'id',textField:'text'" disabled="disabled"/>
			     </td> -->
			   
			     <td>企业规模:</td>
                 <td>
                 <select id="invermodel_m" name="invermodel" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:大型</option>
						<option value="02">02:中型</option>
						<option value="03">03:小型</option>
						<option value="04">04:微型</option>
	                </select>
                 </td>
               </tr>
			   
			   <tr>
			     <td>实际控制人名称 :</td>
			     <td><input class="easyui-validatebox" id="actctrlname_m" name="actctrlname" style="height: 23px; width:230px" disabled="disabled"/></td>
			     
                 <td>实际控制人证件类型:</td>
                 <td>                                                                                                                                                             
					<input class="easyui-validatebox" id='actctrlidtype_m' name='actctrlidtype' style="height: 23px; width:230px" disabled="disabled"/>
				 </td>
			   </tr>
			   
			   <tr>
			     <td>实际控制人证件代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="actctrlcode_m" name="actctrlcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>从业人员数 :</td>
			     <td>
			        <input class="easyui-validatebox" id="personnum_m" name="personnum" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第一大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="onestockcode_m" name="onestockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第一大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="onestockprop_m" name="onestockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第二大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="twostockcode_m" name="twostockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第二大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="twostockprop_m" name="twostockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第三大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="threestockcode_m" name="threestockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第三大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="threestockprop_m" name="threestockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第四大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="fourstockcode_m" name="fourstockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第四大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="fourstockprop_m" name="fourstockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第五大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="fivestockcode_m" name="fivestockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第五大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="fivestockprop_m" name="fivestockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第六大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="sixstockcode_m" name="sixstockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第六大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="sixstockprop_m" name="sixstockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第七大股东代码:</td>
			     <td>
			        <input class="easyui-validatebox" id="sevenstockcode_m" name="sevenstockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第七大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="sevenstockprop_m" name="sevenstockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第八大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="eightstockcode_m" name="eightstockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第八大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="eightstockprop_m" name="eightstockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第九大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="ninestockcode_m" name="ninestockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第九大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="ninestockprop_m" name="ninestockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>第十大股东代码 :</td>
			     <td>
			        <input class="easyui-validatebox" id="tenstockcode_m" name="tenstockcode" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   
			     <td>第十大股东持股比例 :</td>
			     <td>
			        <input class="easyui-validatebox" id="tenstockprop_m" name="tenstockprop" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
              </table>
           </div>
         </form>
	</div>
</div>
<!-- 金融机构（法人）基础信息-资产负债及风险统计表  详情-->
<div style="visibility: hidden;">
    <div id="dialog_m2" class="easyui-dialog" title="金融机构（法人）基础信息-资产负债及风险统计表"  data-options="iconCls:'icon-more'" style="width:520px;height:500px;">
          
         <form id="formForMore2" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>各项存款 :</td>
                 <td><input class="easyui-validatebox" id="gxck_m" name="gxck" style="height: 23px; width:230px" disabled="disabled"/></td>
                </tr> 
                 
               <tr>
                 <td>各项贷款:</td>
                 <td><input class="easyui-validatebox" id="gxdk_m" name="gxdk" style="height: 23px; width:230px" disabled="disabled"/></td>
              </tr>
                
               <tr> 
                 <td>资产总计 :</td>
                 <td><input class="easyui-validatebox" id="zczj_m" name="zczj" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>  
                 <td>负债总计 :</td>
                 <td><input class="easyui-validatebox" id="fzzj_m" name="fzzj" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
                
               <tr>  
                 <td>所有者权益合计 :</td>
                 <td><input class="easyui-validatebox" id="syzqyhj_m" name="syzqyhj" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>  
                 <td>生息资产 :</td>
                 <td><input class="easyui-validatebox" id="sxzc_m" name="sxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>
			     <td>付息负债 :</td>
			     <td><input class="easyui-validatebox" id="fxzc_m" name="fxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>流动性资产 :</td>
			     <td><input class="easyui-validatebox" id="ldxzc_m" name="ldxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>流动性负债 :</td>
			     <td>
			        <input class="easyui-validatebox" id="ldxfz_m" name="ldxfz" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>正常类贷款 :</td>
			     <td><input class="easyui-validatebox" id="zcldk_m" name="zcldk" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>关注类贷款 :</td>
			     <td>
			        <input class="easyui-validatebox" id="gzldk_m" name="gzldk" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>次级类贷款 :</td>
			     <td>
			        <input class="easyui-validatebox" id="cjldk_m" name="cjldk" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>可疑类贷款 :</td>
			     <td>
			        <input class="easyui-validatebox" id="jyldk_m" name="jyldk" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>损失类贷款 :</td>
			     <td>
			        <input class="easyui-validatebox" id="ssldk_m" name="ssldk" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>逾期贷款 :</td>
			     <td>
			        <input class="easyui-validatebox" id="yqdk_m" name="yqdk" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>逾期90天以上贷款 :</td>
			     <td>
			        <input class="easyui-validatebox" id="yqninetytysdk_m" name="yqninetytysdk" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>贷款减值准备 :</td>
			     <td>
			        <input class="easyui-validatebox" id="dkjzzb_m" name="dkjzzb" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
              </table>
           </div>
         </form>
	</div>
</div>
<!-- 金融机构（法人）基础信息-利润及资本统计表  详情-->
<div style="visibility: hidden;">
    <div id="dialog_m3" class="easyui-dialog" title="金融机构（法人）基础信息-利润及资本统计表"  data-options="iconCls:'icon-more'" style="width:520px;height:500px;">
          
         <form id="formForMore3" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>营业收入 :</td>
                 <td><input class="easyui-validatebox" id="yysr_m" name="yysr" style="height: 23px; width:230px" disabled="disabled"/></td>
                </tr> 
                 
               <tr>
                 <td>利息净收入 :</td>
                 <td><input class="easyui-validatebox" id="lxjsr_m" name="lxjsr" style="height: 23px; width:230px" disabled="disabled"/></td>
              </tr>
                
               <tr> 
                 <td>利息收入 :</td>
                 <td><input class="easyui-validatebox" id="lxsr_m" name="lxsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>  
                 <td>金融机构往来利息收入:</td>
                 <td><input class="easyui-validatebox" id="jrjgwllxsr_m" name="jrjgwllxsr" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
			   
               <tr>  
                 <td>其中：系统内往来利息收入:</td>
                 <td><input class="easyui-validatebox" id="xtnwllxsr_m" name="xtnwllxsr" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>  
                 <td>各项贷款利息收入 :</td>
                 <td><input class="easyui-validatebox" id="gxdklxsr_m" name="gxdklxsr" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>
			     <td>债券利息收入 :</td>
			     <td><input class="easyui-validatebox" id="zqlxsr_m" name="zqlxsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他利息收入 :</td>
			     <td><input class="easyui-validatebox" id="qtlxsr_m" name="qtlxsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>利息支出 :</td>
			     <td>
			        <input class="easyui-validatebox" id="lxzc_m" name="lxzc" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>金融机构往来利息支出 :</td>
			     <td><input class="easyui-validatebox" id="jrjgwllxzc_m" name="jrjgwllxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其中：系统内往来利息支出 :</td>
			     <td>
			        <input class="easyui-validatebox" id="xtnwllxzc_m" name="xtnwllxzc" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>各项存款利息支出:</td>
			     <td><input class="easyui-validatebox" id="gxcklxzc_m" name="gxcklxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>债券利息支出:</td>
			     <td><input class="easyui-validatebox" id="zqlxzc_m" name="zqlxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他利息支出:</td>
			     <td><input class="easyui-validatebox" id="qtlxzc_m" name="qtlxzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>手续费及佣金净收入:</td>
			     <td><input class="easyui-validatebox" id="sxfjyjjsr_m" name="sxfjyjjsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>手续费及佣金收入:</td>
			     <td><input class="easyui-validatebox" id="sxfjyjsr_m" name="sxfjyjsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>手续费及佣金支出:</td>
			     <td><input class="easyui-validatebox" id="jxfjyjzc_m" name="jxfjyjzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>租赁收益:</td>
			     <td><input class="easyui-validatebox" id="zlsy_m" name="zlsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>投资收益:</td>
			     <td><input class="easyui-validatebox" id="tzsy_m" name="tzsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>债券投资收益:</td>
			     <td><input class="easyui-validatebox" id="zqtzsy_m" name="zqtzsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>股权投资收益:</td>
			     <td><input class="easyui-validatebox" id="gqtzsy_m" name="gqtzsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他投资收益:</td>
			     <td><input class="easyui-validatebox" id="qttzsy_m" name="qttzsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>公允价值变动收益:</td>
			     <td><input class="easyui-validatebox" id="gyjzbdsy_m" name="gyjzbdsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>汇兑净收益:</td>
			     <td><input class="easyui-validatebox" id="hdjsy_m" name="hdjsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>资产处置收益:</td>
			     <td><input class="easyui-validatebox" id="zcczsy_m" name="zcczsy" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他业务收入:</td>
			     <td><input class="easyui-validatebox" id="qtywsr_m" name="qtywsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业支出:</td>
			     <td><input class="easyui-validatebox" id="yyzc_m" name="yyzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>业务及管理费:</td>
			     <td><input class="easyui-validatebox" id="ywjglf_m" name="ywjglf" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其中:职工工资:</td>
			     <td><input class="easyui-validatebox" id="zggz_m" name="zggz" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>福利费:</td>
			     <td><input class="easyui-validatebox" id="flf_m" name="flf" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>住房公积金和住房补贴:</td>
			     <td><input class="easyui-validatebox" id="zfgjjhzfbt_m" name="zfgjjhzfbt" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>税金及附加:</td>
			     <td><input class="easyui-validatebox" id="sjjfj_m" name="sjjfj" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>资产减值损失:</td>
			     <td><input class="easyui-validatebox" id="zcjzss_m" name="zcjzss" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>其他业务支出:</td>
			     <td><input class="easyui-validatebox" id="qtywzc_m" name="qtywzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业利润:</td>
			     <td><input class="easyui-validatebox" id="yylr_m" name="yylr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业外收入（加）:</td>
			     <td><input class="easyui-validatebox" id="yywsr_m" name="yywsr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业外支出（减）:</td>
			     <td><input class="easyui-validatebox" id="yywzc_m" name="yywzc" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>利润总额:</td>
			     <td><input class="easyui-validatebox" id="lrze_m" name="lrze" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>所得税（减）:</td>
			     <td><input class="easyui-validatebox" id="sds_m" name="sds" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>净利润:</td>
			     <td><input class="easyui-validatebox" id="jlr_m" name="jlr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>年度损益调整（加）:</td>
			     <td><input class="easyui-validatebox" id="ndsytz_m" name="ndsytz" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>留存利润:</td>
			     <td><input class="easyui-validatebox" id="lclr_m" name="lclr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>未分配利润:</td>
			     <td><input class="easyui-validatebox" id="wfplr_m" name="wfplr" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>应纳增值税:</td>
			     <td><input class="easyui-validatebox" id="ynzzs_m" name="ynzzs" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>核心一级资本净额:</td>
			     <td><input class="easyui-validatebox" id="hxyjzbje_m" name="hxyjzbje" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>一级资本净额:</td>
			     <td><input class="easyui-validatebox" id="yjzbje_m" name="yjzbje" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>资本净额:</td>
			     <td><input class="easyui-validatebox" id="zbje_m" name="zbje" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   <tr>
			     <td>应用资本底线及校准后的风险加权资产合计 :</td>
			     <td>
			        <input class="easyui-validatebox" id="ygzbjfxjqzchj_m" name="ygzbjfxjqzchj" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
              </table>
           </div>
         </form>
	</div>
</div>

<script type="text/javascript">
var operationtimeParam = '<%=operationtimeParam%>';
$(function(){
	$('#dialog_m1').dialog('close');
	$('#dialog_m2').dialog('close');
	$('#dialog_m3').dialog('close');
	$("#importFileDialog").dialog("close");
	$('#exportFileDialog').dialog('close');
	$('#datedialog').window('close')
	getbasecode();
	
	$('#jrjg_tabs').tabs({
		onSelect:function(id){
			var t1 = "金融机构（法人）基础信息-基础情况统计表";
			var t2 = "金融机构（法人）基础信息-资产负债及风险统计表";
			var t3 = "金融机构（法人）基础信息-利润及资本统计表";
			if(id == t1){
				$("#tableName").val("1");
				$("#dg1").datagrid({
					method:'post',
					url:'XGetHistoryXjrjgfrBaseinfoH?operationtimeParam='+operationtimeParam,
					loadMsg:'数据加载中,请稍后...',
					singleSelect:true,
					checkOnSelect:true,
					autoRowHeight:false,
					//fitColumns:false,
					pagination:true,
					rownumbers:true,
					toolbar:'#tb1',
					pageSize:20,
					pageList:[15,20,30,50],
					columns:[[
						    {field:'operationname',title:'操作名',width:100,align:'center',hidden:true},
						    {field:'nopassreason',title:'审核不通过原因',width:100,align:'center',hidden:true},
						    {field:'checkstatus',title:'校验结果',width:100,align:'center',formatter: function(value,row,index){					
								if(value == '2'){
									return '<span style="color:red;">' + '校验失败' + '</span>';
								}else if(value == '1'){
									return '校验成功';
								}else if(value == '0'){
									return '未校验';
								}else{
									return value;
								}
								},hidden:true},
						    {field:'finorgname',title:'金融机构名称',width:100,align:'center'},
						    {field:'finorgcode',title:'金融机构代码',width:100,align:'center'},
						    {field:'finorgnum',title:'金融机构编码',width:100,align:'center'},
						    {field:'regaddress',title:'注册地址',width:100,align:'center'},
						    {field:'regarea',title:'注册地行政区划代码',width:100,align:'center'},
						    {field:'regamt',title:'注册资本',width:100,align:'center'},
						    {field:'setupdate',title:'成立日期',width:100,align:'center'},
						    {field:'phone',title:'联系电话',width:100,align:'center'},
						    {field:'orgmanagestatus',title:'机构经营状态',width:100,align:'center'},
						    {field:'orgstoreconomy',title:'机构出资人经济成分',width:100,align:'center'},
						    {field:'industrycetegory',title:'行业分类',width:100,align:'center'},
						    {field:'invermodel',title:'企业规模',width:100,align:'center'},
						    {field:'actctrlidtype',title:'实际控制人身份类别',width:100,align:'center'},
						    {field:'actctrlname',title:'实际控制人名称',width:100,align:'center'},
						    {field:'actctrlcode',title:'实际控制人代码',width:100,align:'center'},
						    {field:'personnum',title:'员工数',width:100,align:'center'},
						    {field:'onestockcode',title:'第一大股东代码',width:100,align:'center'},
						    {field:'twostockcode',title:'第二大股东代码',width:100,align:'center'},
						    {field:'threestockcode',title:'第三大股东代码',width:100,align:'center'},
						    {field:'fourstockcode',title:'第四大股东代码',width:100,align:'center'},
						    {field:'fivestockcode',title:'第五大股东代码',width:100,align:'center'},
						    {field:'sixstockcode',title:'第六大股东代码',width:100,align:'center'},
						    {field:'sevenstockcode',title:'第七大股东代码',width:100,align:'center'},
						    {field:'eightstockcode',title:'第八大股东代码',width:100,align:'center'},
						    {field:'ninestockcode',title:'第九大股东代码',width:100,align:'center'},
						    {field:'tenstockcode',title:'第十大股东代码',width:100,align:'center'},
						    {field:'onestockprop',title:'第一大股东持股比例',width:100,align:'center'},
						    {field:'twostockprop',title:'第二大股东持股比例',width:100,align:'center'},
						    {field:'threestockprop',title:'第三大股东持股比例',width:100,align:'center'},
						    {field:'fourstockprop',title:'第四大股东持股比例',width:100,align:'center'},
						    {field:'fivestockprop',title:'第五大股东持股比例',width:100,align:'center'},
						    {field:'sixstockprop',title:'第六大股东持股比例',width:100,align:'center'},
						    {field:'sevenstockprop',title:'第七大股东持股比例',width:100,align:'center'},
						    {field:'eightstockprop',title:'第八大股东持股比例',width:100,align:'center'},
						    {field:'ninestockprop',title:'第九大股东持股比例',width:100,align:'center'},
						    {field:'tenstockprop',title:'第十大股东持股比例',width:100,align:'center'},
						    {field:'operator',title:'操作人',width:100,align:'center'},
						    {field:'operationtime',title:'操作时间',width:100,align:'center'},
						    {field:'sjrq',title:'数据日期',width:100,align:'center'}
				    ]],
				    onDblClickRow :function(rowIndex,rowData){
				    	initForm();
				    	var dia = $('#dialog_m1').dialog('open');
			        	$("#formForMore1").form('load',rowData);
			        	$('#dialog_m1').dialog({modal : true});	
			        	//$("#regarea_m").combobox({disabled: true});
			        	//$("#setupdate_m").datebox({disabled: true});
			        	//$("#orgmanagestatus_m").combobox({disabled: true});
			        	//$("#orgstoreconomy_m").combobox({disabled: true});
			        	//$("#industrycetegory_m").combobox({disabled: true});
			        	//$("#invermodel_m").combobox({disabled: true});
			        	//$("#actctrlidtype_m").combobox({disabled: true});
			        	//关闭弹窗默认隐藏div
			    		/* $(dia).window({
			    	    	onBeforeClose: function () {
			    	    		//初始化表单的元素的状态
			    	    		initForm("formForMore1");
			    	    	}
			    		}); */
				    }
				});
				//scrollShow($('#dg1'));
			}else if(id == t2){
				$("#tableName").val("2");
				$("#dg2").datagrid({
					method:'post',
					url:'XGetHistoryXjrjgfrAssetsH?operationtimeParam='+operationtimeParam,
					loadMsg:'数据加载中,请稍后...',
					singleSelect:true,
					checkOnSelect:true,
					autoRowHeight:false,
					//fitColumns:false,
					pagination:true,
					rownumbers:true,
					toolbar:'#tb2',
					pageSize:20,
					pageList:[15,20,30,50],
					columns:[[
						    {field:'operationname',title:'操作名',width:100,align:'center',hidden:true},
						    {field:'nopassreason',title:'审核不通过原因',width:100,align:'center',hidden:true},
						    {field:'checkstatus',title:'校验结果',width:100,align:'center',formatter: function(value,row,index){					
								if(value == '2'){
									return '<span style="color:red;">' + '校验失败' + '</span>';
								}else if(value == '1'){
									return '校验成功';
								}else if(value == '0'){
									return '未校验';
								}else{
									return value;
								}
								},hidden:true},
						    {field:'gxck',title:'各项存款',width:100,align:'center'},
						    {field:'gxdk',title:'各项贷款',width:100,align:'center'},
						    {field:'zczj',title:'资产总计',width:100,align:'center'},
						    {field:'fzzj',title:'负债总计',width:100,align:'center'},
						    {field:'syzqyhj',title:'所有者权益合计',width:100,align:'center'},
						    {field:'sxzc',title:'生息资产',width:100,align:'center'},
						    {field:'fxzc',title:'付息负债',width:100,align:'center'},
						    {field:'ldxzc',title:'流动性资产',width:100,align:'center'},
						    {field:'ldxfz',title:'流动性负债',width:100,align:'center'},
						    {field:'zcldk',title:'正常类贷款',width:100,align:'center'},
						    {field:'gzldk',title:'关注类贷款',width:100,align:'center'},
						    {field:'cjldk',title:'次级类贷款',width:100,align:'center'},
						    {field:'jyldk',title:'可疑类贷款',width:100,align:'center'},
						    {field:'ssldk',title:'损失类贷款',width:100,align:'center'},
						    {field:'yqdk',title:'逾期贷款',width:100,align:'center'},
						    {field:'yqninetytysdk',title:'逾期90天以上贷款',width:100,align:'center'},
						    {field:'dkjzzb',title:'贷款减值准备',width:100,align:'center'},
						    {field:'operator',title:'操作人',width:100,align:'center'},
						    {field:'operationtime',title:'操作时间',width:100,align:'center'},
						    {field:'sjrq',title:'数据日期',width:100,align:'center'}
				    ]],
				    onDblClickRow :function(rowIndex,rowData){
				    	initForm();
				    	var dia = $('#dialog_m2').dialog('open');
			        	$("#formForMore2").form('load',rowData);
			        	$('#dialog_m2').dialog({modal : true});	
			        	//关闭弹窗默认隐藏div
			    		/* $(dia).window({
			    	    	onBeforeClose: function () {
			    	    		//初始化表单的元素的状态
			    	    		initForm("formForMore2");
			    	    	}
			    		}); */
				    }
				});
				//scrollShow($('#dg2'));
			}else if(id == t3){
				$("#tableName").val("3");
				$("#dg3").datagrid({
					method:'post',
					url:'XGetHistoryXjrjgfrProfitH?operationtimeParam='+operationtimeParam,
					loadMsg:'数据加载中,请稍后...',
					singleSelect:true,
					checkOnSelect:true,
					autoRowHeight:false,
					//fitColumns:false,
					pagination:true,
					rownumbers:true,
					toolbar:'#tb3',
					pageSize:20,
					pageList:[15,20,30,50],
					columns:[[
						    {field:'operationname',title:'操作名',width:100,align:'center',hidden:true},
						    {field:'nopassreason',title:'审核不通过原因',width:100,align:'center',hidden:true},
						    {field:'checkstatus',title:'校验结果',width:100,align:'center',formatter: function(value,row,index){					
								if(value == '2'){
									return '<span style="color:red;">' + '校验失败' + '</span>';
								}else if(value == '1'){
									return '校验成功';
								}else if(value == '0'){
									return '未校验';
								}else{
									return value;
								}
								},hidden:true},
						    {field:'yysr',title:'营业收入',width:100,align:'center'},
						    {field:'lxjsr',title:'利息净收入',width:100,align:'center'},
						    {field:'lxsr',title:'利息收入',width:100,align:'center'},
						    {field:'jrjgwllxsr',title:'金融机构往来利息收入',width:100,align:'center'},
						    {field:'xtnwllxsr',title:'系统内往来利息收入',width:100,align:'center'},
						    {field:'gxdklxsr',title:'各项贷款利息收入',width:100,align:'center'},
						    {field:'zqlxsr',title:'债券利息收入',width:100,align:'center'},
						    {field:'qtlxsr',title:'其他利息收入',width:100,align:'center'},
						    {field:'lxzc',title:'利息支出',width:100,align:'center'},
						    {field:'jrjgwllxzc',title:'金融机构往来利息支出',width:100,align:'center'},
						    {field:'xtnwllxzc',title:'系统内往来利息支出',width:100,align:'center'},
						    {field:'gxcklxzc',title:'各项存款利息支出',width:100,align:'center'},
						    {field:'zqlxzc',title:'债券利息支出',width:100,align:'center'},
						    {field:'qtlxzc',title:'其他利息支出',width:100,align:'center'},
						    {field:'sxfjyjjsr',title:'手续费及佣金净收入',width:100,align:'center'},
						    {field:'sxfjyjsr',title:'手续费及佣金收入',width:100,align:'center'},
						    {field:'jxfjyjzc',title:'手续费及佣金支出',width:100,align:'center'},
						    {field:'zlsy',title:'租赁收益',width:100,align:'center'},
						    {field:'tzsy',title:'投资收益',width:100,align:'center'},
						    {field:'zqtzsy',title:'债券投资收益',width:100,align:'center'},
						    {field:'gqtzsy',title:'股权投资收益',width:100,align:'center'},
						    {field:'qttzsy',title:'其他投资收益',width:100,align:'center'},
						    {field:'gyjzbdsy',title:'公允价值变动收益',width:100,align:'center'},
						    {field:'hdjsy',title:'汇兑净收益',width:100,align:'center'},
						    {field:'zcczsy',title:'资产处置收益',width:100,align:'center'},
						    {field:'qtywsr',title:'其他业务收入',width:100,align:'center'},
						    {field:'yyzc',title:'营业支出',width:100,align:'center'},
						    {field:'ywjglf',title:'业务及管理费',width:100,align:'center'},
						    {field:'zggz',title:'职工工资',width:100,align:'center'},
						    {field:'flf',title:'福利费',width:100,align:'center'},
						    {field:'zfgjjhzfbt',title:'住房公积金和住房补贴',width:100,align:'center'},
						    {field:'sjjfj',title:'税金及附加',width:100,align:'center'},
						    {field:'zcjzss',title:'资产减值损失',width:100,align:'center'},
						    {field:'qtywzc',title:'其他业务支出',width:100,align:'center'},
						    {field:'yylr',title:'营业利润',width:100,align:'center'},
						    {field:'yywsr',title:'营业外收入（加）',width:100,align:'center'},
						    {field:'yywzc',title:'营业外支出（减）',width:100,align:'center'},
						    {field:'lrze',title:'利润总额',width:100,align:'center'},
						    {field:'sds',title:'所得税（减）',width:100,align:'center'},
						    {field:'jlr',title:'净利润',width:100,align:'center'},
						    {field:'ndsytz',title:'年度损益调整（加）',width:100,align:'center'},
						    {field:'lclr',title:'留存利润',width:100,align:'center'},
						    {field:'wfplr',title:'未分配利润',width:100,align:'center'},
						    {field:'ynzzs',title:'应纳增值税',width:100,align:'center'},
						    {field:'hxyjzbje',title:'核心一级资本净额',width:100,align:'center'},
						    {field:'yjzbje',title:'一级资本净额',width:100,align:'center'},
						    {field:'zbje',title:'资本净额',width:100,align:'center'},
						    {field:'ygzbjfxjqzchj',title:'应用资本底线及校准后的风险加权资产合计',width:100,align:'center'},
						    {field:'operator',title:'操作人',width:100,align:'center'},
						    {field:'operationtime',title:'操作时间',width:100,align:'center'},
						    {field:'sjrq',title:'数据日期',width:100,align:'center'}
				    ]],
				    onDblClickRow :function(rowIndex,rowData){
				    	initForm();
				    	var dia = $('#dialog_m3').dialog('open');
			        	$("#formForMore3").form('load',rowData);
			        	$('#dialog_m3').dialog({modal : true});	
			        	//关闭弹窗默认隐藏div
			    		/* $(dia).window({
			    	    	onBeforeClose: function () {
			    	    		//初始化表单的元素的状态
			    	    		initForm("formForMore3");
			    	    	}
			    		}); */
				    }
				});
				//scrollShow($('#dg3'));
			}
		}
	});
	
});

//获取基础代码
function getbasecode(){
	//getAindustry(); //一级行业代码
	//getArea(); //行政区划代码
	//getBindustry(); //二级行业代码
	//getCountry("customerIdType2"); //国家代码
	//getCurrency("artificialPersonType"); //币种代码
}

//获取一级行业代码
function getAindustry(){
	$.ajax({
	    url:'getAindustryCode',
	    type:'POST', //GET
	    async:true,    //或false,是否异步
	    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
	    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
	    success:function(data){
	    	var areaList = data.list;
		    if(areaList != null && areaList != ""){
		    	var jsonStr='[';
		    	for(var i = 0;i<areaList.length;i++){
		     		 jsonStr = jsonStr + '{"id":"'+areaList[i].aindustrycode+'",'+'"val":"'+areaList[i].aindustryname+'",'+'"text":"'+areaList[i].aindustrycode+'-'+areaList[i].aindustryname+'"},'
		        }
		    	jsonStr = jsonStr.substring(0,jsonStr.lastIndexOf(','));   //如果是以,结尾，则截取,前面的字符串
		        jsonStr = jsonStr + ']';
		    	
		        var jsonObj = JSON.parse(jsonStr);     //将json格式的字符串转为json对象
		 	    $("#industrycetegory_m").combobox("loadData",jsonObj);
		    }
	    }
	});
}

//获取行政区划代码
function getArea(){
	$.ajax({
	    url:'getAreaCode',
	    type:'POST', //GET
	    async:true,    //或false,是否异步
	    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
	    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
	    success:function(data){
	    	var areaList = data.list;
		    if(areaList != null && areaList != ""){
		    	var jsonStr='[';
		    	for(var i = 0;i<areaList.length;i++){
		     		 jsonStr = jsonStr + '{"id":"'+areaList[i].areacode+'",'+'"val":"'+areaList[i].areaname+'",'+'"text":"'+areaList[i].areacode+'-'+areaList[i].areaname+'"},'
		        }
		    	jsonStr = jsonStr.substring(0,jsonStr.lastIndexOf(','));   //如果是以,结尾，则截取,前面的字符串
		        jsonStr = jsonStr + ']';
		    	
		        var jsonObj = JSON.parse(jsonStr);     //将json格式的字符串转为json对象
		 	    $("#regarea_m").combobox("loadData",jsonObj);   
		    }
	    }
	});
}

function initForm(){
	//document.getElementById(id).reset(); 
	$("#finorgname_m").val('');
	$("#finorgcode_m").val('');
	$("#finorgnum_m").val('');
	$("#regaddress_m").val('');
	$("#regarea_m").val('');
	$("#regamt_m").val('');
	$("#setupdate_m").val('');
	$("#phone_m").val('');
	$("#orgmanagestatus_m").val('');
	$("#orgstoreconomy_m").val('');
	$("#industrycetegory_m").val('');
	$("#invermodel_m").val('');
	$("#actctrlidtype_m").val('');
	$("#actctrlname_m").val('');
	$("#actctrlcode_m").val('');
	$("#personnum_m").val('');
	$("#onestockcode_m").val('');
	$("#onestockprop_m").val('');
	$("#twostockcode_m").val('');
	$("#twostockprop_m").val('');
	$("#threestockcode_m").val('');
	$("#threestockprop_m").val('');
	$("#fourstockcode_m").val('');
	$("#fourstockprop_m").val('');
	$("#fivestockcode_m").val('');
	$("#fivestockprop_m").val('');
	$("#sixstockcode_m").val('');
	$("#sixstockprop_m").val('');
	$("#sevenstockcode_m").val('');
	$("#sevenstockprop_m").val('');
	$("#eightstockcode_m").val('');
	$("#eightstockprop_m").val('');
	$("#ninestockcode_m").val('');
	$("#ninestockprop_m").val('');
	$("#tenstockcode_m").val('');
	$("#tenstockprop_m").val('');
	$("#gxck_m").val('');
	$("#gxdk_m").val('');
	$("#zczj_m").val('');
	$("#fzzj_m").val('');
	$("#syzqyhj_m").val('');
	$("#sxzc_m").val('');
	$("#fxzc_m").val('');
	$("#ldxzc_m").val('');
	$("#ldxfz_m").val('');
	$("#zcldk_m").val('');
	$("#gzldk_m").val('');
	$("#cjldk_m").val('');
	$("#jyldk_m").val('');
	$("#ssldk_m").val('');
	$("#yqdk_m").val('');
	$("#yqninetytysdk_m").val('');
	$("#dkjzzb_m").val('');
	$("#yysr_m").val('');
	$("#lxjsr_m").val('');
	$("#lxsr_m").val('');
	$("#jrjgwllxsr_m").val('');
	$("#xtnwllxsr_m").val('');
	$("#gxdklxsr_m").val('');
	$("#zqlxsr_m").val('');
	$("#qtlxsr_m").val('');
	$("#lxzc_m").val('');
	$("#jrjgwllxzc_m").val('');
	$("#xtnwllxzc_m").val('');
	$("#gxcklxzc_m").val('');
	$("#zqlxzc_m").val('');
	$("#qtlxzc_m").val('');
	$("#sxfjyjjsr_m").val('');
	$("#sxfjyjsr_m").val('');
	$("#jxfjyjzc_m").val('');
	$("#zlsy_m").val('');
	$("#tzsy_m").val('');
	$("#zqtzsy_m").val('');
	$("#gqtzsy_m").val('');
	$("#qttzsy_m").val('');
	$("#gyjzbdsy_m").val('');
	$("#hdjsy_m").val('');
	$("#zcczsy").val('');
	$("#qtywsr_m").val('');
	$("#yyzc_m").val('');
	$("#ywjglf_m").val('');
	$("#zggz_m").val('');
	$("#flf_m").val('');
	$("#zfgjjhzfbt_m").val('');
	$("#sjjfj_m").val('');
	$("#zcjzss_m").val('');
	$("#qtywzc_m").val('');
	$("#yylr_m").val('');
	$("#yywsr_m").val('');
	$("#yywzc_m").val('');
	$("#lrze_m").val('');
	$("#sds_m").val('');
	$("#jlr_m").val('');
	$("#ndsytz_m").val('');
	$("#lclr_m").val('');
	$("#wfplr_m").val('');
	$("#ynzzs_m").val('');
	$("#hxyjzbje_m").val('');
	$("#yjzbje_m").val('');
	$("#zbje_m").val('');
	$("#ygzbjfxjqzchj_m").val('');
}

//查询
function chaxun(){
	var selectdbhtbm = $("#selectdbhtbm").val();
	$("#dg1").datagrid("load",{selectdbhtbm:selectdbhtbm});
}

//导出
function daochu_jc(){
	var rows = $("#dg1").datagrid('getSelections'); //获取选中行对象
	if(rows.length > 0){
		$.messager.confirm('操作提示','确认将选中的'+rows.length+'条数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
			    var exportExcelUrl = "XdaochujrjgfrBaseinfoH?selectid="+selectid;
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				window.location.href = "XdaochujrjgfrBaseinfoH?selectid="+selectid;
			}
		});
	}else{
		$.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
				var exportExcelUrl = "XdaochujrjgfrBaseinfoH";
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				window.location.href = "XdaochujrjgfrBaseinfoH?sjrqParam="+operationtimeParam;
			}
		});	
	}
}
function daochu_zc(){
	var rows = $("#dg2").datagrid('getSelections'); //获取选中行对象
	if(rows.length > 0){
		$.messager.confirm('操作提示','确认将选中的'+rows.length+'条数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
			    var exportExcelUrl = "XdaochujrjgfrAssetsH?selectid="+selectid;
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				window.location.href = "XdaochujrjgfrAssetsH?selectid="+selectid;
			}
		});
	}else{
		$.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
				var exportExcelUrl = "XdaochujrjgfrAssetsH";
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				window.location.href = "XdaochujrjgfrAssetsH?sjrqParam="+operationtimeParam;
			}
		});	
	}
}
function daochu_lr(){
	var rows = $("#dg3").datagrid('getSelections'); //获取选中行对象
	if(rows.length > 0){
		$.messager.confirm('操作提示','确认将选中的'+rows.length+'条数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
			    var exportExcelUrl = "XdaochujrjgfrProfitH?selectid="+selectid;
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
    			}
				window.location.href = "XdaochujrjgfrProfitH?selectid="+selectid;
			}
		});
	}else{
		$.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
			if(r){
				/* $('#exportFileDialog').dialog('open');
				$("#exportFileDialog").dialog({
					   closable: false
				});
				$('#exportFileDialog').dialog({modal:true});
				var scanTime = 2000; //请求间隔毫秒
				var interval = 2000;
				var isExportUrl = "exportJinDuTiao";
				var exportExcelUrl = "XdaochujrjgfrProfitH";
				exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval); */
				window.location.href = "XdaochujrjgfrProfitH?sjrqParam="+operationtimeParam;
			}
		});	
	}
}
/**
 * 导出excel（带进度条）
 * @param exportExcelUrl
 * @param scanTime 检测是否导出完毕请求间隔 单位毫秒
 * @param interval 进度条更新间隔（每次更新进度10%）  单位毫秒  导出时间越长 请设置越大 200 对应2秒导出时间
 */
function exportExcWithprogress(isExportUrl,exportExcelUrl,scanTime,interval){
    window.location.href = exportExcelUrl;
     var timer = setInterval(function(){
         $.ajax({
                url: isExportUrl,
                type:'POST', //GET
        	    async:true,    //或false,是否异步
                success: function(data){
                    if(data == "success"){
                        $('#exportFile').progressbar('setValue','100');
                        clearInterval(timer);
                        $('#exportFileDialog').dialog('close');
                        $('#exportFile').progressbar('setValue','0');
                    }else{
                    	var value = $('#exportFile').progressbar('getValue');
                    	if(value <= 100){
                    		value += Math.floor(Math.random() * 10);
                        	$('#exportFile').progressbar('setValue', value);
                    	}else{
                        	$('#exportFile').progressbar('setValue', '0');
                    	}
                    }
                },
                error:function(e){
                    console.log(e.responseText);
                }
            }); 
          }, scanTime);
}


//生成报文
function shengchengbaowen(){
	var rows = $('#dg1').datagrid('getSelections');
	/*if(rows.length > 0){
		$.messager.confirm('操作提示','确定将选中的数据生成报文吗',function(r){
			if(r){
				$.ajax({
		            url:'Xcreatemessagedkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            data:{"checktype":"some","rows":JSON.stringify(rows)},
		            //data:{"checktype":"some","checkid":checkid},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','生成报文成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则生成失败
			           		$.messager.alert('','生成报文失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           		//$('#dg1').datagrid('reload');
							//$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							//window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定将所有的数据生成报文吗',function(r){
			if(r){
				$.ajax({
		            url:'Xcreatemessagedkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"checktype":"all"},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','生成报文成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则生成失败
			           		$.messager.alert('','生成报文失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           		//$('#dg1').datagrid('reload');
							//$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							//window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}*/
	$.messager.confirm('提示','是否生成报文？',function (r){
		if(r){
			$.messager.progress({
				title: '请稍等',
				msg: '数据处理中......'
			});
			$.ajax({
				url: "XCreateReportFour",
				type: "POST",
				contentType: 'application/json;charset=UTF-8',
				async: true,
				success: function (data) {
					$.messager.progress('close');
					$.messager.alert('操作提示', '生成报文成功'+data+'条', 'info');
					$("#dg1").datagrid("reload");
				},
				error: function (error) {
					$.messager.progress('close');
					$.messager.alert('操作提示', '生成报文失败', 'error');
				}
			})
		}
	});
}

//校验
function jiaoyan(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要校验选中的数据吗',function(r){
			if(r){
				/* var checkid = "";
				for (var i = 0; i < rows.length; i++){
					checkid = checkid+rows[i].id+"-";
				} */
				$.ajax({
		            url:'XcheckjrjgfrBaseinfo',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            data:{"checktype":"some","rows":JSON.stringify(rows)},
		            //data:{"checktype":"some","checkid":checkid},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','校验成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则删除失败
			           		/* $.messager.alert('','校验失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		}); */
			           		$('#dg1').datagrid('reload');
							$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要校验所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'checkXdkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"checktype":"all"},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','校验成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则删除失败
			           		//$.messager.alert('操作提示','校验失败','error');
			           		$('#dg1').datagrid('reload');
							$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}

//数据打回
function dataBack(){
	var v = $('#dateform').form('validate');
	if(v){
		var value = $('#dateselect').datebox('getValue');
		var tableName = $("#tableName").val();
		var url = "";
		if(tableName == "1"){
			url = "XdatabackxjrjgfrbaseinfoH";
		}else if(tableName = "2"){
			url = "XdatabackxjrjgfrassetsH";
		}else if(tableName = "3"){
			url = "XdatabackxjrjgfrprofitH";
		}else{
			$.messager.alert('','未知表名','error');
			return;
		}
		$('#datedialog').window('close');
	    $.messager.confirm('提示','是否打回数据？',function (r){
	        if(r){
				$.messager.progress({
					title: '请稍等',
					msg: '数据处理中......'
				});
				$.ajax({
					url: url,
					type: "POST",
					contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
					async: true,
					data:{"date":value},
			        dataType:'json',
					success: function (data) {
						if(data != 0){
							$.messager.progress('close');
    						$.messager.alert('操作提示', '数据打回成功！', 'info');
    						$("#dg1").datagrid("reload");
						}else{
							$.messager.progress('close');
    						$.messager.alert('操作提示', '没有数据可打回！', 'error');
    						$("#dg1").datagrid("reload");
						}
						
					},
					error: function (error) {
						$.messager.progress('close');
						$.messager.alert('操作提示', '数据打回失败！', 'error');
					}
				})
	        }
	    });
	}
	
}

//返回
function fanhui(){
	$.messager.progress({
        title: '请稍等',
        msg: '数据正在加载中......'
    });    	
}
</script>
</body>
</html>
