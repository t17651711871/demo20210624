<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String operationtimeParam = (String) session.getAttribute("operationtimeParam");
    if (StringUtils.isBlank(operationtimeParam)){
        operationtimeParam = "";
    }
    List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
    List<String> baseAreas = new ArrayList<>();
    for (BaseArea baseArea : baseAreaList) {
        baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
    }
    List<BaseCountry> baseCountryList = (List<BaseCountry>) request.getAttribute("baseCountryList");
    List<String>  baseCountrys = new ArrayList<>();
    for (BaseCountry baseCountry : baseCountryList) {
    	baseCountrys.add("'"+baseCountry.getCountrycode()+ "-" +baseCountry.getCountryname()+"'");
    }
    List<String>  baseAreaAndCountrys = new ArrayList<>();
    baseAreaAndCountrys.addAll(baseAreas);
    baseAreaAndCountrys.addAll(baseCountrys);
    List<BaseAindustry> baseAindustryList = (List<BaseAindustry>) request.getAttribute("baseAindustryList");
    List<String> baseAindustrys = new ArrayList<>();
    for (BaseAindustry baseAindustry : baseAindustryList) {
        baseAindustrys.add("'"+baseAindustry.getAindustrycode()+ "-" +baseAindustry.getAindustryname()+"'");
    }
    List<BaseBindustry> baseBindustryList = (List<BaseBindustry>) request.getAttribute("baseBindustryList");
    List<String> baseBindustrys = new ArrayList<>();
    for (BaseBindustry baseBindustry : baseBindustryList) {
        baseBindustrys.add("'"+baseBindustry.getBindustrycode()+ "-" +baseBindustry.getBindustryname()+"'");
    }
    List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
    List<String> baseCurrencys = new ArrayList<>();
    for (BaseCurrency baseCurrency : baseCurrencyList) {
        baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>个人贷款发生额信息</title>
    <%@include file="../../common/head.jsp"%>
</head>
<body>

<table id="dg1" style="height: 480px;" title="个人贷款发生额信息列表"></table>

<!-- 日期选择框 -->
<div id="datedialog" class="easyui-dialog" title="日期选择" style="width:400px;height:200px;padding-left:30px;padding-top:50px" data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="dateform" action="">
        请选择报文日期：<input id="dateselect" type="text" class="easyui-datebox" required="required"><br/><br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-ok" onclick="dataBack()">确定</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#datedialog').window('close')">取消</a>
    </form>
</div>

<!--表格工具栏-->
<div id="tb1" style="padding: 5px; height: auto;">
    <a class="easyui-linkbutton" href="XHistoryDataUi" iconCls="icon-return">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <label>金融机构代码：</label><input id="finorgcodeParam" type="text" style="height: 23px; width:160px"/>&nbsp;
    <label>贷款合同编码：</label><input id="loancontractcodeParam" type="text" style="height: 23px; width:160px"/>&nbsp;
    <label>贷款借据编码：</label><input id="loanbrowcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>借款人证件代码：</label><input id="browidcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>借款人证件类型：</label>
    <select class='easyui-combobox'  id="isfarmerloanParam" name="isfarmerloanParam" editable=false style="height: 23px; width:130px">
        <option value=''>=请选择=</option>
        <option value='A01'>A01-统一社会信用代码</option>
        <option value='A02'>A02-组织机构代码</option>
        <option value='A03'>A03-其他</option>
        <option value='B01'>B01-身份证</option>
        <option value='B02'>B02-户口簿</option>
        <option value='B03'>B03-护照</option>
        <option value='B04'>B04-军官证</option>
        <option value='B05'>B05-士兵证</option>
        <option value='B06'>B06-港澳居民来往内地通行证</option>
        <option value='B07'>B07-台湾同胞来往内地通行证</option>
        <option value='B08'>B08-临时身份证</option>
        <option value='B09'>B09-外国人居留证</option>
        <option value='B010'>B010-警官证</option>
        <option value='B011'>B011-外国人永久居留身份证</option>
        <option value='B012'>B012-港澳台居民居住证</option>
        <option value='B99'>B99-其他证件</option>
    </select>
    &nbsp;
    <label>数据日期：</label><input id="operationtimeParam" class='easyui-datebox' style="height: 23px; width:160px"/>&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;<br>
    <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="showOut()">导出</a>&nbsp;&nbsp;&nbsp;
    <a class="easyui-linkbutton" iconCls="icon-edit" onclick="javascript:$('#datedialog').window('open')">数据打回</a>
</div>

<!--详情窗口-->
<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
    <div align="center" style="padding-top: 30px">
        <form id="info_formForAdd" method="post">
            <input id="info_id" class="backId" name="id" type="hidden">
            <table class='enterTable' id='info_enterTable'>
                <tr>
                    <td align='right'>
                        数据日期:
                    </td>
                    <td>
                        <input id='info_sjrq' name='sjrq' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        交易流水号:
                    </td>
                    <td>
                        <input id='info_transactionnum' name='transactionnum' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        金融机构代码:
                    </td>
                    <td>
                        <input id='info_finorgcode' name='finorgcode' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        内部机构号:
                    </td>
                    <td>
                        <input id='info_finorgincode' name='finorgincode' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        金融机构地区代码:
                    </td>
                    <td>
                        <input id='info_finorgareacode' name='finorgareacode' disabled="disabled" style='height: 23px; width:173px'>
                        </input>
                    </td>
                    <td align='right'>
                        借款人证件代码:
                    </td>
                    <td>
                        <input id='info_browidcode' name='browidcode' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        借款人地区代码:
                    </td>
                    <td>
                        <input id='info_browareacode' name='browareacode' disabled="disabled" style='height: 23px; width:173px' >
                    </td>
                    <td align='right'>
                        借款人证件类型:
                    </td>
                    <td>
                        <select id='info_isfarmerloan' name='isfarmerloan' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='A01'>A01-统一社会信用代码</option>
                            <option value='A02'>A02-组织机构代码</option>
                            <option value='A03'>A03-其他</option>
                            <option value='B01'>B01-身份证</option>
                            <option value='B02'>B02-户口簿</option>
                            <option value='B03'>B03-护照</option>
                            <option value='B04'>B04-军官证</option>
                            <option value='B05'>B05-士兵证</option>
                            <option value='B06'>B06-港澳居民来往内地通行证</option>
                            <option value='B07'>B07-台湾同胞来往内地通行证</option>
                            <option value='B08'>B08-临时身份证</option>
                            <option value='B09'>B09-外国人居留证</option>
                            <option value='B010'>B010-警官证</option>
                            <option value='B011'>B011-外国人永久居留身份证</option>
                            <option value='B012'>B012-港澳台居民居住证</option>
                            <option value='B99'>B99-其他证件</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款借据编码:
                    </td>
                    <td>
                        <input id='info_loanbrowcode' name='loanbrowcode' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        贷款合同编码:
                    </td>
                    <td>
                        <input id='info_loancontractcode' name='loancontractcode' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款产品类别:
                    </td>
                    <td>
                        <select id='info_loanprocode' name='loanprocode' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='F0211'>F0211-个人住房贷款</option>
                            <option value='F0212'>F0212-个人汽车消费贷款</option>
                            <option value='F02131'>F02131-国家助学贷款</option>
                            <option value='F02132'>F02132-一般商业性助学贷款</option>
                            <option value='F0219'>F021-其他消费贷款</option>
                            <option value='F022'>F022-经营贷款</option>
                            <option value='F023'>F023-固定资产贷款</option>
                            <option value='F03'>F03-拆借</option>
                            <option value='F041'>F041-账户透支</option>
                            <option value='F042'>F402-货记卡透支</option>
                            <option value='F043'>F403-准货记卡透支</option>
                            <option value='F061'>F061-债券回购/返售</option>
                            <option value='F062'>F062-票据回购/返售</option>
                            <option value='F063'>F063-贷款回购/返售</option>
                            <option value='F064'>F064-股票及其他股权回购/返售</option>
                            <option value='F065'>F065-黄金回购/返售</option>
                            <option value='F069'>F069-其他资产回购/返售</option>
                            <option value='F081'>F081-国际贸易融资</option>
                            <option value='F082'>F082-国内贸易融资</option>
                            <option value='F09'>F09-融资租赁</option>
                            <option value='F12'>F12-并购贷款</option>
                            <option value='F13'>F13-其他债券投资</option>
                        </select>
                    </td>
                    <td align='right'>
                        贷款重组方式:
                    </td>
                    <td>
                        <select id='info_loanrestructuring' name='loanrestructuring' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='01'>01-续贷</option>
                            <option value='02'>02-借新还旧</option>
                            <option value='09'>09-其他</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款发放日期:
                    </td>
                    <td>
                        <input id='info_loanstartdate' name='loanstartdate' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        贷款到期日期:
                    </td>
                    <td>
                        <input id='info_loanenddate' name='loanenddate' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款实际终止日期:
                    </td>
                    <td>
                        <input id='info_loanactenddate' name='loanactenddate' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        贷款币种:
                    </td>
                    <td>
                        <input id='info_loancurrency' name='loancurrency' disabled="disabled" style='height: 23px; width:173px'>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款发生金额:
                    </td>
                    <td>
                        <input id='info_loanamt' name='loanamt' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        贷款发生金额折人民币:
                    </td>
                    <td>
                        <input id='info_loancnyamt' name='loancnyamt' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        利率是否固定:
                    </td>
                    <td>
                        <select id='info_rateisfix' name='rateisfix' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='RF01'>RF01-固定利率</option>
                            <option value='RF02'>RF02-浮动利率</option>
                        </select>
                    </td>
                    <td align='right'>
                        利率水平:
                    </td>
                    <td>
                        <input id='info_ratelevel' name='ratelevel' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款定价基准类型:
                    </td>
                    <td>
                        <select id='info_loanfixamttype' name='loanfixamttype' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='TR01'>TR01-上海银行间同业拆放利率</option>
                            <option value='TR02'>TR02-伦敦银行间同业拆放利率</option>
                            <option value='TR03'>TR03-香港银行间同业拆放利率</option>
                            <option value='TR04'>TR04-欧洲银行间同业拆放利率</option>
                            <option value='TR05'>TR05-贷款市场报价利率</option>
                            <option value='TR06'>TR06-中国国债收益率</option>
                            <option value='TR07'>TR07-人名币存款基准利率</option>
                            <option value='TR08'>TR08-人名币贷款基准利率</option>
                            <option value='TR09'>TR09-再贷款利率</option>
                            <option value='TR99'>TR99-其他</option>
                        </select>
                    </td>
                    <td align='right'>
                        基准利率:
                    </td>
                    <td>
                        <input id='info_rate' name='rate' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款财政扶持方式:
                    </td>
                    <td>
                        <select id='info_loanfinancesupport' name='loanfinancesupport' disabled="disabled" style='height: 23px; width:173px' >
                            <option value='A0101'>A0101-中央政府全额贴息</option>
                            <option value='A0102'>A0102-中央政府部分贴息</option>
                            <option value='A0201'>A0201-地方政府全额贴息</option>
                            <option value='A0202'>A0202-地方政府部分贴息</option>
                            <option value='B'>B-税前提取准备</option>
                            <option value='C'>C-联合贴息</option>
                            <option value='Z'>Z-其他</option>
                        </select>
                    </td>
                    <td align='right'>
                        贷款利率重新定价日:
                    </td>
                    <td>
                        <input id='info_loanraterepricedate' name='loanraterepricedate' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        贷款担保方式:
                    </td>
                    <td>
                        <select id='info_gteemethod' name='gteemethod' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='A'>A-质押贷款</option>
                            <option value='B01'>B01-房地产抵押贷款</option>
                            <option value='B99'>B99-其他抵押贷款</option>
                            <option value='C01'>C01-联保贷款</option>
                            <option value='C99'>C99-其他保证贷款</option>
                            <option value='D'>D-信用/免担保贷款</option>
                            <option value='E'>E-组合担保</option>
                            <option value='Z'>Z-其他</option>
                        </select>
                    </td>
                    <td align='right'>
                        贷款状态:
                    </td>
                    <td>
                        <select id='info_loanstatus' name='loanstatus' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='LF01'>LF01-正常</option>
                            <option value='LF02'>LF02-核銷</option>
                            <option value='LF03'>LF03-剥离</option>
                            <option value='LF04'>LF04-转让</option>
                            <option value='LF05'>LF05-重组</option>
                            <option value='LF06'>LF06-以物抵债</option>
                            <option value='LF07'>LF07-资产证券化转让</option>
                            <option value='LF08'>LF08-债转股</option>
                            <option value='LF99'>LF99-其他</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        发放/收回标识:
                    </td>
                    <td>
                        <select id='info_givetakeid' name='givetakeid' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='0'>0-收回</option>
                            <option value='1'>1-发放</option>
                        </select>
                    </td>
                    <td align='right'>
                        贷款用途:
                    </td>
                    <td>
                        <input id='info_issupportliveloan' name='issupportliveloan' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        是否首次贷款:
                    </td>
                    <td>
                        <select id='info_isplatformloan' name='isplatformloan' disabled="disabled" style='height: 23px; width:173px'>
                            <option value='1'>1-是</option>
                            <option value='0'>0-否</option>
                        </select>
                    </td>
                    <td align='right'>
                        资产证券化产品代码:
                    </td>
                    <td>
                        <input id='info_assetproductcode' name='assetproductcode' disabled="disabled" style='width:173px;'/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<script type="text/javascript">
    var index = 5;
    var baseAreaAndCountrys = <%=baseAreaAndCountrys%>;
    var baseAreas = <%=baseAreas%>;
    var baseAindustrys = <%=baseAindustrys%>;
    var baseBindustrys = <%=baseBindustrys%>;
    var baseCurrencys = <%=baseCurrencys%>;
    var operationtimeParam = '<%=operationtimeParam%>';
    $(function(){
        $('#datedialog').window('close');
        $("#dg1").datagrid({
            loadMsg:'数据加载中,请稍后...',
            method:'post',
            url:'XGetHistoryXgrdkfsxxH?operationtimeParam='+operationtimeParam,
            checkOnSelect:true,
            autoRowHeight:false,
            pagination:true,
            rownumbers:true,
            toolbar:'#tb1',
            fitColumns:false,
            pageSize:20,
            pageList:[15,20,30,50],
            columns:[[
            	{field:'ck',checkbox:true},
                {field:'id', title: '编号', width: 80, align: 'center', hidden: true},
                {field:'sjrq',title:'数据日期',width:150,align:'center'},
                {field:'finorgcode',title:'金融机构代码',width:150,align:'center'},
                {field:'finorgincode',title:'内部机构号',width:150,align:'center'},
                {field:'finorgareacode',title:'金融机构地区代码',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseAreas.length;i++){
                            if (baseAreas[i].substring(0,6)==value){
                                return baseAreas[i];
                            }
                        }
                    }},
                {field:'isfarmerloan',title:'借款人证件类型',width:150,align:'center',formatter:function (value) {
                        if (value=='A01'){
                            return "A01-统一社会信用代码";
                        }else if(value=='A02'){
                            return "A02-组织机构代码";
                        }else if (value=='A03'){
                            return "A03-其他";
                        }else if (value =='B01'){
                            return "B01-身份证";
                        }else if (value =='B02'){
                            return "B02-户口簿";
                        }else if (value =='B03'){
                            return "B03-护照";
                        }else if (value =='B04'){
                            return "B04-军官证";
                        }else if (value =='B05'){
                            return "B05-士兵证";
                        }else if (value =='B06'){
                            return "B06-港澳居民来往内地通行证";
                        }else if (value =='B07'){
                            return "B07-台湾同胞来往内地通行证";
                        }else if (value =='B08'){
                            return "B08-临时身份证";
                        }else if (value =='B09'){
                            return "B09-外国人居留证";
                        }else if (value =='B010'){
                            return "B10-警官证";
                        }else if (value =='B011'){
                            return "B11-外国人永久居留身份证";
                        } else if (value =='B012'){
                            return "B12-港澳台居民居住证";
                        } else if (value =='B99'){
                            return "B99-其他证件";
                        }
                        return value;
                    }},
                {field:'browidcode',title:'借款人证件代码',width:150,align:'center'},
                {field:'browareacode',title:'借款人地区代码',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseAreaAndCountrys.length;i++){
                            if (baseAreaAndCountrys[i].substring(0,6)==value){
                                return baseAreaAndCountrys[i];
                            }
                        }
                    }},
                {field:'loanbrowcode',title:'贷款借据编码',width:150,align:'center'},
                {field:'loancontractcode',title:'贷款合同编码',width:150,align:'center'},
                {field:'loanprocode',title:'贷款产品类别',width:150,align:'center',formatter:function (value) {
                        if(value=='F0211'){
                            return "F0211-个人住房贷款";
                        }else if(value=="F0212"){
                            return "F0212-个人汽车消费贷款";
                        }else if(value=="F02131"){
                            return "F02131-国家助学贷款";
                        }else if(value=="F02132"){
                            return "F02132-一般商业性助学贷款";
                        }else if(value=="F0219"){
                            return "F0219-其他消费贷款";
                        } else if(value=='F022'){
                            return "F022-经营贷款";
                        }else if (value=='F023'){
                            return "F023-固定资产贷款";
                        }else if (value=='F03'){
                            return "F03-拆借";
                        }else if(value=='F041'){
                            return "F041-账户透支";
                        }else if(value=="F042"){
                            return "F402-货记卡透支";
                        }else if(value=="F043"){
                            return "F403-准货记卡透支";
                        }else if(value=='F061'){
                            return "F061-债券回购/返售";
                        }else if(value=='F062'){
                            return "F062-票据回购/返售";
                        }else if(value=='F063'){
                            return "F063-贷款回购/返售";
                        }else if(value=='F064'){
                            return "F064-股票及其他股权回购/返售";
                        }else if(value=='F065'){
                            return "F065-黄金回购/返售";
                        }else if(value=='F069'){
                            return "F069-其他资产回购/返售";
                        }else if(value=='F081'){
                            return "F081-国际贸易融资";
                        }else if(value=='F082'){
                            return "F082-国内贸易融资";
                        }else if (value=='F09'){
                            return "F09-融资租赁";
                        }else if (value=='F12'){
                            return "F12-并购贷款";
                        }else if(value=='F13'){
                            return "F13-其他债券投资";
                        }
                        return value;
                    }},
                {field:'loanstartdate',title:'贷款发放日期',width:150,align:'center'},
                {field:'loanenddate',title:'贷款到期日期',width:150,align:'center'},
                {field:'loanactenddate',title:'贷款实际终止日期',width:150,align:'center'},
                {field:'loancurrency',title:'贷款币种',width:150,align:'center',formatter:function (value) {
                        for (var i = 0;i <  baseCurrencys.length;i++){
                            if (baseCurrencys[i].substring(0,3)==value){
                                return baseCurrencys[i];
                            }
                        }
                    }},
                {field:'loanamt',title:'贷款发生金额',width:150,align:'center'},
                {field:'loancnyamt',title:'贷款发生金额折人民币',width:150,align:'center'},
                {field:'rateisfix',title:'利率是否固定',width:150,align:'center',formatter:function (value) {
                   		if (value=='RF01'){
                        	return "RF01-固定利率";
                    	}else if(value=='RF02'){
                        	return "RF02-浮动利率";
                    	}
                   		return value;
                }},
                {field:'ratelevel',title:'利率水平',width:150,align:'center'},
                {field:'loanfixamttype',title:'贷款定价基准类型',width:150,align:'center',formatter:function (value) {
                        if (value=='TR01'){
                            return "TR01-上海银行间同业拆放利率";
                        }else if(value=='TR02'){
                            return "TR02-伦敦银行间同业拆放利率";
                        }else if (value=='TR03'){
                            return "TR03-香港银行间同业拆放利率";
                        }else if(value=='TR04'){
                            return "TR04-欧洲银行间同业拆放利率";
                        }else if (value=='TR05'){
                            return "TR05-贷款市场报价利率";
                        }else if (value=='TR06'){
                            return "TR06-中国国债收益率";
                        }else if(value=='TR07'){
                            return "TR07-人名币存款基准利率";
                        }else if (value=='TR08'){
                            return "TR08-人名币贷款基准利率";
                        }else if (value=='TR09'){
                            return "TR09-再贷款利率";
                        }else if (value=='TR99'){
                            return "TR99-其他";
                        }
                        return value;
                    }},
                {field:'rate',title:'基准利率',width:150,align:'center'},
                {field:'loanfinancesupport',title:'贷款财政扶持方式',width:150,align:'center',formatter:function (value) {
                        if (value=='A0101'){
                            return "A0101-中央政府全额贴息";
                        }else if(value=='A0102'){
                            return "A0102-中央政府部分贴息";
                        }else if (value=='A0201'){
                            return "A0201-地方政府全额贴息";
                        }else if(value=='A0202'){
                            return "A0202-地方政府部分贴息";
                        }else if (value=='B'){
                            return "B-税前提取准备";
                        }else if (value=='C'){
                            return "C-联合贴息";
                        }else if (value=='Z'){
                            return "Z-其他";
                        }
                        return value;
                    }},
                {field:'loanraterepricedate',title:'贷款利率重新定价日',width:150,align:'center'},
                {field:'gteemethod',title:'贷款担保方式',width:150,align:'center',formatter:function (value) {
                        if (value=='A'){
                            return "A-质押贷款";
                        }else if (value=='B01'){
                            return "B01-房地产抵押贷款";
                        }else if(value=='B99'){
                            return "B99-其他抵押贷款";
                        }else if (value=='C01'){
                            return "C01-联保贷款";
                        }else if(value=='C99'){
                            return "C99-其他保证贷款";
                        }else if (value=='D'){
                            return "D-信用/免担保贷款";
                        }else if (value=='E'){
                            return "E-组合担保";
                        }else if (value=='Z'){
                            return "Z-其他";
                        }
                        return value;
                    }},
                {field:'loanstatus',title:'贷款状态',width:150,align:'center',formatter:function (value) {
                        if (value=='LF01'){
                            return "LF01-正常";
                        }else if(value=='LF02'){
                            return "LF02-核銷";
                        }else if(value=='LF03'){
                            return "LF03-剥离";
                        }else if(value=='LF04'){
                            return "LF04-转让";
                        }else if(value=='LF05'){
                            return "LF05-重组";
                        }else if(value=='LF06'){
                            return "LF06-以物抵债";
                        }else if(value=='LF07'){
                            return "LF07-资产证券化转让";
                        }else if(value=='LF08'){
                            return "LF08-债转股";
                        }else if(value=='LF99'){
                            return "LF99-其他";
                        }
                        return value;
                    }},
                {field:'givetakeid',title:'发放/收回标识',width:150,align:'center',formatter:function (value) {
                        if (value=='1'){
                            return "1-发放";
                        }else if(value=='0'){
                            return "0-收回";
                        }
                        return value;
                    }},
                {field:'isplatformloan',title:'是否首次贷款',width:150,align:'center',formatter:function (value) {
                        if (value=='1'){
                            return "1-是";
                        }else if(value=='0'){
                            return "0-否";
                        }
                        return value;
                    }},
                {field:'issupportliveloan',title:'贷款用途',width:150,align:'center'},
                {field:'assetproductcode',title:'资产证券化产品代码',width:150,align:'center'},
                {field:'loanrestructuring',title:'贷款重组方式',width:150,align:'center',formatter:function (value) {
                        if (value=='01'){
                            return "01-续贷";
                        }else if(value=='02'){
                            return "02-借新还旧";
                        }else if(value=='09'){
                            return "09-其他";
                        }
                        return value;
                    }},
                {field:'transactionnum',title:'交易流水号',width:150,align:'center'},
                {field: 'operator', title: '操作人',  align: 'center'},
                {field: 'operationtime', title: '操作时间',  align: 'center'}
    	    ]],
            onDblClickRow: function (rowIndex, rowData) {
                $('#info_formForAdd').form('clear');
                $('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','个人贷款发生额信息详情');
                $('#info_formForAdd').form('load',rowData);
            }
        });
        $('#operationtimeParam').datebox('setValue',operationtimeParam);
    })

    // 查询
    function searchOnSelected(){
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();

        var loanbrowcodeParam = $("#loanbrowcodeParam").val().trim();
        var browidcodeParam = $("#browidcodeParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");

        var operationtimeParam = $("#operationtimeParam").datebox("getValue");
        $("#dg1").datagrid("load", {"finorgcodeParam" : finorgcodeParam,"loancontractcodeParam" : loancontractcodeParam,"loanbrowcodeParam" : loanbrowcodeParam,"browidcodeParam" : browidcodeParam,"isfarmerloanParam" : isfarmerloanParam,"sjrqParam" : operationtimeParam});
    }

    // 导出
    function showOut(){
        var row = $("#dg1").datagrid("getSelections");
        var id = '';
        var info = '';
        for( var dataIndex in row){
            id = id + row[dataIndex].id + ",";
        }
        if(id != ''){
            info = '是否将选中的数据导出到Excel表中？';
        }else {
            info = '是否将全部数据导出到Excel表中？'
        }
        var finorgcodeParam = $("#finorgcodeParam").val().trim();
        var loancontractcodeParam = $("#loancontractcodeParam").val().trim();

        var loanbrowcodeParam = $("#loanbrowcodeParam").val().trim();
        var browidcodeParam = $("#browidcodeParam").val().trim();
        var isfarmerloanParam = $("#isfarmerloanParam").combobox("getValue");

        var operationtimeParam = $("#operationtimeParam").datebox("getValue");
        $.messager.confirm('操作提示', info, function (r) {
            if (r) {
                window.location.href = "XEExportXgrdkfsxxH?finorgcodeParam="+finorgcodeParam+"&loancontractcodeParam="+loancontractcodeParam+"&loanbrowcodeParam="+loanbrowcodeParam+"&browidcodeParam="+browidcodeParam+"&isfarmerloanParam="+isfarmerloanParam+"&sjrqParam="+operationtimeParam +"&id="+id;
            }
        });
    }

    //数据打回
    function dataBack(){
        var v = $('#dateform').form('validate');
        if(v){
            var value = $('#dateselect').datebox('getValue');
            $('#datedialog').window('close');
            $.messager.confirm('提示','是否打回数据？',function (r){
                if(r){
                    $.messager.progress({
                        title: '请稍等',
                        msg: '数据处理中......'
                    });
                    $.ajax({
                        url: "XdatabackxgrdkfsxxH",
                        type: "POST",
                        contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                        async: true,
                        data:{"date":value},
                        dataType:'json',
                        success: function (data) {
                            if(data != 0){
                                $.messager.progress('close');
                                $.messager.alert('操作提示', '数据打回成功！', 'info');
                                $("#dg1").datagrid("reload");
                            }else{
                                $.messager.progress('close');
                                $.messager.alert('操作提示', '没有数据可打回！', 'error');
                                $("#dg1").datagrid("reload");
                            }

                        },
                        error: function (error) {
                            $.messager.progress('close');
                            $.messager.alert('操作提示', '数据打回失败！', 'error');
                        }
                    })
                }
            });
        }

    }
</script>
</body>
</html>
