<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String datamanege = (String) request.getAttribute("datamanege");
	List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
	List<String> baseCurrencys = new ArrayList<>();
	for (BaseCurrency baseCurrency : baseCurrencyList) {
		baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
	}
	List<BaseAindustry> baseAindustryList = (List<BaseAindustry>) request.getAttribute("baseAindustryList");
	List<String> baseAindustrys = new ArrayList<>();
	for (BaseAindustry baseAindustry : baseAindustryList) {
		baseAindustrys.add("'"+baseAindustry.getAindustrycode()+ "-" +baseAindustry.getAindustryname()+"'");
	}
	List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
	List<String> baseAreas = new ArrayList<>();
	for (BaseArea baseArea : baseAreaList) {
		baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>单位贷款担保合同信息</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>

	<table id="dg1" style="height: 480px;" title="单位贷款担保合同信息列表"></table>
	<!-- 日期选择框 -->
	<div id="datedialog" class="easyui-dialog" title="日期选择" style="width:400px;height:200px;padding-left:30px;padding-top:50px" data-options="iconCls:'icon-save',resizable:true,modal:true">
    	<form id="dateform" action="">
    	请选择报文日期：<input id="dateselect" type="text" class="easyui-datebox" required="required" editable=false><br/><br/>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-ok" onclick="genMessage()">确定</a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#datedialog').window('close')">取消</a>
    	</form>
	</div>


	<!--表格工具栏-->
	<div id="tb1" style="padding: 5px; height: auto;">
		<%if (!"datamanege".equals(datamanege)){
		%>
		<a class="easyui-linkbutton" href="XCreateReportManager" iconCls="icon-return" onclick="fanhui()">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<%}else {%>
		<a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return" onclick="fanhui()">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<%}%>
		担保合同编码：<input id="gteecontractcodeParam" type="text" style="height: 23px; width:160px"/>&nbsp;&nbsp;&nbsp;&nbsp;
		被担保合同编码：<input id="loancontractcodeParam" type="text" style="height: 23px; width:160px"/>&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;<br>
		<%if (!"datamanege".equals(datamanege)){
		%>
		<a class="easyui-linkbutton" iconCls="icon-generatemessage" onclick="javascript:$('#datedialog').window('open')">生成报文</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-cross" onclick="goback()">数据打回</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<%}%>
	</div>
	<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">

		<form id="info_formForAdd" method="post">
			<input id="info_id" class="backId" name="id" type="hidden">
			<div style="padding-top: 20px">
				<table style="padding-left: 30px">
					<tr>
						<td>担保合同编码:</td>
						<td><input type="text" id="info_gteecontractcode" name="gteecontractcode" disabled="disabled" style="height: 23px; width:173px" /></td>
						<td>被担保合同编码:</td>
						<td><input type="text" id="info_loancontractcode" name="loancontractcode" disabled="disabled" style="height: 23px; width:173px"/></td>
					</tr>

					<tr>
						<td>担保合同类型:</td>
						<td>
							<select id="info_gteecontracttype" name="gteecontracttype" disabled="disabled" style="height: 23px; width:173px" >
								<option value="01">01-一般担保合同</option>
								<option value="02">02-最高额担保合同</option>
							</select>
						</td>
						<td>担保合同起始日期:</td>
						<td><input disabled="disabled" id="info_gteestartdate" name="gteestartdate" style="height: 23px; width:173px"/></td>
					</tr>

					<tr>
						<td>担保合同到期日期:</td>
						<td><input disabled="disabled" id="info_gteeenddate" name="gteeenddate" style="height: 23px; width:173px"/></td>
						<td>币种:</td>
						<td><input disabled="disabled" type="text" id="info_gteecurrency" name="gteecurrency" style="height: 23px; width:173px"/>
							<%--<select required="true" id="info_gteecurrency" name="gteecurrency" disabled="disabled" style="height: 23px; width:173px" >
								<c:forEach items="${baseCurrencyList}" var="aa">
									<option value="${aa.currencycode}">${aa.currencycode}-${aa.currencyname}</option>
								</c:forEach>
							</select>--%>
						</td>
					</tr>

					<tr>
						<td>担保合同金额:</td>
						<td><input type="text" id="info_gteeamount" name="gteeamount" disabled="disabled" style="height: 23px; width:173px"/></td>
						<td>担保合同金额折人民币:</td>
						<td><input type="text" id="info_gteecnyamount" name="gteecnyamount" disabled="disabled" style="height: 23px; width:173px"/></td>
					</tr>

					<tr>
						<td>担保人证件类型:</td>
						<td><input disabled="disabled" type="text" id="info_gteeidtype" name="gteeidtype" style="height: 23px; width:173px"/>
							<%--<select id="info_gteeidtype" name="gteeidtype" disabled="disabled" style="height: 23px; width:173px" >
								<option value="A01">A01-统一社会信用代码</option>
								<option value="A02">A02-组织机构代码</option>
								<option value="A03">A03-其他</option>
								<option value="B01">B01-身份证</option>
								<option value="B02">B02-户口簿</option>
								<option value="B03">B03-护照</option>
								<option value="B04">B04-军官证</option>
								<option value="B05">B05-士兵证</option>
								<option value="B06">B06-港澳居民来往内地通行证</option>
								<option value="B07">B07-台湾同胞来往内地通行证</option>
								<option value="B08">B08-临时身份证</option>
								<option value="B09">B09-外国人居留证</option>
								<option value="B10">B10-警官证</option>
								<option value="B11">B11-个体工商户营业执照</option>
								<option value="B12">B12-外国人永久拘留省份证</option>
								<option value="B13">B13-港澳台居民居住证</option>
								<option value="B99">B99-其他证件</option>
							</select>--%>
						</td>
						<td>担保人证件代码:</td>
						<td><input disabled="disabled" type="text" id="info_gteeidnum" name="gteeidnum" style="height: 23px; width:173px"/></td>
					</tr>

					<tr>
						<td>金融机构代码:</td>
						<td><input disabled="disabled" type="text" id="info_financeorgcode" name="financeorgcode"  style="height: 23px; width:173px"/></td>
						<td>内部机构号:</td>
						<td><input disabled="disabled" type="text" id="info_financeorginnum" name="financeorginnum" style="height: 23px; width:173px"/></td>
					</tr>

					<tr>
						<td>交易类型:</td>
						<td><input disabled="disabled" type="text" id="info_transactiontype" name="transactiontype" style="height: 23px; width:173px"/>
							<%--<select disabled="disabled" id="info_transactiontype" name="transactiontype" style="height: 23px; width:173px" >
                            <option value="01">01-回购类交易</option>
                            <option value="02">02-信贷交易</option>
                            <option value="03">03-其他交易</option>
                        </select>--%></td>
						<td>抵质押率:</td>
						<td><input disabled="disabled" type="text" id="info_pledgerate" name="pledgerate" style="height: 23px; width:173px"/></td>
					</tr>

					<tr>
						<td>担保人国民经济部门:</td>
						<td><select disabled="disabled" id="info_isgreenloan" name="isgreenloan" style="height: 23px; width:173px" >
							<option value='A'>A-广义政府</option>
							<option value='A01'>A01-中央政府</option>
							<option value='A02'>A02-地方政府</option>
							<option value='A03'>A03-社会保障基金</option>
							<option value='A04'>A04-机关团体</option>
							<option value='A05'>A05-部队</option>
							<option value='A06'>A06-住房公积金</option>
							<option value='A99'>A99-其他</option>
							<option value='B'>B-金融机构部门</option>
							<option value='B01'>B01-货币当局</option>
							<option value='B02'>B02-监管当局</option>
							<option value='B03'>B03-银行业存款类金融机构</option>
							<option value='B04'>B04-银行业非存款类金融机构</option>
							<option value='B05'>B05-证券业金融机构</option>
							<option value='B06'>B06-保险业金融机构</option>
							<option value='B07'>B07-交易及结算类金融机构</option>
							<option value='B08'>B08-金融控股公司</option>
							<option value='B09'>B09-特定目的载体</option>
							<option value='B99'>B99-其他</option>
							<option value='C'>C-非金融企业部门</option>
							<option value='C01'>C01-公司</option>
							<option value='C02'>C02-非公司企业</option>
							<option value='C99'>C99-其他非金融企业部门</option>
							<option value='D'>D-住户部门</option>
							<option value='D01'>D01-住户</option>
							<option value='D02'>D02-为住户服务的非营力机构</option>
							<option value='E'>E-非居民部门</option>
							<option value='E01'>E01-国际组织</option>
							<option value='E02'>E02-外国政府</option>
							<option value='E03'>E03-境外金融机构</option>
							<option value='E04'>E04-境外非金融企业</option>
							<option value='E05'>E05-外国居民</option>
						</select></td>
						<td>担保人行业:</td>
						<td><select disabled="disabled" id="info_brrowerindustry" name="brrowerindustry" style="height: 23px; width:173px" >
							<c:forEach items='${baseAindustryList}' var='aa'>
								<option value='${aa.aindustrycode}'>${aa.aindustrycode}-${aa.aindustryname}</option>
							</c:forEach>
						</select></td>
					</tr>

					<tr>
						<td>担保人地区代码:</td>
						<td><input disabled="disabled" id="info_brrowerareacode" name="brrowerareacode" style="height: 23px; width:173px" >
							<!--<c:forEach items='${baseAreaList}' var='aa'>
								<option value='${aa.areacode}'>${aa.areacode}-${aa.areaname}</option>
							</c:forEach>-->
						</input></td>
						<td>担保人企业规模:</td>
						<td><select disabled="disabled" id="info_enterprisescale" name="enterprisescale" style="height: 23px; width:173px" >
							<option value='CS01'>CS01-大型</option>
							<option value='CS02'>CS02-中型</option>
							<option value='CS03'>CS03-小型</option>
							<option value='CS04'>CS04-微型</option>
							<option value='CS05'>CS05-其他（非企业类单位）</option>
						</select></td>
					</tr>
				</table>
			</div>
		</form>
	</div>
<script type="text/javascript">
$(function(){
	var baseCurrencys = <%=baseCurrencys%>;
	var baseAindustrys = <%=baseAindustrys%>;
	var baseAreas = <%=baseAreas%>;
	$('#datedialog').window('close');
	$("#dg1").datagrid({
		loadMsg:'数据加载中,请稍后...',
		method:'post',
		url:'XGetXdkdbhtData?datastatus='+'${datastatus}',
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		fitColumns:false,
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			{field:'ck',checkbox:true},
        	{field:'id', title: '编号', width: 80, align: 'center', hidden: true},
			{field: 'gteecontractcode', title: '担保合同编码', width: 100, align: 'center'},
			{field: 'loancontractcode', title: '被担保合同编码', width: 100, align: 'center'},
			{field: 'gteecontracttype', title: '担保合同类型', width: 100, align: 'center',formatter:function (value) {
					if (value=='01'){
						return "01-一般担保合同";
					}else if(value=='02'){
						return "02-最高额担保合同";
					}
					return value;
				}},
			{field: 'gteestartdate', title: '担保合同起始日期', width: 100, align: 'center'},
			{field: 'gteeenddate', title: '担保合同到期日期', width: 100, align: 'center'},
			{field: 'gteecurrency', title: '币种', width: 100, align: 'center',formatter:function (value) {
					for (var i = 0;i <  baseCurrencys.length;i++){
						if (baseCurrencys[i].substring(0,3)==value){
							return baseCurrencys[i];
						}
					}
				}},
			{field: 'gteeamount', title: '担保合同金额', width: 100, align: 'center'},
			{field: 'gteecnyamount', title: '担保合同金额折人民币', width: 100, align: 'center'},
			{field: 'gteeidtype', title: '担保人证件类型', width: 100, align: 'center',formatter:function (value) {
					if (value=='A01'){
						return "A01-统一社会信用代码";
					}else if(value=='A02'){
						return "A02-组织机构代码";
					}else if (value=='A03'){
						return "A03-其他";
					}else if(value=='B01'){
						return "B01-身份证";
					}else if (value=='B02'){
						return "B02-户口簿";
					}else if (value=='B03'){
						return "B03-护照";
					}else if(value=='B04'){
						return "B04-军官证";
					}else if (value=='B05'){
						return "B05-士兵证";
					}else if (value=='B06'){
						return "B06-港澳居民来往内地通行证";
					}else if(value=='B07'){
						return "B07-台湾同胞来往内地通行证";
					}else if (value=='B08'){
						return "B08-临时身份证";
					}else if(value=='B09'){
						return "B09-外国人居留证";
					}else if (value=='B10'){
						return "B10-警官证";
					}else if(value=='B11'){
						return "B11-外国人永久拘留省份证";
					}else if (value=='B12'){
						return "B12-港澳台居民居住证";
					}else if (value=='B99'){
						return "B99-其他证件";
					}
					return value;
				}},
			{field: 'gteeidnum', title: '担保人证件代码', width: 100, align: 'center'},
			{field: 'financeorgcode', title: '金融机构代码', width: 100, align: 'center'},
			{field: 'financeorginnum', title: '内部机构号', width: 100, align: 'center'},
			{field: 'transactiontype', title: '交易类型', width: 100, align: 'center',formatter:function (value) {
					if (value=='01'){
						return "01-回购类交易";
					}else if(value=='02'){
						return "02-信贷交易";
					}else if (value=='03'){
						return "03-其他交易";
					}
					return value;
				}},
			{field: 'pledgerate', title: '抵质押率', width: 100, align: 'center'},
			{field: 'isgreenloan', title: '担保人国民经济部门', width: 100, align: 'center',formatter:function (value) {
					if (value=='A'){
						return "A-广义政府";
					}else if (value=='A01'){
						return "A01-中央政府";
					}else if(value=='A02'){
						return "A02-地方政府";
					}else if (value=='A03'){
						return "A03-社会保障基金";
					}else if (value=='A04'){
						return "A04-机关团体";
					}else if(value=='A05'){
						return "A05-部队";
					}else if (value=='A06'){
						return "A06-住房公积金";
					}else if (value=='A99'){
						return "A99-其他";
					}else if(value=='B'){
						return "B-金融机构部门";
					}else if(value=='B01'){
						return "B01-货币当局";
					}else if (value=='B02'){
						return "B02-监管当局";
					}else if (value=='B03'){
						return "B03-银行业存款类金融机构";
					}else if(value=='B04'){
						return "B04-银行业非存款类金融机构";
					}else if (value=='B05'){
						return "B05-证券业金融机构";
					}else if (value=='B06'){
						return "B06-保险业金融机构";
					}else if(value=='B07'){
						return "B07-交易及结算类金融机构";
					}else if (value=='B08'){
						return "B08-金融控股公司";
					}else if(value=='B09'){
						return "B09-特定目的载体";
					}else if (value=='B99'){
						return "B99-其他";
					}else if(value=='C'){
						return "C-非金融企业部门";
					}else if(value=='C01'){
						return "C01-公司";
					}else if (value=='C02'){
						return "C02-非公司企业";
					}else if (value=='C99'){
						return "C99-其他非金融企业部门";
					}else if(value=='D'){
						return "D-住户部门";
					}else if(value=='D01'){
						return "D01-住户";
					}else if (value=='D02'){
						return "D02-为住户服务的非营力机构";
					}else if(value=='E'){
						return "E-非居民部门";
					}else if(value=='E01'){
						return "E01-国际组织";
					}else if (value=='E02'){
						return "E02-外国政府";
					}else if (value=='E03'){
						return "E03-境外金融机构";
					}else if(value=='E04'){
						return "E04-境外非金融企业";
					}else if (value=='E05'){
						return "E05-外国居民";
					}
					return value;
				}},
			{field: 'brrowerindustry', title: '担保人行业', width: 100, align: 'center',formatter:function (value) {
					for (var i = 0;i <  baseAindustrys.length;i++){
						if (baseAindustrys[i].substring(0,3)==value){
							return baseAindustrys[i];
						}
					}
				}},
			{field: 'brrowerareacode', title: '担保人地区代码', width: 100, align: 'center',formatter:function (value) {
					for (var i = 0;i <  baseAreas.length;i++){
						if (baseAreas[i].substring(0,6)==value){
							return baseAreas[i];
						}
					}
				}},
			{field: 'enterprisescale', title: '担保人企业规模', width: 100, align: 'center',formatter:function (value) {
					if (value=='CS01'){
						return "CS01-大型";
					}else if(value=='CS02'){
						return "CS02-中型";
					}else if (value=='CS03'){
						return "CS03-小型";
					}else if(value=='CS04'){
						return "CS04-微型";
					}else if (value=='CS05'){
						return "CS05-其他（非企业类单位）";
					}
					return value;
				}},
			{field: 'operator', title: '操作人',  align: 'center'},
			{field: 'operationtime', title: '操作时间',  align: 'center'}
    ]],
		onDblClickRow: function (rowIndex,rowData) {
			$('#info_formForAdd').form('clear');
			$('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','单位贷款担保合同信息详情');
			$('#info_formForAdd').form('load',rowData);
		}
	});
})

// 查询
function searchOnSelected(){
	var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
	var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
	$("#dg1").datagrid("load", {"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam});
}

//生成报文
function genMessage(){
	var v = $('#dateform').form('validate');
	if(v){
		var value = $('#dateselect').datebox('getValue');
		$('#datedialog').window('close');
	    $.messager.confirm('提示','是否生成报文？',function (r){
	        if(r){
				$.messager.progress({
					title: '请稍等',
					msg: '数据处理中......'
				});
				$.ajax({
					url: "XCreateReportThree",
					type: "POST",
					contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
					async: true,
					data:{"date":value},
			        dataType:'json',
					success: function (data) {
						$.messager.progress('close');
						$.messager.alert('操作提示', '生成报文成功'+data+'条', 'info');
						$("#dg1").datagrid("reload");
					},
					error: function (error) {
						$.messager.progress('close');
						$.messager.alert('操作提示', '生成报文失败', 'error');
					}
				})
	        }
	    });
	}
	
}


// 打回按钮
function goback() {
	var rows = $('#dg1').datagrid('getSelections');
	var info = '';
	var id = '';
	for (var i = 0;i < rows.length;i++){
		id = id + rows[i].id + ',';
	}
	if(rows.length > 0){
		info = '是否打回选中的数据？';
	}else {
		info = '是否打回所有的数据？';
	}
	var gteecontractcodeParam = $("#gteecontractcodeParam").val().trim();
	var loancontractcodeParam = $("#loancontractcodeParam").val().trim();
	$.messager.confirm('提示',info,function (r){
		if(r){
			$.ajax({
				sync: true,
				type: "POST",
				dataType: "json",
				url: "XgobackXdkdbht",
				data:{"id" : id,"gteecontractcodeParam" : gteecontractcodeParam,"loancontractcodeParam" : loancontractcodeParam},
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				success: function (data) {
					if (data == 0){
						$.messager.alert('操作提示','打回成功','info');
						$("#dg1").datagrid("reload");
					}else {
						$.messager.alert('操作提示','打回失败','info');
					}
				},
				error: function (err) {
					$.messager.alert('操作提示','打回失败','error');
				}
			});
		}
	});
}

//返回
    function fanhui(){
    	$.messager.progress({
            title: '请稍等',
            msg: '数据正在加载中......'
        });    	
    }
</script>
</body>
</html>
