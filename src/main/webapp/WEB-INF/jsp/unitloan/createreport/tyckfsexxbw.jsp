<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseBindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
    String datamanege = (String) request.getAttribute("datamanege");
    List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
    List<String> baseAreas = new ArrayList<>();
    for (BaseArea baseArea : baseAreaList) {
        baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
    }
    List<BaseCountry> baseCountryList = (List<BaseCountry>) request.getAttribute("baseCountryList");
    List<String>  baseCountrys = new ArrayList<>();
    for (BaseCountry baseCountry : baseCountryList) {
    	baseCountrys.add("'"+baseCountry.getCountrycode()+ "-" +baseCountry.getCountryname()+"'");
    }
    List<String>  baseAreaAndCountrys = new ArrayList<>();
    baseAreaAndCountrys.addAll(baseAreas);
    baseAreaAndCountrys.addAll(baseCountrys);
    List<BaseAindustry> baseAindustryList = (List<BaseAindustry>) request.getAttribute("baseAindustryList");
    List<String> baseAindustrys = new ArrayList<>();
    for (BaseAindustry baseAindustry : baseAindustryList) {
        baseAindustrys.add("'"+baseAindustry.getAindustrycode()+ "-" +baseAindustry.getAindustryname()+"'");
    }
    List<BaseBindustry> baseBindustryList = (List<BaseBindustry>) request.getAttribute("baseBindustryList");
    List<String> baseBindustrys = new ArrayList<>();
    for (BaseBindustry baseBindustry : baseBindustryList) {
        baseBindustrys.add("'"+baseBindustry.getBindustrycode()+ "-" +baseBindustry.getBindustryname()+"'");
    }
    List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
    List<String> baseCurrencys = new ArrayList<>();
    for (BaseCurrency baseCurrency : baseCurrencyList) {
        baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>同业存款发生额信息</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
	<table id="dg1" style="height: 480px;" title="同业存款发生额信息"></table>
	<!-- 日期选择框 -->
	<div id="datedialog" class="easyui-dialog" title="日期选择" style="width:400px;height:200px;padding-left:30px;padding-top:50px"
    data-options="iconCls:'icon-save',resizable:true,modal:true">
    	<form id="dateform" action="">
    	请选择报文日期：<input id="dateselect" type="text" class="easyui-datebox" required="required"><br/><br/>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-ok" onclick="genMessage()">确定</a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#datedialog').window('close')">取消</a>
    	</form>
	</div>
	<!--表格工具栏-->
	<div id="tb1" style="padding: 5px; height: auto;">
        <%if (!"datamanege".equals(datamanege)){
        %>
        <a class="easyui-linkbutton" href="XCreateReportManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <%}else {%>
        <a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <%}%>
        <label>金融机构代码：</label><input id="financeorgcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>合同编码：</label><input id="contractcodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>交易对手证件类型：</label>
    <select class='easyui-combobox'  id="jydszjlxParam" name="jydszjlxParam" editable=false style="height: 23px; width:130px">
        <option value=''>=请选择=</option>
        <option value='A01'>A01-统一社会信用代码</option>
        <option value='A02'>A02-组织机构代码</option>
        <option value='C01'>C01-资管产品统计编码</option>
        <option value='C02'>C02-资管产品登记备案编码</option>
        <option value='L01'>L01-全球法人识别编码（LEI码）</option>
        <option value='Z99'>Z99-自定义码</option>
    </select>&nbsp;
    <label>交易对手代码：</label><input id="jydsdmParam" type="text" style="height: 23px; width:130px"/>&nbsp;
    <label>校验类型：</label>
    <select class='easyui-combobox'  id='checkstatusParam' name='checkstatusParam' editable=false style='height: 23px; width:80px' >
        <option value=''>=请选择=</option>
        <option value='0'>未校验</option>
        <option value='1'>校验成功</option>
        <option value='2'>校验失败</option>
    </select>
        &nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="selectRecordByDate()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;<br>
        <%if (!"datamanege".equals(datamanege)){
        %>
        <a class="easyui-linkbutton" iconCls="icon-generatemessage" onclick="javascript:$('#datedialog').window('open')">生成报文</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="easyui-linkbutton" iconCls="icon-cross" onclick="goback()">数据打回</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <%}%>
	</div>

    <!--详情窗口-->
    <div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
        <div align="center" style="padding-top: 30px">
            <form id="info_formForAdd" method="post">
                <input id="info_id" class="backId" name="id" type="hidden">
                <table class='enterTable' id='info_enterTable'>
                <tr> <td align='right'>
                    金融机构代码:</td>
                    <td>
                        <input id='info_financeorgcode' name='financeorgcode' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        内部机构号:
                    </td>
                    <td>
                        <input id='info_financeorginnum' name='financeorginnum' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    业务类型:</td>
                    <td>
                        <select class='easyui-combobox' id='info_ywlx' name='ywlx' disabled='disabled' style='width:173px;'>
                        	<option value='T01'>T01-同业存放</option>
							<option value='T011'>T011-活期存放</option>
							<option value='T012'>T012-定期存放</option>
							<option value='T02'>T02-存放同业</option>
							<option value='T021'>T021-活期存放</option>
							<option value='T022'>T022-定期存放</option>
							<option value='T03'>T03-同业存单发行</option>
							<option value='T04'>T04-同业存单投资</option>
                    	</select>
                    </td>
     <td align='right'>
                        交易对手证件类型:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_jydszjlx' name='jydszjlx' disabled='disabled' style='width:173px;'>
                        	<option value='A01'>A01-统一社会信用代码</option>
                        	<option value='A02'>A02-组织机构代码</option>
                        	<option value='C01'>C01-资管产品统计编码</option>
                        	<option value='C02'>C02-资管产品登记备案编码</option>
                        	<option value='L01'>L01-全球法人识别编码（LEI码）</option>
                        	<option value='Z99'>Z99-自定义码</option>
                        </select>
                    </td>
                </tr>
<tr> <td align='right'>
                    交易对手代码:</td>
                    <td>
                        <input id='info_jydsdm' name='jydsdm' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        存款账户编码:
                    </td>
                    <td>
                        <input id='info_ckzhbm' name='ckzhbm' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    存款协议代码:</td>
                    <td>
                        <input id='info_ckxydm' name='ckxydm' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        协议起始日期:
                    </td>
                    <td>
                        <input class='easyui-datebox' id='info_startdate' name='startdate' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    协议到期日期:</td>
                    <td>
                        <input class='easyui-datebox' id='info_enddate' name='enddate' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        币种:
                    </td>
                    <td>
                        <select class='easyui-combobox' id='info_currency' name='currency' disabled='disabled' style='width:173px;'>
                    		<c:forEach items='${baseCurrencyList}' var='aa'>
                                <option value='${aa.currencycode}'>${aa.currencycode}-${aa.currencyname}</option>
                            </c:forEach>
                            </select>
                    </td>
                </tr>
<tr> <td align='right'>
                    交易金额:</td>
                    <td>
                        <input id='info_receiptbalance' name='receiptbalance' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        交易金额折人民币:
                    </td>
                    <td>
                        <input id='info_receiptcnybalance' name='receiptcnybalance' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    交易日期:</td>
                    <td>
                        <input class='easyui-datebox' id='info_jyrq' name='jyrq' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        交易流水号:
                    </td>
                    <td>
                        <input id='info_jylsh' name='jylsh' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    利率水平:</td>
                    <td>
                        <input id='info_llsp' name='llsp' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        交易账户号:
                    </td>
                    <td>
                        <input id='info_jyzhh' name='jyzhh' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    交易账户开户行号:</td>
                    <td>
                        <input id='info_jyzhkhhh' name='jyzhkhhh' disabled='disabled' style='width:173px;'/>
                    </td>
     <td align='right'>
                        交易对手账户号:
                    </td>
                    <td>
                        <input id='info_jydszhh' name='jydszhh' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
<tr> <td align='right'>
                    交易方向:</td>
                    <td>
                        <select class='easyui-combobox' id='info_jyfx' name='jyfx' disabled='disabled' style='width:173px;'>
                        	<option value='1'>1-发生</option>
                        	<option value='0'>0-结清</option>
                        </select>
                    </td>
     <td align='right'>
                        数据日期:
                    </td>
                    <td>
                        <input class='easyui-datebox' id='info_sjrq' name='sjrq' disabled='disabled' style='width:173px;'/>
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>
<div style="visibility: hidden;">
</div>
<script type="text/javascript">
$(function(){
	var baseAreaAndCountrys = <%=baseAreaAndCountrys%>;
    var baseAreas = <%=baseAreas%>;
    var baseAindustrys = <%=baseAindustrys%>;
    var baseBindustrys = <%=baseBindustrys%>;
    var baseCurrencys = <%=baseCurrencys%>;
	$("#importFileDialog").dialog("close");
	$('#datedialog').window('close');
	$("#dg1").datagrid({
		method:'post',
		url:'XGetXtyckfsexxData?datastatus='+'${datastatus}',
		loadMsg:'数据加载中,请稍后...',
		singleSelect:false,
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
            {field:'ck',checkbox:true},
            {field:'id', title: '编号', width: 80, align: 'center', hidden: true},
            {field:'financeorgcode',title:'金融机构代码',width:150,align:'center'},
            {field:'financeorginnum',title:'内部机构号',width:150,align:'center'},
            {field:'ywlx',title:'业务类型',width:150,align:'center',formatter:function (value) {
            	if (value=='T01'){
            		  return "T01-同业存放";
            		}else if (value=='T011'){
            		  return "T011-活期存放";
            		}else if (value=='T012'){
            		  return "T012-定期存放";
            		}else if (value=='T02'){
            		  return "T02-存放同业";
            		}else if (value=='T021'){
            		  return "T021-活期存放";
            		}else if (value=='T022'){
            		  return "T022-定期存放";
            		}else if (value=='T03'){
            		  return "T03-同业存单发行";
            		}else if (value=='T04'){
            		  return "T04-同业存单投资";
                }
                return value;
            }},
            {field:'jydszjlx',title:'交易对手证件类型',width:150,align:'center',formatter:function (value) {
            	if (value=='A01'){
            		  return "A01-统一社会信用代码";
            		}else if (value=='A02'){
            		  return "A02-组织机构代码";
            		}else if (value=='C01'){
            		  return "C01-资管产品统计编码";
            		}else if (value=='C02'){
            		  return "C02-资管产品登记备案编码";
            		}else if (value=='L01'){
            		  return "L01-全球法人识别编码（LEI码）";
            		}else if (value=='Z99'){
            		  return "Z99-自定义码";
                }
                return value;
            }},
            {field:'jydsdm',title:'交易对手代码',width:150,align:'center'},
            {field:'ckzhbm',title:'存款账户编码',width:150,align:'center'},
            {field:'ckxydm',title:'存款协议代码',width:150,align:'center'},
            {field:'startdate',title:'协议起始日期',width:150,align:'center'},
            {field:'enddate',title:'协议到期日期',width:150,align:'center'},
            {field:'currency',title:'币种',width:150,align:'center',formatter:function (value) {
            	for (var i = 0;i <  baseCurrencys.length;i++){
                    if (baseCurrencys[i].substring(0,3)==value){
                        return baseCurrencys[i];
                    }
                }
            }},
            {field:'receiptbalance',title:'交易金额',width:150,align:'center'},
            {field:'receiptcnybalance',title:'交易金额折人民币',width:150,align:'center'},
            {field:'jyrq',title:'交易日期',width:150,align:'center'},
            {field:'jylsh',title:'交易流水号',width:150,align:'center'},
            {field:'llsp',title:'利率水平',width:150,align:'center'},
            {field:'jyzhh',title:'交易账户号',width:150,align:'center'},
            {field:'jyzhkhhh',title:'交易账户开户行号',width:150,align:'center'},
            {field:'jydszhh',title:'交易对手账户号',width:150,align:'center'},
            {field:'jyfx',title:'交易方向',width:150,align:'center',formatter:function(value){
            	if (value=='1'){
          		  return "1-发生";
          		}else if (value=='0'){
          		  return "0-结清";
          		}
              return value;
            }},
            {field:'sjrq',title:'数据日期',width:150,align:'center'},
            {field: 'operator', title: '操作人',  align: 'center'},
            {field: 'operationtime', title: '操作时间',  align: 'center'}
	    ]],
        onDblClickRow: function (rowIndex,rowData) {
            $('#info_formForAdd').form('clear');
            $('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','同业存款发生额信息详情');
            $('#info_formForAdd').form('load',rowData);
        }
	});
})
//动态查询
function selectRecordByDate(){
    var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
    var contractcodeParam = $("#contractcodeParam").val().trim();
    var jydsdmParam = $("#jydsdmParam").val().trim();
    var jydszjlxParam = $("#jydszjlxParam").combobox("getValue");
    $("#dg1").datagrid("load", {"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"jydsdmParam":jydsdmParam,"jydszjlxParam":jydszjlxParam});
}
function genMessage(){
	var v = $('#dateform').form('validate');
	if(v){
		var value = $('#dateselect').datebox('getValue');
		console.info(value)
		$('#datedialog').window('close');
	    $.messager.confirm('提示','是否生成报文？',function (r){
	        if(r){
	            $.messager.progress({
	                title: '请稍等',
	                msg: '数据处理中......'
	            });
	            $.ajax({
	                url: "XCreateReportXtyckfsexx",
	                data:{"date":value},
	                type: "POST",
	                contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
	                async: true,
	                success: function (data) {
	                    $.messager.progress('close');
	                    $.messager.alert('操作提示', '生成报文成功'+data+'条', 'info');
	                    $("#dg1").datagrid("reload");
	                },
	                error: function (error) {
	                    $.messager.progress('close');
	                    $.messager.alert('操作提示', '生成报文失败', 'error');
	                }
	            })
	        }
	    });
	}
}// 打回按钮
function goback() {
    var rows = $('#dg1').datagrid('getSelections');
    var info = '';
    var id = '';
    for (var i = 0;i < rows.length;i++){
        id = id + rows[i].id + ',';
    }
    if(rows.length > 0){
        info = '是否打回选中的数据？';
    }else {
        info = '是否打回所有的数据？';
    }
    var financeorgcodeParam = $("#financeorgcodeParam").val().trim();
    var contractcodeParam = $("#contractcodeParam").val().trim();
    var jydsdmParam = $("#jydsdmParam").val().trim();
    var jydszjlxParam = $("#jydszjlxParam").combobox("getValue");
    $.messager.confirm('提示',info,function (r){
        if(r){
            $.ajax({
                sync: true,
                type: "POST",
                dataType: "json",
                url: "XgobackXtyckfsexx",
                data:{"id" : id,"financeorgcodeParam" : financeorgcodeParam,"contractcodeParam" : contractcodeParam,"jydsdmParam":jydsdmParam,"jydszjlxParam":jydszjlxParam},
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (data) {
                    if (data == 0){
                        $.messager.alert('操作提示','打回成功','info');
                        $("#dg1").datagrid("reload");
                    }else {
                        $.messager.alert('操作提示','打回失败','info');
                    }
                },
                error: function (err) {
                    $.messager.alert('操作提示','打回失败','error');
                }
            });
        }
    });
}
</script>
</body>
</html>
