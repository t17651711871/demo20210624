<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseArea" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseAindustry" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCurrency" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseCountryTwo" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseEducation" %>
<%@ page import="com.geping.etl.UNITLOAN.entity.baseInfo.BaseNation" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String datamanege = (String) request.getAttribute("datamanege");
	List<BaseArea> baseAreaList = (List<BaseArea>) request.getAttribute("baseAreaList");
	List<String> baseAreas = new ArrayList<>();
	for (BaseArea baseArea : baseAreaList) {
		baseAreas.add("'"+baseArea.getAreacode()+ "-" +baseArea.getAreaname()+"'");
	}
	List<BaseAindustry> baseAindustryList = (List<BaseAindustry>) request.getAttribute("baseAindustryList");
	List<String> baseAindustrys = new ArrayList<>();
	for (BaseAindustry baseAindustry : baseAindustryList) {
		baseAindustrys.add("'"+baseAindustry.getAindustrycode()+ "-" +baseAindustry.getAindustryname()+"'");
	}
	List<BaseCurrency> baseCurrencyList = (List<BaseCurrency>) request.getAttribute("baseCurrencyList");
	List<String> baseCurrencys = new ArrayList<>();
	for (BaseCurrency baseCurrency : baseCurrencyList) {
		baseCurrencys.add("'"+baseCurrency.getCurrencycode()+ "-" +baseCurrency.getCurrencyname()+"'");
	}
	List<BaseCountryTwo> baseCountryTwoList = (List<BaseCountryTwo>) request.getAttribute("baseCountryTwoList");
    List<String> baseCountryTwos = new ArrayList<>();
    for (BaseCountryTwo baseCountryTwo : baseCountryTwoList) {
    	baseCountryTwos.add("'"+baseCountryTwo.getCountrytwocode()+ "-" +baseCountryTwo.getCountrytwoname()+"'");
    }
    List<BaseEducation> baseEducationList = (List<BaseEducation>) request.getAttribute("baseEducationList");
    List<String> baseEducations = new ArrayList<>();
    for (BaseEducation baseEducation : baseEducationList) {
    	baseEducations.add("'"+baseEducation.getEducationcode()+ "-" +baseEducation.getEducationname()+"'");
    }
    List<BaseNation> baseNationList = (List<BaseNation>) request.getAttribute("baseNationList");
    List<String> baseNations = new ArrayList<>();
    for (BaseNation baseNation : baseNationList) {
    	baseNations.add("'"+baseNation.getNationcode()+ "-" +baseNation.getNationname()+"'");
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>个人客户基础信息</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>

	<table id="dg1" style="height: 480px;" title="个人客户基础信息列表"></table>
	<!-- 日期选择框 -->
	<div id="datedialog" class="easyui-dialog" title="日期选择" style="width:400px;height:200px;padding-left:30px;padding-top:50px"
    data-options="iconCls:'icon-save',resizable:true,modal:true">
    	<form id="dateform" action="">
    	请选择报文日期：<input id="dateselect" type="text" class="easyui-datebox" required="required" editable=false><br/><br/>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="easyui-linkbutton" iconCls="icon-ok" onclick="genMessage()">确定</a>&nbsp;&nbsp;&nbsp;&nbsp;
    	<a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#datedialog').window('close')">取消</a>
    	</form>
	</div>
	<!--表格工具栏-->
	<div id="tb1" style="padding: 5px; height: auto;">
		<%if (!"datamanege".equals(datamanege)){
		%>
		<a class="easyui-linkbutton" href="XCreateReportManager" iconCls="icon-return" onclick="fanhui()">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<%}else {%>
		<a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return" onclick="fanhui()">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<%}%>
		金融机构代码：<input type="text" id="finorgcodeParam" style="height: 23px; width:160px"/>&nbsp;&nbsp;&nbsp;&nbsp;
		<label>客户证件类型：</label><select class='easyui-combobox' id='regamtcrenyParam' style='height: 23px; width:173px'>
    						<option value=''>=请选择=</option>
                            <option value='A01'>A01-统一社会信用代码</option>
                            <option value='A02'>A02-组织机构代码</option>
                            <option value='A03'>A03-其他</option>
                            <option value='B01'>B01-身份证</option>
					        <option value='B02'>B02-户口簿</option>
					        <option value='B03'>B03-护照</option>
					        <option value='B04'>B04-军官证</option>
					        <option value='B05'>B05-士兵证</option>
					        <option value='B06'>B06-港澳居民来往内地通行证</option>
					        <option value='B07'>B07-台湾同胞来往内地通行证</option>
					        <option value='B08'>B08-临时身份证</option>
					        <option value='B09'>B09-外国人居留证</option>
					        <option value='B10'>B10-警官证</option>
					        <option value='B11'>B11-外国人永久居留身份证</option>
					        <option value='B12'>B12-港澳台居民居住证</option>
					        <option value='B99'>B99-其他证件</option>
                        </select>
		<label>客户证件代码：</label><input id="customercodeParam" type="text" style="height: 23px; width:130px"/>&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="searchOnSelected()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;<br>
		<%if (!"datamanege".equals(datamanege)){
		%>
		<a class="easyui-linkbutton" iconCls="icon-generatemessage" onclick="javascript:$('#datedialog').window('open')">生成报文</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-cross" onclick="goback()">数据打回</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<%}%>
	</div>

	<!--详情窗口-->
	<div id="info_dialog1" class="easyui-dialog" style="width:800px;height:400px;margin-top:15px;" data-options="resizable:true,modal:true,closed:true">
		<div align="center" style="padding-top: 30px">
			<form id="info_formForAdd" method="post">
            <input id="info_id" class="backId" name="id" type="hidden">
            <table class='enterTable' id='info_enterTable'>
                <tr>
                    <td align='right'>
                        金融机构代码:
                    </td>
                    <td>
                        <input id='info_finorgcode' name='finorgcode' disabled="disabled" style='width:173px;'/>
                    </td>
                    <td align='right'>
                        客户证件类型:
                    </td>
                    <td>
                        <input id='info_regamtcreny' name='regamtcreny' style='width:173px;' maxlength='200' disabled="disabled"/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        客户证件代码:
                    </td>
                    <td>
                        <input id='info_customercode' name='customercode' style='width:173px;' disabled="disabled"/>
                    </td>
                    <td align='right'>
                        国籍:
                    </td>
                    <td>
                        <select id='info_country' name='country' style='width:173px;' disabled="disabled">
                           <c:forEach items='${baseCountryTwoList}' var='aa'>                                                                                                        
                                <option value='${aa.countrytwocode}'>${aa.countrytwocode}-${aa.countrytwoname}</option>                                                                 
                           </c:forEach>                                        
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                       民族:
                    </td>
                    <td>
                        <select id='info_nation' name='nation' style='width:173px;' disabled="disabled">
                        <c:forEach items='${baseNationList}' var='aa'>                                                                                                        
                                <option value='${aa.nationcode}'>${aa.nationcode}-${aa.nationname}</option>                                                                 
                           </c:forEach>
                        </select>
                    </td>
                    <td align='right'>
                        性别:
                    </td>
                    <td>
                        <select id='info_sex' name='sex' style='width:173px;' disabled="disabled">
                        	<option value='01'>01-男性</option>				
        					<option value='02'>02-女性</option>
        					<option value='03'>03-其他性别</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        最高学历:
                    </td>
                    <td>
                        <select id='info_education' name='education' style='height: 23px; width:173px' disabled="disabled">
                            <c:forEach items='${baseEducationList}' var='aa'>                                                                                                        
                                <option value='${aa.educationcode}'>${aa.educationcode}-${aa.educationname}</option>                                                                 
                           </c:forEach>
                        </select>
                    </td>
                    <td align='right'>
                        出生日期:
                    </td>
                    <td>
                        <input id='info_birthday' name='birthday' style='width:173px;' disabled="disabled"/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        地区代码:
                    </td>
                    <td>
                        <input id='info_regareacode' name='regareacode' style='height: 23px; width:173px' disabled="disabled"/>
                    </td>
                    <td align='right'>
                        个人年收入:
                    </td>
                    <td>
                        <input id='info_grincome' name='grincome' style='width:173px;' disabled="disabled"/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        家庭年收入:
                    </td>
                    <td>
                        <input id='info_familyincome' name='familyincome' style='width:173px;' disabled="disabled"/>
                    </td>
                    <td align='right'>
                        婚姻情况:
                    </td>
                    <td>
                        <select id='info_marriage' name='marriage' disabled="disabled" style='height: 23px; width:173px' >
                                <option value='M01'>M01-未婚</option>				
        						<option value='M02'>M02-已婚</option>
        						<option value='M06'>M06-丧偶</option>
        						<option value='M07'>M07-离异</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        是否关联方:
                    </td>
                    <td>
                        <select id='info_isrelation' name='isrelation' disabled="disabled" style='height: 23px; width:173px' >
                            <option value='1'>1-是</option>
                            <option value='0'>0-否</option>
                        </select>
                    </td>
                    <td align='right'>
                        授信额度:
                    </td>
                    <td>
                        <input id='info_creditamt' name='creditamt' style='width:173px;' disabled="disabled"/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        已用额度:
                    </td>
                    <td>
                        <input id='info_usedamt' name='usedamt' style='width:173px;' disabled="disabled"/>
                    </td>
                    <td align='right'>
                        个人客户身份标识:
                    </td>
                    <td>
                        <input id='info_grkhsfbs' name='grkhsfbs' style='height: 23px; width:173px' disabled="disabled">
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                       个体工商户营业执照代码:
                    </td>
                    <td>
                        <input id='info_gtgshyyzzdm' name='gtgshyyzzdm' style='width:173px;' disabled="disabled"/>
                    </td>
                    <td align='right'>
                        小微企业社会统一信用代码:
                    </td>
                    <td>
                        <input id='info_xwqyshtyxydm' name='xwqyshtyxydm' style='width:173px;' disabled="disabled"/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        客户信用级别总等级数:
                    </td>
                    <td>
                        <input id='info_workareacode' name='workareacode' style='width:173px;' disabled="disabled"/>
                    </td>
                    <td align='right'>
                        客户信用评级:
                    </td>
                    <td>
                        <input id='info_actctrltype' name='actctrltype' style='width:173px;' disabled="disabled"/>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        数据日期:
                    </td>
                    <td>
                        <input id='info_sjrq' name='sjrq' style='width:173px;' disabled="disabled"/>
                    </td>
                </tr>
            </table>
        </form>
		</div>
	</div>
	
<script type="text/javascript">
	var baseAreas = <%=baseAreas%>;
	var baseAindustrys = <%=baseAindustrys%>;
	var baseCurrencys = <%=baseCurrencys%>;
    var baseCountryTwos = <%=baseCountryTwos%>;
    var baseEducations = <%=baseEducations%>;
    var baseNations = <%=baseNations%>;
$(function(){
	$('#datedialog').window('close');
	$("#dg1").datagrid({
		loadMsg:'数据加载中,请稍后...',
		method:'post',
		url:'XGetXgrkhxxData?datastatus='+'${datastatus}',
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		fitColumns:false,
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			{field: 'ck', checkbox: true},
            {field: 'id', title: '编号', width: 80, align: 'center', hidden: true},
            {field: 'operationname', title: '操作名', width: 150, align: 'center'},
            {field: 'nopassreason', title: '审核不通过原因', width: 150, align: 'center'},
            {field: 'checkstatus', title: '校验结果', width: 150, align: 'center', formatter: function (value) {
                    if (value == '0') {
                        return "未校验";
                    } else if (value == '1') {
                        return "校验成功";
                    } else if (value == '2') {
                        return '<span style="color:red;">' + '校验失败' + '</span>';
                    }
                    return value;
                }
            },
            {field: 'finorgcode', title: '金融机构代码', width: 100, align: 'center'},
            {field: 'regamtcreny', title: '客户证件类型', width: 100, align: 'center',formatter:function (value) {
                if (value=='A01'){
                    return "A01-统一社会信用代码";
                }else if(value=='A02'){
                    return "A02-组织机构代码";
                }else if (value=='A03'){
                    return "A03-其他";
                }else if (value =='B01'){
                    return "B01-身份证";
                }else if (value =='B02'){
                    return "B02-户口簿";
                }else if (value =='B03'){
                    return "B03-护照";
                }else if (value =='B04'){
                    return "B04-军官证";
                }else if (value =='B05'){
                    return "B05-士兵证";
                }else if (value =='B06'){
                    return "B06-港澳居民来往内地通行证";
                }else if (value =='B07'){
                    return "B07-台湾同胞来往内地通行证";
                }else if (value =='B08'){
                    return "B08-临时身份证";
                }else if (value =='B09'){
                    return "B09-外国人居留证";
                }else if (value =='B10'){
                    return "B10-警官证";
                }else if (value =='B11'){
                    return "B11-外国人永久居留身份证";
                } else if (value =='B12'){
                    return "B12-港澳台居民居住证";
                } else if (value =='B99'){
                    return "B99-其他证件";
                }
                return value;
            }},
            {field: 'customercode', title: '客户证件代码', width: 100, align: 'center'},
            {field: 'country', title: '国籍', width: 100, align: 'center',formatter:function (value) {
            	for (var i = 0;i <  baseCountryTwos.length;i++){
                    if (baseCountryTwos[i].substring(0,2)==value){
                        return baseCountryTwos[i];
                    }
                }
                }
            },
            {field: 'nation', title: '民族', width: 100, align: 'center',formatter:function (value) {
            	for (var i = 0;i <  baseNations.length;i++){
                    if (baseNations[i].substring(0,2)==value){
                        return baseNations[i];
                    }
                }
                }
            },
            {field: 'sex', title: '性别', width: 100, align: 'center',formatter: function (value) {
            	if(value == '01'){
            		return "01-男性";
            	}else if(value == '02'){
            		return "02-女性";
            	}else if(value == '03'){
            		return "03-其他性别";
            	}else{
            		return value;
            	}
            }
            },
            {field: 'education', title: '最高学历', width: 100, align: 'center',formatter:function (value) {
            	for (var i = 0;i <  baseEducations.length;i++){
                    if (baseEducations[i].substring(0,2)==value){
                        return baseEducations[i];
                    }
                }
                }
            },
            {field: 'birthday', title: '出生日期', width: 100, align: 'center'},
            {field: 'regareacode', title: '地区代码', width: 100, align: 'center',formatter:function (value) {
            	for (var i = 0;i <  baseAreas.length;i++){
                    if (baseAreas[i].substring(0,6)==value){
                        return baseAreas[i];
                    }
                }
                }},
            {field: 'grincome', title: '个人年收入', width: 100, align: 'center'},
            {field: 'familyincome', title: '家庭年收入', width: 100, align: 'center'},
            {field: 'marriage', title: '婚姻情况', width: 100, align: 'center',formatter: function (value) {
            	if(value == 'M01'){
            		return "M01-未婚";
            	}else if(value == 'M02'){
            		return "M02-已婚";
            	}else if(value == 'M03'){
            		return "M03-丧偶";
            	}else if(value == 'M07'){
            		return "M07-离异";
            	}
            }
            },
             {field: 'isrelation', title: '是否关联方', width: 100, align: 'center',formatter:function (value) {
                 if (value=='1'){
                     return "1-是";
                 }else if(value=='0'){
                     return "0-否";
                 }
                 return value;
             }},
            {field: 'creditamt', title: '授信额度', width: 100, align: 'center'},
            {field: 'usedamt', title: '已用额度', width: 100, align: 'center'},
			{field: 'grkhsfbs', title: '个人客户身份标识', width: 100, align: 'center', formatter: function (value) {
            	if(value == '1'){
            		return "1-农户";
            	}else if(value == '2'){
            		return "2-个体工商户";
            	}else if(value == '3'){
            		return "3-小微企业主";
            	}else if(value == '9'){
            		return "9-其他";
            	}else{
            		return value;
            	}
			}
            },
            {field: 'gtgshyyzzdm', title: '个体工商户营业执照代码', width: 100, align: 'center'},
            {field: 'xwqyshtyxydm', title: '小微企业社会统一信用代码', width: 100, align: 'center'},
            {field: 'workareacode', title: '客户信用级别总等级数', width: 100, align: 'center'},
            {field: 'actctrltype', title: '客户信用评级', width: 100, align: 'center'},
            {field: 'sjrq', title: '数据日期', width: 100, align: 'center'},
            {field: 'operator', title: '操作人',  align: 'center'},
            {field: 'operationtime', title: '操作时间',  align: 'center'}
    ]],
		onDblClickRow: function (rowIndex,rowData) {
			$('#info_formForAdd').form('clear');
			$('#info_dialog1').dialog('open').dialog('center').dialog('setTitle','个人客户基础信息详情');
			$('#info_formForAdd').form('load',rowData);
		}
	});
})

// 查询
function searchOnSelected(){
	var finorgcodeParam = $("#finorgcodeParam").val().trim();
	var customercodeParam = $("#customercodeParam").val().trim();
    var regamtcrenyParam = $("#regamtcrenyParam").combobox("getValue");
	$("#dg1").datagrid("load", {"finorgcodeParam" : finorgcodeParam,"customercodeParam":customercodeParam,"regamtcrenyParam":regamtcrenyParam});
}

// 生成报文
function genMessage(){
	var v = $('#dateform').form('validate');
	if(v){
		var value = $('#dateselect').datebox('getValue');
		$('#datedialog').window('close');
		$.messager.confirm('提示','是否生成报文？',function (r){
	        if(r){
				$.messager.progress({
					title: '请稍等',
					msg: '数据处理中......'
				});
				$.ajax({
					url: "XCreateReportXgrkhxx",
					type: "POST",
					contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
					async: true,
					data:{"date":value},
			        dataType:'json',
					success: function (data) {
						$.messager.progress('close');
						$.messager.alert('操作提示', '生成报文成功'+data+'条', 'info');
						$("#dg1").datagrid("reload");
					},
					error: function (error) {
						$.messager.progress('close');
						$.messager.alert('操作提示', '生成报文失败', 'error');
					}
				})
	        }
	    });
	}
    
}
	// 打回按钮
	function goback() {
		var rows = $('#dg1').datagrid('getSelections');
		var info = '';
		var id = '';
		for (var i = 0;i < rows.length;i++){
			id = id + rows[i].id + ',';
		}
		if(rows.length > 0){
			info = '是否打回选中的数据？';
		}else {
			info = '是否打回所有的数据？';
		}
		var finorgcodeParam = $("#finorgcodeParam").val().trim();
		var customercodeParam = $("#customercodeParam").val().trim();
		$.messager.confirm('提示',info,function (r){
			if(r){
				$.ajax({
					sync: true,
					type: "POST",
					dataType: "json",
					url: "XgobackXgrkhxx",
					data:{"id" : id,"finorgcodeParam" : finorgcodeParam,"customercodeParam" : customercodeParam},
					contentType:'application/x-www-form-urlencoded; charset=UTF-8',
					success: function (data) {
						if (data == 0){
							$.messager.alert('操作提示','打回成功','info');
							$("#dg1").datagrid("reload");
						}else {
							$.messager.alert('操作提示','打回失败','info');
						}
					},
					error: function (err) {
						$.messager.alert('操作提示','打回失败','error');
					}
				});
			}
		});
	}
	
	//返回
    function fanhui(){
    	$.messager.progress({
            title: '请稍等',
            msg: '数据正在加载中......'
        });    	
    }
</script>
</body>
</html>
