<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String datamanege = (String) request.getAttribute("datamanege");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>金融机构（分支机构）基础信息</title>
<%@include file="../../common/head.jsp"%>
</head>
<body>
	<table id="dg1" style="height: 480px;" title="金融机构（分支机构）基础信息"></table>
	
	<!--表格工具栏-->
	<div id="tb1" style="padding: 5px; height: auto;">
		<%if (!"datamanege".equals(datamanege)){%>
		<a class="easyui-linkbutton" href="XCreateReportManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;
		金融机构名称 ：<input type="text" id="selectjrjgmc" name="selectjrjgmc" style="height: 23px; width:200px"/>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="chaxun()">查询</a></br>
		<%--<a class="easyui-linkbutton" iconCls="icon-generatemessage" onclick="shengchengbaowen()">生成报文</a>&nbsp;&nbsp;--%>
		<a class="easyui-linkbutton" iconCls="icon-generatemessage" onclick="showDate()">生成报文</a>&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-cross" onclick="shujudahui()">数据打回</a>
		<%}else {%>
		<a class="easyui-linkbutton" href="XDataManager" iconCls="icon-return">返回</a>&nbsp;&nbsp;
		<%}%>
	</div>


<!-- 导入文件dialog -->
    <div style="visibility: hidden;">
        <div id="importFileDialog" class="easyui-dialog" style="width: 600px; height: 160px; padding-left: 30px" title="导入数据">
            <h2>请选择要导入的Excel文件</h2>
            <table id="importTable" border="0">
                <tr>
                    <td><input type="file" name="file_info" id="file_info" size="60" onchange="fileSelected()" />&nbsp;</td>
                </tr>
                <tr>
                    <td><input type="button" id="importId" value="提交" size="60" onclick="fileUp()" /></td>
                </tr>
            </table>
        </div>
    </div>

	<div style="visibility: hidden;">
	<div id="dialog_d" class="easyui-dialog" title="生成报文日期选择" data-options="iconCls:'icon-more'" style="width:400px;height:200px;padding-left:30px;padding-top:50px">		
		<form id="formForDate" method="post">
		<table>
			<tr>
				<td>请选择报文日期:</td>
				<td>
					<input class="easyui-datebox" id="createdate" name="date" style="height: 23px; width:150px" data-options="required:true" missingMessage="必填项"/>
				</td>
			</tr>
		</table>
		</form>
		<div id="tbForEditDialog" style="padding-left: 30px;padding-top: 10px">
			<a class="easyui-linkbutton" data-options="iconCls:'icon-save'" id="queding" onclick="shengchengbaowen()">确定</a>&nbsp;&nbsp;
			<a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="resetForEdit()">取消</a>
		</div>
	</div>
</div>

<!-- 详情 -->
<div style="visibility: hidden;">
    <div id="dialog_m" class="easyui-dialog" title="金融机构（分支机构）基础信息"  data-options="iconCls:'icon-more'" style="width:520px;height:500px;">
          
         <form id="formForMore" method="post">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
               <tr>
                 <td>金融机构名称 :</td>
                 <td><input class="easyui-validatebox" id="finorgname_m" name="finorgname" style="height: 23px; width:230px" disabled="disabled"/></td>
                </tr> 
                 
               <tr>
                 <td>金融机构代码:</td>
                 <td><input class="easyui-validatebox" id="finorgcode_m" name="finorgcode" style="height: 23px; width:230px" disabled="disabled"/></td>
              </tr>
                
               <tr> 
                 <td>金融机构编码 :</td>
                 <td><input class="easyui-validatebox" id="finorgnum_m" name="finorgnum" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>  
                 <td>内部机构号 :</td>
                 <td><input class="easyui-validatebox" id="inorgnum_m" name="inorgnum" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
			   
			   <tr> 
                 <td>许可证号 :</td>
                 <td><input class="easyui-validatebox" id="xkzh_m" name="xkzh" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>  
                 <td>支付行号 :</td>
                 <td><input class="easyui-validatebox" id="zfhh_m" name="zfhh" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr> 
			   
			   <tr>
			     <td>机构级别:</td>
                 <td>
                 <select id="orglevel_m" name="orglevel" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:总行</option>
						<option value="02">02:分行</option>
						<option value="03">03:支行</option>
						<option value="04">04:网点（分理处、储蓄所等）</option>
						<option value="05">05:事业部</option>
						<option value="99">99:其他</option>
	                </select>
                 </td>
               </tr> 
                
               <tr>  
                 <td>直属上级管理机构名称:</td>
                 <td><input class="easyui-validatebox" id="highlevelorgname_m" name="highlevelorgname" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>  
                 <td>直属上级管理机构金融机构编码 :</td>
                 <td><input class="easyui-validatebox" id="highlevelfinorgcode_m" name="highlevelfinorgcode" style="height: 23px; width:230px" disabled="disabled"/></td>
               </tr>
			   
			   <tr>
			     <td>直属上级管理机构内部机构号 :</td>
			     <td><input class="easyui-validatebox" id="highlevelinorgnum_m" name="highlevelinorgnum" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>注册地址 :</td>
			     <td><input class="easyui-validatebox" id="regarea_m" name="regarea" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>地区代码 :</td>
			     <td>
			        <input id="regareacode_m" name="regareacode" style="height: 23px; width:230px" data-options="valueField:'id',textField:'text'" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <!-- <tr>
			     <td>办公地址 :</td>
			     <td>
			        <input class="easyui-validatebox" id="workarea_m" name="workarea" style="height: 23px; width:230px" disabled="disabled"/>
			     </td>
			   </tr>
			   
			   <tr>
			     <td>办公地行政区划代码 :</td>
			     <td>
			        <input id="workareacode_m" name="workareacode" style="height: 23px; width:230px" data-options="valueField:'id',textField:'text'" disabled="disabled"/>
			     </td>
			   </tr> -->
			   
			   <tr>
			     <td>成立时间 :</td>
			     <td><input id="setupdate_m" name="setupdate" style="height: 23px; width:230px" disabled="disabled"/></td>
			   </tr>
			   
			   <tr>
			     <td>营业状态 :</td>
			     <td>
			        <select id="mngmestus_m" name="mngmestus" style="height: 23px; width:230px" disabled="disabled">
						<option value="01">01:正常运营</option>
					    <option value="02">02:停业（歇业）</option>
					    <option value="03">03:筹建</option>
					    <option value="04">04:当年关闭</option>
					    <option value="05">05:当年破产</option>
					    <option value="06">06:当年注销</option>
					    <option value="07">07:当年吊销</option>
					    <option value="99">99:其他</option>
	                </select>
			     </td>
			   </tr>

				 <tr>
					 <td>数据日期 :</td>
					 <td><input id="sjrq_m" name="setupdate" style="height: 23px; width:230px" disabled="disabled"/></td>
				 </tr>

			 </table>
           </div>
         </form>
	</div>
</div>	

<script type="text/javascript">
$(function(){
	$('#dialog_m').dialog('close');
    $('#dialog_d').dialog('close');
	$("#importFileDialog").dialog("close");
	getbasecode();
	
	$("#dg1").datagrid({
		method:'post',
		url:'XfindJRJGFZJCXXscbw',
		loadMsg:'数据加载中,请稍后...',
		//singleSelect:true,
		checkOnSelect:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb1',
		fitColumns:true,
		pageSize:20,
		pageList:[15,20,30,50],
		columns:[[
			{field:'ck',checkbox:true},
		    {field:'operationname',title:'操作名',width:100,align:'center'},
		    {field:'nopassreason',title:'审核不通过原因',width:100,align:'center'},
		    {field:'checkstatus',title:'校验结果',width:100,align:'center',formatter: function(value,row,index){					
				if(value == '2'){
					return '<span style="color:red;">' + '校验失败' + '</span>';
				}else if(value == '1'){
					return '校验成功';
				}else if(value == '0'){
					return '未校验';
				}else{
					return value;
				}
				}},
		    {field:'finorgname',title:'金融机构名称',width:100,align:'center'},
		    {field:'finorgcode',title:'金融机构代码',width:100,align:'center'},
		    {field:'finorgnum',title:'金融机构编码',width:100,align:'center'},
		    {field:'inorgnum',title:'内部机构号',width:100,align:'center'},
		    {field:'xkzh',title:'许可证号',width:100,align:'center'},
		    {field:'zfhh',title:'支付行号',width:100,align:'center'},
		    {field:'orglevel',title:'机构级别',width:100,align:'center'},
		    {field:'highlevelorgname',title:'直属上级管理机构名称',width:100,align:'center'},
		    {field:'highlevelfinorgcode',title:'直属上级管理机构金融机构编码',width:100,align:'center'},
		    {field:'highlevelinorgnum',title:'直属上级管理机构内部机构号',width:100,align:'center'},
		    {field:'regarea',title:'注册地址',width:100,align:'center'},
		    {field:'regareacode',title:'地区代码',width:100,align:'center'},
		    {field:'workarea',title:'办公地址',width:100,align:'center',hidden:true},
		    {field:'workareacode',title:'办公地行政区划代码',width:100,align:'center',hidden:true},
		    {field:'setupdate',title:'成立时间',width:100,align:'center'},
		    {field:'mngmestus',title:'营业状态',width:100,align:'center'},
            {field:'sjrq',title:'数据日期',width:100,align:'center'},
		    {field:'operator',title:'操作人',width:100,align:'center'},
		    {field:'operationtime',title:'操作时间',width:100,align:'center'}
    	]],
	    onDblClickRow :function(rowIndex,rowData){
	    	initForm();
	    	var dia = $('#dialog_m').dialog('open');
        	$("#formForMore").form('load',rowData);
        	$('#dialog_m').dialog({modal : true});	
        	//$("#orglevel_m").combobox({disabled: true});
        	//$("#regareacode_m").combobox({disabled: true});
        	//$("#workareacode_m").combobox({disabled: true});
        	//$("#setupdate_m").datebox({disabled: true});
        	//$("#mngmestus_m").combobox({disabled: true});
        	//关闭弹窗默认隐藏div
    		/* $(dia).window({
    	    	onBeforeClose: function () {
    	    		//初始化表单的元素的状态
    	    		initForm();
    	    	}
    		}); */
	    }
	});
	
	//$("#dg1").datagrid('loadData',datas);
});

//获取基础代码
function getbasecode(){
	//getAindustry("CURRENCY"); //一级行业代码
	getArea(); //行政区划代码
	//getBindustry("customerIdType"); //二级行业代码
	//getCountry("customerIdType2"); //国家代码
	//getCurrency("artificialPersonType"); //币种代码
}

//获取行政区划代码
function getArea(){
	$.ajax({
	    url:'getAreaCode',
	    type:'POST', //GET
	    async:true,    //或false,是否异步
	    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
	    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
	    success:function(data){
	    	var areaList = data.list;
		    if(areaList != null && areaList != ""){
		    	var jsonStr='[';
		    	for(var i = 0;i<areaList.length;i++){
		     		 jsonStr = jsonStr + '{"id":"'+areaList[i].areacode+'",'+'"val":"'+areaList[i].areaname+'",'+'"text":"'+areaList[i].areacode+'-'+areaList[i].areaname+'"},'
		        }
		    	jsonStr = jsonStr.substring(0,jsonStr.lastIndexOf(','));   //如果是以,结尾，则截取,前面的字符串
		        jsonStr = jsonStr + ']';
		    	
		        var jsonObj = JSON.parse(jsonStr);     //将json格式的字符串转为json对象
		 	   $("#assessmethod").combobox("loadData",jsonObj);
		 	   $("#assessmethod").combobox({
		        	filter: function(q, row){
		        		var opts = $(this).combobox('options');
		        		return match(row[opts.textField],q)!=-1;
		        	}
		       });
		 	   $("#assessmethod_m").combobox("loadData",jsonObj);
		 	   $("#workareacode").combobox("loadData",jsonObj);
		 	   $("#workareacode").combobox({
		        	filter: function(q, row){
		        		var opts = $(this).combobox('options');
		        		return match(row[opts.textField],q)!=-1;
		        	}
		       });
		 	   $("#workareacode_m").combobox("loadData",jsonObj);
		    }
	    }
	});
}


//查询
function chaxun(){
	var selectjrjgmc = $("#selectjrjgmc").val();
	$("#dg1").datagrid("load",{selectjrjgmc:selectjrjgmc});
}

function initForm(){
	//document.getElementById("formForMore").reset(); 
	$("#finorgname_m").val('');
	$("#finorgcode_m").val('');
	$("#finorgnum_m").val('');
	$("#inorgnum_m").val('');
	$("#xkzh_m").val('');
	$("#zfhh_m").val('');
	$("#orglevel_m").val('setValue','');
	$("#highlevelorgname_m").val('');
	$("#highlevelfinorgcode_m").val('');
	$("#highlevelinorgnum_m").val('');
	$("#regarea_m").val('');
	$("#regareacode_m").val('setValue','');
	//$("#workarea_m").val('');
	//$("#workareacode_m").val('setValue','');
	$("#setupdate_m").val('setValue','');
    $("#sjrq_m").val('setValue','');
	$("#mngmestus_m").val('setValue','');
}

//数据打回
function shujudahui(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定将选中的数据打回吗',function(r){
			if(r){
				var selectid = "";
				for (var i = 0; i < rows.length; i++){
					selectid = selectid+rows[i].id+"-";
				}
				$.ajax({
		            url:'Xdahuijrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"checktype":"some","rows":JSON.stringify(rows)},
		            data:{"checktype":"some","selectid":selectid},
		            dataType:'json',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','数据打回成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则失败
			           		$.messager.alert('','数据打回失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定将所有的数据打回吗',function(r){
			if(r){
				$.ajax({
		            url:'Xdahuijrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"checktype":"some","rows":JSON.stringify(rows)},
		            data:{"checktype":"all"},
		            dataType:'json',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','数据打回成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则失败
			           		$.messager.alert('','数据打回失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}
//打开日期选择
function showDate(){
    $("#createdate").datebox('setValue','');
	$('#dialog_d').dialog('open');
}
//取消
function resetForEdit() {
    $("#createdate").datebox('setValue','');
    $('#dialog_d').dialog('close');
}


//生成报文
function shengchengbaowen(){
	var rows = $('#dg1').datagrid('getSelections');
	/*if(rows.length > 0){
		$.messager.confirm('操作提示','确定将选中的数据生成报文吗',function(r){
			if(r){
				$.ajax({
		            url:'Xcreatemessagejrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            data:{"checktype":"some","rows":JSON.stringify(rows)},
		            //data:{"checktype":"some","checkid":checkid},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','生成报文成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则生成失败
			           		$.messager.alert('','生成报文失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           		//$('#dg1').datagrid('reload');
							//$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							//window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定将所有的数据生成报文吗',function(r){
			if(r){
				$.ajax({
		            url:'Xcreatemessagejrjgfz',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"checktype":"all"},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','生成报文成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则生成失败
			           		$.messager.alert('','生成报文失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           		//$('#dg1').datagrid('reload');
							//$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							//window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}*/
	// $.messager.confirm('提示','是否生成报文？',function (r){
	// 	if(r){
	// 		$.messager.progress({
	// 			title: '请稍等',
	// 			msg: '数据处理中......'
	// 		});
	// 		$.ajax({
	// 			url: "XCreateReportFive",
	// 			type: "POST",
	// 			contentType: 'application/json;charset=UTF-8',
	// 			async: true,
	// 			success: function (data) {
	// 				$.messager.progress('close');
	// 				$.messager.alert('操作提示', '生成报文成功'+data+'条', 'info');
	// 				$("#dg1").datagrid("reload");
	// 			},
	// 			error: function (error) {
	// 				$.messager.progress('close');
	// 				$.messager.alert('操作提示', '生成报文失败', 'error');
	// 			}
	// 		})
	// 	}
	// });
	$.messager.confirm('提示','是否生成报文？',function (r){
        if(r) {
            $("#formForDate").form('submit', {
                url: 'XCreateReportFive',
                contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                dataType: 'json',
                async: true,
                onSubmit: function () {
                    var isValidate = $("#formForDate").form('validate');
                    if (!isValidate) {
                        return isValidate;
                    } else {
                        $.messager.progress({
                            title: '请稍等',
                            msg: '处理中......',
                            timeout: 10000,
                        });
                    }
                },
                success: function (data) {
                    $.messager.progress('close');
                    $.messager.alert('操作提示', '生成报文成功' + data + '条', 'info');
                    $('#dialog_d').dialog('close');
                    $("#createdate").datebox('setValue','');
                    $("#dg1").datagrid("reload");
                },
                error: function (error) {
                    $.messager.progress('close');
                    $.messager.alert('操作提示', '生成报文失败', 'error');
                    $("#createdate").datebox('setValue','');
                    $('#dialog_d').dialog('close');
                }
            });
        }
    });

}

//校验
function jiaoyan(){
	var rows = $('#dg1').datagrid('getSelections');
	if(rows.length > 0){
		$.messager.confirm('操作提示','确定要校验选中的数据吗',function(r){
			if(r){
				/* var checkid = "";
				for (var i = 0; i < rows.length; i++){
					checkid = checkid+rows[i].id+"-";
				} */
				$.ajax({
		            url:'checkXdkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            data:{"checktype":"some","rows":JSON.stringify(rows)},
		            //data:{"checktype":"some","checkid":checkid},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','校验成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则删除失败
			           		/* $.messager.alert('','校验失败'+res,'error',function(r){
			           			$('#dg1').datagrid('reload');
			           		}); */
			           		$('#dg1').datagrid('reload');
							$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}else{
		$.messager.confirm('操作提示','确定要校验所有数据吗',function(r){
			if(r){
				$.ajax({
		            url:'checkXdkdbwx',
		            type:'POST', //GET
		            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		            //data:{"rows":JSON.stringify(rows)},
		            data:{"checktype":"all"},
		            dataType:'text',    
		            success:function(res){
			           	if(res == '1'){ 
			           		$.messager.alert('','校验成功','info',function(r){
			           			$('#dg1').datagrid('reload');
			           		});
			           	}else{  
			           		//否则删除失败
			           		//$.messager.alert('操作提示','校验失败','error');
			           		$('#dg1').datagrid('reload');
							$.messager.alert('操作提示','校验完成,将自动下载校验结果','info');
							window.location.href = "downFileCheckXdkdbwx?name="+res;
			           	}
		            },
		           	error : function(){
		    			$.messager.alert('操作提示','网络异常请稍后再试','error');
		    		}
		        });
			}
		});
	}
}


</script>
</body>
</html>
