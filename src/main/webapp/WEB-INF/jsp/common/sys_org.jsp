<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>机构信息</title>
    <%@include file="head.jsp"%>
</head>
<body>
	<table class="easyui-datagrid" title="机构信息" id="dg1"
			data-options="singleSelect:true,rownumbers:true,toolbar:'#tb',autoRowHeight:false,pagination:true,pageSize:25,pageList:[15,25,35]" style="height: 460px">
		<thead>
			<tr>
			    <th field="ck" checkbox="true"></th>
			    <th data-options="field:'id',width:80,align:'center',hidden:true">ID</th>
			    <th data-options="field:'orgId',width:120,align:'center'">机构编号</th>
				<th data-options="field:'orgName',width:200,align:'center'">机构名称</th>
				<th data-options="field:'orgInsideCode',width:160,align:'center'">内部机构号</th>
				<th data-options="field:'licenseNumber',width:160,align:'center'">金融许可证号</th>
				<th data-options="field:'orgSortName',width:120,align:'center'">机构简称</th>
				<th data-options="field:'orgParentId',width:120,align:'center'">上级机构编号</th>
			    <th data-options="field:'orgRegion',width:200,align:'center'">机构所属区域</th>
			    <th data-options="field:'enabled',width:80,align:'center'">是否停用</th>
				<th data-options="field:'isBussiness',width:80,align:'center'">是否拥有业务</th>
				<th data-options="field:'isHead',width:100,align:'center'">是否为上级机构</th>
				<th data-options="field:'orgLevel',width:80,align:'center'">机构等级</th>
				<th data-options="field:'tel',width:80,align:'center'">联系人</th>
				<th data-options="field:'option_',width:120,align:'center'">联系电话</th>
			</tr>
		</thead>
		
		<tbody>
		   <c:forEach items="${list}" var="li">
			<tr> 
			    <td></td>
			    <td>${li.id}</td>
                <td>${li.orgId}</td>
                <td>${li.orgName}</td>
                <td>${li.orgInsideCode}</td>
                <td>${li.licenseNumber}</td>
                <td>${li.orgSortName}</td>
                <td>${li.orgParentId}</td>
                <td>${li.orgRegion}</td>
                <td>
                    <c:if test="${li.enabled == 'N'}">
							<a href="javascript:void(0);" style="text-decoration: none" onclick="remove('${li.id}','${li.enabled}')"><span style="color: red">已停用</span></a>
				    </c:if> 
				    
				    <c:if test="${li.enabled == 'Y'}">
							<a href="javascript:void(0);" style="text-decoration: none" onclick="remove('${li.id}','${li.enabled}')">未停用</a>
					</c:if>
				</td>
                <td>${li.isBussiness}</td>
                <td>${li.isHead}</td>
                <td>${li.orgLevel}</td>
                <td>${li.option_}</td>
                <td>${li.tel}</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
	
	<div id="tb" style="padding:5px;height:auto;">
		机构名称:
			<input type="text" style="width:173px;height:21px" id="orgName" name="orgName" value="${orgName}">&nbsp;&nbsp;
		    <a class="easyui-linkbutton" iconCls="icon-search" onclick="selectRecordByName()">查询</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-add"  onclick="addOrg()">新增</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a class="easyui-linkbutton" iconCls="icon-edit"  onclick="editOrg()">修改</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- <a class="easyui-linkbutton" iconCls="icon-remove" onclick="remove()">删除机构</a> -->
			<!-- <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="openExcelTemplet()">导出</a> -->
	</div>
	
	
<!-- 新增机构dialog -->
<div style="visibility: hidden;">
   <div id="addDialog" class="easyui-dialog" title="新增机构" data-options="iconCls:'icon-save',toolbar:'#tbForAddDialog'" style="width:500px;height:400px;">
          <!--   新增对话框的工具栏  -->
          <div id="tbForAddDialog" style="padding-left: 30px;padding-top: 10px">
	           <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveForAddDialog()">保存</a>&nbsp;&nbsp;
               <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="resetForAdd()">重置</a>
          </div>
          
         <!-- 信息录入 -->
         <form id="formForAddOrg" method="post">
           <div style="padding-top: 10px">
             <table style="padding-left: 30px;font-size: 8px;">
               <tr>
                 <td align="right">机构编号:</td>
			     <td><input class="easyui-validatebox" name="orgId_add" id="orgId_add" data-options="required:true,missingMessage:'机构编号不能为空'" style="height: 21px; width: 173px"/></td>
			   </tr>
             
               <tr>
                 <td align="right">机构名称:</td>
			     <td><input class="easyui-validatebox" name="orgName_add" id="orgName_add" data-options="required:true,missingMessage:'机构名称不能为空'" style="height: 21px; width: 173px"/></td>
			   </tr>
			
			   <tr>
                 <td align="right">内部机构号:</td>
			     <td><input class="easyui-validatebox" name="orgInsideCode_add" id="orgName_add" data-options="required:true,missingMessage:'内部机构号不能为空'" style="height: 21px; width: 173px"/></td>
			   </tr>
			   
			   <tr>
                 <td align="right">金融许可证号:</td>
			     <td><input class="easyui-validatebox" name="licenseNumber_add" id="orgName_add" data-options="required:true,missingMessage:'金融许可证号不能为空'" style="height: 21px; width: 173px"/></td>
			   </tr>
			   
			   <tr>
                 <td align="right">机构简称:</td>
			     <td><input type="text" name="orgSortName_add" id="orgSortName_add" style="height: 21px; width: 173px"/></td>
			   </tr>
			   
               <tr>
                 <td align="right">上级机构编码:</td>
			     <td>
			         <input class="easyui-combobox" name="orgParentId_add" id="orgParentId_add" data-options="valueField:'id',textField:'text',editable:false" style="height: 21px;width: 173px">
			         <%-- <select class="easyui-combobox" name="orgParentId_add" id="orgParentId_add" editable=false style="height: 32px;width: 173px">
			             <option value="">---没有上级机构---</option>
			             <c:forEach items="${list}" var="ol">
			                 <option value="${ol.orgId}">${ol.orgId}-${ol.orgName}</option>
			             </c:forEach>
			         </select> --%>
				 </td>
			   </tr>
			   
			   <tr>
                 <td align="right">机构所属区域:</td>
			     <td><input type="text" name="orgRegion_add" id="orgRegion_add" style="height: 21px; width: 173px"/></td>
			   </tr>
			   
			   <tr>
                 <td align="right">是否拥有业务:</td>
			     <td>
			       <select class="easyui-combobox" name="isBussiness_add" id="isBussiness_add" editable=false style="height: 21px;width: 173px">
			           <option value="Y" selected="selected">包含业务</option>
			           <option value="N">无业务</option>
			       </select>
			     </td>
			   </tr>
			   
			   <tr>
                 <td align="right">是否为顶级机构:</td>
			     <td>
			       <input type="text" name="isHead_add" id="isHead_add" editable=false style="height: 21px;width: 173px" readonly="readonly"/>
			       <!-- <select class="easyui-combobox" name="isHead_add" id="isHead_add" editable=false style="height: 32px;width: 173px">
			           <option value="N" selected="selected">否</option>
			           <option value="Y">是</option>
			       </select> -->
			     </td>
			   </tr>
			   
			    <tr>
                 <td align="right">机构等级:</td>
			     <td>
			       <select class="easyui-combobox" name="orgLevel_add" id="orgLevel_add"  editable=false style="height: 21px;width: 173px">
			           <option value="全国" selected="selected">全国</option>
			           <option value="省级">省级</option>
			           <option value="市级">市级</option>
			           <option value="境外">境外</option>
			           <option value="行编一级">行编一级</option>
			           <option value="行编二级">行编二级</option>
			           <option value="行编三级">行编三级</option>
			           <option value="行编四级">行编四级</option>
			           <option value="行编五级">行编五级</option>
			       </select>
			     </td>
			   </tr>
			   
			   <tr>
                 <td align="right">联系人:</td>
			     <td><input type="text" name="option_add" id="option_add" style="height: 21px; width: 173px"/></td>
			   </tr>
			   
			   <tr>
                 <td align="right">联系电话:</td>
			     <td><input type="text" name="tel_add" id="tel_add" style="height: 21px; width: 173px"/></td>
			   </tr>
              </table>
           </div>
         </form>
	</div>
</div>





<!-- 修改机构dialog -->
<div style="visibility: hidden;">
   <div id="editDialog" class="easyui-dialog" title="修改机构" data-options="iconCls:'icon-save',toolbar:'#tbForEditDialog'" style="width:500px;height:400px;">
          <!--   新修改对话框的工具栏  -->
          <div id="tbForEditDialog" style="padding-left: 30px;padding-top: 10px">
	           <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveForEditDialog()">保存</a>&nbsp;&nbsp;
               <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="resetForEdit()">重置</a>
          </div>
          
         <!-- 信息录入 -->
         <form id="formForEditOrg" method="post">
           <div style="padding-top: 10px">
             <table style="padding-left: 30px;font-size: 8px;">
              <input type="hidden" name="editId" id="editId"/>
              <input type="hidden" name="isEditOrgId" id="isEditOrgId"/>
              <input type="hidden" name="isEditOrgName" id="isEditOrgName"/>
              
               <tr>
                 <td align="right">机构编号:</td>
			     <td><input class="easyui-validatebox" name="orgId_edit" id="orgId_edit" data-options="required:true,missingMessage:'机构编号不能为空'" style="height: 21px; width: 173px"/></td>
			   </tr>
             
               <tr>
                 <td align="right">机构名称:</td>
			     <td><input class="easyui-validatebox" name="orgName_edit" id="orgName_edit" data-options="required:true,missingMessage:'机构名称不能为空'" style="height: 21px; width: 173px"/></td>
			   </tr>
			   
			    <tr>
                 <td align="right">内部机构号:</td>
			     <td><input class="easyui-validatebox" name="orgInsideCode_edit" id="orgInsideCode_edit" data-options="required:true,missingMessage:'内部机构号不能为空'" style="height: 21px; width: 173px"/></td>
			   </tr>
			   
			   <tr>
                 <td align="right">金融许可证号:</td>
			     <td><input class="easyui-validatebox" name="licenseNumber_edit" id="licenseNumber_edit" data-options="required:true,missingMessage:'金融许可证号不能为空'" style="height: 21px; width: 173px"/></td>
			   </tr>
			   
			   <tr>
                 <td align="right">机构简称:</td>
			     <td><input type="text" name="orgSortName_edit" id="orgSortName_edit" style="height: 21px; width: 173px"/></td>
			   </tr>
			   
               <tr>
                 <td align="right">上级机构编码:</td>
			     <td>
			         <%-- <select class="easyui-combobox" name="orgParentId_edit" id="orgParentId_edit" style="height: 32px;width: 173px">
			              <c:forEach items="${list}" var="ol">
			                 <option value="${ol.orgId}">${ol.orgId}-${ol.orgName}</option>
			             </c:forEach>
			         </select> --%>
			         <input class="easyui-combobox" name="orgParentId_edit" id="orgParentId_edit" data-options="valueField:'id',textField:'text',editable:false" style="height: 21px;width: 173px">
				 </td>
			   </tr>
			   
			   <tr>
                 <td align="right">机构所属区域:</td>
			     <td><input type="text" name="orgRegion_edit" id="orgRegion_edit" style="height: 21px; width: 173px"/></td>
			   </tr>
			   
			   <tr>
                 <td align="right">是否拥有业务:</td>
			     <td>
			       <select class="easyui-combobox" name="isBussiness_edit" id="isBussiness_edit" editable=false style="height: 21px;width: 173px">
			           <option value="Y">包含业务</option>
			           <option value="N">无业务</option>
			       </select>
			     </td>
			   </tr>
			   
			   <tr>
                 <td align="right">是否为顶级机构:</td>
			     <td>
			        <input type="text" name="isHead_edit" id="isHead_edit" editable=false style="height: 21px;width: 173px" readonly="readonly"/>
			     </td>
			   </tr>
			   
			    <tr>
                 <td align="right">机构等级:</td>
			     <td>
			       <select class="easyui-combobox" name="orgLevel_edit" id="orgLevel_edit" editable=false style="height: 21px;width: 173px">
			           <option value="全国">全国</option>
			           <option value="省级">省级</option>
			           <option value="市级">市级</option>
			           <option value="境外">境外</option>
			           <option value="行编一级">行编一级</option>
			           <option value="行编二级">行编二级</option>
			           <option value="行编三级">行编三级</option>
			           <option value="行编四级">行编四级</option>
			           <option value="行编五级">行编五级</option>
			       </select>
			     </td>
			   </tr>
			   
			   <tr>
                 <td align="right">联系人:</td>
			     <td><input type="text" name="option_edit" id="option_edit" style="height: 21px; width: 173px"/></td>
			   </tr>
			   
			   <tr>
                 <td align="right">联系电话:</td>
			     <td><input type="text" name="tel_edit" id="tel_edit" style="height: 21px; width: 173px"/></td>
			   </tr>
              </table>
           </div>
         </form>
	</div>
</div>


<!-- 导出模板选择 -->
<div style="visibility: hidden;">
   <div id="selectExcelTemplet" class="easyui-dialog" title="选择导出Excel模板" style="width:300px;height:120px;align-items: center;text-align: center;padding: 20px 5px">
        <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="exportExcel('xls')">xls(2003版)</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="easyui-linkbutton" iconCls="icon-page-excel" onclick="exportExcel('xlsx')">xlsx(2007版)</a>
	</div>
</div>


<!--------------------------------------------------js ------------------------------------------------------------------------------->	
<script type="text/javascript">
   
   var orgIdBackUp;       //存放选中要修改的机构编号
   var orgNameBackUp;     //存放选中要修改的机构名称
  
   $(function(){
	  $('#addDialog').dialog('close');
	  $('#editDialog').dialog('close');
	  $('#selectExcelTemplet').dialog('close');
	  $('#dg1').datagrid({loadFilter:pagerFilter});//.datagrid('loadData', getData());
	  $('.datagrid-header-check').find('input').attr('style','display:none');
	  
	  
	  $('#orgParentId_add').combobox({
		  onSelect: function(){
			  var headName = $("#orgParentId_add").combobox('getValue');
		      if(headName != ''){
		    	   $("#isHead_add").val('否');
		      }else{
		    	   $("#isHead_add").val('是');
		      }
			}
	  });
	  
	  $('#orgParentId_edit').combobox({
		  onSelect: function(){
			  var headName = $("#orgParentId_edit").combobox('getValue');
		      if(headName != ''){
		    	   $("#isHead_edit").val('否');
		      }else{
		    	   $("#isHead_edit").val('是');
		      }
			}
	  });
});

   
   function pagerFilter(data){
		if (typeof data.length == 'number' && typeof data.splice == 'function'){	// is array
			data = {
				total: data.length,
				rows: data
			}
		}
		var dg = $(this);
		var opts = dg.datagrid('options');
		var pager = dg.datagrid('getPager');
		pager.pagination({
			onSelectPage:function(pageNum, pageSize){
				opts.pageNumber = pageNum;
				opts.pageSize = pageSize;
				pager.pagination('refresh',{
					pageNumber:pageNum,
					pageSize:pageSize
				});
				dg.datagrid('loadData',data);
			}
		});
		if (!data.originalRows){
			data.originalRows = (data.rows);
		}
		var start = (opts.pageNumber-1)*parseInt(opts.pageSize);
		var end = start + parseInt(opts.pageSize);
		data.rows = (data.originalRows.slice(start, end));
		return data;
}
   
   
      
   //点击查询按钮
   function selectRecordByName(){
	   var orgName = $("#orgName").val();
	   window.location.href='getSys_OrgByLikeOrgName?orgName='+encode(orgName);
}

   //点击添加机构信息按钮
   function addOrg(){
	   var jsonStr = '[{"id":"","text":"---没有上级机构---"},';
	   var rows = $("#dg1").datagrid('getRows');
       for(var i = 0;i<rows.length;i++){
    	    jsonStr = jsonStr + '{"id":"'+rows[i].orgId+'",'+'"text":"'+rows[i].orgId+'-'+rows[i].orgName+'"},'
       }
       
       if(jsonStr.lastIndexOf(",") > 0){   //如果json以，结尾
    	   jsonStr = jsonStr.substring(0,jsonStr.lastIndexOf(','));   //如果是以},结尾，则截取,前面的字符串
       }
       
       jsonStr = jsonStr + ']';
       var jsonObj = JSON.parse(jsonStr);     //将json格式的字符串转为json对象
	   $("#orgParentId_add").combobox("loadData",jsonObj);
       
	   var headName = $("#orgParentId_add").combobox('getValue');
	   if(headName != ''){
	   	     $("#isHead_add").val('否');
	   }else{
	   	   $("#isHead_add").val('是');
	   }
	   $('#addDialog').dialog('open');  //打开对话框
	   $('#addDialog').dialog({modal:true});
}

   function editOrg(){
	   var row = $("#dg1").datagrid('getSelected'); //获取选中行对象
       if(row != null){ //如果选中
    	   var jsonStr = '[{"id":"","text":"---没有上级机构---"},';
    	   var rows = $("#dg1").datagrid('getRows');
           for(var i = 0;i<rows.length;i++){
        	   if(rows[i].orgId != row.orgId){
        			jsonStr = jsonStr + '{"id":"'+rows[i].orgId+'",'+'"text":"'+rows[i].orgId+'-'+rows[i].orgName+'"},'
        	   }
           }
          
           if(jsonStr.lastIndexOf(",") > 0){   //如果json以，结尾
        	   jsonStr = jsonStr.substring(0,jsonStr.lastIndexOf(','));   //如果是以},结尾，则截取,前面的字符串
           }
           
           jsonStr = jsonStr + ']';
           var jsonObj = JSON.parse(jsonStr);     //将json格式的字符串转为json对象
    	   $("#orgParentId_edit").combobox("loadData",jsonObj);
    	   
       	  //为修改dialog框中的文本框赋值
       	  document.getElementById("editId").value = row.id;
       	  orgIdBackUp = row.orgId;
       	  orgNameBackUp = row.orgName;
       	  $("#orgId_edit").val(row.orgId);
          $("#orgName_edit").val(row.orgName);
          $("#orgInsideCode_edit").val(row.orgInsideCode);
          $("#licenseNumber_edit").val(row.licenseNumber);
          $("#orgSortName_edit").val(row.orgSortName);
          $("#orgParentId_edit").combobox('setValue',row.orgParentId);
          $("#orgRegion_edit").val(row.orgRegion);
          $("#isBussiness_edit").combobox('setValue',row.isBussiness);
          if(row.isHead == 'Y'){
        	  $("#isHead_edit").val('是');
          }else{
        	  $("#isHead_edit").val('否');
          }
          $("#orgLevel_edit").combobox('setValue',row.orgLevel);
          $("#option_edit").val(row.option_);
          $("#tel_edit").val(row.tel);
          
       	  $('#editDialog').dialog('open');
   		  $('#editDialog').dialog({modal:true});
       }else{
       	$.messager.alert('操作提示','请选择一条数据修改','info');
       }
}
   
   
//停用、启用机构
function remove(id,enabled){
	if(enabled == 'N'){
		$.messager.alert('操作提示','确定启用该机构吗？','info',function(r) {
			$.ajax({
                    url:'deleteSys_Org',
                    type:'POST', //GET
                    async:true,    //或false,是否异步
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    data:{"id":id,"enabled":"Y"},
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(data){
                   	    if(data.result == '1'){  //如果返回1，成功
                   		    $.messager.alert('','启用机构成功!','info',function(r){
                   			   window.location.href="sys_org";
                   		    });
                   	    }else{  //否则失败
                   		    $.messager.alert('操作提示','此机构启用失败','error');
                   	    }
                    }
            })
		})
	}else{
		$.messager.alert('操作提示','停用机构后,该机构将不可用!停用后请修改正在使用该机构的用户!','warning',function(r) {
			$.ajax({
                    url:'deleteSys_Org',
                    type:'POST', //GET
                    async:true,    //或false,是否异步
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    data:{"id":id,"enabled":"N"},
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(data){
                   	    if(data.result == '1'){  //如果返回1，成功
                   		    $.messager.alert('','停用机构成功!','info',function(r){
                   			   window.location.href="sys_org";
                   		    });
                   	    }else{  //否则失败
                   		    $.messager.alert('操作提示','此机构停用失败','error');
                   	    }
                    }
            })
		})
	}
}


    /*删除 
    function remove(){
    	var row = $("#dg1").datagrid('getSelected'); //获取选中行对象
		if(row != null){ //如果选中
			$.messager.alert('操作提示','是否要将此机构删除？','warning',function(r){
				$.ajax({
                    url:'deleteSys_Org',
                    type:'POST', //GET
                    async:true,    //或false,是否异步
                    contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                    data:{"id":row.id},
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(data){
                   	    if(data.result == '1'){  //如果返回1，删除成功
                   		    $.messager.alert('','删除机构成功','info',function(r){
                   			   window.location.href="sys_org";
                   		    });
                   	    }else{  //否则删除失败
                   		    $.messager.alert('操作提示','此机构删除失败','error');
                   	    }
                    }
                })
			});
       }else{
       	   $.messager.alert('操作提示','请选择一条数据删除','info');
       }
} */
   
    
    
   function exportExcel(){
	   $.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
			if(r){
				window.location.href="exportExcelSys_Org";
			}
		});
}

/************************************************************************************************************************************/
/************************************************************************************************************************************/
/************************************************************************************************************************************/


     function resetForAdd(){
    	 $("#orgId_add").val('');
         $("#orgName_add").val('');
         $("#orgSortName_add").val('');
         $("#orgParentId_add").combobox('setValue','');
         $("#orgRegion_add").val('');
         $("#option_add").val('');
         $("#tel_add").val('');
}

    function resetForEdit(){
    	$("#orgId_edit").val('');
        $("#orgName_edit").val('');
        $("#orgSortName_edit").val('');
        $("#orgParentId_edit").combobox('setValue','');
        $("#orgRegion_edit").val('');
        $("#option_edit").val('');
        $("#tel_edit").val('');
    }
	
	
    //点击新增dialog上的保存按钮
    function saveForAddDialog(){
    	$("#formForAddOrg").form('submit',{
			 url:'addSys_Org',
			 onSubmit:function(){
				 return $("#formForAddOrg").form('validate');
			 },
			 success:function(res){
				  if(res == '1'){
					  $.messager.alert('','新增成功！','info',function(r){
						  window.location.href="sys_org";
					  });
				  }else if(res == '0'){
					  $.messager.alert('操作提示','新增机构失败了','error');
				  }else{
					  $.messager.alert('操作提示','机构编号或机构名称已存在','error');
				  }
			 }
		});
}

    //点击修改dialog上的保存按钮
    function saveForEditDialog(){
    	if(orgIdBackUp != $("#orgId_edit").val()){     //如果修改前的机构编码和修改后的机构编码不一致，则表示用户修改了机构编码
    		document.getElementById("isEditOrgId").value = '1';
    	}else{
    		document.getElementById("isEditOrgId").value = '0';
    	}
    	
    	if(orgNameBackUp != $("#orgName_edit").val()){     //如果修改前的机构名称和修改后的机构名称不一致，则表示用户修改了机构名称
    		document.getElementById("isEditOrgName").value = '1';
    	}else{
    		document.getElementById("isEditOrgName").value = '0';
    	}
    	
		
		$("#formForEditOrg").form('submit',{
			 url:'editSys_Org',
			 onSubmit:function(){
				 return $("#formForEditOrg").form('validate');
			 },
			 success:function(res){
				  if(res == '1'){
					  $.messager.alert('','修改成功！','info',function(r){
						  window.location.href="sys_org";
					  });
				  }else if(res == '0'){
					  $.messager.alert('操作提示','修改机构失败了','error');
				  }else{
					  $.messager.alert('操作提示','机构编号或机构名称已存在','error');
				  }
			 }
		});
}
    
    
    function openExcelTemplet(){
 	   $('#selectExcelTemplet').dialog('open');
 	   $('#selectExcelTemplet').dialog({modal:true});
 }
    
    
    function exportExcel(excelType){
 	   var departmentName = $("#departmentName").val();
 	   $.messager.confirm('操作提示','确认将数据导出到Excel表格吗？',function(r){
 		if(r){
 			window.location.href="exportSys_Department?excelType="+excelType+"&departmentName="+encode(departmentName);
 			$('#selectExcelTemplet').dialog('close');
 		}
 	   });
    }
</script>
</body>
</html >
