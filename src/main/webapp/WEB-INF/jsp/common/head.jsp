<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script type="text/javascript" src="js/base-loading.js"></script>
<link rel="stylesheet" type="text/css" href="jquery/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="jquery/themes/icon.css">
<script type="text/javascript" src="jquery/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="jquery/jquery.easyui.min.js"></script>
<script type="text/javascript" src="jquery/jquery_add_tab.js"></script>
<script type="text/javascript" src="jquery/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="js/ajaxfileupload.js"></script>
<script type="text/javascript" src="js/zhengzeyanzheng.js"></script>
<script type="text/javascript" src="js/valid.js"></script>
<script type="text/javascript" src="js/excelUtils.js"></script>
<script type="text/javascript" src="js/easyUiHelper.js"></script>
<script type="text/javascript" src="js/reportButtonAction.js"></script>
<body>
<script type="text/javascript">
$(function(){
	$(".combo").click(function (e) {
        if (e.target.className == 'combo-text validatebox-text' || e.target.className == 'combo-text validatebox-text validatebox-invalid'){
			if ($(this).prev().combobox("panel").is(":visible")) {
				$(this).prev().combobox("hidePanel");
			} else {
				$(this).prev().combobox("showPanel");
			}
		}
	});
	//combobox可编辑，自定义模糊查询  
	$.fn.combobox.defaults.editable = true;  
	$.fn.combobox.defaults.filter = function(q, row){  
	    var opts = $(this).combobox('options');  
	    return row[opts.textField].indexOf(q.toUpperCase()) >= 0;  
	};
	
	//combotree可编辑，自定义模糊查询  
	$.fn.combotree.defaults.editable = true;  
	$.extend($.fn.combotree.defaults.keyHandler,{  
	    up:function(){  
	        console.log('up');  
	    },  
	    down:function(){  
	        console.log('down');  
	    },  
	    enter:function(){  
	        console.log('enter');  
	    },  
	    query:function(q){  
	        var t = $(this).combotree('tree');  
	        var nodes = t.tree('getChildren');
	        for(var i=0; i<nodes.length; i++){  
	            var node = nodes[i];  
	            if (node.text.indexOf(q) >= 0){
	            	console.log(node.text);
	                $(node.target).show();  
	            } else {  
	                $(node.target).hide();  
	            }  
	        }  
	        
	        var opts = $(this).combotree('options');  
	        if (!opts.hasSetEvents){  
	            opts.hasSetEvents = true;  
	            var onShowPanel = opts.onShowPanel;  
	            opts.onShowPanel = function(){  
	                var nodes = t.tree('getChildren');  
	                for(var i=0; i<nodes.length; i++){  
	                    $(nodes[i].target).show();  
	                }  
	                onShowPanel.call(this);  
	            };  
	            $(this).combo('options').onShowPanel = opts.onShowPanel;  
	        }  
	        
	    }  
	});
});
function forbidBackSpace(e) {
    var ev = e || window.event; //获取event对象 
    var obj = ev.target || ev.srcElement; //获取事件源 
    var t = obj.type || obj.getAttribute('type'); //获取事件源类型 
    //获取作为判断条件的事件类型 
    var vReadOnly = obj.readOnly;
    var vDisabled = obj.disabled;
    //处理undefined值情况 
    vReadOnly = (vReadOnly == undefined) ? false : vReadOnly;
    vDisabled = (vDisabled == undefined) ? true : vDisabled;
    //当敲Backspace键时，事件源类型为密码或单行、多行文本的， 
    //并且readOnly属性为true或disabled属性为true的，则退格键失效 
    var flag1 = ev.keyCode == 8 && (t == "password" || t == "text" || t == "textarea") && (vReadOnly == true || vDisabled == true);
    //当敲Backspace键时，事件源类型非密码或单行、多行文本的，则退格键失效 
    var flag2 = ev.keyCode == 8 && t != "password" && t != "text" && t != "textarea";
    //判断 
    if (flag2 || flag1) return false;
}
//禁止后退键 作用于Firefox、Opera
document.onkeypress = forbidBackSpace;
//禁止后退键  作用于IE、Chrome
document.onkeydown = forbidBackSpace;
</script>
</body>
</html>