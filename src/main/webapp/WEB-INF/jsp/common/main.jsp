<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.geping.etl.common.entity.Sys_UserAndOrgDepartment"%>
<%
Sys_UserAndOrgDepartment sysUser = (Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User");
String needEditPwd = (String)request.getAttribute("needEditPwd");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="head.jsp"%>
<link href="css/mian.css" rel="stylesheet" type="text/css" />
<meta http-equiv="X-UA-Compatible" content="IE=11">
<title>金融监管统一报送平台</title>
<style type="text/css">
       ul li {
            margin-top: 5px;
            list-style: none;
        }

        .easyui-accordion ul {
            text-align: center;
            padding: 0px;
            margin: 0px;
            font-size: 16px;
            color: white;
            overflow: hidden;
        }

        .easyui-accordion ul li span {
                padding-top: 10px;
                display: block;
                overflow: hidden;
                cursor: pointer;
                color: Black;
                width: 230px;
                height: 32px;
                border: 1px solid #d5d5d5;
        }

        .easyui-accordion .div_1 ul li {
            list-style: none;
            height: 44px;
            width: 252px;
            overflow: hidden;
        }

        .easyui-accordion .accordion-header {
            height: 44px;
            width: 251px;
            overflow: hidden;
        }

        .easyui-accordion .panel-header {
            padding: 0px;
            text-align: center;
        }

        .easyui-accordion .panel-header .panel-title {
                font-size: 16px;
                padding-top: 13px;
                color: #575765;
                text-align: left;
                margin-left: 20px;
         }

        .cs-navi-tab {
            font-size: 14px;
        }

            .cs-navi-tab span {
                margin-left: 10px;
            }

        a {
            text-decoration: none;
        }

        a.font_size {
                color: #fff;
        }

        .layout-panel-west {
            text-align: center;
        }

        .layout-panel-west .panel-header {
                background: none;
        }

        .panel-header .panel-title {
            font-size: 18px;
            padding-left: 0px;
            font-weight: normal;
            height: 28px;
            padding-top: 10px;
        }

        #divright ul {
            list-style: none;
            margin-right: 20px;
            padding: 0px;
        }

        .cs-west {
            width: 252px;
            padding: 0px;
        }
</style>
<script type="text/javascript">

   
</script>
</head>
<body class="easyui-layout">
	<!-- 北 -->
	<div data-options="region:'north',border:false" style="width:100%;height: 140px; overflow: hidden; background: url(images/Top.jpg);background-size: 1930px,140px; background-repeat: no-repeat;">
        <div class="cs-north-bg" style="width: 100%; height: 100%;">
            <div style="width: 900px; height: 70px; float: right; margin-top: 5px;" id="divright">
                <ul>
                    <li style="float: right; padding-right: 40px; margin: 8px;">
                        <a href="javascript:void(0);" onclick="userOut()" style="color: blue; font-size: 14px; font-weight: 700;">注销</a>
                    </li>
                    <li style="float: right; padding-right: 0px; margin: 8px;">
                        <span style="color: #fff; font-size: 18px;">|</span>
                    </li>
                    <li style="float: right; padding-right: 0px; margin: 8px;">
                        <a href="javascript:void(0);" onclick="editPwd()" style="color: blue; font-size: 14px; font-weight: 700;">修改密码</a>
                    </li> 
                    <li style="float: right; padding-right: 0px; margin: 8px;">
                        <span style="color: #fff; font-size: 18px;">|</span>
                    </li>
                    <li style="float: right; padding-right: 0px; margin: 8px;">
                        <span style="color: blue; font-size: 14px;font-weight: 700;" id="nowDateTimeSpan"></span>
                    </li>
                    <li style="float: right; padding-right: 0px; margin: 8px;">
                        <span style="color: #fff; font-size: 18px;">|</span>
                    </li>
                    <li style="float: right; padding-right: 0px; margin: 8px;">
                        <%-- <a href="javascript:void(0);" style="color: blue; font-size: 14px; font-weight: 700;">金融机构名称:<%=sysUser.getOrgname()%></a> --%>
                        <span style="color:blue;font-size: 14px;font-weight: 700;">金融机构名称:<%=sysUser.getOrgname()%></span>
                    </li>
                    <li style="float: right; padding-right: 0px; margin: 8px;">
                        <span style="color: #fff; font-size: 18px;">|</span>
                    </li>
                    <li style="float: right; padding-right: 0px; margin: 8px;">
                        <%-- <a href="javascript:void(0);" style="color: blue; font-size: 17px; font-weight: 700;">当前用户:<%=sysUser.getLoginid()%></a> --%>
                        <span style="color:blue;font-size: 14px;font-weight: 700;">当前用户:<%=sysUser.getLoginid()%></span>
                    </li>
                </ul>
            </div>
            <div style="width: 100%; height: 55px; margin-left: 30px; float: left;clearfix;" data-options="border:false">
                <ul>
                   <c:forEach items="${subjectList}" var="sl">
                   <li style="margin-right: 30px; margin-left: 10px; margin-top: 4px; float: left;">
                        <%-- <a href="javascript:void(0)" style="font-size: 20px;color: yellow;" id="subject${sl.id}" onclick="showFrame('${sl.id}','${sl.subjectName}')">${sl.subjectName}</a> --%>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'${sl.icon}'" id="subject${sl.id}" onclick="showFrame('${sl.id}','${sl.subjectName}')">${sl.subjectName}</a>
                    </li>
			       </c:forEach>
                </ul>
            </div>
        </div>
    </div>
		
		<!-- 中 -->
		<div id="welframe" data-options="region:'center',border:true" style="overflow: hidden;">
			 <%-- <jsp:include page="/home.jsp"></jsp:include> --%>
			 <iframe  id="mainWelcome" name="mainWelcome"  frameborder="0" height="100%" width="100%" style="border-color: red;overflow: hidden;"></iframe>
		</div>
		
		
		<!-- 南 -->
		<div data-options="region:'south',border:false" style="height: 20px; background-color: #f3f3f3;text-align: right;">
			&copy;剑阁信息&nbsp;&nbsp;版权所有&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</div>
		
		
<!-- 修改密码-->
<div style="visibility: hidden;">
   <div id="editPasswordDialog" class="easyui-dialog" title="修改密码" data-options="iconCls:'icon-save',toolbar:'#tbForEditPasswordDialog'" style="width:600px;height:220px">
          
          <!--修改对话框的工具栏  -->
          <div id="tbForEditPasswordDialog" style="padding-left: 30px;padding-top: 10px">
	           <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveForEditPasswordDialog()">保存</a>&nbsp;&nbsp;
               <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="resetForEditPassword()">重置</a>
          </div>
          
          <!-- 信息录入 -->
         <form id="formForEditPasswordUser" method="get">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
	              <%--<input type="hidden" name="editPasswordId" id="editPasswordId"/>--%>
	              <%--<input type="hidden" name="isFirstLogin" id="isFirstLogin"/>--%>
				  <tr>
				      <td align="right">新密码:</td>
				      <td><input type="password" name="password_edit" id="password_edit" onblur="checkpwd()" style="height: 21px; width: 173px"/>(不少于8位,至少包含大小写英文字母和阿拉伯数字)</td>
				  </tr>
			   
				  <tr>
				      <td align="right">重复新密码:</td>
				      <td><input type="password" name="passwordRepeat_edit" id="passwordRepeat_edit" onblur="repeatCheckpwd()" style="height: 21px; width: 173px"/></td>
				  </tr>
              </table>
           </div>
         </form>
	</div>
</div>

<!-- 首次登录或者已有30天没有修改密码 -->
<div style="visibility: hidden;">
   <div id="mustEditPwdDialog" class="easyui-dialog" title="" data-options="iconCls:'icon-save',toolbar:'#tbForEditPasswordDialog1'" style="width:600px;height:220px">
          
          <div style="text-align: center;">
               <span style="font-size: large;color:red;">首次登录系统用户或距上次修改密码已有30天</span>
          </div>
          
          <!--修改对话框的工具栏  -->
          <div id="tbForEditPasswordDialog1" style="padding-left: 30px;padding-top: 10px">
	           <a class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveForEditPasswordDialog1()">保存</a>&nbsp;&nbsp;
               <a class="easyui-linkbutton" data-options="iconCls:'icon-undo'" onclick="resetForEditPassword()">重置</a>
          </div>
          
          <!-- 信息录入 -->
         <form id="formForEditPasswordUser1" method="get">
           <div style="padding-top: 20px">
             <table style="padding-left: 30px">
	              <input type="hidden" name="editPasswordId" id="editPasswordId1"/>
	              <input type="hidden" name="isFirstLogin" id="isFirstLogin1"/>
				  <tr>
				      <td align="right">新密码:</td>
				      <td><input type="password" name="password_edit" id="password_edit1" onblur="checkpwd1()" style="height: 21px; width: 173px"/>(不少于8位,至少包含大小写英文字母和阿拉伯数字)</td>
				  </tr>
			   
				  <tr>
				      <td align="right">重复新密码:</td>
				      <td><input type="password" name="passwordRepeat_edit" id="passwordRepeat_edit1" onblur="repeatCheckpwd1()" style="height: 21px; width: 173px"/></td>
				  </tr>
              </table>
           </div>
         </form>
	</div>
</div>

		
	</body>
	
	<script type="text/javascript">
	   var isforedit = false;
	   var isforlogin = false;
	   $(function(){
		   $('#editPasswordDialog').dialog('close');
		   $('#mustEditPwdDialog').dialog('close');
		   
		    var needEditPwd = '<%=needEditPwd%>';
		    openPwd(needEditPwd);
	    	
	   	    $('#mainWelcome').attr('src','home');
	   	    
	   	    fnDate();
	    	
	    	//定时器每秒调用一次
	   	    setInterval(function(){
	   	      fnDate();
	   	    },1000);
	    })
	    
	   function fnDate(){
			var oDiv=document.getElementById("nowDateTimeSpan");
			var date=new Date();
			var year=date.getFullYear();//当前年份
			var month=date.getMonth();//当前月份
			var data=date.getDate();//天
			var hours=date.getHours();//小时
			var minute=date.getMinutes();//分
			var second=date.getSeconds();//秒
			var time=year+"-"+fnW((month+1))+"-"+fnW(data)+" "+fnW(hours)+":"+fnW(minute)+":"+fnW(second);
			oDiv.innerHTML=time;
	    }

	  //补位 当某个字段不是两位数时补0
	  function fnW(str){
			var num;
			str<10?num="0"+str:num=str;
			return num;
	   }
	    
	   function openPwd(needEditPwd){
		   if(needEditPwd == 'Y'){
			   $('#editPasswordId1').val('<%=sysUser.getId()%>');
	    	   $('#isFirstLogin1').val('<%=sysUser.getIsfirstlogin()%>');
			   $('#mustEditPwdDialog').dialog('open');
			   $('#mustEditPwdDialog').dialog({modal:true});
		   }
	   }
	   
	   function showFrame(subjectId,subjectName){
		   for(var i = 1;i<=20;i++){
			   if(i == parseInt(subjectId)){
				   $('#subject'+i).linkbutton('disable');
			   }else{
				   $('#subject'+i).linkbutton('enable');
			   }
		   }
		   
		   if(subjectId == '1'){
			   $('#mainWelcome').attr('src','home');
		   }else{
			   var sn = encodeURI(subjectName);
			   $('#mainWelcome').attr('src','index?userId='+<%=sysUser.getId()%>+'&subjectId='+subjectId+'&subjectName='+sn);
		   }
		  <%--  if(subjectId == '1'){
			   $('#mainWelcome').attr('src','home');
			   //window.open("http://192.168.1.125:8080/ODSPRO/login.do");
		   }else{
			   var sn = encodeURI(subjectName);
			   $('#mainWelcome').attr('src','index?userId='+<%=sysUser.getId()%>+'&subjectId='+subjectId+'&subjectName='+sn);
		   } --%>
	   }
	   
	   //用户点击退出按钮
	   function userOut(){
			window.location.href = "userOut";
	   }
	   
	   
       function editPwd(){
    	   $('#editPasswordId').val('<%=sysUser.getId()%>');
    	   $('#isFirstLogin').val('<%=sysUser.getIsfirstlogin()%>');
		   $('#editPasswordDialog').dialog('open');
		   $('#editPasswordDialog').dialog({modal:true});
	   }
       
       function checkpwd(){
    	   if($.trim($('#password_edit').val()).length == 0){
    		   $.messager.alert('操作提示','请输入密码','error');
    	   }else{
    		   var reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[^]{8,}$/;
    		   if(!$.trim($('#password_edit').val()).match(reg)){
    			   $.messager.alert('操作提示','密码不少于8位,至少包含大小写英文字母和阿拉伯数字','error');
    			   $('#password_edit').val("");
    		   }
    	   }
       }
       
       function repeatCheckpwd(){
    	   if($.trim($('#password_edit').val()) != $.trim($('#passwordRepeat_edit').val())){
    		   $.messager.alert('操作提示','两次密码不一致,请重新输入密码','error');
    	   }else{
    		   isforedit = true;
    	   }
       }
       
     //点击修改用户密码dialog上的保存按钮
       function saveForEditPasswordDialog(){
    	 if(isforedit){
    		 $("#formForEditPasswordUser").form('submit',{
           		 url:'editPasswordSys_User',
           		 onSubmit:function(){
           			if($.trim($('#password_edit').val()) == $.trim($('#passwordRepeat_edit').val()) && $.trim($('#password_edit').val()).length > 0 && $.trim($('#passwordRepeat_edit').val()).length > 0){
             		   return true;
             	   }else{
             		  $.messager.alert('操作提示','两次密码不一致或密码为空','error');
             		  return false;
             	   }
           		 },
           		 success:function(res){
           			  if(res == '1'){
           				  $.messager.alert('','修改密码成功！','info',function(r){
           					  isforedit = false;
           					  window.location.href="userOut";
           				  });
           			  }else if(res = '2'){
           				  $.messager.alert('操作提示','该密码已经使用过','error');
           			  }else{
           				  $.messager.alert('操作提示','修改密码失败了','error');
           			  }
           		 }
           	});
    	 }else{
    		 $.messager.alert('操作提示','密码不正确,请重新输入','error');
    	 }
       }
       
       function saveForEditPasswordDialog1(){
    	   if(isforlogin){
    		   $("#formForEditPasswordUser1").form('submit',{
            		 url:'editPasswordSys_User',
            		 onSubmit:function(){
            			if($.trim($('#password_edit1').val()) == $.trim($('#passwordRepeat_edit1').val()) && $.trim($('#password_edit1').val()).length > 0 && $.trim($('#passwordRepeat_edit1').val()).length > 0){
              		   return true;
              	   }else{
              		  $.messager.alert('操作提示','两次密码不一致或密码为空','error');
              		  return false;
              	   }
            		 },
            		 success:function(res){
            			  if(res == '1'){
            				  $.messager.alert('','修改密码成功！','info',function(r){
            					  isforlogin = false;
            					  window.location.href="userOut";
            				  });
            			  }else if(res = '2'){
            				  $.messager.alert('操作提示','该密码已经使用过','error');
            			  }else{
            				  $.messager.alert('操作提示','修改密码失败了','error');
            			  }
            		 }
            	});
    	   }else{
    		   $.messager.alert('操作提示','密码不正确,请重新输入','error');
    	   }
          }
       
       function checkpwd1(){
    	   if($.trim($('#password_edit1').val()).length == 0){
    		   $.messager.alert('操作提示','请输入密码','error');
    	   }else{
    		   var reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[^]{8,}$/;
    		   if(!$.trim($('#password_edit1').val()).match(reg)){
    			   $.messager.alert('操作提示','密码不少于8位,至少包含大小写英文字母和阿拉伯数字','error');
    			   $('#password_edit1').val("");
    		   }
    	   }
       }
       
       function repeatCheckpwd1(){
    	   if($.trim($('#password_edit1').val()) != $.trim($('#passwordRepeat_edit1').val())){
    		   $.messager.alert('操作提示','两次密码不一致,请重新输入密码','error');
    	   }else{
    		   isforlogin = true;
    	   }
       }
	   
       function resetForEditPassword(){
    	   $("#formForEditPasswordUser").form('reset');
    	   $("#formForEditPasswordUser1").form('reset');
       }
	</script>
	
</html>