<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.geping.etl.common.entity.Sys_UserAndOrgDepartment"%>
<%Sys_UserAndOrgDepartment sysUser = (Sys_UserAndOrgDepartment)request.getSession().getAttribute("sys_User");
  String subjectId = (String)request.getAttribute("subjectId");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="head.jsp"%>
<title></title>
</head>
<body class="easyui-layout">
    <!-- 西 -->
		<div data-options="region:'west',split:true,title:'${subjectName}'"style="width: 250px;">
			<div class="easyui-accordion" data-options="fit:true, border:false">
					<c:out value="${menuHtml}" escapeXml="false"></c:out>
			</div>
		</div>
		
		<!-- <div data-options="region:'center'">
		    <iframe  id="mainFramePage" name="mainFramePage" frameborder="0" height="99%" width="100%" scrolling="no" style="border-color: red;overflow: hidden;"></iframe>
	    </div> -->
	    <div data-options="region:'center'">
			<div class="easyui-tabs" id="tabs" style="overflow: hidden;background-color: #f3f3f3;" data-options="fit:true,border:false,plain:true">
				<%
				  if(subjectId.equals("4")){
				%>
					<div title="待办事务" style="overflow: hidden;">
						<jsp:include page="task.jsp"></jsp:include>
					</div>
				<%
				  }
				%>
			</div>
		</div>
		
</body>
</html>