<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>  
    <title>查看日志</title>
    <%@include file="head.jsp"%>
</head>
<body>
<table title="查看日志" id="dg1" style="height: 460px"></table>

<div style="visibility: hidden;">
	<div id="tb" style="padding:5px;height:auto;">
                         日志生成时间：
		<input class="easyui-datebox" style="width:173px;height:21px" id="startDate" editable=false/>&nbsp;&nbsp;
		—&nbsp;&nbsp;
		<input class="easyui-datebox" style="width:173px;height:21px" id="endDate" editable=false/>&nbsp;&nbsp;&nbsp;&nbsp;				
                         银行机构代码：
        <select class="easyui-combobox" id="logOrgCode" editable=false style="height: 21px;width: 173px">
			   <option value="all">全部</option>
			   <c:forEach items="${list}" var="li">
			        <option value="${li.orgId}">${li.orgId}-${li.orgName}</option>
			   </c:forEach>
		</select>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="easyui-linkbutton" iconCls="icon-search" onclick="selectRecordByDate()">查询</a>
    </div>
</div>
<script type="text/javascript">
$(function(){
	
	$("#startDate").datebox("setValue",'');
	$("#endDate").datebox("setValue",'');
	
	$("#dg1").datagrid({
		method:'post',
		url:"showLogs",
		loadMsg:'数据加载中,请稍后...',
		//checkOnSelect:true,
		singleSelect:true,
		fitColumns:true,
		autoRowHeight:false,
		pagination:true,
		rownumbers:true,
		toolbar:'#tb',
		pageSize:25,
		pageList:[15,25,35],
		columns:[[
			{field:'id',title:'ID',width:80,align:'center',hidden:true},
			{field:'loginId',title:'操作人',width:80,align:'center'},
			{field:'logContent',title:'日志明细',width:200,align:'center'},
			{field:'logType',title:'日志类型',width:100,align:'center'},
			{field:'logOrgCode',title:'银行机构代码',width:80,align:'center'},
			{field:'logDate',title:'日志生成时间',width:80,align:'center'}
	    ]]
	});
});


function selectRecordByDate(){
	var startDate = $("#startDate").datebox("getValue");
	var endDate = $("#endDate").datebox("getValue");
	var logOrgCode = $("#logOrgCode").combobox("getValue");
	
	$("#dg1").datagrid("load",{startDate:startDate,endDate:endDate,logOrgCode:logOrgCode});
}
</script>
</body>
</html>