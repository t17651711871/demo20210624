<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <title>金融监管统一报送平台</title>
    <script src="jquery/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        if(top.location!=self.location){
            top.location = "loginUi";
        }
    </script>
    <style type="text/css">
        body {
            width: 100%;
            height: 100%;
            /*color: #fff;*/
            font-family: "微软雅黑";
            font-size: 14px;
            background-image: url('images/Login_S.jpg');
            background-size: 100%,100%;
            position: absolute;
            background-repeat: no-repeat;
            -moz-background-size: 100% 100%;
            overflow-x: hidden;
            overflow-y: hidden;
        }

        .form-group {
            position: relative;
        }

        .login_btn {
            display: block;
            background: #3872f6;
            color: #fff;
            font-size: 15px;
            width: 80%;
            line-height: 50px;
            border-radius: 3px;
            border: none;
            /*margin-top: 40px;*/
        }

        .login_input {
            width: 73%;
            border: 1px solid #3872f6;
            border-radius: 3px;
            line-height: 40px;
            padding: 2px 5px 2px 30px;
            background: none;
            color: #000;
        }

        input:-ms-input-placeholder {
            color: #000;
        }

        input:-ms-webkit-autofill {
            background-color: none;
        }
    </style>
    <%--SM3加密 START--%>
    <script type="text/javascript" src="js/sm3/core.js"></script>
    <script type="text/javascript" src="js/sm3/cipher-core.js"></script>
    <script type="text/javascript" src="js/sm3/jsbn.js"></script>
    <script type="text/javascript" src="js/sm3/jsbn2.js"></script>
    <script type="text/javascript" src="js/sm3/sm3.js"></script>
    <script type="text/javascript" src="js/sm3/SM3Utils.js"></script>
    <%--SM3加密 END--%>
</head>

<body>
<!-- <div id="formbackground" style="position:absolute;z-index:-1; left:0; top:0px;height:100%;width:100%;background-image:url('images/index2.jpg');background-size: 100% 100%;"> -->
<div style="width: 28%; height: 30%; margin-top: 20%; margin-left: 3%;">
    <div style="margin: 0px auto; padding-top: 25px; padding-left: 20px;">
        <form  id="form1" method="post" action="login">
            <div class="form-group mg-t20">
                <input type="text" style="height: 50px;" class="login_input" id="loginId" name="loginId" placeholder="Please enter a user name" />
            </div>
            <br>
            <div class="form-group mg-t20">
                <input type="password" style="height: 50px;" class="login_input" id="password" name="password" placeholder="Please enter password" />
            </div>
            <br>
<%--            <div>--%>
<%--                <div class="form-group mg-t20" style="float: left">--%>
<%--                    <input type="text" style="height: 50px;width: 85%" class="login_input" id="code" name="code" placeholder="Please enter code" />--%>
<%--                </div>--%>
<%--                <div style="float: left;margin-left: 2%">--%>
<%--                    <img style="width: 66%;" src="${pageContext.request.contextPath}/getVerificationCode" alt="验证码看不清,换一张" id="verificationCode" onclick="changeImg()">--%>
<%--                    <br>--%>
<%--                    <a style="font-size: 13px;" href="javascript:void(0)" onclick="changeImg()">验证码看不清,换一张</a>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <div style="clear: both"></div>--%>
<%--            <p style="position: relative; ">--%>
                <span style="color: red;font-size: large;">${message}</span>
<%--            </p>--%>

            <div class="checkbox mg-t20">
                <input class="login_btn" type="button" onclick="login()" id="loginc" value="Login"/>
            </div>
        </form>
    </div>
</div>
<div style="width: 480px; height: 20px; position: absolute; margin-top: 11%; text-align: center; font-size: 10px; color: #808080;">
    <p>上海剑阁信息技术有限公司 © 版权所有 www.swordgate.com.cn</p>
</div>


<script type="text/javascript">

    //更换验证码
    function changeImg() {
        document.getElementById("verificationCode").src="${pageContext.request.contextPath}/getVerificationCode?"+Math.random();
    }

    //点击重置按钮
    function repeat(){
        $('#loginId').val('');
        $('#password').val('');
    }

    //点击登录按钮
    function login(){
        $('#password').val(SM3Encrypt($('#password').val()));
        $("#form1").submit();
        /* var loginId = $('#loginId').val();
        var password = $('#password').val();
        $.ajax({
            url:'login',
            async:true,//是否异步
            type : 'GET',
            contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
            dataType : 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            data:{"loginId":loginId,"password":password},
            success : function(data) {
                 if (data == '1') {
                     $.messager.alert('','重置密码成功','info',function(r) {
                                         window.location.href = "sys_user";
                     });
                 } else {
                     $.messager.alert('操作提示','哎呦，重置密码失败','error');
                 }
             }

        }) */
    }

    $(function(){
        //登录页绑定enter键登录
        $(document).keydown(function(event) {
            if (($("#loginId").val() != "" && $("#password").val() != "")
                && event.keyCode === 13) {
                $("#loginc").trigger("click");
                return false;
            }
        });
    });
</script>

</body>
</html>