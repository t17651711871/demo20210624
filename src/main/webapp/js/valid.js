$.extend($.fn.validatebox.defaults.rules, {
	teshu:{
	    validator: function (value) {
	    	var patrn = /[\`\~\!\@\#\$\%\^\&\*\(\)\_\\\-\+\=\<\>\?\:\"\{\}\|\,\.\/\;\'\\[\]\·\~\！\@\#\￥\%\……\&\*\（\）\——\\\-\+\=\｛\｝|\《\》\？\：\“\”\【\】\、\；\‘\'\，\。\、 ]/im;
	    	return patrn.test(value)==false;    
		},
		message: '不能包含特殊符号'
	},
	zip:{
	    validator: function (value) {
	    	 return /^[0-9]\d{5}$/.test(value);    
		},
		message: '邮政编码为6位数字'
	},
    chs: {
      validator: function (value, param) {
        return /^[\u0391-\uFFE5]+$/.test(value);
      },
      message: '请输入汉字'
    },
    subjectA: {
        validator: function (value) {
          return /^\d{4}$/.test(value);
        },
        message: '一级科目编号格式不正确'
    },
    subjectB: {
          validator: function (value) {
            return /(^\d{4})-(\d{2}$)/.test(value);
          },
          message: '二级科目编号格式不正确'
    },
    subjectC: {
        validator: function (value) {
          return /^[0-8]{1}\d{7}$|^9\d{6}$/.test(value);
        },
        message: '三级科目编号格式不正确'
    },
    english : {// 验证英语
         validator : function(value) {
             return /^[A-Za-z]+$/i.test(value);
         },
         message : '请输入英文'
    },
    ip : {// 验证IP地址
        validator : function(value) {
            return /\d+\.\d+\.\d+\.\d+/.test(value);
        },
        message : 'IP地址格式不正确'
    },
    qq: {
      validator: function (value, param) {
        return /^[1-9]\d{4,10}$/.test(value);
      },
      message: 'QQ号码不正确'
    },
    mobile: {
      validator: function (value, param) {
    	
        return /(^1[0-9]{10}$)/.test(value);
      },
      message: '手机号码格式不正确'
    },
    tel:{
      validator:function(value,param){
        return /^(\d{3}-|\d{4}-)?(\d{8}|\d{7})?(-\d{1,6})?$/.test(value);
      },
      message:'电话号码格式不正确'
    },
    mobileAndTel: {
      validator: function (value, param) {
    	  //验证格式：
    	  //1、11位手机号
    	  //2、以0开头的3位数字-四位数字-四位数字
    	  //3、以0开头的4位数字-七位数字
    	  //4、七位数字
        return /(^1[0-9]{10}$)|(^([0\+]\d{2})\-\d{4}\-\d{4}$)|(^([0\+]\d{3})\-\d{7}$)|(^\d{7}$)/.test(value);
      },
      message: '电话号码格式不正确'
    },
    number: {
      validator: function (value, param) {
        return /^[0-9]+.?[0-9]*$/.test(value);
      },
      message: '请输入数字'
    },
    money:{
      validator: function (value, param) {
           return (/^(([1-9]\d*)|\d)(\.\d{1,2})?$/).test(value);
       },
       message:'请输入正确的金额'

    },
    mone:{
      validator: function (value, param) {
           return (/^(([1-9]\d*)|\d)(\.\d{1,2})?$/).test(value);
       },
       message:'请输入整数或小数'

    },
    integer:{
      validator:function(value,param){
        return /^[+]?[1-9]\d*$/.test(value);
      },
      message: '请输入最小为1的整数'
    },
    integ:{
      validator:function(value,param){
        return /^[+]?[0-9]\d*$/.test(value);
      },
      message: '请输入整数'
    },
    range:{
      validator:function(value,param){
        if(/^[1-9]\d*$/.test(value)){
          return value >= param[0] && value <= param[1]
        }else{
          return false;
        }
      },
      message:'输入的数字在{0}到{1}之间'
    },
    minLength:{
      validator:function(value,param){
        return value.length >=param[0]
      },
      message:'至少输入{0}个字'
    },
    maxLength:{
      validator:function(value,param){
        return value.length<=param[0]
      },
      message:'最多{0}个字'
    },
    idCode:{
      validator:function(value,param){
        return /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(value);
      },
      message: '请输入正确的身份证号'
    },
    loginName: {
      validator: function (value, param) {
        return /^[\u0391-\uFFE5\w]+$/.test(value);
      },
      message: '登录名称只允许汉字、英文字母、数字及下划线。'
    },
    equalTo: {
      validator: function (value, param) {
        return value == $(param[0]).val();
      },
      message: '两次输入的字符不一至'
    },
    englishOrNum : {// 只能输入英文和数字
          validator : function(value) {
              return /^[a-zA-Z0-9_ ]{1,}$/.test(value);
          },
          message : '请输入英文、数字、下划线或者空格'
      },
     xiaoshu:{ 
        validator : function(value){ 
        	return /^(([1-9]+)|([0-9]+\.[0-9]{1,2}))$/.test(value);
        }, 
        message : '最多保留两位小数！'    
        },
    ddPrice:{
    validator:function(value,param){
      if(/^[1-9]\d*$/.test(value)){
        return value >= param[0] && value <= param[1];
      }else{
        return false;
      }
    },
    message:'请输入1到100之间正整数'
  },
  jretailUpperLimit:{
    validator:function(value,param){
      if(/^[0-9]+([.]{1}[0-9]{1,2})?$/.test(value)){
        return parseFloat(value) > parseFloat(param[0]) && parseFloat(value) <= parseFloat(param[1]);
      }else{
        return false;
      }
    },
    message:'请输入0到100之间的最多俩位小数的数字'
  },
  rateCheck:{
    validator:function(value,param){
      if(/^[0-9]+([.]{1}[0-9]{1,2})?$/.test(value)){
        return parseFloat(value) > parseFloat(param[0]) && parseFloat(value) <= parseFloat(param[1]);
      }else{
        return false;
      }
    },
    message:'请输入0到1000之间的最多俩位小数的数字'
  },/*下面是用于合同开始日期验证，请勿随意更改*/
  ContractDate: {
      validator: function (value, param) {
    	/*开始日期*/
    	var beginDate=value;
    	/*结束日期*/
    	var endDate=$(param[0]).datebox('getValue');
    	
    	var d1 = new Date(beginDate.replace(/\-/g, "\/"));  
    	var d2 = new Date(endDate.replace(/\-/g, "\/")); 
    	if(beginDate!=""&&endDate!=""&&d1 >=d2)  
    	{  
    	  	return false;  
    	}else{
    		return true;
    	}
      },
      message: '合同开始日期不能大于合同到期日期！'
    },
    ContractEndDate: {
        validator: function (value, param) {
        	
          	/*开始日期*/
        	var beginDate=$(param[0]).datebox('getValue');
        	/*结束日期*/
        	var endDate=value;
        	
        	var d1 = new Date(beginDate.replace(/\-/g, "\/")); 
        	var d2 = new Date(endDate.replace(/\-/g, "\/")); 
        	if(beginDate!=""&&endDate!=""&&d1 >=d2)  
        	{  
        	  	return false;  
        	}else{
        		return true;
        	}
          },
          message: '合同开始日期不能大于合同到期日期！'
        },/*用于币种信息的验证*/
        comboxValidate:{
    	  validator: function (value, param) {
    		  if(value=="---请选择币种---"){
    			  return false;
    		  }
    		  return true;
      },
    	message: '请选择币种信息，币种信息不能为空！'
     },
    choose: {
        validator:function(value,param){
            if (value == '请选择' || value == '--请选择--' || value == "" || value == undefined) {
                return false;
            }
            return true;
        },
        message: '该项为必选项,请选择数据项!'
    },
    career: {
        validator:function(value,param){
            var status;
            if (param != undefined) {
                status = $(param[0]).combobox('getValue');
            }
            if (status == '11' || status == '13' || status == '17' || status == '21' || status == '24' || status == '91') {
                if (value == '请选择' || value == '--请选择--' || value == "" || value == undefined) {
                    return false;
                }
            }
            return true;
        },
        message: '该项为必选项,请选择数据项!'
    },
    acctcode: {
        validator:function(value,param){
            var patt = /[a-zA-Z0-9]{15,60}/;
            return patt.test(value);
        },
        message: '请输入正确的标识码'
    },
    initcode: {
        validator:function(value,param){
            var patt = /[a-zA-Z0-9]{18}/;
            return patt.test(value);
        },
        message: '请输入正确的标识码初始债权人机构代码'
    },
    infsurccode: {
        validator:function(value,param){
            var patt = /[a-zA-Z0-9]{14,20}/;
            return patt.test(value);
        },
        message: '请输入正确的信息来源编码'
    },
    maxguarmcc: {
        validator:function(value,param){
            var patt = /[a-zA-Z0-9]{15,60}/;
            return patt.test(value);
        },
        message: '请输入正确的保证合同编号'
    },
    cardCheck: {
        validator:function(value,param){
            var status = $(param[0]).combobox('getValue');
            var type = $(param[1]).combobox('getValue');
            if (status == undefined || status == "" && type == undefined || type == "") {
                this.message = "请先选择身份类别与身份标识类型"
                return false;
            }
            var result = {
                code: true,
                msg: ""
            };
            if (type == "1") {
                switch (status) {
                    case "1" :
                    	result = hukouben(value);
                        break;
                    case "2" :
                    	result = huzhao(value);
                        break;
                    case "5" :
                    	result = HM(value);
                        break;
                    case "6" :
                    	result = TW(value);
                        break;
                    case "8" :
                    	result = WaiPerson(value);
                        break;
                    case "9" :
                    	result = police(value);
                        break;
                    case "A" :
                    	result = HKcard(value);
                        break;
                    case "B" :
                    	result = OMcard(value);
                        break;
                    case "C" :
                        result = idTypeRocValidator(value);
                        break;
                    case "X" :
                        result = otherCard(value);
                        break;
                    case "10" :
                        result = checkIdCard(value);
                        break;
                    case "20" :
                        result = OfficerCard(value);
                        break;
                }
            } else if (type = "2") {
                switch (status) {
                    case "10" :
                        result = checkLoanCardnorules(value);
                        break;
                    case "20" :
                        result = isCCode(value);
                        break;
                    case "30" :
                        result = isCorpNo(value);
                        break;
                }
            } else {
                return false;
            }

            if (result.code) {
                return true
            }
            this.message = result.msg;
            return false;
        },
        message: ''
    },
    /**
     * 个人证件类型号码校验
     */
    personCheck: {
        validator:function(value,param){
            var status = $(param[0]).combobox('getValue');
            if (status == undefined || status == "") {
                this.message = "请先身份标识类型"
                return false;
            }
            var result = {
                code: true,
                msg: ""
            };
            switch (status) {
                case "1" :
                	result = hukouben(value);
                    break;
                case "2" :
                	result = huzhao(value);
                    break;
                case "5" :
                	result = HM(value);
                    break;
                case "6" :
                	result = TW(value);
                    break;
                case "8" :
                	result = WaiPerson(value);
                    break;
                case "9" :
                	result = police(value);
                    break;
                case "A" :
                	result = HKcard(value);
                    break;
                case "B" :
                	result = OMcard(value);
                    break;
                case "C" :
                    result = idTypeRocValidator(value);
                    break;
                case "X" :
                    result = otherCard(value);
                    break;
                case "10" :
                    result = checkIdCard(value);
                    break;
                case "20" :
                    result = OfficerCard(value);
                    break;
            }
            if (result.code) {
                return true
            }
            this.message = result.msg;
            return false;
        },
        message: ''
    },
    personCheckAllowNull: {
        validator:function(value,param){
            var status = $(param[0]).combobox('getValue');
            var result = {
                code: true,
                msg: ""
            };
            switch (status) {
                case "1" :
                    result = hukouben(value);
                    break;
                case "2" :
                    result = huzhao(value);
                    break;
                case "5" :
                    result = HM(value);
                    break;
                case "6" :
                    result = TW(value);
                    break;
                case "8" :
                    result = WaiPerson(value);
                    break;
                case "9" :
                    result = police(value);
                    break;
                case "A" :
                    result = HKcard(value);
                    break;
                case "B" :
                    result = OMcard(value);
                    break;
                case "C" :
                    result = idTypeRocValidator(value);
                    break;
                case "X" :
                    result = otherCard(value);
                    break;
                case "10" :
                    result = checkIdCard(value);
                    break;
                case "20" :
                    result = OfficerCard(value);
                    break;
            }
            if (result.code) {
                return true
            }
            this.message = result.msg;
            return false;
        },
        message: ''
    }
    
    
});



/**
 * [isCorpNo 组织机构代码]
 * @param idCard 要校验的组织机构码
 * @returns
 */
function isCorpNo(idCard){
    var result = {
        code: true,
        msg: ""
    };
    var idCard = trim(idCard);
    if (idCard == ""){
        result.code = false;
        result.msg = "该项为必选项,请填写数据!";
        return result;
    }
    //第i位置上的加权因子
    var ws = [3, 7, 9, 10, 5, 8, 4, 2];
    var str = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var reg = /^([0-9A-Z]){8}-[0-9|X]$/;
    if (!reg.test(idCard)) {
        result.code = false;
        result.msg = "请输入正确的组织机构代码!";
        return result;
    }
    var sum = 0;
    for (var i = 0; i < 8; i++) {
        sum += str.indexOf(idCard.charAt(i)) * ws[i];
    }
    var c9 = 11 - (sum % 11);
    if(c9==10){
        c9='X';
    }else if(c9==11){
        c9='0';
    }

    if(c9 != idCard.charAt(9)){
        result.code = false;
        result.msg = "组织机构代码经校验码校验不正确!";
        return result;
    }
    return result;
}



/**
 * [isCCode 统一社会信用代码码]
 * @param {[type]}
 *            creditCode [description]
 * @return {Boolean} [description]
 */
function isCCode(code){
    var result = {
        code: true,
        msg: ""
    };
    var patrn = /^[0-9A-Z]+$/;
    // 18位校验及大写校验
    if ((code.length != 18) || (patrn.test(code) == false)){
        result.code = false;
        result.msg = "请输入正确的统一社会信用代码!";
        return result;
    }else{
        var Ancode;// 统一社会信用代码的每一个值
        var Ancodevalue;// 统一社会信用代码每一个值的权重
        var total = 0;
        var weightedfactors = [1, 3, 9, 27, 19, 26, 16, 17, 20, 29, 25, 13, 8, 24, 10, 30, 28];// 加权因子
        var str = '0123456789ABCDEFGHJKLMNPQRTUWXY';
        // 不用I、O、S、V、Z
        for (var i = 0; i < code.length - 1; i++)
        {
            Ancode = code.substring(i, i + 1);
            Ancodevalue = str.indexOf(Ancode);
            total = total + Ancodevalue * weightedfactors[i];
            // 权重与加权因子相乘之和
        }
        var logiccheckcode = 31 - total % 31;
        if (logiccheckcode == 31)
        {
            logiccheckcode = 0;
        }
        var Str = "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,J,K,L,M,N,P,Q,R,T,U,W,X,Y";
        var Array_Str = Str.split(',');
        logiccheckcode = Array_Str[logiccheckcode];


        var checkcode = code.substring(17, 18);
        if (logiccheckcode != checkcode){
            result.code = false;
            result.msg = "统一社会信用代码经校验码校验不正确!";
            return result;
        }
        return result;
    }
}



/**
 * [checkLoanCardnorules 中征码]
 * @param {[type]}
 * loanCardNo [description]
 * @return {[type]} [description]
 */
function checkLoanCardnorules(loanCardNo) {
    var result = {
        code: true,
        msg: ""
    };

    if (trim(loanCardNo).length != 16) {
        result.code = false;
        result.msg = "中征码长度不正确!";
        return result;
    }
    var checkCode;
    var weightValue = new Array();
    var checkValue = new Array();
    var totalValue = 0;
    var c = 0;
    // 生成的校验码
    var str1;
    checkCode = trim(loanCardNo).substring(0, 14);
    var patrn = /^([0-9a-zA-Z]){3}[0-9]*$/;
    if(patrn.test(checkCode)==false){
        result.code = false;
        result.msg = "请输入正确的中征码!";
        return result;
    }
    weightValue[0] = 1;
    weightValue[1] = 3;
    weightValue[2] = 5;
    weightValue[3] = 7;
    weightValue[4] = 11;
    weightValue[5] = 2;
    weightValue[6] = 13;
    weightValue[7] = 1;
    weightValue[8] = 1;
    weightValue[9] = 17;
    weightValue[10] = 19;
    weightValue[11] = 97;
    weightValue[12] = 23;
    weightValue[13] = 29;

    for (var j = 0; j < 3; j++) {
        var tempValue = checkCode.substring(j, j + 1);
        if (tempValue >= "A" && tempValue <= "Z") {
            checkValue[j] = tempValue.charCodeAt() - 55;
        } else {
            checkValue[j] = tempValue;
        }

        totalValue = totalValue + weightValue[j] * checkValue[j];
    }

    for (var j = 3; j < 14; j++) {
        checkValue[j] = checkCode.substring(j, j + 1);
        totalValue = totalValue + weightValue[j] * checkValue[j];
    }

    c = 1 + (totalValue % 97);
    str1 = String(c);

    if (str1.length == 1) {
        str1 = '0' + str1;
    }

    if (str1 != trim(loanCardNo).substring(14)) {
        result.code = false;
        result.msg = "中征码经校验码校验不正确!";
        return result;
    }
    return result;
}

/**
 * [trim 将数据去掉空格]
 *
 * @param {[type]}
 *            val [description]
 * @return {[type]} [description]
 */
function trim(val) {
    var str = val + "";
    if (str.length == 0)
        return str;
    var re = /^\s*/;
    str = str.replace(re, '');
    re = /\s*$/;
    return str.replace(re, '');
};

/**
 * 军官证
 * @param card
 * @returns {*}
 * @constructor
 */
function  OfficerCard(card) {
    // 规则： 军/兵/士/文/职/广/（其他中文） + "字第" + 4到8位字母或数字 + "号"
    // 样本： 军字第2001988号, 士字第P011816X号
    var result = {
        code: true,
        msg: ""
    };
    var reg = /^[\u4E00-\u9FA5](字第)([0-9a-zA-Z]{4,8})(号?)$/;
    if (reg.test(card) === false) {
        result.code = false;
        result.msg = "请输入正确的军官证!";
        return result;
    }
    return result;
}

/**
 * [checkIdCard 校验身份证]
 *
 * @param {[type]}
 *  idcard [证件号码]
 * @return {[type]} [根据大系统的身份证校验代码判断 15位身份证只校验长度 每一位必须为整数 18位弱校验]
 */
function checkIdCard(idcard) {
    var result = {
        code: true,
        msg: ""
    };
    if (idcard.length > 0 && idcard != null) {
        // 15位身份证号转换18位
        idcard = idLen15To18(idcard);
        if (!idcard) {
            result.code = false;
            result.msg = "请输入正确的证件号码!";
            return result;
        }
        var re = /^\d{17}([0-9]|X)$/;
        var idcard, Y, JYM;
        var S, M;
        var idcard_array = new Array();
        idcard_array = idcard.split("");
        // 先判断18位身份证是否通过基本校验： 如长度 ，每一位是否为整数 以及18位最后一位是否为"X"
        if (!re.test(idcard)) {
            result.code = false;
            result.msg = "请输入正确的证件号码!";
            return result;
        }
        // 再判断18身份证的出生日期与校验位

        // 判断出生日期是否合法
        var date = idcard.substr(6, 4) + idcard.substr(10, 2)
            + idcard.substr(12, 2);
        if (isDate(date)) {
            // 计算校验位
            S = (parseInt(idcard_array[0]) + parseInt(idcard_array[10])) * 7
                + (parseInt(idcard_array[1]) + parseInt(idcard_array[11]))
                * 9
                + (parseInt(idcard_array[2]) + parseInt(idcard_array[12]))
                * 10
                + (parseInt(idcard_array[3]) + parseInt(idcard_array[13]))
                * 5
                + (parseInt(idcard_array[4]) + parseInt(idcard_array[14]))
                * 8
                + (parseInt(idcard_array[5]) + parseInt(idcard_array[15]))
                * 4
                + (parseInt(idcard_array[6]) + parseInt(idcard_array[16]))
                * 2 + parseInt(idcard_array[7]) * 1
                + parseInt(idcard_array[8]) * 6 + parseInt(idcard_array[9])
                * 3;
            Y = S % 11;
            M = "F";
            JYM = "10X98765432";

            // 判断校验位是否合法
            M = JYM.substr(Y, 1);
            if (M != idcard_array[17]) {
                result.code = false;
                result.msg = "证件号码校验位不合法!";
                return result;
            }
            return result;
        } else {
            result.code = false;
            result.msg = "证件号码日期格式不正确!";
            return result;
        }
    } else {
        result.code = false;
        result.msg = "证件号码不能为空!";
        return result;
    }
};


/**
 * [idLen15To18 身份证号码15位转18位]
 * @param  {[String]} idcard 	[页面取值输入的身份证号码]
 * @return {[Boolean/String]}   [18位身份证号码或false]
 */
function idLen15To18(idcard){
    /*每位加权因子*/
    var powers = ["7","9","10","5","8","4","2","1","6","3","7","9","10","5","8","4","2"];

    /*第18位校检码*/
    var parityBit = ["1","0","X","9","8","7","6","5","4","3","2"];

    if(idcard.length==15){
        var id17 = idcard.substring(0,6) + '19' + idcard.substring(6);
        var id18 = getParityBit(id17,powers,parityBit);
        return id17 + id18;
    }else if(idcard.length==18){
        return idcard;
    }else{
        return false;
    }
}

/**
 * [getParityBit 身份证号码15位转18位第18位号码算法]
 * @param  {[String]} id17     [前17位身份证号码]
 * @param  {[Array]} powers    [加权数组]
 * @param  {[Array]} parityBit [第18位校检码数组]
 * @return {[type]}            [身份证号第18位号码]
 */
function getParityBit(id17,powers,parityBit){
    /*加权 */
    var power = 0;
    for(var i=0;i<17;i++){
        power += parseInt(id17.charAt(i),10) * parseInt(powers[i]);
    }
    /*取模*/
    var mod = power % 11;
    return parityBit[mod];
}

/**
 * isDate getMaxDay isNumber 身份证校验用到的三个函数 isDate 判断生日的合法性 getMaxDay 取得天的最大数
 * 包括平年和闰年 isNumber 校验整数
 */
function isDate(date) {
    var fmt = "yyyyMMdd";
    var yIndex = fmt.indexOf("yyyy");
    if (yIndex == -1)
        return false;
    var year = date.substring(yIndex, yIndex + 4);
    var mIndex = fmt.indexOf("MM");
    if (mIndex == -1)
        return false;
    var month = date.substring(mIndex, mIndex + 2);
    var dIndex = fmt.indexOf("dd");
    if (dIndex == -1)
        return false;
    var day = date.substring(dIndex, dIndex + 2);
    if (!isNumber(year) || year > "2100" || year < "1900")
        return false;
    if (!isNumber(month) || month > "12" || month < "01")
        return false;
    if (day > getMaxDay(year, month) || day < "01")
        return false;
    return true;
}
function getMaxDay(year, month) {
    if (month == 4 || month == 6 || month == 9 || month == 11)
        return "30";
    if (month == 2)
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
            return "29";
        else
            return "28";
    return "31";
}
function isNumber(s) {
    var regu = "^[0-9]+$";
    var re = new RegExp(regu);
    if (s.search(re) != -1) {
        return true;
    } else {
        return false;
    }
}

/**
 * [idTypeRocValidator 台湾居民往来大陆通行证]
 * @param {[type]}
 * arr [系统对象]
 * @param {[type]}
 *            idCard [证件号码]
 * @param {[type]}
 *            idCardLength [证件号码长度]
 * @return {[type]} [证件号码合法时返回true，证件号码不合法时返回提示信息]
 */
function idTypeRocValidator(idCard){
    var result = {
        code: true,
        msg: ""
    };
    var re1 = /^([0-9]{8}|[0-9]{10}[0-9A-Za-z]?[0-9A-Za-z]?)$/;
    var re2 = /^[0-9]{10}\([A-Za-z]\)$/;
    var re3 = /^[0-9]{10}\([0-9]{2}\)$/;
    if(re1.test(idCard)||re2.test(idCard)||re3.test(idCard)){
        return result;
    }else{
        result.code = false;
        result.msg = "请输入正确台湾居民往来大陆通行证!";
        return result;
    }
}

/**
 * 其他证件类型
 * @param card
 * @returns {{code: boolean, msg: string}}
 */
function otherCard(card) {
    var result = {
        code: true,
        msg: ""
    };
    if (card.length > 20 || card.length == 0) {
        result.code = false;
        result.msg = "请输入正确的证件号码!";
    }
    return result;
}

/**
 * 户口本号
 */

function hukouben(card){
	 var re = /^[a-zA-Z0-9]{9}$/;
	 var result = {
		        code: true,
		        msg: ""
		    };
	 if(re.test(card)){
		 return result;
	 }else{
		 result.code = false;
	     result.msg = "请输入正确的户口本号码!";
		  return result;
	 }
}

/**
 * 护照
 */

function huzhao(card){
	 var re =new RegExp("(^([PSE]{1}\\d{7}|[GS]{1}\\d{8})$)");
     var card=$(text).val().toUpperCase();
     if(re.test(card)){
       	  var result = {
						code: true,
						msg: ""
					};
 			return result;
     }else{
		 var result = {
						code: false,
						msg: "请输入正确的护照号码"
					};
		 return result;
	}
}

/**
 * 港澳居民来往大陆通行证
 */
function HM(card){
	var re = /^[HM]{1}([0-9]{10}|[0-9]{8})$/;
	if(re.test(card)){
		 var result = {
						code: true,
						msg: ""
					};	
		 return result;
	}else{
		 var result = {
						code: false,
						msg: "请输入正确的港澳居民来往大陆通行证"
					};
		 return result;
	}
}


/**
 * 台湾居民来往大陆通行证
 */
function TW(card){
	var re1 = /^([0-9]{8}|[0-9]{10}[0-9A-Za-z]?[0-9A-Za-z]?)$/;
	var re2 = /^[0-9]{10}\([A-Za-z]\)$/;
	var re3 = /^[0-9]{10}\([0-9]{2}\)$/;
	if(re1.test(card)||re2.test(card)||re3.test(card)){
		var result = {
						code: true,
						msg: ""
					};	
		return result;
	}else{
		 var result = {
						code: false,
						msg: "请输入正确的台湾居民来往大陆通行证"
					};
		 return result;
	}
}

/**
 * 香港居民身份证
 */
function HKcard(card){
	var re = /^((\s?[A-Za-z])|([A-Za-z]{2}))\d{6}(\([0−9aA]\)|[0-9aA])$/;
	if(re.test(card)){
		var hkLegalValue = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    var hkWeightedfactors = [7, 6, 5, 4, 3, 2];
	    var firstCode = card.substring(0, 1);
		var firstIndex = hkLegalValue.indexOf(firstCode) + 1;
	    var total = 0;
	    total = firstIndex * 8;
        for (var i = 0; i < 6; i++) {
            total += card.substring(i + 1, i + 2) * hkWeightedfactors[i];
        }
        var chekcCode = total % 11;
        var checkValue;
        if (chekcCode == 0) {
            checkValue = "1";
        } else if (chekcCode == 1) {
            checkValue = "A";
        } else {
            checkValue = 11 - chekcCode;
        }
        if (checkValue == card.substring(8, 9)) {
        	var result = {
					code: true,
					msg: ""
				};
        	return result;
        }
        var result = {
				code: false,
				msg: "请输入正确的香港居民身份证"
			};
        return result;
	 }else {
		var result = {
						code: false,
						msg: "请输入正确的香港居民身份证"
					};
		 return result;
	 }
}

/**
 * 澳门身份证居民身份证
 */
function OMcard(card){
	var re = /^((\s?[A-Za-z])|([A-Za-z]{2}))\d{6}(\([0−9aA]\)|[0-9aA])$/;
	if(re.test(card)){
		 var result = {
						code: true,
						msg: ""
					};
		 return result;
	 }else{
		var result = {
						code: false,
						msg: "请输入正确的澳门身份证居民身份证"
					};
		 return result;
	 }
}


/**
 * 外国人居留证
 */
function WaiPerson(card){
	var re = /^[a-zA-Z]{3}\d{12}$/;
	 if(re.test(card)){
		var result = {
						code: true,
						msg: ""
					};
	 }else{
		var result = {
						code: false,
						msg: "请输入正确的外国人居留证"
					};
	 }
}

/**
 * 警官证
 */

function police(card){
		var result = {
						code: true,
						msg: ""
					};
		return result;
}


