/**
 * 查询
 * @param value
 */
function refreshDatagrid(queryParam){
    $("#dg1").datagrid("load",queryParam);

}

// 新增
function add(title){
    resetForAdd();
    $('#dialog1').dialog('open').dialog('center').dialog('setTitle',title);
}

// 修改
function edit(title){
    var rows = $('#dg1').datagrid('getSelections');
    if(rows.length == 1){
        resetForAdd();
        $("#formForAdd").form("load", rows[0]);
        $('#dialog1').dialog('open').dialog('center').dialog('setTitle',title);
    }else{
        $.messager.alert("提示", "请选择一条数据进行修改!", "info");
    }
}

function resetForAdd(){
    $('#formForAdd').form('clear');
}

// 保存
function save(entityName) {
    if($("#formForAdd").form('validate')){
        $.ajax({
            type: "POST",//为post请求
            url: "Xsave"+entityName,
            data: $('#formForAdd').serialize(),//将该表单序列化
            dataType: "json",
            async: false,
            success: function (res) {
                if (res.status == "1") {
                    $.messager.alert('提示', '提交成功!', 'info');
                    $("#dialog1").dialog("close");
                    $("#dg1").datagrid("reload");
                } else {
                    $.messager.alert('提示', '提交失败!', 'info');
                }
            },
            error: function (err) {//请求失败之后的操作

            }
        });
    }

}

// 取消
function cancel(){
    $('#dialog1').dialog('close');
}

// 导出
function downloadExcel(entityName,datastatus){
    console.log(entityName)
    console.log(datastatus)
    var row = $("#dg1").datagrid("getSelections");
    var id = '';
    var info = '';
    for( var dataIndex in row){
            id = id + row[dataIndex].id + ",";
    }
    if(id != ''){
            info = '是否将选中的数据导出到Excel表中？';
    }else {
            info = '是否将全部数据导出到Excel表中？'
    }
    $.messager.confirm('操作提示', info, function (r) {
        if (r) {
            window.location.href = "exportExcel"+entityName+"?id="+id +"&datastatus="+datastatus;
        }});
}

// 校验
function check(entityName){
    var row = $("#dg1").datagrid("getSelections");
    var id = '';
    for( var dataIndex in row){
        id = id + row[dataIndex].id + ",";
        if (row[dataIndex].checkstatus != 0){
            $.messager.alert("提示", "只能校验未校验的数据", "info");
            return;
        }
    }
    var info = '';
    if(id != ''){
        info = '是否校验选中的数据？';
    }else {
        info = '是否校验所有数据？'
    }

    $.messager.confirm('提示',info,function (r) {
        if (r) {
            $.messager.progress({
                title: '请稍等',
                msg: '正在校验数据中......',
                /* timeout: 100000,*/
            });

            $.ajax({
                url: "checkDispatch"+entityName,
                dataType: "json",
                async: true,
                data:{"id" : id},
                type: "POST",
                success: function (data) {
                    $("#dg1").datagrid("reload");
                    $.messager.progress('close');
                    if (data.msg == "1") {
                        $.messager.alert('提示', '校验成功！', 'info');
                    }else if (data.msg == "-1") {
                        $.messager.alert('提示', '没有可校验的数据！', 'info');
                    } else {
                        $.messager.alert('提示', '校验完成,将自动下载校验结果', 'info', function (r) {
                            window.location.href = "downLoadCheckResult"+entityName;
                        });
                    }
                },
                error: function () {
                    $.messager.progress('close');
                    $.messager.alert("提示", "校验出错，请重新校验！", "error");
                }
            });
        }
    })
}


// 提交
function submitit(entityName){
    var rows = $('#dg1').datagrid('getSelections');
    var info = '';
    var id = '';
    if(rows.length > 0){
        for(var i = 0;i < rows.length;i++){
            if(rows[i].checkstatus != '1'){
                $.messager.alert("提示", "只能提交校验成功的数据", "info");
                return;
            }
            id = id + rows[i].id + ',';
        }
        info = '是否提交所选中的数据？';
    }else {
        info = '您没有选择数据，是否提交所有数据？';
    }
    $.messager.confirm('提示',info,function (r){
        if(r){
            $.messager.confirm('提示',info,function (r){
                if(r){
                    $.ajax({
                        sync: true,
                        type: "POST",
                        dataType: "json",
                        url: "Xsubmit"+entityName,
                        data:{"id" : id,"datastatus":'${datastatus}'},
                        contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                        success: function (data) {
                            if (data == 0){
                                $.messager.alert('操作提示','提交成功','info');
                                $("#dg1").datagrid("reload");
                            }else {
                                $.messager.alert('操作提示','提交失败','info');
                            }
                        },
                        error: function (err) {
                            $.messager.alert('操作提示','提交失败','error');
                        }
                    });
                    $("#dg1").datagrid("reload");
                }
            });
        }
    });
}

//申请删除dsh
function applyDelete(entityName) {
    var rows = $('#dg1').datagrid('getSelections');
    var info = '';
    var id = '';
    if(rows.length > 0){
        info = '是否申请删除所选中的数据？';
    }else {
        info = '是否申请删除所有数据？';
    }
    for (var i = 0;i < rows.length;i++){
        id = id + rows[i].id + ',';
    }
    $.messager.confirm('提示',info,function (r){
        if(r){
            $.ajax({
                sync: true,
                type: "POST",
                dataType: "json",
                url: "XApplyDelete"+entityName,
                data:{"id" : id,"datastatus":'${datastatus}'},
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (data) {
                    if (data == 0){
                        $.messager.alert('操作提示','申请删除成功','info');
                        $("#dg1").datagrid("reload");
                    }else {
                        $.messager.alert('操作提示','申请删除失败','info');
                    }
                },
                error: function (err) {
                    $.messager.alert('操作提示','申请删除失败','error');
                }
            });
        }
    });
}


/*同意删除申请*/
function agreeApply(entityName,operator,role,shifoushenheziji) {
    var rows = $('#dg1').datagrid('getSelections');
    var info = '';
    var id = '';
    for (var i = 0;i < rows.length;i++){
        id = id + rows[i].id + ',';
        if(operator == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
            $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
            return;
        }
        if (rows[i].operationname != '申请删除'){
            $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
            return;
        }
    }
    if(rows.length > 0){
        info = '是否同意所选中的申请？';
    }else {
        info = '是否同意所有的申请？';
    }
    $.messager.confirm('提示',info,function (r){
        if(r){
            $.ajax({
                sync: true,
                type: "POST",
                dataType: "json",
                url: "XAgreeApply"+entityName,
                data:{"id" : id},
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (data) {
                    if (data == 0){
                        $.messager.alert('操作提示','同意成功','info');
                        $("#dg1").datagrid("reload");
                    }else {
                        $.messager.alert('操作提示','同意失败','info');
                    }
                },
                error: function (err) {
                    $.messager.alert('操作提示','同意失败','error');
                }
            });
        }
    });
}

/*拒绝删除申请*/
function noAgreeApply(entityNme,operator,role,shifoushenheziji) {
    var rows = $('#dg1').datagrid('getSelections');
    var info = '';
    var id = '';
    for (var i = 0;i < rows.length;i++){
        id = id + rows[i].id + ',';
        if(operator== rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
            $.messager.alert("提示","不能对自己提交的数据进行操作！","info");
            return;
        }
        if (rows[i].operationname != '申请删除'){
            $.messager.alert("提示","请选择操作名为申请删除的数据！","error");
            return;
        }
    }
    if(rows.length > 0){
        info = '是否拒绝所选中的申请？';
    }else {
        info = '是否拒绝所有的申请？';
    }
    $.messager.confirm('提示',info,function (r){
        if(r){
            $.ajax({
                sync: true,
                type: "POST",
                dataType: "json",
                url: "XNoAgreeApply"+entityNme,
                data:{"id" : id},
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (data) {
                    if (data == 0){
                        $.messager.alert('操作提示','拒绝成功','info');
                        $("#dg1").datagrid("reload");
                    }else {
                        $.messager.alert('操作提示','拒绝失败','info');
                    }
                },
                error: function (err) {
                    $.messager.alert('操作提示','拒绝意失败','error');
                }
            });
        }
    });
}

/*审核通过*/
function agreeAudit(entityName,operator,role,shifoushenheziji) {
    var rows = $('#dg1').datagrid('getSelections');
    var info = '';
    var id = '';
    for (var i = 0;i < rows.length;i++){
        id = id + rows[i].id + ',';
        if(operator == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
            $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
            return;
        }
        if (rows[i].checkstatus != 1){
            $.messager.alert("提示","请选择校验通过的数据！","error");
            return;
        }
        if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
        }else {
            $.messager.alert("提示","请选择操作名为空的数据！","error");
            return;
        }
    }
    if(rows.length > 0){
        info = '是否审核通过所选中的数据？';
    }else {
        info = '是否审核通过所有数据？';
    }
    $.messager.confirm('提示',info,function (r){
        if(r){
            $.ajax({
                sync: true,
                type: "POST",
                dataType: "json",
                url: "XAgreeAudit"+entityName,
                data:{"id" : id},
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (data) {
                    if (data == 0){
                        $.messager.alert('操作提示','审核通过成功','info');
                        $("#dg1").datagrid("reload");
                    }else {
                        $.messager.alert('操作提示','审核通过失败','info');
                    }
                },
                error: function (err) {
                    $.messager.alert('操作提示','审核通过失败','error');
                }
            });
        }
    });
}

/*审核不通过*/
function noAgreeAudit(entityName,operator,role,shifoushenheziji) {
    var rows = $('#dg1').datagrid('getSelections');
    var info = '';
    var id = '';
    for (var i = 0;i < rows.length;i++){
        id = id + rows[i].id + ',';
        if(operator == rows[i].operator && role == 'makerAndChecker' && shifoushenheziji == 'no'){
            $.messager.alert("提示","不能对自己提交的数据进行操作！","error");
            return;
        }
        if (rows[i].checkstatus != 1){
            $.messager.alert("提示","请选择校验通过的数据！","error");
            return;
        }
        if (rows[i].operationname == null || rows[i].operationname == "" || rows[i].operationname == " "){
        }else {
            $.messager.alert("提示","请选择操作名为空的数据！","error");
            return;
        }
    }
    if(rows.length > 0){
        info = '是否审核不通过所选中的数据？';
    }else {
        info = '是否审核不通过所有数据？';
    }
    $.messager.prompt('提示',info,function (r){
        if(r){
            console.log(r)
            console.log("范德萨发生的")
            console.info("12")
            $.ajax({
                sync: true,
                type: "POST",
                dataType: "json",
                url: "XNoAgreeAudit"+entityName,
                data:{"id" : id,"reason":r},
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (data) {
                    if (data == 0){
                        $.messager.alert('操作提示','审核不通过成功','info');
                        $("#dg1").datagrid("reload");
                    }else {
                        $.messager.alert('操作提示','审核不通过失败','info');
                    }
                },
                error: function (err) {
                    $.messager.alert('操作提示','审核不通过失败','error');
                }
            });
        }else {
            $.messager.alert('操作提示','审核不通过原因必填','info');
        }
    });
}

//生成报文
function genMessage(entityName){
    var v = $('#dateform').form('validate');
    if(v){
        var value = $('#dateselect').datebox('getValue');
        $('#datedialog').window('close');
        $.messager.confirm('提示','是否生成报文？',function (r){
            if(r){
                $.messager.progress({
                    title: '请稍等',
                    msg: '数据处理中......'
                });
                $.ajax({
                    url: "createReport"+entityName,
                    type: "POST",
                    contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                    async: true,
                    data:{"date":value},
                    dataType:'json',
                    success: function (data) {
                        $.messager.progress('close');
                        $.messager.alert('操作提示', '生成报文成功'+data+'条', 'info');
                        $("#dg1").datagrid("reload");
                    },
                    error: function (error) {
                        $.messager.progress('close');
                        $.messager.alert('操作提示', '生成报文失败', 'error');
                    }
                })
            }
        });
    }

}

// 打回按钮
function goback(entityName) {
    var rows = $('#dg1').datagrid('getSelections');
    var info = '';
    var id = '';
    for (var i = 0;i < rows.length;i++){
        id = id + rows[i].id + ',';
    }
    if(rows.length > 0){
        info = '是否打回选中的数据？';
    }else {
        info = '是否打回所有的数据？';
    }
    $.messager.confirm('提示',info,function (r){
        if(r){
            $.ajax({
                sync: true,
                type: "POST",
                dataType: "json",
                url: "Xgoback"+entityName,
                data:{"id" : id},
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (data) {
                    if (data == 0){
                        $.messager.alert('操作提示','打回成功','info');
                        $("#dg1").datagrid("reload");
                    }else {
                        $.messager.alert('操作提示','打回失败','info');
                    }
                },
                error: function (err) {
                    $.messager.alert('操作提示','打回失败','error');
                }
            });
        }
    });
}

// 历史数据导出导出
function hisDownloadExcel(entityName,datastatus){
    var row = $("#dg1").datagrid("getSelections");
    var id = '';
    var info = '';
    for( var dataIndex in row){
        id = id + row[dataIndex].id + ",";
    }
    if(id != ''){
        info = '是否将选中的数据导出到Excel表中？';
    }else {
        info = '是否将全部数据导出到Excel表中？'
    }
    $.messager.confirm('操作提示', info, function (r) {
        if (r) {
            window.location.href = "exportExcelH"+entityName+"?id="+id+"&datastatus="+datastatus;
        }
    });
}

//数据打回
function dataBack(entityName){
    var v = $('#dateform').form('validate');
    if(v){
        var value = $('#dateselect').datebox('getValue');
        $('#datedialog').window('close');
        $.messager.confirm('提示','是否打回数据？',function (r){
            if(r){
                $.messager.progress({
                    title: '请稍等',
                    msg: '数据处理中......'
                });
                $.ajax({
                    url: "Xdataback"+entityName,
                    type: "POST",
                    contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                    async: true,
                    data:{"date":value},
                    dataType:'json',
                    success: function (data) {
                        if(data != 0){
                            $.messager.progress('close');
                            $.messager.alert('操作提示', '数据打回成功！', 'info');
                            $("#dg1").datagrid("reload");
                        }else{
                            $.messager.progress('close');
                            $.messager.alert('操作提示', '没有数据可打回！', 'error');
                            $("#dg1").datagrid("reload");
                        }

                    },
                    error: function (error) {
                        $.messager.progress('close');
                        $.messager.alert('操作提示', '数据打回失败！', 'error');
                    }
                })
            }
        });
    }
}